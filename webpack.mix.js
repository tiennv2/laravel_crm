let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.js('resources/assets/js/app.js', 'public/js')
    .sass('resources/assets/sass/vendor.scss', 'public/css')
    .sass('resources/assets/sass/app.scss', 'public/css')
    .copy('resources/assets/metronic/media/app/img/bg/bg-3.jpg', 'public/img/login-bg.jpg')
    .copy('resources/assets/metronic/media/app/img/error/bg6.jpg', 'public/img/error-bg.jpg')
    .webpackConfig({
      output: {
        chunkFilename: `js/[name]${mix.config.inProduction ? '.[chunkhash].chunk.js' : '.chunk.js'}`
      }
    })
    .copyDirectory('resources/assets/img', 'public/img');

mix.browserSync({
    proxy: 'localhost:8081'
});

if (mix.inProduction()) {
    mix.version();
}
