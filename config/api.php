<?php

return [
    'qrcode' => env('API_QRCODE'),
    'affiliate' => env('API_AFFILIATE'),
    'core' => env('CORE_API_URL', 'https://core.icheck.com.vn'),
    'gateway' => env('GATEWAY_API_URL', 'https://core.icheck.com.vn'),
    'chat' => env('CHAT_API_URL', 'https://chat.icheck.com.vn'),
    'batch_download' => env('API_BATCH_DOWNLOAD'),
    'batch_download_key' => env('API_BATCH_DOWNLOAD_KEY'),
    'batch_download_domain' => env('API_BATCH_DOWNLOAD_DOMAIN'),
    'icheck_backend' => env('API_ICHECK_BACKEND'),
    'icheck-authorization' => env('ICHECK-AUTHORIZATION'),
    'collaborator_transfers' => env('API_COLLABORATOR_TRANSFERS'),
    'transfers_authorization' => env('TRANSFERS_AUTHORIZATION'),
    'business_gateway' => env('API_BUSINESS_GATEWAY')
];

