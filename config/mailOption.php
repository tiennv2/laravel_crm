<?php

return [
    'gateway_mail' => env('GATEWAY_MAIL', 'https://example@gmail.com'),
    'gateway_mail_name' => env('GATEWAY_MAIL_NAME', 'iCheck'),
    'collaborator_mail' => env('COLLABORATOR_MAIL', ''),
    'collaborator_mail_name' => env('COLLABORATOR_MAIL_NAME', 'iCheck'),
];
