<?php

return [
    'service_account_credentials' => env('GOOGLE_SERVICE_ACCOUNT_CREDENTIALS'),
    'map_api_key' => env('GOOGLE_MAP_API_KEY'),
];
