<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Database Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the database connections below you wish
    | to use as your default connection for all database work. Of course
    | you may use many connections at once using the Database library.
    |
    */

    'default' => env('DB_CONNECTION', 'mysql'),

    /*
    |--------------------------------------------------------------------------
    | Database Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the database connections setup for your application.
    | Of course, examples of configuring each database platform that is
    | supported by Laravel is shown below to make development simple.
    |
    |
    | All database work in Laravel is done through the PHP PDO facilities
    | so make sure you have the driver for your particular database of
    | choice installed on your machine before you begin development.
    |
    */

    'connections' => [

        'sqlite' => [
            'driver' => 'sqlite',
            'database' => env('DB_DATABASE', database_path('database.sqlite')),
            'prefix' => '',
        ],

        'mysql' => [
            'driver' => 'mysql',
            'host' => env('DB_HOST', '127.0.0.1'),
            'port' => env('DB_PORT', '3306'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'unix_socket' => env('DB_SOCKET', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => 'u_',
            'strict' => true,
            'engine' => null,
            'modes'  => [
                'ONLY_FULL_GROUP_BY',
                'STRICT_TRANS_TABLES',
                'NO_ZERO_IN_DATE',
                'NO_ZERO_DATE',
                'ERROR_FOR_DIVISION_BY_ZERO',
                'NO_ENGINE_SUBSTITUTION',
            ],
        ],

        'pgsql' => [
            'driver' => 'pgsql',
            'host' => env('DB_HOST', '127.0.0.1'),
            'port' => env('DB_PORT', '5432'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'schema' => 'public',
            'sslmode' => 'prefer',
        ],

        'sqlsrv' => [
            'driver' => 'sqlsrv',
            'host' => env('DB_HOST', 'localhost'),
            'port' => env('DB_PORT', '1433'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'charset' => 'utf8',
            'prefix' => '',
        ],

        'icheck_product' => [
            'driver'      => 'mysql',
            'host'        => env('DB_ICHECK_PRODUCT_HOST', '127.0.0.1'),
            'port'        => env('DB_ICHECK_PRODUCT_PORT', '3306'),
            'database'    => env('DB_ICHECK_PRODUCT_DATABASE', 'forge'),
            'username'    => env('DB_ICHECK_PRODUCT_USERNAME', 'forge'),
            'password'    => env('DB_ICHECK_PRODUCT_PASSWORD', ''),
            'unix_socket' => env('DB_ICHECK_PRODUCT_SOCKET', ''),
            'charset'     => 'utf8mb4',
            'collation'   => 'utf8mb4_unicode_ci',
            'prefix'      => '',
            'strict'      => true,
            'engine'      => null,
        ],

        'icheck_user' => [
            'driver'      => 'mysql',
            'host'        => env('DB_ICHECK_USER_HOST', '127.0.0.1'),
            'port'        => env('DB_ICHECK_USER_PORT', '3306'),
            'database'    => env('DB_ICHECK_USER_DATABASE', 'forge'),
            'username'    => env('DB_ICHECK_USER_USERNAME', 'forge'),
            'password'    => env('DB_ICHECK_USER_PASSWORD', ''),
            'unix_socket' => env('DB_ICHECK_USER_SOCKET', ''),
            'charset'     => 'utf8mb4',
            'collation'   => 'utf8mb4_unicode_ci',
            'prefix'      => '',
            'strict'      => true,
            'engine'      => null,
        ],

        'icheck_gateway' => [
            'driver'      => 'mysql',
            'host'        => env('DB_ICHECK_GATEWAY_HOST', '127.0.0.1'),
            'port'        => env('DB_ICHECK_GATEWAY_PORT', '3306'),
            'database'    => env('DB_ICHECK_GATEWAY_DATABASE', 'forge'),
            'username'    => env('DB_ICHECK_GATEWAY_USERNAME', 'forge'),
            'password'    => env('DB_ICHECK_GATEWAY_PASSWORD', ''),
            'unix_socket' => env('DB_ICHECK_GATEWAY_SOCKET', ''),
            'charset'     => 'utf8mb4',
            'collation'   => 'utf8mb4_unicode_ci',
            'prefix'      => '',
            'strict'      => true,
            'engine'      => null,
        ],
        'icheck-new-cms' => [
            'driver'      => 'mysql',
            'host'        => env('DB_ICHECK_NEW-CMS_HOST', '127.0.0.1'),
            'port'        => env('DB_ICHECK_NEW-CMS_PORT', '3306'),
            'database'    => env('DB_ICHECK_NEW-CMS_DATABASE', 'forge'),
            'username'    => env('DB_ICHECK_NEW-CMS_USERNAME', 'forge'),
            'password'    => env('DB_ICHECK_NEW-CMS_PASSWORD', ''),
            'unix_socket' => env('DB_ICHECK_NEW-CMS_SOCKET', ''),
            'charset'     => 'utf8mb4',
            'collation'   => 'utf8mb4_unicode_ci',
            'prefix'      => '',
            'strict'      => true,
            'engine'      => null,
        ],
        'icheck-legacy-business' => [
            'driver'      => 'mysql',
            'host'        => env('DB_ICHECK_LEGACY_BUSINESS_HOST', '127.0.0.1'),
            'port'        => env('DB_ICHECK_LEGACY_BUSINESS_PORT', '3306'),
            'database'    => env('DB_ICHECK_LEGACY_BUSINESS_DATABASE', 'forge'),
            'username'    => env('DB_ICHECK_LEGACY_BUSINESS_USERNAME', 'forge'),
            'password'    => env('DB_ICHECK_LEGACY_BUSINESS_PASSWORD', ''),
            'unix_socket' => env('DB_ICHECK_NEW-CMS_SOCKET', ''),
            'charset'     => 'utf8mb4',
            'collation'   => 'utf8mb4_unicode_ci',
            'prefix'      => '',
            'strict'      => true,
            'engine'      => null,
        ],
        'icheck_business' => [
            'driver'      => 'mysql',
            'host'        => env('DB_ICHECK_BUSINESS_HOST', '127.0.0.1'),
            'port'        => env('DB_ICHECK_BUSINESS_PORT', '3306'),
            'database'    => env('DB_ICHECK_BUSINESS_DATABASE', 'forge'),
            'username'    => env('DB_ICHECK_BUSINESS_USERNAME', 'forge'),
            'password'    => env('DB_ICHECK_BUSINESS_PASSWORD', ''),
            'unix_socket' => env('DB_ICHECK_BUSINESS_SOCKET', ''),
            'charset'     => 'utf8mb4',
            'collation'   => 'utf8mb4_unicode_ci',
            'prefix'      => '',
            'strict'      => true,
            'engine'      => null,
        ],
        'icheck_business_authorization' => [
            'driver'      => 'mysql',
            'host'        => env('DB_ICHECK_BUSINESS_AUTHORIZATION_HOST', '127.0.0.1'),
            'port'        => env('DB_ICHECK_BUSINESS_AUTHORIZATION_PORT', '3306'),
            'database'    => env('DB_ICHECK_BUSINESS_AUTHORIZATION_DATABASE', 'forge'),
            'username'    => env('DB_ICHECK_BUSINESS_AUTHORIZATION_USERNAME', 'forge'),
            'password'    => env('DB_ICHECK_BUSINESS_AUTHORIZATION_PASSWORD', ''),
            'unix_socket' => env('DB_ICHECK_BUSINESS_AUTHORIZATION_SOCKET', ''),
            'charset'     => 'utf8mb4',
            'collation'   => 'utf8mb4_unicode_ci',
            'prefix'      => '',
            'strict'      => true,
            'engine'      => null,
        ],

        'icheck_old_business' => [
            'driver'      => 'mysql',
            'host'        => env('DB_ICHECK_OLD_BUSINESS_HOST', '127.0.0.1'),
            'port'        => env('DB_ICHECK_OLD_BUSINESS_PORT', '3306'),
            'database'    => env('DB_ICHECK_OLD_BUSINESS_DATABASE', 'forge'),
            'username'    => env('DB_ICHECK_OLD_BUSINESS_USERNAME', 'forge'),
            'password'    => env('DB_ICHECK_OLD_BUSINESS_PASSWORD', ''),
            'unix_socket' => env('DB_ICHECK_OLD_BUSINESS_SOCKET', ''),
            'charset'     => 'utf8mb4',
            'collation'   => 'utf8mb4_unicode_ci',
            'prefix'      => '',
            'strict'      => true,
            'engine'      => null,
        ],

        'icheck_collaborator' => [
            'driver'      => 'mysql',
            'host'        => env('DB_ICHECK_COLLABORATOR_HOST', '127.0.0.1'),
            'port'        => env('DB_ICHECK_COLLABORATOR_PORT', '3306'),
            'database'    => env('DB_ICHECK_COLLABORATOR_DATABASE', 'forge'),
            'username'    => env('DB_ICHECK_COLLABORATOR_USERNAME', 'forge'),
            'password'    => env('DB_ICHECK_COLLABORATOR_PASSWORD', ''),
            'unix_socket' => env('DB_ICHECK_COLLABORATOR_SOCKET', ''),
            'charset'     => 'utf8mb4',
            'collation'   => 'utf8mb4_unicode_ci',
            'prefix'      => '',
            'strict'      => true,
            'engine'      => null,
        ],

        'icheck_qrcode' => [
            'driver'      => 'mysql',
            'host'        => env('DB_ICHECK_QRCODE_HOST', '127.0.0.1'),
            'port'        => env('DB_ICHECK_QRCODE_PORT', '3306'),
            'database'    => env('DB_ICHECK_QRCODE_DATABASE', 'forge'),
            'username'    => env('DB_ICHECK_QRCODE_USERNAME', 'forge'),
            'password'    => env('DB_ICHECK_QRCODE_PASSWORD', ''),
            'unix_socket' => env('DB_ICHECK_QRCODE_SOCKET', ''),
            'charset'     => 'utf8mb4',
            'collation'   => 'utf8mb4_unicode_ci',
            'prefix'      => '',
            'strict'      => true,
            'engine'      => null,
            'modes'  => [
                'ONLY_FULL_GROUP_BY',
                'STRICT_TRANS_TABLES',
                'NO_ZERO_IN_DATE',
                'NO_ZERO_DATE',
                'ERROR_FOR_DIVISION_BY_ZERO',
                'NO_ENGINE_SUBSTITUTION',
            ],
        ],

        'icheck_qrcode_agency' => [
            'driver'      => 'mysql',
            'host'        => env('DB_ICHECK_QRCODE_AGENCY_HOST', '127.0.0.1'),
            'port'        => env('DB_ICHECK_QRCODE_AGENCY_PORT', '3306'),
            'database'    => env('DB_ICHECK_QRCODE_AGENCY_DATABASE', 'forge'),
            'username'    => env('DB_ICHECK_QRCODE_AGENCY_USERNAME', 'forge'),
            'password'    => env('DB_ICHECK_QRCODE_AGENCY_PASSWORD', ''),
            'unix_socket' => env('DB_ICHECK_QRCODE_AGENCY_SOCKET', ''),
            'charset'     => 'utf8mb4',
            'collation'   => 'utf8mb4_unicode_ci',
            'prefix'      => '',
            'strict'      => true,
            'engine'      => null,
            'modes'  => [
                'ONLY_FULL_GROUP_BY',
                'STRICT_TRANS_TABLES',
                'NO_ZERO_IN_DATE',
                'NO_ZERO_DATE',
                'ERROR_FOR_DIVISION_BY_ZERO',
                'NO_ENGINE_SUBSTITUTION',
            ],
        ],

        'icheck_expo' => [
            'driver'      => 'mysql',
            'host'        => env('DB_ICHECK_EXPO_HOST', '127.0.0.1'),
            'port'        => env('DB_ICHECK_EXPO_PORT', '3306'),
            'database'    => env('DB_ICHECK_EXPO_DATABASE', 'forge'),
            'username'    => env('DB_ICHECK_EXPO_USERNAME', 'forge'),
            'password'    => env('DB_ICHECK_EXPO_PASSWORD', ''),
            'unix_socket' => env('DB_ICHECK_EXPO_SOCKET', ''),
            'charset'     => 'utf8mb4',
            'collation'   => 'utf8mb4_unicode_ci',
            'prefix'      => '',
            'strict'      => true,
            'engine'      => null,
        ],

        'mongodb' => [
            'driver'   => 'mongodb',
            'dsn' => env('DB_MONGO_DSN'),
            'database' => env('DB_MONGO_DATABASE'),
        ],

    ],

    /*
    |--------------------------------------------------------------------------
    | Migration Repository Table
    |--------------------------------------------------------------------------
    |
    | This table keeps track of all the migrations that have already run for
    | your application. Using this information, we can determine which of
    | the migrations on disk haven't actually been run in the database.
    |
    */

    'migrations' => 'migrations',

    /*
    |--------------------------------------------------------------------------
    | Redis Databases
    |--------------------------------------------------------------------------
    |
    | Redis is an open source, fast, and advanced key-value store that also
    | provides a richer set of commands than a typical key-value systems
    | such as APC or Memcached. Laravel makes it easy to dig right in.
    |
    */

    'redis' => [

        'client' => 'predis',

        'default' => [
            'host' => env('REDIS_HOST', '127.0.0.1'),
            'password' => env('REDIS_PASSWORD', null),
            'port' => env('REDIS_PORT', 6379),
            'database' => 0,
        ],

    ],

];
