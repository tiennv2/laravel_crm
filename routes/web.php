<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/config', ['uses' => function () {
    return;
}]);


Auth::routes();

Route::get('/app/{all}', ['middleware' => 'auth', 'uses' => function () {
    return file_get_contents(public_path('app/index.html'));
}])->where('all', '.*');

Route::any('apiProxy', 'APIProxyController@index');
Route::any('dhlProxy', 'APIProxyController@dhl');

Route::group([
    'prefix' => 'contributed-products',
], function () {
    Route::get('count', 'ContributedProductController@count');
});

Route::group([
    'prefix' => 'product',
    'namespace' => 'Product',
], function () {
    Route::group([
        'prefix' => 'categories',
    ], function () {
        Route::get('/', ['uses' => 'ProductCategoryController@index']);
        Route::post('/', ['uses' => 'ProductCategoryController@store']);
        Route::post('{category}', ['uses' => 'ProductCategoryController@update']);
    });

    Route::group([
        'prefix' => 'products',
    ], function () {
        Route::post('/', ['uses' => 'ProductController@index']);
        Route::post('upload', ['uses' => 'UploadController@upload']);
        Route::get('{product}', ['uses' => 'ProductController@show']);
    });

    Route::group([
        'prefix' => 'attributes',
    ], function () {
        Route::get('/', ['uses' => 'AttributeController@index']);
    });

    Route::group([
        'prefix' => 'sellers',
    ], function () {
        Route::get('/', ['uses' => 'SellerController@index']);
        Route::post('/', ['uses' => 'SellerController@store']);
    });
});

Route::group([
    'prefix' => 'contributedProducts',
], function () {
    Route::get('/', 'ContributedProductController@index');
    Route::get('reviewHistorical', 'ContributedProductController@reviewHistorical');
    Route::post('approve', 'ContributedProductController@approve');
    Route::post('reject', 'ContributedProductController@reject');
    Route::get('count', 'ContributedProductController@count');
});

Route::group([
    'prefix' => 'accounts',
], function () {
    Route::get('/', ['uses' => 'AccountController@index']);
    Route::get('{account}', ['uses' => 'AccountController@show']);
});
Route::get('/test', function () {
    return view('test');
});

//Affiliate
Route::group([
    'namespace' => 'AFFILIATE',
    'prefix' => 'affiliate',
    'as' => 'AFFILIATE::',
    'middleware' => 'auth'
], function () {
    Route::group([
        'prefix' => 'product',
        'as' => 'product@',
    ], function () {
        Route::get('active/{id}', ['as' => 'active', 'uses' => 'ProductController@active']);
        Route::get('deactive/{id}', ['as' => 'deactive', 'uses' => 'ProductController@deactive']);
    });
    Route::group([
        'prefix' => 'promotion',
        'as' => 'promotion@',
    ], function () {
        Route::get('/', ['as' => 'index', 'uses' => 'PromotionController@index']);
        Route::get('/statistics', ['as' => 'statistics', 'uses' => 'PromotionController@getStatistics']);
        Route::get('/yesterday_statistics', ['as' => 'yesterday_statistics', 'uses' => 'PromotionController@getStatisticsOfYesterday']);
        Route::get('/add', ['as' => 'add', 'uses' => 'PromotionController@add']);
        Route::post('/store', ['as' => 'store', 'uses' => 'PromotionController@store']);
        Route::get('/{id}/edit', ['as' => 'edit', 'uses' => 'PromotionController@edit']);
        Route::get('/{id}/detail', ['as' => 'detail', 'uses' => 'PromotionController@detail']);
        Route::get('/{id}/nominate', ['as' => 'nominate', 'uses' => 'PromotionController@nominate']);
        Route::match(['put', 'patch'], '/{id}', ['as' => 'update', 'uses' => 'PromotionController@update']);
        Route::get('/{id}/delete', ['as' => 'delete', 'uses' => 'PromotionController@delete']);
        Route::get('/statistic_filter', ['as' => 'statistic_filter', 'uses' => 'PromotionController@statistic_filter']);
        Route::get('active/{id}', ['as' => 'active', 'uses' => 'PromotionController@active']);
        Route::get('deactive/{id}', ['as' => 'deactive', 'uses' => 'PromotionController@deactive']);
        Route::get('approve/{id}', ['as' => 'approve', 'uses' => 'PromotionController@approve']);
        Route::post('nominates/{id}', ['as' => 'nominate_order', 'uses' => 'PromotionController@orderNominate']);
    });


});


Route::group([
    'namespace' => 'Qrcode',
    'prefix' => 'qrcode',
    'as' => 'Qrcode::',
    'middleware' => 'auth'
], function () {
    Route::group([
        'prefix' => 'companies',
        'as' => 'company@',
    ], function () {
        Route::get('/', ['as' => 'index', 'uses' => 'CompanyController@index']);
        Route::get('/new_company', ['as' => 'new_company', 'uses' => 'CompanyController@new_company']);
        Route::get('/add', ['as' => 'add', 'uses' => 'CompanyController@add']);
        Route::get('/edit/{id}', ['as' => 'edit', 'uses' => 'CompanyController@edit']);
        Route::post('/store', ['as' => 'store', 'uses' => 'CompanyController@store']);
        Route::match(['put', 'patch'], '/{id}', ['as' => 'update', 'uses' => 'CompanyController@update']);
        Route::post('/{id}/inbox', ['as' => 'inbox', 'uses' => 'CompanyController@inbox']);
        Route::get('/admin-inbox', ['as' => 'admin-inbox', 'uses' => 'CompanyController@getInboxList']);
        Route::post('/{id}/note', ['as' => 'note', 'uses' => 'CompanyController@note']);
        Route::get('/{id}/approve', ['as' => 'approve', 'uses' => 'CompanyController@approve']);
        Route::get('/{id}/disapprove', ['as' => 'disapprove', 'uses' => 'CompanyController@disapprove']);
        Route::get('/{id}/lock', ['as' => 'lock', 'uses' => 'CompanyController@lock']);
        Route::get('/{id}/unlock', ['as' => 'unlock', 'uses' => 'CompanyController@unlock']);

    });

    Route::group([
        'prefix' => 'agencies',
        'as' => 'agency@',
    ], function () {
        Route::get('/', ['as' => 'index', 'uses' => 'AgencyController@index']);
        Route::get('/add', ['as' => 'add', 'uses' => 'AgencyController@add']);
        Route::get('/{id}/edit', ['as' => 'edit', 'uses' => 'AgencyController@edit']);
        Route::post('/store', ['as' => 'store', 'uses' => 'AgencyController@store']);
        Route::match(['put', 'patch'], '/{id}', ['as' => 'update', 'uses' => 'AgencyController@update']);
        Route::get('/{id}/approve', ['as' => 'approve', 'uses' => 'AgencyController@approve']);
        Route::get('/{id}/disapprove', ['as' => 'disapprove', 'uses' => 'AgencyController@disapprove']);
        Route::get('/{id}/delete', ['as' => 'delete', 'uses' => 'AgencyController@delete']);

    });

    Route::group([
        'prefix' => 'districts',
        'as' => 'district@',
    ], function () {
        Route::post('/', ['as' => 'showDistrictsInCity', 'uses' => 'DistrictController@showDistrictsInCity']);
    });
    Route::group([
        'prefix' => 'order',
        'as' => 'order@',
    ], function () {
        Route::get('/', ['as' => 'index', 'uses' => 'OrderController@index']);
        Route::get('/add', ['as' => 'add', 'uses' => 'OrderController@add']);
        Route::post('/store', ['as' => 'store', 'uses' => 'OrderController@store']);
        Route::get('/{id}/approve', ['as' => 'approve', 'uses' => 'OrderController@approve']);
        Route::get('/{id}/disapprove', ['as' => 'disapprove', 'uses' => 'OrderController@disapprove']);
        Route::match(['put', 'patch'], '/{id}', ['as' => 'update', 'uses' => 'OrderController@update']);
        Route::get('/{collaborator_phone}/filter', ['as' => 'filter', 'uses' => 'OrderController@getOrdersByAgency']);
        Route::get('/export_excel', ['as' => 'export_excel', 'uses' => 'OrderController@export_excel']);
        Route::post('/updateOrderValue', ['as' => 'updateOrderValue', 'uses' => 'OrderController@updateOrderValue']);
        Route::post('/updatePrintValue', ['as' => 'updatePrintValue', 'uses' => 'OrderController@updatePrintValue']);
        Route::post('/updatePrintQuantity', ['as' => 'updatePrintQuantity', 'uses' => 'OrderController@updatePrintQuantity']);
        Route::post('/updatePaymentStatus/{id}', ['as' => 'updatePaymentStatus', 'uses' => 'OrderController@updatePaymentStatus']);
    });

    Route::group([
        'prefix' => 'marketingStamps',
        'as' => 'marketingStamp@',
    ], function () {
        Route::get('/', ['as' => 'index', 'uses' => 'MarketingStampController@index']);
        Route::get('/{id}/approve', ['as' => 'approve', 'uses' => 'MarketingStampController@approve']);
        Route::get('/{id}/disapprove', ['as' => 'disapprove', 'uses' => 'MarketingStampController@disapprove']);
        Route::get('/{id}/activeShowProduct', ['as' => 'activeShowProduct', 'uses' => 'MarketingStampController@activeShowProduct']);
        Route::get('/{id}/deactiveShowProduct', ['as' => 'deactiveShowProduct', 'uses' => 'MarketingStampController@deactiveShowProduct']);
        Route::get('/{id}/activeShowVendor', ['as' => 'activeShowVendor', 'uses' => 'MarketingStampController@activeShowVendor']);
        Route::get('/{id}/deactiveShowVendor', ['as' => 'deactiveShowVendor', 'uses' => 'MarketingStampController@deactiveShowVendor']);
        Route::get('/{id}/activeShowRelated', ['as' => 'activeShowRelated', 'uses' => 'MarketingStampController@activeShowRelated']);
        Route::get('/{id}/deactiveShowRelated', ['as' => 'deactiveShowRelated', 'uses' => 'MarketingStampController@deactiveShowRelated']);
        Route::get('/{id}/activeShowStore', ['as' => 'activeShowStore', 'uses' => 'MarketingStampController@activeShowStore']);
        Route::get('/{id}/deactiveShowStore', ['as' => 'deactiveShowStore', 'uses' => 'MarketingStampController@deactiveShowStore']);
        Route::get('/{id}/activeShowMessage', ['as' => 'activeShowMessage', 'uses' => 'MarketingStampController@activeShowMessage']);
        Route::get('/{id}/deactiveShowMessage', ['as' => 'deactiveShowMessage', 'uses' => 'MarketingStampController@deactiveShowMessage']);
        Route::get('/{id}/activeShowPrice', ['as' => 'activeShowPrice', 'uses' => 'MarketingStampController@activeShowPrice']);
        Route::get('/{id}/deactiveShowPrice', ['as' => 'deactiveShowPrice', 'uses' => 'MarketingStampController@deactiveShowPrice']);
        Route::get('/{id}/activeShowInfo', ['as' => 'activeShowInfo', 'uses' => 'MarketingStampController@activeShowInfo']);
        Route::get('/{id}/deactiveShowInfo', ['as' => 'deactiveShowInfo', 'uses' => 'MarketingStampController@deactiveShowInfo']);
        Route::get('/{id}/delRequestExpired', ['as' => 'delRequestExpired', 'uses' => 'MarketingStampController@delRequestExpired']);
        Route::get('/{id}/delete', ['as' => 'delete', 'uses' => 'MarketingStampController@delete']);
        Route::get('/{id}/getById', ['as' => 'getById', 'uses' => 'MarketingStampController@getById']);
        Route::post('/{id}/updateExpiredTime', ['as' => 'updateExpiredTime', 'uses' => 'MarketingStampController@updateExpiredTime']);
        Route::post('/{id}/expandExpire', ['as' => 'expandExpire', 'uses' => 'MarketingStampController@expandExpire']);
    });

    Route::group([
        'prefix' => 'batch',
        'as' => 'batch@',
    ], function () {
        Route::get('/', ['as' => 'index', 'uses' => 'BatchController@index']);
        Route::get('/{id}/lock_batch', ['as' => 'lock_batch', 'uses' => 'BatchController@lock_batch']);
        Route::get('/{id}/unlock_batch', ['as' => 'unlock_batch', 'uses' => 'BatchController@unlock_batch']);
        Route::post('/{id}/download', ['as' => 'download', 'uses' => 'BatchController@download']);
    });
    Route::group(['prefix' => 'products',
        'as' => 'product@',
    ], function () {
        Route::get('/{id}/approve_product_file', ['as' => 'approve_product_file', 'uses' => 'ProductController@approve_product_file']);
        Route::get('/{id}/disapprove_product_file', ['as' => 'disapprove_product_file', 'uses' => 'ProductController@disapprove_product_file']);
        Route::get('/{id}/upload_product_file', ['as' => 'get_upload_product_file', 'uses' => 'ProductController@get_upload_product_file']);
        Route::post('/{id}/upload_product_file', ['as' => 'post_upload_product_file', 'uses' => 'ProductController@post_upload_product_file']);
    });
    Route::group([
        'prefix' => 'templates',
        'as' => 'template@',
    ], function () {
        Route::get('/', ['as' => 'index', 'uses' => 'TemplateController@index']);
        Route::get('/batches_by_account', ['as' => 'getBatchesByAccount', 'uses' => 'TemplateController@getBatchesByAccount']);
        Route::get('/batches_by_template', ['as' => 'getBatchesByTemplate', 'uses' => 'TemplateController@getBatchesByTemplate']);
        Route::get('/add', ['as' => 'add', 'uses' => 'TemplateController@add']);
        Route::post('/store', ['as' => 'store', 'uses' => 'TemplateController@store']);
        Route::get('/{id}/edit', ['as' => 'edit', 'uses' => 'TemplateController@edit']);
        Route::match(['put', 'patch'], '/{id}', ['as' => 'update', 'uses' => 'TemplateController@update']);
        Route::get('/{id}/delete', ['as' => 'delete', 'uses' => 'TemplateController@delete']);
    });

});


Route::group([
    'namespace' => 'Collaborator',
    'prefix' => 'collaborator',
    'as' => 'Collaborator::',
    'middleware' => 'auth'
], function () {
    Route::group([
        'prefix' => 'product_categories',
        'as' => 'product_category@',
    ], function () {
        Route::get('/', ['as' => 'index', 'uses' => 'ProductCategoryController@index']);
        Route::get('/add', ['as' => 'add', 'uses' => 'ProductCategoryController@add']);
        Route::post('/store', ['as' => 'store', 'uses' => 'ProductCategoryController@store']);
        Route::get('/{id}/edit', ['as' => 'edit', 'uses' => 'ProductCategoryController@edit']);
        Route::match(['put', 'patch'], '/{id}', ['as' => 'update', 'uses' => 'ProductCategoryController@update']);
        Route::get('/{id}/delete', ['as' => 'delete', 'uses' => 'ProductCategoryController@delete']);
        Route::get('/root_cat', ['as' => 'root_cat', 'uses' => 'ProductCategoryController@getRootCategories']);
        Route::get('/{id}/sub_cat', ['as' => 'sub_cat', 'uses' => 'ProductCategoryController@getSubCategories']);

    });

    Route::group([
        'prefix' => 'collaborator_list',
        'as' => 'collaborator_list@',
    ], function () {
        Route::get('/', ['as' => 'index', 'uses' => 'CollaboratorController@index']);
        Route::get('/add', ['as' => 'add', 'uses' => 'CollaboratorController@add']);
        Route::post('/store', ['as' => 'store', 'uses' => 'CollaboratorController@store']);
        Route::get('/{id}/edit', ['as' => 'edit', 'uses' => 'CollaboratorController@edit']);
        Route::match(['put', 'patch'], '/{id}', ['as' => 'update', 'uses' => 'CollaboratorController@update']);
    });

    Route::group([
        'prefix' => 'collaborator_groups',
        'as' => 'collaborator_group@',
    ], function () {
        Route::get('/', ['as' => 'index', 'uses' => 'CollaboratorGroupController@index']);
        Route::get('/add', ['as' => 'add', 'uses' => 'CollaboratorGroupController@add']);
        Route::post('/store', ['as' => 'store', 'uses' => 'CollaboratorGroupController@store']);
        Route::get('/{id}/edit', ['as' => 'edit', 'uses' => 'CollaboratorGroupController@edit']);
        Route::match(['put', 'patch'], '/{id}', ['as' => 'update', 'uses' => 'CollaboratorGroupController@update']);
//        Route::get('/{id}/delete', ['as' => 'delete', 'uses' => 'CollaboratorGroupController@delete']);

    });

    Route::group([
        'prefix' => 'need_update_products',
        'as' => 'need_update_product@',
    ], function () {
        Route::get('/', ['as' => 'index', 'uses' => 'NeedUpdateProductController@index']);
        Route::get('/add', ['as' => 'add', 'uses' => 'NeedUpdateProductController@add']);
        Route::post('/store', ['as' => 'store', 'uses' => 'NeedUpdateProductController@store']);
        Route::get('/{id}/edit', ['as' => 'edit', 'uses' => 'NeedUpdateProductController@edit']);
        Route::match(['put', 'patch'], '/{id}', ['as' => 'update', 'uses' => 'NeedUpdateProductController@update']);
        Route::get('/{id}/delete', ['as' => 'delete', 'uses' => 'NeedUpdateProductController@delete']);
        Route::get('/{id}/showGtin', ['as' => 'showGtin', 'uses' => 'NeedUpdateProductController@showGtin']);
        Route::get('/{id}/hideGtin', ['as' => 'hideGtin', 'uses' => 'NeedUpdateProductController@hideGtin']);
    });

    Route::group([
        'prefix' => 'contributed_informations',
        'as' => 'contributed_information@',
    ], function () {
        Route::get('/', ['as' => 'index', 'uses' => 'Contributed_InformationController@index']);
        Route::get('/{id}/approve', ['as' => 'approve', 'uses' => 'Contributed_InformationController@approve']);
        Route::get('/{id}/disapprove', ['as' => 'disapprove', 'uses' => 'Contributed_InformationController@disapprove']);
        Route::get('/{id}/checkStatus', ['as' => 'checkStatus', 'uses' => 'Contributed_InformationController@checkStatus']);
    });

    Route::group([
        'prefix' => 'jobs',
        'as' => 'job@',
    ], function () {
        Route::get('/', ['as' => 'index', 'uses' => 'JobManagementController@index']);
        Route::get('/add', ['as' => 'add', 'uses' => 'JobManagementController@add']);
        Route::post('/store', ['as' => 'store', 'uses' => 'JobManagementController@store']);
    });

//    Route::group([
//        'prefix' => 'profitsTransfer',
//        'as' => 'profitsTransfer@',
//    ], function () {
//        Route::get('/cHJvZml0c1RyYW5zZmVyMTUwMTIwMTk=', ['as' => 'profitsTransfer', 'uses' => 'ProfitsTransferController@profitsTransfer']);
//    });
});

Route::group([
    'namespace' => 'Business',
    'prefix' => 'business_management',
    'as' => 'Business::',
    'middleware' => 'auth'
], function () {
    Route::group([
        'prefix' => 'businesses',
        'as' => 'businesses@',
    ], function () {
        Route::get('/', ['as' => 'index', 'uses' => 'BusinessController@index']);
        Route::get('/add', ['as' => 'add', 'uses' => 'BusinessController@add']);
        Route::get('/{id}/edit', ['as' => 'edit', 'uses' => 'BusinessController@edit']);
        Route::get('/{id}/syncLegacyData', ['as' => 'syncLegacyData', 'uses' => 'BusinessController@syncLegacyData']);
        Route::get('/{id}/openExpoStore', ['as' => 'openExpoStore', 'uses' => 'BusinessController@openExpoStore']);
        Route::match(['put', 'patch'], '/{id}', ['as' => 'update', 'uses' => 'BusinessController@update']);
        Route::post('/store', ['as' => 'store', 'uses' => 'BusinessController@store']);
        Route::get('/{id}/approve', ['as' => 'approve', 'uses' => 'BusinessController@approve']);
        Route::get('/count', ['as' => 'count', 'uses' => 'BusinessController@countBusiness']);
        Route::get('/{id}/disapprove', ['as' => 'disapprove', 'uses' => 'BusinessController@disapprove']);
        Route::get('/{id}/delete', ['as' => 'delete', 'uses' => 'BusinessController@delete']);
        Route::get('/{id}/restore', ['as' => 'restore', 'uses' => 'BusinessController@restore']);
        Route::get('/excelExport', ['as' => 'excelExport', 'uses' => 'BusinessController@excelExport']);
    });
    Route::group([
        'prefix' => 'plans',
        'as' => 'plan@',
    ], function () {
        Route::get('/', ['as' => 'index', 'uses' => 'PlanController@index']);
        Route::get('/add', ['as' => 'add', 'uses' => 'PlanController@add']);
        Route::get('/{id}/edit', ['as' => 'edit', 'uses' => 'PlanController@edit']);
        Route::match(['put', 'patch'], '/{id}', ['as' => 'update', 'uses' => 'PlanController@update']);
        Route::post('/store', ['as' => 'store', 'uses' => 'PlanController@store']);
        Route::get('/{id}/delete', ['as' => 'delete', 'uses' => 'PlanController@delete']);
    });
    Route::group([
        'prefix' => 'subscriptions',
        'as' => 'subscription@',
    ], function () {
        Route::get('/', ['as' => 'index', 'uses' => 'SubscriptionController@index']);
        Route::get('/add', ['as' => 'add', 'uses' => 'SubscriptionController@add']);
        Route::get('/{id}/edit', ['as' => 'edit', 'uses' => 'SubscriptionController@edit']);
        Route::get('/excelExport', ['as' => 'excelExport', 'uses' => 'SubscriptionController@excelExport']);
        Route::match(['put', 'patch'], '/{id}', ['as' => 'update', 'uses' => 'SubscriptionController@update']);
        Route::post('/store', ['as' => 'store', 'uses' => 'SubscriptionController@store']);
        Route::post('/upload', ['as' => 'upload', 'uses' => 'SubscriptionController@uploadFile']);
        Route::post('/updateContract', ['as' => 'updateContract', 'uses' => 'SubscriptionController@updateContract']);
        Route::post('/{id}/approve', ['as' => 'approve', 'uses' => 'SubscriptionController@approve']);
        Route::post('/{id}/disapprove', ['as' => 'disapprove', 'uses' => 'SubscriptionController@disapprove']);
        Route::post('/updateNote', ['as' => 'updateNote', 'uses' => 'SubscriptionController@updateNote']);
        Route::post('/updatePaymentStatus/{id}', ['as' => 'updatePaymentStatus', 'uses' => 'SubscriptionController@updatePaymentStatus']);
        Route::get('/confirmPaymentStatus/{id}', ['as' => 'confirmPaymentStatus', 'uses' => 'SubscriptionController@confirmPaymentStatus']);
        Route::post('/confirmPaymentStatusAfterExpired/{id}', ['as' => 'confirmPaymentStatusAfterExpired', 'uses' => 'SubscriptionController@confirmPaymentStatusAfterExpired']);
        Route::get('/notConfirmPaymentStatus/{id}', ['as' => 'notConfirmPaymentStatus', 'uses' => 'SubscriptionController@notConfirmPaymentStatus']);
        Route::post('/updateContractValue', ['as' => 'updateContractValue', 'uses' => 'SubscriptionController@updateContractValue']);
        Route::post('/updateExpireType', ['as' => 'updateExpireType', 'uses' => 'SubscriptionController@updateExpireType']);
        Route::post('/updateAssignedTo', ['as' => 'updateAssignedTo', 'uses' => 'SubscriptionController@updateAssignedTo']);
        Route::post('/updateStartTime', ['as' => 'updateStartTime', 'uses' => 'SubscriptionController@updateStartTime']);
        Route::post('/updateExpiresOn', ['as' => 'updateExpiresOn', 'uses' => 'SubscriptionController@updateExpiresOn']);

    });

    Route::group([
        'prefix' => 'glns',
        'as' => 'gln@'
    ], function () {
        Route::get('/', ['as' => 'index', 'uses' => 'GlnController@index']);
        Route::get('/add', ['as' => 'add', 'uses' => 'GlnController@add']);
        Route::get('/{id}/edit', ['as' => 'edit', 'uses' => 'GlnController@edit']);
        Route::match(['put', 'patch'], '/{id}', ['as' => 'update', 'uses' => 'GlnController@update']);
        Route::post('/store', ['as' => 'store', 'uses' => 'GlnController@store']);
        Route::get('/{id}/approve', ['as' => 'approve', 'uses' => 'GlnController@approve']);
        Route::post('/{id}/disapprove', ['as' => 'disapprove', 'uses' => 'GlnController@disapprove']);
        Route::post('/updateFile', ['as' => 'updateFile', 'uses' => 'GlnController@updateFile']);
        Route::post('/checkGLN', ['as' => 'checkGLN', 'uses' => 'GlnController@checkGLN']);
        Route::post('/updateNote', ['as' => 'updateNote', 'uses' => 'GlnController@updateNote']);
        Route::post('/checkBusinessCode', ['as' => 'checkBusinessCode', 'uses' => 'GlnController@checkBusinessCode']);
        Route::post('/getFilesById', ['as' => 'getFilesById', 'uses' => 'GlnController@getFilesById']);
        Route::post('/getGLNByGLNCode', ['as' => 'getGLNByGLNCode', 'uses' => 'GlnController@getGLNByGLNCode']);
    });

    Route::group([
        'prefix' => 'products',
        'as' => 'product@'
    ], function () {
        Route::get('/', ['as' => 'index', 'uses' => 'ProductController@index']);
        Route::get('/{id}/approve', ['as' => 'approve', 'uses' => 'ProductController@approve']);
        Route::get('/{id}/checkStatus', ['as' => 'checkStatus', 'uses' => 'ProductController@checkStatus']);
        Route::get('/{id}/distributors', ['as' => 'showDistributors', 'uses' => 'ProductController@showDistributors']);
        Route::get('/{id}/base_info', ['as' => 'showBaseInfo', 'uses' => 'ProductController@showBaseInfo']);
        Route::get('/{id}/getUploadCertificateImages', ['as' => 'getUploadCertificateImages', 'uses' => 'ProductController@getUploadCertificateImages']);
        Route::post('/postUploadCertificateImages', ['as' => 'postUploadCertificateImages', 'uses' => 'ProductController@postUploadCertificateImages']);
        Route::get('/{id}/disapprove', ['as' => 'disapprove', 'uses' => 'ProductController@disapprove']);
        Route::get('/{id}/disapproveInfo', ['as' => 'disapproveInfo', 'uses' => 'ProductController@disapproveInfo']);
        Route::post('/updateFile', ['as' => 'updateFile', 'uses' => 'ProductController@updateFile']);
        Route::post('/disapproveMultiProducts', ['as' => 'disapproveMultiProducts', 'uses' => 'ProductController@disapproveMultiProducts']);
        Route::post('/disapproveMultiProductsInfo', ['as' => 'disapproveMultiProductsInfo', 'uses' => 'ProductController@disapproveMultiProductsInfo']);
        Route::post('/disapproveAllProducts', ['as' => 'disapproveAllProducts', 'uses' => 'ProductController@disapproveAllProducts']);
        Route::get('/ajaxSearch', ['as' => 'ajaxSearch', 'uses' => 'ProductController@ajaxSearch']);
        Route::get('/excelExport', ['as' => 'excelExport', 'uses' => 'ProductController@excelExport']);
        Route::get('/{id}/syncDistributors', ['as' => 'syncDistributors', 'uses' => 'ProductController@syncDistributors']);
    });

    Route::group([
        'prefix' => 'business_products',
        'as' => 'business_product@'
    ], function () {
        Route::get('/', ['as' => 'index', 'uses' => 'BusinessProductController@index']);
        Route::post('/approveProductInfo', ['as' => 'approveProductInfo', 'uses' => 'BusinessProductController@approveProductInfo']);
        Route::post('/disapproveProductInfo', ['as' => 'disapproveProductInfo', 'uses' => 'BusinessProductController@disapproveProductInfo']);
        Route::post('/approveManagementRoleOfProduct', ['as' => 'approveManagementRoleOfProduct', 'uses' => 'BusinessProductController@approveManagementRoleOfProducts']);
        Route::post('/disapproveManagementRoleOfProducts', ['as' => 'disapproveManagementRoleOfProducts', 'uses' => 'BusinessProductController@disapproveManagementRoleOfProducts']);
        Route::post('/disapproveManagementRoleOfAllProductsOfBusiness', ['as' => 'disapproveManagementRoleOfAllProductsOfBusiness', 'uses' => 'BusinessProductController@disapproveManagementRoleOfAllProductsOfBusiness']);
        Route::get('/excelExport', ['as' => 'excelExport', 'uses' => 'BusinessProductController@excelExport']);
        Route::post('/uploadFiles', ['as' => 'uploadFiles', 'uses' => 'BusinessProductController@uploadFiles']);
        Route::post('/updateCertificates', ['as' => 'updateCertificates', 'uses' => 'BusinessProductController@updateCertificates']);
        Route::get('/setFullControlRole/{id}', ['as' => 'setFullControlRole', 'uses' => 'BusinessProductController@setFullControlRole']);
        Route::get('/setDistributorRole/{id}', ['as' => 'setDistributorRole', 'uses' => 'BusinessProductController@setDistributorRole']);
    });

    Route::group([
        'prefix' => 'productDistributors',
        'as' => 'productDistributor@'
    ], function () {
        Route::get('/', ['as' => 'index', 'uses' => 'ProductDistributorController@index']);
        Route::get('/add', ['as' => 'add', 'uses' => 'ProductDistributorController@add']);
        Route::post('/step1', ['as' => 'form_step1', 'uses' => 'ProductDistributorController@creation_step1']);
        Route::get('/step2', ['as' => 'form_step2', 'uses' => 'ProductDistributorController@creation_step2']);
        Route::post('/store', ['as' => 'store', 'uses' => 'ProductDistributorController@store']);
        Route::get('/{id}/approve', ['as' => 'approve', 'uses' => 'ProductDistributorController@approve']);
//        Route::post('/approveProductDistributors', ['as' => 'approveMultiProductDistributors', 'uses' => 'ProductDistributorController@approveMultiProductDistributors']);
        Route::get('/{id}/checkSync', ['as' => 'checkSync', 'uses' => 'ProductDistributorController@checkSync']);
        Route::post('/{id}/disapprove', ['as' => 'disapprove', 'uses' => 'ProductDistributorController@disapprove']);
        Route::get('/{id}/delete', ['as' => 'delete', 'uses' => 'ProductDistributorController@delete']);
        Route::post('/', ['as' => 'showProductAndDistributorByBusiness', 'uses' => 'ProductDistributorController@showProductAndDistributorByBusiness']);
        Route::post('/updateFiles', ['as' => 'updateFiles', 'uses' => 'ProductDistributorController@updateFiles']);
    });
    Route::group([
        'prefix' => 'distributors',
        'as' => 'distributor@',
    ], function () {
        Route::get('/', ['as' => 'index', 'uses' => 'DistributorController@index']);
        Route::get('/add', ['as' => 'add', 'uses' => 'DistributorController@add']);
        Route::get('/{id}/edit', ['as' => 'edit', 'uses' => 'DistributorController@edit']);
        Route::match(['put', 'patch'], '/{id}', ['as' => 'update', 'uses' => 'DistributorController@update']);
        Route::post('/store', ['as' => 'store', 'uses' => 'DistributorController@store']);
        Route::get('/{id}/delete', ['as' => 'delete', 'uses' => 'DistributorController@delete']);
        Route::get('/{id}/approve', ['as' => 'approve', 'uses' => 'DistributorController@approve']);
        Route::get('/ajaxSearch', ['as' => 'ajaxSearch', 'uses' => 'DistributorController@ajaxSearch']);
    });
    Route::group([
        'prefix' => 'productRequests',
        'as' => 'productRequest@'
    ], function () {
        Route::get('/', ['as' => 'index', 'uses' => 'ManageProductRequestController@index']);
        Route::post('/approve', ['as' => 'approve', 'uses' => 'ManageProductRequestController@approve']);
        Route::get('/{id}/checkStatus', ['as' => 'checkStatus', 'uses' => 'ManageProductRequestController@checkStatus']);
        Route::get('/{id}/detail', ['as' => 'detail', 'uses' => 'ProductController@detail']);
        Route::get('/{id}/disapprove', ['as' => 'disapprove', 'uses' => 'ManageProductRequestController@disapprove']);
        Route::post('/updateFile', ['as' => 'updateFile', 'uses' => 'ManageProductRequestController@updateFile']);
    });

    Route::group([
        'prefix' => 'certificates',
        'as' => 'certificate@',
    ], function () {
        Route::get('/', ['as' => 'index', 'uses' => 'CertificateController@index']);
        Route::get('/add', ['as' => 'add', 'uses' => 'CertificateController@add']);
        Route::get('/{id}/edit', ['as' => 'edit', 'uses' => 'CertificateController@edit']);
        Route::post('/store', ['as' => 'store', 'uses' => 'CertificateController@store']);
        Route::match(['put', 'patch'], '/{id}', ['as' => 'update', 'uses' => 'CertificateController@update']);
        Route::get('/{id}/delete', ['as' => 'delete', 'uses' => 'CertificateController@delete']);
        Route::post('/certificatesByBusinessId', ['as' => 'certificatesByBusinessId', 'uses' => 'CertificateController@getAllCertificatesByBusinessId']);
        Route::post('/checkUniqueName', ['as' => 'checkUniqueName', 'uses' => 'CertificateController@checkUniqueName']);
        Route::get('/{id}/approve', ['as' => 'approve', 'uses' => 'CertificateController@approve']);
        Route::get('/{id}/disapprove', ['as' => 'disapprove', 'uses' => 'CertificateController@disapprove']);
    });

    Route::group([
        'prefix' => 'holidays',
        'as' => 'holiday@',
    ], function () {
        Route::get('/', ['as' => 'index', 'uses' => 'HolidayController@index']);
        Route::get('/add', ['as' => 'add', 'uses' => 'HolidayController@add']);
        Route::get('/{id}/edit', ['as' => 'edit', 'uses' => 'HolidayController@edit']);
        Route::match(['put', 'patch'], '/{id}', ['as' => 'update', 'uses' => 'HolidayController@update']);
        Route::post('/store', ['as' => 'store', 'uses' => 'HolidayController@store']);
        Route::get('/{id}/delete', ['as' => 'delete', 'uses' => 'HolidayController@delete']);
    });

    Route::post('upload', ['as' => 'upload', 'uses' => 'UploadController@getUploadUrl']);
});
Route::get('/home', 'HomeController@index')->name('home');

Route::group([
    'namespace' => 'User_Management',
    'prefix' => 'user_management',
    'as' => 'User_Management::',
    'middleware' => 'auth'
], function () {
    Route::group([
        'prefix' => 'users',
        'as' => 'user@',
    ], function () {
        Route::get('/', ['as' => 'index', 'uses' => 'UserController@index']);
        Route::get('/add', ['as' => 'add', 'uses' => 'UserController@add']);
        Route::post('/store', ['as' => 'store', 'uses' => 'UserController@store']);
        Route::get('/{id}/edit', ['as' => 'edit', 'uses' => 'UserController@edit']);
        Route::match(['put', 'patch'], '/{id}', ['as' => 'update', 'uses' => 'UserController@update']);
        Route::get('/{id}/delete', ['as' => 'delete', 'uses' => 'UserController@delete']);


    });
    Route::group([
        'prefix' => 'roles',
        'as' => 'role@',
    ], function () {
        Route::get('/', ['as' => 'index', 'uses' => 'RoleController@index']);
        Route::get('/add', ['as' => 'add', 'uses' => 'RoleController@add']);
        Route::get('/{id}/edit', ['as' => 'edit', 'uses' => 'RoleController@edit']);
        Route::match(['put', 'patch'], '/{id}', ['as' => 'update', 'uses' => 'RoleController@update']);
        Route::post('/store', ['as' => 'store', 'uses' => 'RoleController@store']);
        Route::get('/{id}/delete', ['as' => 'delete', 'uses' => 'RoleController@delete']);

    });

    Route::group([
        'prefix' => 'permissions',
        'as' => 'permission@',
    ], function () {
        Route::get('/', ['as' => 'index', 'uses' => 'PermissionController@index']);
        Route::get('/add', ['as' => 'add', 'uses' => 'PermissionController@add']);
        Route::get('/{id}/edit', ['as' => 'edit', 'uses' => 'PermissionController@edit']);
        Route::match(['put', 'patch'], '/{id}', ['as' => 'update', 'uses' => 'PermissionController@update']);
        Route::post('/store', ['as' => 'store', 'uses' => 'PermissionController@store']);
    });
});

Route::post('/business_management/productDistributors/approveProductDistributors', ['as' => 'approveMultiProductDistributors', 'uses' => 'Business\ProductDistributorController@approveMultiProductDistributors']);



Route::get('/{any}', ['middleware' => 'auth', 'uses' => function () {
    $roles = auth()->user()->getRoleNames();

    if (count($roles) === 1 and $roles[0] === 'collaborator') {
        return redirect(config('app.url') . '/app/contribute/new');
    }

    return view('app');
}])->where('any', '.*');

