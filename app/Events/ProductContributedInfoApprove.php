<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ProductContributedInfoApprove
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $contributed_info;
    public $gtin;
    public $contributed_info_id;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($contributed_info, $gtin, $contributed_info_id)
    {
        $this->contributed_info = $contributed_info;
        $this->gtin = $gtin;
        $this->contributed_info_id = $contributed_info_id;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
