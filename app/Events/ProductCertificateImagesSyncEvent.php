<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ProductCertificateImagesSyncEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $certificate_images,$product_icheck_id;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($certificate_images,$product_icheck_id)
    {
        $this->certificate_images = $certificate_images;
        $this->product_icheck_id = $product_icheck_id;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
