<?php

namespace App\Events\Business;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class DisapproveManagementRoleOfProductsEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $product_ids;
    public $reason;
    public $user_id;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($product_ids, $reason, $user_id)
    {
        $this->product_ids = $product_ids;
        $this->reason = $reason;
        $this->user_id = $user_id;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
