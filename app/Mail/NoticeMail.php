<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Qrcode\Account;

class NoticeMail extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * The account instance.
     *
     * @var Account
     */
    protected $account;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Account $account)
    {
        //
        $this->account = $account;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->view('qrcode.company.sendmail')
            ->subject("iCheck - Thông báo duyệt tài khoản iCheck QR code")
            ->with([
                'accountName' => $this->account->username,
            ]);
    }
}
