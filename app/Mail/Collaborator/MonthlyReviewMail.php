<?php

namespace App\Mail\Collaborator;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MonthlyReviewMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels, InteractsWithQueue;
    public $tries = 5;

    protected $contributed_total;
    protected $profits_total;
    protected $reviewMonth;
    protected $reviewYear;
    protected $collaborator_name;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($contributed_total, $profits_total, $reviewMonth, $reviewYear, $collaborator_name)
    {
        //
        $this->contributed_total = $contributed_total;
        $this->profits_total = $profits_total;
        $this->reviewMonth = $reviewMonth;
        $this->reviewYear = $reviewYear;
        $this->collaborator_name = $collaborator_name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $sender_mail = config('mailOption.collaborator_mail');
        $sender_name = config('mailOption.collaborator_mail_name');
        return $this->from("$sender_mail", "$sender_name")
            ->view('collaborator.email.sendmail')
            ->subject("Thông tin đối soát cộng tác viên iCheck")
            ->with([
                'contributed_total' => $this->contributed_total,
                'profits_total' => $this->profits_total,
                'reviewMonth' => $this->reviewMonth,
                'reviewYear' => $this->reviewYear,
                'collaborator_name' => $this->collaborator_name,
            ]);
    }

    public function failed($exception)
    {
        var_dump($exception->getMessage());
    }
}
