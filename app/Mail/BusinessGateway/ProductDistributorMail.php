<?php

namespace App\Mail\BusinessGateway;

use App\Models\Business\Business;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ProductDistributorMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels, InteractsWithQueue;
    public $tries = 5;

    protected $business;
    protected $extra_info;
    protected $note;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Business $business, $extra_info, $note)
    {
        //
        $this->business = $business;
        $this->extra_info = $extra_info;
        $this->note = $note;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $sender_mail = config('mailOption.gateway_mail');
        $name = config('mailOption.gateway_mail_name');
        return $this->from("$sender_mail","$name")
            ->view('business.email.productDistributor')
            ->subject("iCheck - Hệ thống quản lý sản phẩm")
            ->with([
                'business' => $this->business,
                'extra_info' => $this->extra_info,
                'note' => $this->note
            ]);
    }
}
