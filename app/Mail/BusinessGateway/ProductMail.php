<?php

namespace App\Mail\BusinessGateway;

use App\Models\Business\Business;
use App\Models\Business\Product;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ProductMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels, InteractsWithQueue;
    public $tries = 5;

    protected $business;
    protected $product;
    protected $note;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Business $business, $product, $note)
    {
        //
        $this->business = $business;
        $this->product = $product;
        $this->note = $note;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $sender_mail = config('mailOption.gateway_mail');
        return $this->from("$sender_mail")
            ->view('business.email.product')
            ->subject("iCheck - Hệ thống quản lý sản phẩm")
            ->with([
                'business' => $this->business,
                'product' => $this->product,
                'note' => $this->note
            ]);
    }
}
