<?php

namespace App\Mail\BusinessGateway;

use App\Models\Business\Business;
use App\Models\Business\Gln;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class GlnMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels, InteractsWithQueue;
    public $tries = 5;

    protected $business;
    protected $gln;
    protected $note;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Business $business, Gln $gln, $note)
    {
        //
        $this->business = $business;
        $this->gln = $gln;
        $this->note = $note;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $sender_mail = config('mailOption.gateway_mail');
        return $this->from("$sender_mail")
            ->view('business.email.gln')
            ->subject("iCheck - Hệ thống quản lý sản phẩm")
            ->with([
                'business' => $this->business,
                'gln' => $this->gln,
                'note' => $this->note
            ]);
    }
}
