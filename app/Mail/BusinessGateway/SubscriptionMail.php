<?php

namespace App\Mail\BusinessGateway;

use App\Models\Business\Business;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SubscriptionMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels, InteractsWithQueue;
    public $tries = 5;

    protected $business;
    protected $content;
    protected $note;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Business $business, $content, $note)
    {
        //
        $this->business = $business;
        $this->content = $content;
        $this->note = $note;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $sender_mail = config('mailOption.gateway_mail');
        return $this->from("$sender_mail")
            ->view('business.email.subscription')
            ->subject("iCheck - Hệ thống quản lý sản phẩm")
            ->with([
                'business' => $this->business,
                'content' => $this->content,
                'note' => $this->note
            ]);
    }
}
