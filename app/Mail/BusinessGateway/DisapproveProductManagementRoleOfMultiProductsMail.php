<?php

namespace App\Mail\BusinessGateway;

use App\Models\Business\Business;
use App\Models\Business\Product;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class DisapproveProductManagementRoleOfMultiProductsMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels, InteractsWithQueue;
    public $tries = 5;

    protected $business_name;
    protected $product_data;
    protected $note;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($business_name, $product_data, $note)
    {
        //
        $this->business_name = $business_name;
        $this->product_data = $product_data;
        $this->note = $note;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $sender_mail = config('mailOption.gateway_mail');
        return $this->from("$sender_mail")
            ->view('business.email.disapproveProductManagementRoleOfMultiProducts')
            ->subject("iCheck - Hệ thống quản lý sản phẩm")
            ->with([
                'business_name' => $this->business_name,
                'product_data' => $this->product_data,
                'note' => $this->note
            ]);
    }
}
