<?php

namespace App\Services\Flysystem;

use Google\Cloud\Storage\StorageClient;
use Google\Cloud\Storage\Acl;
use Google\Cloud\Storage\Bucket;
use Google\Cloud\Core\Exception\NotFoundException;
use League\Flysystem\Adapter\AbstractAdapter;
use League\Flysystem\AdapterInterface;
use League\Flysystem\Config;
use GuzzleHttp\Psr7\StreamWrapper;

class GoogleCloudStorageAdapter extends AbstractAdapter
{
    /**
     * @const STORAGE_API_URI_DEFAULT
     */
    const STORAGE_API_URI = 'https://storage.googleapis.com';

    /**
     * @var \Google\Cloud\Storage\StorageClient
     */
    protected $storageClient;

    /**
     * @var \Google\Cloud\Storage\Bucket
     */
    protected $bucket;

    public function __construct(StorageClient $storageClient, Bucket $bucket)
    {
        $this->storageClient = $storageClient;
        $this->bucket = $bucket;
    }

    public function getStorageClient()
    {
        return $this->storageClient;
    }

    public function getBucket()
    {
        return $this->bucket;
    }

    public function setBucket(Bucket $bucket)
    {
        $this->bucket = $bucket;
    }

    /**
     * {@inheritdoc}
     */
    public function has($path)
    {
        return $this->getObject($path)->exists();
    }

    /**
     * {@inheritdoc}
     */
    public function read($path)
    {
        $object = $this->getObject($path);
        $data = [];
        $data['contents'] = $object->downloadAsString();

        return $data;
    }

    /**
     * {@inheritdoc}
     */
    public function readStream($path)
    {
        $object = $this->getObject($path);
        $data = [];
        $data['stream'] = StreamWrapper::getResource($object->downloadAsStream());

        return $data;
    }

    /**
     * {@inheritdoc}
     */
    public function listContents($directory = '', $recursive = false)
    {
        $objects = $this->bucket->objects(['prefix' => $directory]);
    }

    /**
     * {@inheritdoc}
     */
    public function getMetadata($path)
    {
        return $object = $this->getObject($path)->info();
    }

    /**
     * {@inheritdoc}
     */
    public function getSize($path)
    {
        return $this->getMetadata($path);
    }

    /**
     * {@inheritdoc}
     */
    public function getMimetype($path)
    {
        $meta = $this->getMetadata($path);
        $meta['mimetype'] = $meta['contentType'];

        return $meta;
    }

    /**
     * {@inheritdoc}
     */
    public function getTimestamp($path)
    {
        return $this->getMetadata($path);
    }

    /**
     * {@inheritdoc}
     */
    public function getVisibility($path)
    {

    }

    /**
     * {@inheritdoc}
     */
    public function write($path, $contents, Config $config)
    {
        return $this->upload($path, $contents, $config);
    }

    /**
     * {@inheritdoc}
     */
    public function writeStream($path, $contents, Config $config)
    {
        return $this->upload($path, $contents, $config);
    }

    /**
     * {@inheritdoc}
     */
    public function update($path, $contents, Config $config)
    {
        return $this->upload($path, $contents, $config);
    }

    /**
     * {@inheritdoc}
     */
    public function updateStream($path, $contents, Config $config)
    {
        return $this->upload($path, $contents, $config);
    }

    /**
     * {@inheritdoc}
     */
    public function rename($path, $newpath)
    {
        return $this->getObject($path)->rename($newpath);
    }

    /**
     * {@inheritdoc}
     */
    public function copy($path, $newpath)
    {
        return $this->getObject($path)->copy($newpath);
    }

    /**
     * {@inheritdoc}
     */
    public function delete($path)
    {
        $this->getObject($path)->delete();

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteDir($path)
    {
        return $this->delete($path);
    }

    /**
     * {@inheritdoc}
     */
    public function createDir($dirname, Config $config)
    {

    }

    /**
     * {@inheritdoc}
     */
    public function setVisibility($path, $visibility)
    {
        $object = $this->getObject($path);

        if ($visibility === AdapterInterface::VISIBILITY_PUBLIC) {
            $object->acl()->add('allUsers', Acl::ROLE_READER);
        } elseif ($visibility === AdapterInterface::VISIBILITY_PRIVATE) {
            $object->acl()->delete('allUsers');
        }

        return compact('path', 'visibility');
    }

    public function getUrl($path)
    {
        $objectPieces = explode('/', $path);
        array_walk($objectPieces, function (&$piece) {
            $piece = rawurlencode($piece);
        });
        $objectName = implode('/', $objectPieces);

        return sprintf('%s/%s/%s', self::STORAGE_API_URI, $this->bucket->name(), $objectName);
    }

    protected function upload($path, $contents, Config $config)
    {
        $options = [];
        $options['name'] = $path;
        $object = $this->bucket->upload($contents, $options);

        return $object;
    }

    protected function getObject($name)
    {
        return $this->bucket->object($name);
    }
}
