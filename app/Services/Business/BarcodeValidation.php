<?php

namespace App\Services\Business;

class BarcodeValidation
{
    public function calculateCheckDigit($barcode)
    {
        $barcode = (string)$barcode;

        if (!is_numeric($barcode)) {
            return null;
        }

        $length = strlen($barcode);

        if (!in_array($length, [7, 11, 12, 13, 16, 17])) {
            return null;
        }

        $num = $length - 1;
        $isMultiplyBy3 = true;
        $sum = 0;

        for ($i = $num; $i >= 0; $i--) {
            $sum += (int)$barcode[$i] * ($isMultiplyBy3 ? 3 : 1);
            $isMultiplyBy3 = !$isMultiplyBy3;
        }

        $nextMultipleOfTen = ceil($sum / 10) * 10;

        return (int)$nextMultipleOfTen - $sum;
    }

    public function validateBarcode($barcode)
    {
        $barcode = (string)$barcode;
        $barcode = trim($barcode);
        if (strlen($barcode) === 0) {
            return false;
        }

        return ((int)$barcode[-1]) === $this->calculateCheckDigit(substr($barcode, 0, -1));
    }

}
