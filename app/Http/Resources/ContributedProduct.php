<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\ContributedProduct as Model;

class ContributedProduct extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $attachments = [];

        foreach ($this->attachments as $attachment) {
            $attachments[] = $this->getLegacyImageUrl($attachment, 'original');
        }

        return [
            'id'                 => $this->id,
            'gtin'               => $this->gtin_code,
            'isExistedProduct'   => true,
            'name'               => $this->name,
            'currentName'        => $this->product ? $this->product->product_name : null,
            'price'              => $this->price,
            'currentPrice'       => $this->product ? $this->product->price_default : null,
            'thumbnail'          => $this->getLegacyImageUrl($this->image, 'thumb_large'),
            'currentThumbnail'   => $this->product ? $this->getLegacyImageUrl($this->product->image_default, 'thumb_large') : null,
            'image'              => $this->getLegacyImageUrl($this->image, 'original'),
            'currentImage'       => $this->product ? $this->getLegacyImageUrl($this->product->image_default, 'thumb_large') : null,
            'attachments'        => $attachments,
            'currentAttachments' => $attachments,
            'gln'                => $this->gln,
            'currentGLN'         => $this->product ? $this->product->vendor : null,
            'category1'          => $this->category1,
            'currentCategory1'   => $this->category1,
            'category2'          => $this->category2,
            'currentCategory2'   => $this->category2,
            'category3'          => $this->category3,
            'currentCategory3'   => $this->category3,
            'category4'          => $this->category4,
            'currentCategory4'   => $this->category4,
            'contributorId'      => $this->account_id,
            'description'        => $this->description,
            'currentDescription' => $this->product ? $this->product->information : null,
            'status'             => Model::$statusTexts[$this->status],
            'reward'             => $this->reward,
            'reviewedBy'         => $this->reviewed_by,
            'reviewedAt'         => $this->reviewed_at ? $this->reviewed_at->format('c') : null,
            'createdAt'          => $this->created_at->format('c'),
            'updatedAt'          => $this->updated_at->format('c'),
            'deletedAt'          => $this->deleted_at ? $this->deleted_at->format('c') : null,
        ];
    }

    protected function getLegacyImageUrl($key, $size = 'original')
    {
        if (!$key) {
            return null;
        }

        $sizes = ["original", "thumb_small", "thumb_medium", "thumb_large", "small", "medium", "large"];

        if (!in_array($size, $sizes)) {
            $size = 'original';
        }

        return 'http://ucontent.icheck.vn/' . $key . '_' . $size . '.jpg';
    }
}
