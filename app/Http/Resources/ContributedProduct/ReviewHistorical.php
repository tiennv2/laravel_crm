<?php

namespace App\Http\Resources\ContributedProduct;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\ContributedProduct;

class ReviewHistorical extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $attachments = [];

        foreach ($this->attachments as $attachment) {
            $attachments[] = $this->getLegacyImageUrl($attachment, 'original');
        }

        return [
            'id'                 => $this->id,
            'gtin'               => $this->gtin_code,
            'name'               => $this->name,
            'price'              => $this->price,
            'thumbnail'          => $this->getLegacyImageUrl($this->image, 'thumb_large'),
            'image'              => $this->getLegacyImageUrl($this->image, 'original'),
            'attachments'        => $attachments,
            'gln'                => $this->gln,
            'category1'          => $this->category1,
            'category2'          => $this->category2,
            'category3'          => $this->category3,
            'category4'          => $this->category4,
            'contributorId'      => $this->account_id,
            'description'        => $this->description,
            'status'             => ContributedProduct::$statusTexts[$this->status],
            'reward'             => $this->reward ? $this->reward->money : '@',
            'reviewedBy'         => $this->reviewed_by,
            'reviewedAt'         => $this->reviewed_at->format('c'),
            'createdAt'          => $this->created_at->format('c'),
            'updatedAt'          => $this->updated_at->format('c'),
            'deletedAt'          => $this->deleted_at ? $this->deleted_at->format('c') : null,
        ];
    }

    protected function getLegacyImageUrl($key, $size = 'original')
    {
        $sizes = ["original", "thumb_small", "thumb_medium", "thumb_large", "small", "medium", "large"];

        if (!in_array($size, $sizes)) {
            $size = 'original';
        }

        return 'http://ucontent.icheck.vn/' . $key . '_' . $size . '.jpg';
    }
}
