<?php

namespace App\Http\Resources\Product;

use Illuminate\Http\Resources\Json\JsonResource;

class Attribute extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id'    => $this->id,
            'title' => $this->title,
        ];

        if ($this->pivot) {
            $data['content'] = $this->pivot->content;
        }

        return $data;
    }
}
