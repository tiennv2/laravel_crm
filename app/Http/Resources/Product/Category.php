<?php

namespace App\Http\Resources\Product;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Database\Eloquent\Collection;

class Category extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $display = $request->input('display', 'tree');

        $traverse = function ($categories) use (&$traverse, $display) {
            $isTopLevel = false;

            if (!($categories instanceof Collection)) {
                $categories = collect([$categories]);
                $isTopLevel = true;
            }

            $arr = [];

            foreach ($categories as $category) {
                $c = [
                    'id'        => $category->id,
                    'name'      => $category->name,
                    'lft'       => $category->_lft,
                    'rgt'       => $category->_rgt,
                    'parentId'  => $category->parent_id,
                    'createdAt' => $this->created_at->format('c'),
                    'updatedAt' => $this->updated_at->format('c'),
                ];

                if ($display === 'flat' and $category->relationLoaded('ancestors')) {
                    $c['path'] = $category->path;
                }

                if ($display !== 'flat' and $category->relationLoaded('children')) {
                    $c['children'] = $traverse($category->children);
                }

                $arr[] = $c;
            }

            return $isTopLevel ? $arr[0] : $arr;
        };

        // return isset($this->children);
        return [
            'id'        => $this->id,
            'name'      => $this->name,
            'path'      => $this->path,
            'lft'       => $this->_lft,
            'rgt'       => $this->_rgt,
            'parentId'  => $this->parent_id,
            'children' => new CategoryCollection($this->whenLoaded('children')),
            'createdAt' => $this->created_at->format('c'),
            'updatedAt' => $this->updated_at->format('c'),
        ];
    }
}
