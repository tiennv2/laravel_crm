<?php

namespace App\Http\Resources\Product;

use Illuminate\Http\Resources\Json\JsonResource;

class Product extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'         => $this->id,
            'gtin'       => $this->gtin_code,
            'name'       => $this->product_name,
            'vendor'     => new Vendor($this->vendor),
            'price'      => $this->price_default,
            'attributes' => new AttributeCollection($this->whenLoaded('attributes')),
        ];
    }
}
