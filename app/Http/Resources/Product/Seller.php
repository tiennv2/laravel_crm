<?php

namespace App\Http\Resources\Product;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Misc\{City, District};

class Seller extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'         => $this->id,
            'name'       => $this->name,
            'address'    => $this->address,
            'district'   => new District($this->whenLoaded('district')),
            'city'       => new City($this->whenLoaded('city')),
            'location'   => ['longitude' => $this->long, 'latitude'  => $this->lat,],
            'factor'     => $this->factor,
            'phone'      => $this->phone,
            'email'      => $this->email,
            'website'    => $this->website,
            'isVerified' => $this->is_verify,
            'createdAt'  => $this->created_at->format('c'),
            'updatedAt'  => $this->updated_at->format('c'),
            'deletedAt'  => $this->deleted_at ? $this->deleted_at->format('c') : null,
        ];
    }
}
