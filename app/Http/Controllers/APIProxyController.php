<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request as Psr7Request;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\DB;
use App\Models\NotFoundProduct;

class APIProxyController extends Controller
{
    public function index(Request $request)
    {
        $url = $request->get('url');

        if (!$url) {
            abort(400);
        }

        $icheck_authorization = config('api.icheck-authorization');
        $client = new Client([
            'headers' => [
                'icheck-authorization' => $icheck_authorization
            ]
        ]);
        $ignoreHeaders = ['cookie', 'host', 'content-length'];
        $headers = collect($request->headers->all())->except($ignoreHeaders)->toArray();
        $proxyRequest = new Psr7Request($request->method(), $url, $headers, $request->getContent());

        try {
            $response = $client->send($proxyRequest);
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $response = $e->getResponse();

                return response((string) $response->getBody(), $response->getStatusCode(), $response->getHeaders());
            }

            return response($e->getMessage(), 400);
        }

        return response((string) $response->getBody(), $response->getStatusCode(), $response->getHeaders());
    }

    public function dhl(Request $request)
    {
        $notFoundProducts = DB::connection('mongodb')->collection('p_hit')->where("status", "Assignable")->limit(100)->get();
        $notFoundProducts2 = DB::connection('mongodb')->collection('p_hit')->limit(100)->get();

        var_dump($notFoundProducts2);
        return $notFoundProducts;
    }
}
