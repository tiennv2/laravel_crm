<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ContributedProduct;
use App\Http\Resources\ContributedProductCollection;
use App\Http\Resources\ContributedProduct\ReviewHistoricalCollection;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ContributedProductController extends Controller
{
    public function index2(Request $request)
    {
        $rowGroupCols = $request->input('rowGroupCols', []);
        $groupKeys = $request->input('groupKeys', []);
        $valueCols = $request->input('valueCols', []);
        $filterModel = $request->input('filterModel', []);
        $sortModel = $request->input('sortModel', []);

        $data = ContributedProduct::with('category1', 'category2', 'category3', 'category4', 'gln', 'reward');

        if (count($rowGroupCols) > count($groupKeys)) {
            $select = [];
            $rowGroupCol = $rowGroupCols[count($groupKeys)];
            $select[] = $rowGroupCol['field'];

            foreach ($valueCols as $valueCol) {
                $select[] = $valueCol;
            }

            $data = $data->select($select);

            $colsToGroupBy = [];
            $colsToGroupBy[] = $rowGroupCol['field'];

            foreach ($colsToGroupBy as $col) {
                $data = $data->groupBy($col);
            }
        } else {

        }

        $whereParts = [];

        if (count($groupKeys) > 0) {
            foreach ($groupKeys as $index => $key) {
                $whereParts[] = [$rowGroupCols[$index]['field'], '=', $key];
            }
        }

        // if ($filterModel) {
        //     foreach ($groupKeys as $key => $index) {
        //         $whereParts[] = [$rowGroupCols[$index]['field'], '=', $key];
        //     }
        // }

        foreach ($whereParts as $where) {
            $data = $data->where($where[0], $where[1], $where[2]);
        }

        if (count($sortModel) > 0) {
            foreach ($sortModel as $item) {
                $data = $data->orderBy($item['colId'], $item['sort']);
            }
        }

        $startRow = $request->input('startRow', 0);
        $endRow = $request->input('endRow', 100);
        $pageSize = $endRow - $startRow;
        // $data = $data->skip($startRow)
        //     ->take($pageSize + 1)
        // ;
        $data = new ContributedProductCollection($data->get());
        // $lastRow = null;
        // $rowCount = count($data);

        // if (count($data) > $pageSize) {
        //     $lastRow = null;
        // } else {
        //     $lastRow = $startRow + $rowCount;
        // }

        return $data;
    }

    public function index(Request $request)
    {
        // Query params
        $offset   = $request->input('offset', 0);
        $limit    = $request->input('limit', 50);
        $ids      = $request->input('ids', []);
        $statuses = $request->input('statuses', []);

        $data = ContributedProduct::with(
            'category1',
            'category2',
            'category3',
            'category4',
            'gln',
            'product'
        );

        if (is_array($ids) and count($ids) > 0) {
            $data = $data->whereIn('id', $ids);
        }

        if (is_array($statuses) and count($statuses) > 0) {
            $statuses = array_filter($statuses, function ($status) {
                 return in_array($status, ContributedProduct::$statusTexts);
            });

            if (count($statuses) > 0) {
                $statuses = array_map(function ($status) {
                     return array_search($status, ContributedProduct::$statusTexts);
                }, $statuses);
                $data = $data->whereIn('status', $statuses);
            }
        }

        $data = $data->skip($offset)->take($limit);

        return new ContributedProductCollection($data->get());
    }

    public function reviewHistorical(Request $request)
    {
        // Query params
        $before   = $request->input('before');
        $after    = $request->input('after');
        $limit    = $request->input('limit', 50);
        $statuses = $request->input('statuses', []);

        $data = ContributedProduct::with(
            'category1',
            'category2',
            'category3',
            'category4',
            'gln',
            'reward'
        );
        $data = $data->orderBy('reviewed_at', 'DESC');

        if ($before and $beforeId = base64_decode($before)) {
            $data = $data->where('reviewed_at', '<', $beforeId);
        } else if ($after and $afterId = base64_decode($after)) {
            $data = $data->where('reviewed_at', '>', $afterId);
        }

        $avalidableStatuses = array_except(ContributedProduct::$statusTexts, [0]);

        if (is_array($statuses) and count($statuses) > 0) {
            $statuses = array_filter($statuses, function ($status) use ($avalidableStatuses) {
                 return in_array($status, $avalidableStatuses);
            });

            if (count($statuses) > 0) {
                $statuses = array_map(function ($status) use ($avalidableStatuses) {
                     return array_search($status, $avalidableStatuses);
                }, $statuses);
                $data = $data->whereIn('status', $statuses);
            } else {
                $data = $data->whereIn('status', array_keys($avalidableStatuses));
            }
        } else {
            $data = $data->whereIn('status', array_keys($avalidableStatuses));
        }

        $data = $data->take($limit);
        $data = $data->get();
        $collection = new ReviewHistoricalCollection($data);
        $paging = ['cursors' => []];
        $paging['previous'] = '';
        $paging['cursors']['after'] = base64_encode($data->first()->reviewed_at->getTimestamp());

        if ($data->count() >= $limit) {
            $paging['next'] = '';
            $paging['cursors']['before'] = base64_encode($data->last()->reviewed_at->getTimestamp());
        }

        $collection->additional(['paging' => $paging]);

        return $collection;
    }

    public function approve(Request $request)
    {
        $user = $request->user();
        $now = Carbon::now();
        $ids = $request->input('ids');
        $actionAfterApproved = $request->input('actionAfterApproved');
        // Lấy tất cả mã vạch
        $barcodes = ContributedProduct::whereIn('id', $ids)->pluck('gtin_code');

        if ($actionAfterApproved) {
            if ($actionAfterApproved == ContributedProduct::ACTION_APPROVE_ALL) {
                $ids = ContributedProduct::whereIn('gtin_code', $barcodes)
                    ->where('status', ContributedProduct::STATUS_PENDING_REVIEW)
                    ->pluck('id')
                ;
            } elseif ($actionAfterApproved == ContributedProduct::ACTION_REJECT_ALL) {
                $willRejectIds = ContributedProduct::whereIn('gtin_code', $barcodes)
                    ->whereNotIn('id', $ids)
                    ->where('status', ContributedProduct::STATUS_PENDING_REVIEW)
                    ->pluck('id')
                ;
            }
        }

        ContributedProduct::whereIn('id', $ids)->update([
            'status'      => ContributedProduct::STATUS_SYNCING,
            'reviewed_by' => $user->icheck_id,
            'reviewed_at' => $now,
        ]);

        if (isset($willRejectIds)) {
            ContributedProduct::whereIn('id', $willRejectIds)->update([
                'status'      => ContributedProduct::STATUS_REJECTED,
                'reviewed_by' => $user->icheck_id,
                'reviewed_at' => $now,
            ]);
            $ids = $willRejectIds->merge($ids);
        }

        return ['success' => true, 'removeIds' => $ids];
    }

    public function reject(Request $request)
    {
        $user = $request->user();
        $now = Carbon::now();
        $ids = $request->input('ids');
        $actionAfterRejected = $request->input('actionAfterRejected');
        // Lấy tất cả mã vạch
        $barcodes = ContributedProduct::whereIn('id', $ids)->pluck('gtin_code');

        if ($actionAfterRejected) {
            if ($actionAfterRejected == ContributedProduct::ACTION_APPROVE_ALL) {
                $willApproveIds = ContributedProduct::whereIn('gtin_code', $barcodes)
                    ->whereNotIn('id', $ids)
                    ->where('status', ContributedProduct::STATUS_PENDING_REVIEW)
                    ->pluck('id')
                ;
            } elseif ($actionAfterRejected == ContributedProduct::ACTION_REJECT_ALL) {
                $ids = ContributedProduct::whereIn('gtin_code', $barcodes)
                    ->where('status', ContributedProduct::STATUS_PENDING_REVIEW)
                    ->pluck('id')
                ;
            }
        }

        ContributedProduct::whereIn('id', $ids)->update([
            'status'      => ContributedProduct::STATUS_REJECTED,
            'reviewed_by' => $user->icheck_id,
            'reviewed_at' => $now,
        ]);

        if (isset($willApproveIds)) {
            ContributedProduct::whereIn('id', $willApproveIds)->update([
                'status'      => ContributedProduct::STATUS_SYNCING,
                'reviewed_by' => $user->icheck_id,
                'reviewed_at' => $now,
            ]);
            $ids = $willApproveIds->merge($ids);
        }

        return ['success' => true, 'removeIds' => $ids];
    }

    public function count()
    {
        $data = ContributedProduct::select('status', DB::raw('COUNT(*) as total'))
            ->whereIn('status', [ContributedProduct::STATUS_PENDING_REVIEW, ContributedProduct::STATUS_APPROVED, ContributedProduct::STATUS_REJECTED])
            ->groupBy('status')
            ->get()
        ;
        $statusMap = [
            ContributedProduct::STATUS_PENDING_REVIEW => 'pendingReview',
            ContributedProduct::STATUS_APPROVED       => 'approved',
            ContributedProduct::STATUS_REJECTED       => 'rejected',
        ];
        $count = [];

        foreach ($data as $value) {
            $count[$statusMap[$value['status']]] = $value['total'];
        }

        return $count;
    }
}
