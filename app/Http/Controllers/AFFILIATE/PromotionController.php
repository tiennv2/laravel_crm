<?php

namespace App\Http\Controllers\AFFILIATE;

use App\Http\Controllers\Controller;
use App\API\Api_affiliate;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;

class PromotionController extends Controller
{
    private $api_affiliate;

    public function __construct()
    {
        $this->api_affiliate = new Api_affiliate();

    }

    public function index(Request $request)
    {
        if (auth()->user()->cannot('AFFILIATE-viewPromotion') && auth()->user()->cannot('AFFILIATE-addPromotion')) {
            abort(403);
        }
        try {
            $limit = $request->input('limit', 10);
            $page = $request->input('page', 1);
            $data = $this->api_affiliate->getListPromotions(['limit' => $limit, 'offset' => ($page - 1) * 10]);
            $promotions = $data['data'];
            $categories = $this->api_affiliate->getCategories();
            return view('affiliate.promotion.index', array('promotions' => $promotions, 'categories' => $categories, 'total_record' => $data['summary']['total']));
        } catch (RequestException $exception) {
            return view('errors.404');
        }

    }

    public function getStatistics()
    {
        if (auth()->user()->cannot('AFFILIATE-viewStatistics')) {
            abort(403);
        }
        try {
            $from = date('m/d/Y', strtotime("-1 days"));
            $to = date('m/d/Y', strtotime("-1 days"));
            return redirect()->route('AFFILIATE::promotion@yesterday_statistics', ['from' => $from, 'to' => $to]);
        } catch (RequestException $exception) {
            return view('errors.404');
        }

    }

    public function getStatisticsOfYesterday()
    {
        if (auth()->user()->cannot('AFFILIATE-viewStatistics')) {
            abort(403);
        }
        try {
            $time_start_1 = date('Y-m-d H:i:s', strtotime(("-1 days") . "00:00:00"));
            $time_end_1 = date('Y-m-d H:i:s', strtotime(("-1 days") . "23:59:59"));
            $time_start_2 = date('Y-m-d H:i:s', strtotime(("-2 days") . "00:00:00"));
            $time_end_2 = date('Y-m-d H:i:s', strtotime(("-2 days") . "23:59:59"));
            $data_1 = $this->api_affiliate->getStatistics(['time_start' => $time_start_1, 'time_end' => $time_end_1]);
            $data_2 = $this->api_affiliate->getStatistics(['time_start' => $time_start_2, 'time_end' => $time_end_2]);
            $yesterday_statistics = $data_1['data'];
            $before_yesterday_statistics = $data_2['data'];
            $increase = $yesterday_statistics['order_count'] - $before_yesterday_statistics['order_count'];

            return view('affiliate.promotion.statistic', array('statistics' => $yesterday_statistics, 'increase' => $increase));
        } catch (RequestException $exception) {
            return view('errors.404');
        }

    }

    public function statistic_filter(Request $request)
    {
        if (auth()->user()->cannot('AFFILIATE-viewStatistics')) {
            abort(403);
        }
        try {
            $time_start_1 = date('Y-m-d H:i:s', strtotime(("-1 days") . "00:00:00"));
            $time_end_1 = date('Y-m-d H:i:s', strtotime(("-1 days") . "23:59:59"));
            $time_start_2 = date('Y-m-d H:i:s', strtotime(("-2 days") . "00:00:00"));
            $time_end_2 = date('Y-m-d H:i:s', strtotime(("-2 days") . "23:59:59"));
            $data_1 = $this->api_affiliate->getStatistics(['time_start' => $time_start_1, 'time_end' => $time_end_1]);
            $data_2 = $this->api_affiliate->getStatistics(['time_start' => $time_start_2, 'time_end' => $time_end_2]);
            $yesterday_statistics = $data_1['data'];
            $before_yesterday_statistics = $data_2['data'];
            $increase = $yesterday_statistics['order_count'] - $before_yesterday_statistics['order_count'];

            $time_start = (date('Y-m-d H:i:s', strtotime($request->input('from'))));
            $time_end = (date('Y-m-d H:i:s', strtotime(($request->input('to')) . "23:59:59")));
            $data = $this->api_affiliate->getStatistics(['time_start' => $time_start, 'time_end' => $time_end]);
            $statistics = $data['data'];
            return view('affiliate.promotion.statistic', array('statistics' => $statistics, 'increase' => $increase));
        } catch (RequestException $exception) {
            return view('errors.404');
        }

    }

    public function add()
    {
        if (auth()->user()->cannot('AFFILIATE-addPromotion')) {
            abort(403);
        }
        try {
            $categories = $this->api_affiliate->getCategories();
            return view('affiliate.promotion.form', array('categories' => $categories));
        } catch (RequestException $exception) {
            return view('errors.404');
        }

    }

    public function store(Request $request)
    {
        if (auth()->user()->cannot('AFFILIATE-addPromotion')) {
            abort(403);
        }
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
            'banner' => 'required',
            'condition' => 'required',
            'time_start' => 'required|after:today',
            'time_end' => 'required|after:start_time',
            'nominate_count_max' => 'integer|min:1',
            'nominate_count_max_per_user' => 'integer|min:1',
            'categories' => 'required',
            'discount_min' => 'required|integer|between:0,100',
            'discount_max' => 'required|integer|between:0,100'
        ],
            [
                'name.required' => 'Tên chiến dịch không được để trống',
                'description.required' => 'Mô tả chung không được để trống',
                'banner.required' => 'Bạn chưa chọn banner cho chiến dịch',
                'condition.required' => 'Điều kiện tham gia chiến dịch không được để trống',
                'categories.required' => 'Bạn chưa chọn nhóm sản phẩm tham gia',
                'time_start.required' => 'Bạn chưa chọn thời gian bắt đầu chiến dịch',
                'time_end.required' => 'Bạn chưa chọn thời gian kết thúc chiến dịch',
                'time_start.after' => 'Thời gian bắt đầu chiến dịch phải sau ngày hiện tại',
                'time_end.after' => 'Thời gian kết thúc chiến dịch phải sau ngày bắt đầu chiến dịch',
                'nominate_count_max.integer' => 'Số lượng sản phẩm tối đa tham gia chiến dịch phải là một số nguyên',
                'nominate_count_max.min' => 'Số lượng sản phẩm tối đa tham gia chiến dịch phải lớn hơn hoặc bằng 1',
                'nominate_count_max_per_user.integer' => 'Số lượng sản phẩm tối đa của cửa hàng tham gia vào chiến dịch phải là một số nguyên',
                'nominate_count_max_per_user.min' => 'Số lượng sản phẩm tối đa của cửa hàng tham gia vào chiến dịch phải lớn hơn hoặc bằng 1',
                'discount_min.required' => 'Bạn chưa điền thông tin chiết khấu nhỏ nhất',
                'discount_min.between' => 'Chiết khấu nhỏ nhất phải trong khoảng  từ 0% đến 100%',
                'discount_min.integer' => 'Chiết khấu nhỏ nhất phải là một số nguyên',
                'discount_max.required' => 'Bạn chưa điền thông tin chiết khấu lớn nhất',
                'discount_max.between' => 'Chiết khấu cao nhất phải trong khoảng  từ 0% đến 100%',
                'discount_max.integer' => 'Chiết khấu cao nhất phải là một số nguyên'
            ]);
        $data = $request->all();
        $path = "upload";
        $file = $request->file('banner');
        $filename = $file->getClientOriginalName();
        $file->move($path, $filename);
        $client = new Client();
        try {
            $res = $client->request(
                'POST',
                'http://upload.icheck.vn/v1/images?uploadType=multipart&responseType=json',
                [
                    'multipart' => [
                        ['name' => 'file', 'contents' => fopen("upload/$filename", 'r')]
                    ],
                ]
            );
            $res = json_decode((string)$res->getBody());
        } catch (RequestException $e) {
            return $e->getResponse()->getBody();
        }
        $banner = $res->prefix;
        $replace1 = array('time_start' => (date('Y-m-d H:i:s', strtotime($data['time_start']))));
        $replace2 = array('time_end' => (date('Y-m-d H:i:s', strtotime($data['time_end'] . "23:59:59"))));
        $replace3 = array('banner' => $banner);
        $promotion = array_replace($data, $replace1, $replace2, $replace3);
        if (isset($promotion['nominate_count_max'])) {
            settype($promotion['nominate_count_max'], "integer");
        }
        if (isset($promotion['nominate_count_max_per_user'])) {
            settype($promotion['nominate_count_max_per_user'], "integer");
        }
        settype($promotion['discount_min'], "integer");
        settype($promotion['discount_max'], "integer");
        unset($promotion["_token"]);
        unlink($path . "/" . $filename);
        try {
            $this->api_affiliate->postPromotion($promotion);
            return redirect()->route('AFFILIATE::promotion@index')
                ->with('success', 'Đã thêm mới chương trình thành công');
        } catch (RequestException $exception) {
            return view('errors.404');
        }

    }

    public function edit($id)
    {
        if (auth()->user()->cannot('AFFILIATE-updatePromotion')) {
            abort(403);
        }
        try {
            $categories = $this->api_affiliate->getCategories();
            $promotion = $this->api_affiliate->getPromotion($id);
            return view('affiliate.promotion.form', array('promotion' => $promotion, 'categories' => $categories));
        } catch (RequestException $exception) {
            return view('errors.404');
        }

    }

    public function nominate($id, Request $request)
    {
        if (auth()->user()->cannot('AFFILIATE-viewNominateProductOfPromotion')) {
            abort(403);
        }
        try {
            $limit = $request->input('limit', 10);
            $page = $request->input('page', 1);
            $data = $this->api_affiliate->getListProducts($id, ['limit' => $limit, 'offset' => ($page - 1) * 10]);
            $products = $data['data'];
            return view('affiliate.promotion.product', array('products' => $products, 'total_record' => $data['summary']['total']));
        } catch (RequestException $exception) {
            return view('errors.404');
        }

    }

    public function update($id, Request $request)
    {
        if (auth()->user()->cannot('AFFILIATE-updatePromotion')) {
            abort(403);
        }
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
            'condition' => 'required',
            'time_start' => 'required|date|after:today',
            'time_end' => 'required|date|after:time_start',
            'nominate_count_max' => 'integer|min:1|nullable',
            'nominate_count_max_per_user' => 'integer|min:1|nullable',
            'categories' => 'array',
            'discount_min' => 'integer|between:0,100|nullable',
            'discount_max' => 'integer|between:0,100|nullable'
        ], [
            'name.required' => 'Tên chiến dịch không được để trống',
            'description.required' => 'Mô tả chung không được để trống',
            'condition.required' => 'Điều kiện tham gia chiến dịch không được để trống',
            'time_start.required' => 'Bạn chưa chọn thời gian bắt đầu chiến dịch',
            'time_end.required' => 'Bạn chưa chọn thời gian kết thúc chiến dịch',
            'time_start.after' => 'Thời gian bắt đầu chiến dịch phải sau ngày hiện tại',
            'time_end.after' => 'Thời gian kết thúc chiến dịch phải sau ngày bắt đầu chiến dịch',
            'nominate_count_max.integer' => 'Số lượng sản phẩm tối đa tham gia chiến dịch phải là một số nguyên',
            'nominate_count_max.min' => 'Số lượng sản phẩm tối đa tham gia chiến dịch phải lớn hơn hoặc bằng 1',
            'nominate_count_max_per_user.integer' => 'Số lượng sản phẩm tối đa của cửa hàng tham gia vào chiến dịch phải là một số nguyên',
            'nominate_count_max_per_user.min' => 'Số lượng sản phẩm tối đa của cửa hàng tham gia vào chiến dịch phải lớn hơn hoặc bằng 1',
            'discount_min.between' => 'Chiết khấu nhỏ nhất của sản phẩm tham gia phải trong khoảng  từ 0% đến 100%',
            'discount_min.integer' => 'Chiết khấu nhỏ nhất của sản phẩm tham gia phải là một số nguyên',
            'discount_max.between' => 'Chiết khấu cao nhất của sản phẩm tham gia phải trong khoảng  từ 0% đến 100%',
            'discount_max.integer' => 'Chiết khấu cao nhất của sản phẩm tham gia phải là một số nguyên'
        ]);
        $data = $request->all();
        unset($data['_token'], $data['_method']);
        if (isset($data['nominate_count_max'])) {
            settype($data['nominate_count_max'], "integer");
        }
        if (isset($data['nominate_count_max_per_user'])) {
            settype($data['nominate_count_max_per_user'], "integer");
        }
        settype($data['discount_min'], "integer");
        settype($data['discount_max'], "integer");
        $promotion = array_filter($data);
        //Upload banner
        $path = "upload";
        $banner = "";
        $filename = "";
        if ($request->hasFile('banner')) {
            $file = $request->file('banner');
            $filename = $file->getClientOriginalName();
            $file->move($path, $filename);
            $client = new Client();
            try {
                $res = $client->request(
                    'POST',
                    'http://upload.icheck.vn/v1/images?uploadType=multipart&responseType=json',
                    [
                        'multipart' => [
                            ['name' => 'file', 'contents' => fopen("upload/$filename", 'r')]
                        ],
                    ]
                );
                $res = json_decode((string)$res->getBody());
            } catch (RequestException $e) {
                return $e->getResponse()->getBody();
            }
            $banner = $res->prefix;
        }

        $replace1 = array('time_start' => (date('Y-m-d H:i:s', strtotime($data['time_start']))));
        $replace2 = array('time_end' => (date('Y-m-d H:i:s', strtotime($data['time_end'] . "23:59:59"))));
        $replace3 = null;
        if ($banner != "") {
            $replace3 = array('banner' => $banner);
            $promotion = array_replace($promotion, $replace1, $replace2, $replace3);
        } else {
            $promotion = array_replace($promotion, $replace1, $replace2);
        }
        if ($filename != "") {
            unlink($path . "/" . $filename);
        }
        try {
            $this->api_affiliate->updatePromotion($id, $promotion);
            return redirect()->route('AFFILIATE::promotion@index')
                ->with('success', 'Đã cập nhật chương trình thành công');
        } catch (RequestException $exception) {
            return view('errors.404');
        }


    }

    public function detail($id)
    {
        if (auth()->user()->cannot('AFFILIATE-viewDetailPromotion')) {
            abort(403);
        }
        try {
            $promotion = $this->api_affiliate->getPromotion($id);;
            $categories = $this->api_affiliate->getCategories();
            return view('affiliate.promotion.detail', array('promotion' => $promotion, 'categories' => $categories));
        } catch (RequestException $exception) {
            return view('errors.404');
        }
    }

    public function deactive($id)
    {
        if (auth()->user()->cannot('AFFILIATE-deactivePromotion')) {
            abort(403);
        }
        try {
            $this->api_affiliate->deactivePromotion($id);
            return redirect()->route('AFFILIATE::promotion@index')
                ->with('success', 'Đã ngừng chạy chương trình');
        } catch (RequestException $exception) {
            return view('errors.404');
        }


    }

    public function active($id)
    {
        if (auth()->user()->cannot('AFFILIATE-activePromotion')) {
            abort(403);
        }
        try {
            $this->api_affiliate->activePromotion($id);
            return redirect()->route('AFFILIATE::promotion@index')
                ->with('success', 'Đã kích hoạt chạy chương trình');
        } catch (RequestException $exception) {
            return view('errors.404');
        }
    }

    public function approve($id)
    {
        if (auth()->user()->cannot('AFFILIATE-approvePromotion')) {
            abort(403);
        }
        try {
            $this->api_affiliate->approvePromotion($id);
            return redirect()->route('AFFILIATE::promotion@index')
                ->with('success', 'Đã duyệt chương trình thành công');
        } catch (RequestException $exception) {
            return view('errors.404');
        }


    }

    public function delete($id)
    {
        if (auth()->user()->cannot('AFFILIATE-delPromotion')) {
            abort(403);
        }
        try {
            $this->api_affiliate->deletePromotion($id);
            return redirect()->route('AFFILIATE::promotion@index')
                ->with('success', 'Đã xóa chương trình thành công');
        } catch (RequestException $exception) {
            return view('errors.404');
        }

    }

    public function orderNominate($id, Request $request)
    {
        $request = $request->input('order');
        $this->api_affiliate->orderNominate($id, $request);
    }

}