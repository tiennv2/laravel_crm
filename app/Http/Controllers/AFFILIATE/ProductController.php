<?php

namespace App\Http\Controllers\AFFILIATE;
use App\Http\Controllers\Controller;
use App\API\Api_affiliate;
use GuzzleHttp\Exception\RequestException;

class ProductController extends Controller
{
    private $api_affiliate;

    public function __construct()
    {
        $this->api_affiliate = new Api_affiliate();

    }
    public function active($id)
    {
        if (auth()->user()->cannot('AFFILIATE-activeProduct')) {
            abort(403);
        }
        try{
            $this->api_affiliate->activeProduct($id);
            return redirect()->back()
                ->with('success', 'Đã duyệt sản phẩm thành công');
        }
        catch (RequestException $exception){
            return view('errors.404');
        }

    }

    public function deactive($id)
    {
        if (auth()->user()->cannot('AFFILIATE-deactiveProduct')) {
            abort(403);
        }
        try{
            $this->api_affiliate->deactiveProduct($id);
            return redirect()->back()
                ->with('success', 'Đã bỏ duyệt sản phẩm');
        }
        catch (RequestException $exception)
        {
            return view('errors.404');
        }

    }

}