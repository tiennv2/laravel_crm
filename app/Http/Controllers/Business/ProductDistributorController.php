<?php

namespace App\Http\Controllers\Business;

use App\Events\DistributorApproveEvent;
use App\Events\DistributorDisapproveEvent;
use App\Http\Controllers\Controller;
use App\Models\Business\Business;
use App\Models\Business\Distributor;
use App\Models\Business\IcheckDistributorTitle;
use App\Models\Business\Product;
use App\Models\Business\ProductDistributor;
use App\Models\Icheck_newCMS\ActivityLog;
use Google\Cloud\Storage\StorageClient;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\Storage;

class ProductDistributorController extends Controller
{

    public function index(Request $request, ProductDistributor $productDistributor)
    {
        if (auth()->user()->cannot('GATEWAY-viewProductDistributor') && auth()->user()->cannot('GATEWAY-addProductDistributor')) {
            abort(403);
        }
        try {
            $productDistributor = $productDistributor->newQuery();
            $query = [];
            $productDistributor->leftJoin('distributors', 'product_distributors.distributor_id', '=', 'distributors.id')
                ->leftJoin('products', 'product_distributors.product_id', '=', 'products.id')
                ->select('product_distributors.*', 'distributors.name as distributor_name', 'products.name as product_name', 'products.barcode as product_barcode', 'products.business_id as business_id');
            if ($request->input('product_name')) {
                $product_name = $request->input('product_name');
                $productDistributor->where('products.name', 'like', '%' . $product_name . '%');
                $query['product_name'] = $product_name;
            }
            if ($request->input('product_barcode')) {
                $product_barcode = $request->input('product_barcode');
                $productDistributor->where('products.barcode', 'like', '%' . $product_barcode . '%');
                $query['product_barcode'] = $product_barcode;
            }
            if ($request->input('business_id')) {
                $business_id = $request->input('business_id');
                $productDistributor->where('products.business_id', $business_id);
                $query['business_id'] = $business_id;
            }
            if ($request->input('distributor_name')) {
                $distributor_name = $request->input('distributor_name');
                $productDistributor->where('distributors.name', 'like', '%' . $distributor_name . '%');
                $query['distributor_name'] = $distributor_name;
            }
            if ($request->input('title_id')) {
                $title_id = $request->input('title_id');
                $title_id->where('title_id', $title_id);
                $query['title_id'] = $title_id;
            }
            if ($request->input('status') != null) {
                $status = $request->input('status');
                $productDistributor->where('product_distributors.status', $status);
                $query['status'] = $status;
            }

            if ($request->input('from')) {
                $from = $request->input('from');
                $productDistributor->where([['product_distributors.created_at', '>=', date('Y-m-d' . ' 00:00:00', strtotime($from))]]);
                $query['from'] = $from;
            }
            if ($request->input('to')) {
                $to = $request->input('to');
                $productDistributor->where([['product_distributors.created_at', '<=', date('Y-m-d' . ' 23:59:59', strtotime($to))]]);
                $query['to'] = $to;
            }
            $count_record_total = $productDistributor->count();
            $productDistributors = $productDistributor->orderBy('id', 'desc')->paginate(20)->appends($query);
            $count_record_in_page = $productDistributors->count();
            $waiting_productDistributor_count = ProductDistributor::where('status', '=', 0)->count();

            $distributor_titles = IcheckDistributorTitle::all()->toArray();
            $businesses = Business::where('deleted_at', '=', null)->get();
            foreach ($productDistributors as $productDistributor) {
                for ($i = 0; $i < count($distributor_titles); $i++) {
                    if ($productDistributor->title_id == $distributor_titles[$i]['id']) {
                        $productDistributor->title_name = $distributor_titles[$i]['title'];
                        continue;
                    }
                }
                foreach ($businesses as $business) {
                    if ($productDistributor->business_id == $business->id) {
                        $productDistributor->business_name = $business->name;
                    }
                }
                if ($productDistributor->files) {
                    $paths = json_decode($productDistributor->files);
                    $full_paths = [];
                    $baseUrl = sprintf('https://%s/%s', 'storage.googleapis.com', config('filesystems.disks.gcs_business.bucket'));
                    foreach ($paths as $path) {
                        $full_file_name = explode("/", $path);
                        $full_file_name = end($full_file_name);
                        $origin_file_name = explode("_", $full_file_name);
                        $origin_file_name = end($origin_file_name);
                        $url = sprintf('%s/%s', $baseUrl, $path);
                        array_push($full_paths, [$origin_file_name, $path, $url]);
                    }
                    $productDistributor->files = json_encode($full_paths);
                }
            }
            return view('business.product_distributor.index', ['productDistributors' => $productDistributors, 'businesses' => $businesses, 'count_record_total' => $count_record_total,
                'count_record_in_page' => $count_record_in_page, 'waiting_productDistributor_count' => $waiting_productDistributor_count]);
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

    public function add()
    {
        if (auth()->user()->cannot('GATEWAY-addProductDistributor')) {
            abort(403);
        }
        $businesses = Business::where('deleted_at', '=', null)->get();
        $distributor_titles = IcheckDistributorTitle::all();
//        dd($distributor_titles);exit();
        return view('business.product_distributor.form_step1', ["businesses" => $businesses, "distributor_titles" => $distributor_titles]);
    }

    public function creation_step1(Request $request)
    {
        if (auth()->user()->cannot('GATEWAY-addProductDistributor')) {
            abort(403);
        }
        try {
            $data = $request->except(["_token"]);
            $default_title_id = null;
            if ($request->input("title_id")) {
                $default_title_id = $request->input("title_id");
            }

//            if ($request->input("uploaded_files")) {
//                $certificate_files = $request->input("uploaded_files");
//                $full_paths = [];
//                $baseUrl = sprintf('https://%s/%s', 'storage.googleapis.com', config('filesystems.disks.gcs_business.bucket'));
//                foreach ($certificate_files as $certificate_file) {
//                    $url = sprintf('%s/%s', $baseUrl, $certificate_file);
//                    array_push($full_paths, [$certificate_file, $url]);
//                }
//                $certificate_files = json_encode($full_paths);
//
//            }

            $productDistributors = [];

            $barcodes = array_unique(explode("\r\n", trim($data["barcode"])));
            foreach ($barcodes as $key => $value) {
                if ($request->input("selected_business_id")) {
                    $selected_business_id = $request->input("selected_business_id");
                    $product = Product::where([['barcode', '=', $value], ['business_id', '=', $selected_business_id]])->first();
                } else {
                    $product = Product::where('barcode', '=', $value)->whereNotNull('business_id')->first();
                }
                if ($product) {
                    foreach ($data['distributor_id'] as $distributor_id) {
                        $distributor = Distributor::findOrFail($distributor_id);
                        array_push($productDistributors, ['product_id' => $product->id, 'product_name' => $product->name, 'product_barcode' => $product->barcode, 'distributor_id' => $distributor_id, 'distributor_name' => $distributor->name, 'default_title_id' => $default_title_id]);
                    }
                }
            }


//            foreach ($data['product_id'] as $product_id) {
//                $product = Product::findOrFail($product_id);
//                foreach ($data['distributor_id'] as $distributor_id) {
//                    $distributor = Distributor::findOrFail($distributor_id);
//                    array_push($productDistributors, ['product_id' => $product_id, 'product_name' => $product->name, 'product_barcode' => $product->barcode, 'distributor_id' => $distributor_id, 'distributor_name' => $distributor->name, 'default_title_id' => $default_title_id]);
//                }
//            }

            $distributor_titles = IcheckDistributorTitle::all();
//            session(["productDistributors" => $productDistributors, "distributor_titles" => $distributor_titles, "business_id" => $data['business_id']]);
            session(["productDistributors" => $productDistributors, "distributor_titles" => $distributor_titles]);
            return redirect()->action('Business\ProductDistributorController@creation_step2');
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

    public function creation_step2()
    {
        if (auth()->user()->cannot('GATEWAY-addProductDistributor')) {
            abort(403);
        }
        $productDistributors = session('productDistributors');
        $distributor_titles = session('distributor_titles');
//        $certificate_files = session('certificate_files');
//        $business_id = session('business_id');
//        dd($distributor_titles);exit();
        return view('business.product_distributor.form_step2', ["productDistributors" => $productDistributors, "distributor_titles" => $distributor_titles]);
    }

    public function store(Request $request)
    {
        if (auth()->user()->cannot('GATEWAY-addProductDistributor')) {
            abort(403);
        }
        $product_ids = $request->input("product_id");
        $distributor_ids = $request->input("distributor_id");
        $title_ids = $request->input("title_id");
//        dd($request->all());
//        $businessId = $request->input("business_id");
        if (count($product_ids)) {
            for ($i = 0; $i < count($product_ids); $i++) {
                $upload_index = "upload_files_" . "$product_ids[$i]" . "_" . "$distributor_ids[$i]";
                $files = null;
                if ($request->input($upload_index)) {
                    $files = $this->saveUploadedFiles($request->input($upload_index));
                }
                $productDistributor = ProductDistributor::updateOrCreate(['distributor_id' => $distributor_ids[$i], 'product_id' => $product_ids[$i], 'title_id' => $title_ids[$i]], ['status' => 1, 'files' => $files, 'is_visible' => 1]);
                $this->approve($productDistributor->id);
                //Activity_log
                ActivityLog::create([
                    "description" => "Create a new productDistributor",
                    "subject_id" => $productDistributor->id,
                    "subject_type" => "App\Models\Business\ProductDistributor",
                    "causer_id" => auth()->user()->id,
                    "causer_type" => "users"
                ]);
            }
        }
        return redirect()->route('Business::productDistributor@index')
            ->with('success', 'Đã thêm mới Nhà phân phối cho Sản phẩm thành công');
    }

    public function approve($id)
    {
        if (auth()->user()->cannot('GATEWAY-approveProductDistributor')) {
            abort(403);
        }
        $productDistributor = ProductDistributor::findOrFail($id);
        //Đồng bộ NPP <=> iCheck
        event(new DistributorApproveEvent($productDistributor));
        //Activity_log
        ActivityLog::create([
            "description" => "Approve a productDistributor",
            "subject_id" => $id,
            "subject_type" => "App\Models\Business\ProductDistributor",
            "causer_id" => auth()->user()->id,
            "causer_type" => "users"
        ]);
    }

    public function approveMultiProductDistributors(Request $request)
    {
        $ids = $request->input('ids');
        foreach ($ids as $id) {
            $productDistributor = ProductDistributor::findOrFail($id);
            event(new DistributorApproveEvent($productDistributor));
        }
    }

    public function disapprove($id, Request $request)
    {
        if (auth()->user()->cannot('GATEWAY-disapproveProductDistributor')) {
            abort(403);
        }

        $note = null;
        if ($request->input('note')) {
            $note = $request->input('note');
        }
        $productDistributor = ProductDistributor::findOrFail($id)->toArray();
        $productDistributor['is_deleted'] = false;
        $productDistributor['note'] = $note;
        //Đồng bộ NPP <=> iCheck
        event(new DistributorDisapproveEvent($productDistributor));
        //Activity_log
        ActivityLog::create([
            "description" => "Disapprove a productDistributor",
            "subject_id" => $id,
            "subject_type" => "App\Models\Business\ProductDistributor",
            "causer_id" => auth()->user()->id,
            "causer_type" => "users"
        ]);
        return redirect()->back()
            ->with('success', 'Đã bỏ duyệt Nhà phân phối thành công!');
    }

    public function checkSync($id)
    {
        $productDistributor = ProductDistributor::find($id);
        if ($productDistributor) {
            return $productDistributor->status;
        }
        return -1;
    }

    public function delete($id)
    {
        if (auth()->user()->cannot('GATEWAY-delProductDistributor')) {
            abort(403);
        }
        $productDistributor = ProductDistributor::findOrFail($id)->toArray();
        $productDistributor['is_deleted'] = true;
        //Đồng bộ NPP <=> iCheck
        event(new DistributorDisapproveEvent($productDistributor));
        //Activity_log
        ActivityLog::create([
            "description" => "Delete a productDistributor",
            "subject_id" => $id,
            "subject_type" => "App\Models\Business\ProductDistributor",
            "causer_id" => auth()->user()->id,
            "causer_type" => "users",
            "meta_data" => json_encode($productDistributor)
        ]);
    }

    public function showProductAndDistributorByBusiness(Request $request)
    {
        if ($request->ajax()) {
            $business_id = $request->business_id;
            $products = Product::where("business_id", $business_id)->get();;
            $distributors = Distributor::where([["business_id", $business_id]])->get();

            return response()->json(["products" => $products, "distributors" => $distributors]);
        }
    }

    public function saveUploadedFiles($files)
    {
        $uploaded_files = $files;
//        $businessHash = hash_hmac('sha1', sprintf('b:%s', $businessId), 'unkn0wn');
//        $businessHash = sprintf('%s-%s', substr($businessHash, 0, 10), substr($businessHash, 10, 10));
//        $businessBasePath = 'businesses/' . $businessHash;
        $businessBasePath = 'businesses';
        $disk = Storage::disk('gcs_business');
        $data['files'] = array_map(function ($path) use ($businessBasePath, $disk) {
            if (starts_with($path, $businessBasePath . '/')) {
                return $path;
            }
            if (!$disk->exists($path)) {
                return null;
            }

            $filename = basename($path);
            $newPath = sprintf('%s/productDistributor_files/%s', $businessBasePath, $filename);
            $disk->move($path, $newPath);
            $disk->setVisibility($newPath, 'public');
            return $newPath;
        }, $uploaded_files);

        $data['files'] = json_encode(array_unique(array_filter($data['files'], function ($path) {
            return $path !== null;
        })));
        return $data['files'];
    }

    public function updateFiles(Request $request)
    {
        $data = $request->only('uploaded_files', 'productDistributor_id');
        $productDistributor = ProductDistributor::findOrFail($data['productDistributor_id']);
//        $product = Product::findOrFail($productDistributor->product_id);
//        $businessId = $product->business_id;
        if ($request->input('uploaded_files')) {
            $files = $this->saveUploadedFiles($request->input('uploaded_files'));
            $productDistributor->update(['files' => $files]);
        } else {
            $productDistributor->update(['files' => null]);
        }
        return redirect()->back()
            ->with('success', 'Đã cập nhật files thành công!');
    }

}
