<?php

namespace App\Http\Controllers\Business;

use App\Models\Business\Plan;
use App\Models\Business\Subscription;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PlanController extends Controller
{
    public function index(Request $request, Plan $plan)
    {
        if (auth()->user()->cannot('GATEWAY-viewPlan') && auth()->user()->cannot('GATEWAY-addPlan')) {
            abort(403);
        }
        try {
            $plan = $plan->newQuery();
            $query = [];
            $plan->select('plans.*');
            $count_record_total = $plan->count();
            $plans = $plan->orderBy('id', 'desc')->paginate(20)->appends($query);
            $count_record_in_page = $plans->count();
            foreach ($plans as $plan) {
                $count = Subscription::where('plan_id', $plan->id)->count();
                if ($count > 0) {
                    $plan->used = true;
                } else {
                    $plan->used = false;
                }
            }

            return view('business.plan.index', ['plans' => $plans, 'count_record_total' => $count_record_total,
                'count_record_in_page' => $count_record_in_page]);
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

    public function add()
    {
        if (auth()->user()->cannot('GATEWAY-addPlan')) {
            abort(403);
        }
        return view('business.plan.form');
    }

    public function edit($id)
    {
        if (auth()->user()->cannot('GATEWAY-updatePlan')) {
            abort(403);
        }
        try {
            $plan = Plan::findOrFail($id);
            return view('business.plan.form', ['plan' => $plan]);
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

    public function store(Plan $plan, Request $request)
    {
        if (auth()->user()->cannot('GATEWAY-addPlan')) {
            abort(403);
        }
        $this->validate($request, [
            'name' => 'required|max:255',
            'quota' => 'required|numeric|min:0',
            'price' => 'required|numeric|min:0',
        ],
            [
                'name.required' => 'Trường tên không được để trống!',
                'quota.required' => 'Trường số lượng không được để trống!',
                'price.required' => 'Trường giá không được đẻ trống'
            ]);
        try {
            $data = $request->except(["_token"]);
            $plan->create($data);
            return redirect()->route('Business::plan@index')
                ->with('success', 'Đã thêm mới gói dịch vụ thành công');
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

    public function update($id, Request $request)
    {
        if (auth()->user()->cannot('GATEWAY-updatePlan')) {
            abort(403);
        }
        $this->validate($request, [
            'name' => 'required|max:255',
            'quota' => 'required|numeric|min:0',
            'price' => 'required|numeric|min:0',
        ],
            [
                'name.required' => 'Trường tên không được để trống!',
                'quota.required' => 'Trường số lượng không được để trống!',
                'price.required' => 'Trường giá không được đẻ trống'
            ]);
        try {
            $plan = Plan::findOrFail($id);
            $data = $request->except(["_token", "_method"]);
            $plan->update($data);
            return redirect()->route('Business::plan@index')
                ->with('success', 'Đã cập nhật gói dịch vụ thành công');
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

//    public function delete($id)
//    {
//        try {
//            $plan = Plan::findOrFail($id);
//            $plan->delete();
//            return redirect()->back()->with('success', 'Đã xoá thành công');
//        } catch (Exception $exception) {
//            return view('errors.404');
//        }
//
//    }

}
