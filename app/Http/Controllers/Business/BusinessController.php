<?php

namespace App\Http\Controllers\Business;

use App\API\Api_business_gateway;
use App\API\Api_iCheck_backend;
use App\Events\ProductSyncIcheckExpoEvent;
use App\Exports\Business\BusinessExport;
use App\Models\Business\Assigned_Roles;
use App\Models\Business\Business;
use App\Models\Business\Plan;
use App\Models\Business\Product;
use App\Models\Business\Subscription;
use App\Models\Icheck_newCMS\ActivityLog;
use App\Models\Icheck_newCMS\ModelHasRoles;
use App\Models\Icheck_newCMS\Role;
use App\Repository\Business\BusinessRepositoryInterface;
use App\Repository\Business\UserRepositoryInterface;
use App\User;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class BusinessController extends Controller
{
    private $api_icheck_backend;
    private $api_business_gateway;
    private $userRepository;
    private $businessRepository;

    public function __construct(UserRepositoryInterface $userRepository, BusinessRepositoryInterface $businessRepository)
    {
        $this->api_icheck_backend = new Api_iCheck_backend();
        $this->api_business_gateway = new Api_business_gateway();
        $this->userRepository = $userRepository;
        $this->businessRepository = $businessRepository;
    }

    public function index(Request $request)
    {
        if (auth()->user()->cannot('GATEWAY-viewBusiness') && auth()->user()->cannot('GATEWAY-viewLimitBusiness') && auth()->user()->cannot('GATEWAY-addBusiness')) {
            abort(403);
        }
//        try {
        $response = $this->businessRepository->getList($request);
        $business = $response['business'];
        $query = $response['query'];
        $count_record_total = $business->count();
        $businesses = $business->orderBy('id', 'desc')->paginate(20)->appends($query);
        $count_record_in_page = $businesses->count();
        $CMSusers = User::all();

        foreach ($businesses as $item) {
            $item->subscriptionsInfo = $item->subscriptions()->get();
            $product_subscription_quantity = 0;
            $item->product_managed_quantity = Product::where([['business_id', $item->id], ['status', '<>', Product::STATUS_EXPIRED]])->count();
            $plan_ids = Subscription::where([['business_id', '=', $item->id], ['status', '=', 1]])
                ->get()->pluck('plan_id')->all();
            if ($plan_ids != []) {
                foreach ($plan_ids as $plan_id) {
                    $plan = Plan::findOrFail($plan_id);
                    $product_subscription_quantity = $product_subscription_quantity + $plan->quota;
                }
                $item->product_subscription_quantity = $product_subscription_quantity;
            } else {
                $item->product_subscription_quantity = 0;
            }
            //Created_by
            if ($item->created_by) {
                foreach ($CMSusers as $user) {
                    if ($item->created_by == $user->icheck_id) {
                        $item->created_by = $user->name;
                    }
                }
            }
            //Assignee
            $users = $this->userRepository->getUsersByBusinessId($item->id);
            $filtered_1 = $users->where('icheck_id', '<>', null)->first();
            if ($filtered_1) {
                $item->assigned_to = $filtered_1->name;
            } else {
                $item->assigned_to = '';
            }
            $filtered_2 = $users->where('icheck_id', null)->first();
            if ($filtered_2) {
                $item->login_email = $filtered_2->email;
            } else {
                $item->login_email = '';
            }
        }

        return view('business.businesses.index', ['businesses' => $businesses, 'CMSusers' => $CMSusers, 'count_record_total' => $count_record_total,
            'count_record_in_page' => $count_record_in_page, 'query' => $query]);
//        } catch (Exception $exception) {
//            return view('errors.404');
//        }
    }

    public function add()
    {
        if (auth()->user()->cannot('GATEWAY-addBusiness')) {
            abort(403);
        }
        $CMSusers = User::all();
        return view('business.businesses.form', ['CMSusers' => $CMSusers]);
    }

    public function edit($id)
    {
        if (auth()->user()->cannot('GATEWAY-updateBusiness')) {
            abort(403);
        }

//        $users = $this->userRepository->getUsersByBusinessId($id);
//        $filtered = $users->where('icheck_id', '<>', null);
//        dd($filtered->pluck("id")->toArray());

//        try {
        $business = Business::findOrFail($id);
        $users = $this->userRepository->getUsersByBusinessId($id);
        $filtered_1 = $users->where('icheck_id', '<>', null)->first();
        if ($filtered_1) {
            $business->assigned_to = $filtered_1->icheck_id;
        } else {
            $business->assigned_to = '';
        }
        $filtered_2 = $users->where('icheck_id', null)->first();
        if ($filtered_2) {
            $business->login_email = $filtered_2->email;
        } else {
            $business->login_email = '';
        }
        $CMSusers = User::whereNotNull("name")->get();
        foreach ($CMSusers as $user) {
            $role_ids_assigned = ModelHasRoles::where('model_id', $user->id)->pluck('role_id')->toArray();
            $role_names = Role::whereIn('id', $role_ids_assigned)->pluck('description')->toArray();
            $user->role_names = implode(";", $role_names);
        }
        return view('business.businesses.form', ['business' => $business, 'CMSusers' => $CMSusers]);
//        } catch (Exception $exception) {
//            return view('errors.404');
//        }
    }

    public function store(Request $request)
    {
        if (auth()->user()->cannot('GATEWAY-addBusiness')) {
            abort(403);
        }

        // dd($request->all());

        $this->validate($request, [
            'name' => 'required',
            'address' => 'required',
            'phone_number' => 'required|regex:/^[0-9]{9,11}$/|unique:icheck_business.businesses',
            'email' => 'required|email|unique:icheck_business.businesses',
            'login_email' => 'required|email',
            'tax_code' => 'required|unique:icheck_business.businesses',
            'password' => 'required|min:6'
        ],
            [
                'name.required' => 'Trường tên không được để trống!',
                'tax_code.required' => 'Trường mã số thuế không được để trống!',
                'phone_number.regex' => 'Số điện thoại phải bao gồm các chữ số từ 0 đến 9 và có từ 9 đến 11 chữ số',
                'phone_number.required' => 'Trường số điện thoại không được để trống!',
                'email.required' => 'Trường email doanh nghiệp không được để trống!',
                'login_email.required' => 'Trường email đăng nhập không được để trống!',
                'password.required' => 'Trường mật khẩu không được đẻ trống',
                'password.min' => 'Mật khẩu ít nhất 6 ký tự',
                'email.unique' => 'Email doanh nghiệp đã tồn tại!',
//                'login_email.unique' => 'Email đăng nhập đã tồn tại!',
                'tax_code.unique' => 'Mã số thuế đã tồn tại!',
                'phone_number.unique' => 'Số điện thoại đã đã tồn tại!',
            ]);
//        try {
        $data_request = $request->except(["_token", "password", "assigned_to", "login_email"]);
        $validator = Validator::make(["login_email" => $request->input("login_email")], [
            'login_email' => "email|unique:icheck_business_authorization.users,email",
        ], [
            'login_email.unique' => 'Email đăng nhập đã tồn tại!',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        // Sync iCheck account
        $icheck_authorization = config('api.icheck-authorization');
        $client = new Client([
            'base_uri' => config('api.core'),
            'headers' => [
                'Content-Type' => 'application/json',
                'icheck-authorization' => $icheck_authorization
            ],
            'timeout' => 10,
        ]);

        $response = $client->request('GET', '/backend/users/query-one?q=' . $data_request['email']);
        $data = json_decode((string)$response->getBody(), true);

        if ($data['status'] != 200) {
            $response = $client->request('POST', '/backend/users', [
                'json' => [
                    'type' => 3,
                    'name' => $data_request['name'],
                    'phone' => $data_request['phone_number'],
                    'email' => $data_request['email'],
                    'phone_verified' => true,
                    'email_verified' => true,
                    'password' => str_random(16),
                ],
            ]);
            $data = json_decode((string)$response->getBody(), true);
        }

        if (isset($data['data']['icheck_id'])) {
            //Tạo doanh nghiệp
            $business = Business::create($data_request);
            $business->icheck_id = $data['data']['icheck_id'];
            $business->created_by = auth()->user()->icheck_id;
            $business->save();
            //Tạo user mặc định cho Doanh nghiệp
            $business_user = \App\Models\Business\User::firstOrCreate(["email" => $request->input("login_email")], ["name" => $business->name, "password" => bcrypt($request->input('password'))]);
            $assigned_role = Assigned_Roles::updateOrCreate(["role_id" => 1, "entity_id" => $business_user->id, "entity_type" => "users", "scope" => $business->id]);
            //Tạo user iCheck phụ trách (nếu có) sau khi tạo doanh nghiệp
            if ($request->input("assigned_to")) {
                $CMS_User = User::find($request->input("assigned_to"));
                if ($CMS_User) {
                    $business_CMSuser = \App\Models\Business\User::firstOrCreate(["icheck_id" => $CMS_User->icheck_id], ["name" => $CMS_User->name, "password" => bcrypt("123456")]);
                    $CMS_assigned_role = Assigned_Roles::updateOrCreate(["role_id" => 1, "entity_id" => $business_CMSuser->id, "entity_type" => "users", "scope" => $business->id]);
                }
            }

        } else {
            return "Lỗi!Không tạo được doanh nghiệp!";
        }
        return redirect()->route('Business::businesses@index')
            ->with('success', 'Đã thêm mới doanh nghiệp thành công');
//        } catch (Exception $exception) {
//            return view('errors.404');
//        }
    }

    public function update($id, Request $request)
    {
        if (auth()->user()->cannot('GATEWAY-updateBusiness')) {
            abort(403);
        }
        $this->validate($request, [
            'name' => 'required|max:255',
            'address' => 'required|max:255',
            'phone_number' => "required|regex:/^[0-9]{9,11}$/|unique:icheck_business.businesses,phone_number,$id",
            'email' => "required|email|unique:icheck_business.businesses,email,$id",
            'login_email' => 'required|email',
            'tax_code' => "required|unique:icheck_business.businesses,tax_code,$id",
        ],
            [
                'name.required' => 'Trường tên không được để trống!',
                'tax_code.required' => 'Trường mã số thuế không được để trống!',
                'phone_number.regex' => 'Số điện thoại phải bao gồm các chữ số từ 0 đến 9 và có từ 9 đến 11 chữ số',
                'phone_number.required' => 'Trường số điện thoại không được để trống!',
                'email.required' => 'Trường email doanh nghiệp không được để trống!',
                'email.unique' => 'Email doanh nghiệp đã tồn tại!',
                'tax_code.unique' => 'Mã số thuế đã tồn tại!',
                'phone_number.unique' => 'Số điện thoại đã đã tồn tại!',
            ]);
//        try {
        $business = Business::findOrFail($id);
        $users = $this->userRepository->getUsersByBusinessId($business->id);
        $business_user = $users->where('icheck_id', '=', null)->pluck("id")->toArray();
        $validator = Validator::make(["login_email" => $request->input("login_email")], [
            'login_email' => "email|unique:icheck_business_authorization.users,email,$business_user[0]",
        ], [
            'login_email.unique' => 'Email đăng nhập đã tồn tại!',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $data_request = $request->except(["_token", "_method", "password", "assigned_to"]);

        $business->update($data_request);
        //Reset MK Doanh nghiệp
        if ($request->input('password')) {
            $users = $this->userRepository->getUsersByBusinessId($business->id);
            $filtered = $users->where('icheck_id', '=', null)->pluck("id")->toArray();
            \App\Models\Business\User::whereIn("id", $filtered)->update(["password" => bcrypt($request->input('password'))]);
        }
        if ($request->input('login_email')) {
            $users = $this->userRepository->getUsersByBusinessId($business->id);
            $filtered = $users->where('icheck_id', '=', null)->pluck("id")->toArray();
            \App\Models\Business\User::whereIn("id", $filtered)->update(["email" => $request->input('login_email')]);
        }
        //Update user iCheck phụ trách (nếu có) sau khi update doanh nghiệp
        $users = $this->userRepository->getUsersByBusinessId($business->id);
        $filtered = $users->where('icheck_id', '<>', null)->pluck("id")->toArray();
        if ($request->input("assigned_to")) {
            $CMS_User = User::find($request->input("assigned_to"));
            if ($CMS_User) {
                $business_CMSuser = \App\Models\Business\User::firstOrCreate(["icheck_id" => $CMS_User->icheck_id], ["name" => $CMS_User->name, "password" => bcrypt("123456")]);
                Assigned_Roles::where('scope', $id)->whereIn("entity_id", $filtered)->delete();
                $CMS_assigned_role = Assigned_Roles::updateOrCreate(["role_id" => 1, "entity_id" => $business_CMSuser->id, "entity_type" => "users", "scope" => $business->id]);
            }
        } else {
            Assigned_Roles::where('scope', $id)->whereIn("entity_id", $filtered)->delete();
        }

        return redirect()->back()->with('success', 'Đã cập nhật thành công!');
//        } catch (Exception $exception) {
//            return view('errors.404');
//        }
    }

    public function delete($id)
    {
        if (auth()->user()->cannot('GATEWAY-delBusiness')) {
            abort(403);
        }
        try {
            $this->businessRepository->delete($id);
            return redirect()->back()->with('success', 'Đã xoá thành công!');
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

    public
    function approve($id)
    {
        try {
            $business = Business::findOrFail($id);
            $business->update(['status' => 1]);
            return redirect()->back()
                ->with('success', 'Đã kích hoạt thành công');
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

    public
    function disapprove($id)
    {
        try {
            $business = Business::findOrFail($id);
            $business->update(['status' => 0]);
            return redirect()->back()
                ->with('success', 'Đã kích hoạt thành công');
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

    public function countBusiness()
    {
        $count = Business::where('deleted_at', '=', null)->count();
        return $count;
    }

    public function restore($id)
    {
        if (auth()->user()->cannot('GATEWAY-restoreBusiness')) {
            abort(403);
        }
        try {
            $company = Business::withTrashed()->findOrFail($id);
            $company_name = $company->name;
            $company->restore();
            return redirect()->back()->with("success", "Đã khôi phục tài khoản của doanh nghiệp $company_name");
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

//openExpoStore
    public function openExpoStore($id)
    {
        if (auth()->user()->cannot('GATEWAY-openStoreExpoBusiness')) {
            abort(403);
        }
        try {
            $res = expo_sync_store($id);
            if ($res) {
                Business::where('id', $id)->update(['expo_account_id' => $res]);
                event(new ProductSyncIcheckExpoEvent($id));
                return redirect()->back()->with('success', 'Đã tạo gian hàng thành công!');
            } else {
                return "Lỗi không tạo được gian hàng trên IcheckExpo!";
            }
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

    public function excelExport(Request $request)
    {
        $response = $this->businessRepository->getList($request);
        $business = $response['business'];
        $data = $this->businessRepository->excelExport($business);
        return Excel::download(new BusinessExport($data), 'DS_DoanhNghiep.xlsx');
    }

    public function syncLegacyData($id)
    {
        if (auth()->user()->cannot('GATEWAY-syncLegacyData')) {
            abort(403);
        }
        $this->api_business_gateway->syncLegacyData(["businessId" => $id]);
        return redirect()->back()->with('success', 'Hệ thống đang xử lý đồng bộ thông tin từ hệ thống cũ sang hệ thống mới!');
    }


}
