<?php

namespace App\Http\Controllers\Business;

use App\Mail\BusinessGateway\GlnMail;
use App\Models\Business\Business;
use App\Models\Business\Certificate;
use App\Models\Business\Gln;
use App\Models\Business\IcheckCountry;
use App\Models\Business\User;
use App\Repository\Business\GlnRepositoryInterface;
use App\Repository\Business\UserRepositoryInterface;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class GlnController extends Controller
{
    private $glnRepository;
    private $userRepository;

    public function __construct(GlnRepositoryInterface $glnRepository, UserRepositoryInterface $userRepository)
    {
        $this->glnRepository = $glnRepository;
        $this->userRepository = $userRepository;
    }


    public function index(Request $request, Gln $gln)
    {
        if (auth()->user()->cannot('GATEWAY-viewGLN') && auth()->user()->cannot('GATEWAY-addGLN')) {
            abort(403);
        }
        try {
            $gln = $gln->newQuery();
            $query = [];
            $gln->leftJoin('businesses', 'glns.business_id', '=', 'businesses.id')
                ->leftJoin('certificates', 'glns.certificate_id', '=', 'certificates.id')
                ->select('glns.*', 'businesses.name as business_name', 'certificates.files as certificates')
                ->whereNotNull('glns.business_id');

            if ($request->input('name')) {
                $name = $request->input('name');
                $gln->where('glns.name', 'like', '%' . $name . '%');
                $query['name'] = $name;
            }

            if ($request->input('business_code')) {
                $business_code = $request->input('business_code');
                $gln->where('business_code', 'like', '%' . $business_code . '%');
                $query['business_code'] = $business_code;
            }

            if ($request->input('gln_code')) {
                $gln_code = $request->input('gln_code');
                $gln->where('gln', 'like', '%' . $gln_code . '%');
                $query['gln_code'] = $gln_code;
            }

            if ($request->input('gln_status') != null) {
                $gln_status = $request->input('gln_status');
                $gln->where('glns.status', $gln_status);
                $query['gln_status'] = $gln_status;
            }

            if ($request->input('certificate_status') == 1) {
                $status = $request->input('certificate_status');
                $gln->where('glns.certificate_id', null);
                $query['certificate_status'] = $status;
            }

            if ($request->input('certificate_status') == 2) {
                $certificate_status = $request->input('certificate_status');
                $gln->where('glns.certificate_id', '<>', null);
                $query['certificate_status'] = $certificate_status;
            }

            if ($request->input('business_id')) {
                $business_id = $request->input('business_id');
                $gln->where('glns.business_id', $business_id);
                $query['business_id'] = $business_id;
            }
            $count_record_total = $gln->count();
            $glns = $gln->orderBy('updated_at', 'desc')->paginate(20)->appends($query);
            $count_record_in_page = $glns->count();
            $businesses = Business::where('deleted_at', '=', null)->orderBy('id', 'desc')->get();
            $countries = IcheckCountry::all();
            $approve_waiting_number = Gln::where('status', 0)->whereNotNull('glns.business_id')->get()->count();
            foreach ($glns as $gln) {
                $gln->country_name = '';
                if ($gln->certificates) {
                    $paths = json_decode($gln->certificates);
                    $full_paths = [];
                    foreach ($paths as $path) {
                        $full_file_name = explode("/", $path);
                        $full_file_name = end($full_file_name);
                        $origin_file_name = explode("_", $full_file_name);
                        $origin_file_name = end($origin_file_name);
                        array_push($full_paths, [$origin_file_name, $path, Storage::disk('gcs_business')->url($path)]);
                    }
                    $gln->certificates = json_encode($full_paths);
                }
                foreach ($countries as $country) {
                    if ($gln->country == $country->alpha_2) {
                        $gln->country_name = $country->name;
                    }
                }
            }
            return view('business.gln.index', ['glns' => $glns, 'businesses' => $businesses, 'approve_waiting_number' => $approve_waiting_number, 'count_record_total' => $count_record_total,
                'count_record_in_page' => $count_record_in_page]);
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

    public function add()
    {
        if (auth()->user()->cannot('GATEWAY-addGLN')) {
            abort(403);
        }
        $businesses = Business::where('deleted_at', '=', null)->orderBy('id', 'desc')->get();
        $countries = IcheckCountry::all();
        return view('business.gln.form', ['businesses' => $businesses, 'countries' => $countries]);
    }

    public function edit($id)
    {
        if (auth()->user()->cannot('GATEWAY-updateGLN')) {
            abort(403);
        }
        try {
            $gln = Gln::findOrFail($id);;
            if ($gln->certificate_id) {
                $certificate = Certificate::findOrFail($gln->certificate_id);
                if ($certificate && $certificate->files) {
                    $paths = json_decode($certificate->files);
                    $full_paths = [];
                    foreach ($paths as $path) {
                        $full_file_name = explode("/", $path);
                        $full_file_name = end($full_file_name);
                        $origin_file_name = explode("_", $full_file_name);
                        $origin_file_name = end($origin_file_name);
                        array_push($full_paths, [$origin_file_name, $path, Storage::disk('gcs_business')->url($path)]);
                    }
                    $gln->certificate_files = $full_paths;
                } else {
                    $gln->certificate_files = null;
                }
            }
            $businesses = Business::where('deleted_at', '=', null)->orderBy('id', 'desc')->get();
            $countries = IcheckCountry::all();
            $certificates = Certificate::where('business_id', '=', $gln->business_id)->orderBy('id', 'desc')->get();
            return view('business.gln.form', ['gln' => $gln, 'businesses' => $businesses, 'certificates' => $certificates, 'countries' => $countries]);
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }


    public function store(Gln $gln, Request $request)
    {
        if (auth()->user()->cannot('GATEWAY-addGLN')) {
            abort(403);
        }
        $this->validate($request, [
            'business_id' => 'required',
            'business_code' => 'required',
            'name' => 'required',
            'gln' => 'required',
            'address' => 'required',
            'country' => 'required'
        ],
            [
                'business_id.required' => 'Trường doanh nghiệp không được để trống!',
                'business_code.required' => 'Mã doanh nghiệp không được để trống!',
                'gln.required' => 'Mã GLN không được để trống!',
                'name.required' => 'Tên doanh nghiệp không được để trống!',
                'address.required' => 'Địa chỉ không được để trống!',
                'country.required' => 'Trường quốc gia không được để trống!'
            ]);
//        try {
        $data = $request->except(["_token"]);
        $glnFormatCheck = $this->validateBarcode($data['gln']);
//        $checkUniqueGln = Gln::where([['gln', $data['gln']], ['status', Gln::STATUS_APPROVE]])->count();
        $checkUniqueGln = Gln::where([['gln', $data['gln']]])->whereNotNull('business_id')->count();
        $checkUniqueBusinessCode = Gln::where([['business_code', $data['business_code']], ['status', Gln::STATUS_APPROVE]])->count();

        if (!$glnFormatCheck) {
            return redirect()
                ->back()
                ->with('error', 'Mã GLN không hợp lệ!')
                ->withInput($request->all);
        }

        if ($checkUniqueGln) {
            return redirect()
                ->back()
                ->with('error', 'Mã GLN đã tồn tại trên hệ thống!')
                ->withInput($request->all);
        }

        if ($checkUniqueBusinessCode) {
            return redirect()
                ->back()
                ->with('error', 'Mã doanh nghiệp đã tồn tại trên hệ thống!')
                ->withInput($request->all);
        }

//        $checkExistedGln = Gln::where([['gln', $data['gln']]])->whereNull('business_id')->first();
//        if ($checkExistedGln) {
//            $checkExistedGln->update(['business_id' => $data['business_id']]);
//        } else {
//            $gln->create($data);
//        }
        Gln::updateOrCreate(['gln' => $data['gln'], 'business_id' => null], $data);
        return redirect()->route('Business::gln@index')
            ->with('success', 'Đã thêm mới GLN thành công');
//        } catch (Exception $exception) {
//            return view('errors.404');
//        }
    }

    public function update($id, Request $request)
    {
        if (auth()->user()->cannot('GATEWAY-updateGLN')) {
            abort(403);
        }
        $this->validate($request, [
            'name' => 'required',
            'address' => 'required',
            'country' => 'required'
        ],
            [
                'name.required' => 'Tên doanh nghiệp không được để trống!',
                'address.required' => 'Địa chỉ không được để trống!',
                'country.required' => 'Trường quốc gia không được để trống!'
            ]);
        try {
            $data = $request->except(["_token", "_method"]);
            $gln = Gln::findOrFail($id);
            $gln->update($data);
            if ($gln->status == Gln::STATUS_APPROVE) {
                $this->glnRepository->updateIcheckVendor($id);
            }
            return redirect()->back()
                ->with('success', 'Đã cập nhật GLN thành công!');
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

    public function approve($id)
    {
        if (auth()->user()->cannot('GATEWAY-approveGLN')) {
            abort(403);
        }
        try {
            $this->glnRepository->approve($id);
            return redirect()->back()
                ->with('success', 'Đã kích hoạt thành công!');
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

    public function disapprove($id, Request $request)
    {
        if (auth()->user()->cannot('GATEWAY-disapproveGLN')) {
            abort(403);
        }
        try {
            $note = null;
            if ($request->input('note')) {
                $note = $request->input('note');
            }
            $this->glnRepository->disapprove($id, $note);

            return redirect()->back()
                ->with('success', 'Đã hủy kích hoạt thành công!');
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

    public function updateFile(Request $request)
    {
        try {
            $data = $request->only('gln_id', 'certificate_id');
            $gln = Gln::findOrFail($data['gln_id']);
            $gln->update(['certificate_id' => $data['certificate_id']]);

            return redirect()->back()
                ->with('success', 'Đã cập nhật files thành công');

        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

    public function getFilesById(Request $request)
    {
        if ($request->ajax()) {
            $certificate = Certificate::findOrFail($request->certificate_id);
            if ($certificate && $certificate->files) {
                $paths = json_decode($certificate->files);
                $full_paths = [];
                foreach ($paths as $path) {
                    $full_file_name = explode("/", $path);
                    $full_file_name = end($full_file_name);
                    $origin_file_name = explode("_", $full_file_name);
                    $origin_file_name = end($origin_file_name);
                    array_push($full_paths, [$origin_file_name, $path, Storage::disk('gcs_business')->url($path)]);
                }
                $certificate->files = json_encode($full_paths);
                return response()->json($certificate->files);
            }
        }
        return "";
    }

    public function getGLNByGLNCode(Request $request)
    {
        if ($request->ajax()) {
            $gln_code = $request->gln;
            if ($gln_code) {
                $gln = Gln::where('gln', $gln_code)->whereNull('business_id')->first();
                if ($gln) {
                    return response()->json($gln->toArray());
                }
                return "";
            }
            return "";
        }
        return "";
    }

    protected function calculateCheckDigit($barcode)
    {
        $barcode = (string)$barcode;

        if (!is_numeric($barcode)) {
            return null;
        }

        $length = strlen($barcode);

        if (!in_array($length, [7, 11, 12, 13, 16, 17])) {
            return null;
        }

        $num = $length - 1;
        $isMultiplyBy3 = true;
        $sum = 0;

        for ($i = $num; $i >= 0; $i--) {
            $sum += (int)$barcode[$i] * ($isMultiplyBy3 ? 3 : 1);
            $isMultiplyBy3 = !$isMultiplyBy3;
        }

        $nextMultipleOfTen = ceil($sum / 10) * 10;

        return (int)$nextMultipleOfTen - $sum;
    }

    protected function validateBarcode($barcode)
    {
        $barcode = (string)$barcode;

        return ((int)$barcode[-1]) === $this->calculateCheckDigit(substr($barcode, 0, -1));
    }

    public function checkGLN(Request $request)
    {
        if ($request->ajax()) {
            $gln = $request->gln;
            $check = $this->validateBarcode($gln);
            if ($check) {
                $checkUniqueGln = Gln::where([['gln', $gln], ['status', Gln::STATUS_APPROVE]])->count();
                if ($checkUniqueGln) {
                    return -2;
                }
                return 1;
            }
        }
        return -1;
    }

    public function checkBusinessCode(Request $request)
    {
        if ($request->ajax()) {
            $business_code = $request->business_code;
            $checkUniqueBusinessCode = Gln::where([['business_code', $business_code], ['status', Gln::STATUS_APPROVE]])->count();
            if ($checkUniqueBusinessCode) {
                return -1;
            } else {
                return 1;
            }

        }
        return -1;
    }

    public function updateNote(Request $request)
    {
        if ($request->ajax()) {
            $note = $request->note;
            $gln_id = $request->gln_id;
            $gln = Gln::findOrFail($gln_id);
            $gln->update(['note' => $note]);
            return $gln->note;
        }
        return false;
    }

}
