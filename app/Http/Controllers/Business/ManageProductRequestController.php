<?php

namespace App\Http\Controllers\Business;

use App\API\Api_business_gateway;
use App\API\Api_iCheck_backend;
use App\Mail\BusinessGateway\ProductMail;
use App\Models\Business\Business;
use App\Models\Business\ManagementProductRequest;
use App\Models\Business\Plan;
use App\Models\Business\Product;
use App\Models\Business\Subscription;
use App\Repository\Business\ProductRequestRepositoryInterface;
use App\Repository\Business\UserRepositoryInterface;
use App\Services\Business\BarcodeValidation;
use Exception;
use Google\Cloud\Storage\StorageClient;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class ManageProductRequestController extends Controller
{
    private $api_icheck_backend;
    private $api_business_gateway;
    private $productRequestRepository;
    private $barcodeValidation;

//    private $userRepository;

    public function __construct(ProductRequestRepositoryInterface $productRequestRepository, UserRepositoryInterface $userRepository)
    {
        $this->api_icheck_backend = new Api_iCheck_backend();
        $this->api_business_gateway = new Api_business_gateway();
        $this->productRequestRepository = $productRequestRepository;
        $this->barcodeValidation = new BarcodeValidation();
//        $this->userRepository = $userRepository;
    }

    public function index(Request $request, ManagementProductRequest $productRequest)
    {
        if (auth()->user()->cannot('GATEWAY-viewProductRequest')) {
            abort(403);
        }
//        try {
        $productRequest = $productRequest->newQuery();
        $query = [];
        $productRequest->leftJoin('businesses', 'management_product_requests.business_id', '=', 'businesses.id')
            ->select('management_product_requests.*', 'businesses.name as business_name');
        if ($request->input('business_id')) {
            $business_id = $request->input('business_id');
            $productRequest->where('business_id', $business_id);
            $query['business_id'] = $business_id;
        }

        if ($request->input('barcode')) {
            $barcode = $request->input('barcode');
            $productRequest->where('management_product_requests.barcode', 'like', '%' . $barcode . '%');
            $query['barcode'] = $barcode;
        }

        if ($request->input('name')) {
            $name = $request->input('name');
            $productRequest->whereRaw('lower(data->"$.name") like lower(?)', ['%' . $name . '%']);
            $query['name'] = $name;
        }

        if ($request->input('status') != null) {
            $status = $request->input('status');
            if ($status == 'waiting') {
                $productRequest->whereIn('management_product_requests.status', [0, 2]);
            } else {
                $productRequest->where('management_product_requests.status', $status);
            }
            $query['status'] = $status;
        }

        if ($request->input('from')) {
            $from = $request->input('from');
            $productRequest->where([['management_product_requests.created_at', '>=', date('Y-m-d' . ' 00:00:00', strtotime($from))]]);
            $query['from'] = $from;
        }
        if ($request->input('to')) {
            $to = $request->input('to');
            $productRequest->where([['management_product_requests.created_at', '<=', date('Y-m-d' . ' 23:59:59', strtotime($to))]]);
            $query['to'] = $to;
        }
        $count_record_total = $productRequest->count();
        $businesses = Business::where('deleted_at', '=', null)->orderBy('id', 'desc')->get();
        $productRequests = $productRequest->orderBy('id', 'desc')->paginate(20)->appends($query);
        $count_record_in_page = $productRequests->count();
        $waiting_productRequest_count = ManagementProductRequest::whereIn('status', [0, 2])->count();

        foreach ($productRequests as $productRequest) {
            $product_data = [];
            $productRequest_data = [];
            $product = Product::where([['barcode', $productRequest->barcode]])->first();

            if ($product) {
                $product = $product->toArray();
                $product = array_only($product, ['certificate_id', 'name', 'attributes', 'images', 'price', 'certificate_images']);
//                dd($product_data);
                if (array_key_exists("name", $product) and $product['name']) {
                    $product_data['name'] = $product['name'];
                }
                if (array_key_exists("price", $product) and $product['price']) {
                    $product_data['price'] = $product['price'];
                }
                if (array_key_exists("certificate_id", $product) and $product['certificate_id']) {
                    $product_data['certificate_id'] = $product['certificate_id'];
                }
                if (array_key_exists("images", $product) and $product['images']) {
                    $images = json_decode($product['images'], true);
                    $baseUrl = sprintf('https://%s/%s', 'storage.googleapis.com', config('filesystems.disks.gcs_business.bucket'));
                    $full_paths = [];
                    foreach ($images as $image) {
                        $url = sprintf('%s/%s', $baseUrl, $image);
                        array_push($full_paths, $url);
                    }
                    $product_data['images'] = $full_paths;
                }
                if (array_key_exists("certificate_images", $product) and $product['certificate_images']) {
                    $images = json_decode($product['certificate_images'], true);
                    $baseUrl = sprintf('https://%s/%s', 'storage.googleapis.com', config('filesystems.disks.gcs_business.bucket'));
                    $full_paths = [];
                    foreach ($images as $image) {
                        $url = sprintf('%s/%s', $baseUrl, $image);
                        array_push($full_paths, $url);
                    }
                    $product_data['certificate_images'] = $full_paths;
                }
                if (array_key_exists("attributes", $product) && $product['attributes'] != null && count(json_decode($product['attributes'], true)) > 0) {
                    $attributes = json_decode($product['attributes'], true);
                    $icheck_attributes = $this->api_icheck_backend->getAttributes();
                    if ($icheck_attributes['data']) {
                        foreach ($attributes as $key => $value) {
                            foreach ($icheck_attributes['data'] as $icheck_attribute) {
                                if ($icheck_attribute['id'] == $key) {
                                    $attributes[$icheck_attribute['title']] = $value;
                                    unset($attributes[$key]);
                                }
                            }
                        }
                        $product_data['attributes'] = $attributes;
                    }
                }
            }

//            dd($product_data);
            if ($productRequest->data) {
                $data = json_decode($productRequest->data, true);
                if (array_key_exists("certificate_id", $data) and $data['certificate_id']) {
                    $productRequest_data['certificate_id'] = $data['certificate_id'];
                }
                if (array_key_exists("name", $data) and $data['name']) {
                    $productRequest_data['name'] = $data['name'];
                }
                if (array_key_exists("barcode", $data) and $data['barcode']) {
                    $productRequest_data['barcode'] = $data['barcode'];
                }
                if (array_key_exists("price", $data) and $data['price']) {
                    $productRequest_data['price'] = $data['price'];
                }
                if (array_key_exists("images", $data) and $data['images']) {
                    $images = $data['images'];
                    $baseUrl = sprintf('https://%s/%s', 'storage.googleapis.com', config('filesystems.disks.gcs_business.bucket'));
                    $full_paths = [];
                    foreach ($images as $image) {
                        $url = sprintf('%s/%s', $baseUrl, $image);
                        array_push($full_paths, $url);
                    }
                    $productRequest_data['images'] = $full_paths;
                }

                if (array_key_exists("certificate_images", $data) and $data['certificate_images']) {
                    $images = $data['certificate_images'];
                    $baseUrl = sprintf('https://%s/%s', 'storage.googleapis.com', config('filesystems.disks.gcs_business.bucket'));
                    $full_paths = [];
                    foreach ($images as $image) {
                        $url = sprintf('%s/%s', $baseUrl, $image);
                        array_push($full_paths, $url);
                    }
                    $productRequest_data['certificate_images'] = $full_paths;
                }

                if (array_key_exists("attributes", $data) && $data['attributes']) {
                    $attributes = $data['attributes'];
                    $icheck_attributes = $this->api_icheck_backend->getAttributes();
                    if ($icheck_attributes['data']) {
                        foreach ($attributes as $key => $value) {
                            foreach ($icheck_attributes['data'] as $icheck_attribute) {
                                if ($icheck_attribute['id'] == $key) {
                                    $attributes[$icheck_attribute['title']] = $value;
                                    unset($attributes[$key]);
                                }
                            }
                        }
                        $productRequest_data['attributes'] = $attributes;
                    }
                }
            }

            $mergedData = array_merge($product_data, $productRequest_data);

            if (!array_key_exists("certificate_id", $mergedData)) {
                $mergedData['certificate_id'] = null;
            }
            if (!array_key_exists("name", $mergedData)) {
                $mergedData['name'] = null;
            }

            if (!array_key_exists("price", $mergedData)) {
                $mergedData['price'] = null;
            }
            if (!array_key_exists("images", $mergedData)) {
                $mergedData['images'] = null;
            }
            if (!array_key_exists("certificate_images", $mergedData)) {
                $mergedData['certificate_images'] = null;
            }
            if (!array_key_exists("attributes", $mergedData)) {
                $mergedData['attributes'] = null;
            }
            $productRequest->data = $mergedData;
            //Validate barcode
            $check_barcode = $this->barcodeValidation->validateBarcode($productRequest->barcode);
            if ($check_barcode) {
                $productRequest->validBarcode = 1;
            } else {
                $productRequest->validBarcode = 0;
            }

            //Check management status
            $check_roleManagement = $this->checkProductManagementRoleStatus($productRequest->id);
            if ($check_roleManagement) {
                $productRequest->managed = 1;
            } else {
                $productRequest->managed = 0;
            }
            //Check Subscriptions Status
            $product_subscription_quantity = 0;
            $product_managed_quantity = Product::where('business_id', $productRequest->business_id)->count();
            $plan_ids = array_filter(
                Subscription::where([['business_id', '=', $productRequest->business_id], ['status', '=', 1], ['expires_on', '>', date('Y-m-d H:i:s')]])
                    ->get()
                    ->pluck('plan_id')
                    ->all()
            );
            if ($plan_ids != []) {
                foreach ($plan_ids as $plan_id) {
                    $plan = Plan::findOrFail($plan_id);
                    $product_subscription_quantity = $product_subscription_quantity + $plan->quota;
                }
            }

            if ($product_managed_quantity - $product_subscription_quantity >= 0) {
                $productRequest->subscriptionStatus = 0;
            } else {
                $productRequest->subscriptionStatus = 1;
            }

        }
        return view('business.product.product_request', ['productRequests' => $productRequests, 'businesses' => $businesses, 'waiting_productRequest_count' => $waiting_productRequest_count, 'count_record_total' => $count_record_total,
            'count_record_in_page' => $count_record_in_page]);
//        } catch (Exception $exception) {
//            return view('errors.404');
//        }
    }

    public function approve(Request $request)
    {
        if (auth()->user()->cannot('GATEWAY-approveProductRequest')) {
            abort(403);
        }

        //Check Subscriptions Status
        $id = $request->input('id');
        $requestProduct = ManagementProductRequest::find($id);
        $product_subscription_quantity = 0;
        $product_managed_quantity = Product::where('business_id', $requestProduct->business_id)->count();
        $plan_ids = array_filter(
            Subscription::where([['business_id', '=', $requestProduct->business_id], ['status', '=', 1], ['expires_on', '>', date('Y-m-d H:i:s')]])
                ->get()
                ->pluck('plan_id')
                ->all()
        );
        if ($plan_ids != []) {
            foreach ($plan_ids as $plan_id) {
                $plan = Plan::findOrFail($plan_id);
                $product_subscription_quantity = $product_subscription_quantity + $plan->quota;
            }
        }

        if ($product_managed_quantity - $product_subscription_quantity >= 0) {
            $requestProduct->update(['status' => ManagementProductRequest::STATUS_DISAPPROVED, 'note' => 'Gói dịch vụ quản lý đã thông tin Sản phẩm đã dùng hết! Không thể duyệt thêm Sản phẩm nữa!']);
            return response()->json([
                'messages' => 'Gói dịch vụ quản lý đã thông tin Sản phẩm đã dùng hết! Không thể duyệt thêm Sản phẩm nữa!'
            ]);
        }


        if ($request->input("id")) {
            $id = $request->input("id");
            $verify = 0;
            if ($request->input("verify") == 1) {
                $verify = 1;
            }
            $this->productRequestRepository->approve($id, $verify);
        }

//        return redirect()->back()->with('success', 'Đã duyệt yêu cầu quản lý sản phẩm!');

//        } catch (Exception $exception) {
//            return view('errors.404');
//        }
        return response()->json([
            'messages' => 'Request sent!'
        ]);
    }

    public function disapprove($id, Request $request)
    {
        if (auth()->user()->cannot('GATEWAY-disapproveProductRequest')) {
            abort(403);
        }
        try {
            $note = null;
            if ($request->input('note')) {
                $note = $request->input('note');
            }
//            $product_request = ManagementProductRequest::findOrFail($id);
//            $business = Business::findOrFail($product_request->business_id);
//            $product = [];
//            $product['barcode'] = $product_request->barcode;
//            if ($product_request->data) {
//                $data = json_decode($product_request->data, true);
//                if (array_key_exists("name", $data)) {
//                    $product['name'] = $data['name'];
//                } else {
//                    $product['name'] = "";
//                }
//            } else {
//                $product['name'] = "";
//            }
//            $note = null;
//            if ($request->input('note')) {
//                $note = $request->input('note');
//            }
//
//            $product_request->update(['status' => 3, 'note' => $note]);
//            //Send mail
//            $users = $this->userRepository->getUsersByBusinessId($business->id);
//            $emails = array_filter($users->pluck("email")->toArray());
//
//            if (count($emails) > 0) {
//                foreach ($emails as $email) {
//                    Mail::to($email)->queue(new ProductMail($business, $product, $note));
//                }
//            }
//            return redirect()->back()
//                ->with('success', 'Đã hủy yêu cầu quản lý sản phẩm!');
            $res = $this->productRequestRepository->disapprove($id, $note);

            return redirect()->back()->with('success', 'Hệ thống đang xử lý hủy yêu cầu quản lý sản phẩm!');
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

    public function detail($id)
    {
        if (auth()->user()->cannot('GATEWAY-viewRequestProduct')) {
            abort(403);
        }
        try {
            $product = Product::findOrFail($id);
            $attributes = json_decode($product->attributes, true);
            $icheck_attributes = $this->api_icheck_backend->getAttributes();
            if ($icheck_attributes['data'] && $attributes) {
                foreach ($attributes as $key => $value) {
                    foreach ($icheck_attributes['data'] as $icheck_attribute) {
                        if ($icheck_attribute['id'] == $key) {
                            $attributes[$icheck_attribute['title']] = $value;
                            unset($attributes[$key]);
                        }
                    }
                }
                $product->attributes = $attributes;
            }
            return view('business.product.detail', ['product' => $product]);
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

    public function updateFile(Request $request)
    {
        if (auth()->user()->cannot('GATEWAY-updateRequestProductCertificate')) {
            abort(403);
        }
        try {
            $request_data = $request->only('productRequest_id', 'certificate_id');
            $productRequest = ManagementProductRequest::findOrFail($request_data['productRequest_id']);
            $data = json_decode($productRequest->data, true);
            $data['certificate_id'] = (int)$request_data['certificate_id'];
            $productRequest->data = json_encode($data);
            if ($productRequest->status == 2) {
                $productRequest->status = 0;
            }

            $productRequest->save();
            return redirect()->back()
                ->with('success', 'Đã cập nhật files sản phẩm thành công');

        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

    public function checkStatus($id)
    {
        $productRequest = ManagementProductRequest::findOrFail($id);
        return $productRequest->status;
    }

    public function checkProductManagementRoleStatus($id)
    {
        $product_request = ManagementProductRequest::findOrFail($id);
        $product = Product::where('barcode', $product_request->barcode)->first();
        if ($product->business_id) {
            return true;
        }
        return false;
    }

}
