<?php

namespace App\Http\Controllers\Business;

use App\API\Api_business_gateway;
use App\API\Api_iCheck_backend;
use App\Events\DisapproveProductManagementRoleOfMultiProductsEvent;
use App\Events\DistributorApproveEvent;
use App\Events\ProductInfoApprove;
use App\Events\ProductCertificateImagesSyncEvent;
use App\Exports\Business\ProductExport;
use App\Mail\BusinessGateway\DisapproveMultiProductsInfoMail;
use App\Models\Business\Business;
use App\Models\Business\Certificate;
use App\Models\Business\Gln;
use App\Models\Business\IcheckDistributorTitle;
use App\Models\Business\IcheckProductDistributor;
use App\Models\Business\Product;
use App\Models\Business\ProductDistributor;
use App\Repository\Business\ProductRepositoryInterface;
use App\Repository\Business\UserRepositoryInterface;
use App\Services\Business\BarcodeValidation;
use DateTime;
use Exception;
use Google\Cloud\Storage\StorageClient;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class ProductController extends Controller
{
    private $api_icheck_backend;
    private $barcodeValidation;
    private $userRepository;
    private $productRepository;
    private $api_business_gateway;

    public function __construct(UserRepositoryInterface $userRepository, ProductRepositoryInterface $productRepository)
    {
        $this->api_icheck_backend = new Api_iCheck_backend();
        $this->barcodeValidation = new BarcodeValidation();
        $this->userRepository = $userRepository;
        $this->productRepository = $productRepository;
        $this->api_business_gateway = new Api_business_gateway();
    }

    public function index(Request $request)
    {
        if (auth()->user()->cannot('GATEWAY-viewProduct') && auth()->user()->cannot('GATEWAY-viewLimitProduct')) {
            abort(403);
        }
//        try {
//            $product = $product->newQuery();
//            $query = [];
//            $product->leftJoin('businesses', 'products.business_id', '=', 'businesses.id')
//                ->leftJoin('glns', 'products.gln_id', '=', 'glns.id')
//                ->select('products.*', 'businesses.name as business_name', 'glns.gln as gln')
//                ->where('products.business_id', '<>', null);
//
//            if (auth()->user()->can('GATEWAY-viewLimitProduct') && auth()->user()->cannot('GATEWAY-viewProduct')) {
//                $product->where(function ($query) {
//                    $query->whereExists(function ($query) {
//                        $query->select('business_id')
//                            ->from('user_has_businesses')
//                            ->where("user_has_businesses.user_id", auth()->user()->id)
//                            ->whereRaw('business_id = products.business_id');;
//                    });
//                });
//            }
//
//            if ($request->input('name')) {
//                $name = $request->input('name');
//                $product->where('products.name', 'like', '%' . $name . '%');
//                $query['name'] = $name;
//            }
//
//            if ($request->input('gln_id')) {
//                $gln_id = $request->input('gln_id');
//                $product->where('products.gln_id', '=', $gln_id);
//                $query['gln_id'] = $gln_id;
//            }
//
//            if ($request->input('business_id')) {
//                $business_id = $request->input('business_id');
//                $product->where('products.business_id', $business_id);
//                $query['business_id'] = $business_id;
//            }
//
//            if ($request->input('barcode')) {
//                $barcode = $request->input('barcode');
//                $product->where('products.barcode', 'like', '%' . $barcode . '%');
//                $query['barcode'] = $barcode;
//            }
//
//            if ($request->input('status') != null) {
//                $status = $request->input('status');
//                $product->where('products.status', $status);
//                $query['status'] = $status;
//            }
//
//            if ($request->input('certificate_status') == 1) {
//                $certificate_status = $request->input('certificate_status');
//                $product->where('products.certificate_id', null);
//                $query['certificate_status'] = $certificate_status;
//            }
//
//            if ($request->input('certificate_status') == 2) {
//                $certificate_status = $request->input('certificate_status');
//                $product->where('products.certificate_id', '<>', null);
//                $query['certificate_status'] = $certificate_status;
//            }
//            if ($request->input('has_certificate_images')) {
//                $has_certificate_images = $request->input('has_certificate_images');
//                $product->where('products.has_certificate_images', $has_certificate_images);
//                $query['has_certificate_images'] = $has_certificate_images;
//            }
        $response = $this->productRepository->getList($request);
        $product = $response['product'];
        $query = $response['query'];
        $count_record_total = $product->count();
        $businesses = Business::where('deleted_at', '=', null)->orderBy('id', 'desc')->get();
        $products = $product->orderBy('updated_at', 'desc')->paginate(20)->appends($query);
//            dd($products);
        $count_record_in_page = $products->count();
        foreach ($products as $product) {
            if ($product->attributes == "[]" || $product->attributes == null) {
                $product->attributes = null;
            } else {
                $attributes = json_decode($product->attributes, true);
                $icheck_attributes = $this->api_icheck_backend->getAttributes();
                if ($icheck_attributes['data'] && $attributes) {
                    foreach ($attributes as $key => $value) {
                        foreach ($icheck_attributes['data'] as $icheck_attribute) {
                            if ($icheck_attribute['id'] == $key) {
                                $attributes[$icheck_attribute['title']] = $value;
                                unset($attributes[$key]);
                            }
                        }
                    }
                    $product->attributes = $attributes;
                }
            }
            //Get iCheck vendor for product
            $product->vendor_name = '';
            if ($product->gln_id) {
                $gln = Gln::find($product->gln_id);
                if ($gln) {
                    $product->vendor_name = $gln->name;
                }
            }

            if ($product->vendor_name == '') {
                $res = $this->api_icheck_backend->getProduct($product->barcode);
                if ($res['status'] == 200 && array_key_exists("vendor", $res['data'])) {
                    if (array_key_exists("name", $res['data']['vendor'])) {
                        $product->vendor_name = $res['data']['vendor']['name'];
                    }
                }
            }
            //Product Images
            if ($product->images == "[]" || $product->images == null) {
                $product->images = null;
            } else {
                $images = json_decode($product->images, true);
                $baseUrl = sprintf('https://%s/%s', 'storage.googleapis.com', config('filesystems.disks.gcs_business.bucket'));
                $full_paths = [];
                foreach ($images as $image) {
                    $url = sprintf('%s/%s', $baseUrl, $image);
                    array_push($full_paths, $url);
                }
                $product->images = $full_paths;
            }
            //Product Certificate Images
            if ($product->certificate_images == "[]" || $product->certificate_images == null) {
                $product->certificate_images = null;
            } else {
                $certificate_images = json_decode($product->certificate_images, true);
                $baseUrl = sprintf('https://%s/%s', 'storage.googleapis.com', config('filesystems.disks.gcs_business.bucket'));
                $full_paths = [];
                foreach ($certificate_images as $image) {
                    $url = sprintf('%s/%s', $baseUrl, $image);
                    array_push($full_paths, $url);
                }
                $product->certificate_images = $full_paths;
            }


            //Validate barcode
            $check = $this->barcodeValidation->validateBarcode($product->barcode);
            if ($check) {
                $product->validBarcode = 1;
            } else {
                $product->validBarcode = 0;
            }

            //Original Data (if exists)
            $original_data = [];
            if ($product->original_data) {
                $original_data = [];
                $data = json_decode($product->original_data, true);
                if (array_key_exists("certificate_id", $data) and $data['certificate_id']) {
                    $original_data['certificate_id'] = $data['certificate_id'];
                }
                if (array_key_exists("name", $data) and $data['name']) {
                    $original_data['name'] = $data['name'];
                }

                if (array_key_exists("price", $data) and $data['price']) {
                    $original_data['price'] = $data['price'];
                }
                if (array_key_exists("images", $data) and $data['images']) {
                    $images = $data['images'];
                    $baseUrl = sprintf('https://%s/%s', 'storage.googleapis.com', config('filesystems.disks.gcs_business.bucket'));
                    $full_paths = [];
                    foreach ($images as $image) {
                        $url = sprintf('%s/%s', $baseUrl, $image);
                        array_push($full_paths, $url);
                    }
                    $original_data['images'] = $full_paths;
                }

                if (array_key_exists("certificate_images", $data) and $data['certificate_images']) {
                    $images = $data['certificate_images'];
                    $baseUrl = sprintf('https://%s/%s', 'storage.googleapis.com', config('filesystems.disks.gcs_business.bucket'));
                    $full_paths = [];
                    foreach ($images as $image) {
                        $url = sprintf('%s/%s', $baseUrl, $image);
                        array_push($full_paths, $url);
                    }
                    $original_data['certificate_images'] = $full_paths;
                }

                if (array_key_exists("attributes", $data) && $data['attributes']) {
                    $attributes = $data['attributes'];
                    $icheck_attributes = $this->api_icheck_backend->getAttributes();
                    if ($icheck_attributes['data']) {
                        foreach ($attributes as $key => $value) {
                            foreach ($icheck_attributes['data'] as $icheck_attribute) {
                                if ($icheck_attribute['id'] == $key) {
                                    $attributes[$icheck_attribute['title']] = $value;
                                    unset($attributes[$key]);
                                }
                            }
                        }
                        $original_data['attributes'] = $attributes;
                    }
                }
            }
            $product->original_data = $original_data;

        }
        //Count waiting approve product

        if (auth()->user()->can('GATEWAY-viewLimitProduct') && auth()->user()->cannot('GATEWAY-viewProduct')) {
            $waiting_product_count = Product::where([['products.business_id', '<>', null], ['products.status', 1]])
                ->where(function ($query) {
                    $query->whereExists(function ($query) {
                        $query->select('business_id')
                            ->from('user_has_businesses')
                            ->where("user_has_businesses.user_id", auth()->user()->id)
                            ->whereRaw('business_id = products.business_id');
                    });
                })->count();
        } else {
            $waiting_product_count = Product::where([['products.business_id', '<>', null], ['products.status', 1]])->count();
        }
        //Count waiting need_recheck product
        $needRecheck_product_count = Product::where([['products.business_id', '<>', null], ['products.need_recheck', 1]])->count();
        return view('business.product.index', ['products' => $products, 'businesses' => $businesses, 'waiting_product_count' => $waiting_product_count, 'count_record_total' => $count_record_total,
            'count_record_in_page' => $count_record_in_page, 'needRecheck_product_count' => $needRecheck_product_count, 'query' => $query]);
//        } catch (Exception $exception) {
//            return view('errors.404');
//        }
    }

    public function approve($id)
    {
        if (auth()->user()->cannot('GATEWAY-approveProductInfo')) {
            abort(403);
        }
        $product = Product::findOrFail($id);
        if ($product->status == Product::STATUS_EXPIRED) {
            return "expired";
        }
        if ($product->need_recheck) {
            $product->need_recheck = 0;
            $product->original_data = null;
            $product->save();
        }
//        try {
//        $product = Product::findOrFail($id)->toArray();
//
//        if ($product['attributes'] == '[]' || $product['attributes'] == null) {
//            $product['attributes'] = null;
//        } else {
//            $product['attributes'] = json_decode($product['attributes'], true);
////            if (array_key_exists(2, $product['attributes'])) {
////                $product['attributes'][2] = '';
////            }
//        }
//
//        if ($product['images'] == '[]' || $product['images'] == null) {
//            $product['images'] = null;
//        } else {
//            $product['images'] = json_decode($product['images'], true);
//        }
//
//        if ($product['certificate_images'] == '[]' || $product['certificate_images'] == null) {
//            $product['certificate_images'] = null;
//        } else {
//            $product['certificate_images'] = json_decode($product['certificate_images'], true);
//        }
//        $product['status'] = 2;
//        $product = Product::findOrFail($id);notifyRejected
//        $data = [];
//        $data['gtins'] = [$product->barcode];
//        var_dump($data);
//        $res = $this->api_business_gateway->approveProductInfo($data);
//        var_dump($res);

        event(new ProductInfoApprove($id, $status = Product::STATUS_APPROVED));

//            return redirect()->back()
//                ->with('success', 'Đã duyệt sản phẩm thành công!');
//        } catch (Exception $exception) {
//            return view('errors.404');
//        }
    }

//    public function disapproveInfo($id)
//    {
//        if (auth()->user()->cannot('GATEWAY-approveProductInfo')) {
//            abort(403);
//        }
//        $this->productRepository->disapproveProductInfo($id);
//        $product = Product::find($id);
//        $business = Business::find($product->business_id);
//        if ($business) {
//            //Send emails
//            $product = Product::where('id', $id)->get();
//            $mail_productData = $product->pluck("name", "barcode");
//
//            $users = $this->userRepository->getUsersByBusinessId($business->id);
//            $emails = array_filter($users->pluck("email")->toArray());
//
//            if (count($emails) > 0) {
//                foreach ($emails as $email) {
//                    Mail::to($email)->queue(new DisapproveMultiProductsInfoMail($business->name, $mail_productData, $reason));
//                }
//            }
//        }
//        return redirect()->back()
//            ->with('success', 'Hệ thống đã tiếp nhận yêu cầu và đang xử lý hủy duyệt thông tin Sản phẩm!');
//    }

    public function disapproveMultiProductsInfo(Request $request)
    {
//        dd($request->all());
        if ($request->input('product_ids')) {
            $reason = '';
            if ($request->input('reason')) {
                $reason = $request->input('reason');
            }

            $product_ids = json_decode($request->input('product_ids'), true);
            if ($product_ids) {
                foreach ($product_ids as $product_id) {
                    $this->productRepository->disapproveProductInfo($product_id);
                }
                $business_ids = array_filter(array_unique(Product::whereIn("id", $product_ids)->pluck("business_id")->toArray()));
                if (count($business_ids) > 0) {
                    foreach ($business_ids as $business_id) {
                        $business = Business::find($business_id);
                        if ($business) {
                            //Send emails
                            $products = Product::where('business_id', $business_id)->whereIn('id', $product_ids)->get();
                            $mail_productData = $products->pluck("name", "barcode");

                            $users = $this->userRepository->getUsersByBusinessId($business_id);
                            $emails = array_filter($users->pluck("email")->toArray());
                            if (count($emails) > 0) {
                                foreach ($emails as $email) {
                                    Mail::to($email)->queue(new DisapproveMultiProductsInfoMail($business->name, $mail_productData, $reason));
                                }
                            }
                        } else {
                            continue;
                        }
                    }
                }
            }
        }
        return redirect()->back()
            ->with('success', 'Hệ thống đã tiếp nhận yêu cầu và đang xử lý hủy duyệt thông tin Sản phẩm!');
    }

    public
    function disapprove($id, Request $request)
    {
        $product = Product::findOrFail($id);
        if ($product->status == Product::STATUS_EXPIRED) {
            return "expired";
        }
        $note = '';
        if ($request->input('note')) {
            $note = $request->input('note');
        }
        $data = [];
        $data['businessId'] = $product->business_id;
        $data['gtins'] = [$product->barcode];
        $data['notify'] = true;
        $data['reason'] = $note;
        $res = $this->api_business_gateway->disapproveProductManagementRole($data);
        return redirect()->back()
            ->with('success', 'Hệ thống đã tiếp nhận yêu cầu và đang xử lý hủy bỏ quyền quản lý Sản phẩm!');
    }

//    public function disapprove($id, Request $request)
//    {
//        if (auth()->user()->cannot('GATEWAY-disapproveProductManageRole')) {
//            abort(403);
//        }
//        try {
//            $product = Product::findOrFail($id);
//            $business = Business::findOrFail($product->business_id);
//            $note = null;
//            if ($request->input('note')) {
//                $note = $request->input('note');
//            }
////            $data = [];
////            //Get product information to send mail
////            $product_info_mail = [];
////            $product_info_mail['name'] = $product->name;
////            $product_info_mail['barcode'] = $product->barcode;
////            $data['name'] = $product->name;
////            $data['price'] = $product->price;
////            $data['certificate_id'] = $product->certificate_id;
////            $data['certificate_files'] = json_decode($product->certificate_files, true);
////            $data['images'] = json_decode($product->images, true);
////            $data['certificate_images'] = json_decode($product->certificate_images, true);
////            $data['attributes'] = json_decode($product->attributes, true);
////            $data = json_encode($data);
////            $response = $this->api_icheck_backend->productVerify(['ids' => [$product->barcode], 'expired_at' => date('c', strtotime("-1 days"))]);
////            $note = null;
////            if ($request->input('note')) {
////                $note = $request->input('note');
////            }
////            if ($response['status'] == 200) {
////                ManagementProductRequest::where([['business_id', '=', $product->business_id], ['barcode', '=', $product->barcode]])
////                    ->update(['status' => 3, 'data' => $data, 'note' => $note]);
////                $product->update(['status' => 3, 'business_id' => null]);
////                //Update product_count in businesses table
////                $count = Product::where("business_id", $business->id)->count();
////                Business::where("id", $business->id)->update(["product_count" => $count]);
////                //
////                //Send emails
////                $users = $this->userRepository->getUsersByBusinessId($business->id);
////                $emails = array_filter($users->pluck("email")->toArray());
////                if (count($emails) > 0) {
////                    foreach ($emails as $email) {
////                        Mail::to($email)->queue(new ProductMail($business, $product_info_mail, $note));
////                    }
////                }
////
//////                $emails = User::where([['business_id', '=', $business->id], ['deleted_at', '=', null]])->pluck('email')->toArray();
//////                if (count($emails) > 0) {
//////                    foreach ($emails as $email) {
//////                        Mail::to($email)->queue(new ProductMail($business, $product_info_mail, $note));
//////                    }
//////                }
////
////                return redirect()->back()
////                    ->with('success', 'Đã hủy bỏ quyền quản lý sản phẩm!');
////            } else {
////                return "Chưa hủy xác thực sản phẩm khi bỏ quyền quản lý. Vui lòng báo bộ phận kĩ thuật kiểm tra lại";
////            }
//        } catch (Exception $exception) {
//            return view('errors.404');
//        }
//    }

    public
    function showDistributors($id)
    {
        if (auth()->user()->cannot('GATEWAY-viewDistributor')) {
            abort(403);
        }
        try {
            $product = Product::findOrFail($id);
            $product_distributors = ProductDistributor::leftJoin('distributors', 'product_distributors.distributor_id', '=', 'distributors.id')
                ->select('product_distributors.*', 'distributors.name as distributor_name',
                    'distributors.address as distributor_address', 'distributors.phone as distributor_phone',
                    'distributors.email as distributor_email', 'distributors.website as distributor_website')
                ->where('product_id', $id)->paginate(10);
            $distributor_titles = IcheckDistributorTitle::all()->toArray();
            foreach ($product_distributors as $distributor) {
                for ($i = 0; $i < count($distributor_titles); $i++) {
                    if ($distributor->title_id == $distributor_titles[$i]['id']) {
                        $distributor->title_name = $distributor_titles[$i]['title'];
                        continue;
                    }
                }
            }

            return view('business.product.distributor', ['product' => $product, 'product_distributors' => $product_distributors]);
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

    public
    function updateFile(Request $request)
    {
        if (auth()->user()->cannot('GATEWAY-updateRequestProductCertificate')) {
            abort(403);
        }
        try {
            $data = $request->only('product_id', 'certificate_id');
            $product = Product::findOrFail($data['product_id']);
            $product->update(['certificate_id' => $data['certificate_id']]);

            return redirect()->back()
                ->with('success', 'Đã cập nhật files sản phẩm thành công');

        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

    public
    function checkStatus($id)
    {
        $product = Product::findOrFail($id);
        return $product->status;
    }

    public
    function showBaseInfo($id)
    {
        $product = Product::findOrFail($id);
        return "";
    }

    public
    function getUploadCertificateImages($id)
    {
        $product = Product::findOrFail($id);
        if ($product->certificate_images) {
            $paths = json_decode($product->certificate_images);
            $full_paths = [];

            $storage = app(StorageClient::class);
            $bucket = $storage->bucket(config('filesystems.disks.gcs_business.bucket'));
            $expires = now()->addHour()->getTimestamp();

            foreach ($paths as $path) {
                $object = $bucket->object($path);
                $url = $object->signedUrl($expires);
                $full_file_name = explode("/", $path);
                $full_file_name = end($full_file_name);
                $origin_file_name = explode("_", $full_file_name);
                $origin_file_name = end($origin_file_name);
                array_push($full_paths, [$origin_file_name, $path, $url]);
            }
            $product->certificate_images = $full_paths;
        }
        return view('business.product.uploadCertificateImages', ['product' => $product]);
    }

    public
    function postUploadCertificateImages(Request $request)
    {
        try {
            $product = Product::findOrFail($request->input('product_id'));
            if ($request->has('uploaded_files')) {
                $uploaded_files = $request->input('uploaded_files');
                $businessId = $product->business_id;
                $businessHash = hash_hmac('sha1', sprintf('b:%s', $businessId), 'unkn0wn');
                $businessHash = sprintf('%s-%s', substr($businessHash, 0, 10), substr($businessHash, 10, 10));
                $businessBasePath = 'businesses/' . $businessHash;
                $disk = Storage::disk('gcs_business');
                $data['files'] = array_map(function ($path) use ($businessBasePath, $disk) {
                    if (starts_with($path, $businessBasePath . '/')) {
                        return $path;
                    }
                    if (!$disk->exists($path)) {
                        return null;
                    }

                    $filename = basename($path);
                    $newPath = sprintf('%s/certificate_images/%s', $businessBasePath, $filename);
                    $disk->move($path, $newPath);
                    $disk->setVisibility($newPath, 'public');
                    return $newPath;
                }, $uploaded_files);
                $data['files'] = json_encode(array_unique(array_filter($data['files'], function ($path) {
                    return $path !== null;
                })));
                $product->certificate_images = $data['files'];
                $product->has_certificate_images = 1;
                $product->save();
                //Create iCheck Document
                event(new ProductCertificateImagesSyncEvent($product->certificate_images, $product->icheck_id));
                return redirect()->back()
                    ->with('success', 'Đã cập nhật ảnh chứng chỉ sản phẩm thành công!');

            } else {
                $product->certificate_images = null;
                $product->has_certificate_images = 0;
                $product->save();
                $res = $this->api_icheck_backend->updateCertificateImages([
                    "product_id" => $product->icheck_id,
                    "file_ids" => []
                ]);
                if ($res['status'] == 200) {
                    return redirect()->back()
                        ->with('success', 'Đã cập nhật ảnh chứng chỉ sản phẩm thành công!');
                } else {
                    dd("Có lỗi khi đồng bộ ảnh chứng chỉ sản phẩm lên dữ liệu iCheck!");
                }
            }
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

    public
    function disapproveMultiProducts(Request $request)
    {
//        dd($request->only("product_ids", "multi_note"));
        $data = $request->only("product_ids", "multi_note");
        $product_ids = $data['product_ids'];
        $note = '';
        if ($request->input('multi_note')) {
            $note = $data['multi_note'];
        }
        event(new DisapproveProductManagementRoleOfMultiProductsEvent($product_ids, $note));
        return redirect()->back()
            ->with('success', 'Hệ thống đã tiếp nhận yêu cầu và đang xử lý hủy bỏ quyền Quản lý Sản phẩm!');
    }

    public
    function disapproveAllProducts(Request $request)
    {
        $data = $request->only("disapprove_business_id", "all_note");
        $note = '';
        if ($request->input('all_note')) {
            $note = $data['all_note'];
        }
        $product_ids = array_filter(Product::where("business_id", $data['disapprove_business_id'])->pluck("id")->toArray());
        if (count($product_ids) > 0) {
            event(new DisapproveProductManagementRoleOfMultiProductsEvent($product_ids, $note));
            return redirect()->back()
                ->with('success', 'Hệ thống đã tiếp nhận yêu cầu và đang xử lý hủy bỏ quyền Quản lý Sản phẩm!');
        } else {
            return redirect()->back()
                ->with('success', 'Doanh nghiệp bạn chọn hiện tại không quản lý Sản phẩm nào!');
        }
    }

    public
    function ajaxSearch(Request $request)
    {
        if ($request->input('search')) {
            $search = $request->input('search');
            $products = Product::select("id", "barcode", "name")
                ->whereNotNull("business_id")
                ->where(function ($query) use ($search) {
                    $query->where('name', 'like', '%' . $search . '%')
                        ->orWhere('barcode', 'like', '%' . $search . '%');
                })->get();

            $formatted_data = [];

            foreach ($products as $product) {
                $formatted_data[] = ['id' => $product->id, 'text' => $product->name . ' (' . $product->barcode . ')'];
            }
            $count_searched = $products->count();
            return response()->json(['results' => $formatted_data, 'count_searched' => $count_searched]);
        }
        return '[]';
    }

    public
    function excelExport(Request $request)
    {
        $response = $this->productRepository->getList($request);
        $product = $response['product'];
        $data = $this->productRepository->excelExport($product);
        return Excel::download(new ProductExport($data), 'DS_SanPham.xlsx');
    }

    public
    function syncDistributors($id)
    {
        $product = Product::find($id);
        if ($product and $product->icheck_id) {
            IcheckProductDistributor::where([["product_id", $product->icheck_id], ["visible", 1]])->delete();
            $productDistributorIds = ProductDistributor::where([["product_id", $id], ["status", 1], ["is_visible", 1]])->pluck("id")->toArray();
            if ($productDistributorIds) {
                foreach ($productDistributorIds as $productDistributorId) {
                    $productDistributor = ProductDistributor::findOrFail($productDistributorId);
                    //Đồng bộ NPP <=> iCheck
                    event(new DistributorApproveEvent($productDistributor));
                }
            }
        }
        return redirect()->back()
            ->with('success', 'Hệ thống đã tiếp nhận yêu cầu và đang xử lý đồng bộ lại Nhà phân phối cho Sản phẩm!');
    }
}
