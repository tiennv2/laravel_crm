<?php

namespace App\Http\Controllers\Business;

use App\Models\Business\Holiday;
use App\Models\Icheck_newCMS\ActivityLog;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HolidayController extends Controller
{
    public function index(Request $request, Holiday $holiday)
    {
        if (auth()->user()->cannot('GATEWAY-viewHoliday') && auth()->user()->cannot('GATEWAY-addHoliday')) {
            abort(403);
        }
        try {
            $holiday = $holiday->newQuery();
            $query = [];
            $holiday->select('holidays.*');
            $count_record_total = $holiday->count();
            $holidays = $holiday->orderBy('id', 'desc')->paginate(20)->appends($query);
            $count_record_in_page = $holidays->count();

            return view('business.holiday.index', ['holidays' => $holidays, 'count_record_total' => $count_record_total,
                'count_record_in_page' => $count_record_in_page]);
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }


    public function store(Holiday $holiday, Request $request)
    {
        if (auth()->user()->cannot('GATEWAY-addHoliday')) {
            abort(403);
        }
        $this->validate($request, [
            'start_time' => 'required',
            'end_time' => 'required',
        ],
            [
                'start_time.required' => 'Ngày bắt đầu không được để trống!',
                'end_time.required' => 'Ngày kết thúc không được để trống!',
            ]);
        try {

            $data = [];
//            dd($request->all());
            if ($request->input('start_time')) {
                $request_date = $request->input("start_time");
                $request_date_reformat = \DateTime::createFromFormat("d/m/Y H:i", $request_date)->format("Y-m-d H:i:s");
                $data['start_time'] = $request_date_reformat;
            }

            if ($request->input('end_time')) {
                $request_date = $request->input("end_time");
                $request_date_reformat = \DateTime::createFromFormat("d/m/Y H:i", $request_date)->format("Y-m-d H:i:s");
                $data['end_time'] = $request_date_reformat;
            }

            $new_holiday = $holiday->create($data);
            //Activity_log
            ActivityLog::create([
                "description" => "Create holiday setup",
                "subject_id" => $new_holiday->id,
                "subject_type" => "App\Models\Business\Holiday",
                "causer_id" => auth()->user()->id,
                "causer_type" => "users"
            ]);
            return redirect()->route('Business::holiday@index')
                ->with('success', 'Đã tạo mới khoảng thời gian thành công');
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

    public function update($id, Request $request)
    {
        if (auth()->user()->cannot('GATEWAY-updateHoliday')) {
            abort(403);
        }
        $this->validate($request, [
            'start_time' => 'required',
            'end_time' => 'required',
        ],
            [
                'start_time.required' => 'Ngày bắt đầu không được để trống!',
                'end_time.required' => 'Ngày kết thúc không được để trống!',
            ]);
        try {
            $holiday = Holiday::findOrFail($id);
            $data = [];
            if ($request->input('start_time')) {
                $request_date = $request->input("start_time");
                $request_date_reformat = \DateTime::createFromFormat("d/m/Y H:i", $request_date)->format("Y-m-d H:i:s");
                $data['start_time'] = $request_date_reformat;
            }

            if ($request->input('end_time')) {
                $request_date = $request->input("end_time");
                $request_date_reformat = \DateTime::createFromFormat("d/m/Y H:i", $request_date)->format("Y-m-d H:i:s");
                $data['end_time'] = $request_date_reformat;
            }
            $holiday->update($data);
            //Activity_log
            ActivityLog::create([
                "description" => "Update holiday setup",
                "subject_id" => $id,
                "subject_type" => "App\Models\Business\Holiday",
                "causer_id" => auth()->user()->id,
                "causer_type" => "users"
            ]);
            return redirect()->back()
                ->with('success', 'Đã cập nhật thành công');
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

    public function delete($id)
    {
        if (auth()->user()->cannot('GATEWAY-delHoliday')) {
            abort(403);
        }
        try {
            $holiday = Holiday::findOrFail($id);
            $holiday_data = Holiday::findOrFail($id)->toArray();
            $holiday->delete();
            //Activity_log
            ActivityLog::create([
                "description" => "Delete holiday setup",
                "subject_id" => $id,
                "subject_type" => "App\Models\Business\Holiday",
                "causer_id" => auth()->user()->id,
                "causer_type" => "users",
                "meta_data" => json_encode($holiday_data)
            ]);
            return redirect()->back()->with('success', 'Đã xoá thành công');
        } catch (Exception $exception) {
            return view('errors.404');
        }

    }

}
