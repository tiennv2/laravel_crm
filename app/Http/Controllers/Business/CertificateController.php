<?php

namespace App\Http\Controllers\Business;

use App\Models\Business\Business;
use App\Models\Business\Certificate;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class CertificateController extends Controller
{
    public function index(Request $request, Certificate $certificate)
    {
        if (auth()->user()->cannot('GATEWAY-viewCertificate') && auth()->user()->cannot('GATEWAY-addCertificate')) {
            abort(403);
        }
        try {
            $certificate = $certificate->newQuery();
            $query = [];
            $certificate->leftJoin('businesses', 'certificates.business_id', '=', 'businesses.id')
                ->select('certificates.*', 'businesses.name as business_name');

            if ($request->input('name')) {
                $name = $request->input('name');
                $certificate->where('certificates.name', 'like', '%' . $name . '%');
                $query['name'] = $name;
            }

            if ($request->input('description')) {
                $description = $request->input('description');
                $certificate->where('description', 'like', '%' . $description . '%');
                $query['description'] = $description;
            }

            if ($request->input('status') != null) {
                $status = $request->input('status');
                if ($status == 0) {
                    $certificate->where(function ($query) {
                        $query->where('certificates.status', 0);
                        $query->orWhere('certificates.status', null);
                    });
                } else {
                    $certificate->where('certificates.status', $status);
                }
                $query['status'] = $status;
            }

            if ($request->input('business_id')) {
                $business_id = $request->input('business_id');
                $certificate->where('business_id', $business_id);
                $query['business_id'] = $business_id;
            }

            $certificates = $certificate->orderBy('id', 'desc')->paginate(10)->appends($query);
            $businesses = Business::where('deleted_at', '=', null)->orderBy('id', 'desc')->get();
            foreach ($certificates as $certificate) {
                if ($certificate->files) {
                    $paths = json_decode($certificate->files);
                    $full_paths = [];
                    foreach ($paths as $path) {
                        array_push($full_paths, [$path, Storage::disk('gcs_business')->url($path)]);
                    }
                    $certificate->files = json_encode($full_paths);
                }
                if (strlen(trim($certificate->description)) > 100) {
                    $certificate->short_description = mb_convert_encoding(substr($certificate->description, 0, 100), 'UTF-8', 'UTF-8') . '...';
                } else {
                    $certificate->short_description = $certificate->description;
                }
            }
            return view('business.certificate.index', ['certificates' => $certificates, 'businesses' => $businesses]);
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

    public function add()
    {
        if (auth()->user()->cannot('GATEWAY-addCertificate')) {
            abort(403);
        }
        $businesses = Business::where('deleted_at', '=', null)->get();
        return view('business.certificate.form', ['businesses' => $businesses]);
    }

    public function edit($id)
    {
        if (auth()->user()->cannot('GATEWAY-updateCertificate')) {
            abort(403);
        }
        try {
            $businesses = Business::where('deleted_at', '=', null)->get();
            $certificate = Certificate::findOrFail($id);
            if ($certificate->files) {
                $paths = json_decode($certificate->files);
                $full_paths = [];
                foreach ($paths as $path) {
                    $full_file_name = explode("/", $path);
                    $full_file_name = end($full_file_name);
                    $origin_file_name = explode("_", $full_file_name);
                    $origin_file_name = end($origin_file_name);
                    array_push($full_paths, [$origin_file_name, $path, Storage::disk('gcs_business')->url($path)]);
                }
                $certificate->files = $full_paths;
            }
//        dd($certificate);exit();
            return view('business.certificate.form', ['certificate' => $certificate, 'businesses' => $businesses]);
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

    public function store(Certificate $certificate, Request $request)
    {
        if (auth()->user()->cannot('GATEWAY-addCertificate')) {
            abort(403);
        }
        $this->validate($request, [
            'business_id' => 'required',
//            'name' => 'required|unique:icheck_business.certificates|max:255|regex:/^[-a-z0-9]+$/',
            'name' => 'required',
        ],
            [
                'business_id.required' => 'Trường doanh nghiệp không được để trống!',
                'name.required' => 'Trường tên không được để trống!',
            ]);
        try {
            $data = $request->only(['business_id', 'name', 'description']);
            if ($request->has('uploaded_files')) {
                $uploaded_files = $request->input('uploaded_files');
                $businessId = $data['business_id'];
                $businessHash = hash_hmac('sha1', sprintf('b:%s', $businessId), 'unkn0wn');
                $businessHash = sprintf('%s-%s', substr($businessHash, 0, 10), substr($businessHash, 10, 10));
                $businessBasePath = 'businesses/' . $businessHash;
                $disk = Storage::disk('gcs_business');
                $data['files'] = array_map(function ($path) use ($businessBasePath, $disk, $data) {
                    if (starts_with($path, $businessBasePath . '/')) {
                        return $path;
                    }
                    if (!$disk->exists($path)) {
                        return null;
                    }

                    $filename = basename($path);
                    $newPath = sprintf('%s/certificates/%s/%s', $businessBasePath, $data['name'], $filename);
                    $disk->move($path, $newPath);
                    $disk->setVisibility($newPath, 'public');
                    return $newPath;
                }, $uploaded_files);
                $data['files'] = json_encode(array_unique(array_filter($data['files'], function ($path) {
                    return $path !== null;
                })));
                unset($data['uploaded_files']);
            }
            $data['status'] = 1;
            $certificate->create($data);
            return redirect()->route('Business::certificate@index')
                ->with('success', 'Đã thêm mới giấy tờ thành công');
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }


    public function update($id, Request $request)
    {
        if (auth()->user()->cannot('GATEWAY-updateCertificate')) {
            abort(403);
        }
        $this->validate($request, [
//            'name' => 'required|max:255|regex:/^[-a-z0-9]+$/|unique:icheck_business.certificates,name,' . $id,
            'name' => 'required',
        ],
            [
                'name.required' => 'Trường tên không được để trống!',
            ]);
        try {
            $data = $request->only(['name', 'description', 'business_id']);
            if ($request->has('uploaded_files')) {
                $uploaded_files = $request->input('uploaded_files');
                $businessId = $data['business_id'];
                $businessHash = hash_hmac('sha1', sprintf('b:%s', $businessId), 'unkn0wn');
                $businessHash = sprintf('%s-%s', substr($businessHash, 0, 10), substr($businessHash, 10, 10));
                $businessBasePath = 'businesses/' . $businessHash;
                $disk = Storage::disk('gcs_business');
                $data['files'] = array_map(function ($path) use ($businessBasePath, $disk, $data) {
                    if (starts_with($path, $businessBasePath . '/')) {
                        return $path;
                    }
                    if (!$disk->exists($path)) {
                        return null;
                    }

                    $filename = basename($path);
                    $newPath = sprintf('%s/certificates/%s/%s', $businessBasePath, $data['name'], $filename);
                    $disk->move($path, $newPath);
                    $disk->setVisibility($newPath, 'public');
                    return $newPath;
                }, $uploaded_files);
                $data['files'] = json_encode(array_unique(array_filter($data['files'], function ($path) {
                    return $path !== null;
                })));
                unset($data['uploaded_files']);
            } else {
                $data['files'] = null;
            }
            $certificate = Certificate::findOrFail($id);
            $certificate->update($data);
            return redirect()->back()
                ->with('success', 'Đã cập nhật giấy tờ thành công');
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

    public function delete($id)
    {
        try {
            $certificate = Certificate::findOrFail($id);
            $certificate->delete();
            return redirect()->back()->with('success', 'Đã xoá thành công');
        } catch (Exception $exception) {
            return view('errors.404');
        }

    }

    public function getAllCertificatesByBusinessId(Request $request)
    {
        if ($request->ajax()) {
            $certificates = Certificate::where([['business_id', "=", $request->business_id], ['files', "<>", null]])->get();
            foreach ($certificates as $certificate) {
                $paths = json_decode($certificate->files);
                $full_paths = [];
                foreach ($paths as $path) {
                    $full_file_name = explode("/", $path);
                    $full_file_name = end($full_file_name);
                    $origin_file_name = explode("_", $full_file_name);
                    $origin_file_name = end($origin_file_name);
                    array_push($full_paths, [$origin_file_name, $path, Storage::disk('gcs_business')->url($path)]);
                }
                $certificate->files = json_encode($full_paths);
            }
            return response()->json($certificates);
        }
        return true;
    }

    public function checkUniqueName(Request $request)
    {
        if ($request->ajax()) {
            $certificates = Certificate::where([['name', '=', $request->certificate_name], ['id', '<>', $request->certificate_id]])->get()->toArray();
            if ($certificates) {
                return response()->json(['message' => 'Tên giấy tờ đã tồn tại trên hệ thống. Hãy chọn một tên khác!']);
            } else {
                return response()->json(['message' => 'valid']);
            }
        }
        return true;
    }

    public function approve($id)
    {
        try {
            $gln = Certificate::findOrFail($id);
            $gln->update(['status' => 1]);
            return redirect()->back()
                ->with('success', 'Đã duyệt thành công');
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

    public function disapprove($id)
    {
        try {
            $gln = Certificate::findOrFail($id);
            $gln->update(['status' => 2]);
            return redirect()->back()
                ->with('success', 'Đã hủy duyệt giấy tờ');
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

}
