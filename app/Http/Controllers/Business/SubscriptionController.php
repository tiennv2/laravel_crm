<?php

namespace App\Http\Controllers\Business;

use App\API\Api_business_gateway;
use App\Exports\Business\SubscriptionExport;
use App\Mail\BusinessGateway\SubscriptionMail;
use App\Models\Business\Business;
use App\Models\Business\Plan;
use App\Models\Business\Product;
use App\Models\Business\Subscription;
use App\Models\Business\User;
use App\Models\Icheck_newCMS\ActivityLog;
use App\Repository\Business\SubscriptionRepositoryInterface;
use App\Repository\Business\UserRepositoryInterface;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class SubscriptionController extends Controller
{
    private $userRepository;
    private $subscriptionRepository;
    private $api_business_gateway;

    public function __construct(UserRepositoryInterface $userRepository, SubscriptionRepositoryInterface $subscriptionRepository)
    {
        $this->userRepository = $userRepository;
        $this->subscriptionRepository = $subscriptionRepository;
        $this->api_business_gateway = new Api_business_gateway();
    }

    public function index(Request $request, Subscription $subscription)
    {
        if (auth()->user()->cannot('GATEWAY-viewSubscription') && auth()->user()->cannot('GATEWAY-addSubscription')) {
            abort(403);
        }

        $response = $this->subscriptionRepository->getList($request);
        $subscription = $response['subscription'];
        $query = $response['query'];

        $businesses = Business::where('deleted_at', '=', null)->orderBy('id', 'desc')->get();
        $plans = Plan::orderBy('id', 'desc')->get();
        $icheckUsers = \App\User::all();
        $count_record_total = $subscription->count();
        $subscriptions = $subscription->orderBy('id', 'desc')->paginate(20)->appends($query);
        $count_record_in_page = $subscriptions->count();
        $alert_day = date('Y-m-d H:i:s', strtotime("+3 days"));
        $now = date('Y-m-d H:i:s');
        $alert_number = Subscription::whereNotNull('expires_on')->where([['expires_on', '<', $alert_day], ['expires_on', '>', $now]])->get()->count();
        $expired_number = Subscription::whereNotNull('expires_on')->where([['expires_on', '<', $now]])->get()->count();

        //Approve Waiting
        $approve_waiting_number = Subscription::where('status', 0)->get()->count();
        foreach ($subscriptions as $item) {

            if ($item->contract_files) {
                $paths = json_decode($item->contract_files);
                $full_paths = [];
                foreach ($paths as $path) {
                    array_push($full_paths, [$path, Storage::disk('gcs_business')->url($path)]);
                }
                $item->contract_files = json_encode($full_paths);
            }

            //Get a barcode image of business
            $product = Product::where([["business_id", $item->business_id], ["status", 2]])->first();
            if ($product) {
                $generator = new \Picqer\Barcode\BarcodeGeneratorPNG();
                $item->barcode_image = '<img style="width:120px" src="data:image/png;base64,' . base64_encode($generator->getBarcode($product->barcode, $generator::TYPE_CODE_128)) . '">';
            } else {
                $item->barcode_image = '';
            }
        }

        return view('business.subscription.index', ['subscriptions' => $subscriptions, 'businesses' => $businesses, 'plans' => $plans,
            'alert_number' => $alert_number, 'expired_number' => $expired_number, 'approve_waiting_number' => $approve_waiting_number, 'count_record_total' => $count_record_total,
            'count_record_in_page' => $count_record_in_page, 'icheckUsers' => $icheckUsers, 'query' => $query]);
//        } catch (Exception $exception) {
//            return view('errors.404');
//        }
    }

    public function add()
    {
        if (auth()->user()->cannot('GATEWAY-addSubscription')) {
            abort(403);
        }
        $businesses = Business::where('deleted_at', '=', null)->orderBy('id', 'desc')->get();
        $plans = Plan::all();
        $icheckUsers = \App\User::all();
        return view('business.subscription.form', ['businesses' => $businesses, 'plans' => $plans, 'icheckUsers' => $icheckUsers]);
    }

    public function edit($id)
    {
        if (auth()->user()->cannot('GATEWAY-updateSubscription')) {
            abort(403);
        }
        try {
            $subscription = Subscription::findOrFail($id);

//            if ($subscription->expires_on) {
//                $subscription->expires_on = date("d/m/Y", strtotime($subscription->expires_on));
//            }
            if ($subscription->contract_expiration) {
                $subscription->contract_expiration = date("d/m/Y", strtotime($subscription->contract_expiration));
            } else {
                if ($subscription->expires_on) {
                    $subscription->contract_expiration = date("d/m/Y", strtotime($subscription->expires_on));
                }
            }
            if ($subscription->trial_expiration) {
                $subscription->trial_expiration = date("d/m/Y", strtotime($subscription->trial_expiration));
            }
//            elseif (!$subscription->trial_expiration && $subscription->expire_type === Subscription::EXPIRE_TYPE_DEMO && $subscription->expires_on) {
//                $subscription->trial_expiration = date("d/m/Y", strtotime($subscription->expires_on));
//            }
            if ($subscription->start_time) {
                $subscription->start_time = date("d/m/Y", strtotime($subscription->start_time));
            }
            $businesses = Business::where('deleted_at', '=', null)->orderBy('id', 'desc')->get();
            $plans = Plan::all();
            $icheckUsers = \App\User::all();
            return view('business.subscription.form', ['subscription' => $subscription, 'businesses' => $businesses, 'plans' => $plans, 'icheckUsers' => $icheckUsers]);
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

    public function store(Subscription $subscription, Request $request)
    {
        if (auth()->user()->cannot('GATEWAY-addSubscription')) {
            abort(403);
        }
        $this->validate($request, [
            'business_id' => 'required',
            'plan_id' => 'required',
            'days' => 'required',
            'contract_expiration' => 'required',
        ],
            [
                'business_id.required' => 'Trường doanh nghiệp không được để trống!',
                'plan_id.required' => 'Trường gói dịch vụ không được để trống!',
                'days.required' => 'Trường thời hạn không được để trống',
                'contract_expiration.required' => 'Trường ngày hết hạn hợp đồng không được để trống'
            ]);

//        try {

        $data = $request->except(["_token", "start_time", "contract_expiration", "trial_expiration", "demo_days"]);
        if ($request->input("start_time")) {
            $request_date = $request->input("start_time");
            $request_date_reformat = \DateTime::createFromFormat("d/m/Y", $request_date)->format("Y-m-d 00:00:00");
            $data['start_time'] = $request_date_reformat;
        }
        if ($request->input("trial_expiration")) {
            $request_date = $request->input("trial_expiration");
            $request_date_reformat = \DateTime::createFromFormat("d/m/Y", $request_date)->format("Y-m-d 00:00:00");
            $data['trial_expiration'] = $request_date_reformat;
        }
        if ($request->input("contract_expiration")) {
            $request_date = $request->input("contract_expiration");
            $request_date_reformat = \DateTime::createFromFormat("d/m/Y", $request_date)->format("Y-m-d 00:00:00");
            $data['contract_expiration'] = $request_date_reformat;
            if ($request->input("trial_expiration") || $request->input("days") == 7) {
                if ($request->input("days") == 7) {
                    $data['expires_on'] = $data['contract_expiration'];
                } else {
                    $data['expires_on'] = $data['trial_expiration'];
                }
                $data['expire_type'] = Subscription::EXPIRE_TYPE_DEMO;
                $data['payment_status'] = Subscription::PAYMENT_STATUS_FALSE;
            } else {
                $data['expires_on'] = $data['contract_expiration'];
                $data['expire_type'] = Subscription::EXPIRE_TYPE_IN_CONTRACT;
                $data['payment_status'] = Subscription::PAYMENT_STATUS_TRUE;
            }
        }

        $data['status'] = Subscription::SUBSCRIPTION_STATUS_PENDING;
        $new_sub = $subscription->create($data);
        //Activity_log
        ActivityLog::create([
            "description" => "Create a subscription",
            "subject_id" => $new_sub->id,
            "subject_type" => "App\Models\Business\Subscription",
            "causer_id" => auth()->user()->id,
            "causer_type" => "users"
        ]);
        //Auto approve
        $this->api_business_gateway->approve([
            "subscriptionId" => $new_sub->id
        ]);

        return redirect()->route('Business::subscription@index')
            ->with('success', 'Đã đăng ký gói dịch vụ thành công');
//        } catch (Exception $exception) {
//            return view('errors.404');
//        }
    }

    public function update($id, Request $request)
    {
        if (auth()->user()->cannot('GATEWAY-updateSubscription')) {
            abort(403);
        }
        $this->validate($request, [
            'business_id' => 'required',
            'plan_id' => 'required',
            'days' => 'required',
            'contract_expiration' => 'required',
        ],
            [
                'business_id.required' => 'Trường doanh nghiệp không được để trống!',
                'plan_id.required' => 'Trường gói dịch vụ không được để trống!',
                'days.required' => 'Trường thời hạn không được để trống',
                'contract_expiration.required' => 'Trường ngày hết hạn hợp đồng không được để trống'
            ]);
        try {
            $data = $request->except(["_token", "_method", "start_time", "contract_expiration", "trial_expiration", "demo_days"]);
            $subscription = Subscription::findOrFail($id);
            if ($request->input("start_time")) {
                $request_date = $request->input("start_time");
                $request_date_reformat = \DateTime::createFromFormat("d/m/Y", $request_date)->format("Y-m-d 00:00:00");
                $data['start_time'] = $request_date_reformat;
            } else {
                $data['start_time'] = null;
            }
            if ($request->input("trial_expiration")) {
                $request_date = $request->input("trial_expiration");
                $request_date_reformat = \DateTime::createFromFormat("d/m/Y", $request_date)->format("Y-m-d 00:00:00");
                $data['trial_expiration'] = $request_date_reformat;
            }
            if ($request->input("contract_expiration")) {
                $request_date = $request->input("contract_expiration");
                $request_date_reformat = \DateTime::createFromFormat("d/m/Y", $request_date)->format("Y-m-d 00:00:00");
                $data['contract_expiration'] = $request_date_reformat;
                if ($request->input("days") == 7 || $request->input("trial_expiration") && !$subscription->trial_expiration|| $request->input("trial_expiration") && $subscription->payment_status == Subscription::PAYMENT_STATUS_FALSE) {
                    if ($request->input("days") == 7) {
                        $data['expires_on'] = $data['contract_expiration'];
                    } else {
                        $data['expires_on'] = $data['trial_expiration'];
                    }
                    $data['expire_type'] = Subscription::EXPIRE_TYPE_DEMO;
                    $data['payment_status'] = Subscription::PAYMENT_STATUS_FALSE;
                } else {
                    $data['expires_on'] = $data['contract_expiration'];
                    $data['expire_type'] = Subscription::EXPIRE_TYPE_IN_CONTRACT;
                }
            }

            if (!$request->input("assigned_to")) {
                $data['assigned_to'] = null;
            }

            $subscription->update($data);
            //Activity_log
            ActivityLog::create([
                "description" => "Update a subscription",
                "subject_id" => $subscription->id,
                "subject_type" => "App\Models\Business\Subscription",
                "causer_id" => auth()->user()->id,
                "causer_type" => "users"
            ]);

//            if ($subscription->status == Subscription::SUBSCRIPTION_STATUS_PENDING) {
//                $this->api_business_gateway->approve([
//                    "subscriptionId" => $subscription->id
//                ]);
//            }
            return redirect()->route('Business::subscription@index')
                ->with('success', 'Đã cập nhật gói dịch vụ thành công');
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

    public function approve($id, Request $request)
    {
        if (auth()->user()->cannot('GATEWAY-approveSubscription')) {
            abort(403);
        }
//        try {
        $subscription = Subscription::findOrFail($id);
        $data = [];

        if ($request->input('approve_contract_expiration')) {
            $request_date = $request->input("approve_contract_expiration");
            $request_date_reformat = \DateTime::createFromFormat("d/m/Y", $request_date)->format("Y-m-d 00:00:00");
            $data['contract_expiration'] = $request_date_reformat;
        } else {
            $data['contract_expiration'] = date('Y-m-d 00:00:00', strtotime(" + $subscription->days days"));
        }
        if ($request->input('approve_trial_expiration')) {
            $request_date = $request->input("approve_trial_expiration");
            $request_date_reformat = \DateTime::createFromFormat("d/m/Y", $request_date)->format("Y-m-d 00:00:00");
            $data['trial_expiration'] = $request_date_reformat;
        }

        if ($request->input("approve_trial_expiration") || $subscription->days == 7) {
            if ($subscription->days == 7) {
                $data['expires_on'] = $data['contract_expiration'];
            } else {
                $data['expires_on'] = $data['trial_expiration'];
            }
            $data['expire_type'] = Subscription::EXPIRE_TYPE_DEMO;
            $data['payment_status'] = Subscription::PAYMENT_STATUS_FALSE;
        } else {
            $data['expires_on'] = $data['contract_expiration'];
            $data['expire_type'] = Subscription::EXPIRE_TYPE_IN_CONTRACT;
            $data['payment_status'] = Subscription::PAYMENT_STATUS_TRUE;
        }

        if ($request->input('approve_start_time')) {
            $request_date = $request->input("approve_start_time");
            $request_date_reformat = \DateTime::createFromFormat("d/m/Y", $request_date)->format("Y-m-d 00:00:00");
            $data['start_time'] = $request_date_reformat;
        } else {
            $data['start_time'] = date('Y-m-d 00:00:00');
        }

        $subscription->update($data);
        //Activity_log
        ActivityLog::create([
            "description" => "Approve a subscription",
            "subject_id" => $subscription->id,
            "subject_type" => "App\Models\Business\Subscription",
            "causer_id" => auth()->user()->id,
            "causer_type" => "users"
        ]);
        $this->api_business_gateway->approve([
            "subscriptionId" => $id
        ]);

        return redirect()->back()
            ->with('success', 'Hệ thống đang xử lý yêu cầu kích hoạt gói dịch vụ cho Doanh nghiệp!');
//        } catch (Exception $exception) {
//            return view('errors.404');
//        }
    }

    public function disapprove($id, Request $request)
    {
        if (auth()->user()->cannot('GATEWAY-disapproveSubscription')) {
            abort(403);
        }
//        try {
        $note = null;
        if ($request->input('note')) {
            $note = $request->input('note');
        }
        $this->subscriptionRepository->disapprove($id, $note);
        //Activity_log
        ActivityLog::create([
            "description" => "Disapprove a subscription",
            "subject_id" => $id,
            "subject_type" => "App\Models\Business\Subscription",
            "causer_id" => auth()->user()->id,
            "causer_type" => "users"
        ]);
        //Update quota in businesses table
//            $product_subscription_quantity = 0;
//            $plan_ids = Subscription::where([['business_id', '=', $subscription->id], ['status', '=', 1], ['expires_on', '>', date('Y-m-d H:i:s')]])
//                ->get()->pluck('plan_id')->all();
//            if ($plan_ids != []) {
//                foreach ($plan_ids as $plan_id) {
//                    $plan = Plan::findOrFail($plan_id);
//                    $product_subscription_quantity = $product_subscription_quantity + $plan->quota;
//                }
//                Business::where("id", $subscription->business_id)->update(["quota" => $product_subscription_quantity]);
//            } else {
//                Business::where("id", $subscription->business_id)->update(["quota" => 0]);
//            }

        return redirect()->back()
            ->with('success', 'Đang xử lý hủy kích hoạt gói dịch vụ của Doanh nghiệp!');
//        } catch (Exception $exception) {
//            return view('errors.404');
//        }
    }

    public function uploadFile(Request $request)
    {
//        try {
        if ($request->hasFile('files')) {
            $files = $request->file('files');
            $disk = Storage::disk('gcs_business');
            $fileNames = [];
            foreach ($files as $file) {
                if ($file->getError() == 0) {
                    $file_name = trim($file->getClientOriginalName());
//                        $file_name = preg_replace(" / [^a - zA - Z0 - 9.]/", "_", $file_name);
                    $path = $disk->putFileAs('contracts/files', $file, $file_name, 'public');
                    $fileNames[] = $path;
                }
            }
            $full_paths = [];
            foreach ($fileNames as $fileName) {
                array_push($full_paths, [$fileName, $disk->url($fileName)]);

            }
            $full_paths = json_encode($full_paths);
            return response()->json([
                'full_paths' => $full_paths
            ]);
        }
        return response()->json([
            'error' => 'Upload fail!',
        ]);

//        } catch (\Exception $exception) {
//            return response()->json([
//                'error' => $exception->getMessage(),
//            ]);
//        }

    }

    public function updateContract(Request $request)
    {
        if (auth()->user()->cannot('GATEWAY-updateSubscriptionContract')) {
            abort(403);
        }
        try {
            $files = $request->only('uploaded_files', 'subscription_id');
            $subscription = Subscription::findOrFail($files['subscription_id']);
            if ($request->has('uploaded_files')) {
                $contract_files = json_encode($files['uploaded_files']);
                $subscription->update(['contract_files' => $contract_files]);
            } else {
                $subscription->update(['contract_files' => null]);
            }
            return redirect()->back()
                ->with('success', 'Đã cập nhật files hợp đồng thành công');

        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

    public function updatePaymentStatus($id, Request $request)
    {
//        try {
        if (auth()->user()->cannot('GATEWAY-updatePaymentStatus')) {
            abort(403);
        }
        $payment_status = $request->input('payment_status');
        $subscription = Subscription::findOrFail($id);
        $subscription->update(['payment_status' => $payment_status]);
        return "success";
//        return redirect()->back()
//            ->with('success', 'Đã cập nhật files hợp đồng thành công');

//        } catch (Exception $exception) {
//            return view('errors.404');
//        }
    }

    public function confirmPaymentStatus($id)
    {
        if (auth()->user()->cannot('GATEWAY-updatePaymentStatus')) {
            abort(403);
        }
        $subscription = Subscription::findOrFail($id);
        $data = [];
        $data['payment_status'] = Subscription::PAYMENT_STATUS_TRUE;
        if ($subscription->status == Subscription::SUBSCRIPTION_STATUS_APPROVED || $subscription->status == Subscription::SUBSCRIPTION_STATUS_DISAPPROVED) {
            if ($subscription->contract_expiration) {
                $data['expires_on'] = $subscription->contract_expiration;
                $data['expire_type'] = Subscription::EXPIRE_TYPE_IN_CONTRACT;
            }
        }

        $subscription->update($data);
        if ($subscription->status == Subscription::SUBSCRIPTION_STATUS_DISAPPROVED) {
            $this->api_business_gateway->approve([
                "subscriptionId" => $id
            ]);
        }

        //Activity_log
        ActivityLog::create([
            "description" => "Confirm payment status of a subscription",
            "subject_id" => $id,
            "subject_type" => "App\Models\Business\Subscription",
            "causer_id" => auth()->user()->id,
            "causer_type" => "users"
        ]);
        return redirect()->back()->with('success', 'Xác nhận đã thanh toán cho Gói dịch vụ có Id = ' . $id);
    }

    public function confirmPaymentStatusAfterExpired($id, Request $request)
    {
        if (auth()->user()->cannot('GATEWAY-updatePaymentStatus')) {
            abort(403);
        }
        $subscription = Subscription::findOrFail($id);
        $data = [];
        if ($subscription->status == Subscription::SUBSCRIPTION_STATUS_DISAPPROVED && $request->input('contract_expiration')) {
            $request_date = $request->input("contract_expiration");
            $request_date_reformat = \DateTime::createFromFormat("d/m/Y", $request_date)->format("Y-m-d 00:00:00");
            $data['contract_expiration'] = $request_date_reformat;
            $data['expires_on'] = $data['contract_expiration'];
            $data['payment_status'] = Subscription::PAYMENT_STATUS_TRUE;
            $data['expire_type'] = Subscription::EXPIRE_TYPE_IN_CONTRACT;

            $subscription->update($data);
            $this->api_business_gateway->approve([
                "subscriptionId" => $id
            ]);
            //Activity_log
            ActivityLog::create([
                "description" => "Confirm payment status of a subscription",
                "subject_id" => $id,
                "subject_type" => "App\Models\Business\Subscription",
                "causer_id" => auth()->user()->id,
                "causer_type" => "users"
            ]);
        }
        return redirect()->back()->with('success', 'Xác nhận đã thanh toán cho Gói dịch vụ có Id = ' . $id);
    }

    public function notConfirmPaymentStatus($id)
    {
        if (auth()->user()->cannot('GATEWAY-updatePaymentStatus')) {
            abort(403);
        }
        $subscription = Subscription::findOrFail($id);
        $data = [];
        $data['payment_status'] = Subscription::PAYMENT_STATUS_FALSE;
        $subscription->update($data);
        //Activity_log
        ActivityLog::create([
            "description" => "Not confirm payment status of a subscription",
            "subject_id" => $id,
            "subject_type" => "App\Models\Business\Subscription",
            "causer_id" => auth()->user()->id,
            "causer_type" => "users"
        ]);
        if ($subscription->status == Subscription::SUBSCRIPTION_STATUS_APPROVED) {
            $this->api_business_gateway->disapprove([
                "subscriptionId" => $id,
                "notify" => true,
                "reason" => "Gói dịch vụ chưa được thanh toán!"
            ]);
        }
        return redirect()->back()->with('success', 'Xác nhận trạng thái chưa thanh toán đối vói Gói dịch vụ có Id = ' . $id);
    }

    public function updateNote(Request $request)
    {
        if ($request->ajax()) {
            $note = $request->note;
            $subscription_id = $request->subscription_id;
            $subscription = Subscription::findOrFail($subscription_id);
            $subscription->update(['note' => $note]);
            return $subscription->note;
        }
        return "";
    }

    public function updateContractValue(Request $request)
    {
        if (auth()->user()->cannot('GATEWAY-updateSubscription')) {
            abort(403);
        }
        if ($request->ajax()) {
            $subscription_id = $request->subscription_id;
            $contract_value = $request->contract_value;
            $subscription = Subscription::findOrFail($subscription_id);
            $subscription->update(['contract_value' => $contract_value]);
            return $subscription->contract_value;
        }
        return '';
    }

    public function updateAssignedTo(Request $request)
    {
        if (auth()->user()->cannot('GATEWAY-updateSubscription')) {
            abort(403);
        }
        if ($request->ajax()) {
            $subscription_id = $request->subscription_id;
            $assigned_to = $request->icheck_id;
            $subscription = Subscription::findOrFail($subscription_id);
            $subscription->update(['assigned_to' => $assigned_to]);
            return $subscription->assigned_to;
        }
        return '';
    }

    public function updateExpireType(Request $request)
    {
        if (auth()->user()->cannot('GATEWAY-updateSubscription')) {
            abort(403);
        }
        if ($request->ajax()) {
            $subscription_id = $request->subscription_id;
            $expire_type = $request->expire_type;
            $subscription = Subscription::findOrFail($subscription_id);
            $subscription->update(['expire_type' => $expire_type]);
            return $subscription->expire_type;
        }
        return '';
    }

    public function updateStartTime(Request $request)
    {
        if (auth()->user()->cannot('GATEWAY-updateSubscription')) {
            abort(403);
        }
        if ($request->ajax()) {
            $request_date = $request->input('start_time');
            $request_date_reformat = \DateTime::createFromFormat("d/m/Y", $request_date)->format("Y-m-d 00:00:00");

            $start_time = $request_date_reformat;
            $subscription_id = $request->subscription_id;
            $subscription = Subscription::findOrFail($subscription_id);
            $subscription->update(['start_time' => $start_time]);
            return $subscription->start_time;
        }
        return '';
    }


    public function updateExpiresOn(Request $request)
    {
        if (auth()->user()->cannot('GATEWAY-updateSubscription')) {
            abort(403);
        }
        if ($request->ajax()) {
            $request_date = $request->input('expires_on');
            $request_date_reformat = \DateTime::createFromFormat("d/m/Y", $request_date)->format("Y-m-d 00:00:00");

            $expires_on = $request_date_reformat;
            $subscription_id = $request->subscription_id;
            $subscription = Subscription::findOrFail($subscription_id);
            $subscription->update(['expires_on' => $expires_on]);
            return $subscription->expires_on;
        }
        return '';
    }

    public function excelExport(Request $request)
    {
        $response = $this->subscriptionRepository->getList($request);
        $subscription = $response['subscription'];
        $data = $this->subscriptionRepository->excelExport($subscription);
        return Excel::download(new SubscriptionExport($data), 'DS_GoiDichVu.xlsx');
    }

}
