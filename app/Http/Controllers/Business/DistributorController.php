<?php

namespace App\Http\Controllers\Business;

use App\Models\Business\Business;
use App\Models\Business\Distributor;
use App\Models\Business\IcheckDistributor;
use App\Models\Business\IcheckProductDistributor;
use App\Models\Business\ProductDistributor;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class DistributorController extends Controller
{
    public function index(Request $request, Distributor $distributor)
    {
        if (auth()->user()->cannot('GATEWAY-viewDistributor') && auth()->user()->cannot('GATEWAY-addDistributor')) {
            abort(403);
        }
        try {
            $distributor = $distributor->newQuery();
            $query = [];
            $distributor->leftJoin('businesses', 'distributors.business_id', '=', 'businesses.id')
                ->select('distributors.*', 'businesses.name as business_name');

            if ($request->input('business_id')) {
                $business_id = $request->input('business_id');
                $distributor->where('business_id', $business_id);
                $query['business_id'] = $business_id;
            }

            if ($request->input('name')) {
                $name = $request->input('name');
                $distributor->where('distributors.name', 'like', '%' . $name . '%');
                $query['name'] = $name;
            }

            if ($request->input('address')) {
                $address = $request->input('address');
                $distributor->where('distributors.address', 'like', '%' . $address . '%');
                $query['address'] = $address;
            }

            if ($request->input('phone')) {
                $phone = $request->input('phone');
                $distributor->where('distributors.phone', 'like', '%' . $phone . '%');
                $query['phone'] = $phone;
            }

            if ($request->input('email')) {
                $email = $request->input('email');
                $distributor->where('distributors.email', 'like', '%' . $email . '%');
                $query['email'] = $email;
            }

            if ($request->input('from')) {
                $from = $request->input('from');
                $distributor->where([['distributors.created_at', '>=', date('Y-m-d' . ' 00:00:00', strtotime($from))]]);
                $query['from'] = $from;
            }
            if ($request->input('to')) {
                $to = $request->input('to');
                $distributor->where([['distributors.created_at', '<=', date('Y-m-d' . ' 23:59:59', strtotime($to))]]);
                $query['to'] = $to;
            }
            $businesses = Business::where('deleted_at', '=', null)->get();
            $count_record_total = $distributor->count();
            $distributors = $distributor->orderBy('id', 'desc')->paginate(20)->appends($query);
            $count_record_in_page = $distributors->count();
            return view('business.distributor.index', ['distributors' => $distributors, 'businesses' => $businesses, 'count_record_total' => $count_record_total,
                'count_record_in_page' => $count_record_in_page]);
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

    public function add()
    {
        if (auth()->user()->cannot('GATEWAY-addDistributor')) {
            abort(403);
        }
        $businesses = Business::where('deleted_at', '=', null)->get();
        return view('business.distributor.form', ["businesses" => $businesses]);
    }

    public function edit($id)
    {
        if (auth()->user()->cannot('GATEWAY-updateDistributor')) {
            abort(403);
        }
        try {
            $distributor = Distributor::findOrFail($id);
            $businesses = Business::where('deleted_at', '=', null)->get();
            if ($distributor->files) {
                $paths = json_decode($distributor->files);
                $full_paths = [];
                foreach ($paths as $path) {
                    $full_file_name = explode("/", $path);
                    $full_file_name = end($full_file_name);
                    $origin_file_name = explode("_", $full_file_name);
                    $origin_file_name = end($origin_file_name);
                    array_push($full_paths, [$origin_file_name, $path, Storage::disk('gcs_business')->url($path)]);
                }
                $distributor->files = $full_paths;
            }
            return view('business.distributor.form', ['distributor' => $distributor, "businesses" => $businesses]);
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

    public function store(Request $request)
    {
        if (auth()->user()->cannot('GATEWAY-addDistributor')) {
            abort(403);
        }
        $this->validate($request, [
            'name' => 'required',
            'address' => 'required',
//            'business_id' => 'required'
        ],
            [
                'name.required' => 'Trường tên không được để trống!',
                'address.required' => 'Trường địa chỉ không được để trống!',
//                'business_id.required' => 'Trường Doanh nghiệp không được để trống!',
            ]);
//        try {
        $data_request = $request->except(["_token", "uploaded_files", "files"]);
        $distributor = Distributor::create($data_request);
//        if ($request->has('uploaded_files')) {
//            $uploaded_files = $request->input('uploaded_files');
//            $distributorId = $distributor->id;
//            $distributorHash = hash_hmac('sha1', sprintf('b:%s', $distributorId), 'unkn0wn');
//            $distributorHash = sprintf('%s-%s', substr($distributorHash, 0, 10), substr($distributorHash, 10, 10));
//            $distributorBasePath = 'distributors/' . $distributorHash;
//            $disk = Storage::disk('gcs_business');
//            $data['files'] = array_map(function ($path) use ($distributorBasePath, $disk) {
//                if (starts_with($path, $distributorBasePath . '/')) {
//                    return $path;
//                }
//                if (!$disk->exists($path)) {
//                    return null;
//                }
//
//                $filename = basename($path);
//                $newPath = sprintf('%s/files/%s', $distributorBasePath, $filename);
//                $disk->move($path, $newPath);
//                $disk->setVisibility($newPath, 'public');
//                return $newPath;
//            }, $uploaded_files);
//            $data['files'] = json_encode(array_unique(array_filter($data['files'], function ($path) {
//                return $path !== null;
//            })));
//            $distributor->update(["files" => $data['files']]);
//        }
        if ($request->ajax()) {
            return json_encode($distributor);
        }
        return redirect()->route('Business::distributor@index')
            ->with('success', 'Đã thêm mới Nhà phân phối thành công');
//        } catch (Exception $exception) {
//            return view('errors.404');
//        }
    }

    public function update($id, Request $request)
    {
        if (auth()->user()->cannot('GATEWAY-updateDistributor')) {
            abort(403);
        }
        $this->validate($request, [
            'name' => 'required',
            'address' => 'required',
//            'business_id' => 'required'
        ],
            [
                'name.required' => 'Trường tên không được để trống!',
                'address.required' => 'Trường địa chỉ không được để trống!',
//                'business_id.required' => 'Trường Doanh nghiệp không được để trống!',
            ]);
        try {
            $distributor = Distributor::findOrFail($id);
            $data_request = $request->except(["_token", "_method", "files", "uploaded_files"]);
//            if ($request->has('uploaded_files')) {
//                $uploaded_files = $request->input('uploaded_files');
//                $distributorId = $distributor->id;
//                $distributorHash = hash_hmac('sha1', sprintf('b:%s', $distributorId), 'unkn0wn');
//                $distributorHash = sprintf('%s-%s', substr($distributorHash, 0, 10), substr($distributorHash, 10, 10));
//                $distributorBasePath = 'distributors/' . $distributorHash;
//                $disk = Storage::disk('gcs_business');
//                $data_request['files'] = array_map(function ($path) use ($distributorBasePath, $disk) {
//                    if (starts_with($path, $distributorBasePath . '/')) {
//                        return $path;
//                    }
//                    if (!$disk->exists($path)) {
//                        return null;
//                    }
//
//                    $filename = basename($path);
//                    $newPath = sprintf('%s/files/%s', $distributorBasePath, $filename);
//                    $disk->move($path, $newPath);
//                    $disk->setVisibility($newPath, 'public');
//                    return $newPath;
//                }, $uploaded_files);
//                $data_request['files'] = json_encode(array_unique(array_filter($data_request['files'], function ($path) {
//                    return $path !== null;
//                })));
//            }
            $distributor->update($data_request);
            if ($distributor->icheck_ids) {
                $this->syncIcheckDistributor($distributor);
            }
            return redirect()->back()->with('success', 'Đã cập nhật thành công!');
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

    public function delete($id)
    {
        if (auth()->user()->cannot('GATEWAY-delDistributor')) {
            abort(403);
        }
        try {
            $distributor = Distributor::findOrFail($id);
            $deletedRows = ProductDistributor::where('distributor_id', $id)->delete();
            if ($distributor->icheck_ids != null && $distributor->icheck_ids != "[]") {
                $icheck_distributor_ids = json_decode($distributor->icheck_ids, true);
                for ($i = 0; $i < count($icheck_distributor_ids); $i++) {
                    $icheck_distributor = IcheckDistributor::find($icheck_distributor_ids[$i]);
                    if ($icheck_distributor) {
                        $deletedRows = IcheckProductDistributor::where([['distributor_id', $icheck_distributor->id]])->delete();
                        $icheck_distributor->delete();
                    }
                }
            }
            $distributor->delete();
            return redirect()->back()->with('success', 'Đã xoá thành công!');
        } catch (Exception $exception) {
            return view('errors.404');
        }

    }

//    public function approve($id)
//    {
//        try {
//            $distributor = Distributor::findOrFail($id);
//            $distributor->update(['status' => 1]);
//            if ($distributor->icheck_ids) {
//                $this->syncIcheckDistributor($distributor);
//            }
//            return redirect()->back()
//                ->with('success', 'Đã duyệt thành công!');
//        } catch (Exception $exception) {
//            return view('errors.404');
//        }
//    }

    public function syncIcheckDistributor($distributor)
    {
        $data = [];
        $data['name'] = $distributor->name;
        $data['address'] = $distributor->address;
        if ($distributor->phone) {
            $data['contact'] = $distributor->phone;
        }
        if ($distributor->website) {
            $data['site'] = $distributor->website;
        }
        $icheck_distributor_ids = json_decode($distributor->icheck_ids, true);
        for ($i = 0; $i < count($icheck_distributor_ids); $i++) {
            $icheck_distributor = IcheckDistributor::find($icheck_distributor_ids[$i]);
            //Information Distributor Sync with iCheck data
            if ($icheck_distributor) {
                $icheck_distributor->update($data);
            }
        }
    }

    public function ajaxSearch(Request $request)
    {
        if ($request->input('search')) {
            $search = $request->input('search');
            $distributors = Distributor::select("id", "name")
                ->where('name', 'like', '%' . $search . '%')
                ->get();

            $formatted_data = [];

            foreach ($distributors as $distributor) {
                $formatted_data[] = ['id' => $distributor->id, 'text' => $distributor->name];
            }
            $count_searched = $distributors->count();
            return response()->json(['results' => $formatted_data, 'count_searched' => $count_searched]);
        }
        return '[]';
    }
}
