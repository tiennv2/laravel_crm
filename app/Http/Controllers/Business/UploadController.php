<?php

namespace App\Http\Controllers\Business;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class UploadController extends Controller
{
    public function getUploadUrl(Request $request)
    {
        $typeExtMap = [
            'image/gif' => '.gif',
            'image/jpeg' => '.jpg',
            'image/png' => '.png',
            'application/pdf' => '.pdf',
            'application/msword' => '.doc',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document' => '.docx',
            'application/vnd.ms-excel' => '.xls',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' => '.xlsx',

        ];
        $allowedTypes = array_keys($typeExtMap);

        if ($request->hasFile('files')) {
            $files = $request->file('files');
            $fileNames = [];
            $disk = Storage::disk('gcs_business');
            foreach ($files as $file) {
                if ($file->getError() == 0) {
                    $type = $file->getClientMimeType();
                    if (!in_array($type, $allowedTypes)) {
                        abort(400);
                    }
                    $filename = trim($file->getClientOriginalName());
                    $now = Carbon::now();
                    $microtime = $now->timestamp . $now->micro;
                    $filename = $microtime . '_' . str_random(10) . '_' . $filename;
                    $path = $disk->putFileAs('tmp', $file, $filename, 'public');
                    $fileNames[] = $path;
                }
            }
            $full_paths = [];
            foreach ($fileNames as $fileName) {
                array_push($full_paths, [$fileName, $disk->url($fileName)]);
            }
            $full_paths = json_encode($full_paths);
            return response()->json([
                'full_paths' => $full_paths
            ]);
        }
        return response()->json([
            'error' => 'Upload fail!'
        ]);
    }
}
