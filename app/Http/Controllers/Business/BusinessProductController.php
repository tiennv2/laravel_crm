<?php

namespace App\Http\Controllers\Business;

use App\API\Api_business_gateway;
use App\API\Api_iCheck_backend;
use App\Events\Business\ApproveManagementRoleOfProductsEvent;
use App\Events\Business\ApproveProductsInfoEvent;
use App\Events\Business\DisapproveManagementRoleOfProductsEvent;
use App\Events\Business\DisapproveProductsInfoEvent;
use App\Exports\Business\BusinessExport;
use App\Http\Controllers\Controller;
use App\Models\Business\Business;
use App\Models\Business\Business_Product;
use App\Models\Business\Gln;
use App\Models\Icheck_newCMS\ActivityLog;
use App\Repository\Business\BusinessProductRepositoryInterface;
use App\Repository\Business\UserRepositoryInterface;
use App\Services\Business\BarcodeValidation;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class BusinessProductController extends Controller
{
    private $api_icheck_backend;
    private $barcodeValidation;
    private $userRepository;
    private $businessProductRepository;
    private $api_business_gateway;

    public function __construct(UserRepositoryInterface $userRepository, BusinessProductRepositoryInterface $businessProductRepository)
    {
        $this->api_icheck_backend = new Api_iCheck_backend();
        $this->barcodeValidation = new BarcodeValidation();
        $this->userRepository = $userRepository;
        $this->businessProductRepository = $businessProductRepository;
        $this->api_business_gateway = new Api_business_gateway();
    }

    public function index(Request $request)
    {
        $response = $this->businessProductRepository->getList($request);
        $products = $response['products'];
        $query = $response['query'];
        $count_record_total = $products->count();
        $businesses = Business::where('deleted_at', '=', null)->orderBy('id', 'desc')->get();
        $products = $products->orderBy('updated_at', 'desc')->paginate(20)->appends($query);
//        dd($products);
        $count_record_in_page = $products->count();
        $icheck_attributes = $this->api_icheck_backend->getAttributes();
        foreach ($products as $product) {
            $stdProduct = $this->businessProductRepository->getStdProduct($product->barcode);
            if ($product->certificates) {
                $paths = json_decode($product->certificates);
                $full_paths = [];
                foreach ($paths as $path) {
                    array_push($full_paths, [$path, Storage::disk('gcs_business')->url($path)]);
                }
                $product->certificates = json_encode($full_paths);
            }

            $product_info = json_decode($product->product_info, true);
            if ($product_info && array_key_exists("images", $product_info) and $product_info['images']) {
                $images = $product_info['images'];
                $baseUrl = sprintf('https://%s/%s', 'storage.googleapis.com', config('filesystems.disks.gcs_business.bucket'));
                $full_paths = [];
                foreach ($images as $image) {
                    $url = sprintf('%s/%s', $baseUrl, $image);
                    array_push($full_paths, $url);
                }
                $product_info['images'] = $full_paths;
            }

            if ($product_info && array_key_exists("certificate_images", $product_info) and $product_info['certificate_images']) {
                $certificate_images = $product_info['certificate_images'];
                $baseUrl = sprintf('https://%s/%s', 'storage.googleapis.com', config('filesystems.disks.gcs_business.bucket'));
                $full_paths = [];
                foreach ($certificate_images as $image) {
                    $url = sprintf('%s/%s', $baseUrl, $image);
                    array_push($full_paths, $url);
                }
                $product_info['certificate_images'] = $full_paths;
            }
            if ($product->management_status != Business_Product::MANAGEMENT_STATUS_APPROVED || ($product->info_status == Business_Product::INFO_STATUS_PENDING && !$stdProduct) || ($product->info_status == Business_Product::INFO_STATUS_APPROVED && $product_info)) {
                $attributes = [];
                if (isset($product_info['attributes']))
                    $attributes = $product_info['attributes'];
                if ($attributes && $icheck_attributes['data']) {
                    foreach ($attributes as $key => $value) {
                        foreach ($icheck_attributes['data'] as $icheck_attribute) {
                            if ($icheck_attribute['id'] == $key) {
                                $attributes[$icheck_attribute['title']] = $value;
                                unset($attributes[$key]);
                            }
                        }
                    }
                    $product_info['attributes'] = $attributes;
                }
            }

            $product->product_info = $product_info;

            //Get vendor_name
            $product->vendor_name = '';
            $stdProduct = $this->businessProductRepository->getStdProduct($product->barcode);
            if ($stdProduct && $stdProduct->gln_id) {
                $gln = Gln::find($stdProduct->gln_id);
                if ($gln) {
                    $product->vendor_name = $gln->name;
                }
            }

            //Validate barcode
            $check = $this->barcodeValidation->validateBarcode($product->barcode);
            if ($check) {
                $product->validBarcode = 1;
            } else {
                $product->validBarcode = 0;
            }
            //Get stdProduct
            $product->stdProduct = null;
            if ($product->info_status == Business_Product::INFO_STATUS_PENDING || $product->need_recheck == 1) {
                if ($stdProduct) {
                    //Product Images
                    if ($stdProduct->images == "[]" || $stdProduct->images == null) {
                        $stdProduct->images = null;
                    } else {
                        $images = json_decode($stdProduct->images, true);
                        $baseUrl = sprintf('https://%s/%s', 'storage.googleapis.com', config('filesystems.disks.gcs_business.bucket'));
                        $full_paths = [];
                        foreach ($images as $image) {
                            $url = sprintf('%s/%s', $baseUrl, $image);
                            array_push($full_paths, $url);
                        }
                        $stdProduct->images = $full_paths;
                    }
                    //Product Certificate Images
                    if ($stdProduct->certificate_images == "[]" || $stdProduct->certificate_images == null) {
                        $stdProduct->certificate_images = null;
                    } else {
                        $certificate_images = json_decode($stdProduct->certificate_images, true);
                        $baseUrl = sprintf('https://%s/%s', 'storage.googleapis.com', config('filesystems.disks.gcs_business.bucket'));
                        $full_paths = [];
                        foreach ($certificate_images as $image) {
                            $url = sprintf('%s/%s', $baseUrl, $image);
                            array_push($full_paths, $url);
                        }
                        $stdProduct->certificate_images = $full_paths;
                    }
                    if ($stdProduct->attributes == "[]" || $stdProduct->attributes == null) {
                        $stdProduct->attributes = null;
                    } else {
                        $stdProduct->attributes = json_decode($stdProduct->attributes, true);
                    }

                    $product->stdProduct = $stdProduct;
                }
            }

        }
        //Count waiting approve product info
        $waitingInfoApproveProduct_count = Business_Product::where([['business_id', '<>', null], ['info_status', Business_Product::INFO_STATUS_PENDING], ['management_status', Business_Product::MANAGEMENT_STATUS_APPROVED], ['management_type', Business_Product::MANAGEMENT_TYPE_FULL_CONTROL]])->count();
        //Count waiting approve product management role
        $waitingRoleApproveProduct_count = Business_Product::where('business_id', '<>', null)->whereIn('management_status', [Business_Product::MANAGEMENT_STATUS_PENDING, Business_Product::MANAGEMENT_STATUS_NEED_UPDATE_PROOF])->count();
        //Count waiting need_recheck product
        $needRecheck_product_count = Business_Product::where([['business_id', '<>', null], ['need_recheck_info', 1],])->count();
        return view('business.business_products.index', ['products' => $products, 'businesses' => $businesses, 'count_record_total' => $count_record_total,
            'count_record_in_page' => $count_record_in_page, 'waitingInfoApproveProduct_count' => $waitingInfoApproveProduct_count, 'needRecheck_product_count' => $needRecheck_product_count,
            'waitingRoleApproveProduct_count' => $waitingRoleApproveProduct_count, 'icheck_attributes' => $icheck_attributes['data'],
            'query' => $query]);
    }

//    public function index(Request $request)
//    {
//        $data = $this->api_business_gateway->getProductList(['limit' => 5, 'barcode' => '5868568785129']);
//        dd($data);
//    }

    public function approveProductInfo(Request $request)
    {
        $product_ids = $request->input('product_ids');
        $user_id = auth()->user()->id;
        event(new ApproveProductsInfoEvent($product_ids, $user_id));
        session(['AjaxRequestSuccess' => 'Hệ thống đang xử lý duyệt thông tin Sản phẩm!']);
    }

    public function disapproveProductInfo(Request $request)
    {
        $product_ids = $request->input('product_ids');
        $reason = $request->input('reason');
        $user_id = auth()->user()->id;
        event(new DisapproveProductsInfoEvent($product_ids, $reason, $user_id));
        return redirect()->back()
            ->with('warning', 'Hệ thống đang xử lý hủy duyệt thông tin Sản phẩm!');
    }

    public function approveManagementRoleOfProducts(Request $request)
    {
        $product_ids = $request->input('product_ids');
        $user_id = auth()->user()->id;
        event(new ApproveManagementRoleOfProductsEvent($product_ids, $verify = false, $user_id));
        session(['AjaxRequestSuccess' => 'Hệ thống đang xử lý duyệt quyền quản lý Sản phẩm!']);
    }

    public function disapproveManagementRoleOfProducts(Request $request)
    {
        $product_ids = $request->input('product_ids');
        $reason = $request->input('reason');
        $user_id = auth()->user()->id;
        event(new DisapproveManagementRoleOfProductsEvent($product_ids, $reason, $user_id));
        return redirect()->back()
            ->with('warning', 'Hệ thống đang xử lý hủy duyệt quyền quản lý Sản phẩm!');
    }

    public function disapproveManagementRoleOfAllProductsOfBusiness(Request $request)
    {
        if ($request->input('disapprove_business_id')) {
            $business_id = $request->input('disapprove_business_id');
            $product_ids = Business_Product::where([['business_id', '=', $business_id], ['management_status', '<>', Business_Product::MANAGEMENT_STATUS_DISAPPROVED]])->pluck('id')->toArray();
            $reason = '';
            if ($request->input('reason')) {
                $reason = $request->input('reason');
            }
            $user_id = auth()->user()->id;
            event(new DisapproveManagementRoleOfProductsEvent($product_ids, $reason, $user_id));
            return redirect()->back()
                ->with('warning', 'Hệ thống đang xử lý hủy duyệt quyền quản lý Sản phẩm!');
        }
        return redirect()->back()
            ->with('warning', 'Bạn chưa chọn Doanh nghiệp nào!');
    }

    public
    function excelExport(Request $request)
    {
        $response = $this->businessProductRepository->getList($request);
        $products = $response['products'];
        $data = $this->businessProductRepository->excelExport($products);
        return Excel::download(new BusinessExport($data), 'DS_SanPham.xlsx');
    }

    public function uploadFiles(Request $request)
    {
        if ($request->hasFile('files')) {
            $files = $request->file('files');
            $disk = Storage::disk('gcs_business');
            $fileNames = [];
            foreach ($files as $file) {
                if ($file->getError() == 0) {
                    $file_name = trim($file->getClientOriginalName());
//                        $file_name = preg_replace(" / [^a - zA - Z0 - 9.]/", "_", $file_name);
                    $path = $disk->putFileAs('products/certificates', $file, $file_name, 'public');
                    $fileNames[] = $path;
                }
            }
            $full_paths = [];
            foreach ($fileNames as $fileName) {
                array_push($full_paths, [$fileName, $disk->url($fileName)]);

            }
            $full_paths = json_encode($full_paths);
            return response()->json([
                'full_paths' => $full_paths
            ]);
        }
        return response()->json([
            'error' => 'Upload fail!',
        ]);

    }

    public function updateCertificates(Request $request)
    {
        if (auth()->user()->cannot('GATEWAY-updateProductCertificate')) {
            abort(403);
        }
        try {
            $data = $request->only('uploaded_files', 'product_id');
            $product = Business_Product::findOrFail($data['product_id']);
            if ($request->has('uploaded_files')) {
                $uploaded_files = $request->input('uploaded_files');
                $businessId = $product->business_id;
                $businessHash = hash_hmac('sha1', sprintf('b:%s', $businessId), 'unkn0wn');
                $businessHash = sprintf('%s-%s', substr($businessHash, 0, 10), substr($businessHash, 10, 10));
                $businessBasePath = 'businesses/' . $businessHash;
                $disk = Storage::disk('gcs_business');
                $data['files'] = array_map(function ($path) use ($businessBasePath, $disk) {
                    if (starts_with($path, $businessBasePath . '/')) {
                        return $path;
                    }
                    if (!$disk->exists($path)) {
                        return null;
                    }

                    $filename = basename($path);
                    $newPath = sprintf('%s/product_certificates/%s', $businessBasePath, $filename);
                    $disk->move($path, $newPath);
                    $disk->setVisibility($newPath, 'public');
                    return $newPath;
                }, $uploaded_files);
                $data['files'] = json_encode(array_unique(array_filter($data['files'], function ($path) {
                    return $path !== null;
                })));
                $product->certificates = $data['files'];
                $product->save();

            } else {
                $product->certificates = null;
                $product->save();
            }
            //Activity_log
            ActivityLog::create([
                "description" => "Update Product Certificates",
                "subject_id" => $product->id,
                "subject_type" => "App\Models\Business\Business_Product",
                "causer_id" => auth()->user()->id,
                "causer_type" => "users"
            ]);
            return redirect()->back()
                ->with('success', "Đã cập nhật giấy tờ cho sản phẩm có Id = $product->id");

        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

    public function setFullControlRole($id)
    {
        $product = Business_Product::findOrFail($id);
        $business_name = $this->businessProductRepository->getBusinessNameForProduct($id);
        $check = Business_Product::where([['id', '<>', $id], ['barcode', $product->barcode], ['management_status', Business_Product::MANAGEMENT_STATUS_APPROVED], ['management_type', Business_Product::MANAGEMENT_TYPE_FULL_CONTROL]])->first();
        if ($check) {
            return redirect()->action('Business\BusinessProductController@index')->with('warning', "Không thể cập nhật trạng thái toàn quyền cho Doanh nghiệp '$business_name' đối với mã sản phẩm '$product->barcode'");
        } else {
            $product->management_type = Business_Product::MANAGEMENT_TYPE_FULL_CONTROL;
            $product->save();
            //Activity_log
            ActivityLog::create([
                "description" => "Update Product Full Control Role Of Business",
                "subject_id" => $id,
                "subject_type" => "App\Models\Business\Business_Product",
                "causer_id" => auth()->user()->id,
                "causer_type" => "users"
            ]);
            return redirect()->action('Business\BusinessProductController@index')->with('success', "Đã cập nhật trạng thái toàn quyền cho Doanh nghiệp '$business_name' đối với mã sản phẩm '$product->barcode'");
        }
    }

    public function setDistributorRole($id)
    {
        $product = Business_Product::findOrFail($id);
        $product->management_type = Business_Product::MANAGEMENT_TYPE_DISTRIBUTOR;
        $product->save();
        $business_name = $this->businessProductRepository->getBusinessNameForProduct($id);
        //Activity_log
        ActivityLog::create([
            "description" => "Update Product Distributor Role Of Business",
            "subject_id" => $id,
            "subject_type" => "App\Models\Business\Business_Product",
            "causer_id" => auth()->user()->id,
            "causer_type" => "users"
        ]);
        return redirect()->action('Business\BusinessProductController@index')->with('success', "Đã cập nhật vai trò Nhà phân phối cho Doanh nghiệp '$business_name' đối với mã sản phẩm '$product->barcode'");
    }

}
