<?php

namespace App\Http\Controllers\Collaborator;

use App\Models\Collaborator\Collaborator;
use App\Models\Collaborator\Collaborator_Group;
use App\Models\Collaborator\Group;
use App\Repository\Collaborator\CollaboratorRepositoryInterface;
use App\User;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CollaboratorController extends Controller
{
    private $collaboratorRepository;

    public function __construct(CollaboratorRepositoryInterface $collaboratorRepository)
    {
        $this->collaboratorRepository = $collaboratorRepository;
    }

    public function index(Request $request, Collaborator $collaborator)
    {
        if (auth()->user()->cannot('COLLABORATOR-addCollaborator') && auth()->user()->cannot('COLLABORATOR-viewCollaboratorList')) {
            abort(403);
        }
//        try{
        $collaborator = $collaborator->newQuery();
        $query = [];
        $collaborator->select('collaborators.*');
        if ($request->input('collaborator_id')) {
            $id = $request->input('collaborator_id');
            $collaborator->where('id', $id);
            $query['collaborator_id'] = $id;
        }
        $collaborator_total = $this->collaboratorRepository->getAll();
        $collaborators = $collaborator->orderBy('id', 'desc')->paginate(20)->appends($query);
        foreach ($collaborators as $collaborator) {
            $collaborator->name = '';
            $user = User::where("icheck_id", $collaborator->icheck_id)->first();
            if($user) {
                $collaborator->name = $user->name;
            }
            $group_ids = Collaborator_Group::where("collaborator_id", $collaborator->id)->pluck("group_id")->toArray();
            $group_names = Group::whereIn("id", $group_ids)->pluck("name")->toArray();
            $collaborator->group_name = implode("; ", $group_names);
        }
        return view('collaborator.collaborator_list.index', ['collaborators' => $collaborators, 'collaborator_total' => $collaborator_total]);
//        } catch (Exception $exception) {
//            return view('errors.404');
//        }
    }

    public function add()
    {
        if (auth()->user()->cannot('COLLABORATOR-addCollaborator')) {
            abort(403);
        }
        $groups = Group::all();
        $icheckUsers = User::all();
        return view('collaborator.collaborator_list.form', ["icheckUsers" => $icheckUsers, "groups" => $groups]);
    }

    public function edit($id)
    {
//        try {
        if (auth()->user()->cannot('COLLABORATOR-updateCollaborator')) {
            abort(403);
        }
        $collaborator = Collaborator::findOrFail($id);
        $icheckUsers = User::all();
        $group_ids = Collaborator_Group::where("collaborator_id", $collaborator->id)->pluck("group_id")->toArray();
        if (count($group_ids) > 0) {
            $collaborator->group_ids = $group_ids;
        } else {
            $collaborator->group_ids = [];
        }

        $groups = Group::all();
        return view('collaborator.collaborator_list.form', ["collaborator" => $collaborator, "groups" => $groups, "icheckUsers" => $icheckUsers]);
//        } catch (Exception $exception) {
//            return view('errors.404');
//        }
    }

    public function store(Request $request)
    {
        if (auth()->user()->cannot('COLLABORATOR-addCollaborator')) {
            abort(403);
        }
        //        try {
//        dd($request->all());
        $this->validate($request, [
            'icheck_id' => 'required|unique:icheck_collaborator.collaborators',
            'email' => 'email|required|unique:icheck_collaborator.collaborators'
        ],
            [
                'icheck_id.required' => 'Bạn chưa chọn Cộng tác viên!',
                'email.required' => 'Bạn chưa nhập email Cộng tác viên!',
                'icheck_id.unique' => 'Cộng tác viên đã tồn tại!',
                'email.unique' => 'Email đã tồn tại!',
            ]);
        $data = $request->except(["_token", "group_ids"]);
        $new_collaborator = Collaborator::create($data);
        if ($new_collaborator->id && $request->input("group_ids")) {
            $group_ids = $request->input("group_ids");
            foreach ($group_ids as $group_id) {
                Collaborator_Group::create(["collaborator_id" => $new_collaborator->id, "group_id" => $group_id]);
            }
        }
        return redirect()->action("Collaborator\CollaboratorController@index")->with("success", "Đã thêm mới thành công!");
        //        } catch (Exception $exception) {
//            return view('errors.404');
//        }
    }


    public function update($id, Request $request)
    {
        if (auth()->user()->cannot('COLLABORATOR-updateCollaborator')) {
            abort(403);
        }
        //        try {
        $this->validate($request, [
            'icheck_id' => 'unique:icheck_collaborator.collaborators,icheck_id,' . $id,
            'email' => 'required|unique:icheck_collaborator.collaborators,email,' . $id,
        ],
            [
                'email.required' => 'Bạn chưa nhập email Cộng tác viên!',
                'icheck_id.unique' => 'Cộng tác viên đã tồn tại!',
                'email.unique' => 'Email đã tồn tại!',
            ]);
        $data = $request->only(["email"]);
        Collaborator::where("id", $id)->update($data);
        Collaborator_Group::where("collaborator_id", $id)->delete();
        if ($request->input("group_ids")) {
            $group_ids = $request->input("group_ids");
            foreach ($group_ids as $group_id) {
                Collaborator_Group::updateOrCreate(["collaborator_id" => $id, "group_id" => $group_id]);
            }
        }
        return redirect()->back()->with("success", "Đã cập nhật thành công!");
        //        } catch (Exception $exception) {
//            return view('errors.404');
//        }
    }

    public static function getCollaboratorById($id)
    {
        $collaborator = Collaborator::find($id);
        if ($collaborator) {
            $iCheckUsers = User::all();
            foreach ($iCheckUsers as $user) {
                if ($collaborator->icheck_id == $user->icheck_id) {
                    $collaborator->name = $user->name;
                }
            }
            return $collaborator;
        }

        return $collaborator;
    }
}
