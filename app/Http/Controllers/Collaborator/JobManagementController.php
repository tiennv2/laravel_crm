<?php

namespace App\Http\Controllers\Collaborator;

use App\Jobs\Collaborator\GTIN_Filter;
use App\Models\Collaborator\Group;
use App\Models\Collaborator\Job;
use App\Models\Collaborator\Need_Update_Product;
use App\Models\Collaborator\Transaction;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class JobManagementController extends Controller
{

    public function index(Request $request)
    {
        if (auth()->user()->cannot('COLLABORATOR-viewJobs') && auth()->user()->cannot('COLLABORATOR-addJob')) {
            abort(403);
        }

//        $transactions = Transaction::groupBy('collaborator_id')
//            ->selectRaw('sum(amount) as sum, collaborator_id')
//            ->whereBetween("created_at", ["2019-01-14 00:00:00", "2019-01-31 23:59:59"])
//            ->where("type", 1)
//            ->pluck("sum","collaborator_id")
//            ->toArray();
//        $transactions = array_unique($transactions);
//        if ($transactions) {
//            foreach ($transactions as $key => $value) {
//                DB::connection('icheck_collaborator')->beginTransaction();
//                try {
//                    $collaborator = Collaborator::findOrFail($key);
//                    //Create record in Transaction table
//                    Transaction::create([
//                        'collaborator_id' => $collaborator->id,
//                        'amount' => $value,
////                            'ref_id' => ,
//                        'type' => Transaction::TYPE_WITHDRAW,
//                        'status' => 1
//                    ]);
//                    //Create money transfer
//                    $api_collaborator_transfer = new Api_collaborator_transfers();
//                    $response = $api_collaborator_transfer->transfer([
//                        "description" => "Thanh toan tien dong gop thong tin San pham iCheck duoc duyet tu ngay 1-1-2019 den 15-1-2019",
//                        "amount" => $value * 100,
//                        "recipient" => $collaborator->icheck_id,
//                        "metadata" => []
//                    ]);
//                    if ($response['status'] != 200) {
//                        dd($response);
//                        DB::connection('icheck_collaborator')->rollBack();
//                    }
//                    $collaborator->update(["balance" => 0]);
//                    DB::connection('icheck_collaborator')->commit();
//                    // endTransaction
//                } catch (\Exception $e) {
//                    DB::connection('icheck_collaborator')->rollBack();
//                    echo $e->getMessage();
//                }
//            }
//        }


//        try{
        $jobs = Job::select("id", "filters", "need_update", "group_ids", "status", "search_cost", "scheduled_at", "created_at")->orderBy('id', 'desc')->paginate(20);
//        dd($jobs);
        foreach ($jobs as $job) {
            //
            $job->need_update_name = '';
            $collection = collect([]);
            foreach (Need_Update_Product::$infoTexts as $key => $value) {
                if ($job->need_update & $key) {
                    $collection->push($value);
                }
            }
            $job->need_update_name = implode(" - ", $collection->all());
            //
            if ($job->status == Job::PENDING_STATUS) {
                $job->status_name = Job::$statusTexts[Job::PENDING_STATUS];
            }
            if ($job->status == Job::GOT_GTINS_STATUS) {
                $job->status_name = Job::$statusTexts[Job::GOT_GTINS_STATUS];
            }
            if ($job->status == Job::FAIL_GOT_GTINS_STATUS) {
                $job->status_name = Job::$statusTexts[Job::FAIL_GOT_GTINS_STATUS];
            }
            if ($job->status == Job::POURING_GTINS_STATUS) {
                $job->status_name = Job::$statusTexts[Job::POURING_GTINS_STATUS];
            }
            if ($job->status == Job::POURED_GTINS_STATUS) {
                $job->status_name = Job::$statusTexts[Job::POURED_GTINS_STATUS];
            }
            if ($job->status == Job::FAIL_POURED_GTINS_STATUS) {
                $job->status_name = Job::$statusTexts[Job::FAIL_POURED_GTINS_STATUS];
            }
            //
            $filters_content = [];
            if ($job->filters) {
                if (array_key_exists("non_exist", $job->filters)) {
                    array_push($filters_content, "Chưa có trên hệ thống");
                }
                if (array_key_exists("non_name", $job->filters)) {
                    array_push($filters_content, "Chưa có tên");
                }
                if (array_key_exists("non_images", $job->filters)) {
                    array_push($filters_content, "Chưa có ảnh");
                }
                if (array_key_exists("non_gln", $job->filters)) {
                    array_push($filters_content, "Chưa có thông tin Nhà sản xuất");
                }
                if (array_key_exists("non_categories", $job->filters)) {
                    array_push($filters_content, "Chưa có danh mục");
                }
                if (array_key_exists("non_descriptions", $job->filters)) {
                    array_push($filters_content, "Chưa có mô tả");
                }
                if (array_key_exists("glns", $job->filters)) {
                    $glns = implode(",", $job->filters['glns']);
                    array_push($filters_content, "Sản phẩm của Nhà sản xuất có mã GLN ($glns)");
                }
                if (count($filters_content) > 0) {
                    $job->filters_content = implode(", ", $filters_content);
                }
            }

            //
            $group_names = Group::whereIn("id", json_decode($job->group_ids))->pluck("name")->toArray();
            $job->group_names = implode("; ", $group_names);
        }
//            dd($jobs);

        return view('collaborator.job.index', ['jobs' => $jobs]);
//        } catch (Exception $exception) {
//            return view('errors.404');
//        }
    }

    public
    function add()
    {
        if (auth()->user()->cannot('COLLABORATOR-addJob')) {
            abort(403);
        }
        $groups = Group::all();
        return view('collaborator.job.form', ["groups" => $groups]);
    }

    public
    function store(Request $request)
    {
        if (auth()->user()->cannot('COLLABORATOR-addJob')) {
            abort(403);
        }
//        dd($request->all());
        //Create Filter
        $filters = [];
        if ($request->input('non_exist')) {
            $filters = array_merge($filters, array("non_exist" => 1));
        }
        if ($request->input('non_name')) {
            $filters = array_merge($filters, array("non_name" => 1));
        }
        if ($request->input('non_images')) {
            $filters = array_merge($filters, array("non_images" => 1));
        }
        if ($request->input('non_descriptions')) {
            $filters = array_merge($filters, array("non_descriptions" => 1));
        }
        if ($request->input('non_categories')) {
            $filters = array_merge($filters, array("non_categories" => 1));
        }
        if ($request->input('non_gln')) {
            $filters = array_merge($filters, array("non_gln" => 1));
        }

//        if ($request->input('categories')) {
//            var_dump($request->input('categories'));
//            $categories_array = array_map("intval", array_filter($request->input('categories')));
//            $categories = end($categories_array);
//            if ($categories) {
//                $filters = array_merge($filters, array("categories" => $categories));
//            }
//        }

        if ($request->input('glns')) {
            $glns = array_unique(explode("\r\n", trim($request->input('glns'))));
            $filters = array_merge($filters, array("glns" => $glns));
        }


        //Need_update
        $need_update = null;
        if ($request->input("need_update")) {
            $need_update = array_reduce($request->input("need_update"), function ($a, $b) {
                return $a | $b;
            }, 0);
        }
        //Need_search
        $need_search = 0;
        if ($request->input("need_search") == 1) {
            $need_search = 1;
        }
        //Groups
        $group_ids = null;
        if ($request->input("group_ids")) {
            $group_ids = json_encode(array_map("intval", $request->input("group_ids")));
        }
        //Schedule
        $scheduled_at = null;
        if ($request->input("scheduled_at")) {
            $scheduled_at = $request->input("scheduled_at") . " 00:00:59";
        }

        if ($request->input("immediately_start")) {
            $scheduled_at = date("Y-m-d H:i:s");
        }

        $job = Job::create(["filters" => $filters, "need_update" => $need_update, "group_ids" => $group_ids, "status" => Job::PENDING_STATUS, "scheduled_at" => $scheduled_at, "need_search" => $need_search]);
        if ($job->id) {
            GTIN_Filter::dispatch($filters, $job->id, $scheduled_at)
                ->delay(now()->addSeconds(10));
        }

        return redirect()->action("Collaborator\JobManagementController@index")->with("success", "Đã thêm mới công việc thành công!");
    }

}
