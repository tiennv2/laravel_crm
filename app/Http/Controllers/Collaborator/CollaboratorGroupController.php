<?php

namespace App\Http\Controllers\Collaborator;

use App\Models\Collaborator\Collaborator;
use App\Models\Collaborator\Group;
use App\Models\Collaborator\Collaborator_Group;
use App\Models\Collaborator\Need_Update_Product;
use App\Repository\Collaborator\CollaboratorRepositoryInterface;
use App\User;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CollaboratorGroupController extends Controller
{
    private $collaboratorRepository;

    public function __construct(CollaboratorRepositoryInterface $collaboratorRepository)
    {
        $this->collaboratorRepository = $collaboratorRepository;
    }

    public function index(Request $request)
    {
        if (auth()->user()->cannot('COLLABORATOR-viewCollaboratorGroups') && auth()->user()->cannot('COLLABORATOR-addNewCollaboratorGroup')) {
            abort(403);
        }
//        try{
        $groups = Group::orderBy('id')->paginate(20);
        foreach ($groups as $group) {
            $group_members = [];
            $collaborator_ids = Collaborator_Group::where("group_id", $group->id)->pluck("collaborator_id")->toArray();
            if (!empty($collaborator_ids)) {
                foreach ($collaborator_ids as $collaborator_id) {
                    $collaborator_name = $this->collaboratorRepository->getCollaboratorName($collaborator_id);
                    array_push($group_members, $collaborator_name);
                }
                $group_members = implode(", ", $group_members);
                $group->group_members = $group_members;
            } else {
                $group->group_members = "";
            }
        }
        return view('collaborator.collaborator_group.index', ['groups' => $groups]);
//        } catch (Exception $exception) {
//            return view('errors.404');
//        }
    }

    public function add()
    {
        if (auth()->user()->cannot('COLLABORATOR-addNewCollaboratorGroup')) {
            abort(403);
        }
//        try {
        $icheckUsers = User::all();
        return view('collaborator.collaborator_group.form', ["icheckUsers" => $icheckUsers]);
//        } catch (Exception $exception) {
//            return view('errors.404');
//        }
    }

    public function store(Request $request)
    {
        if (auth()->user()->cannot('COLLABORATOR-addNewCollaboratorGroup')) {
            abort(403);
        }
        //        try {
//        dd($request->all());
        $data = $request->except(["_token", "icheck_ids"]);
        $new_group = Group::create($data);
        if ($new_group->id && $request->input("icheck_ids")) {
            $icheck_ids = $request->input("icheck_ids");
            foreach ($icheck_ids as $icheck_id) {
                $collaborator = Collaborator::firstOrCreate(["icheck_id" => $icheck_id]);
                Collaborator_Group::create(["collaborator_id" => $collaborator->id, "group_id" => $new_group->id]);
            }
        }
        return redirect()->action("Collaborator\CollaboratorGroupController@index");
        //        } catch (Exception $exception) {
//            return view('errors.404');
//        }
    }

    public function edit($id)
    {
        if (auth()->user()->cannot('COLLABORATOR-updateCollaboratorGroup')) {
            abort(403);
        }
//        try {
        $icheckUsers = User::all();
        $group = Group::findOrFail($id);
        $collaborator_ids = Collaborator_Group::where("group_id", $id)->pluck("collaborator_id")->toArray();
        $icheck_ids = Collaborator::whereIn("id", $collaborator_ids)->pluck("icheck_id")->toArray();
        $group->icheck_ids = $icheck_ids;
        return view('collaborator.collaborator_group.form', ["group" => $group, "icheckUsers" => $icheckUsers]);
//        } catch (Exception $exception) {
//            return view('errors.404');
//        }
    }

    public function update($id, Request $request)
    {
        if (auth()->user()->cannot('COLLABORATOR-updateCollaboratorGroup')) {
            abort(403);
        }
        //        try {

        $data = $request->except(["_token", "_method", "icheck_ids"]);
        $group = Group::findOrFail($id);
        $group->update($data);
        if ($request->input("icheck_ids")) {
            Collaborator_Group::where("group_id", $id)->delete();
            $icheck_ids = $request->input("icheck_ids");
            foreach ($icheck_ids as $icheck_id) {
                $collaborator = Collaborator::firstOrCreate(["icheck_id" => $icheck_id]);
                Collaborator_Group::updateOrCreate(["collaborator_id" => $collaborator->id, "group_id" => $id]);
            }
        } else {
            Collaborator_Group::where("group_id",$id)->delete();
        }
        return redirect()->action("Collaborator\CollaboratorGroupController@index")->with("success", "Đã cập nhật thành công!");
        //        } catch (Exception $exception) {
//            return view('errors.404');
//        }
    }

//    public function delete($id)
//    {
//        $group = Group::findOrFail($id);
//        $group->delete();
//        Collaborator_Group::where("group_id", $id)->delete();
//        Need_Update_Product::where("group_id", $id)->update(["group_id" => null]);
//        return redirect()->action("Collaborator\CollaboratorGroupController@index")->with("success", "Đã xóa thành công!");
//    }
}
