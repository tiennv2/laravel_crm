<?php

namespace App\Http\Controllers\Collaborator;

use App\Http\Controllers\Controller;
use App\Models\Collaborator\Product_Category;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use GuzzleHttp\Exception\RequestException;

class ProductCategoryController extends Controller
{
    public function index()
    {
        if (auth()->user()->cannot('COLLABORATOR-viewProductCategory') && auth()->user()->cannot('COLLABORATOR-addProductCategory')) {
            abort(403);
        }
//        try {
            return view('collaborator.product_category.index');
//        } catch (Exception $exception) {
//            return view('errors.404');
//        }
    }

    public function getRootCategories()
    {
        try {
            $categories = Product_Category::where('parent_id', null)->get();
            return $categories->toJson();
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

    public function getSubCategories($id)
    {
        try {
            $categories = Product_Category::where('parent_id', $id)->get();
            return $categories->toJson();
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

    public static function recursive($data, $parent = null, $level = 0)
    {
        $list = [];
        if (isset($data[$parent])) {
            foreach ($data[$parent] as $cat) {

                $cat->level = $level;
                $list[] = $cat;

                foreach (static::recursive($data, $cat['id'], $level + 1) as $subCat) {
                    $list[] = $subCat;
                }
            }
        }

        return $list;
    }

    public function add()
    {
        if (auth()->user()->cannot('COLLABORATOR-addProductCategory')) {
            abort(403);
        }
        try {
            $category_list = Product_Category::all()->groupBy('parent_id');
            $category_list = $this->recursive($category_list, null);
            return view('collaborator.product_category.form', array('category_list' => $category_list));
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

    public function store(Request $request)
    {
        if (auth()->user()->cannot('COLLABORATOR-addProductCategory')) {
            abort(403);
        }
        $this->validate($request, [
            'name' => 'required',

        ]);
        try {
            $category = new Product_Category();
            $category->name = $request->input('name');
            $category->slug = str_slug($request->input('name'));
            $category->parent_id = $request->input('parent_id');
            //upload image
            $path = "upload";
            if ($request->hasFile('image')) {
                $file = $request->file('image');
                $filename = $file->getClientOriginalName();
                $file->move($path, $filename);
                $client = new Client();
                try {
                    $res = $client->request(
                        'POST',
                        'https://upload.icheck.com.vn/v1/images?uploadType=multipart&responseType=json',
                        [
                            'multipart' => [
                                ['name' => 'file', 'contents' => fopen("upload/$filename", 'r')]
                            ],
                        ]
                    );
                    $res = json_decode((string)$res->getBody());
                } catch (RequestException $e) {
                    return $e->getResponse()->getBody();
                }

                $category->image = $res->size->thumb_medium;
                unlink($path . "/" . $filename);
            }
            $category->save();

            return redirect()->route('Collaborator::product_category@index')
                ->with('success', 'Đã thêm danh mục mới thành công');
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

    public function edit($id)
    {
        if (auth()->user()->cannot('COLLABORATOR-updateProductCategory')) {
            abort(403);
        }
        try {
            $cat = Product_Category::findOrFail($id);
            $category_list = Product_Category::all()->groupBy('parent_id');
            $category_list = $this->recursive($category_list, null);
            return view('collaborator.product_category.form', compact('category_list', 'cat'));
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }


    public function update($id, Request $request)
    {
        if (auth()->user()->cannot('COLLABORATOR-updateProductCategory')) {
            abort(403);
        }
        $this->validate($request, [
            'name' => 'required',

        ]);
        try {
            $category = Product_Category::findOrFail($id);
            $category->name = $request->input('name');
            $category->slug = str_slug($request->input('name'));
            $category->parent_id = $request->input('parent_id');
            //upload image
            $path = "upload";
            if ($request->hasFile('image')) {
                $file = $request->file('image');
                $filename = $file->getClientOriginalName();
                $file->move($path, $filename);
                $client = new Client();
                try {
                    $res = $client->request(
                        'POST',
                        'https://upload.icheck.com.vn/v1/images?uploadType=multipart&responseType=json',
                        [
                            'multipart' => [
                                ['name' => 'file', 'contents' => fopen("upload/$filename", 'r')]
                            ],
                        ]
                    );
                    $res = json_decode((string)$res->getBody());
                } catch (RequestException $e) {
                    return $e->getResponse()->getBody();
                }

                $category->image = $res->size->thumb_medium;
                unlink($path . "/" . $filename);
            }
            $category->save();

            return redirect()->route('Collaborator::product_category@index')
                ->with('success', 'Đã cập nhật danh mục thành công!');
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

    public function delete($id)
    {
        if (auth()->user()->cannot('COLLABORATOR-delProductCategory')) {
            abort(403);
        }
        try {
            $category = Product_Category::findOrFail($id);
            $category->delete();

            return redirect()->route('Collaborator::product_category@index')->with('success', 'Đã xoá thành công');
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

}
