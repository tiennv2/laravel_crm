<?php

namespace App\Http\Controllers\Collaborator;

use App\API\Api_collaborator_transfers;
use App\API\Api_iCheck_backend;
use App\Events\ProductContributedInfoApprove;
use App\Models\Business\IcheckCountry;
use App\Models\Business\Product;
use App\Models\Collaborator\Collaborator;
use App\Models\Collaborator\Contributed_Information;
use App\Models\Collaborator\Gtin_Group;
use App\Models\Collaborator\Need_Update_Product;
use App\Models\Collaborator\Product_Category;
use App\Models\Legacy_Business\Business_Product;
use App\Repository\Collaborator\CollaboratorRepositoryInterface;
use App\User;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class Contributed_InformationController extends Controller
{
    private $collaboratorRepository;
//    private $api_collaborator_transfer;
    private $api_icheck_backend;

    public function __construct(CollaboratorRepositoryInterface $collaboratorRepository)
    {
        $this->collaboratorRepository = $collaboratorRepository;
//        $this->api_collaborator_transfer = $api_collaborator_transfer;
        $this->api_icheck_backend = new Api_iCheck_backend();
    }

    public function index(Request $request, Contributed_Information $contributed_Information)
    {

        if (auth()->user()->cannot('COLLABORATOR-viewContributedInfo')) {
            abort(403);
        }
//        try{
        $contributed_Information = $contributed_Information->newQuery();
        $query = [];
        $contributed_Information->select('contributed_informations.*');

        if ($request->input('gtin')) {
            $gtin = $request->input('gtin');
            $contributed_Information->where('gtin', 'like', '%' . $gtin . '%');
            $query['gtin'] = $gtin;
        }

        if ($request->input('collaborator_id')) {
            $contributed_by = $request->input('collaborator_id');
            $contributed_Information->where('contributed_by', $contributed_by);
            $query['collaborator_id'] = $contributed_by;
        }

        if ($request->input('contributed_informations')) {
            $contributed_informations = array_sum($request->input('contributed_informations'));
            $contributed_Information->where('contributed_informations', $contributed_informations);
            $query['contributed_informations'] = $request->input('contributed_informations');
        }

        if ($request->input('contributed_from')) {
            $contributed_from = $request->input('contributed_from');
            $contributed_Information->where([['contributed_informations.contributed_at', '>=', date('Y-m-d' . ' 00:00:00', strtotime($contributed_from))]]);
            $query['contributed_from'] = $contributed_from;
        }
        if ($request->input('contributed_to')) {
            $contributed_to = $request->input('contributed_to');
            $contributed_Information->where([['contributed_informations.contributed_at', '<=', date('Y-m-d' . ' 23:59:59', strtotime($contributed_to))]]);
            $query['contributed_to'] = $contributed_to;
        }

        if ($request->input('approved_from')) {
            $approved_from = $request->input('approved_from');
            $contributed_Information->where([['contributed_informations.approved_at', '>=', date('Y-m-d' . ' 00:00:00', strtotime($approved_from))]]);
            $query['approved_from'] = $approved_from;
        }
        if ($request->input('approved_to')) {
            $approved_to = $request->input('approved_to');
            $contributed_Information->where([['contributed_informations.approved_at', '<=', date('Y-m-d' . ' 23:59:59', strtotime($approved_to))]]);
            $query['approved_to'] = $approved_to;
        }
        if ($request->input('status') != null) {
            $status = $request->input('status');
            $contributed_Information->where('status', $status);
            $query['status'] = $status;
        }

        $count_record_total = $contributed_Information->count();
//        dd($count_record_total);
//        $contributed_Information_ids = $contributed_Information->pluck('id')->toArray();
        $a = $contributed_Information;
        $contributed_informations = $contributed_Information->orderBy('id', 'desc')->paginate(50)->appends($query);;
//        dd($contributed_informations);
        $approved_amount_total = $a->where("status",1)->sum("profits");
        $count_record_in_page = $contributed_informations->count();
        $collaborators = $this->collaboratorRepository->getAll();
        foreach ($contributed_informations as $contributed_information) {
            $contributed_information->contributor_name = "";
            if ($contributed_information->contributed_by) {
                $collaborator = Collaborator::find($contributed_information->contributed_by);
                $user = User::where("icheck_id", $collaborator->icheck_id)->first();
                $contributed_information->contributor_name = $user->name;
            }
            //
            $contributed_information->contributed_informations_name = '';
            $collection = collect([]);
            foreach (Need_Update_Product::$infoTexts as $key => $value) {
                if ($contributed_information->contributed_informations & $key) {
                    $collection->push($value);
                }
                $contributed_information->contributed_informations_name = implode(" - ", $collection->all());
//                $contributed_information->contributed_informations_name = implode(" - ",$contributed_information->contributed_informations_name);
            }
            //
            if ($contributed_information->contributed_data) {
                $data = $contributed_information->contributed_data;
                if (array_key_exists("categories", $data)) {
                    $category_names = [];
                    foreach ($data['categories'] as $id) {
                        $category = Product_Category::find($id);
                        array_push($category_names, $category->name);
                    }
                    $data['categories'] = implode(" - ", $category_names);
                } else {
                    $data['categories'] = '';
                }
                if (!array_key_exists("name", $data)) {
                    $data['name'] = '';
                }
                if (!array_key_exists("price", $data)) {
                    $data['price'] = '';
                }
                if (!array_key_exists("description", $data)) {
                    $data['description'] = '';
                }
                if (array_key_exists("glnInfo", $data)) {
                    if (array_key_exists("country", $data['glnInfo'])) {
                        $country = IcheckCountry::where("alpha_2", $data['glnInfo']['country'])->first();
                        $data['glnInfo']['country'] = $country->name;
                    }
                }

                $contributed_information->contributed_data = $data;
            }
            //Get iCheck_info
            $res = $this->api_icheck_backend->getProduct($contributed_information->gtin);
            if ($res['status'] == 200) {
                $icheck_product = $res['data'];
                $descriptions = "";
                if (!empty($icheck_product['attributes'])) {
                    foreach ($icheck_product['attributes'] as $attribute) {
                        if ($attribute['attribute_id'] == 1) {
                            $descriptions = $attribute['content'];
                        }
                    }
                }
                $icheck_info = [
                    "product_name" => $icheck_product['product_name'],
                    "image" => $icheck_product['image_default'],
                    "price" => $icheck_product['price_default'],
                    "descriptions" => $descriptions
                ];
                $contributed_information->icheck_info = $icheck_info;
                if ($icheck_product['vendor_id']) {
                    $response = $this->api_icheck_backend->getVendorById($icheck_product['vendor_id']);
                    if ($response['status'] == 200) {
                        $country_name = '';
                        if (array_key_exists("country", $response['data'])) {
                            $country_name = $response['data']['country']['name'];
                        }
                        $icheck_vendor = [
                            "gln" => $response['data']['gln_code'],
                            "name" => $response['data']['name'],
                            "email" => $response['data']['email'],
                            "phone" => $response['data']['phone'],
                            "address" => $response['data']['address'],
                            "country" => $country_name,
                            "website" => $response['data']['website'],
                            "businessPrefix" => $response['data']['prefix']
                        ];
                        $contributed_information->icheck_vendor = $icheck_vendor;
                    } else {
                        $contributed_information->icheck_vendor = null;
                    }
                } else {
                    $contributed_information->icheck_vendor = null;
                }
            } else {
                $contributed_information->icheck_info = [
                    "product_name" => '',
                    "image" => '',
                    "price" => '',
                    "descriptions" => ''
                ];
                $contributed_information->icheck_vendor = null;
            }
        }
        //Add approved_amount_total
//        $approved_amount_total = DB::connection('icheck_collaborator')->table('contributed_informations')
//            ->select('profits')
//            ->whereIn('id', $contributed_Information_ids)
//            ->where('status', 1)
//            ->sum('profits');
//        $approved_amount_total = $contributed_informations->where("status",1)->sum("profits");
        //Count waiting approve product
        $waiting_product_count = Contributed_Information::where([['status', 0]])->count();
        return view('collaborator.contributed_information.index', ['contributed_informations' => $contributed_informations, 'count_record_total' => $count_record_total,
            'count_record_in_page' => $count_record_in_page, 'collaborators' => $collaborators, 'approved_amount_total' => $approved_amount_total, 'waiting_product_count' => $waiting_product_count]);
//        } catch (Exception $exception) {
//            return view('errors.404');
//        }
    }

    public function approve($id)
    {
        if (auth()->user()->cannot('COLLABORATOR-approveContributedInfo')) {
            abort(403);
        }
        $contributed_information = Contributed_Information::findOrFail($id);
        //Check Product Management Role in Business Gateway System
        $product = Product::where('barcode', $contributed_information->gtin)->first();
        //Check Product Management Role in Business Gateway OLD System
        $legacy_product = Business_Product::where('product_id',$contributed_information->gtin)->first();
        if ($product || $legacy_product) {
            $contributed_information->update(["status" => Contributed_Information::STATUS_DISAPPROVED]);
            Need_Update_Product::where('gtin', $contributed_information->gtin)->delete();
            Gtin_Group::where('gtin', $contributed_information->gtin)->delete();
            return true;
        }
        if ($contributed_information->status == Contributed_Information::STATUS_APPROVED) {
            return true;
        }
        $contributed_data = $contributed_information->contributed_data;

        event(new ProductContributedInfoApprove($contributed_data, $contributed_information->gtin, $id));

    }

    public function disapprove($id)
    {
        if (auth()->user()->cannot('COLLABORATOR-disapproveContributedInfo')) {
            abort(403);
        }
        $contributed_information = Contributed_Information::findOrFail($id);
        if ($contributed_information->status == Contributed_Information::STATUS_DISAPPROVED) {
            return;
        }
        DB::connection('icheck_collaborator')->beginTransaction();
        $contributed_information = Contributed_Information::findOrFail($id);
        $contributed_information->update(["status" => Contributed_Information::STATUS_DISAPPROVED]);
        //Update Product_Need_Update Table
        $need_update_product = Need_Update_Product::where("gtin", $contributed_information->gtin)->first();
        $need_update_product->update(["need_update" => $need_update_product->need_update + $contributed_information->contributed_informations]);
        DB::connection('icheck_collaborator')->commit();

    }

    public function checkStatus($id)
    {
        $contributed_information = Contributed_Information::findOrFail($id);
        return $contributed_information->status;
    }
}
