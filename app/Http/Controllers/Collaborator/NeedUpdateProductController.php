<?php

namespace App\Http\Controllers\Collaborator;

use App\Models\Business\Product;
use App\Models\Collaborator\Contributed_Information;
use App\Models\Collaborator\Group;
use App\Models\Collaborator\Gtin_Group;
use App\Models\Collaborator\Job;
use App\Models\Collaborator\Need_Update_Product;
use App\Repository\Collaborator\CollaboratorRepositoryInterface;
use App\Services\Business\BarcodeValidation;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class NeedUpdateProductController extends Controller
{
    private $collaboratorRepository;
    private $barcodeValidation;

    public function __construct(CollaboratorRepositoryInterface $collaboratorRepository)
    {
        $this->collaboratorRepository = $collaboratorRepository;
        $this->barcodeValidation = new BarcodeValidation();
    }

    public function index(Request $request, Need_Update_Product $product)
    {

        if (auth()->user()->cannot('COLLABORATOR-viewNeedUpdateProducts') && auth()->user()->cannot('COLLABORATOR-addNeedUpdateProducts')) {
            abort(403);
        }
//        dd($request->all());
        $product = $product->newQuery();
        $query = [];
        $product->select('product_need_update.*')->where([['need_update', '>', 0], ['is_active', '=', 1]]);

        if ($request->input('gtin')) {
            $gtin = $request->input('gtin');
            $product->where('gtin', 'like', '%' . $gtin . '%');
            $query['gtin'] = $gtin;
        }

        if ($request->input('group_id')) {
            $group_id = $request->input('group_id');
            $product->whereExists(function ($query) use ($group_id){
                $query->select(DB::connection('icheck_collaborator')->raw(1))
                    ->from('gtin_group')
                    ->whereRaw('gtin_group.gtin = product_need_update.gtin')
                    ->where('gtin_group.group_id', $group_id)
                ;
            });
//            $gtins = Gtin_Group::where('group_id', $group_id)->pluck('gtin')->toArray();
//            $product->whereIn('gtin', $gtins);
            $query['group_id'] = $group_id;
        }

        if ($request->input('collaborator_id')) {
            $assigned_to = $request->input('collaborator_id');
            $product->where('assigned_to', $assigned_to);
            $query['collaborator_id'] = $assigned_to;
        }

        if ($request->input('need_update')) {
            $need_update = array_sum($request->input('need_update'));
            $product->where('need_update', $need_update);
            $query['need_update'] = $request->input('need_update');
        }

        if ($request->input('from')) {
            $from = $request->input('from');
            $product->where([['product_need_update.created_at', '>=', date('Y-m-d' . ' 00:00:00', strtotime($from))]]);
            $query['from'] = $from;
        }
        if ($request->input('to')) {
            $to = $request->input('to');
            $product->where([['product_need_update.created_at', '<=', date('Y-m-d' . ' 23:59:59', strtotime($to))]]);
            $query['to'] = $to;
        }

        $count_record_total = $product->count();
        $products = $product->orderBy('id', "desc")->paginate(20)->appends($query);
        $count_record_in_page = $products->count();
        $groups = Group::all();
        $collaborators = $this->collaboratorRepository->getAll();
        foreach ($products as $product) {
//            $product->need_update_text = $this->convertNeedUpdateNumberToText($product->need_update);
            $product->save();
            $product->need_update_text = '';
            $collection = collect([]);
            foreach (Need_Update_Product::$infoTexts as $key => $value) {
                if ($product->need_update & $key) {
                    $collection->push($value);
                }
            }
            $product->need_update_text = implode(" - ", $collection->all());

            $product->assigned_name = "";
            foreach ($collaborators as $collaborator) {
                if ($collaborator->id == $product->assigned_to) {
                    $product->assigned_name = $collaborator->name;
                }
            }

            //Add group name
            $group_ids = Gtin_Group::where("gtin", $product->gtin)->pluck("group_id")->toArray();
            $group_names = [];
            foreach ($group_ids as $group_id) {
                $group = Group::find($group_id);
                if ($group) {
                    array_push($group_names, $group->name);
                }
            }
            $product->group_name = implode(", ", $group_names);
        }
//        dd($products);
        return view('collaborator.need_update_product.index', ['products' => $products, 'count_record_total' => $count_record_total,
            'count_record_in_page' => $count_record_in_page, 'groups' => $groups, 'collaborators' => $collaborators]);
//        } catch (Exception $exception) {
//            return view('errors.404');
//        }
    }

    public function add()
    {
        if (auth()->user()->cannot('COLLABORATOR-addNeedUpdateProducts')) {
            abort(403);
        }
//        try {
        $groups = Group::all();
        return view('collaborator.need_update_product.form', ["groups" => $groups]);
//        } catch (Exception $exception) {
//            return view('errors.404');
//        }
    }

    public function store(Request $request)
    {
        if (auth()->user()->cannot('COLLABORATOR-addNeedUpdateProducts')) {
            abort(403);
        }
        //        try {
//        dd($request->all());
        //GTINS
        $data = $request->except("_token");
        $gtins = array_unique(explode("\r\n", trim($data["gtin"])));
        foreach ($gtins as $key => $value) {
            $need_update_product = Need_Update_Product::where('gtin', $value)->first();
            $productInBusinessGateway = Product::where('barcode', $value)->first();
            if ($need_update_product || $productInBusinessGateway) {
                unset($gtins[$key]);
            }
        }
        if (count($gtins) == 0) {
            return redirect()->action("Collaborator\NeedUpdateProductController@index")->with("success", "Không thêm được tất cả các mã vừa nhập lên hệ thống!");
        }
        $gtins = json_encode($gtins);
        //Need_update
        $need_update = null;
        if ($request->input("need_update")) {
            $need_update = array_reduce($request->input("need_update"), function ($a, $b) {
                return $a | $b;
            }, 0);
        }
        //Groups
        $group_ids = null;
        if ($request->input("group_ids")) {
            $group_ids = json_encode(array_map("intval", $request->input("group_ids")));
        }
        //Need_search
        $need_search = 0;
        if ($request->input("need_search") == 1) {
            $need_search = 1;
        }
        //Schedule
        $scheduled_at = date("Y-m-d H:i:s");

        Job::create(["gtins" => $gtins, "need_update" => $need_update, "group_ids" => $group_ids, "status" => Job::GOT_GTINS_STATUS, "scheduled_at" => $scheduled_at, "need_search" => $need_search]);

        return redirect()->action("Collaborator\NeedUpdateProductController@index")->with("success", "Đã thêm mới công việc, hệ thống đang xử lý dữ liệu!");
        //        } catch (Exception $exception) {
//            return view('errors.404');
//        }
    }

    public function edit($id)
    {
        if (auth()->user()->cannot('COLLABORATOR-updateNeedUpdateProducts')) {
            abort(403);
        }
//        try {
        $groups = Group::all();
        $product = Need_Update_Product::findOrFail($id);
        $product->group_ids = Gtin_Group::where("gtin", $product->gtin)->pluck("group_id")->toArray();
        return view('collaborator.need_update_product.form', ["groups" => $groups, "product" => $product]);
//        } catch (Exception $exception) {
//            return view('errors.404');
//        }
    }

    public function update($id, Request $request)
    {
        if (auth()->user()->cannot('COLLABORATOR-updateNeedUpdateProducts')) {
            abort(403);
        }
        //        try {
        $this->validate($request, [
            'gtin' => 'required|max:20',
        ],
            [
                'gtin.required' => 'Trường mã barcode không được để trống!',
                'gtin.max' => 'Mã barcode không quá 20 ký tự!'
            ]);
        $data = $request->except("_token", "_method");
        $product = Need_Update_Product::findOrFail($id);
        $need_update = null;
        if ($request->input("need_update")) {
            $need_update = array_reduce($request->input("need_update"), function ($a, $b) {
                return $a | $b;
            }, 0);
        }
//        $product->update(["gtin" => $data["gtin"], "group_id" => $data["group_id"], "need_update" => $need_update]);
        $product->update(["gtin" => $data["gtin"], "need_update" => $need_update]);
        Gtin_Group::where("gtin", $product->gtin)->delete();
        //Add to gtin_group table
        if ($request->input("group_ids")) {
            $group_ids = $request->input("group_ids");
            foreach ($group_ids as $group_id) {
                Gtin_Group::updateOrCreate(["gtin" => $product->gtin, "group_id" => $group_id]);
            }
        }
        return redirect()->back()->with("success", "Đã cập nhật thành công!");
        //        } catch (Exception $exception) {
//            return view('errors.404');
//        }
    }

    public function delete($id)
    {
        if (auth()->user()->cannot('COLLABORATOR-delNeedUpdateProducts')) {
            abort(403);
        }
        $product = Need_Update_Product::findOrFail($id);
        DB::connection('icheck_collaborator')->beginTransaction();
        Gtin_Group::where("gtin", $product->gtin)->delete();
        $product->delete();
        DB::connection('icheck_collaborator')->commit();
        return redirect()->back()->with("success", "Đã xóa thành công!");
    }

    public function showGtin($id)
    {
        if (auth()->user()->cannot("COLLABORATOR-hideNeedUpdateProducts")) {
            return '';
        }
        $product = Need_Update_Product::findOrFail($id);
        $product->update(['status' => 1]);
        return $product->status;
    }

    public function hideGtin($id)
    {
        if (auth()->user()->cannot("COLLABORATOR-hideNeedUpdateProducts")) {
            return '';
        }
        $product = Need_Update_Product::findOrFail($id);
        $product->update(['status' => 0]);
        return $product->status;
    }

}
