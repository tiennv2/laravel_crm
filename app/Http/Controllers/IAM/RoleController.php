<?php

namespace App\Http\Controllers\IAM;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use App\Http\Resources\IAM\RoleCollection;

class RoleController extends Controller
{
    public function index()
    {
        $data = Role::all();

        return new RoleCollection($data);
    }
}
