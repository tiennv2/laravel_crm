<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Account;
use App\Http\Resources\Account as AccountResource;
use App\Http\Resources\AccountCollection;

class AccountController extends Controller
{
    public function index(Request $request)
    {
        $offset    = $request->input('offset', 0);
        $limit     = $request->input('limit', 50);
        $ids       = $request->input('ids', []);
        $icheckIds = $request->input('icheckIds', []);
        $startRow  = $request->input('startRow', 0);
        $endRow    = $request->input('endRow', 100);

        $data = new Account;
        $data = $data->where(function ($query) use ($ids, $icheckIds) {
            if (is_array($ids) and count($ids) > 0) {
                $query->orWhereIn('id', $ids);
            }

            if (is_array($icheckIds) and count($icheckIds) > 0) {
                $query->orWhereIn('icheck_id', $icheckIds);
            }
        });
        $data = $data->skip($offset)->take($limit);
        $data = $data->get();

        return new AccountCollection($data);
    }

    public function show(Account $account)
    {
        return new AccountResource($account);
    }
}
