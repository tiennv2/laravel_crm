<?php

namespace App\Http\Controllers\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product\Attribute;
use App\Http\Resources\Product\AttributeCollection;

class AttributeController extends Controller
{
    public function index(Request $request)
    {
        $data = Attribute::all();

        return new AttributeCollection($data);
    }
}
