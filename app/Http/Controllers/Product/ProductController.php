<?php

namespace App\Http\Controllers\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product\Product;
use App\Http\Resources\Product\Product as ProductResource;
use App\Http\Resources\Product\ProductCollection;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        $rowGroupCols = $request->input('rowGroupCols', []);
        $groupKeys    = $request->input('groupKeys', []);
        $valueCols    = $request->input('valueCols', []);
        $filterModel  = $request->input('filterModel', []);
        $sortModel    = $request->input('sortModel', []);
        $startRow     = $request->input('startRow', 0);
        $endRow       = $request->input('endRow', 100);

        $data = Product::with('vendor');

        if (count($rowGroupCols) > count($groupKeys)) {
            $select = [];
            $rowGroupCol = $rowGroupCols[count($groupKeys)];
            $select[] = $rowGroupCol['field'];

            foreach ($valueCols as $valueCol) {
                $select[] = $valueCol;
            }

            $data = $data->select($select);

            $colsToGroupBy = [];
            $colsToGroupBy[] = $rowGroupCol['field'];

            foreach ($colsToGroupBy as $col) {
                $data = $data->groupBy($col);
            }
        }

        $whereParts = [];

        if (count($groupKeys) > 0) {
            foreach ($groupKeys as $index => $key) {
                $whereParts[] = [$rowGroupCols[$index]['field'], '=', $key];
            }
        }

        // if ($filterModel) {
        //     foreach ($groupKeys as $key => $index) {
        //         $whereParts[] = [$rowGroupCols[$index]['field'], '=', $key];
        //     }
        // }

        foreach ($whereParts as $where) {
            $data = $data->where($where[0], $where[1], $where[2]);
        }

        if (count($sortModel) > 0) {
            foreach ($sortModel as $item) {
                $data = $data->orderBy($item['colId'], $item['sort']);
            }
        }

        $pageSize = $endRow - $startRow;
        $data = $data->skip($startRow)->take($pageSize);

        return new ProductCollection($data->get());
    }

    public function show(Product $product)
    {
        $product->loadMissing('attributes');

        return new ProductResource($product);
    }
}
