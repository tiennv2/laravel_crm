<?php

namespace App\Http\Controllers\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product\Category;
use App\Http\Resources\Product\Category as CategoryResource;
use App\Http\Resources\Product\CategoryCollection;

class CategoryController extends Controller
{
    public function index(Request $request)
    {
        $display = $request->input('display', 'tree');

        if ($display === 'flat') {
            $data = Category::with('ancestors')->get();
        } else {
            $data = Category::get()->toTree();
        }

        return new CategoryCollection($data);
    }

    public function store(Request $request)
    {
        $node = new Category($request->all());
        $node->parent_id = $request->input('parentId');
        $node->save();

        return new CategoryResource($node);
    }

    public function update(Category $category, Request $request)
    {
        $category->update($request->all());

        return ['success' => true];
    }
}
