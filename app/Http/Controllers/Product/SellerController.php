<?php

namespace App\Http\Controllers\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product\Seller;
use App\Http\Resources\Product\Seller as SellerResource;
use App\Http\Resources\Product\SellerCollection;

class SellerController extends Controller
{
    public function index(Request $request)
    {
        $data = Seller::with('district', 'city')->paginate(10);

        return new SellerCollection($data);
    }

    public function store(Request $request)
    {
        $seller = new Seller($request->all());
        $seller->district()->associate($request->input('districtId'));
        $seller->city()->associate($request->input('cityId'));
        $seller->save();

        return new SellerResource($seller);
    }
}
