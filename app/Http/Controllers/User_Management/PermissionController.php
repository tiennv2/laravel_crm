<?php

namespace App\Http\Controllers\User_Management;

use App\Models\Icheck_newCMS\Permission;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PermissionController extends Controller
{
    public function index(Request $request, Permission $permissions)
    {
        if (auth()->user()->cannot('SYSTEM-viewPermission') && auth()->user()->cannot('SYSTEM-addPermission')) {
            abort(403);
        }
        try {
            $permissions = $permissions->newQuery();
            $query = [];
            $permissions->select('permissions.*');
            if ($request->input('name')) {
                $name = $request->input('name');
                $permissions->where('name', 'like', '%' . $name . '%');
                $query['name'] = $name;
            }

            if ($request->input('description')) {
                $description = $request->input('description');
                $permissions->where('description', 'like', '%' . $description . '%');
                $query['description'] = $description;
            }
            $permissions = $permissions->orderBy('id', 'desc')->paginate(20)->appends($query);
            return view('authorization.permission.index', ['permissions' => $permissions]);
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

    public function add()
    {
        if (auth()->user()->cannot('SYSTEM-addPermission')) {
            abort(403);
        }
        try {
            return view('authorization.permission.form');
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

    public function edit($id)
    {
        if (auth()->user()->cannot('SYSTEM-updatePermission')) {
            abort(403);
        }
        try {
            $permission = Permission::findOrFail($id);
            return view('authorization.permission.form', ['permission' => $permission]);
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

    public function store(Request $request)
    {
        if (auth()->user()->cannot('SYSTEM-viewPermission')) {
            abort(403);
        }
        try {
            $this->validate($request, [
                'name' => 'required'
            ],
                [
                    'name.required' => 'Trường tên quyền không được để trống!',
                ]);
            $data = $request->except(["_token"]);
            $permission = Permission::create($data);
            return redirect()->route('User_Management::permission@index')
                ->with('success', 'Đã thêm mới quyền thành công');
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

    public function update($id, Request $request)
    {
        if (auth()->user()->cannot('SYSTEM-updatePermission')) {
            abort(403);
        }
        try {
            $permission = Permission::findOrFail($id);
            $data = $request->except(["_token", "_method"]);
            $permission->update($data);
            return redirect()->back()
                ->with('success', 'Đã cập nhật quyền thành công');
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }
}
