<?php

namespace App\Http\Controllers\User_Management;

use App\Models\Icheck_newCMS\ModelHasRoles;
use App\Models\Icheck_newCMS\Permission;
use App\Models\Icheck_newCMS\Role;
use App\Models\Icheck_newCMS\RoleHasPermissions;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RoleController extends Controller
{
    public function index(Request $request, Role $roles)
    {
        if (auth()->user()->cannot('SYSTEM-viewRole') && auth()->user()->cannot('SYSTEM-addRole')) {
            abort(403);
        }
        try {
            $roles = $roles->newQuery();
            $query = [];
            $roles->select('roles.*');
//            $super_admin = ["i-1526871410385", "i-9999"];
            $role_ids_assigned = ModelHasRoles::where('model_id', auth()->user()->id)->pluck('role_id')->toArray();
            $role_ids_createdBy_currentUser = Role::where('created_by', auth()->user()->icheck_id)->pluck('id')->toArray();
            $canEdit_role_ids = array_merge($role_ids_assigned, $role_ids_createdBy_currentUser);

            $roles = $roles->orderBy('id', 'desc')->paginate(20)->appends($query);
            foreach ($roles as $role) {
                if (in_array($role->id, $canEdit_role_ids) || auth()->user()->hasRole('super-admin')) {
                    $role->canEdit = true;
                } else {
                    $role->canEdit = false;
                }
            }
            return view('authorization.role.index', ['roles' => $roles]);
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

    public function add()
    {
        if (auth()->user()->cannot('SYSTEM-addRole')) {
            abort(403);
        }
        try {
//            $super_admin = ["i-1526871410385", "i-9999"];
            if (auth()->user()->hasRole('super-admin')) {
                $permissions = Permission::orderBy('name', 'desc')->get();
            } else {
                $permission_ids = auth()->user()->getAllPermissions()->pluck('id')->toArray();
                $permissions = Permission::whereIn("permissions.id", $permission_ids)->orderBy('name', 'desc')->get();
            }
            return view('authorization.role.form', ['permissions' => $permissions]);
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

    public function edit($id)
    {
        if (auth()->user()->cannot('SYSTEM-updateRole')) {
            abort(403);
        }
        try {
//            $super_admin = ["i-1526871410385", "i-9999"];
            if (auth()->user()->hasRole('super-admin')) {
                $permissions = Permission::orderBy('name', 'desc')->get();
            } else {
                $permission_ids = auth()->user()->getAllPermissions()->pluck('id')->toArray();
                $permissions = Permission::whereIn("permissions.id", $permission_ids)->orderBy('name', 'desc')->get();
            }
            $role = Role::findOrFail($id);
            $rolePermissions = Permission::join("role_has_permissions", "role_has_permissions.permission_id", "=", "permissions.id")
                ->where("role_has_permissions.role_id", $id)
                ->pluck('permission_id')
                ->toArray();
            return view('authorization.role.form', ['permissions' => $permissions, 'role' => $role, 'rolePermissions' => $rolePermissions]);
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

    public function update($id, Request $request)
    {
        if (auth()->user()->cannot('SYSTEM-updateRole')) {
            abort(403);
        }
        try {
            $this->validate($request, [
                'name' => 'required'
            ],
                [
                    'name.required' => 'Trường tên nhóm quyền không được để trống!',
                ]);
            $data = $request->except(["_token", "_method", "permissions"]);
            $role = Role::findOrFail($id);
            $role->update($data);
            if ($request->input('permissions')) {
                $role->syncPermissions($request->input('permissions'));
            }

            return redirect()->back()
                ->with('success', 'Đã cập nhật quyền thành công');
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

    public function store(Request $request)
    {
        if (auth()->user()->cannot('SYSTEM-addRole')) {
            abort(403);
        }
        try {
            $this->validate($request, [
                'name' => 'required'
            ],
                [
                    'name.required' => 'Trường tên nhóm quyền không được để trống!',
                ]);
            $data = $request->except(["_token", "permissions"]);
            $data['created_by'] = auth()->user()->icheck_id;
            $role = Role::create($data);
            if ($request->input('permissions')) {
                $role->syncPermissions($request->input('permissions'));
            }

            return redirect()->route('User_Management::role@index')
                ->with('success', 'Đã thêm mới nhóm quyền thành công');
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

    public function delete($id)
    {
        if (auth()->user()->cannot('SYSTEM-delRole')) {
            abort(403);
        }
        try {
            $role = Role::findById($id);
            $role->delete();
            ModelHasRoles::where("role_id", $id)->delete();
            RoleHasPermissions::where("role_id", $id)->delete();
            return redirect()->route('User_Management::role@index')
                ->with('success', 'Đã xóa nhóm quyền thành công');
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

}
