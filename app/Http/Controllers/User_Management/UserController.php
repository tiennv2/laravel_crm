<?php

namespace App\Http\Controllers\User_Management;

use App\Models\Icheck_newCMS\ModelHasRoles;
use App\Models\Icheck_newCMS\Permission;
use App\Models\Icheck_newCMS\Role;
use App\User;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function index(Request $request, User $users)
    {
        if (auth()->user()->cannot('SYSTEM-viewUser') && auth()->user()->cannot('SYSTEM-addUser')) {
            abort(403);
        }
        try {
            $users = $users->newQuery();
            $users->select('users.*');
            $query = [];
            if ($request->input('name')) {
                $name = $request->input('name');
                $users->where('name', 'like', '%' . $name . '%');
                $query['name'] = $name;
            }

            if ($request->input('icheck_id')) {
                $icheck_id = $request->input('icheck_id');
                $users->where('icheck_id', 'like', '%' . $icheck_id . '%');
                $query['icheck_id'] = $icheck_id;
            }
            $users = $users->orderBy('id', 'desc')->paginate(20)->appends($query);
            foreach ($users as $user) {
                $role_ids_assigned = ModelHasRoles::where('model_id', $user->id)->pluck('role_id')->toArray();
                $role_names = Role::whereIn('id', $role_ids_assigned)->pluck('description')->toArray();
                $user->role_names = $role_names;
            }
            return view('authorization.user.index', ['users' => $users]);
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

    public function add()
    {
        if (auth()->user()->cannot('SYSTEM-addUser')) {
            abort(403);
        }
        try {
            return view('authorization.user.addUserForm');
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

    public function store(Request $request)
    {
        if (auth()->user()->cannot('SYSTEM-addUser')) {
            abort(403);
        }
        try {
            $this->validate($request, [
                'icheck_id' => 'required'
            ],
                [
                    'icheck_id.required' => 'Trường icheck_id không được để trống!',
                ]);
            $data = $request->except(["_token"]);
            $icheck_id = $data['icheck_id'];
            $check = User::where('icheck_id', $icheck_id)->first();
            if ($check) {
                return redirect()
                    ->back()
                    ->with('error', 'Tài khoản người dùng đã tồn tại trên Hệ thống CMS. Hãy kiểm tra lại!')
                    ->withInput($request->all);
            }

            $icheck_user = DB::connection('icheck_user')->table('u_account')
                ->select('name')->where([['icheck_id', $icheck_id], ['phone_verified', 1]])
                ->first();
            if (!$icheck_user) {
                return redirect()
                    ->back()
                    ->with('error', 'ICHECK_ID không tồn tại hoặc số điện thoại chưa được xác thực. Hãy kiểm tra lại!')
                    ->withInput($request->all);
            } else {
                $new_user = User::create(['icheck_id' => $icheck_id, 'name' => $icheck_user->name]);
                if ($new_user) {
                    return redirect()->route('User_Management::user@index')
                        ->with('success', 'Đã thêm mới người dùng thành công');
                } else {
                    return "Có lỗi xảy ra trong quá trình tạo mới người dùng!";
                }
            }

        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

    public function edit($id)
    {
        if (auth()->user()->cannot('SYSTEM-updateUser')) {
            abort(403);
        }
        try {
            $roles = [];
//            $super_admin = ["i-1526871410385", "i-9999"];
            if (auth()->user()->hasRole('super-admin')) {
                $roles = Role::all();
            } else {
                $role_ids_assigned = ModelHasRoles::where('model_id', auth()->user()->id)->pluck('role_id')->toArray();
                $role_ids_createdBy_currentUser = Role::where('created_by', auth()->user()->icheck_id)->pluck('id')->toArray();
                $role_ids = array_merge($role_ids_assigned, $role_ids_createdBy_currentUser);
                $roles = Role::whereIn('id', $role_ids)->get();
            }
//
            foreach ($roles as $role) {
                $rolePermissions = Permission::join("role_has_permissions", "role_has_permissions.permission_id", "=", "permissions.id")
                    ->where("role_has_permissions.role_id", $role->id)
                    ->orderBy('description', 'asc')
                    ->get();
                if ($rolePermissions->count()) {
                    $role->permissions = $rolePermissions;
                } else {
                    $role->permissions = [];
                }

            }

            $user = User::findOrFail($id);
            $userRoles = Role::join("model_has_roles", "model_has_roles.role_id", "=", "id")
                ->where("model_has_roles.model_id", $id)
                ->pluck('role_id')
                ->toArray();

//            $userPermissions = Permission::join("model_has_permissions", "model_has_permissions.permission_id", "=", "id")
//                ->where("model_has_permissions.model_id", $id)
//                ->pluck('permission_id')
//                ->toArray();

            return view('authorization.user.form', ['roles' => $roles,
                'user' => $user, 'userRoles' => $userRoles]);
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

    public function update($id, Request $request)
    {
        if (auth()->user()->cannot('SYSTEM-updateUser')) {
            abort(403);
        }
        try {
            $user = User::findOrFail($id);
            if ($request->input('role_ids')) {
                $role_ids = $request->input('role_ids');
                $user->syncRoles([$role_ids]);
            } else {
                $user->syncRoles([]);
            }

            return redirect()->back()
                ->with('success', 'Đã cập nhật quyền cho người dùng thành công!');
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

    public function delete($id)
    {
        if (auth()->user()->cannot('SYSTEM-delUser')) {
            abort(403);
        }
        try {
            $user = User::findOrFail($id);
            $user->delete();
            return redirect()->back()->with('success', 'Đã xoá thành công!');
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }
}
