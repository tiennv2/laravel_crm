<?php

namespace App\Http\Controllers\Qrcode;

use App\Http\Controllers\Controller;
use App\Models\Qrcode\Account;
use App\Models\Qrcode\Static_Stamp;
use Exception;
use function GuzzleHttp\Promise\queue;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class MarketingStampController extends Controller
{

    public function index(Request $request, Static_Stamp $static_stamps)
    {
        if (auth()->user()->cannot('QRCODE-viewMarketingStamp')) {
            abort(403);
        }
        try {
            $static_stamps = $static_stamps->newQuery();
            $query = [];
            $static_stamps->leftJoin('static_template', 'static_stamp.template_id', '=', 'static_template.id')
                ->leftJoin('account', 'static_stamp.user_id', '=', 'account.id')
                ->leftJoin('product', 'static_stamp.product_id', '=', 'product.id');
            $static_stamps->select('static_stamp.*', 'static_template.name as template_name',
                'static_template.image as template_image', 'product.name as product_name', 'account.name as account_name')
                ->orderBy('static_stamp.id', 'desc');

            if ($request->input('name')) {
                $name = $request->input('name');
                $static_stamps->where('static_stamp.name', 'like', '%' . $name . '%');
                $query['name'] = $name;
            }
            if ($request->input('account_id')) {
                $account_id = $request->input('account_id');
                $static_stamps->where('account.id', $account_id);
                $query['account_id'] = $account_id;
            }

            if ($request->input('product_name')) {
                $product_name = $request->input('product_name');
                $static_stamps->where('product.name', 'like', '%' . $product_name . '%');
                $query['product_name'] = $product_name;
            }
            if (($request->input('status')) && $request->input('status') != "request_expired") {
                $status = $request->input('status');
                $static_stamps->where('static_stamp.status', $status)
                    ->whereIn('static_stamp.request_expired', [0, null]);
                $query['status'] = $status;
            }
            if (($request->input('status')) && $request->input('status') == "request_expired") {
                $static_stamps->where('static_stamp.request_expired', ">", 0);
                $query['status'] = "request_expired";
            }
            if (($request->input('active_from')) && $request->input('active_from') != null) {
                $active_from = $request->input('active_from');
                $static_stamps->where('static_stamp.active_time', '>=', date('Y-m-d' . ' 00:00:00', strtotime($active_from)));
                $query['active_from'] = $active_from;
            }
            if (($request->input('active_to')) && $request->input('active_to') != null) {
                $active_to = $request->input('active_to');
                $static_stamps->where('static_stamp.active_time', '<=', date('Y-m-d' . ' 23:59:59', strtotime($active_to)));
                $query['active_to'] = $active_to;
            }
            if (($request->input('expired_from')) && $request->input('expired_from') != null) {
                $expired_from = $request->input('expired_from');
                $static_stamps->where('static_stamp.expired_time', '>=', date('Y-m-d' . ' 00:00:00', strtotime($expired_from)));
                $query['expired_from'] = $expired_from;
            }
            if (($request->input('expired_to')) && $request->input('expired_to') != null) {
                $expired_to = $request->input('expired_to');
                $static_stamps->where('static_stamp.expired_time', '<=', date('Y-m-d' . ' 23:59:59', strtotime($expired_to)));
                $query['expired_to'] = $expired_to;
            }
            $accounts = Account::select('id', 'name')
                ->where('type', 1)
                ->get();
            $count_record = $static_stamps->count();
            $static_stamps = $static_stamps->paginate(10)->appends($query);
            foreach($static_stamps as $stamp){
                $stamp->expired = false;
                if($stamp->expired_time < date("Y-m-d h:i:s")){
                    $stamp->expired = true;
                }
            }

            return view('qrcode.marketing-stamp.index', ['static_stamps' => $static_stamps, 'accounts' => $accounts,
                'query' => $query,'count_record' => $count_record
            ]);
        } catch (Exception $ex) {
            return view('errors.404');
        }
    }


    public function approve($id)
    {
        if (auth()->user()->cannot('QRCODE-approveMarketingStamp')) {
            abort(403);
        }
        try {
            $static_stamp = Static_Stamp::findOrFail($id);
            if ($static_stamp->request_expired) {
                $request_expired = $static_stamp->request_expired;
                if ($static_stamp->active_time && $static_stamp->expired_time > date("Y-m-d h:i:s")) {
                    $active_time = $static_stamp->active_time;
                } else {
                    $active_time = date("Y-m-d h:i:s");
                }

                if ($static_stamp->expired_time && $static_stamp->expired_time > date("Y-m-d h:i:s")) {
                    $expired_time = date_add(date_create($static_stamp->expired_time), date_interval_create_from_date_string("+ " . $request_expired . "years"));
                } else {
                    $expired_time = date("Y-m-d h:i:s", strtotime("+ " . $request_expired . "years"));
                }
                $static_stamp->update(['status' => 'approved', 'active_time' => $active_time, 'expired_time' => $expired_time, 'request_expired' => 0]);
            } else {
                if ($static_stamp->active_time) {
                    $active_time = $static_stamp->active_time;
                } else {
                    $active_time = date("Y-m-d h:i:s");
                }
                $static_stamp->update(['status' => 'approved', 'active_time' => $active_time]);
            }
            return redirect()->back()
                ->with('success', 'Đã duyệt thành công!');
        } catch (Exception $ex) {
            return view('errors.404');
        }
    }

    public function disapprove($id)
    {
        if (auth()->user()->cannot('QRCODE-disapproveMarketingStamp')) {
            abort(403);
        }
        try {
            $static_stamp = Static_Stamp::findOrFail($id);
            $static_stamp->update(['status' => 'reject']);
            return redirect()->back()
                ->with('success', 'Đã bỏ duyệt thành công!');
        } catch (Exception $ex) {
            return view('errors.404');
        }
    }

    public function activeShowProduct($id)
    {
        if (auth()->user()->cannot('QRCODE-modifyMarketingStampProperty')) {
            return false;
        }
        $static_stamp = Static_Stamp::findOrFail($id);
        $static_stamp->update(['show_product' => 1]);
        return true;
    }

    public function deactiveShowProduct($id)
    {
        if (auth()->user()->cannot('QRCODE-modifyMarketingStampProperty')) {
            return false;
        }
        $static_stamp = Static_Stamp::findOrFail($id);
        $static_stamp->update(['show_product' => 0]);
        return true;
    }

    public function activeShowVendor($id)
    {
        if (auth()->user()->cannot('QRCODE-modifyMarketingStampProperty')) {
            return false;
        }
        $static_stamp = Static_Stamp::findOrFail($id);
        $static_stamp->update(['show_vendor' => 1]);
        return true;
    }

    public function deactiveShowVendor($id)
    {
        if (auth()->user()->cannot('QRCODE-modifyMarketingStampProperty')) {
            return false;
        }
        $static_stamp = Static_Stamp::findOrFail($id);
        $static_stamp->update(['show_vendor' => 0]);
        return true;
    }

    public function activeShowRelated($id)
    {
        if (auth()->user()->cannot('QRCODE-modifyMarketingStampProperty')) {
            return false;
        }
        $static_stamp = Static_Stamp::findOrFail($id);
        $static_stamp->update(['show_related' => 1]);
        return true;
    }

    public function deactiveShowRelated($id)
    {
        if (auth()->user()->cannot('QRCODE-modifyMarketingStampProperty')) {
            return false;
        }
        $static_stamp = Static_Stamp::findOrFail($id);
        $static_stamp->update(['show_related' => 0]);
        return true;
    }

    public function activeShowStore($id)
    {
        if (auth()->user()->cannot('QRCODE-modifyMarketingStampProperty')) {
            return false;
        }
        $static_stamp = Static_Stamp::findOrFail($id);
        $static_stamp->update(['show_store' => 1]);
        return true;
    }

    public function deactiveShowStore($id)
    {
        if (auth()->user()->cannot('QRCODE-modifyMarketingStampProperty')) {
            return false;
        }
        $static_stamp = Static_Stamp::findOrFail($id);
        $static_stamp->update(['show_store' => 0]);
        return true;
    }

    public function activeShowMessage($id)
    {
        if (auth()->user()->cannot('QRCODE-modifyMarketingStampProperty')) {
            return false;
        }
        $static_stamp = Static_Stamp::findOrFail($id);
        $static_stamp->update(['show_message' => 1]);
        return true;
    }

    public function deactiveShowMessage($id)
    {
        if (auth()->user()->cannot('QRCODE-modifyMarketingStampProperty')) {
            return false;
        }
        $static_stamp = Static_Stamp::findOrFail($id);
        $static_stamp->update(['show_message' => 0]);
        return true;
    }

    public function activeShowPrice($id)
    {
        if (auth()->user()->cannot('QRCODE-modifyMarketingStampProperty')) {
            return false;
        }
        $static_stamp = Static_Stamp::findOrFail($id);
        $static_stamp->update(['show_price' => 1]);
        return true;
    }

    public function deactiveShowPrice($id)
    {
        if (auth()->user()->cannot('QRCODE-modifyMarketingStampProperty')) {
            return false;
        }
        $static_stamp = Static_Stamp::findOrFail($id);
        $static_stamp->update(['show_price' => 0]);
        return true;
    }

    public function activeShowInfo($id)
    {
        if (auth()->user()->cannot('QRCODE-modifyMarketingStampProperty')) {
            return false;
        }
        $static_stamp = Static_Stamp::findOrFail($id);
        $static_stamp->update(['show_info' => 1]);
        return true;
    }

    public function deactiveShowInfo($id)
    {
        if (auth()->user()->cannot('QRCODE-modifyMarketingStampProperty')) {
            return false;
        }
        $static_stamp = Static_Stamp::findOrFail($id);
        $static_stamp->update(['show_info' => 0]);
        return true;
    }

    public function delRequestExpired($id)
    {
        $static_stamp = Static_Stamp::findOrFail($id);
        $static_stamp->update(['request_expired' => 0]);
        return $static_stamp->request_expired;
    }

    public function delete($id)
    {
        if (auth()->user()->cannot('QRCODE-delMarketingStamp')) {
            return false;
        }
        $static_stamp = Static_Stamp::findOrFail($id);
        $static_stamp->delete();
    }

    public function getById($id)
    {
        $static_stamp = Static_Stamp::find($id);
        if ($static_stamp) {
            return 1;
        }
        return -1;
    }

    public function updateExpiredTime($id, Request $request)
    {
        if (auth()->user()->cannot('QRCODE-updateExpiredTimeMarketingStamp')) {
            abort(403);
        }

        $expired_time = $request->input('expired_time');
        $expired_time = $expired_time . " 08:00:00";
        if(date("Y-m-d h:i:s") > date("Y-m-d h:i:s", strtotime($expired_time))){
            return '';
        }
        $static_stamp = Static_Stamp::findOrFail($id);
        $static_stamp->update(['expired_time' => $expired_time]);
        return $static_stamp->expired_time;
    }
    public function expandExpire($id, Request $request)
    {
        if (auth()->user()->cannot('QRCODE-updateExpiredTimeMarketingStamp')) {
            abort(403);
        }

        $request_expired = $request->input('request_expired');
        $expired_time = date("Y-m-d h:i:s", strtotime("+ " . $request_expired . "years"));
        $static_stamp = Static_Stamp::findOrFail($id);
        $static_stamp->update(['expired_time' => $expired_time, "status" => "approved"]);
        return redirect()->back()
            ->with('success', 'Đã gia hạn thành công!');
    }

}
