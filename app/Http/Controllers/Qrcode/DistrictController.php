<?php

namespace App\Http\Controllers\Qrcode;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DistrictController extends Controller
{
    public function showDistrictsInCity(Request $request)
    {
        if ($request->ajax()) {
            $districts = DB::connection('icheck_qrcode')->table('g_district')->select("name","id")->where('city_id',$request->city_id)->get();

            return response()->json($districts);
        }
    }


}