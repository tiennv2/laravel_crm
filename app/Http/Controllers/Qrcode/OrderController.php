<?php

namespace App\Http\Controllers\Qrcode;

use App\API\Api_qrcode;
use App\Http\Controllers\Controller;
use App\Models\Icheck_newCMS\ActivityLog;
use App\Models\Qrcode\Approve_Order_History;
use App\Models\Qrcode\Create_Order_History;
use App\Models\Qrcode\Order;
use App\Repository\Qrcode\OrderRepositoryInterface;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class OrderController extends Controller
{
    private $api_qrcode;
    private $orderRepository;

    public function __construct(OrderRepositoryInterface $orderRepository)
    {
        $this->api_qrcode = new Api_qrcode();
        $this->orderRepository = $orderRepository;
    }

    public function index(Request $request, Order $orders)
    {
        if (auth()->user()->cannot('QRCODE-viewOrder') && auth()->user()->cannot('QRCODE-addOrder')) {
            abort(403);
        }
        try {
            $orders = $orders->newQuery();
            $query = [];
            $orders->leftJoin('account', 'orders.user_id', '=', 'account.id');
            $orders->select('orders.*', 'account.name as account_name', 'account.id as account_id')
                ->orderBy('orders.id', 'desc');
            //Lọc tuyệt đối
            if ($request->has('services')) {
                $services = Input::get('services');
                sort($services);
                $services = implode(",", $services);
                $orders->where('orders.services', '=', $services);
                $query['services'] = explode(",", $services);
            }

            if ($request->has('name')) {
                $name = $request->input('name');
                $orders->where('account.name', 'like', '%' . $name . '%');
                $query['name'] = $name;

            }

            if ($request->input('payment_status') != null) {
                $payment_status = $request->input('payment_status');
                if ($payment_status == 0) {
                    $orders->where(
                        function ($query) {
                            $query->where('orders.payment_status', 0)
                                ->orWhereNull('orders.payment_status');
                        }
                    );
                } else {
                    $orders->where('orders.payment_status', $payment_status);
                }
                $query['payment_status'] = $payment_status;
            }

            if (($request->has('status')) && $request->input('status') != null) {
                $status = $request->input('status');
                $orders->where('orders.status', $status);
                $query['status'] = $status;
            }

            if (($request->has('from')) && $request->input('from') != null) {
                $from = $request->input('from');
                $orders->where('orders.created_at', '>=', date('Y-m-d' . ' 00:00:00', strtotime($from)));
                $query['from'] = $from;
            }


            if (($request->has('to')) && $request->input('to') != null) {
                $to = $request->input('to');
                $orders->where('orders.created_at', '<=', date('Y-m-d' . ' 23:59:59', strtotime($to)));
                $query['to'] = $to;
            }

            if ($request->has('user_created') && $request->input('user_created') != null) {
                $user_created = $request->input('user_created');
                if ($user_created == 'admin') {
                    $create_order_history = Create_Order_History::pluck('order_id')->toArray();
                    $orders->wherenotIn('orders.id', $create_order_history)
                        ->where('orders.user_created', "=", null);
                    $query['user_created'] = $user_created;
                } else {
                    $create_order_history = Create_Order_History::where('collaborator_id', '=', $user_created)
                        ->pluck('order_id')->toArray();
                    $orders->whereIn('orders.id', $create_order_history)
                        ->where('orders.user_created', "=", null);
                    $query['user_created'] = $user_created;
                }

            }
            if ($request->has('account_created') && $request->input('account_created') != null) {
                $account_created = $request->input('account_created');
                $orders->where('orders.user_created', $account_created);
                $query['account_created'] = $account_created;

            }
            if ($request->has('account_group') && $request->input('account_group') != null) {
                $account_group = $request->input('account_group');
                if ($account_group == 'icheck') {
                    $orders->where('account.collaborator_phone', '=', null);
                    $query['account_group'] = $account_group;
                } else {
                    $orders->where('account.collaborator_phone', $account_group);
                    $query['account_group'] = $account_group;
                }
            }
            if ($request->has('user_approve') && $request->input('user_approve') != null) {
                $user_approve = $request->input('user_approve');
                if ($user_approve == 'admin') {
                    $approve_order_history = Approve_Order_History::pluck('order_id')->toArray();
                    $orders->wherenotIn('orders.id', $approve_order_history)
                        ->where('orders.status', '=', 1);;
                    $query['user_approve'] = $user_approve;
                } else {
                    $approve_order_history = Approve_Order_History::where('collaborator_id', '=', $user_approve)
                        ->pluck('order_id')->toArray();
                    $orders->whereIn('orders.id', $approve_order_history);
                    $query['user_approve'] = $user_approve;
                }
            }

            $orders_id_list = $orders->pluck('orders.id')->toArray();
            $orders_list = $orders->paginate(10)->appends($query);

            $services = DB::connection('icheck_qrcode')->table('service_stamp')->select('id', 'name as service_name')->get();
            $accounts = DB::connection('icheck_qrcode')->table('account')
                ->select('id', 'name')
                ->where('account.status', '>', 0)
                ->get();
            //stamp_ordered statistics
            $stamp_ordered_total = DB::connection('icheck_qrcode')->table('orders')
                ->select('orders.quantity')
                ->whereIn('orders.id', $orders_id_list)
                ->sum('quantity');
            $stamp_ordered_waiting_total = DB::connection('icheck_qrcode')->table('orders')
                ->select('orders.quantity')
                ->whereIn('orders.id', $orders_id_list)
                ->where('orders.status', '=', 0)
                ->sum('quantity');
            $stamp_ordered_approved_total = DB::connection('icheck_qrcode')->table('orders')
                ->select('orders.quantity')
                ->whereIn('orders.id', $orders_id_list)
                ->where('orders.status', '=', 1)
                ->sum('quantity');
            $stamp_ordered_disapproved_total = DB::connection('icheck_qrcode')->table('orders')
                ->select('orders.quantity')
                ->whereIn('orders.id', $orders_id_list)
                ->where('orders.status', '=', 2)
                ->sum('quantity');
            //Order statistics
            $order_total = DB::connection('icheck_qrcode')->table('orders')
                ->whereIn('orders.id', $orders_id_list)
                ->where('deleted_at', '=', null)
                ->count();
            $order_approve_waiting_total = DB::connection('icheck_qrcode')->table('orders')
                ->whereIn('orders.id', $orders_id_list)
                ->where('deleted_at', '=', null)
                ->where('status', '=', 0)->count();
            $order_approved_total = DB::connection('icheck_qrcode')->table('orders')
                ->whereIn('orders.id', $orders_id_list)
                ->where('deleted_at', '=', null)
                ->where('status', '=', 1)->count();
            $order_disapproved_total = DB::connection('icheck_qrcode')->table('orders')
                ->whereIn('orders.id', $orders_id_list)
                ->where('deleted_at', '=', null)
                ->where('status', '=', 2)->count();

            $create_order_history = DB::connection('icheck_qrcode_agency')->table('create_order_history')
                ->select('create_order_history.*')->get();
            $approve_order_history = DB::connection('icheck_qrcode_agency')->table('approve_order_history')
                ->select('approve_order_history.*')->get();
            $users = DB::connection('icheck_qrcode_agency')->table('users')
                ->select('users.id', 'users.name', 'users.collaborator_phone')
                ->where('collaborator_id', '=', null)
                ->where('deleted_at', '=', null)
                ->get();
            return view('qrcode.order.index', ['orders' => $orders_list, 'services' => $services,
                'accounts' => $accounts, 'create_order_history' => $create_order_history,
                'approve_order_history' => $approve_order_history, 'users' => $users,
                'stamp_ordered_total' => $stamp_ordered_total, 'stamp_ordered_waiting_total' => $stamp_ordered_waiting_total,
                'stamp_ordered_approved_total' => $stamp_ordered_approved_total, 'stamp_ordered_disapproved_total' => $stamp_ordered_disapproved_total,
                'order_total' => $order_total, 'order_approve_waiting_total' => $order_approve_waiting_total,
                'order_approved_total' => $order_approved_total, 'order_disapproved_total' => $order_disapproved_total,
                'query' => $query
            ]);
        } catch (Exception $ex) {
            return view('errors.404');
        }
    }

    public function add()
    {
        if (auth()->user()->cannot('QRCODE-addOrder')) {
            abort(403);
        }
        try {
            $services = DB::connection('icheck_qrcode')->table('service_stamp')->select('id', 'name as service_name')->get();
            $companies = DB::connection('icheck_qrcode')->table('account')
                ->where([['type', '=', 1], ['status', '=', 1]])->get();
            return view('qrcode.order.form', array('services' => $services, 'companies' => $companies));
        } catch (Exception $ex) {
            return view('errors.404');
        }

    }

    public function approve($id)
    {
        if (auth()->user()->cannot('QRCODE-approveOrder')) {
            abort(403);
        }
        try {
            $this->orderRepository->approve($id);
            return redirect()->back()
                ->with('success', 'Đã duyệt thành công');
        } catch (Exception $ex) {
            return view('errors.404');
        }
    }

    public function disapprove($id)
    {
        if (auth()->user()->cannot('QRCODE-disapproveOrder')) {
            abort(403);
        }
        try {
            DB::connection('icheck_qrcode')->table('orders')
                ->where('id', $id)
                ->update(['status' => Order::STATUS_DISAPPROVED]);
            return redirect()->back()
                ->with('success', 'Đã bỏ duyệt thành công');
        } catch (Exception $ex) {
            return view('errors.404');
        }

    }

    public function store(Request $request)
    {
        if (auth()->user()->cannot('QRCODE-addOrder')) {
            abort(403);
        }
        $this->validate($request, [
            'user_id' => 'required',
            'services' => 'required',
            'quantity' => 'required',
        ],
            [
                'user_id.required' => 'Bạn chưa chọn đối tác',
                'services.required' => 'Bạn chưa chọn dịch vụ',
                'quantity.required' => 'Bạn chưa chọn số lượng tem',

            ]);
        try {
            $data = $request->all();
            $replace = array('services' => implode(',', $data['services']));
            $data = array_replace($data, $replace);
            $data['created_at'] = date("Y-m-d h:i:s");
            $order = new Order;
            $order->user_id = (int)$data['user_id'];
            $order->quantity = $data['quantity'];
            $order->services = $data['services'];
            $order->created_at = $data['created_at'];
            $order->save();

            //Send Notification to Customer
            $notification = [];
            $notification['to'] = $order->user_id;
            $notification['message'] = "Admin iCheck đã tạo đơn hàng " . $order->id .
                " với số lượng " . $order->quantity . " tem vào lúc " . $order->created_at . "";
            $notification['notify_type'] = "CREATED_ORDER";
            $notification['target_id'] = $order->id;
            $this->api_qrcode->sendNotification($notification);

            return redirect()->route('Qrcode::order@index')
                ->with('success', 'Đã thêm mới đơn hàng thành công');
        } catch (Exception $ex) {
            return view('errors.404');
        }

    }

    public function update($id, Request $request)
    {
        if (auth()->user()->cannot('QRCODE-updateOrder')) {
            abort(403);
        }
        try {
            $data = $request->all();
            DB::connection('icheck_qrcode')->table('orders')
                ->where('id', $id)
                ->update(['quantity' => $data['quantity']]);
            return redirect()->back()
                ->with('success', 'Đã cập nhật đơn hàng thành công');
        } catch (Exception $ex) {
            return view('errors.404');
        }

    }

    public function getOrdersByAgency($collaborator_phone)
    {
        try {
            $query = [];
            $orders = DB::connection('icheck_qrcode')->table('orders')
                ->leftJoin('account', 'orders.user_id', '=', 'account.id')
                ->where('account.collaborator_phone', $collaborator_phone)
                ->select('orders.*', 'account.name as account_name', 'account.id as account_id')
                ->orderBy('orders.id', 'desc');
            $query['account_group'] = $collaborator_phone;
            $orders_id_list = $orders->pluck('orders.id')->toArray();
            $orders_filter = $orders->paginate(10)->appends($query);
            //stamp_ordered statistics
            $stamp_ordered_total = DB::connection('icheck_qrcode')->table('orders')
                ->select('orders.quantity')
                ->whereIn('orders.id', $orders_id_list)
                ->where('deleted_at', '=', null)
                ->sum('orders.quantity');
            $stamp_ordered_waiting_total = DB::connection('icheck_qrcode')->table('orders')
                ->select('orders.quantity')
                ->whereIn('orders.id', $orders_id_list)
                ->where('orders.status', '=', 0)
                ->where('deleted_at', '=', null)
                ->sum('quantity');
            $stamp_ordered_approved_total = DB::connection('icheck_qrcode')->table('orders')
                ->select('orders.quantity')
                ->whereIn('orders.id', $orders_id_list)
                ->where('orders.status', '=', 1)
                ->where('deleted_at', '=', null)
                ->sum('quantity');
            $stamp_ordered_disapproved_total = DB::connection('icheck_qrcode')->table('orders')
                ->select('orders.quantity')
                ->whereIn('orders.id', $orders_id_list)
                ->where('orders.status', '=', 2)
                ->where('deleted_at', '=', null)
                ->sum('quantity');
            //Order statistics
            $order_total = DB::connection('icheck_qrcode')->table('orders')
                ->whereIn('orders.id', $orders_id_list)
                ->where('deleted_at', '=', null)
                ->count();
            $order_approve_waiting_total = DB::connection('icheck_qrcode')->table('orders')
                ->whereIn('orders.id', $orders_id_list)
                ->where('orders.status', '=', 0)
                ->where('deleted_at', '=', null)
                ->count();
            $order_approved_total = DB::connection('icheck_qrcode')->table('orders')
                ->whereIn('orders.id', $orders_id_list)
                ->where('orders.status', '=', 1)
                ->where('deleted_at', '=', null)
                ->count();
            $order_disapproved_total = DB::connection('icheck_qrcode')->table('orders')
                ->whereIn('orders.id', $orders_id_list)
                ->where('orders.status', '=', 2)
                ->where('deleted_at', '=', null)
                ->count();
            $services = DB::connection('icheck_qrcode')->table('service_stamp')->select('id', 'name as service_name')->get();
            $accounts = DB::connection('icheck_qrcode')->table('account')
                ->select('id', 'name')
                ->where('account.status', '>', 0)
                ->get();
            $create_order_history = DB::connection('icheck_qrcode_agency')->table('create_order_history')
                ->select('create_order_history.*')->get();
            $approve_order_history = DB::connection('icheck_qrcode_agency')->table('approve_order_history')
                ->select('approve_order_history.*')->get();
            $users = DB::connection('icheck_qrcode_agency')->table('users')
                ->select('users.id', 'users.name', 'users.collaborator_phone')
                ->where('collaborator_id', '=', null)
                ->where('deleted_at', '=', null)
                ->get();
            return view('qrcode.order.index', ['orders' => $orders_filter, 'services' => $services,
                'accounts' => $accounts, 'create_order_history' => $create_order_history,
                'approve_order_history' => $approve_order_history, 'users' => $users,
                'stamp_ordered_total' => $stamp_ordered_total, 'stamp_ordered_waiting_total' => $stamp_ordered_waiting_total,
                'stamp_ordered_approved_total' => $stamp_ordered_approved_total, 'stamp_ordered_disapproved_total' => $stamp_ordered_disapproved_total,
                'order_total' => $order_total, 'order_approve_waiting_total' => $order_approve_waiting_total,
                'order_approved_total' => $order_approved_total, 'order_disapproved_total' => $order_disapproved_total,
                'collaborator_phone' => $collaborator_phone, 'query' => $query
            ]);
        } catch (Exception $ex) {
            return view('errors.404');
        }
    }

    public function export_excel(Request $request, Order $orders)
    {
        if (auth()->user()->cannot('QRCODE-exportExcelOrder')) {
            abort(403);
        }
        try {
            $orders = $orders->newQuery();
            $orders->leftJoin('account', 'orders.user_id', '=', 'account.id');
            $orders->select('orders.*', 'account.name as account_name', 'account.id as account_id')
                ->orderBy('orders.id', 'desc');

            //Lọc tuyệt đối
            if ($request->has('services')) {
                $services = Input::get('services');
                sort($services);
                $services = implode(",", $services);
                $orders->where('orders.services', '=', $services);
            }

            if ($request->has('name')) {
                $name = $request->input('name');
                $orders->where('account.name', 'like', '%' . $name . '%');
            }
            if ($request->input('payment_status') != null) {
                $payment_status = $request->input('payment_status');
                if ($payment_status == 0) {
                    $orders->where(
                        function ($query) {
                            $query->where('orders.payment_status', 0)
                                ->orWhereNull('orders.payment_status');
                        }
                    );
                } else {
                    $orders->where('orders.payment_status', $payment_status);
                }
            }

            if (($request->has('status')) && $request->input('status') != null) {
                $status = $request->input('status');
                $orders->where('orders.status', $status);
            }

            if (($request->has('from')) && $request->input('from') != null) {
                $from = $request->input('from');
                $orders->where('orders.created_at', '>=', date('Y-m-d' . ' 00:00:00', strtotime($from)));
            }


            if (($request->has('to')) && $request->input('to') != null) {
                $to = $request->input('to');
                $orders->where('orders.created_at', '<=', date('Y-m-d' . ' 23:59:59', strtotime($to)));
            }

            if ($request->has('user_created') && $request->input('user_created') != null) {
                $user_created = $request->input('user_created');
                if ($user_created == 'admin') {
                    $create_order_history = Create_Order_History::pluck('order_id')->toArray();
                    $orders->wherenotIn('orders.id', $create_order_history)
                        ->where('orders.user_created', "=", null);
                } else {
                    $create_order_history = Create_Order_History::where('collaborator_id', '=', $user_created)
                        ->pluck('order_id')->toArray();
                    $orders->whereIn('orders.id', $create_order_history)
                        ->where('orders.user_created', "=", null);
                }

            }
            if ($request->has('account_created') && $request->input('account_created') != null) {
                $account_created = $request->input('account_created');
                $orders->where('orders.user_created', $account_created);

            }
            if ($request->has('account_group') && $request->input('account_group') != null) {
                $account_group = $request->input('account_group');
                if ($account_group == 'icheck') {
                    $orders->where('account.collaborator_phone', '=', null);
                } else {
                    $orders->where('account.collaborator_phone', $account_group);
                }
            }
            if ($request->has('user_approve') && $request->input('user_approve') != null) {
                $user_approve = $request->input('user_approve');
                if ($user_approve == 'admin') {
                    $approve_order_history = Approve_Order_History::pluck('order_id')->toArray();
                    $orders->wherenotIn('orders.id', $approve_order_history)
                        ->where('orders.status', '=', 1);
                } else {
                    $approve_order_history = Approve_Order_History::where('collaborator_id', '=', $user_approve)
                        ->pluck('order_id')->toArray();
                    $orders->whereIn('orders.id', $approve_order_history);
                }
            }
            $orders_id_list = $orders->pluck('orders.id')->toArray();
            $orders_filter = $orders->get()->toArray();
            $services = DB::connection('icheck_qrcode')->table('service_stamp')->select('id', 'name as service_name')->get();
            $accounts = DB::connection('icheck_qrcode')->table('account')
                ->select('id', 'name')
                ->where('account.status', '>', 0)
                ->get();
            $create_order_history = DB::connection('icheck_qrcode_agency')->table('create_order_history')
                ->select('create_order_history.*')->get();
            $approve_order_history = DB::connection('icheck_qrcode_agency')->table('approve_order_history')
                ->select('approve_order_history.*')->get();
            $users = DB::connection('icheck_qrcode_agency')->table('users')
                ->select('users.id', 'users.name', 'users.collaborator_phone')
                ->where('collaborator_id', '=', null)
                ->where('deleted_at', '=', null)
                ->get();
            $stamp_ordered_total = DB::connection('icheck_qrcode')->table('orders')
                ->select('orders.quantity')
                ->whereIn('orders.id', $orders_id_list)
                ->where('deleted_at', '=', null)
                ->sum('orders.quantity');
            for ($i = 0; $i < count($orders_filter); $i++) {
                $orders_filter[$i]['user_approved'] = '';
                $order_services = explode(',', $orders_filter[$i]['services']);
                foreach ($services as $service) {
                    for ($j = 0; $j < count($order_services); $j++) {
                        if ($order_services[$j] == $service->id) {
                            $order_services[$j] = $service->service_name;
                        }
                    }
                }
                $orders_filter[$i]['services'] = implode(',', $order_services);

                //detect user_created
                $a = null;
                foreach ($create_order_history as $history) {
                    if ($orders_filter[$i]['id'] == $history->order_id)
                        $a = $history->collaborator_id;
                }

                if ($orders_filter[$i]['user_created'] == null && $a != null) {
                    foreach ($users as $user) {
                        if ($user->id == $a)
                            $orders_filter[$i]['user_created'] = $user->name;
                    }
                } elseif ($orders_filter[$i]['user_created'] == null && $a == null) {
                    $orders_filter[$i]['user_created'] = 'Admin iCheck';
                } else {
                    foreach ($accounts as $account) {
                        if ($orders_filter[$i]['user_created'] == $account->id)
                            $orders_filter[$i]['user_created'] = $account->name;
                    }
                }
                //detect user_approved
                if ($orders_filter[$i]['status'] != 0) {
                    $b = null;
                    foreach ($approve_order_history as $history) {
                        if ($orders_filter[$i]['id'] == $history->order_id)
                            $b = $history->collaborator_id;
                    }

                    if ($b != null) {
                        foreach ($users as $user) {
                            if ($user->id == $b)
                                $orders_filter[$i]['user_approved'] = $user->name;
                        }
                    } else {
                        $orders_filter[$i]['user_approved'] = 'Admin iCheck';
                    }
                }
                if ($orders_filter[$i]['status'] == 1) {
                    $orders_filter[$i]['status'] = 'Đã duyệt';
                } elseif ($orders_filter[$i]['status'] == 0) {
                    $orders_filter[$i]['status'] = 'Chờ duyệt';
                } else {
                    $orders_filter[$i]['status'] = 'Hủy duyệt';
                }
            }
            $proData = "";
            if (count($orders_filter) > 0) {
                if ($request->has('account_group') && $request->input('account_group') != null) {
                    $proData .= '<div style="text-align: center"><h2>CÔNG TY CỔ PHẦN ICHECK</h2></div>';
                    $proData .= '<div style="text-align: center"><h3>BẢNG ĐỐI SOÁT SỐ LƯỢNG ĐẶT HÀNG QR CODE</h3></div>';
                }
                if (($request->has('from')) && $request->input('from') != null) {
                    $from = $request->input('from');
                    $proData .= '<div style="text-align: center">' . '<span style="font-weight: bold">Từ   : </span> ' . date("d-m-y", strtotime($from)) . '</div>';
                }

                if (($request->has('to')) && $request->input('to') != null) {
                    $to = $request->input('to');
                    $proData .= '<div style="text-align: center">' . '<span style="font-weight: bold">Đến: </span> ' . date("d-m-y", strtotime($to)) . '</div>';
                }
                if ($request->has('account_group') && $request->input('account_group') != null) {
                    $account_group = $request->input('account_group');
                    if ($account_group == 'icheck') {
                        $proData .= '<div style="text-align: center">' . 'Nhóm khách hàng của: iCheck' . '</div>';
                    } else {
                        $agency = '';
                        foreach ($users as $user) {
                            if ($user->collaborator_phone == $account_group)
                                $agency = $user->name;
                        }

                        $proData .= '<div style="text-align: center">' . '<span style="font-weight: bold">Nhóm khách hàng của: </span>' . $agency . '</div>';
                    }
                }
                $proData .= '<table style="border: solid 1px #ddd;">
                            <tr>
                            <th style="border: solid 1px #ddd;">ID</th>
                            <th style="border: solid 1px #ddd;">Tên đối tác</th>
                            <th style="border: solid 1px #ddd;">Số lượng tem</th>
                            <th style="border: solid 1px #ddd;">Dịch vụ</th>
                            <th style="border: solid 1px #ddd;">Trạng thái</th>
                            <th style="border: solid 1px #ddd;">Ngày tạo</th>
                            <th style="border: solid 1px #ddd;">Người tạo</th>
                            <th style="border: solid 1px #ddd;">Người duyệt</th>
                            </tr>';
                foreach ($orders_filter as $order) {
                    $proData .= '
                    <tr>
                    <td style="border: solid 1px #ddd;">' . $order['id'] . '</td>
                    <td style="border: solid 1px #ddd;">' . $order['account_name'] . '</td>
                    <td style="border: solid 1px #ddd;">' . $order['quantity'] . '</td>
                    <td style="border: solid 1px #ddd;">' . $order['services'] . '</td>
                    <td style="border: solid 1px #ddd;">' . $order['status'] . '</td>
                    <td style="border: solid 1px #ddd;">' . date("d/m/Y", strtotime($order['created_at'])) . '</td>
                    <td style="border: solid 1px #ddd;">' . $order['user_created'] . '</td>
                    <td style="border: solid 1px #ddd;">' . $order['user_approved'] . '</td>
                    </tr>
                    ';
                }
                $proData .= '
             <tr>
                    <td style="border: solid 1px #ddd;"></td>
                    <td style="border: solid 1px #ddd;font-weight: bold">' . 'Tổng cộng số lượng tem' . '</td>
                    <td style="border: solid 1px #ddd;font-weight: bold">' . $stamp_ordered_total . '</td>
                    <td style="border: solid 1px #ddd;"></td>
                    <td style="border: solid 1px #ddd;"></td>
                    <td style="border: solid 1px #ddd;"></td>
                    <td style="border: solid 1px #ddd;"></td>
                    <td style="border: solid 1px #ddd;"></td>
                    </tr>
            ';
                $proData .= '</table>';

            }
            header('Content-Type: application/xls');
            header('Content-Disposition: attachment; filename=order_report.xls');
            echo $proData;
        } catch (Exception $ex) {
            return view('errors.404');
        }

    }

    public function updateOrderValue(Request $request)
    {
        if ($request->ajax()) {
            $order_id = $request->order_id;
            $order_value = $request->order_value;
            $order = Order::findOrFail($order_id);
            $order->update(['order_value' => $order_value]);
            return $order->order_value;
        }
        return '';
    }

    public function updatePrintValue(Request $request)
    {
        if ($request->ajax()) {
            $order_id = $request->order_id;
            $print_value = $request->print_value;
            $order = Order::findOrFail($order_id);
            $order->update(['print_value' => $print_value]);
            return $order->print_value;
        }
        return '';
    }

    public function updatePrintQuantity(Request $request)
    {
        if ($request->ajax()) {
            $order_id = $request->order_id;
            $print_quantity = $request->print_quantity;
            $order = Order::findOrFail($order_id);
            $order->update(['print_quantity' => $print_quantity]);
            return $order->print_quantity;
        }
        return '';
    }


    public function updatePaymentStatus($id, Request $request)
    {

        if (auth()->user()->cannot('QRCODE-updatePaymentStatus')) {
            abort(403);
        }
        $payment_status = $request->input('payment_status');
        $order = Order::findOrFail($id);
        $order->update(['payment_status' => $payment_status]);
        //Activity_log
        ActivityLog::create([
            "description" => "Update paymentStatus for an QRCODE-ORDER",
            "subject_id" => $id,
            "subject_type" => "App\Models\Qrcode\Order",
            "causer_id" => auth()->user()->id,
            "causer_type" => "users"
        ]);

        if ($order->payment_status == Order::PAYMENT_STATUS_TRUE && $order->status == Order::STATUS_PENDING) {
            $order->update(['status' => Order::STATUS_APPROVED]);
            $this->orderRepository->approve($id);
        }
        return $order->status;
    }
}
