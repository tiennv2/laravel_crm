<?php

namespace App\Http\Controllers\Qrcode;

use App\API\Api_qrcode;
use App\Http\Controllers\Controller;
use App\Models\Qrcode\Batch;
use App\Models\Qrcode\Template;
use App\Models\Qrcode\TemplateBatch;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Firebase\JWT\JWT;

class BatchController extends Controller
{
    private $api_qrcode;

    public function __construct()
    {
        $this->api_qrcode = new Api_qrcode();

    }

    public function index(Request $request, Batch $batch)
    {
        if (auth()->user()->cannot('QRCODE-viewBatch')) {
            abort(403);
        }
        try {
            $batches = $batch->newQuery();
            $query = [];
            $filtered_account_name = null;
            $batches->leftJoin('product', 'batch.product_id', '=', 'product.id')
                ->leftJoin('account', 'batch.user_id', '=', 'account.id')
                ->leftJoin('prefix_user', 'batch.prefix_id', '=', 'prefix_user.id')
                ->select('batch.*', 'product.name as product_name', 'product.approved_file as approved_file',
                    'prefix_user.prefix as prefix', 'account.name as account_name');
            //Lọc tuyệt đối
            if ($request->input('services')) {
                $services = Input::get('services');
                sort($services);
                $services = implode(",", $services);
                $batches->where('batch.services', '=', $services);
                $query['services'] = explode(",", $services);
            }

            if ($request->input('batch_name')) {
                $batch_name = $request->input('batch_name');
                $batches->where('batch.name', 'like', '%' . $batch_name . '%');
                $query['batch_name'] = $batch_name;
            }
            if ($request->input('account_id')) {
                $account_id = $request->input('account_id');
                $batches->where('account.id', $account_id);
                $query['account_id'] = $account_id;
                $filtered_account = DB::connection('icheck_qrcode')->table('account')
                    ->where('id', $account_id)->first();
                $filtered_account_name = $filtered_account->name;
            }

            if ($request->input('account_name')) {
                $account_name = $request->input('account_name');
                $batches->where('account.name', 'like', '%' . $account_name . '%');
                $query['account_name'] = $account_name;
            }

            if ($request->input('parent_id')) {
                $parent_id = $request->input('parent_id');
                $batches->where('batch.parent_id', '=', $parent_id);
                $query['parent_id'] = $parent_id;
            } else {
                $batches->where('batch.parent_id', '=', null);
            }


            if ($request->input('parent_name')) {
                $parent_name = $request->input('parent_name');
                $query['parent_name'] = $parent_name;
            }

            if ($request->input('status') && $request->input('status') != null) {
                $status = $request->input('status');
                if ($status == 'lock') {
                    $batches->where('batch.is_deleted', 1);
                } else if (intval($status) == 2) {
                    $batches->where(function ($query) {
                        $query->where('batch.status', 2)
                            ->orWhere('batch.active_left', '>', 0);
                    });
                } else {
                    $batches->where(function ($query) use ($status) {
                        $query->where('batch.status', $status)
                            ->where('batch.active_left', '=', null);
                    });
                }

                $query['status'] = $status;
            }

            if ($request->input('approved_file') === '0') {
                $batches->where(function ($sub_query) {
                    $sub_query->where([['product.approved_file', '=', null], ['batch.product_id', '>', 0]])
                        ->orWhere('product.approved_file', '=', 0);
                });

                $query['approved_file'] = '0';
            } elseif ($request->input('approved_file') > 0) {
                $approved_file = $request->input('approved_file');
                $batches->where('product.approved_file', $approved_file);
                $query['approved_file'] = $approved_file;
            }
            if ($request->input('attachment_status') !== null) {
                $attachment_status = $request->input('attachment_status');
                $attachments = DB::connection('icheck_qrcode')->table('product_attachments')
                    ->pluck('product_id')->toArray();
                if ($attachment_status == "0") {
                    $batches->whereNotIn('batch.product_id', $attachments)
                        ->where([['batch.product_id', '>', 0], ['batch.status', '=', 2], ['batch.user_id', '<>', 1]]);
                    $query['attachment_status'] = $attachment_status;
                } elseif ($attachment_status == 2) {
                    $date_before_30days = date('Y-m-d H:i:s', strtotime("-30 days"));
                    $batches->whereNotIn('batch.product_id', $attachments)
                        ->where([['batch.product_id', '>', 0], ['batch.status', '=', 2], ['batch.created_time', '<', $date_before_30days], ['batch.user_id', '<>', 1]]);
                    $query['attachment_status'] = $attachment_status;
                } else {
                    $batches->whereIn('batch.product_id', $attachments)
                        ->where([['batch.product_id', '<>', 0], ['batch.status', '=', 2]]);
                    $query['attachment_status'] = $attachment_status;
                }
            }

            if (($request->has('from')) && $request->input('from') != null) {
                $from = $request->input('from');
                $batches->where('batch.created_time', '>=', date('Y-m-d' . ' 00:00:00', strtotime($from)));
                $query['from'] = $from;
            }


            if (($request->has('to')) && $request->input('to') != null) {
                $to = $request->input('to');
                $batches->where('batch.created_time', '<=', date('Y-m-d' . ' 23:59:59', strtotime($to)));
                $query['to'] = $to;
            }

            $batches = $batches->orderBy('batch.id', 'desc')->paginate(10)->appends($query);
            foreach ($batches as $batch) {
                $template_batches_id = TemplateBatch::where([['batch_id', '=', $batch->id]])
                    ->pluck('template_id')
                    ->toArray();
                $templates = Template::whereIn('id', $template_batches_id)->get();
                $batch->templates = $templates;
            }

            $product_attachments = DB::connection('icheck_qrcode')->table('product_attachments')
                ->select("product_attachments.product_id", "product_attachments.path")
                ->get();

            $services = DB::connection('icheck_qrcode')->table('service_stamp')->select('id', 'name as service_name')->get();

            return view('qrcode.batch.index', ['batches' => $batches, 'services' => $services,
                'product_attachments' => $product_attachments, 'query' => $query, 'filtered_account_name' => $filtered_account_name]);
        } catch (Exception $ex) {
            return view('errors.404');
        }
    }

    public function lock_batch($id)
    {
        if (auth()->user()->cannot('QRCODE-lockBatch')) {
            abort(403);
        }
        try {
            $batch = Batch::findOrFail($id);
            $batch_name = $batch->name;
            $batch->update(['is_deleted' => 1, 'deleted_time' => date('Y-m-d h:i:s')]);
            //Send Notification to Customer
            $notification = [];
            $notification['to'] = $batch->user_id;
            $notification['message'] = "Lô tem $batch_name đã bị khóa";
            $notification['notify_type'] = "BATCH_LOCKED";
            $notification['target_id'] = $batch->id;
            $this->api_qrcode->sendNotification($notification);

            return redirect()->back()
                ->with('success', "Đã khóa lô tem $batch_name");
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

    public function unlock_batch($id)
    {
        if (auth()->user()->cannot('QRCODE-unlockBatch')) {
            abort(403);
        }
        try {
            $batch = Batch::findOrFail($id);
            $batch_name = $batch->name;
            $batch->update(['is_deleted' => 0, 'deleted_time' => null]);
            return redirect()->back()
                ->with('success', "Đã mở khóa lô tem $batch_name");
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

    public function download(Request $request)
    {
        if (auth()->user()->cannot('QRCODE-downloadBatch')) {
            abort(403);
        }
        $this->validate($request, [
            'fields' => 'required',
        ],
            [
                'fields.required' => 'Bạn chưa chọn cột nào!',
            ]);
        $params = $request->only(['start', 'end', 'batch_id', 'prefix', 'fields']);
        $params['domain'] = config('api.batch_download_domain');
        $params['fields'] = implode(",", $params['fields']);

        $key = config('api.batch_download_key');
        $token = array(
            "batch_id" => $params['batch_id'],
            "iat" => time() - 30,
            "nbf" => 3600
        );
        $jwt = JWT::encode($token, $key);
        $params['access_token'] = $jwt;
        $query = http_build_query($params);
        $download_link = config('api.batch_download') . 'encode/download?' . $query;
        return redirect($download_link);
    }
}
