<?php

namespace App\Http\Controllers\Qrcode;

use App\API\Api_qrcode;
use App\Http\Controllers\Controller;
use App\Mail\NoticeMail;
use App\Models\Qrcode\Account;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;

class CompanyController extends Controller
{
    private $api_qrcode;

    public function __construct()
    {
        $this->api_qrcode = new Api_qrcode();

    }

    public function index(Request $request, Account $account)
    {
        if (auth()->user()->cannot('QRCODE-viewBusiness') && auth()->user()->cannot('QRCODE-addBusiness')) {
            abort(403);
        }
        try {
            if ($request->has('status') && $request->input('status') == 'lock') {
                $account = $account->withTrashed()->newQuery();
            } else {
                $account = $account->newQuery();
            }
            $query = [];
            $account->leftJoin('g_city', 'account.city_id', '=', 'g_city.id')->leftJoin('g_district', 'account.district_id', '=', 'g_district.id');
            $account->select('account.*', 'g_city.name as city_name', 'g_district.name as district_name')
                ->where('type', 1);
            if ($request->has('account_name') && $request->input('account_name') != null) {
                $account_name = $request->input('account_name');
                $account->where([['account.name', 'like', '%' . $account_name . '%']]);
                $query['account_name'] = $account_name;
            }

            if ($request->has('address') && $request->input('address') != null) {
                $address = $request->input('address');
                $account->where([['account.address', 'like', '%' . $address . '%']]);
                $query['address'] = $address;
            }

            if ($request->has('phone') && $request->input('phone') != null) {
                $phone = $request->input('phone');
                $account->where([['account.phone', 'like', '%' . $phone . '%']]);
                $query['phone'] = $phone;
            }

            if ($request->has('email') && $request->input('email') != null) {
                $email = $request->input('email');
                $account->where([['account.email', 'like', '%' . $email . '%']]);
                $query['email'] = $email;
            }

            if ($request->has('website') && $request->input('website') != null) {
                $website = $request->input('website');
                $account->where([['account.website', 'like', '%' . $website . '%']]);
                $query['website'] = $website;
            }

            if ($request->has('account_city_id') && $request->input('account_city_id') != null) {
                $account_city_id = $request->input('account_city_id');
                $account->where([['account.city_id', $account_city_id]]);
                $query['account_city_id'] = $account_city_id;
                $districts = DB::connection('icheck_qrcode')->table('g_district')
                    ->select("name", "id")->where('city_id', $account_city_id)->get();
            }

            if ($request->has('account_district_id') && $request->input('account_district_id') != null) {
                $account_district_id = $request->input('account_district_id');
                $account->where([['account.district_id', $account_district_id]]);
                $query['account_district_id'] = $account_district_id;
            }
            if ($request->has('status') && $request->input('status') != null) {
                $status = $request->input('status');
                if ($status == 'lock') {
                    $account->where('deleted_at', '<>', null);
                    $query['status'] = $status;
                } else {
                    $account->where([['status', $status]]);
                    $query['status'] = $status;
                }
            }
            if ($request->has('from') && $request->input('from') != null) {
                $from = $request->input('from');
                $account->where([['account.created_at', '>=', date('Y-m-d' . ' 00:00:00', strtotime($from))]]);
                $query['from'] = $from;
            }
            if ($request->has('to') && $request->input('to') != null) {
                $to = $request->input('to');
                $account->where([['account.created_at', '<=', date('Y-m-d' . ' 23:59:59', strtotime($to))]]);
                $query['to'] = $to;
            }

            if ($request->has('user_created') && $request->input('user_created') != null) {
                $user_created = $request->input('user_created');
                if ($user_created == 'admin') {
                    $account->where([['collaborator_phone', null]]);
                    $query['user_created'] = $user_created;
                } else {
                    $account->where([['collaborator_phone', $user_created]]);
                    $query['user_created'] = $user_created;
                }
            }

            $account_id_list = $account->pluck('account.id')->toArray();
            $companies = $account->orderBy('account.id', 'desc')->paginate(10)->appends($query);
            $cities = DB::connection('icheck_qrcode')->table('g_city')->get();

            $company_total = DB::connection('icheck_qrcode')->table('account')
                ->whereIn('account.id', $account_id_list)
                ->where([['deleted_at', '=', null], ['type', '=', 1]])->count();
            $company_active_waiting_total = DB::connection('icheck_qrcode')->table('account')
                ->whereIn('account.id', $account_id_list)
                ->where([['deleted_at', '=', null], ['type', '=', 1], ['status', '=', 0]])->count();
            $company_actived_total = DB::connection('icheck_qrcode')->table('account')
                ->whereIn('account.id', $account_id_list)
                ->where([['deleted_at', '=', null], ['type', '=', 1], ['status', '=', 1]])->count();
            $company_deactived_total = DB::connection('icheck_qrcode')->table('account')
                ->whereIn('account.id', $account_id_list)
                ->where([['deleted_at', '=', null], ['type', '=', 1], ['status', '=', 2]])->count();

            $product_attachments = DB::connection('icheck_qrcode')->table('product_attachments')
                ->pluck('product_id')->toArray();
            $active_batches = DB::connection('icheck_qrcode')->table('batch')
                ->where([['batch.status', '=', 2], ['batch.active_left', '=', null]])
                ->get();
            $active_batches_without_attachments = DB::connection('icheck_qrcode')->table('batch')
                ->whereNotIn('batch.product_id', $product_attachments)
                ->where([['batch.status', '=', 2], ['batch.active_left', '=', null], ['batch.product_id', '>', 0]])
                ->get();
            $date_before_30days = date('Y-m-d H:i:s', strtotime("-30 days"));
            $active_batches_without_attachments_after_30days = DB::connection('icheck_qrcode')->table('batch')
                ->whereNotIn('batch.product_id', $product_attachments)
                ->where([['batch.status', '=', 2], ['batch.parent_id', '=', null],['batch.user_id', '<>', 1],
                    ['batch.product_id', '>', 0], ['batch.created_time', '<', $date_before_30days]])
                ->count();
            $collaborators = DB::connection('icheck_qrcode_agency')->table('users')
                ->where('deleted_at', '=', null)
                ->where('collaborator_id', '=', null)
                ->select('users.id', 'users.name', 'users.collaborator_phone')->get();

            if (isset($districts)) {

                return view('qrcode.company.index', ['companies' => $companies, 'cities' => $cities, 'districts' => $districts,
                    'company_total' => $company_total, 'company_active_waiting_total' => $company_active_waiting_total,
                    'company_actived_total' => $company_actived_total, 'company_deactived_total' => $company_deactived_total,
                    'collaborators' => $collaborators, 'active_batches' => $active_batches,
                    'active_batches_without_attachments' => $active_batches_without_attachments,
                    'active_batches_without_attachments_after_30days' => $active_batches_without_attachments_after_30days]);
            } else {
                return view('qrcode.company.index', ['companies' => $companies, 'cities' => $cities,
                    'company_total' => $company_total, 'company_active_waiting_total' => $company_active_waiting_total,
                    'company_actived_total' => $company_actived_total, 'company_deactived_total' => $company_deactived_total,
                    'collaborators' => $collaborators, 'active_batches' => $active_batches,
                    'active_batches_without_attachments' => $active_batches_without_attachments,
                    'active_batches_without_attachments_after_30days' => $active_batches_without_attachments_after_30days
                ]);
            }
        } catch (Exception $exception) {
            return view('errors.404');
        }

    }


    public function new_company(Request $request, Account $account)
    {
        if (auth()->user()->cannot('QRCODE-viewNewBusinessRegister')) {
            abort(403);
        }
        try {
            $account = $account->newQuery();
            $query = [];
            $account->leftJoin('g_city', 'account.city_id', '=', 'g_city.id')->leftJoin('g_district', 'account.district_id', '=', 'g_district.id');
            $account->select('account.*', 'g_city.name as city_name', 'g_district.name as district_name')
                ->where([['type', 1], ['status', '=', 0], ['deleted_at', '=', null]]);
            if ($request->has('account_name') && $request->input('account_name') != null) {
                $account_name = $request->input('account_name');
                $account->where('account.name', 'like', '%' . $account_name . '%');
                $query['account_name'] = $account_name;
            }

            if ($request->has('address') && $request->input('address') != null) {
                $address = $request->input('address');
                $account->where('account.address', 'like', '%' . $address . '%');
                $query['address'] = $address;
            }

            if ($request->has('phone') && $request->input('phone') != null) {
                $phone = $request->input('phone');
                $account->where('account.phone', 'like', '%' . $phone . '%');
                $query['phone'] = $phone;
            }

            if ($request->has('email') && $request->input('email') != null) {
                $email = $request->input('email');
                $account->where('account.email', 'like', '%' . $email . '%');
                $query['email'] = $email;
            }

            if ($request->has('website') && $request->input('website') != null) {
                $website = $request->input('website');
                $account->where('account.website', 'like', '%' . $website . '%');
                $query['website'] = $website;
            }

            if ($request->has('account_city_id') && $request->input('account_city_id') != null) {

                $account_city_id = $request->input('account_city_id');
                $account->where('account.city_id', $account_city_id);
                $query['account_city_id'] = $account_city_id;
                $districts = DB::connection('icheck_qrcode')->table('g_district')->select("name", "id")->where('city_id', $account_city_id)->get();
            }

            if ($request->has('account_district_id') && $request->input('account_district_id') != null) {
                $account_district_id = $request->input('account_district_id');
                $account->where('account.district_id', $account_district_id);
                $query['account_district_id'] = $account_district_id;
            }

            if ($request->has('from') && $request->input('from') != null) {
                $from = $request->input('from');
                $account->where('account.created_at', '>=', date('Y-m-d' . ' 00:00:00', strtotime($from)));
                $query['from'] = $from;
            }
            if ($request->has('to') && $request->input('to') != null) {
                $to = $request->input('to');
                $account->where('account.created_at', '<=', date('Y-m-d' . ' 23:59:59', strtotime($to)));
                $query['to'] = $to;
            }

            if ($request->has('user_created') && $request->input('user_created') != null) {
                $user_created = $request->input('user_created');
                if ($user_created == 'admin') {
                    $account->where('collaborator_phone', null);
                    $query['user_created'] = $user_created;
                } else {
                    $account->where('collaborator_phone', $user_created);
                    $query['user_created'] = $user_created;
                }
            }

            $companies = $account->orderBy('account.id', 'desc')->paginate(10)->appends($query);
            $cities = DB::connection('icheck_qrcode')->table('g_city')->get();
            $product_attachments = DB::connection('icheck_qrcode')->table('product_attachments')
                ->pluck('product_id')->toArray();
            $collaborators = DB::connection('icheck_qrcode_agency')->table('users')
                ->where('deleted_at', '=', null)
                ->where('collaborator_id', '=', null)
                ->select('users.id', 'users.name', 'users.collaborator_phone')->get();

            if (isset($districts)) {
                return view('qrcode.company.new_company', ['companies' => $companies, 'cities' => $cities, 'districts' => $districts,
                    'collaborators' => $collaborators, 'product_attachments' => $product_attachments]);
            } else {
                return view('qrcode.company.new_company', ['companies' => $companies, 'cities' => $cities,
                    'collaborators' => $collaborators, 'product_attachments' => $product_attachments]);
            }
        } catch (Exception $exception) {
            return view('errors.404');
        }

    }

    public function add()
    {
        if (auth()->user()->cannot('QRCODE-addBusiness')) {
            abort(403);
        }
        try {
            $cities = DB::connection('icheck_qrcode')->table('g_city')->get();
            $districts = DB::connection('icheck_qrcode')->table('g_district')->get();
            return view('qrcode.company.form', array('cities' => $cities, 'districts' => $districts));
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

    public function edit($id)
    {
        if (auth()->user()->cannot('QRCODE-updateBusiness')) {
            abort(403);
        }
        try {
            $company = Account::findOrFail($id);
            $cities = DB::connection('icheck_qrcode')->table('g_city')->get();
            $districts = DB::connection('icheck_qrcode')->table('g_district')
                ->where("g_district.city_id", "=", $company->city_id)
                ->get();
            return view('qrcode.company.form', array('cities' => $cities, 'districts' => $districts, 'company' => $company));
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

    public function store(Request $request)
    {
        if (auth()->user()->cannot('QRCODE-addBusiness')) {
            abort(403);
        }
        $this->validate($request, [
            'username' => 'required|unique:icheck_qrcode.account',
            'name' => 'required',
            'address' => 'required',
            'city_id' => 'required',
            'district_id' => 'required',
            'phone' => 'required|regex:/^[0-9]{9,11}$/|unique:icheck_qrcode.account',
            'email' => 'required|unique:icheck_qrcode.account',
            'password' => 'required|min:6',
            'confirm_password' => 'required|same:password'
        ],
            [
                'username.unique' => 'Tên đăng nhập đã tồn tại!',
                'phone.unique' => 'Số điện thoại đã tồn tại!',
                'phone.regex' => 'Số điện thoại phải bao gồm các chữ số từ 0 đến 9 và có từ 9 đến 11 chữ số',
                'email.unique' => 'Email đã tồn tại!',
                'city_id.required' => 'Chưa chọn thành phố/tỉnh',
                'district_id.required' => 'Chưa chọn quận/huyện!',
                'confirm_password.same' => 'Mật khẩu xác nhận không trùng khớp',
                'password.min' => 'Mật khẩu có độ dài ít nhất 6 ký tự',
            ]);

        $company = $request->all();
        unset($company['_token']);
        if ($company['website'] == null) {
            unset($company['website']);
        }

        try {
            $this->api_qrcode->addCompany($company);
            return redirect()->route('Qrcode::company@index')
                ->with('success', 'Đã thêm mới doanh nghiệp thành công');

        } catch (Exception $exception) {
            return view('errors.404');
        }

    }

    public function update($id, Request $request)
    {
        if (auth()->user()->cannot('QRCODE-updateBusiness')) {
            abort(403);
        }
        $this->validate($request, [
            'name' => 'required',
            'address' => 'required',
            'city_id' => 'required',
            'district_id' => 'required',
            'phone' => 'required|regex:/^[0-9]{9,11}$/|unique:icheck_qrcode.account,phone,' . $id,
            'email' => 'required|unique:icheck_qrcode.account,email,' . $id,
            'password' => 'nullable|min:6',
            'confirm_password' => 'same:password'
        ],
            [
                'phone.unique' => 'Số điện thoại đã tồn tại!',
                'phone.regex' => 'Số điện thoại phải bao gồm các chữ số từ 0 đến 9 và có từ 9 đến 11 chữ số',
                'email.unique' => 'Email đã tồn tại!',
                'city_id.required' => 'Chưa chọn thành phố/tỉnh',
                'district_id.required' => 'Chưa chọn quận/huyện!',
                'confirm_password.same' => 'Mật khẩu xác nhận không trùng khớp',
                'password.min' => 'Mật khẩu có độ dài ít nhất 6 ký tự',
            ]);

        $company = $request->only(['name', 'address', 'city_id', 'district_id', 'phone', 'email']);
        if ($request->has('password') and $request->input('password')) {
            $company['password'] = $request->input('password');
            $company['confirm_password'] = $request->input('confirm_password');
        }
        try {
            $this->api_qrcode->updateCompany($company, $id);
            return redirect()->back()
                ->with('success', 'Đã cập nhật thông tin doanh nghiệp thành công');

        } catch (Exception $exception) {
            return view('errors.404');
        }

    }

    public function approve($id)
    {
        if (auth()->user()->cannot('QRCODE-approveBusiness')) {
            abort(403);
        }
        try {
            DB::connection('icheck_qrcode')->table('account')
                ->where('id', $id)
                ->update(['status' => 1]);
            $company = Account::find($id);
            $company_email = str_replace(' ', '', $company->email);
            Mail::to($company_email)->send(new NoticeMail($company));
            return redirect()->back()
                ->with('success', 'Đã kích hoạt thành công');
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }


    public function disapprove($id)
    {
        if (auth()->user()->cannot('QRCODE-disapproveBusiness')) {
            abort(403);
        }
        try {
            DB::connection('icheck_qrcode')->table('account')
                ->where('id', $id)
                ->update(['status' => 2]);
            return redirect()->back()
                ->with('success', 'Đã hủy kích hoạt thành công');
        } catch (Exception $exception) {
            return view('errors.404');
        }

    }

    public function lock($id)
    {
        if (auth()->user()->cannot('QRCODE-lockBusiness')) {
            abort(403);
        }
        try {
            $company = Account::findOrFail($id);
            $company_name = $company->name;
            $company->delete();
            return redirect()->back()->with("success", "Đã khóa tài khoản của doanh nghiệp $company_name");
        } catch (Exception $exception) {
            return view('errors.404');
        }

    }

    public function unlock($id)
    {
        if (auth()->user()->cannot('QRCODE-unlockBusiness')) {
            abort(403);
        }
        try {
            $company = Account::withTrashed()->findOrFail($id);
            $company_name = $company->name;
            $company->restore();
            return redirect()->back()->with("success", "Đã mở khóa tài khoản của doanh nghiệp $company_name");
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }


    public function inbox($id, Request $request)
    {
        if (auth()->user()->cannot('QRCODE-inboxBusiness')) {
            abort(403);
        }
        $inbox = $request->all();
        unset($inbox['_token']);
        $inbox['to'] = $id;
        $company = Account::findOrFail($id);
        $company_name = $company->name;
        try {
            $this->api_qrcode->sendInbox($inbox);
            return redirect()->back()
                ->with('success', "Đã gửi thông báo đến doanh nghiệp  '$company_name'");

        } catch (Exception $exception) {
            return view('errors.404');
        }

    }

    public function note($id, Request $request)
    {
        if (auth()->user()->cannot('QRCODE-noteBusiness')) {
            abort(403);
        }
        $note = $request->all()['note'];
        try {
            DB::connection('icheck_qrcode')->table('account')
                ->where('id', $id)
                ->update(['note' => $note]);
            return redirect()->back()
                ->with('success', 'Đã thêm ghi chú thành công');
        } catch (Exception $exception) {
            return view('errors.404');
        }

    }

    public function getInboxList(Request $request)
    {
        if (auth()->user()->cannot('QRCODE-viewInboxBusiness')) {
            abort(403);
        }
        try {
            $messages = $this->api_qrcode->getMessageListByUser([
                'user' => $request->input('user'),
                'limit' => $request->input('limit')
            ]);
            return $messages;
        } catch (Exception $exception) {
            return "Connect error";
        }

    }

}
