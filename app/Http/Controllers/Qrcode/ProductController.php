<?php

namespace App\Http\Controllers\Qrcode;

use App\API\Api_iCheck_backend;
use App\Events\ProductFileAgreement;
use App\Http\Controllers\Controller;
use App\Models\Qrcode\Product;
use App\Models\Qrcode\Product_Attachments;
use App\Repository\Qrcode\ProductRepositoryInterface;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;

class ProductController extends Controller
{
    private $api_icheck_backend;
    private $productRepository;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->api_icheck_backend = new Api_iCheck_backend();
        $this->productRepository = $productRepository;
    }

    public function approve_product_file($product_id)
    {
        if (auth()->user()->cannot('QRCODE-approveProduct')) {
            abort(403);
        }
        try {
            $product = $this->productRepository->getProduct($product_id);
            Event::fire(new ProductFileAgreement($product));
            return redirect()->back()->with('success', 'Đã duyệt files sản phẩm thành công');
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

    public function disapprove_product_file($id)
    {
        if (auth()->user()->cannot('QRCODE-disapproveProduct')) {
            abort(403);
        }
        try {
            $product = Product::findOrFail($id);
//            DB::connection('icheck_qrcode')->table('product')
//                ->where('id', $id)
//                ->update(['approved_file' => 0]);
            if (starts_with($product->sku,'IQ' )) {
                $res = $this->api_icheck_backend->productVerify(['ids' => [$product->sku], 'expired_at' => date('c', strtotime("-1 days"))]);
                if ($res['status'] == 200) {
                    $product->approved_file = 0;
                    $product->save();
                } else {
                    echo 'Lỗi hủy xác thực Sản phẩm!';
                    dd($res);
                }
            } else {
                $product->approved_file = 0;
                $product->save();
            }
            return redirect()->back()
                ->with('success', 'Đã bỏ duyệt files sản phẩm');
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

    public function get_upload_product_file($product_id)
    {
        try {
            $product = $this->productRepository->getProduct($product_id);
            $vendor = $this->productRepository->getVendorByProduct($product->vendor_id);
            $product_info = $this->productRepository->getProductInfo($product_id);
            $product_attachments = DB::connection('icheck_qrcode')
                ->table('product_attachments')
                ->where('product_id', $product_id)
                ->pluck('path')->toArray();
            return view('qrcode.product.upload_product_file', ['product_attachments' => $product_attachments, 'product' => $product,
                'vendor' => $vendor, 'product_info' => $product_info]);
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

    public function post_upload_product_file(Request $request, $product_id)
    {
        if (auth()->user()->cannot('QRCODE-uploadProductFiles')) {
            abort(403);
        }
        $this->validate($request, [
            'attachments.*' => 'max:20480|mimes:jpg,jpeg,bmp,png,xls,xlsx,pdf,doc,docx,zip,rar',
        ],
            [
                'attachments.*.max' => 'Một file upload không quá 20MB',
                'attachments.*.mimes' => 'File upload phải là một tập tin có định dạng: jpg, jpeg, bmp, png, xls, xlsx, pdf, doc, docx',
            ]);

        try {
            $data = $request->all();
            $path = "upload";
            $current_product_attachments = DB::connection('icheck_qrcode')
                ->table('product_attachments')
                ->where('product_id', $product_id)
                ->pluck('path')->toArray();
            $remove_files = explode(",", $data['remove_files']);
            $upload_files = [];
            if ($request->hasFile('attachments')) {
                $files = $request->file('attachments');
                if (count($files) + count($current_product_attachments) - count($remove_files) > 4) {
                    return redirect()->back()
                        ->withInput($request->only('attachments_over'))
                        ->withErrors(['attachments_over' => 'Không được upload quá 5 files. Tổng số file upload thêm và đã upload vượt quá 5!']);
                }

                for ($i = 0; $i < count($files); $i++) {
                    $filename = $files[$i]->getClientOriginalName();
                    $files[$i]->move($path, $filename);
                    $client = new Client();
                    try {
                        $res = $client->request(
                            'POST',
                            'https://upload.icheck.com.vn/v1/file?uploadType=simple&responseType=json',
                            [
                                'multipart' => [
                                    ['name' => 'content', 'contents' => fopen("upload/$filename", 'r')]
                                ],
                            ]
                        );
                        $res = json_decode((string)$res->getBody());
                    } catch (RequestException $e) {
                        return $e->getResponse()->getBody();
                    }
                    $upload_files[$i] = $res->path;
                    $upload_files[$i] = str_replace(',', '_', $upload_files[$i]);
                    unlink($path . "/" . $filename);
                }
            }
            $remain_product_attachments = [];
            if ($request->has('remove_files') && $data['remove_files'] != '') {
                $remain_product_attachments = array_diff($current_product_attachments, $remove_files);
                if ($request->hasFile('attachments') && (count($request->file('attachments')) + count($remain_product_attachments) > 5)) {
                    return redirect()->back()
                        ->withInput($request->only('attachments_over'))
                        ->withErrors(['attachments_over' => 'Số file đã upload vượt quá 5']);
                }

            }
            if ($remove_files[0] == '' && !empty($current_product_attachments)) {
                $files_total = array_merge($upload_files, $current_product_attachments);
            } elseif ($remove_files[0] == '' && empty($current_attachments_array)) {
                $files_total = $upload_files;
            } else {
                $files_total = array_merge($upload_files, $remain_product_attachments);
            }

            if (empty($files_total)) {
                $data['attachments'] = [];
            } else {
                $data['attachments'] = $files_total;
            }
            unset ($data['remove_files']);
            unset ($data['_token']);
            Product_Attachments::where('product_id', $product_id)->delete();
            foreach ($data['attachments'] as $attachment) {
                $product_attachment = new Product_Attachments();
                $product_attachment->product_id = $product_id;
                $product_attachment->path = $attachment;
                $product_attachment->save();
            }

            return redirect()->back()
                ->with('success', 'Đã cập nhật files thành công');
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }
}
