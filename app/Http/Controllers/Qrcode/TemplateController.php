<?php

namespace App\Http\Controllers\Qrcode;

use App\Http\Controllers\Controller;
use App\Models\Qrcode\Template;
use App\Models\Qrcode\TemplateBatch;
use Exception;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use GuzzleHttp\Client;

class TemplateController extends Controller
{
    public function index(Request $request)
    {
        if (auth()->user()->cannot('QRCODE-viewTemplate') && auth()->user()->cannot('QRCODE-addTemplate')) {
            abort(403);
        }
        try {
            $templates = new Template();
            $templates = $templates->newQuery();
            $templates->select('templates.*')->where('deleted_at', null);
            $query = [];
            if ($request->has('name') && $request->input('name') != null) {
                $name = $request->input('name');
                $templates->where('name', 'like', '%' . $name . '%');
                $query['name'] = $name;
            }
            if ($request->has('account_id') && $request->input('account_id') != null) {
                $account_id = $request->input('account_id');
                $templates->where('account_id', $account_id);
                $query['account_id'] = $account_id;
            }
            $templates = $templates->orderBy('id', 'desc')->paginate(10)->appends($query);
            $accounts = DB::connection('icheck_qrcode')->table('account')
                ->where([['type', '=', 1], ['status', '=', 1], ['deleted_at', '=', null]])
                ->select('account.*')
                ->orderBy('account.id', 'desc')
                ->get();
            return view('qrcode.template.index', ['templates' => $templates, 'accounts' => $accounts]);
        } catch (Exception $ex) {
            return view("errors.404");
        }
    }

    public function getBatchesByAccount(Request $request)
    {
        if ($request->ajax()) {
            $batches = DB::connection('icheck_qrcode')->table('batch')
                ->leftJoin('account', 'batch.user_id', '=', 'account.id')
                ->select('batch.id', 'batch.name')
                ->where([['batch.user_id', '=', $request->account_id], ['batch.status', '=', 2], ['batch.active_left', '=', null]])
                ->orderBy('batch.id', 'desc')
                ->get();
            return response()->json($batches);
        }
        return 'error';
    }

    public function getBatchesByTemplate(Request $request)
    {
        if ($request->ajax()) {
            $template_batches = TemplateBatch::where([['template_id', '=', $request->template_id]])
                ->pluck('batch_id')
                ->toArray();
            $batches = DB::connection('icheck_qrcode')->table('batch')
                ->select('batch.id', 'batch.name', 'batch.quantity', 'batch.created_time')
                ->whereIn('batch.id', $template_batches)
                ->orderBy('batch.id', 'desc')
                ->get();
            foreach ($batches as $batch) {
                $batch->quantity = number_format($batch->quantity, 0, ".", ",");
                $batch->created_time = date("d/m/Y", strtotime($batch->created_time));
            }

            return response()->json($batches);
        }
        return 'error';
    }


    public function add()
    {
        if (auth()->user()->cannot('QRCODE-addTemplate')) {
            abort(403);
        }
        try {
            $accounts = DB::connection('icheck_qrcode')->table('account')
                ->select('account.id', 'account.name')
                ->where([['type', '=', 1], ['status', '=', 1], ['deleted_at', '=', null]])
                ->orderBy('account.id', 'desc')
                ->get();
            return view('qrcode.template.form', array('accounts' => $accounts));
        } catch (Exception $ex) {
            return view("errors.404");
        }
    }

    public function edit($id)
    {
        if (auth()->user()->cannot('QRCODE-updateTemplate')) {
            abort(403);
        }
        $template = Template::findOrFail($id);
        try {
            $accounts = DB::connection('icheck_qrcode')->table('account')
                ->select('account.id', 'account.name')
                ->where([['type', '=', 1], ['status', '=', 1], ['deleted_at', '=', null]])
                ->orderBy('account.id', 'desc')
                ->get();
            $batches = DB::connection('icheck_qrcode')->table('batch')
                ->leftJoin('account', 'batch.user_id', '=', 'account.id')
                ->select('batch.id', 'batch.name')
                ->where([['batch.user_id', '=', $template->account_id], ['batch.status', '=', 2], ['batch.active_left', '=', null]])
                ->orderBy('batch.id', 'desc')
                ->get();
            $template_batches = TemplateBatch::where([['template_id', '=', $id]])
                ->pluck('batch_id')
                ->toArray();
            return view('qrcode.template.form', array('accounts' => $accounts, 'batches' => $batches,
                'template' => $template, 'template_batches' => $template_batches));
        } catch (Exception $ex) {
            return view("errors.404");
        }
    }

    public function update($id, Request $request)
    {
        if (auth()->user()->cannot('QRCODE-updateTemplate')) {
            abort(403);
        }
        $this->validate($request, [
            'name' => 'required',
            'attachments.*' => 'max:10240|mimes:jpg,jpeg,ai,pdf,gif,png,zip,rar',
        ],
            [
                'name.required' => 'Chưa điền tên thiết kế',
                'attachments.*.mimes' => 'File upload phải là một tập tin có định dạng: jpg, gif,ai,png,zip,rar',
            ]);

        try {
            $data = $request->only(['name', 'account_id', 'attachments']);
            $path = "upload";

            $template = Template::findOrFail($id);
            $current_attachments = $template->attachments;
            if ($current_attachments == '') {
                $current_attachments_array = [];
            } else {
                $current_attachments_array = explode(",", $current_attachments);
            }

            if ($request->remove_files == '') {
                $remove_files = [];
            } else {
                $remove_files = explode(",", $request->remove_files);
            }
            $upload_files = [];
            if ($request->hasFile('attachments')) {
                $files = $request->file('attachments');
                if (count($files) + count($current_attachments_array) - count($remove_files) > 5) {
                    return redirect()->back()
                        ->withInput($request->only('attachments_over'))
                        ->withErrors(['attachments_over' => 'Không được upload quá 5 files. Tổng số file upload thêm và đã upload vượt quá 5!']);
                }
                for ($i = 0; $i < count($files); $i++) {
                    $filename = $files[$i]->getClientOriginalName();
                    $files[$i]->move($path, $filename);
                    $client = new Client();
                    try {
                        $res = $client->request(
                            'POST',
                            'https://upload.icheck.com.vn/v1/file?uploadType=simple&responseType=json',
                            [
                                'multipart' => [
                                    ['name' => 'content', 'contents' => fopen("upload/$filename", 'r')]
                                ],
                            ]
                        );
                        $res = json_decode((string)$res->getBody());
                    } catch (RequestException $e) {
                        return $e->getResponse()->getBody();
                    }
                    $upload_files[$i] = $res->path;
                    $upload_files[$i] = str_replace(',', '_', $upload_files[$i]);
                    unlink($path . "/" . $filename);
                }
            }
            $remain_attachments_array = [];
            if ($request->has('remove_files') && $request->remove_files != '') {
                $remain_attachments_array = array_diff($current_attachments_array, $remove_files);
                if ($request->hasFile('attachments') && (count($request->file('attachments')) + count($remain_attachments_array) > 5)) {
                    return redirect()->back()
                        ->withInput($request->only('attachments_over'))
                        ->withErrors(['attachments_over' => 'Số file đã upload vượt quá 5']);
                }
            }

            if ($remove_files == [] && !empty($current_attachments_array)) {
                $files_total = array_merge($upload_files, $current_attachments_array);
            } elseif ($remove_files == [] && empty($current_attachments_array)) {
                $files_total = $upload_files;
            } else {
                $files_total = array_merge($upload_files, $remain_attachments_array);
            }

            if (empty($files_total)) {
                $data['attachments'] = '';
            } else {
                $data['attachments'] = implode(',', $files_total);
            }
            TemplateBatch::where([['template_id', '=', $id]])->delete();
            if ($request->has('batch_id')) {
                foreach ($request->batch_id as $batch_id) {
                    $record['template_id'] = $template->id;
                    $record['batch_id'] = $batch_id;
                    TemplateBatch::create($record);
                }
            }
            $template->update($data);
            return redirect()->back()
                ->with('success', 'Đã cập nhật thông tin bản thiết kế thành công');
        } catch (Exception $e) {
            return view("errors.404");
        }
    }

    public function store(Request $request)
    {
        if (auth()->user()->cannot('QRCODE-addTemplate')) {
            abort(403);
        }
        $this->validate($request, [
            'name' => 'required',
            'attachments.*' => 'max:10240|mimes:jpg,jpeg,gif,pdf,png,ai,zip,rar',
        ],
            [
                'name.required' => 'Chưa điền tên thiết kế',
                'attachments.*.max' => 'Một file upload không quá 5MB',
                'attachments.*.mimes' => 'File upload phải là một tập tin có định dạng: jpg, gif, png,zip,rar',
            ]);
        try {
            $data = $request->only(['name', 'account_id']);
            $path = "upload";
            if ($request->hasFile('attachments')) {
                $files = $request->file('attachments');
                $file = [];
                for ($i = 0; $i < count($files); $i++) {
                    $filename = $files[$i]->getClientOriginalName();
                    $files[$i]->move($path, $filename);
                    $client = new Client();
                    try {
                        $res = $client->request(
                            'POST',
                            'https://upload.icheck.com.vn/v1/file?uploadType=simple&responseType=json',
                            [
                                'multipart' => [
                                    ['name' => 'content', 'contents' => fopen("upload/$filename", 'r')]
                                ],
                            ]
                        );
                        $res = json_decode((string)$res->getBody());
                    } catch (RequestException $e) {
                        return $e->getResponse()->getBody();
                    }
                    $file[$i] = $res->path;
                    $file[$i] = str_replace(',', '_', $file[$i]);
                    unlink($path . "/" . $filename);
                }
                $data['attachments'] = implode(',', $file);
            }
            $template = Template::create($data);
            if ($request->has('batch_id')) {
                foreach ($request->batch_id as $batch_id) {
                    $record['template_id'] = $template->id;
                    $record['batch_id'] = $batch_id;

                    TemplateBatch::create($record);
                }
            }
            return redirect()->route('Qrcode::template@index')
                ->with('success', 'Đã thêm mới thiết kế thành công');
        } catch (Exception $e) {
            return view("errors.404");
        }
    }

    public function delete($id)
    {
        if (auth()->user()->cannot('QRCODE-delTemplate')) {
            abort(403);
        }
        try {
            $template = Template::findOrFail($id);
            $template->delete();
            return redirect()->back()->with('success', 'Đã xoá thành công');
        } catch (Exception $e) {
            return view("errors.404");
        }
    }

}