<?php

namespace App\Http\Controllers\Qrcode;

use App\Http\Controllers\Controller;
use App\Models\Qrcode\Collaborator;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class AgencyController extends Controller
{
    public function index(Request $request, Collaborator $collaborator)
    {
        if (auth()->user()->cannot('QRCODE-viewAgency') && auth()->user()->cannot('QRCODE-addAgency')) {
            abort(403);
        }
        try {
            $collaborator = $collaborator->newQuery();
            $query = [];
            $collaborator->select('users.*')->where([['deleted_at', '=', null], ['collaborator_id', '=', null]]);

            if ($request->has('name') && $request->input('name') != null) {
                $name = $request->input('name');
                $collaborator->where('name', 'like', '%' . $name . '%');
                $query['name'] = $name;
            }

            if ($request->has('individual_phone') && $request->input('individual_phone') != null) {
                $phone = $request->input('individual_phone');
                $collaborator->where('individual_phone', 'like', '%' . $phone . '%');
                $query['individual_phone'] = $phone;
            }

            if ($request->has('email') && $request->input('email') != null) {
                $email = $request->input('email');
                $collaborator->where('email', 'like', '%' . $email . '%');
                $query['email'] = $email;
            }


            if ($request->has('status') && $request->input('status') != null) {
                $status = $request->input('status');
                $collaborator->where('status', $status);
                $query['status'] = $status;
            }
            if ($request->has('from') && $request->input('from') != null) {
                $from = $request->input('from');
                $collaborator->where('created_at', '>=', date('Y-m-d' . ' 00:00:00', strtotime($from)));
                $query['from'] = $from;
            }
            if ($request->has('to') && $request->input('to') != null) {
                $to = $request->input('to');
                $collaborator->where('created_at', '<=', date('Y-m-d' . ' 23:59:59', strtotime($to)));
                $query['to'] = $to;
            }

            $collaborators = $collaborator->orderBy('id', 'desc')->paginate(10)->appends($query);
            foreach ($collaborators as $collaborator) {
                $orders_id_list = DB::connection('icheck_qrcode')->table('orders')
                    ->leftJoin('account', 'orders.user_id', '=', 'account.id')
                    ->where('account.collaborator_phone', $collaborator->collaborator_phone)
                    ->select('orders.*')
                    ->pluck('orders.id')->toArray();
                $stamp_ordered_approved_total = DB::connection('icheck_qrcode')->table('orders')
                    ->select('orders.quantity')
                    ->whereIn('orders.id', $orders_id_list)
                    ->where('orders.status', '=', 1)
                    ->where('deleted_at', '=', null)
                    ->sum('quantity');
                $collaborator->stamp_ordered_approved_total = number_format($stamp_ordered_approved_total, 0, ".", ",");
            }

            $collaborator_total = DB::connection('icheck_qrcode_agency')->table('users')
                ->where('deleted_at', '=', null)
                ->where('collaborator_id', '=', null)->get();
            $collaborator_active_waiting_total = DB::connection('icheck_qrcode_agency')->table('users')
                ->where('deleted_at', '=', null)
                ->where('collaborator_id', '=', null)
                ->where('status', '=', 0)->get();
            $collaborator_actived_total = DB::connection('icheck_qrcode_agency')->table('users')
                ->where('deleted_at', '=', null)
                ->where('collaborator_id', '=', null)
                ->where('status', '=', 1)->get();
            $collaborator_deactived_total = DB::connection('icheck_qrcode_agency')->table('users')
                ->where('deleted_at', '=', null)
                ->where('collaborator_id', '=', null)
                ->where('status', '=', 2)->get();
            return view('qrcode.agency.index', ['collaborators' => $collaborators, 'collaborator_total' => $collaborator_total,
                'collaborator_actived_total' => $collaborator_actived_total, 'collaborator_deactived_total' => $collaborator_deactived_total,
                'collaborator_active_waiting_total' => $collaborator_active_waiting_total]);
        } catch (Exception $exception) {
            return view('errors.404');
        }

    }

    public function add()
    {
        if (auth()->user()->cannot('QRCODE-addAgency')) {
            abort(403);
        }
        try {
            return view('qrcode.agency.form');
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

    public function edit($id)
    {
        if (auth()->user()->cannot('QRCODE-updateAgency')) {
            abort(403);
        }
        try {
            $collaborator = Collaborator::findOrFail($id);
            return view('qrcode.agency.form', ['collaborator' => $collaborator]);
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }

    public function store(Request $request)
    {
        if (auth()->user()->cannot('QRCODE-addAgency')) {
            abort(403);
        }
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:icheck_qrcode_agency.users',
            'address' => 'required',
            'individual_phone' => 'required|regex:/^[0-9]{9,11}$/|unique:icheck_qrcode_agency.users,individual_phone|unique:icheck_qrcode_agency.users,collaborator_phone',
            'email' => 'required|unique:icheck_qrcode_agency.users',
            'password' => 'required|min:6',
            'confirm_password' => 'required|same:password'
        ],
            [
                'name.unique' => 'Tên đăng nhập đã tồn tại!',
                'individual_phone.unique' => 'Số điện thoại đã tồn tại!',
                'individual_phone.regex' => 'Số điện thoại phải bao gồm các chữ số từ 0 đến 9 và có từ 9 đến 11 chữ số',
                'email.unique' => 'Email đã tồn tại!',
                'confirm_password.same' => 'Mật khẩu xác nhận không trùng khớp',
                'password.min' => 'Mật khẩu có độ dài ít nhất 6 ký tự',
            ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        try {
            $data = $request->all();

            $collaborator = new Collaborator();
            $collaborator->name = $data['name'];
            $collaborator->email = $data['email'];
            $collaborator->address = $data['address'];
            $collaborator->individual_phone = $data['individual_phone'];
            $collaborator->collaborator_phone = $data['individual_phone'];
            $collaborator->status = 0;
            $collaborator->password = bcrypt($data['password']);
            $collaborator->save();

            return redirect()->route('Qrcode::agency@index')
                ->with('success', 'Đã thêm mới đại lý thành công');

        } catch (Exception $exception) {
            return view('errors.404');
        }

    }

    public function update($id, Request $request)
    {
        if (auth()->user()->cannot('QRCODE-updateAgency')) {
            abort(403);
        }
        $this->validate($request, [
            'name' => 'required|unique:icheck_qrcode_agency.users,name,' . $id,
            'address' => 'required',
            'individual_phone' => 'required|regex:/^[0-9]{9,11}$/|unique:icheck_qrcode_agency.users,individual_phone,' . $id,
            'email' => 'required|unique:icheck_qrcode_agency.users,email,' . $id,
            'password' => 'nullable|min:6',
            'confirm_password' => 'same:password'
        ],
            [
                'name.unique' => 'Tên đăng nhập đã tồn tại!',
                'individual_phone.unique' => 'Số điện thoại đã tồn tại!',
                'individual_phone.regex' => 'Số điện thoại phải bao gồm các chữ số từ 0 đến 9 và có từ 9 đến 11 chữ số',
                'email.unique' => 'Email đã tồn tại!',
                'confirm_password.same' => 'Mật khẩu xác nhận không trùng khớp',
                'password.min' => 'Mật khẩu có độ dài ít nhất 6 ký tự',
            ]);

        try {
            $data = $request->all();
            $collaborator = Collaborator::findOrFail($id);
            if ($request->input('password')) {
                $collaborator->password = bcrypt($data['password']);
            }

            $collaborator->name = $data['name'];
            $collaborator->email = $data['email'];
            $collaborator->address = $data['address'];
            $collaborator->individual_phone = $data['individual_phone'];
            $collaborator->save();

            return redirect()->back()
                ->with('success', 'Đã cập nhật thành công');
        } catch (Exception $exception) {
            return view('errors.404');
        }

    }

    public function approve($id)
    {
        if (auth()->user()->cannot('QRCODE-approveAgency')) {
            abort(403);
        }
        try {
            DB::connection('icheck_qrcode_agency')->table('users')
                ->where('id', $id)
                ->update(['status' => 1]);
            return redirect()->back()
                ->with('success', 'Đã kích hoạt thành công');
        } catch (Exception $exception) {
            return view('errors.404');
        }
    }


    public function disapprove($id)
    {
        if (auth()->user()->cannot('QRCODE-disapproveAgency')) {
            abort(403);
        }
        try {
            DB::connection('icheck_qrcode_agency')->table('users')
                ->where('id', $id)
                ->update(['status' => 2]);
            return redirect()->back()
                ->with('success', 'Đã hủy kích hoạt thành công');
        } catch (Exception $exception) {
            return view('errors.404');
        }

    }

    public function delete($id)
    {
        if (auth()->user()->cannot('QRCODE-delAgency')) {
            abort(403);
        }
        try {
            $collaborator = Collaborator::findOrFail($id);
            $collaborator->delete();
            return redirect()->back()->with('success', 'Đã xoá thành công');
        } catch (Exception $exception) {
            return view('errors.404');
        }

    }

}