<?php

namespace App\Listeners;

use App\API\Api_collaborator_transfers;
use App\API\Api_iCheck_backend;
use App\Events\ProductInfoApprove;
use App\Events\ProductContributedInfoApprove;
use App\Models\Business\IcheckCountry;
use App\Models\Collaborator\Collaborator;
use App\Models\Collaborator\Contributed_Information;
use App\Models\Collaborator\Need_Update_Product;
use App\Models\Collaborator\Transaction;
use App\Models\Product\Product;
use App\Repository\Collaborator\CollaboratorRepositoryInterface;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Webpatser\Uuid\Uuid;

class ProductContributedInfoSync implements ShouldQueue
{
    use InteractsWithQueue;
    public $tries = 5;
    private $api_icheck_backend;
    private $collaboratorRepository;
    private $api_collaborator_transfer;

    public function __construct(CollaboratorRepositoryInterface $collaboratorRepository, Api_collaborator_transfers $api_collaborator_transfer)
    {
        $this->api_icheck_backend = new Api_iCheck_backend();
        $this->collaboratorRepository = $collaboratorRepository;
        $this->api_collaborator_transfer = $api_collaborator_transfer;
    }


    /**
     * Handle the event.
     *
     * @param  ProductInfoApprove $event
     * @return void
     */
    public function handle(ProductContributedInfoApprove $event)
    {
        $contributed_information = Contributed_Information::findOrFail($event->contributed_info_id);

        if ($contributed_information->status == Contributed_Information::STATUS_APPROVED) {
            return;
        }

        $contributed_info = $event->contributed_info;
        $contributed_info = array_filter($contributed_info);
        $gtin = $event->gtin;

        $sync_product['status'] = 1;
        if (array_key_exists("name", $contributed_info)) {
            $sync_product['product_name'] = $contributed_info['name'];
        }
        if (array_key_exists("price", $contributed_info)) {
            $sync_product['price_default'] = $contributed_info['price'];
        }

        if (array_key_exists("description", $contributed_info)) {
            $sync_product['attributes'] = [['attribute_id' => 1, 'content' => $contributed_info['description'], 'short_content' => mb_convert_encoding(substr(strip_tags($contributed_info['description']), 0, 100), 'UTF-8', 'UTF-8')]];
        }

        if (array_key_exists("glnInfo", $contributed_info)) {
            $glnInfo = $contributed_info['glnInfo'];

            $data = [];
            if (array_key_exists("name", $glnInfo)) {
                $data['name'] = $glnInfo['name'];
            } else {
                $data['name'] = '';
            }
            if (array_key_exists("email", $glnInfo)) {
                $data['email'] = $glnInfo['email'];
            } else {
                $data['email'] = '';
            }
            if (array_key_exists("phone", $glnInfo)) {
                $data['phone'] = $glnInfo['phone'];
            } else {
                $data['phone'] = '';
            }
            if (array_key_exists("address", $glnInfo)) {
                $data['address'] = $glnInfo['address'];
            } else {
                $data['address'] = '';
            }
            if (array_key_exists("website", $glnInfo)) {
                $data['website'] = $glnInfo['website'];
            } else {
                $data['website'] = '';
            }
            if (array_key_exists("country", $glnInfo)) {
                $country = IcheckCountry::where('alpha_2', $glnInfo['country'])->first();
                $data['country_id'] = $country->id;
            } else {
                $data['country_id'] = null;
            }
            $data['gln_code'] = $glnInfo['gln'];
            $data['internal_code'] = $glnInfo['gln'];
            $data['prefix'] = $glnInfo['businessPrefix'];
            $res = $this->api_icheck_backend->getVendorByGln(["where" => ["gln_code" => $glnInfo['gln']]]);

            if ($res['data']['items'] != []) {
                $icheck_vendor = $res['data']['items'][0];
                $sync_product['vendor_id'] = $icheck_vendor['id'];
                $this->api_icheck_backend->updateVendor($icheck_vendor['id'], $data);
            } else {
                $result = $this->api_icheck_backend->createVendor($data);
                if ($result['status'] != 200) {
                    echo "Loi tao Vendor tren iCheck";
                    dd($result);
                    return;
                }
                $sync_product['vendor_id'] = $result['data']['id'];
            }

        }
        //Image Sync
        if (array_key_exists("images", $contributed_info)) {
            $sync_product['attachments'] = [];
            foreach ($contributed_info['images'] as $product_image) {
                $content = file_get_contents($product_image);
                $baseDir = "-TheHulk" . '/' . substr(hash('sha256', date('Y-m-d')), 0, 10);
                $id = Uuid::generate()->string;
                $baseName = time() . '_' . str_replace('-', '', $id) . '_' . substr(hash('sha256', time() . mt_rand(1, 9999)), 0, 6);
                $basePath = $baseDir . '/' . $baseName;
                $file_extensions = ['_large.jpg', '_medium.jpg', '_small.jpg', '_thumb_large.jpg', '_thumb_medium.jpg', '_thumb_small.jpg', '_original.jpg'];
                $full_paths = [];
                for ($i = 0; $i < count($file_extensions); $i++) {
                    array_push($full_paths, $basePath . $file_extensions[$i]);
                }

                for ($i = 0; $i < count($full_paths); $i++) {
                    Storage::disk('gcs_icheck')->put("$full_paths[$i]", $content, 'public');
                }
                array_push($sync_product['attachments'], array('type' => 'image', 'link' => $basePath));
            }
            $sync_product['image_default'] = $sync_product['attachments'][0]['link'];
        }
//Icheck Sync
        $res_2 = $this->api_icheck_backend->getProduct($gtin);
        if ($res_2['status'] == 404) {
            $sync_product['gtin_code'] = $gtin;
            $res_3 = $this->api_icheck_backend->createProduct($sync_product);
            if ($res_3['status'] == 200) {
                if (array_key_exists("categories", $contributed_info)) {
                    Product::where("id", $res_3['data']['id'])->update(["has_categories" => 1]);
                }
                //beginTransaction
                DB::connection('icheck_collaborator')->beginTransaction();
                try {
                    $contributed_information = Contributed_Information::findOrFail($event->contributed_info_id);
                    $contributed_information->update(["status" => Contributed_Information::STATUS_APPROVED, "approved_at" => date("Y-m-d H:i:s")]);
//                    Need_Update_Product::where([["gtin", $contributed_information->gtin],["need_update",0]])->delete();
//                //Create money transfer
//                $response = $this->api_collaborator_transfer->transfer([
//                    "description" => "Thanh toan tien dong gop thong tin San pham ma $contributed_information->gtin",
//                    "amount" => $contributed_information->profits * 100,
//                    "recipient" => $this->collaboratorRepository->getIcheckId($contributed_information->contributed_by),
//                    "metadata" => ["info_id" => $contributed_information->id]
//                ]);

//                if ($response['status'] != 200) {
//                    dd($response['error']['message']);
//                    return;
//                }

                    //Create record in Transaction table
                    Transaction::create([
                        'collaborator_id' => $contributed_information->contributed_by,
                        'amount' => $contributed_information->profits,
                        'ref_id' => $contributed_information->id,
                        'type' => Transaction::TYPE_APPROVE_CONTRIBUTED_INFORMATION,
                        'status' => 1
                    ]);

                    //Update Balance for Collaborator
                    $collaborator = Collaborator::findOrFail($contributed_information->contributed_by);
                    $current_balance = $collaborator->balance;
                    $new_balance = $current_balance + $contributed_information->profits;
                    $collaborator->update(["balance" => $new_balance]);
                    DB::connection('icheck_collaborator')->commit();
                    // endTransaction
                } catch (\Exception $e) {
                    DB::connection('icheck_collaborator')->rollBack();
                    echo $e->getMessage();
                }
            } else {
                echo "Loi tao San pham tren iCheck";
                dd($res_3);
                return;
            }
        } elseif ($res_2['status'] == 200) {
            $icheck_product = $res_2['data'];
            if (count($icheck_product['attributes']) > 0 && array_key_exists('attributes', $sync_product) && count($sync_product['attributes']) > 0) {
                for ($i = 0; $i < count($icheck_product['attributes']); $i++) {
                    for ($j = 0; $j < count($sync_product['attributes']); $j++) {
                        if ($icheck_product['attributes'][$i]['attribute_id'] == $sync_product['attributes'][$j]['attribute_id']) {
                            $sync_product['attributes'][$j]['id'] = $icheck_product['attributes'][$i]['id'];
                        }
                    }
                }
            }
            $res_5 = $this->api_icheck_backend->updateProduct($icheck_product['id'], $sync_product);
            if ($res_5['status'] == 200) {
                if (array_key_exists("categories", $contributed_info)) {
                    Product::where("id", $icheck_product['id'])->update(["has_categories" => 1]);
                }
                //beginTransaction
                DB::connection('icheck_collaborator')->beginTransaction();
                try {
                    $contributed_information = Contributed_Information::findOrFail($event->contributed_info_id);
//                    Need_Update_Product::where([["gtin", $contributed_information->gtin],["need_update",0]])->delete();
                    //Create money transfer
//                $response = $this->api_collaborator_transfer->transfer([
//                    "description" => "Thanh toan tien dong gop thong tin San pham ma $contributed_information->gtin",
//                    "amount" => $contributed_information->profits * 100,
//                    "recipient" => $this->collaboratorRepository->getIcheckId($contributed_information->contributed_by),
//                    "metadata" => ["info_id" => $contributed_information->id]
//                ]);
//
//                if ($response['status'] != 200) {
//                    echo "Loi tao giao dich thanh toan vi";
//                    dd($response['error']['message']);
//                    return;
//                }

                    //Create record in Transaction table
                    $transaction = Transaction::where([['ref_id', $contributed_information->id], ['type', Transaction::TYPE_APPROVE_CONTRIBUTED_INFORMATION]])->first();
                    if (!$transaction) {
                        $contributed_information->update(["status" => Contributed_Information::STATUS_APPROVED, "approved_at" => date("Y-m-d H:i:s")]);
                        Transaction::create([
                            'collaborator_id' => $contributed_information->contributed_by,
                            'amount' => $contributed_information->profits,
                            'ref_id' => $contributed_information->id,
                            'type' => Transaction::TYPE_APPROVE_CONTRIBUTED_INFORMATION,
                            'status' => 1
                        ]);

                        //Update Balance for Collaborator
                        Collaborator::where("id", $contributed_information->contributed_by)->increment('balance', $contributed_information->profits);
//                    $collaborator = Collaborator::findOrFail($contributed_information->contributed_by);
////                    $current_balance = $collaborator->balance;
////                    $new_balance = $current_balance + $contributed_information->profits;
////                    $collaborator->update(["balance" => $new_balance]);
                    }

                    DB::connection('icheck_collaborator')->commit();
                    // endTransaction
                } catch (\Exception $e) {
                    DB::connection('icheck_collaborator')->rollBack();
                    echo $e->getMessage();
                }
            } else {
                echo "Loi cap nhat San pham iCheck";
                dd($res_5);
                return;
            }
        }
    }

    /**
     * Handle a job failure.
     *
     */
    public function failed(ProductContributedInfoApprove $event, $exception)
    {
        var_dump($exception->getMessage());
    }


}
