<?php

namespace App\Listeners;

use App\API\Api_iCheck_backend;
use App\Events\ProductSyncIcheckExpoEvent;
use App\Models\Business\Product;
use Exception;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;


class ProductSyncIcheckExpo implements ShouldQueue
{
    use InteractsWithQueue;
    public $tries = 5;

    private $api_icheck_backend;

    public function __construct()
    {
        $this->api_icheck_backend = new Api_iCheck_backend();
    }


    /**
     * Handle the event.
     *
     * @param  ProductSyncIcheckExpoEvent $event
     * @return void
     */
    public function handle(ProductSyncIcheckExpoEvent $event)
    {
        $business_id = $event->business_id;
        $products = Product::where([["business_id", $business_id],["status",2]])->get();
        if ($products->count()) {
            foreach ($products as $product) {
                $res = $this->api_icheck_backend->getProduct($product->barcode);
                if ($res['status'] == 200) {
                    $image = $res['data']['image_default'];
                    expo_sync_product($product->barcode, $image);
                }
            }
        }
    }

    /**
     * Handle a job failure.
     *
     */
    public function failed(ProductSyncIcheckExpoEvent $event, $exception)
    {
        var_dump($exception->getMessage());
    }

}
