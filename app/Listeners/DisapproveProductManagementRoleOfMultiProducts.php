<?php

namespace App\Listeners;

use App\API\Api_business_gateway;
use App\API\Api_iCheck_backend;
use App\Events\DisapproveProductManagementRoleOfMultiProductsEvent;
use App\Mail\BusinessGateway\DisapproveProductManagementRoleOfMultiProductsMail;
use App\Models\Business\Business;
use App\Models\Business\ManagementProductRequest;
use App\Models\Business\Product;
use App\Models\Business\User;
use App\Repository\Business\ProductRepositoryInterface;
use App\Repository\Business\ProductRequestRepositoryInterface;
use App\Repository\Business\UserRepositoryInterface;
use Exception;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class DisapproveProductManagementRoleOfMultiProducts implements ShouldQueue
{
    use InteractsWithQueue;
    public $tries = 5;

//    private $userRepository;
//    private $productRequestRepository;
    private $api_business_gateway;

    public function __construct()
    {
//        $this->userRepository = $userRepository;
//        $this->productRequestRepository = $productRequestRepository;
        $this->api_business_gateway = new Api_business_gateway();
    }


    /**
     * Handle the event.
     *
     * @param  DisapproveProductManagementRoleOfMultiProductsEvent $event
     * @return void
     */
    public function handle(DisapproveProductManagementRoleOfMultiProductsEvent $event)
    {
        $product_ids = $event->product_ids;
        $note = $event->note;
        $business_ids = array_filter(array_unique(Product::whereIn("id", $product_ids)->pluck("business_id")->toArray()));
        if(count($business_ids) > 0){
            foreach ($business_ids as $business_id) {
                $gtins = Product::where('business_id', $business_id)->whereIn("id", $product_ids)->pluck("barcode")->toArray();
                $data = [];
                $data['businessId'] = $business_id;
                $data['gtins'] = $gtins;
                $data['notify'] = true;
                $data['reason'] = $note;
                $res = $this->api_business_gateway->disapproveProductManagementRole($data);
            }
        }



//                //Update product_count in businesses table
//                $count = Product::where("business_id", $business_id)->count();
//                Business::where("id", $business_id)->update(["product_count" => $count]);
        //Send emails
//                if (count($fails) > 0) {
//                    $success_product_ids = array_diff($products->pluck("id")->toArray(), $fails);
//                    $success_products = Product::whereIn('id', $success_product_ids)->get();
//                    $mail_productData = $success_products->pluck("name", "barcode");
//                } else {
//                    $mail_productData = $products->pluck("name", "barcode");
//                }
//
//
//                $users = $this->userRepository->getUsersByBusinessId($business->id);
//                $emails = array_filter($users->pluck("email")->toArray());
//
//                if (count($emails) > 0) {
//                    foreach ($emails as $email) {
//                        Mail::to($email)->queue(new DisapproveProductManagementRoleOfMultiProductsMail($business->name, $mail_productData, $note));
//                    }
//                }

//                $emails = User::where([['business_id', '=', $business_id], ['deleted_at', '=', null]])->pluck('email')->toArray();
//                if (count($emails) > 0) {
//                    foreach ($emails as $email) {
//                        Mail::to($email)->queue(new DisapproveProductManagementRoleOfMultiProductsMail($business->name, $mail_productData, $note));
//                    }
//                }

    }

    /**
     * Handle a job failure.
     *
     */
    public function failed(DisapproveProductManagementRoleOfMultiProductsEvent $event, $exception)
    {
        var_dump($exception->getMessage());
    }

}
