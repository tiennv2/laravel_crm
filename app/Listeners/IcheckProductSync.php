<?php

namespace App\Listeners;

use App\API\Api_business_gateway;
use App\API\Api_iCheck_backend;
use App\Events\ProductInfoApprove;
use App\Models\Business\ManagementProductRequest;
use App\Models\Business\Product;
use App\Models\Product\Attribute;
use App\Repository\Business\ProductRepositoryInterface;
use Exception;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Webpatser\Uuid\Uuid;

class IcheckProductSync implements ShouldQueue
{
    use InteractsWithQueue;
    public $tries = 5;

    private $api_business_gateway;
//    private $productRepository;
//    private $error;

    public function __construct()
    {
        $this->api_business_gateway = new Api_business_gateway();
//        $this->productRepository = $productRepository;
    }


    /**
     * Handle the event.
     *
     * @param  ProductInfoApprove $event
     * @return void
     */
    public function handle(ProductInfoApprove $event)
    {
//        try {
        $id = $event->id;
        $product = Product::findOrFail($id);
        $data = [];
        $data['gtins'] = [$product->barcode];
        $data['autoVerify'] = true;
        $data['status'] = $event->p_status;

        $res = $this->api_business_gateway->approveProductInfo($data);
//        $sync_product = [];
//        $sync_product['attributes'] = [];
//        if ($product['attributes']) {
//            $attributes = $product['attributes'];
//            $attribute_list = DB::connection("icheck_product")->table("p_product_attr")->pluck("id")->toArray();
//            foreach ($attribute_list as $item) {
//                if (!in_array($item, array_keys($attributes))) {
//                    $attributes[$item] = '';
//                }
//            }
//
//            foreach ($attributes as $key => $value) {
//                if (!$value) {
//                    $value = '';
//                }
//                array_push($sync_product['attributes'], ['attribute_id' => $key, 'content' => $value, 'short_content' => mb_convert_encoding(substr(strip_tags($value), 0, 100), 'UTF-8', 'UTF-8')]);
//            }
//            $product['attributes'] = json_encode($product['attributes']);
//        }
//        if ($product['gln_id']) {
//            $gln = $this->productRepository->getGlnByProduct($product['gln_id']);
//            if ($gln) {
//                $sync_product['vendor_id'] = $gln->icheck_id;
//            }
//        }
//
//        $sync_product['product_name'] = $product['name'];
//        $sync_product['gtin_code'] = $product['barcode'];
//        $sync_product['price_default'] = $product['price'];
//        $sync_product['status'] = 1;
//        $sync_product['image_default'] = "";
//        $sync_product['attachments'] = [];
//        $file_ids = [];
//        //Image Sync
//        if (array_key_exists("images", $product) && $product['images']) {
//            foreach ($product['images'] as $product_image) {
//                $content = Storage::disk('gcs_business')->get($product_image);
//                $baseDir = "-TheHulk" . '/' . substr(hash('sha256', date('Y-m-d')), 0, 10);
//                $id = Uuid::generate()->string;
//                $baseName = time() . '_' . str_replace('-', '', $id) . '_' . substr(hash('sha256', time() . mt_rand(1, 9999)), 0, 6);
//                $basePath = $baseDir . '/' . $baseName;
//                $file_extensions = ['_large.jpg', '_medium.jpg', '_small.jpg', '_thumb_large.jpg', '_thumb_medium.jpg', '_thumb_small.jpg', '_original.jpg'];
//                $full_paths = [];
//                for ($i = 0; $i < count($file_extensions); $i++) {
//                    array_push($full_paths, $basePath . $file_extensions[$i]);
//                }
//
//                for ($i = 0; $i < count($full_paths); $i++) {
//                    Storage::disk('gcs_icheck')->put("$full_paths[$i]", $content, 'public');
//                }
//
//                array_push($sync_product['attachments'], array('type' => 'image', 'link' => $basePath));
//            }
//            $sync_product['image_default'] = $sync_product['attachments'][0]['link'];
//            $product['images'] = json_encode($product['images']);
//
//        } else {
//            $product['images'] = null;
//        }
//        //Certificate Images Sync
//        if (array_key_exists("certificate_images", $product) && $product['certificate_images']) {
//            foreach ($product['certificate_images'] as $certificate_image) {
//                $content = Storage::disk('gcs_business')->get($certificate_image);
//                $basePath = "-TheHulk/certificate-images" . '/' . hash('sha256', $certificate_image);
//                $file_extensions = ['_large.jpg', '_medium.jpg', '_small.jpg', '_thumb_large.jpg', '_thumb_medium.jpg', '_thumb_small.jpg', '_original.jpg'];
//                $full_paths = [];
//                for ($i = 0; $i < count($file_extensions); $i++) {
//                    array_push($full_paths, $basePath . $file_extensions[$i]);
//                }
//
//                for ($i = 0; $i < count($full_paths); $i++) {
//                    Storage::disk('gcs_icheck')->put("$full_paths[$i]", $content, 'public');
//                }
//                array_push($file_ids, $basePath);
//            }
//            $product['certificate_images'] = json_encode($product['certificate_images']);
//
//        } else {
//            $product['certificate_images'] = null;
//        }
//
//        $approved_product = Product::where('barcode', $product['barcode'])->first();
//        if (!array_key_exists('status', $product)) {
//            $product['status'] = 1;
//        }
////Icheck Sync
//        $res_2 = $this->api_icheck_backend->getProduct($product['barcode']);
//        if ($res_2['status'] == 404) {
//            $res_3 = $this->api_icheck_backend->createProduct($sync_product);
//            if ($res_3['status'] == 200) {
//                $product['icheck_id'] = $res_3['data']['id'];
//                //Certificate Images Sync
//                $this->api_icheck_backend->updateCertificateImages([
//                    "product_id" => $product['icheck_id'],
//                    "file_ids" => $file_ids
//                ]);
//                //Xác thực sản phẩm trên iCheck
//                $res_4 = $this->api_icheck_backend->productVerify(['ids' => [$product['barcode']]]);
//                if ($res_4['status'] == 200) {
//                    $approved_product->update($product);
//                    //Push Webhooks
//                    expo_sync_product($sync_product['gtin_code'], $sync_product['image_default']);
//                } else {
//                    dd($res_4);
//                }
//            } else {
//                dd($res_3);
//            }
//        } elseif ($res_2['status'] == 200) {
//            $icheck_product = $res_2['data'];
//            if (count($icheck_product['attributes']) > 0 && count($sync_product['attributes']) > 0) {
//                for ($i = 0; $i < count($icheck_product['attributes']); $i++) {
//                    for ($j = 0; $j < count($sync_product['attributes']); $j++) {
//                        if ($icheck_product['attributes'][$i]['attribute_id'] == $sync_product['attributes'][$j]['attribute_id']) {
//                            $sync_product['attributes'][$j]['id'] = $icheck_product['attributes'][$i]['id'];
//                        }
//                    }
//                }
//            }
//
//            $res_5 = $this->api_icheck_backend->updateProduct($icheck_product['id'], $sync_product);
//            if ($res_5['status'] == 200) {
//                $product['icheck_id'] = $res_5['data']['id'];
//                //Certificate Images Sync
//                $this->api_icheck_backend->updateCertificateImages([
//                    "product_id" => $product['icheck_id'],
//                    "file_ids" => $file_ids
//                ]);
//
//                //Xác thực sản phẩm trên iCheck
//                $res_6 = $this->api_icheck_backend->productVerify(['ids' => [$product['barcode']]]);
//                if ($res_6['status'] == 200) {
//                    $approved_product->update($product);
//                    //Push Webhooks
//                    expo_sync_product($sync_product['gtin_code'], $sync_product['image_default']);
//                } else {
//                    dd($res_6);
//                }
//            } else {
//                dd($res_5);
//            }
//        }

//        } catch (Exception $exception) {
//            dd($exception->getMessage());
//        }
    }

    /**
     * Handle a job failures.
     *
     */
    public function failed(ProductInfoApprove $event, $exception)
    {
        var_dump($exception);
    }

}
