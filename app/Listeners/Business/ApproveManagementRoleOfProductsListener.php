<?php

namespace App\Listeners\Business;

use App\API\Api_business_gateway;
use App\Events\Business\ApproveManagementRoleOfProductsEvent;
use App\Models\Business\Business_Product;
use App\Models\Icheck_newCMS\ActivityLog;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ApproveManagementRoleOfProductsListener implements ShouldQueue
{
    use InteractsWithQueue;
    public $tries = 5;


    private $api_business_gateway;

    public function __construct()
    {
        $this->api_business_gateway = new Api_business_gateway();
    }


    /**
     * Handle the event.
     *
     * @param  ApproveManagementRoleOfProductsEvent $event
     * @return void
     */
    public function handle(ApproveManagementRoleOfProductsEvent $event)
    {
        $product_ids = $event->product_ids;
        $verify = $event->verify;
        $user_id = $event->user_id;
        $business_ids = array_filter(array_unique(Business_Product::whereIn('id', $product_ids)->pluck('business_id')->toArray()));
        if ($business_ids) {
            foreach ($business_ids as $business_id) {
                $gtins = Business_Product::where('business_id', $business_id)->whereIn('id', $product_ids)->pluck('barcode')->toArray();
                foreach ($gtins as $gtin) {
                    $data = [];
                    $data['businessId'] = $business_id;
                    $data['gtin'] = $gtin;
//                    $data['status'] = Business_Product::INFO_STATUS_PENDING;
//                    if ($verify == true) {
//                        $data['autoVerify'] = true;
//                    }
                    $data['syncRequestData'] = true;
                    $data['notify'] = true;
                    $data['reason'] = "Đã có Doanh nghiệp khác quản lý Sản phẩm này!";
                    $res = $this->api_business_gateway->approveProductManagementRole($data);
                    $product = Business_Product::where([['business_id', $business_id], ['barcode', $gtin]])->first();
                    //Activity_log
                    ActivityLog::create([
                        "description" => "Approve Management Role Of Product",
                        "subject_id" => $product->id,
                        "subject_type" => "App\Models\Business\Business_Product",
                        "causer_id" => $user_id,
                        "causer_type" => "users"
                    ]);
                }
            }
        }

    }

    /**
     * Handle a job failure.
     *
     */
    public function failed(ApproveManagementRoleOfProductsEvent $event, $exception)
    {
        var_dump($exception->getMessage());
    }

}
