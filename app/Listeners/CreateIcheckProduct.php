<?php

namespace App\Listeners;

use App\API\Api_iCheck_backend;
use App\Events\ProductFileAgreement;
use App\Repository\Qrcode\ProductRepositoryInterface;
use Exception;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateIcheckProduct
{
    private $api_icheck_backend;
    private $productRepository;
    private $error;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->api_icheck_backend = new Api_iCheck_backend();
        $this->productRepository = $productRepository;
    }

//    use InteractsWithQueue;
//    public $tries = 5;

    /**
     * Handle the event.
     *
     * @param  ProductFileAgreement $event
     * @return void
     */
    public function handle(ProductFileAgreement $event)
    {
//        try {
        $product = $event->product;
//            $icheck_attributes = $this->api_icheck_backend->getAttributes();
        $product_info = $this->productRepository->getProductInfo($product->id);
        $new_product = [];
        $new_product['attributes'] = [];
        $product_info_str = "";
        if (count($product_info) > 0) {
            for ($i = 0; $i < count($product_info); $i++) {
                $product_info_str .= '<p style="font-weight: bold">' . ucwords(trim(($product_info[$i]->title))) . ' ' . '</p>' . '<p>' . $product_info[$i]->content . ' ' . '</p>';
            }
            array_push($new_product['attributes'], ['attribute_id' => 1, 'content' => $product_info_str, 'short_content' => substr(strip_tags($product_info_str), 0, 100)]);
        }
//            $i = 0;
//            foreach ($icheck_attributes['data'] as $attribute) {
//                if (strcmp('Thông tin sản phẩm', trim($attribute['title'])) == 0) {
//                    array_push($new_product['attributes'], ['attribute_id' => $attribute['id'], 'content' => $product_info_str, 'short_content' => mb_convert_encoding(substr(strip_tags($product_info_str), 0, 100), 'UTF-8', 'UTF-8')]);
//                    $i++;
//                }
//            }
//            if ($i == 0) {
//                $new_icheck_attribute = $this->api_icheck_backend->createAttribute(['title' => 'Thông tin sản phẩm']);
//                array_push($new_product['attributes'], ['attribute_id' => $new_icheck_attribute['data']['id'], 'content' => $product_info_str, 'short_content' => substr(strip_tags($product_info_str), 0, 100)]);
//            }

        $vendor = $this->productRepository->getVendorByProduct($product->vendor_id);
        $condition = ["where" => ["gln_code" => $vendor->gln_code]];
        $res_1 = $this->api_icheck_backend->getVendorByGln($condition);
        if ($res_1['data']['items']) {
            $icheck_vendor = $res_1['data']['items'][0];
            $new_product['vendor_id'] = $icheck_vendor['id'];
        } else {
            $new_icheck_vendor = [];
            $new_icheck_vendor['name'] = $vendor->name;
            $new_icheck_vendor['email'] = $vendor->email;
            $new_icheck_vendor['phone'] = $vendor->phone;
            $new_icheck_vendor['address'] = $vendor->address . ', ' . $vendor->district_name . ', ' . $vendor->city_name;
            $new_icheck_vendor['website'] = $vendor->website;
            $new_icheck_vendor['country_id'] = $vendor->country_id;
            $new_icheck_vendor['gln_code'] = $vendor->gln_code;
            $new_icheck_vendor['internal_code'] = $vendor->gln_code;
            $result = $this->api_icheck_backend->createVendor($new_icheck_vendor);
            $new_product['vendor_id'] = $result['data']['id'];
        }
        $new_product['product_name'] = $product->name;
//            $new_product['gtin_code'] = $product->sku;
        $new_product['status'] = 1;
        $new_product['price_default'] = $product->price;
        $new_product['image_default'] = $product->image;
        $new_product['attachments'] = array(array('type' => 'image', 'link' => $product->image),
            array('type' => 'video', 'link' => $product->video));
        $approved_product = $this->productRepository->getProduct($product->id);
        $res_2 = $this->api_icheck_backend->getProduct($product->sku);
        if ($res_2['status'] == 404) {
            $new_product['gtin_code'] = $product->sku;
            $res_3 = $this->api_icheck_backend->createProduct($new_product);
            if ($res_3['status'] == 200) {
                if (starts_with($product->sku,'IQ' )) {
                    //Xác thực sản phẩm trên iCheck
                    $res_6 = $this->api_icheck_backend->productVerify(['ids' => [$product->sku]]);
                    if ($res_6['status'] == 200) {
                        $approved_product->approved_file = 1;
                        $approved_product->save();
                    } else {
                        echo 'Lỗi xác thực Sản phẩm khi tạo mới!';
                        var_dump($res_6);
                        exit();
                    }
                } else {
                    $approved_product->approved_file = 1;
                    $approved_product->save();
                }
            }

        } elseif ($res_2['status'] == 200) {
            $icheck_product = $res_2['data'];
            if (count($icheck_product['attributes']) > 0 && count($new_product['attributes']) > 0) {
                for ($i = 0; $i < count($icheck_product['attributes']); $i++) {
                    for ($j = 0; $j < count($new_product['attributes']); $j++) {
                        if ($icheck_product['attributes'][$i]['attribute_id'] == $new_product['attributes'][$j]['attribute_id']) {
                            $new_product['attributes'][$j]['id'] = $icheck_product['attributes'][$i]['id'];
                        }
                    }
                }
            }

            $res_4 = $this->api_icheck_backend->updateProduct($icheck_product['id'], $new_product);
            if ($res_4['status'] == 200) {
                if (starts_with($product->sku,'IQ' )) {
                    //Xác thực sản phẩm trên iCheck
                    $res_5 = $this->api_icheck_backend->productVerify(['ids' => [$product->sku]]);
                    if ($res_5['status'] == 200) {
                        $approved_product->approved_file = 1;
                        $approved_product->save();
                    } else {
                        echo 'Lỗi xác thực Sản phẩm sau khi update Sản phẩm!';
                        var_dump($res_5);
                        exit();
                    }
                } else {
                    $approved_product->approved_file = 1;
                    $approved_product->save();
                }
            } else {
                echo 'Lỗi update Sản phẩm sang iCheck!';
                var_dump($res_4);
                exit();
            }
        }

//        } catch (Exception $exception) {
////            echo $this->error = $exception->getMessage();
//            echo "Duyệt sản phẩm không thành công! Vui lòng liên hệ với bộ phận kĩ thuật để được giải quyết. Xin cảm ơn!";
//            exit();
//        }
    }

    public function failed(ProductFileAgreement $event, $exception)
    {
        var_dump($exception->getMessage());
    }
}
