<?php

namespace App\Listeners;

use App\API\Api_iCheck_backend;
use App\Events\ProductCertificateImagesSyncEvent;
use App\Events\ProductSyncIcheckExpoEvent;
use App\Models\Business\Product;
use Exception;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Storage;


class ProductCertificateImagesSync implements ShouldQueue
{
    use InteractsWithQueue;
    public $tries = 5;

    private $api_icheck_backend;

    public function __construct()
    {
        $this->api_icheck_backend = new Api_iCheck_backend();
    }


    /**
     * Handle the event.
     *
     * @param  ProductSyncIcheckExpoEvent $event
     * @return void
     */
    public function handle(ProductCertificateImagesSyncEvent $event)
    {
        $certificate_images = $event->certificate_images;
        $product_icheck_id = $event->product_icheck_id;
        $file_ids = [];
        foreach (json_decode($certificate_images, true) as $image) {
            $content = Storage::disk('gcs_business')->get($image);
            $basePath = "-TheHulk/certificate-images" . '/' . hash('sha256', $image);
            $file_extensions = ['_large.jpg', '_medium.jpg', '_small.jpg', '_thumb_large.jpg', '_thumb_medium.jpg', '_thumb_small.jpg', '_original.jpg'];
            $full_paths = [];
            for ($i = 0; $i < count($file_extensions); $i++) {
                array_push($full_paths, $basePath . $file_extensions[$i]);
            }

            for ($i = 0; $i < count($full_paths); $i++) {
                Storage::disk('gcs_icheck')->put("$full_paths[$i]", $content, 'public');
            }
            array_push($file_ids, $basePath);
        }

        $this->api_icheck_backend->updateCertificateImages([
            "product_id" => $product_icheck_id,
            "file_ids" => $file_ids
        ]);
    }

    /**
     * Handle a job failure.
     *
     */
    public function failed(ProductCertificateImagesSyncEvent $event, $exception)
    {
        var_dump($exception->getMessage());
    }

}
