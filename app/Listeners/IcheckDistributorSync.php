<?php

namespace App\Listeners;

use App\Events\DistributorApproveEvent;
use App\Events\ProductInfoApprove;
use App\Models\Business\Distributor;
use App\Models\Business\IcheckDistributor;
use App\Models\Business\IcheckProductDistributor;
use App\Models\Business\Product;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class IcheckDistributorSync implements ShouldQueue
{
    use InteractsWithQueue;
    public $tries = 5;

    /**
     * Handle the event.
     *
     * @param  ProductInfoApprove $event
     * @return void
     */
    public function handle(DistributorApproveEvent $event)
    {
        $productDistributor = $event->productDistributor;
        $product = Product::findOrFail($productDistributor->product_id);
        $distributor = Distributor::findOrFail($productDistributor->distributor_id);
        $data = [];
        $data['name'] = $distributor->name;
        $data['address'] = $distributor->address;
        if ($distributor->phone) {
            $data['contact'] = $distributor->phone;
        }
//        if ($distributor->phone && $distributor->email) {
//            $data['contact'] = 'Phone: ' . $distributor->phone . '. Email: ' . $distributor->email;
//        } elseif ($distributor->phone) {
//            $data['contact'] = 'Phone: ' . $distributor->phone;
//        } elseif ($distributor->email) {
//            $data['contact'] = 'Email: ' . $distributor->email;
//        }
        if ($distributor->website) {
            $data['site'] = $distributor->website;
        }
        $data['title_id'] = $productDistributor->title_id;
        $data['status'] = 1;
        $data['icheck_id'] = '';
        if ($distributor->icheck_ids == null || $distributor->icheck_ids == "[]") {
            //Create new Distributor in Icheck Data
            $p_distributor = IcheckDistributor::create($data);
            if ($p_distributor) {
                $distributor->update(['icheck_ids' => json_encode([$p_distributor->id])]);
                //Create new Product-Distributor in Icheck data
                $icheck_product_distributor = IcheckProductDistributor::updateOrCreate(['distributor_id' => $p_distributor->id, 'product_id' => $product->icheck_id, 'is_monopoly' => 0],['visible' => 1]);
                if ($icheck_product_distributor && $icheck_product_distributor->visible == 1) {
                    $productDistributor->update(['status' => 1, 'is_visible' => 1]);
                } else {
                    $productDistributor->update(['status' => 0, 'is_visible' => 0]);
                }
            } else {
                $productDistributor->update(['status' => 0, 'is_visible' => 0]);
            }
        } else {
            $icheck_distributor_ids = json_decode($distributor->icheck_ids, true);
            $check = 0;
            for ($i = 0; $i < count($icheck_distributor_ids); $i++) {
                $icheck_distributor = IcheckDistributor::find($icheck_distributor_ids[$i]);
                //Information Distributor Sync with iCheck data
                if ($icheck_distributor) {
                    if ($icheck_distributor->title_id == $productDistributor->title_id) {
						$icheck_distributor->update($data);
                        $check++;
                        $icheck_product_distributor = IcheckProductDistributor::updateOrCreate(['distributor_id' => $icheck_distributor->id, 'product_id' => $product->icheck_id, 'is_monopoly' => 0],['visible' => 1]);
                        if ($icheck_product_distributor && $icheck_product_distributor->visible == 1) {
                            $productDistributor->update(['status' => 1, 'is_visible' => 1]);
                        } else {
                            $productDistributor->update(['status' => 0, 'is_visible' => 0]);
                        }
                    }
                }
            }
            if ($check == 0) {
                $new_p_distributor = IcheckDistributor::create($data);
                if ($new_p_distributor) {
                    array_push($icheck_distributor_ids, $new_p_distributor->id);
                    $distributor->update(['icheck_ids' => json_encode($icheck_distributor_ids)]);
                    //Create new Product-Distributor in Icheck data
                    $icheck_product_distributor = IcheckProductDistributor::updateOrCreate(['distributor_id' => $new_p_distributor->id, 'product_id' => $product->icheck_id, 'is_monopoly' => 0],['visible' => 1]);
                    if ($icheck_product_distributor && $icheck_product_distributor->visible == 1) {
                        $productDistributor->update(['status' => 1, 'is_visible' => 1]);
                    } else {
                        $productDistributor->update(['status' => 0, 'is_visible' => 0]);
                    }
                } else {
                    $productDistributor->update(['status' => 0, 'is_visible' => 0]);
                }
            }

        }
    }

    /**
     * Handle a job failure.
     *
     */
    public function failed(DistributorApproveEvent $event, $exception)
    {
        var_dump($exception->getMessage());
//        $productDistributor = $event->productDistributor;
//        $productDistributor->update(['status' => 0, 'is_visible' => 0]);
    }
}
