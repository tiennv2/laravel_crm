<?php

namespace App\Listeners;

use App\Events\DistributorDisapproveEvent;
use App\Events\ProductInfoApprove;
use App\Mail\BusinessGateway\ProductDistributorMail;
use App\Models\Business\Business;
use App\Models\Business\Distributor;
use App\Models\Business\IcheckDistributor;
use App\Models\Business\IcheckProductDistributor;
use App\Models\Business\Product;
use App\Models\Business\ProductDistributor;
use App\Models\Business\User;
use App\Repository\Business\UserRepositoryInterface;
use Exception;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class IcheckDistributorDisapproveSync
{
//    use InteractsWithQueue;
//    public $tries = 5;


    /**
     * Handle the event.
     *
     * @param  ProductInfoApprove $event
     * @return void
     */
    private $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function handle(DistributorDisapproveEvent $event)
    {
        try {
            $data = $event->productDistributor;
            $productDistributor = ProductDistributor::findOrFail($data['id']);
            $product = Product::findOrFail($productDistributor->product_id);
            $distributor = Distributor::findOrFail($productDistributor->distributor_id);

            if ($distributor->icheck_ids != null && $distributor->icheck_ids != "[]") {
                $icheck_distributor_ids = json_decode($distributor->icheck_ids, true);
                for ($i = 0; $i < count($icheck_distributor_ids); $i++) {
                    $icheck_distributor = IcheckDistributor::find($icheck_distributor_ids[$i]);

                    if ($icheck_distributor && $icheck_distributor->title_id == $productDistributor->title_id) {
                        IcheckProductDistributor::where([['distributor_id', $icheck_distributor->id], ['product_id', $product->icheck_id]])->update(["visible" => 0]);
                    }
                }
            }
            if ($data['is_deleted']) {
                $productDistributor->delete();
            } else {
                $productDistributor->update(['status' => 2, 'note' => $data['note']]);
                if ($product->business_id) {
                    $business = Business::find($product->business_id);
                    if ($business) {
                        $extra_info = [];
                        $extra_info['product_name'] = $product->name;
                        $extra_info['distributor_name'] = $distributor->name;
                        $note = $data['note'];
                        //Send mail
                        $users = $this->userRepository->getUsersByBusinessId($business->id);
                        $emails = array_filter($users->pluck("email")->toArray());
                        if (count($emails) > 0) {
                            foreach ($emails as $email) {
                                Mail::to($email)->queue(new ProductDistributorMail($business, $extra_info, $note));
                            }
                        }
                    }
                }

//                $emails = User::where([['business_id', '=', $business->id], ['deleted_at', '=', null]])->pluck('email')->toArray();
//                if (count($emails) > 0) {
//                    foreach ($emails as $email) {
//                        Mail::to($email)->queue(new ProductDistributorMail($business, $extra_info, $note));
//                    }
//                }
            }

        } catch (Exception $exception) {
            dd($exception->getMessage());
        }
    }

    /**
     * Handle a job failure.
     *
     */
    public function failed(DistributorDisapproveEvent $event, $exception)
    {
        var_dump($exception->getMessage());
    }
}
