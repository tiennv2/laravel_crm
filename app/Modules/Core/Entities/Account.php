<?php

namespace App\Modules\Core\Entities;

use Illuminate\Database\Eloquent\Model;
use Firebase\JWT\JWT;

class Account extends Model
{
    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = 'createdAt';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'updatedAt';

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'icheck_user';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'u_account';

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'icheck_id';
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    /**
     * @return string
     */
    public function getFirebaseTokenAttribute()
    {
        $cert = json_decode(file_get_contents(config('firebase.service_account_key')), true);
        $serviceAccountEmail = $cert['client_email'];
        $privateKey = $cert['private_key'];
        $now = time();
        $payload = array(
            'iss' => $serviceAccountEmail,
            'sub' => $serviceAccountEmail,
            'aud' => 'https://identitytoolkit.googleapis.com/google.identity.identitytoolkit.v1.IdentityToolkit',
            'iat' => $now,
            'exp' => $now + (60 * 60),
            'uid' => $this->icheck_id,
        );

        return JWT::encode($payload, $privateKey, 'RS256');
    }
}
