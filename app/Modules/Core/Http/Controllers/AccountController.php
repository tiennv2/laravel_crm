<?php

namespace App\Modules\Core\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Core\Entities\Account;
use App\Modules\Core\Http\Resources\{Account as AccountResource, AccountCollection};

class AccountController extends Controller
{
    public function show(Account $account)
    {
        return new AccountResource($account);
    }
}
