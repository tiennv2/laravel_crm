<?php

namespace App\Modules\Core\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Core\Entities\Product;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    public function batchUpdateStatus(Request $request)
    {
        $productIds = $request->input('ids', []);
        $productIds = array_map('trim', $productIds);
        Product::whereIn('gtin_code', $productIds)->update(['status' => $request->input('status')]);

        return ['status' => true];
    }

    public function batchShowHideStore(Request $request)
    {
        $productIds = $request->input('ids', []);
        $productIds = array_map('trim', $productIds);
        $action = $request->input('action', 1);
        $table = DB::connection('icheck_gateway')->table('b_product_config');
        $existProductIds = $table->whereIn('gtin_code', $productIds)
            ->where('type', 'local')
            ->pluck('gtin_code');
        $diffIds = array_diff($productIds, $existProductIds->toArray());

        if (!empty($diffIds)) {
            $newData = [];

            foreach ($diffIds as $id) {
                $newData[] = ['gtin_code' => $id, 'on_off' => $action, 'type' => 'local'];
            }

            $table->insert($newData);
        }

        $table->whereIn('gtin_code', $productIds)
            ->where('type', 'local')
            ->update(['on_off' => $action]);

        return ['status' => true];
    }
}
