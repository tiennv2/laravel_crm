<?php

namespace App\Modules\Core\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Account extends Resource
{
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'icheckId'      => $this->icheck_id,
            'name'          => $this->name,
            'phone'         => $this->phone,
            'firebaseToken' => $this->firebase_token,
            'createdAt'     => $this->created_at ? $this->created_at->format('c') : null,
            'updatedAt'     => $this->updated_at ? $this->updated_at->format('c') : null,
        ];
    }
}
