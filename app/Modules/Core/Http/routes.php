<?php

Route::group([
    'middleware' => 'web',
    'prefix' => 'core',
    'namespace' => 'App\Modules\Core\Http\Controllers'
], function() {
    Route::group([
        'prefix' => 'accounts',
    ], function () {
        Route::get('{account}', ['uses' => 'AccountController@show']);
    });

    Route::group([
        'prefix' => 'products',
    ], function () {
        Route::post('batchUpdateStatus', ['uses' => 'ProductController@batchUpdateStatus']);
        Route::post('batchShowHideStore', ['uses' => 'ProductController@batchShowHideStore']);
    });
});
