<?php

namespace App\Modules\Core\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request as Psr7Request;
use GuzzleHttp\Exception\RequestException;
use Maatwebsite\Excel\Excel;
use PhpOffice\PhpSpreadsheet\IOFactory;

class ImportProductStore extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'core:import-product-store';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import product store';

    /**
     * Excel instance.
     *
     * @var \Maatwebsite\Excel\Excel
     */
    protected $excel;

    /**
     * Create a new command instance.
     *
     * @param  Excel  $excel
     * @return void
     */
    public function __construct(Excel $excel)
    {
        parent::__construct();

        $this->excel = $excel;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $spreadsheet = IOFactory::load(base_path('a.xlsx'));
        $worksheet = $spreadsheet->getActiveSheet();

        foreach ($worksheet->getRowIterator(1, 1) as $row) {
            $firstRow = $row;
            break;
        }

        $cellIterator = $row->getCellIterator();
        $cellIterator->setIterateOnlyExistingCells(FALSE);

        $columnMap = [];

        foreach ($cellIterator as $addr => $cell) {
            switch ($cell) {
                case 'name':
                    $columnMap[$addr] = 'name';
                    break;

                case 'address':
                    $columnMap[$addr] = 'address';
                    break;

                case 'district':
                    $columnMap[$addr] = 'district';
                    break;

                case 'city':
                    $columnMap[$addr] = 'city';
                    break;
            }
        }

        $columnMap = ['A' => 'name', 'B' => 'address', 'C' => 'district', 'D' => 'city'];
        $stores = [];

        foreach ($worksheet->getRowIterator(2) as $i => $row) {
            $data = [];

            foreach ($columnMap as $j => $name) {
                $data[$name] = $worksheet->getCell($j . $i)->getValue();
            }

            if (!$data['name']) {
                continue;
            }

            dump($data);

            $stores[] = $data;
        }

        $client = new Client();
        $stores = array_map(function ($store) use ($client) {
            if (!isset($store['latitude']) or !isset($store['longitude'])) {
                try {
                    $response = $client->request('GET', 'https://maps.googleapis.com/maps/api/geocode/json', [
                        'query' => [
                            'address' => $store['address'] . ', ' . $store['district'] . ', ' . $store['city'],
                            'key' => 'AIzaSyCe546KUQ0N7-XgmfwwWDMIKXcwvEWBq_o',
                        ],
                    ]);
                    $data = json_decode((string) $response->getBody(), true);

                    if (isset($data['results']) and !empty($data['results'])) {
                        $addr = $data['results'][0];
                        $store['address'] = $addr['formatted_address'];
                        $store['latitude'] = $addr['geometry']['location']['lat'];
                        $store['longitude'] = $addr['geometry']['location']['lng'];
                    }
                } catch (RequestException $e) {
                    var_dump($e);
                }
            }


            try {
                $response = $client->request('POST', config('api.gateway') . '/locations', [
                    'json' => [
                        'business' => 17723,
                        'name' => $store['name'],
                        'address' => $store['address'],
                        'latitude' => isset($store['latitude']) ? $store['latitude'] : 0,
                        'longitude' => isset($store['longitude']) ? $store['longitude'] : 0,
                    ],
                ]);
                $data = json_decode((string) $response->getBody(), true);
                if (isset($data['id'])) {
                    $store['location_id'] = $data['id'];
                }
            } catch (RequestException $e) {
                var_dump($e);
            }

            return $store;
        }, $stores);

        var_dump('ok');

        $spreadsheet = IOFactory::load(base_path('b.xlsx'));
        $worksheet = $spreadsheet->getActiveSheet();

        foreach ($worksheet->getRowIterator() as $row) {
            foreach ($row->getCellIterator() as $a => $cell) {
                if ($a == 'A') {
                    $gtin = $cell->getValue();
                    continue;
                }

                if ($a == 'B') {
                    $price = $cell->getValue();
                    continue;
                }

                if ($a == 'C') {
                    $price = $cell->getValue();
                    break;
                }
            }
            var_dump('s' . $gtin);
            var_dump('c' . $price);

            // try {
            //     $response = $client->request('GET', config('api.gateway') . '/backend/locations', [
            //         'query' => [
            //             'gtin_code' => $gtin,
            //             'limit' => 9999,
            //         ],
            //     ]);
            //     $data = json_decode((string) $response->getBody(), true);
            //     $data = $data['data'];

            //     foreach ($data as $d) {
            //         $response = $client->request('DELETE', config('api.gateway') . '/backend/locations/' . $d['id']);
            //         $c = json_decode((string) $response->getBody(), true);
            //     }
            // } catch (RequestException $e) {
            //     var_dump($e);
            // }

            foreach ($stores as $store) {
                var_dump($store);
                if (isset($store['location_id'])) {
                    try {
                        var_dump(config('api.gateway') . '/backend/mappinglocations');
                        $response = $client->request('POST', config('api.gateway') . '/backend/mappinglocations', [
                            'json' => [
                                'location_id' => $store['location_id'],
                                'gtin_code' => $gtin,
                                'price' => $price,
                            ],
                        ]);
                        var_dump(config('api.gateway') . '/backend/mappinglocations');
                        $data = json_decode((string) $response->getBody(), true);

                        var_dump($data);
                    } catch (RequestException $e) {
                        var_dump($e);
                    } catch (\Exception $e) {
                        var_dump($e);
                    }
                }
            }
        }


        // var_dump($addresses);

        // var_dump($columnMap);
        // var_dump($sheet);
        // $this->excel->load(base_path('a.csv'));
        // $client = new Client();

        // try {
        //     $response = $client->request('GET', 'https://maps.googleapis.com/maps/api/geocode/json', [
        //         'query' => [
        //             'address' => '243 Chu Văn An, P.12, Q.Bình Thạnh',
        //             'key' => 'AIzaSyCe546KUQ0N7-XgmfwwWDMIKXcwvEWBq_o',
        //         ],
        //     ]);
        //     $data = json_decode((string) $response->getBody(), true);

        //     if (isset($data['results']) and !empty($data['results'])) {
        //         $addr = $data['results'][0];
        //         $formattedAddress = $data['formatted_address'];
        //         $latitude = $addr['geometry']['location']['lat'];
        //         $longitude = $addr['geometry']['location']['lng'];
        //     }
        // } catch (RequestException $e) {
        // }
    }
}
