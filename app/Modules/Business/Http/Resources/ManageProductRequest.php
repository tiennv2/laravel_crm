<?php

namespace App\Modules\Business\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class ManageProductRequest extends Resource
{
    public function toArray($request)
    {
        return [
            'id'        => $this->id,
            'sender'    => new Business($this->whenLoaded('sender')),
            'product'   => new Product($this->whenLoaded('product')),
            'createdAt' => $this->created_at ? $this->created_at->format('c') : null,
            'updatedAt' => $this->updated_at ? $this->updated_at->format('c') : null,
        ];
    }
}
