<?php

namespace App\Modules\Business\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Plan extends Resource
{
    public function toArray($request)
    {
        return [
            'id'        => $this->id,
            'name'      => $this->name,
            'quota'     => $this->quota,
            'price'     => $this->price,
            'createdAt' => $this->created_at ? $this->created_at->format('c') : null,
            'updatedAt' => $this->updated_at ? $this->updated_at->format('c') : null,
        ];
    }
}
