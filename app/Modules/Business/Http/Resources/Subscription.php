<?php

namespace App\Modules\Business\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Subscription extends Resource
{
    public function toArray($request)
    {
        return [
            'id'        => $this->id,
            'name'      => $this->name,
            'days'      => $this->days,
            'business'  => new Business($this->whenLoaded('business')),
            'plan'      => new Plan($this->whenLoaded('plan')),
            'expiresOn' => $this->expires_on ? $this->expires_on->format('c') : null,
            'createdAt' => $this->created_at ? $this->created_at->format('c') : null,
            'updatedAt' => $this->updated_at ? $this->updated_at->format('c') : null,
        ];
    }
}
