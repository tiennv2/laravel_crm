<?php

namespace App\Modules\Business\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Product extends Resource
{
    public function toArray($request)
    {
        return [
            'id'        => $this->id,
            'barcode'   => $this->barcode,
            'createdAt' => $this->created_at ? $this->created_at->format('c') : null,
            'updatedAt' => $this->updated_at ? $this->updated_at->format('c') : null,
        ];
    }
}
