<?php

namespace App\Modules\Business\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Business extends Resource
{
    public function toArray($request)
    {
        return [
            'id'          => $this->id,
            'name'        => $this->name,
            'address'     => $this->address,
            'email'       => $this->email,
            'phoneNumber' => $this->phone_number,
            'fax'         => $this->fax,
            'website'     => $this->website,
            'status'      => $this->status,
            'activatedAt' => $this->activated_at ? $this->activated_at->format('c') : null,
            'createdAt'   => $this->created_at ? $this->created_at->format('c') : null,
            'updatedAt'   => $this->updated_at ? $this->updated_at->format('c') : null,
            'deletedAt'   => $this->deleted_at ? $this->deleted_at->format('c') : null,
        ];
    }
}
