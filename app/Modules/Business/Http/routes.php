<?php

Route::group(['middleware' => 'web', 'prefix' => 'business', 'namespace' => 'App\\Modules\Business\Http\Controllers'], function()
{
    Route::group([
        'prefix' => 'businesses',
    ], function () {
        Route::post('/', ['uses' => 'BusinessController@store']);
        Route::post('query', ['uses' => 'BusinessController@query']);
        Route::get('{business}', ['uses' => 'BusinessController@show']);
        Route::put('{business}', ['uses' => 'BusinessController@update']);
        Route::delete('{business}', ['uses' => 'BusinessController@delete']);
        Route::post('{business}/assignProduct', 'BusinessController@batchAssignProduct');
    });

    Route::group([
        'prefix' => 'gln',
    ], function () {
        Route::post('/', ['uses' => 'GLNController@store']);
        Route::post('query', ['uses' => 'GLNController@query']);
    });

    Route::group([
        'prefix' => 'products',
    ], function () {
        Route::get('{product}/businesses', 'ProductController@businesses');
    });

    Route::group([
        'prefix' => 'plans',
    ], function () {
        Route::get('/', 'PlanController@index');
        Route::post('/', 'PlanController@store');
    });

    Route::group([
        'prefix' => 'subscriptions',
    ], function () {
        Route::get('/', 'SubscriptionController@index');
        Route::put('{subscription}', 'SubscriptionController@update');
        Route::delete('{subscription}', 'SubscriptionController@delete');
    });

    Route::group([
        'prefix' => 'manageProductRequests',
    ], function () {
        Route::get('/', 'ManageProductRequestController@index');
        Route::post('batchUpdate', 'ManageProductRequestController@batchUpdate');
        Route::get('{request}', 'ManageProductRequestController@show');
        Route::put('{request}', 'ManageProductRequestController@update');
    });
});
