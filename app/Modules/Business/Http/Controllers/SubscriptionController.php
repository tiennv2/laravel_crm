<?php

namespace App\Modules\Business\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Business\Entities\Subscription;
use App\Modules\Business\Http\Resources\{Subscription as SubscriptionResource, SubscriptionCollection};
use Carbon\Carbon;

class SubscriptionController extends Controller
{
    public function index()
    {
        return new SubscriptionCollection(
            Subscription::where('status', Subscription::STATUS_PENDING_ACTIVATE)->orderBy('created_at', 'asc')->get()
        );
    }

    public function update(Subscription $subscription, Request $request)
    {
        $subscription->update($request->all());

        return new SubscriptionResource($subscription);
    }

    public function delete(Subscription $subscription)
    {
        $subscription->delete();

        return response(null, 204);
    }
}
