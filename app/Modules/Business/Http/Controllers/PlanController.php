<?php

namespace App\Modules\Business\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Business\Entities\Plan;
use App\Modules\Business\Http\Resources\{Plan as PlanResource, PlanCollection};

class PlanController extends Controller
{
    public function index()
    {
        return new PlanCollection(Plan::orderBy('created_at', 'desc')->get());
    }

    public function store(Request $request)
    {
        $data = $request->all();

        return new PlanResource(Plan::create($data));
    }
}
