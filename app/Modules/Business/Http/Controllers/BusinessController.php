<?php

namespace App\Modules\Business\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Business\Entities\{Business};
use App\Modules\Business\Http\Resources\{Business as BusinessResource, BusinessCollection};
use App\Modules\Business\Jobs\AssignProduct;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request as Psr7Request;
use GuzzleHttp\Exception\RequestException;

class BusinessController extends Controller
{
    public function query(Request $request)
    {
        $rowGroupCols = $request->input('rowGroupCols', []);
        $groupKeys    = $request->input('groupKeys', []);
        $valueCols    = $request->input('valueCols', []);
        $filterModel  = $request->input('filterModel', []);
        $sortModel    = $request->input('sortModel', []);
        $startRow     = $request->input('startRow', 0);
        $endRow       = $request->input('endRow', 100);

        $data = new Business;

        if (count($rowGroupCols) > count($groupKeys)) {
            $select = [];
            $rowGroupCol = $rowGroupCols[count($groupKeys)];
            $select[] = $rowGroupCol['field'];

            foreach ($valueCols as $valueCol) {
                $select[] = $valueCol;
            }

            $data = $data->select($select);

            $colsToGroupBy = [];
            $colsToGroupBy[] = $rowGroupCol['field'];

            foreach ($colsToGroupBy as $col) {
                $data = $data->groupBy($col);
            }
        }

        $whereParts = [];

        if (count($groupKeys) > 0) {
            foreach ($groupKeys as $index => $key) {
                $whereParts[] = [$rowGroupCols[$index]['field'], '=', $key];
            }
        }

        // if ($filterModel) {
        //     foreach ($groupKeys as $key => $index) {
        //         $whereParts[] = [$rowGroupCols[$index]['field'], '=', $key];
        //     }
        // }

        foreach ($whereParts as $where) {
            $data = $data->where($where[0], $where[1], $where[2]);
        }

        if (count($sortModel) > 0) {
            foreach ($sortModel as $item) {
                $data = $data->orderBy($item['colId'], $item['sort']);
            }
        }

        $pageSize = $endRow - $startRow;
        $count = $data->count();
        $data = $data->skip($startRow)->take($pageSize)->get();

        return (new BusinessCollection($data))->additional([
            'meta' => [
                'count' => $count,
            ],
        ]);
    }

    public function show(Business $business)
    {
        return new BusinessResource($business);
    }

    public function store(Request $request)
    {
        $data = $request->all();

        if (array_key_exists('phoneNumber', $data)) {
            $data['phone_number'] = $data['phoneNumber'];
        }

        $business = Business::create($data);
        // Sync iCheck account
        $client = new Client([
            'base_uri' => config('api.core'),
        ]);

        try {
            $response = $client->request('GET', '/backend/users/query-one?q=' . $business->email);
            $data = json_decode((string) $response->getBody(), true);

            if ($data['status'] != 200) {
                $response = $client->request('POST', '/backend/users', [
                    'json' => [
                        'type' => 3,
                        'name' => $business->name,
                        'email' => $business->email,
                        'email_verified' => true,
                        'phone' => $business->phone_number,
                        'phone_verified' => true,
                        'password' => str_random(16),
                    ],
                ]);
                $data = json_decode((string) $response->getBody(), true);
            }

            if (isset($data['data']['icheck_id'])) {
                $business->icheck_id = $data['data']['icheck_id'];
                $business->save();
            }
        } catch (RequestException $e) {
        }

        return new BusinessResource($business);
    }

    public function update(Business $business, Request $request)
    {
        $data = $request->all();

        if (array_key_exists('phoneNumber', $data)) {
            $data['phone_number'] = $data['phoneNumber'];
        }

        $business->update($data);

        return new BusinessResource($business);
    }

    public function delete(Business $business)
    {
        $business->delete();

        return response(null, 204);
    }

    public function batchAssignProduct(Business $business, Request $request)
    {
        $productIds = $request->input('ids', []);
        $productIds = array_map('trim', $productIds);
        AssignProduct::dispatch($business, $productIds)->onQueue('old');

        return ['status' => true];
    }
}
