<?php

namespace App\Modules\Business\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Business\Entities\Product;
use App\Modules\Business\Http\Resources\BusinessCollection;

class ProductController extends Controller
{
    public function businesses(Product $product)
    {
        return new BusinessCollection($product->businesses);
    }
}
