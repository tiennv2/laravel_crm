<?php

namespace App\Modules\Business\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Business\Entities\ManageProductRequest;
use App\Modules\Business\Http\Resources\{ManageProductRequest as ManageProductRequestResource, ManageProductRequestCollection};
use Carbon\Carbon;

class ManageProductRequestController extends Controller
{
    public function index()
    {
        return new ManageProductRequestCollection(
            ManageProductRequest::where('status', ManageProductRequest::STATUS_IN_REVIEW)->orderBy('created_at', 'asc')->get()
        );
    }

    public function show(ManageProductRequest $request)
    {
        return new ManageProductRequestResource($request);
    }

    public function update(ManageProductRequest $request, Request $r)
    {
        $request->update($r->all());

        return new ManageProductRequestResource($request);
    }

    public function batchUpdate(Request $request)
    {
        foreach (ManageProductRequest::whereIn('id', $request->input('ids'))->get() as $manageProductRequest) {
            $manageProductRequest->update($request->input('data'));
        }

        return ['success' => true];
    }
}
