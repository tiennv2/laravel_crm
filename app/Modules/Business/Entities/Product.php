<?php

namespace App\Modules\Business\Entities;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'icheck_old_business';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'products';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'barcode';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'attrs' => 'object',
        'images' => 'array',
        'attributes' => 'object',
        'is_quota' => 'boolean',
    ];

    /**
     * Get the manager record associated with the product.
     */
    public function manager()
    {
        return $this->belongsTo(Business::class, 'business_id', 'id');
    }

    /**
     * Get the producer record associated with the product.
     */
    public function producer()
    {
        return $this->belongsTo(Business::class, 'producer_id', 'id');
    }

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'expiration_date',
    ];

    /**
     * The businesses that belong to the product.
     */
    public function businesses()
    {
        return $this->belongsToMany(Business::class, 'business_product', 'product_id', 'business_id');
    }
}
