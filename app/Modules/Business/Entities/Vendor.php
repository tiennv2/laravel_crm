<?php

namespace App\Modules\Business\Entities;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'icheck_old_business';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'vendors';

    const STATUS_DISAPPROVED        = 0;
    const STATUS_APPROVED           = 1;
    const STATUS_PENDING_APPROVAL   = 2;
    const STATUS_PENDING_DELETE     = 3;
    const STATUS_PENDING_ACTIVATION = 4;

    public static $statusTexts = [
        self::STATUS_DISAPPROVED        => 'disapproved',
        self::STATUS_APPROVED           => 'approved',
        self::STATUS_PENDING_APPROVAL   => 'pendingApproval',
        self::STATUS_PENDING_DELETE     => 'pendingDelete',
        self::STATUS_PENDING_ACTIVATION => 'pendingActivation',
    ];
}
