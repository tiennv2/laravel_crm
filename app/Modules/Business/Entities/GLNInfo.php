<?php

namespace App\Modules\Business\Entities;

use Illuminate\Database\Eloquent\Model;

class GLNInfo extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'icheck_old_business';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'gln';
}
