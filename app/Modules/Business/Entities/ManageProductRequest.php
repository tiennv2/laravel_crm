<?php

namespace App\Modules\Business\Entities;

use Illuminate\Database\Eloquent\Model;

class ManageProductRequest extends Model
{
    const STATUS_IN_REVIEW = 0;
    const STATUS_APPROVED  = 1;
    const STATUS_REJECTED  = 2;

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'icheck_old_business';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'requests';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'files' => 'array',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'status',
    ];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['sender', 'product'];

    /**
     * Get the sender record associated with the manage product request.
     */
    public function sender()
    {
        return $this->belongsTo(Business::class, 'business_id', 'id');
    }

    /**
     * Get the product record associated with the manage product request.
     */
    public function product()
    {
        return $this->belongsTo(Product::class, 'barcode', 'barcode');
    }
}
