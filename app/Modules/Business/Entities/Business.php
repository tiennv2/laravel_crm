<?php

namespace App\Modules\Business\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Business extends Model
{
    use SoftDeletes;

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'icheck_old_business';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'businesses';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'activated_at', 'deleted_at',
        'end_date',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'address', 'email', 'phone_number', 'fax', 'website', 'status',
    ];

    /**
     * The vendors that belong to the business.
     */
    public function vendors()
    {
        return $this->hasMany(Vendor::class, 'business_id', 'id');
    }

    /**
     * The products that belong to the business.
     */
    public function products()
    {
        return $this->belongsToMany(Product::class, 'business_product', 'business_id', 'product_id');
    }

    public function roles()
    {
        return $this->belongsToMany(BRole::class,'business_role', 'business_id', 'brole_id');
    }
}
