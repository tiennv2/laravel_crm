<?php

namespace App\Modules\Business\Entities;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    const STATUS_PENDING_ACTIVATE = 0;
    const STATUS_ACTIVATED = 1;
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'icheck_old_business';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'subscriptions';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'expires_on',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'expires_on', 'status',
    ];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['business', 'plan'];

    /**
     * Get the business record associated with the subscription.
     */
    public function business()
    {
        return $this->belongsTo(Business::class, 'business_id', 'id');
    }

    /**
     * Get the plan record associated with the subscription.
     */
    public function plan()
    {
        return $this->belongsTo(Plan::class, 'plan_id', 'id');
    }
}
