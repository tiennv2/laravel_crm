<?php

namespace App\Modules\Business\Console;

use Illuminate\Console\Command;
use App\Models\Product\Product as ICheckProduct;
use App\Modules\Business\Entities\{Product, Vendor};

class SyncProductInfo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'business:sync:product-info
                           {--id=* : The ID of the product(s)}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync product info from main databse';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $ids = $this->option('id');

        foreach ($ids as $id) {
            $source = ICheckProduct::with('vendor', 'attributes')->where('gtin_code', $id)->first();

            if ($source) {
                if (!($product = Product::where('barcode', $id)->first())) {
                    $product = new Product;
                }

                $product->name = $source->product_name;
                $product->barcode = $id;
                $product->gln = @$source->vendor->gln_code;
                $attributes = [];

                foreach ($source->attributes as $attribute) {
                    $attributes[$attribute->id] = $attribute->pivot->content;
                }

                $product->attributes = $attributes;
                $product->attrs = $attributes;
                $product->status = 1;
                $product->save();
            }
        }
    }
}
