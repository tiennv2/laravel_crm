<?php

namespace App\Modules\Business\Console;

use Illuminate\Console\Command;
use App\Models\Product\Product as ICheckProduct;
use App\Models\Product\Vendor as ICheckVendor;
use App\Modules\Business\Entities\{Business, Product, Vendor, GLNInfo};

class AssignProduct extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'business:assign-product
                           {business : Business ID}
                           {--p|product-id=* : The ID of the product(s)}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Assign product for business';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $businessId = $this->argument('business');
        $business = Business::withCount('products')->findOrFail($businessId);
        $productIds = $this->option('product-id');
        // Sản phẩm sẽ được gán cho tài khoản daonh nghiệp
        $assignIds = array_filter($productIds, function ($id) {
            return $id[0] != '-';
        });
        // Sản phảm sẽ bị loại bỏ khỏi tài khoản doanh nghiệp
        $unassignIds = array_filter($productIds, function ($id) {
            return $id[0] == '-';
        });
        $unassignIds = array_map(function ($id) {
            return ltrim($id, '-');
        }, $unassignIds);

        if (count($unassignIds) > 0) {
            $business->products()->detach($unassignIds);
            // TODO: remove is_quota
            Product::whereIn('barcode', $unassignIds)->update(['is_quota' => false]);
            // TODO: Chuyển sang command vefify product
            ICheckProduct::whereIn('gtin_code', $unassignIds)->update([
                'verify_owner'    => false,
                'expiration_date' => null,
            ]);
        }

        // Tính quota của doanh nghiệp. TODO: Thay đổi cách tính
        $role = $business->roles()->first();

        if (!$role) {
            return ['status' => false];
        }

        $limit = $role->quota;
        // Hết tính quota

        $used = $business->products_count;
        $remain = $limit - $used;

        if ($remain > 0 and count($assignIds) > 0) {
            $assignIds = array_slice($assignIds, 0, $remain);
            // Đoạn này đồng bộ các sản phẩm chưa có trong DB doanh nghiệp
            $existProductIds = Product::whereIn('barcode', $assignIds)->pluck('barcode');
            $diffIds = array_diff($assignIds, $existProductIds->toArray());
            $this->call('business:sync:product-info', [
                '--id' => $diffIds
            ]);
            $business->products()->syncWithoutDetaching($assignIds);

            // TODO: remove is_quota true]);
            Product::whereIn('barcode', $assignIds)->update(['is_quota' => true]);
            // TODO: Chuyển sang command vefify product
            ICheckProduct::whereIn('gtin_code', $assignIds)->update([
                'verify_owner'    => true,
                'expiration_date' => $business->end_date,
            ]);
            $gln = Product::whereIn('barcode', $assignIds)->pluck('gln');
            $glnIds = Product::whereIn('barcode', $assignIds)->pluck('gln_id');
            $gln2 = GLNInfo::whereIn('id', $glnIds)->pluck('gln');
            $gln = array_merge($gln->toArray(), $gln2->toArray());
            ICheckVendor::whereIn('gln_code', $gln)->update(['is_verify' => 1]);
        }
    }
}
