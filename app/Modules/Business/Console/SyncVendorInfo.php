<?php

namespace App\Modules\Business\Console;

use Illuminate\Console\Command;

class SyncVendorInfo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'business:sync:vendor-info';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync product info from main databse';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    }
}
