<?php

namespace App\Modules\Business\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Modules\Business\Entities\Business;
use Artisan;

class AssignProduct implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Business.
     *
     * @var \App\Modules\Business\Entities\Business
     */
    protected $business;

    /**
     * Product IDs.
     *
     * @var array
     */
    protected $productIds;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Business $business, array $productIds)
    {
        $this->business = $business;
        $this->productIds = $productIds;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Artisan::call('business:assign-product', [
            'business'     => $this->business->id,
            '--product-id' => $this->productIds,
        ]);
    }
}
