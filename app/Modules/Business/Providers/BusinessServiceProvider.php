<?php

namespace App\Modules\Business\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;
use App\Modules\Business\Console\{AssignProduct, SyncProductInfo, SyncVendorInfo};
use App\Modules\Business\Entities\{ManageProductRequest, Subscription};
use App\Modules\Business\Observers\{ManageProductRequestObserver, SubscriptionObserver};

class BusinessServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
        $this->registerCommands();
        $this->registerObservers();
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__.'/../Config/config.php' => config_path('business.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__.'/../Config/config.php', 'business'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/business');

        $sourcePath = __DIR__.'/../Resources/views';

        $this->publishes([
            $sourcePath => $viewPath
        ],'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/business';
        }, \Config::get('view.paths')), [$sourcePath]), 'business');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/business');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'business');
        } else {
            $this->loadTranslationsFrom(__DIR__ .'/../Resources/lang', 'business');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (! app()->environment('production')) {
            app(Factory::class)->load(__DIR__ . '/../Database/factories');
        }
    }

    /**
     * Register commands.
     *
     * @return void
     */
    protected function registerCommands()
    {
        $this->commands([
            AssignProduct::class,
            SyncProductInfo::class,
            SyncVendorInfo::class,
        ]);
    }

    /**
     * Register observers.
     *
     * @return void
     */
    protected function registerObservers()
    {
        ManageProductRequest::observe(ManageProductRequestObserver::class);
        Subscription::observe(SubscriptionObserver::class);
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
