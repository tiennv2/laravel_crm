<?php

namespace App\Modules\Business\Observers;

use App\Modules\Business\Entities\Subscription;
use Carbon\Carbon;

class SubscriptionObserver
{
    /**
     * Listen to the Subscription updating event.
     *
     * @param  \App\Modules\Business\Entities\Subscription  $subscription
     * @return void
     */
    public function updating(Subscription $subscription)
    {
        $changes = $subscription->getDirty();
        $original = $subscription->getOriginal();

        if (array_key_exists('status', $changes)) {
            // Cập nhật trạng thái từ "Chờ kích hoạt" sang "Đã kích hoạt"
            if ($original['status'] == Subscription::STATUS_PENDING_ACTIVATE
                and $changes['status'] == Subscription::STATUS_ACTIVATED
            ) {
                $subscription->fill(['expires_on' => Carbon::now()->addDays($subscription->days)]);
            }
        }
    }
}
