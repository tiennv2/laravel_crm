<?php

namespace App\Modules\Business\Observers;

use App\Modules\Business\Entities\{ManageProductRequest, Product};

class ManageProductRequestObserver
{
    /**
     * Listen to the Manage product request updated event.
     *
     * @param  \App\Modules\Business\Entities\ManageProductRequest  $request
     * @return void
     */
    public function updated(ManageProductRequest $request)
    {
        $changes = $request->getDirty();
        $original = $request->getOriginal();

        if (array_key_exists('status', $changes)) {
            // Cập nhật trạng thái từ "Đang duyệt" sang "Đã chấp nhận"
            if ($original['status'] == ManageProductRequest::STATUS_IN_REVIEW) {
                if ($changes['status'] == ManageProductRequest::STATUS_APPROVED) {
                    Product::where('barcode', $request->barcode)->where('id', '<>', $request->product->id)->delete();
                } else if ($changes['status'] == ManageProductRequest::STATUS_REJECTED) {
                    Product::destroy($request->product->id);
                }
            }
        }
    }
}
