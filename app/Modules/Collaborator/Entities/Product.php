<?php

namespace App\Modules\Collaborator\Entities;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    const NEED_UPDATE_GLN_INFO = 1;
    const NEED_UPDATE_CATEGORY = 2;
    const NEED_UPDATE_NAME = 4;
    const NEED_UPDATE_IMAGE = 8;
    const NEED_UPDATE_DESCRIPTION = 16;

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'icheck_collaborator';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'product_need_update';

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'gtin';
    }

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'search_results' => 'array',
        'is_active' => 'boolean',
        'status' => 'boolean',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'assigned_at',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'assigned_to', 'assigned_at', 'need_update',
    ];

    /**
     * The roles that belong to the user.
     */
    public function groups()
    {
        return $this->belongsToMany(Group::class, 'gtin_group', 'gtin', 'group_id', 'gtin');
    }
}
