<?php

namespace App\Modules\Collaborator\Entities;

use Illuminate\Database\Eloquent\Model;

class IgnoredProduct extends Model
{
    const CREATED_AT = 'ignored_at';
    const UPDATED_AT = null;

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'icheck_collaborator';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ignored_products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'gtin', 'collaborator_id',
    ];
}
