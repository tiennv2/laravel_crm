<?php

namespace App\Modules\Collaborator\Entities;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'icheck_collaborator';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'groups';
}
