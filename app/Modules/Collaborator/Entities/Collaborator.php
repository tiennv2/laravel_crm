<?php

namespace App\Modules\Collaborator\Entities;

use Illuminate\Database\Eloquent\Model;

class Collaborator extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'icheck_collaborator';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'collaborators';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'icheck_id', 'balance',
    ];

    /**
     * The roles that belong to the user.
     */
    public function groups()
    {
        return $this->belongsToMany(Group::class, 'collaborator_group');
    }
}
