<?php

namespace App\Modules\Collaborator\Entities;

use Illuminate\Database\Eloquent\Model;

class ContributedInformation extends Model
{
    const STATUS_IN_REVIEW   = 0;
    const STATUS_APPROVED   = 1;
    const STATUS_REJECTED   = 2;

    const CREATED_AT = 'contributed_at';

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'icheck_collaborator';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'contributed_informations';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'gtin', 'profits', 'contributed_data', 'contributed_informations', 'contributed_by', 'status',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'contributed_data' => 'object',
    ];

    /**
     * Get the product record associated with the information.
     */
    public function product()
    {
        return $this->belongsTo(Product::class, 'gtin', 'gtin');
    }
}
