<?php

namespace App\Modules\Collaborator\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Collaborator\Entities\{Collaborator, Product, ContributedInformation, IgnoredProduct};
use App\Modules\Collaborator\Http\Resources\{
    Product as ProductResource,
    ProductCollection,
    ContributedInformation as ContributedInformationResource,
    ContributedInformationCollection
};
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class ContributeController extends Controller
{
    public function me()
    {
        $user = Auth::user();
        $collaborator = $this->getCollaborator($user);

        return ['data' => [
            'name' => $user->name,
            'balance' => $collaborator->balance,
        ]];
    }

    public function productNeedContribute()
    {
        $user = Auth::user();
        $collaborator = $this->getCollaborator($user);

        DB::connection('icheck_collaborator')->beginTransaction();

        $product = Product::where('assigned_to', $collaborator->id)->first();

        if (!$product) {
            $groupIds = $collaborator->groups->pluck('id')->toArray();
            $needUpdate = Product::NEED_UPDATE_GLN_INFO | Product::NEED_UPDATE_CATEGORY | Product::NEED_UPDATE_NAME | Product::NEED_UPDATE_IMAGE | Product::NEED_UPDATE_DESCRIPTION;

            Product::whereRaw('need_update & ? > 0', $needUpdate)
                ->where('assigned_to', null)
                ->where('is_active', true)
                ->where('status', true)
                ->where(function ($query) use ($groupIds) {
                    $query->doesntHave('groups');

                    if (!empty($groupIds)) {
                        $query->orWhereHas('groups', function ($query) use ($groupIds) {
                            $query->whereIn('groups.id', $groupIds);
                        });
                    }
                })
                ->whereNotIn('gtin', IgnoredProduct::where('collaborator_id', $collaborator->id)->pluck('gtin'))
                ->take(1)
                ->update([
                    'assigned_to' => $collaborator->id,
                    'assigned_at' => Carbon::now(),
                ]);
        }

        $product = Product::where('assigned_to', $collaborator->id)->first();

        DB::connection('icheck_collaborator')->commit();

        if ($product) {
            return new ProductResource($product);
        }

        return ['data' => null];
    }

    public function contribute(Product $product, Request $request)
    {
        $user = Auth::user();
        $collaborator = $this->getCollaborator($user);
        $typeOfDataContributed = 0;
        $profits = 0;
        $plus = Product::NEED_UPDATE_NAME | Product::NEED_UPDATE_IMAGE | Product::NEED_UPDATE_DESCRIPTION;

        if ($request->has('name') and (Product::NEED_UPDATE_NAME & $product->need_update) !== 0) {
            $typeOfDataContributed |= Product::NEED_UPDATE_NAME;
            $profits += 150;
        }

        if ($request->has('images') and ($images = $request->get('images')) and !empty($images) and (Product::NEED_UPDATE_IMAGE & $product->need_update) !== 0) {
            $typeOfDataContributed |= Product::NEED_UPDATE_IMAGE;
            $profits += 150;
        }

        if ($request->has('description') and $request->get('description') and (Product::NEED_UPDATE_DESCRIPTION & $product->need_update) !== 0) {
            $typeOfDataContributed |= Product::NEED_UPDATE_DESCRIPTION;
            $profits += 100;
        }

        if ($request->has('categories') and (Product::NEED_UPDATE_CATEGORY & $product->need_update) !== 0) {
            $typeOfDataContributed |= Product::NEED_UPDATE_CATEGORY;
            $profits += 100;
        }

        if ($request->has('glnInfo') and (Product::NEED_UPDATE_GLN_INFO & $product->need_update) !== 0) {
            $typeOfDataContributed |= Product::NEED_UPDATE_GLN_INFO;
            $profits += 150;
        }

        // if (($plus & $typeOfDataContributed) === $plus) {
        //     $profits += 50;
        // }

        DB::connection('icheck_collaborator')->beginTransaction();

        $info = ContributedInformation::create([
            'gtin'               => $product->gtin,
            'profits' => $profits,
            'contributed_data'                     => $request->all(),
            'contributed_informations' => $typeOfDataContributed,
            'contributed_by'           => $collaborator->id,
            'status'                   => ContributedInformation::STATUS_IN_REVIEW,
        ]);

        $product->update([
            'assigned_to' => null,
            'assigned_at' => null,
            'need_update' => $product->need_update ^ $typeOfDataContributed,
        ]);

        DB::connection('icheck_collaborator')->commit();

        return ['success' => true];
    }

    public function stats(Request $request)
    {
        $user = Auth::user();
        $collaborator = $this->getCollaborator($user);
        $stats = ContributedInformation::where('contributed_by', $collaborator->id)
            ->select('status', DB::raw('count(*) as total'))
            ->groupBy('status');
        $profits = ContributedInformation::where('contributed_by', $collaborator->id)
            ->where('status', ContributedInformation::STATUS_APPROVED);

        if ($request->has('fromDate')) {
            $stats = $stats->where('contributed_at', '>=' ,Carbon::createFromFormat('Y-m-d', $request->input('fromDate'))->startOfDay());
            $profits = $profits->where('contributed_at', '>=' ,Carbon::createFromFormat('Y-m-d', $request->input('fromDate'))->startOfDay());
        }

        if ($request->has('toDate')) {
            $stats = $stats->where('contributed_at', '<=' ,Carbon::createFromFormat('Y-m-d', $request->input('toDate'))->endOfDay());
            $profits = $profits->where('contributed_at', '<=' ,Carbon::createFromFormat('Y-m-d', $request->input('toDate'))->endOfDay());
        }

        $stats = $stats->pluck('total', 'status');
        $profits = $profits->sum('profits');
        $data = [
            'inReview' => $stats->get(ContributedInformation::STATUS_IN_REVIEW, 0),
            'approved' => $stats->get(ContributedInformation::STATUS_APPROVED, 0),
            'rejected' => $stats->get(ContributedInformation::STATUS_REJECTED, 0),
            'profits' => $profits,
        ];

        return ['data' => $data];
    }

    public function contributedProducts(Request $request)
    {
        $user = Auth::user();
        $collaborator = $this->getCollaborator($user);
        $contributedProducts = ContributedInformation::where('contributed_by', $collaborator->id)
            ->with('product')
            ->orderBy('contributed_at', 'desc');

        if ($request->has('fromDate')) {
            $contributedProducts = $contributedProducts->where('contributed_at', '>=' ,Carbon::createFromFormat('Y-m-d', $request->input('fromDate'))->startOfDay());
        }

        if ($request->has('toDate')) {
            $contributedProducts = $contributedProducts->where('contributed_at', '<=' ,Carbon::createFromFormat('Y-m-d', $request->input('toDate'))->endOfDay());
        }

        if ($request->has('status')) {
            $contributedProducts = $contributedProducts->where('status', $request->input('status'));
        }

        $contributedProducts = $contributedProducts->paginate($request->input('limit', 20));

        return (new ContributedInformationCollection(collect($contributedProducts->items())))->additional(['meta' => [
            'count' => $contributedProducts->total(),
        ]]);
    }

    public function getContributedProduct($contributedProduct)
    {
        $user = Auth::user();
        $collaborator = $this->getCollaborator($user);
        $contributedProduct = ContributedInformation::with('product')->findOrFail($contributedProduct);

        if ($contributedProduct->contributed_by != $collaborator->id) {
            abort(404);
        }

        return new ContributedInformationResource($contributedProduct);
    }

    public function putContributedProduct($contributedProduct, Request $request)
    {
        $user = Auth::user();
        $collaborator = $this->getCollaborator($user);
        $contributedProduct = ContributedInformation::with('product')->findOrFail($contributedProduct);

        if ($contributedProduct->contributed_by != $collaborator->id) {
            abort(404);
        }

        $typeOfDataContributed = 0;
        $profits = 0;
        $plus = Product::NEED_UPDATE_NAME | Product::NEED_UPDATE_IMAGE | Product::NEED_UPDATE_DESCRIPTION;

        if ($request->has('name') and (Product::NEED_UPDATE_NAME & $contributedProduct->contributed_informations) !== 0) {
            $typeOfDataContributed |= Product::NEED_UPDATE_NAME;
            $profits += 100;
        }

        if ($request->has('images') and (Product::NEED_UPDATE_IMAGE & $contributedProduct->contributed_informations) !== 0) {
            $typeOfDataContributed |= Product::NEED_UPDATE_IMAGE;
            $profits += 100;
        }

        if ($request->has('description') and (Product::NEED_UPDATE_DESCRIPTION & $contributedProduct->contributed_informations) !== 0) {
            $typeOfDataContributed |= Product::NEED_UPDATE_DESCRIPTION;
            $profits += 100;
        }

        if ($request->has('categories') and (Product::NEED_UPDATE_CATEGORY & $contributedProduct->contributed_informations) !== 0) {
            $typeOfDataContributed |= Product::NEED_UPDATE_CATEGORY;
            $profits += 150;
        }

        if ($request->has('glnInfo') and (Product::NEED_UPDATE_GLN_INFO & $contributedProduct->contributed_informations) !== 0) {
            $typeOfDataContributed |= Product::NEED_UPDATE_GLN_INFO;
            $profits += 150;
        }

        if (($plus & $typeOfDataContributed) === $plus) {
            $profits += 50;
        }

        DB::connection('icheck_collaborator')->beginTransaction();

        $contributedProduct->update([
            'profits' => $profits,
            'contributed_data'                     => $request->all(),
            'contributed_informations' => $typeOfDataContributed,
        ]);

        DB::connection('icheck_collaborator')->commit();

        return new ContributedInformationResource($contributedProduct);
    }

    public function ignore(Product $product)
    {
        $user = Auth::user();
        $collaborator = $this->getCollaborator($user);

        DB::connection('icheck_collaborator')->beginTransaction();

        $product->update([
            'assigned_to' => null,
            'assigned_at' => null,
        ]);
        IgnoredProduct::create([
            'gtin' => $product->gtin,
            'collaborator_id' => $collaborator->id,
        ]);

        DB::connection('icheck_collaborator')->commit();

        return ['success' => true];
    }

    protected function getCollaborator($user)
    {
        DB::connection('icheck_collaborator')->beginTransaction();

        $collaborator = Collaborator::where('icheck_id', $user->icheck_id)->first();

        if (!$collaborator) {
            $collaborator = Collaborator::create([
                'icheck_id' => $user->icheck_id,
            ]);
        }

        DB::connection('icheck_collaborator')->commit();

        return $collaborator;
    }
}
