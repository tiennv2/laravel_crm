<?php

namespace App\Modules\Collaborator\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Collaborator\Entities\{ProductCategory};
use App\Modules\Collaborator\Http\Resources\{ProductCategory as ProductCategoryResource, ProductCategoryCollection};

class ProductCategoryController extends Controller
{
    public function index(Request $request)
    {
        $categories = ProductCategory::select('*');

        if ($request->has('parentId')) {
            $parentId = $request->get('parentId');

            if ($parentId == 0) {
                $parentId = null;
            }

            $categories = $categories->where('parent_id', $parentId);
        }

        $categories = $categories->where('shop_id', 0)->get();

        return new ProductCategoryCollection($categories);
    }

    public function show($category)
    {
        $category = ProductCategory::findOrFail($category);

        return new ProductCategoryResource($category);
    }
}
