<?php

namespace App\Modules\Collaborator\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Collaborator\Entities\{Product, ContributedInformation};
use App\Modules\Collaborator\Http\Resources\{Product as ProductResource, ProductCollection};
use App\Modules\Collaborator\Http\Resources\{ContributedInformation as ContributedInformationResource, ContributedInformationCollection};
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $user = \App\User::find(1);
        $maxProducts = 10;
        DB::connection('icheck_collaborator')->beginTransaction();
        $count = Product::whereRaw('need_update & ? > 0', [Product::UPDATE_AFFILIATE_CATEGORY])
            ->where('assigned_to', $user->icheck_id)
            ->count();
        $remain = $maxProducts - $count;

        if ($remain > 0) {
            $products = Product::whereRaw('need_update & ? > 0', [Product::UPDATE_AFFILIATE_CATEGORY])
                ->where('assigned_to', null)
                ->take($remain)
                ->update(['assigned_to' => $user->icheck_id]);
        }

        $products = Product::whereRaw('need_update & ? > 0', [Product::UPDATE_AFFILIATE_CATEGORY])
            ->where('assigned_to', $user->icheck_id)
            ->take($maxProducts)
            ->get();
        DB::connection('icheck_collaborator')->commit();

        return new ProductCollection($products);
    }

    public function contributedInformations(Request $request)
    {
        $offset = $request->query->get('offset', 0);
        $limit = $request->query->get('limit', 2);

        $data = ContributedInformation::with('product');

        if ($request->has('contributedBy')) {
            $data = $data->where('contributed_by', $request->get('contributedBy'));
        }

        $count = $data->count();
        $data = $data->offset($offset)
            ->limit($limit)
            ->get();

        return (new ContributedInformationCollection($data))->additional(['meta' => [
            'count' => $count,
        ]]);
    }
}
