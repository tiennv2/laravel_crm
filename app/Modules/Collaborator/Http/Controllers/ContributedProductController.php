<?php

namespace App\Modules\Collaborator\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Collaborator\Entities\{Product};
use App\Modules\Collaborator\Http\Resources\{Product as ProductResource, ProductCollection};
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ContributedProductController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $user = \App\User::find(1);
        $maxProducts = 10;
        DB::connection('icheck_collaborator')->beginTransaction();
        $count = Product::whereRaw('need_update & ? > 0', [Product::UPDATE_AFFILIATE_CATEGORY])
            ->where('assigned_to', $user->icheck_id)
            ->count();
        $remain = $maxProducts - $count;

        if ($remain > 0) {
            $products = Product::whereRaw('need_update & ? > 0', [Product::UPDATE_AFFILIATE_CATEGORY])
                ->where('assigned_to', null)
                ->take($remain)
                ->update(['assigned_to' => $user->icheck_id]);
        }

        $products = Product::whereRaw('need_update & ? > 0', [Product::UPDATE_AFFILIATE_CATEGORY])
            ->where('assigned_to', $user->icheck_id)
            ->take($maxProducts)
            ->get();
        DB::connection('icheck_collaborator')->commit();

        return new ProductCollection($products);
    }
}
