<?php

namespace App\Modules\Collaborator\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class ContributedInformation extends Resource
{
    public function toArray($request)
    {
        return [
            'id'                      => $this->id,
            'gtin'                    => $this->gtin,
            'profits'                 => $this->profits,
            'product'                 => new Product($this->whenLoaded('product')),
            'contributedData'         => $this->contributed_data,
            'contributedInformations' => $this->contributed_informations,
            'contributedBy'           => $this->contributed_by,
            'contributedAt'           => $this->contributed_at ? $this->contributed_at->format('c') : null,
            'updatedAt'               => $this->updated_at ? $this->updated_at->format('c') : null,
            'status'                  => $this->status,
        ];
    }
}
