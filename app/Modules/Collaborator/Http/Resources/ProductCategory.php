<?php

namespace App\Modules\Collaborator\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class ProductCategory extends Resource
{
    public function toArray($request)
    {
        return [
            'id'       => $this->id,
            'name'     => $this->name,
            'parentId' => $this->parent_id,
        ];
    }
}
