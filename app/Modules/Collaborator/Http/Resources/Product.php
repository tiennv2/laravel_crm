<?php

namespace App\Modules\Collaborator\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Product extends Resource
{
    public function toArray($request)
    {
        return [
            'id'         => $this->id,
            'gtin'       => $this->gtin,
            'searchResults' => $this->search_results,
            'needUpdate' => $this->need_update,
            'createdAt'  => $this->created_at ? $this->created_at->format('c') : null,
            'updatedAt'  => $this->updated_at ? $this->updated_at->format('c') : null,
        ];
    }
}
