<?php

Route::group([
    'middleware' => 'web',
    'prefix' => 'collaborator',
    'namespace' => 'App\\Modules\Collaborator\Http\Controllers'
], function() {
    Route::group([
        'prefix' => 'productCategories',
    ], function () {
        Route::get('/', 'ProductCategoryController@index');
        Route::get('{categoryId}', 'ProductCategoryController@show');
    });

    Route::group([
        'prefix' => 'products',
    ], function () {
        Route::get('/', 'ProductController@index');
        Route::get('contributedInformations', 'ProductController@contributedInformations');
    });

    Route::group([
        'prefix' => 'contributedProducts',
    ], function () {
        Route::get('/', 'ContributedProductController@index');
    });

    Route::get('me', 'ContributeController@me');

    Route::group([
        'prefix' => 'contribute',
    ], function () {
        Route::get('productNeedContribute', 'ContributeController@productNeedContribute');
        Route::put('products/{product}', 'ContributeController@contribute');
        Route::delete('products/{product}', 'ContributeController@ignore');
        Route::get('stats', 'ContributeController@stats');
        Route::get('contributedProducts', 'ContributeController@contributedProducts');
        Route::get('contributedProducts/{product}', 'ContributeController@getContributedProduct');
        Route::put('contributedProducts/{product}', 'ContributeController@putContributedProduct');
    });
});
