<?php

Route::group(['middleware' => 'web', 'prefix' => 'contributedproduct', 'namespace' => 'App\\Modules\ContributedProduct\Http\Controllers'], function()
{
    Route::get('/', 'ContributedProductController@index');
});
