<?php

namespace App\Modules\ContributedProduct\Console\Commands;

use Illuminate\Console\Command;
use App\Models\{ContributedProduct, Product\Product};

class AutoReject extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'contributed-product:auto-reject';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Auto reject contributed product.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        do {
            $barcodes = ContributedProduct::where('status', ContributedProduct::STATUS_PENDING_REVIEW)
                ->where('auto_reject_passed', false)
                ->take(50)
                ->pluck('gtin_code')
            ;
            $willReject = [];

            if ($barcodes->count() > 0) {
                foreach ($barcodes as $barcode) {
                    if (!$this->validateBarcode($barcode)) {
                        $willReject[] = $barcode;
                    }
                }

                $products = Product::whereIn('gtin_code', $barcodes)->get(['product_name', 'gtin_code', 'image_default', 'verify_owner']);

                foreach ($products as $product) {
                    if (($product->name and $product->image_default) or $product->verify_owner) {
                        $willReject[] = $product->gtin_code;
                    }
                }

                if (count($willReject) > 0) {
                    ContributedProduct::whereIn('gtin_code', $willReject)->update(['status' => 200, 'auto_reject_passed' => true]);
                }

                $passed = array_diff($barcodes->toArray(), $willReject);

                if (count($passed) > 0) {
                    ContributedProduct::whereIn('gtin_code', $passed)->update(['auto_reject_passed' => true]);
                }
            }
        } while ($barcodes->count() > 0);
    }

    protected function calculateCheckDigit($barcode)
    {
        $barcode = (string) $barcode;

        if (!is_numeric($barcode)) {
            return null;
        }

        $length = strlen($barcode);

        if (!in_array($length, [7, 11, 12, 13, 16, 17])) {
            return null;
        }

        $num = $length - 1;
        $isMultiplyBy3 = true;
        $sum = 0;

        for ($i = $num; $i >= 0; $i--) {
            $sum += (int) $barcode[$i] * ($isMultiplyBy3 ? 3 : 1);
            $isMultiplyBy3 = !$isMultiplyBy3;
        }

        $nextMultipleOfTen = ceil($sum / 10) * 10;

        return (int) $nextMultipleOfTen - $sum;
    }

    protected function validateBarcode($barcode)
    {
        $barcode = (string) $barcode;

        return ((int) $barcode[-1]) === $this->calculateCheckDigit(substr($barcode, 0, -1));
    }
}
