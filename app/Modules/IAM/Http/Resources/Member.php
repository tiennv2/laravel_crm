<?php

namespace App\Modules\IAM\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Member extends Resource
{
    public function toArray($request)
    {
        return [
            'id'        => $this->id,
            'icheckId'  => $this->icheck_id,
            'name'      => $this->name,
            'roles'     => new RoleCollection($this->whenLoaded('roles')),
            'createdAt' => $this->created_at->format('c'),
            'updatedAt' => $this->updated_at->format('c'),
        ];
    }
}
