<?php

Route::group([
    'middleware' => 'web',
    'prefix' => 'iam',
    'namespace' => 'App\Modules\IAM\Http\Controllers'
], function() {
    Route::group([
        'prefix' => 'members',
    ], function () {
        Route::get('/', ['uses' => 'MemberController@index']);
        Route::post('/', ['uses' => 'MemberController@store']);
    });

    Route::group([
        'prefix' => 'roles',
    ], function () {
        Route::get('/', ['uses' => 'RoleController@index']);
    });
});
