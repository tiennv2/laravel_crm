<?php

namespace App\Modules\IAM\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\IAM\Http\Resources\MemberCollection;
use App\User;

class MemberController extends Controller
{
    public function index()
    {
        $data = User::with('roles')->get();

        return new MemberCollection($data);
    }

    public function store(Request $request)
    {
        $icheckIds = $request->input('icheckIds', []);

        foreach ($icheckIds as $id) {
            User::firstOrCreate(['icheck_id' => $id]);
        }

        return ['status' => true];
    }
}
