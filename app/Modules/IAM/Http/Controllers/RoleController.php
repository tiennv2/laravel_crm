<?php

namespace App\Modules\IAM\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use App\Modules\IAM\Http\Resources\RoleCollection;

class RoleController extends Controller
{
    public function index()
    {
        $data = Role::all();

        return new RoleCollection($data);
    }
}
