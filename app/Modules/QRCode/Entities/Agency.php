<?php

namespace App\Modules\QRCode\Entities;

use Illuminate\Database\Eloquent\Model;

class Agency extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'icheck_qrcode_agency';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'collaborator_phone', 'individual_phone', 'password', 'status',
    ];
}
