<?php

namespace App\Modules\QRCode\Entities;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'icheck_qrcode';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'account';
}
