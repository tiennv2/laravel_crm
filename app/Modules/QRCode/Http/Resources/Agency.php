<?php

namespace App\Modules\QRCode\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Agency extends Resource
{
    public function toArray($request)
    {
        return [
            'id'         => $this->id,
            'name'       => $this->name,
            'email'      => $this->email,
            'phone'      => $this->collaborator_phone,
            'loginPhone' => $this->individual_phone,
            'status'     => $this->status,
            'createdAt'  => $this->created_at ? $this->created_at->format('c') : null,
            'updatedAt'  => $this->updated_at ? $this->updated_at->format('c') : null,
        ];
    }
}
