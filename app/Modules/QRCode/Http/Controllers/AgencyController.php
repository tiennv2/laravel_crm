<?php

namespace App\Modules\QRCode\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\QRCode\Entities\{Agency};
use App\Modules\QRCode\Http\Resources\{Agency as AgencyResource, AgencyCollection};
use Illuminate\Support\Facades\Hash;

class AgencyController extends Controller
{
    public function query(Request $request)
    {
        $rowGroupCols = $request->input('rowGroupCols', []);
        $groupKeys    = $request->input('groupKeys', []);
        $valueCols    = $request->input('valueCols', []);
        $filterModel  = $request->input('filterModel', []);
        $sortModel    = $request->input('sortModel', []);
        $startRow     = $request->input('startRow', 0);
        $endRow       = $request->input('endRow', 100);

        $data = Agency::where('collaborator_id', null);

        if (count($rowGroupCols) > count($groupKeys)) {
            $select = [];
            $rowGroupCol = $rowGroupCols[count($groupKeys)];
            $select[] = $rowGroupCol['field'];

            foreach ($valueCols as $valueCol) {
                $select[] = $valueCol;
            }

            $data = $data->select($select);

            $colsToGroupBy = [];
            $colsToGroupBy[] = $rowGroupCol['field'];

            foreach ($colsToGroupBy as $col) {
                $data = $data->groupBy($col);
            }
        }

        $whereParts = [];

        if (count($groupKeys) > 0) {
            foreach ($groupKeys as $index => $key) {
                $whereParts[] = [$rowGroupCols[$index]['field'], '=', $key];
            }
        }

        // if ($filterModel) {
        //     foreach ($groupKeys as $key => $index) {
        //         $whereParts[] = [$rowGroupCols[$index]['field'], '=', $key];
        //     }
        // }

        foreach ($whereParts as $where) {
            $data = $data->where($where[0], $where[1], $where[2]);
        }

        if (count($sortModel) > 0) {
            foreach ($sortModel as $item) {
                $data = $data->orderBy($item['colId'], $item['sort']);
            }
        }

        $pageSize = $endRow - $startRow;
        $count = $data->count();
        $data = $data->skip($startRow)->take($pageSize)->get();

        return (new AgencyCollection($data))->additional([
            'meta' => [
                'count' => $count,
            ],
        ]);
    }

    public function store(Request $request)
    {
        $data = $request->all();

        return new AgencyResource(Agency::create([
            'name'               => $data['name'],
            'email'              => $data['email'],
            'collaborator_phone' => $data['phone'],
            'individual_phone'   => $data['loginPhone'],
            'password'           => Hash::make($data['password']),
        ]));
    }

    public function show(Agency $agency)
    {
        return new AgencyResource($agency);
    }

    public function update(Agency $agency, Request $request)
    {
        $data = $request->all();

        if (array_key_exists('password', $data)) {
            $data['password'] = Hash::make($data['password']);
        }

        if (array_key_exists('phone', $data)) {
            $data['collaborator_phone'] = $data['phone'];
        }

        if (array_key_exists('loginPhone', $data)) {
            $data['individual_phone'] = $data['loginPhone'];
        }

        $agency->update($data);

        return new AgencyResource($agency);
    }

    public function delete(Agency $agency)
    {
        $agency->delete();
    }
}
