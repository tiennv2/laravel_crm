<?php

Route::group([
    'middleware' => 'web',
    'prefix' => 'qrCode',
    'namespace' => 'App\Modules\QRCode\Http\Controllers'
], function() {
    Route::group([
        'prefix' => 'agencies',
    ], function () {
        Route::post('/', ['uses' => 'AgencyController@store']);
        Route::post('query', ['uses' => 'AgencyController@query']);
        Route::get('{agency}', ['uses' => 'AgencyController@show']);
        Route::put('{agency}', ['uses' => 'AgencyController@update']);
        Route::delete('{agency}', ['uses' => 'AgencyController@delete']);
    });

    Route::group([
        'prefix' => 'companies',
    ], function () {
        Route::post('query', ['uses' => 'CompanyController@query']);
    });
});
