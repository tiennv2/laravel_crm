<?php

namespace App\Exports\Collaborator;

use App\Models\Collaborator\Contributed_Information;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\FromView;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class ContributedInfomationExport implements FromView, WithTitle, ShouldAutoSize, WithColumnFormatting
{
    use Exportable;

    private $month;
    private $year;
    private $contributed_informations;
    private $profits_total;

    public function __construct($year, $month, $contributed_informations, $profits_total)
    {
        $this->month = $month;
        $this->year  = $year;
        $this->contributed_informations  = $contributed_informations;
        $this->profits_total  = $profits_total;
    }

//    public function query()
//    {
//        return Contributed_Information::query()
//            ->where('contributed_by', $this->contributed_by)
//            ->whereYear('approved_at', $this->year)
//            ->whereMonth('approved_at', $this->month);
//    }
//    /**
//     * @var Contributed_Information $contributed_information
//     */
//
//    public function map($contributed_information): array
//    {
//        return [
//            $contributed_information->gtin,
//            $contributed_information->profits,
//        ];
//    }
//    public function headings(): array
//    {
//        return [
//            'Mã GTIN',
//            'Lợi nhuận',
//        ];
//    }
//
//    /**
//     * @return string
//     */
    public function title(): string
    {
        return 'Tháng ' . $this->month.' - '.$this->year;
    }
//
    /**
     * @return array
     */
    public function columnFormats(): array
    {
        return [
            'A' => NumberFormat::FORMAT_TEXT,
        ];
    }

    public function view(): View
    {
        return view('collaborator.exports.monthly_report', [
            'contributed_informations' => $this->contributed_informations,
            'profits_total' => $this->profits_total,
        ]);
    }





}
