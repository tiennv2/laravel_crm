<?php

namespace App\Exports\Business;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Font;

class ProductExport implements FromCollection, WithTitle, ShouldAutoSize, WithHeadings, WithEvents
{
    use Exportable;


    private $products;

    public function __construct($products)
    {

        $this->products = $products;
    }

    public function headings(): array
    {

        return array_keys($this->products[0]);
    }
//
//    /**
//     * @return string
//     */
    public function title(): string
    {
        return 'DS_SanPham';
    }
//
    /**
     * @return array
     */
//    public function columnFormats(): array
//    {
//        return [
//            'A' => NumberFormat::FORMAT_TEXT,
//        ];
//    }

//    public function view(): View
//    {
//        return view('business.excelExport.subscriptionExcelExport', [
//            'subscriptions' => $this->subscriptions,
//        ]);
//    }


    /**
     * @return Collection
     */
    public function collection()
    {
        // TODO: Implement collection() method.
        return collect($this->products);
    }

    /**
     * @return array
     */
    public function registerEvents(): array
    {
        // TODO: Implement registerEvents() method.
        return [
            AfterSheet::class => function (AfterSheet $event) {
//                $event->sheet->getDelegate()->getStyle('A2:M10000')->applyFromArray([
//                    'font' => [
//                        'name' => 'Arial',
//                        'size' => 10,
//                    ]
//                ]);
                $event->sheet->getDelegate()->getStyle('A1:M1')->getAlignment()->setWrapText(true);
                $cellRange = 'A1:Z1'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->applyFromArray(
                    [
                        'font' => [
                            'name' => 'Arial',
                            'bold' => true,
                            'italic' => false,
                            'underline' => Font::UNDERLINE_SINGLE,
                            'strikethrough' => false,
                        ],
                        'quotePrefix' => true
                    ]
                );
            },
        ];
    }
}
