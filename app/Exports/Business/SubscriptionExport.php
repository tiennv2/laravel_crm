<?php

namespace App\Exports\Business;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Font;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class SubscriptionExport implements FromCollection, WithTitle, ShouldAutoSize, WithHeadings, WithEvents
{
    use Exportable;


    private $subscriptions;

    public function __construct($subscriptions)
    {

        $this->subscriptions = $subscriptions;
    }

    public function headings(): array
    {
//        return [
//            'ID',
//            'Tên Doanh nghiệp',
//            'Tên gói đăng ký',
//            'Số lượng sản phẩm',
//            'Đơn giá gói dịch vụ (VNĐ/năm)',
//            'Giá trị theo Hợp đồng (VNĐ)',
//            'Thời hạn gói',
//            'Ngày bắt đầu Hợp đồng',
//            'Ngày hết hạn',
//            'Loại ngày hết hạn',
//            'Trạng thái',
//            'Người phụ trách',
//            'Tình trạng thanh toán'
//        ];
        return array_keys($this->subscriptions[0]);
    }
//
//    /**
//     * @return string
//     */
    public function title(): string
    {
        return 'DS_GoiDichVu';
    }
//
    /**
     * @return array
     */
//    public function columnFormats(): array
//    {
//        return [
//            'A' => NumberFormat::FORMAT_TEXT,
//        ];
//    }

//    public function view(): View
//    {
//        return view('business.excelExport.subscriptionExcelExport', [
//            'subscriptions' => $this->subscriptions,
//        ]);
//    }


    /**
     * @return Collection
     */
    public function collection()
    {
        // TODO: Implement collection() method.
        return collect($this->subscriptions);
    }

    /**
     * @return array
     */
    public function registerEvents(): array
    {
        // TODO: Implement registerEvents() method.
        return [
            AfterSheet::class => function (AfterSheet $event) {
//                $event->sheet->getDelegate()->getStyle('A2:M10000')->applyFromArray([
//                    'font' => [
//                        'name' => 'Arial',
//                        'size' => 10,
//                    ]
//                ]);
                $cellRange = 'A1:M1'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->applyFromArray(
                    [
                        'font' => [
                            'name' => 'Arial',
                            'bold' => true,
                            'italic' => false,
                            'underline' => Font::UNDERLINE_SINGLE,
                            'strikethrough' => false,
                        ],
                        'quotePrefix' => true
                    ]
                );
            },
        ];
    }

//    public function view(): View
//    {
//        dd(collect($this->subscriptions));
//        return view('business.excelExport.subscriptionExcelExport', [
//            'subscriptions' => collect($this->subscriptions)
//        ]);
//    }
}
