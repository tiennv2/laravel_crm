<?php

namespace App\Models\Collaborator;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    const TYPE_APPROVE_CONTRIBUTED_INFORMATION = 1;
    const TYPE_WITHDRAW = 2;

    protected $connection = 'icheck_collaborator';
    protected $table = 'transactions';
    protected $fillable = ['collaborator_id', 'amount', 'ref_id', 'type', 'status', 'meta' ];
}
