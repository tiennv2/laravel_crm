<?php

namespace App\Models\Collaborator;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    const PENDING_STATUS = 0; //Đang lấy mã
    const GOT_GTINS_STATUS = 1; //Đã lấy mã xong
    const FAIL_GOT_GTINS_STATUS = 2; //Lấy mã không thành công
    const POURING_GTINS_STATUS = 3; //Đang đổ mã
    const POURED_GTINS_STATUS = 4; //Đã đổ mã
    const FAIL_POURED_GTINS_STATUS = 5; //Đổ mã thất bại

    public static $statusTexts = [
        self::PENDING_STATUS => 'Đang thực hiện lấy mã',
        self::GOT_GTINS_STATUS => 'Đã lấy mã xong',
        self::FAIL_GOT_GTINS_STATUS => 'Lấy mã không thành công',
        self::POURING_GTINS_STATUS => 'Đang đổ mã',
        self::POURED_GTINS_STATUS => 'Đã đổ mã xong',
        self::FAIL_POURED_GTINS_STATUS => 'Đỗ mã không thành công',
    ];

    protected $connection = 'icheck_collaborator';
    protected $table = 'jobs';
    protected $fillable = ['filters', 'gtins', 'need_update','need_search', 'group_ids', 'status', 'scheduled_at', 'search_cost'];
    protected $casts = [
        'filters' => 'array',
    ];
    public function setScheduledAtAttribute($value)
    {
        $this->attributes['scheduled_at'] =  date("Y-m-d H:i:s", strtotime($value));
    }
    public function getCreatedAtAttribute($value) {
        return \Carbon\Carbon::parse($value)->format('d/m/Y');
    }
    public function getScheduledAtAttribute($value) {
        return \Carbon\Carbon::parse($value)->format('d/m/Y');
    }
}
