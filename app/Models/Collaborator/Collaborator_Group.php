<?php

namespace App\Models\Collaborator;

use Illuminate\Database\Eloquent\Model;

class Collaborator_Group extends Model
{
    protected $connection = 'icheck_collaborator';
    protected $table = 'collaborator_group';
    protected $fillable = ['collaborator_id','group_id'];
    public $timestamps = false;
}
