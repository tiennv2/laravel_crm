<?php

namespace App\Models\Collaborator;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $connection = 'icheck_collaborator';
    protected $table = 'groups';
    protected $fillable = ['name','description'];
}
