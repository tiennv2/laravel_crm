<?php

namespace App\Models\Collaborator;

use Illuminate\Database\Eloquent\Model;

class Need_Update_Product extends Model
{
    const GLN_INFO = 1;
    const CATEGORIES_INFO = 2;
    const NAME = 4;
    const IMAGES = 8;
    const DESCRIPTIONS = 16;

    const ACTIVE = 1;
    const INACTIVE = 0;

    public static $infoTexts = [
        self::GLN_INFO => 'Nhà sản xuất',
        self::CATEGORIES_INFO => 'Danh mục',
        self::NAME => 'Tên sản phẩm',
        self::IMAGES => 'Ảnh sản phẩm',
        self::DESCRIPTIONS => 'Mô tả sản phẩm',
    ];

    protected $connection = 'icheck_collaborator';
    protected $table = 'product_need_update';
    protected $fillable = ['gtin','assigned_to', 'need_update','search_results','is_active', 'status'];
    public function getCreatedAtAttribute($value) {
        return \Carbon\Carbon::parse($value)->format('d/m/Y');
    }
//    public function getAssignedToAttribute($value) {
//        $collaborators = Collaborator::all();
//        foreach ($collaborators as $collaborator)
//        {
//            if($collaborator->id == $value)
//            {
//                return $collaborator->name;
//            }
//        }
////        return "";
//    }
}
