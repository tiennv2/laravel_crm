<?php

namespace App\Models\Collaborator;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product_Category extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $connection = 'icheck_collaborator';
    protected $table = 'product_categories';
    protected $fillable = ['name','slug','parent_id','image'];

}
