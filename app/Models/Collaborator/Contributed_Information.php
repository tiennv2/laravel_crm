<?php

namespace App\Models\Collaborator;

use Illuminate\Database\Eloquent\Model;

class Contributed_Information extends Model
{
    const STATUS_PENDING = 0;
    const STATUS_APPROVED = 1;
    const STATUS_DISAPPROVED = 2;

    protected $connection = 'icheck_collaborator';
    protected $table = 'contributed_informations';
    protected $fillable = ['status','approved_at'];
    public function getContributedAtAttribute($value) {
        return \Carbon\Carbon::parse($value)->format('d/m/Y');
    }
    public function getApprovedAtAttribute($value) {
        if($value){
            return \Carbon\Carbon::parse($value)->format('d/m/Y');
        }
        return '';
    }
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'contributed_data' => 'array',
    ];
}
