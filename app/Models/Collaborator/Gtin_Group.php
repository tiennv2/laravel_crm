<?php

namespace App\Models\Collaborator;

use Illuminate\Database\Eloquent\Model;

class Gtin_Group extends Model
{
    protected $connection = 'icheck_collaborator';
    protected $table = 'gtin_group';
    protected $fillable = ['gtin','group_id'];
    public $timestamps = false;
}
