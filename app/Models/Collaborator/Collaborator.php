<?php

namespace App\Models\Collaborator;

use Illuminate\Database\Eloquent\Model;

class Collaborator extends Model
{
    protected $connection = 'icheck_collaborator';
    protected $table = 'collaborators';
    protected $fillable = ['icheck_id','balance','email'];
    public function getCreatedAtAttribute($value) {
        return \Carbon\Carbon::parse($value)->format('d/m/Y');
    }

}
