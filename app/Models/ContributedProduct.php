<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Product\Product;

class ContributedProduct extends Model
{
    const STATUS_PENDING_REVIEW = 0;
    const STATUS_APPROVED       = 1;
    const STATUS_REJECTED       = 2;
    const STATUS_SYNCING        = 3;

    const ACTION_APPROVE_ALL = 1;
    const ACTION_REJECT_ALL  = 2;

    public static $statusTexts = [
        self::STATUS_PENDING_REVIEW => 'pendingReview',
        self::STATUS_APPROVED       => 'approved',
        self::STATUS_REJECTED       => 'rejected',
        self::STATUS_SYNCING        => 'syncing',
    ];

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'icheck_gateway';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'c_product';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'reviewed_at',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'attachments' => 'array',
        'auto_reject_passed' => 'boolean',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'status', 'auto_reject_passed',
    ];

    /**
     * Get the phone record associated with the user.
     */
    public function category1()
    {
        return $this->belongsTo(ContributedProductCategory::class, 'category_1', 'id');
    }

    /**
     * Get the phone record associated with the user.
     */
    public function category2()
    {
        return $this->belongsTo(ContributedProductCategory::class, 'category_2', 'id');
    }

    /**
     * Get the phone record associated with the user.
     */
    public function category3()
    {
        return $this->belongsTo(ContributedProductCategory::class, 'category_3', 'id');
    }

    /**
     * Get the phone record associated with the user.
     */
    public function category4()
    {
        return $this->belongsTo(ContributedProductCategory::class, 'category_4', 'id');
    }

    /**
     * Get the phone record associated with the user.
     */
    public function gln()
    {
        return $this->belongsTo(ContributedGLN::class, 'c_gln_id', 'id');
    }

    /**
     * Get the phone record associated with the user.
     */
    public function reward()
    {
        return $this->hasOne(ContributionReward::class, 'contribute_id', 'id');
    }

    /**
     * Get the phone record associated with the user.
     */
    public function product()
    {
        return $this->belongsTo(Product::class, 'gtin_code', 'gtin_code');
    }
}
