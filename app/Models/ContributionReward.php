<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContributionReward extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'icheck_gateway';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'c_money';
}
