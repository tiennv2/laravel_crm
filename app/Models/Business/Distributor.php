<?php

namespace App\Models\Business;

use Illuminate\Database\Eloquent\Model;


class Distributor extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'icheck_business';

    /**
     * The table associated with the model.
     *
     * @var string
     */

    protected $table = 'distributors';
    protected $fillable = ['icheck_ids','business_id','name','address', 'phone', 'email', 'website','status','files'];
}
