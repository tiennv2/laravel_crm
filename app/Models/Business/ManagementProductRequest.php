<?php

namespace App\Models\Business;

use Illuminate\Database\Eloquent\Model;

class ManagementProductRequest extends Model
{
    const STATUS_PENDING = 0;
    const STATUS_APPROVED = 1;
    const STATUS_NEED_UPDATE_PROOF = 2;
    const STATUS_DISAPPROVED = 3;
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'icheck_business';
    protected $table = 'management_product_requests';
    protected $fillable = ['data', 'status','note'];
}
