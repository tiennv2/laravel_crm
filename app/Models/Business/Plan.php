<?php

namespace App\Models\Business;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'icheck_business';
    protected $table = 'plans';
    protected $fillable = ['name', 'quota', 'price'];
}
