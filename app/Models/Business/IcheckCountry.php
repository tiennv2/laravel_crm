<?php

namespace App\Models\Business;

use Illuminate\Database\Eloquent\Model;


class IcheckCountry extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'icheck_product';

    /**
     * The table associated with the model.
     *
     * @var string
     */

    protected $table = 'p_country';

}
