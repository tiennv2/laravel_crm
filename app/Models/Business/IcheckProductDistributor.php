<?php

namespace App\Models\Business;

use Illuminate\Database\Eloquent\Model;


class IcheckProductDistributor extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'icheck_product';

    /**
     * The table associated with the model.
     *
     * @var string
     */

    protected $table = 'p_distributor_product';
    protected $fillable = ['distributor_id','product_id','is_monopoly','visible'];
    public $timestamps = false;

}
