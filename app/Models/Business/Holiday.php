<?php

namespace App\Models\Business;

use Illuminate\Database\Eloquent\Model;

class Holiday extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'icheck_business';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'holidays';
    protected $fillable = ['start_time', 'end_time'];

}
