<?php

namespace App\Models\Business;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Business extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'icheck_business';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    use SoftDeletes;
    protected $table = 'businesses';
    protected $dates = ['deleted_at'];
    protected $fillable = ['name', 'address', 'phone_number', 'email', 'website', 'fax', 'status','created_by','expo_account_id','tax_code', 'quota', 'product_count'];
    /**
     * Get the subscriptions for the business.
     */
    public function subscriptions()
    {
        return $this->hasMany('App\Models\Business\Subscription');
    }
}
