<?php

namespace App\Models\Business;

use Illuminate\Database\Eloquent\Model;

class Assigned_Roles extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'icheck_business_authorization';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'assigned_roles';
    protected $fillable = ['role_id','entity_id','entity_type','scope'];
    public $timestamps = false;
}
