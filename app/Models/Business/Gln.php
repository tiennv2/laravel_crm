<?php

namespace App\Models\Business;

use Illuminate\Database\Eloquent\Model;

class Gln extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    const STATUS_PENDING = 0;
    const STATUS_APPROVE = 1;
    const STATUS_DISAPPROVE = 2;
    protected $connection = 'icheck_business';
    protected $table = 'glns';
    protected $fillable = ['status','certificate_id','note','country','icheck_id',
        'business_id','business_code','name','gln','address','email','phone_number','fax','website'];
}
