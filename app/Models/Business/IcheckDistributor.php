<?php

namespace App\Models\Business;

use Illuminate\Database\Eloquent\Model;


class IcheckDistributor extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'icheck_product';

    /**
     * The table associated with the model.
     *
     * @var string
     */

    protected $table = 'p_distributor';
    const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';
    protected $fillable = ['name','address','contact','site','status','title_id','icheck_id'];
}
