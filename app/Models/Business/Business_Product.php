<?php

namespace App\Models\Business;

use Illuminate\Database\Eloquent\Model;

class Business_Product extends Model
{
    const INFO_STATUS_PENDING = 0;
    const INFO_STATUS_APPROVED = 1;
    const INFO_STATUS_DISAPPROVED = 2;

    const MANAGEMENT_STATUS_PENDING = 0;
    const MANAGEMENT_STATUS_APPROVED = 1;
    const MANAGEMENT_STATUS_DISAPPROVED = 2;
    const MANAGEMENT_STATUS_NEED_UPDATE_PROOF = 3;

    const EXPIRE_STATUS_FALSE = 0;
    const EXPIRE_STATUS_TRUE = 1;

    const MANAGEMENT_TYPE_FULL_CONTROL = 0;
    const MANAGEMENT_TYPE_DISTRIBUTOR = 2;

    public static $infoStatusTexts = [
        self::INFO_STATUS_PENDING => 'Chờ duyệt',
        self::INFO_STATUS_APPROVED => 'Đã duyệt',
        self::INFO_STATUS_DISAPPROVED => 'Hủy duyệt',
    ];

    public static $managementStatusTexts = [
        self::MANAGEMENT_STATUS_PENDING => 'Chờ duyệt',
        self::MANAGEMENT_STATUS_APPROVED => 'Đã duyệt',
        self::MANAGEMENT_STATUS_DISAPPROVED => 'Hủy duyệt',
        self::MANAGEMENT_STATUS_NEED_UPDATE_PROOF => 'Cần cung cấp giấy tờ',
    ];

    public static $expireStatusTexts = [
        self::EXPIRE_STATUS_FALSE => 'Còn hạn',
        self::EXPIRE_STATUS_TRUE => 'Hết hạn',
    ];

    public static $managementTypeTexts = [
        self::MANAGEMENT_TYPE_FULL_CONTROL => 'Toàn quyền',
        self::MANAGEMENT_TYPE_DISTRIBUTOR => 'Vai trò phân phối',
    ];
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'icheck_business';
    protected $table = 'business_products';
    protected $fillable = ['business_id','barcode','product_info','certificates','management_status','info_status','management_type','is_expired','note','management_approved_at','info_approved_at'];
}
