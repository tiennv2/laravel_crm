<?php

namespace App\Models\Business;

use Illuminate\Database\Eloquent\Model;


class ProductDistributor extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'icheck_business';

    /**
     * The table associated with the model.
     *
     * @var string
     */

    protected $table = 'product_distributors';
    protected $fillable = ['product_id','distributor_id','title_id', 'status','note','files','is_visible'];
}
