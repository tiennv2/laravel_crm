<?php

namespace App\Models\Business;

use Illuminate\Database\Eloquent\Model;

class Certificate extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'icheck_business';
    protected $table = 'certificates';
    protected $fillable = ['business_id', 'name', 'description', 'files','status'];
}
