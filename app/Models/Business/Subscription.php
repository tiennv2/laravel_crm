<?php

namespace App\Models\Business;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    const PAYMENT_STATUS_UNKNOWN = 0;
    const PAYMENT_STATUS_TRUE = 1;
    const PAYMENT_STATUS_FALSE = 2;
    const PAYMENT_STATUS_PARTIAL = 3;

    const EXPIRE_TYPE_DEMO = 0;
    const EXPIRE_TYPE_IN_CONTRACT = 1;

    const SUBSCRIPTION_STATUS_PENDING = 0;
    const SUBSCRIPTION_STATUS_APPROVED = 1;
    const SUBSCRIPTION_STATUS_DISAPPROVED = 2;

    public static $statusTexts = [
        self::SUBSCRIPTION_STATUS_PENDING => 'Chờ kích hoạt',
        self::SUBSCRIPTION_STATUS_APPROVED => 'Kích hoạt',
        self::SUBSCRIPTION_STATUS_DISAPPROVED => 'Hủy kích hoạt',
    ];
    public static $expireTypeTexts = [
        self::EXPIRE_TYPE_DEMO => 'Demo',
        self::EXPIRE_TYPE_IN_CONTRACT => 'Theo hợp đồng',
    ];
    public static $paymentStatusTexts = [
        self::PAYMENT_STATUS_UNKNOWN => 'Chưa rõ',
        self::PAYMENT_STATUS_TRUE => 'Đã thanh toán',
        self::PAYMENT_STATUS_FALSE => 'Chưa thanh toán',
        self::PAYMENT_STATUS_PARTIAL => 'Đã thanh toán một phần'
    ];
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'icheck_business';
    protected $table = 'subscriptions';
    protected $fillable = ['business_id', 'plan_id', 'days', 'expires_on', 'status',
        'payment_status','contract_files','note','contract_value','assigned_to','start_time',
        'expire_type', 'trial_expiration', 'contract_expiration', 'is_legacy_subscription'];

    /**
     * Get the plan for the subscription.
     */
    public function plan()
    {
        return $this->belongsto('App\Models\Business\Plan');
    }

}
