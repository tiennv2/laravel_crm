<?php

namespace App\Models\Business;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    const STATUS_INHERIT = 0;
    const STATUS_PENDING = 1;
    const STATUS_APPROVED = 2;
    const STATUS_DISAPPROVED = 3;
    const STATUS_EXPIRED = 4;

    public static $statusTexts = [
        self::STATUS_INHERIT => '',
        self::STATUS_PENDING => 'Chờ duyệt',
        self::STATUS_APPROVED => 'Đã duyệt',
        self::STATUS_DISAPPROVED => 'Hủy duyệt thông tin',
        self::STATUS_EXPIRED => 'Hết hạn',
    ];
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'icheck_business';
    protected $table = 'products';
    protected $fillable = ['name','barcode','icheck_id','attributes','images','price','status','gln_id','certificate_id','business_id','certificate_images','has_certificate_images','need_recheck','original_data'];
}
