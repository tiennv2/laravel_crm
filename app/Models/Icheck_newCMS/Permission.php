<?php

namespace App\Models\Icheck_newCMS;

use Spatie\Permission\Models\Permission as SpatiePermission;

class Permission extends SpatiePermission
{

    /**
     * The connection name for the model.
     *
     * @var string
     */
//    protected $connection = 'icheck-new-cms';

    protected $table = 'permissions';
    protected $fillable = ['id', 'guard_name', 'name','description'];
}
