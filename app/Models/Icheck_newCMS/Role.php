<?php

namespace App\Models\Icheck_newCMS;

use Spatie\Permission\Models\Role as SpatieRole;

class Role extends SpatieRole
{

    /**
     * The connection name for the model.
     *
     * @var string
     */
//    protected $connection = 'icheck-new-cms';

    protected $table = 'roles';
    protected $fillable = ['id', 'description', 'name','guard_name','created_by'];
}
