<?php

namespace App\Models\Icheck_newCMS;

use Illuminate\Database\Eloquent\Model;

class RoleHasPermissions extends Model
{

    /**
     * The connection name for the model.
     *
     * @var string
     */
//    protected $connection = 'icheck-new-cms';

    protected $table = 'role_has_permissions';

}
