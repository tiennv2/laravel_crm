<?php

namespace App\Models\Icheck_newCMS;

use Illuminate\Database\Eloquent\Model;

class ActivityLog extends Model
{

    /**
     * The connection name for the model.
     *
     * @var string
     */
//    protected $connection = 'icheck-new-cms';

    protected $table = 'activity_log';
    protected $fillable = ['id', 'description', 'subject_id','subject_type','causer_id', 'causer_type', 'meta_data'];
}
