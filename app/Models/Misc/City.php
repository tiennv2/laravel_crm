<?php

namespace App\Models\Misc;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'icheck_gateway';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'g_city';
}
