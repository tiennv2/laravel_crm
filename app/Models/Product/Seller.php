<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;
use App\Models\Misc\{District, City};

class Seller extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'icheck_gateway';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'b_shop_local';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_verify' => 'boolean',
    ];

    /**
     * Get the district record associated with the seller.
     */
    public function district()
    {
        return $this->belongsTo(District::class, 'district_id', 'id');
    }

    /**
     * Get the city record associated with the seller.
     */
    public function city()
    {
        return $this->belongsTo(City::class, 'city_id', 'id');
    }
}
