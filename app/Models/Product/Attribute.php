<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'icheck_product';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'p_product_attr';
}
