<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'icheck_product';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'p_product';

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'gtin_code';
    }

    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = 'createdAt';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'updatedAt';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'verify_owner' => 'boolean',
    ];

    /**
     * Get the vendor record associated with the product.
     */
    public function vendor()
    {
        return $this->hasOne(Vendor::class, 'id', 'vendor_id');
    }

    /**
     * The attributes that belong to the product.
     */
    public function attributes()
    {
        return $this->belongsToMany(Attribute::class, 'p_product_info', 'product_id', 'attribute_id')
            ->withPivot('content', 'short_content', 'content_text')
        ;
    }

    /**
     * The attributes that belong to the product.
     */
    public function getInformationAttribute()
    {
        $info = $this->attributes()->wherePivot('attribute_id', 1)->first();

        if ($info) {
            return $info->pivot->content;
        }
    }
}
