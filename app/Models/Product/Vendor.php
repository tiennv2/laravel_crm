<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'icheck_product';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'p_vendor';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'is_verify',
    ];

    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = 'createdAt';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'updatedAt';
}
