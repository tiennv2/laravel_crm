<?php

namespace App\Models\Qrcode;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $connection = 'icheck_qrcode';
    protected $table = 'g_city';

}
