<?php

namespace App\Models\Qrcode;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Account extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    use SoftDeletes;

    protected $connection = 'icheck_qrcode';
    protected $table = 'account';
    protected $dates = ['deleted_at'];

}
