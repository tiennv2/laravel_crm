<?php

namespace App\Models\Qrcode;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $connection = 'icheck_qrcode';
    protected $table = 'orders';
    protected $fillable = ['user_id','quantity','services','created_at','status','order_value','print_quantity','print_value','payment_status'];
    public $timestamps = false;
    const PAYMENT_STATUS_UNKNOWN = 0;
    const PAYMENT_STATUS_TRUE = 1;
    const PAYMENT_STATUS_FALSE = 2;

    const STATUS_PENDING = 0;
    const STATUS_APPROVED = 1;
    const STATUS_DISAPPROVED = 2;

    public static $paymentStatusTexts = [
        self::PAYMENT_STATUS_UNKNOWN => 'Chưa rõ',
        self::PAYMENT_STATUS_TRUE => 'Đã thanh toán',
        self::PAYMENT_STATUS_FALSE => 'Chưa thanh toán',
    ];
}
