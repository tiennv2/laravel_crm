<?php

namespace App\Models\Qrcode;

use Illuminate\Database\Eloquent\Model;

class Batch extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $connection = 'icheck_qrcode';
    protected $table = 'batch';
    protected $fillable = ['is_deleted','deleted_time'];
    public $timestamps = false;

}
