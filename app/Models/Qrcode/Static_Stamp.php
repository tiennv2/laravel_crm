<?php

namespace App\Models\Qrcode;

use Illuminate\Database\Eloquent\Model;

class Static_Stamp extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $connection = 'icheck_qrcode';
    protected $table = 'static_stamp';
    protected $fillable = ['status','expired_time','active_time', 'request_expired','show_product'
        ,'show_vendor','show_related','show_store','show_message','show_price','show_info',];
//    public $timestamps = false;

}
