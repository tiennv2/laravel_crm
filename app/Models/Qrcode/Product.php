<?php

namespace App\Models\Qrcode;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $connection = 'icheck_qrcode';
    protected $table = 'product';
    protected $fillable = ['name'];
    public $timestamps = false;

}
