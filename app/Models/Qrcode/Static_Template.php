<?php

namespace App\Models\Qrcode;

use Illuminate\Database\Eloquent\Model;

class Static_Template extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $connection = 'icheck_qrcode';
    protected $table = 'static_template';
    protected $fillable = [];
//    public $timestamps = false;

}
