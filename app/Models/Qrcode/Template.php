<?php

namespace App\Models\Qrcode;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Template extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    use SoftDeletes;

    protected $connection = 'icheck_qrcode_agency';
    protected $table = 'templates';
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'id','name','account_id','agency_id','attachments'
    ];
// Relations
    public function batches()
    {
        return $this->hasMany(TemplateBatch::class, 'template_id', 'id');
    }

}
