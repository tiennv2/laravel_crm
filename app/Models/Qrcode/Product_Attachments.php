<?php

namespace App\Models\Qrcode;

use Illuminate\Database\Eloquent\Model;

class Product_Attachments extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $connection = 'icheck_qrcode';
    protected $table = 'product_attachments';
    protected $fillable = ['product_id','path','created_at','updated_at'];
    public $timestamps = true;

}
