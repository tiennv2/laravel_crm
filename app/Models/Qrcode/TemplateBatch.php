<?php

namespace App\Models\Qrcode;

use Illuminate\Database\Eloquent\Model;

class TemplateBatch extends Model
{

    protected $connection = 'icheck_qrcode_agency';
    protected $table = 'template_batch';
    public $timestamps = false;
    protected $fillable = [
        'template_id','batch_id'
    ];
}
