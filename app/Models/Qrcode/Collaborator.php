<?php

namespace App\Models\Qrcode;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Collaborator extends Model
{

    use SoftDeletes;

    protected $connection = 'icheck_qrcode_agency';
    protected $table = 'users';
    protected $dates = ['deleted_at'];

}
