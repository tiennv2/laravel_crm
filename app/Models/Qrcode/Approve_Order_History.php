<?php

namespace App\Models\Qrcode;

use Illuminate\Database\Eloquent\Model;

class Approve_Order_History extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $connection = 'icheck_qrcode_agency';
    protected $table = 'approve_order_history';
    protected $fillable = [
        'order_id','collaborator_user_id','collaborator_id'
    ];
    public $timestamps = false;
}
