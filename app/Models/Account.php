<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class Account extends Authenticatable
{
    use Notifiable, HasRoles;

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'icheck_user';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'u_users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The column name of the "remember me" token.
     *
     * @var string
     */
    protected $rememberTokenName = '';
}
