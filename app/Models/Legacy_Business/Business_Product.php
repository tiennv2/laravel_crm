<?php

namespace App\Models\Legacy_Business;

use Illuminate\Database\Eloquent\Model;

class Business_Product extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'icheck-legacy-business';
    protected $table = 'business_product';
}
