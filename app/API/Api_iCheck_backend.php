<?php

namespace App\API;

use GuzzleHttp\Client as GuzzleClient;

class Api_iCheck_backend
{

    private function getClient($options = [])
    {
        $api = config('api.icheck_backend');
        $icheck_authorization = config('api.icheck-authorization');
        $defaultOptions = [
            'base_uri' => "$api",
            'headers' => [
                'Content-Type' => 'application/json',
                'icheck-authorization' => $icheck_authorization
            ],
            'timeout' => 10,
        ];

        $options = array_merge_recursive($defaultOptions, $options);
        $client = new GuzzleClient($options);
        return $client;
    }

    private function parseResponse($response)
    {
        $body = json_decode($response->getBody(), true);
        return $body;
    }

    //Product Management
    public function getProduct($gtin_code)
    {
        $response = $this->getClient()->get("products/$gtin_code/gtin");
        $product = $this->parseResponse($response);
        return $product ? $product : [];
    }

    public function createProduct($request)
    {
        $response = $this->getClient()->post('products', ['json' => $request]);
        $product = $this->parseResponse($response);
        return $product ? $product : [];

    }

    public function updateProduct($id, $request)
    {
        $response = $this->getClient()->put("products/$id", ['json' => $request]);
        $product = $this->parseResponse($response);
        return $product ? $product : [];

    }

    //Vendor
    public function getVendorByGln($options = [])
    {
        $response = $this->getClient()->get("vendors", [
            'query' => $options
        ]);
        $vendor = $this->parseResponse($response);
        return $vendor ? $vendor : [];
    }

    public function getVendorById($id)
    {
        $response = $this->getClient()->get("vendors/$id");
        $vendor = $this->parseResponse($response);
        return $vendor ? $vendor : [];
    }

    public function createVendor($request)
    {
        $response = $this->getClient()->post('vendors', ['json' => $request]);
        $vendor = $this->parseResponse($response);
        return $vendor ? $vendor : [];
    }

    public function updateVendor($id, $request)
    {
        $response = $this->getClient()->put("vendors/$id", ['json' => $request]);
        $vendor = $this->parseResponse($response);
        return $vendor ? $vendor : [];

    }

    //Attribute
    public function getAttributes()
    {
        $response = $this->getClient()->get("attributes");
        $attributes = $this->parseResponse($response);
        return $attributes ? $attributes : [];
    }

    public function createAttribute($request)
    {
        $response = $this->getClient()->post('attributes', ['json' => $request]);
        $attribute = $this->parseResponse($response);
        return $attribute ? $attribute : [];
    }

    public function productVerify($request)
    {
        $response = $this->getClient()->post('products/verify', ['json' => $request]);
        $result = $this->parseResponse($response);
        return $result ? $result : [];
    }

    //Update iCheck document (productCertificateImages)
    public function updateCertificateImages($request)
    {
        $response = $this->getClient()->post('documents/batch-update', ['json' => $request]);
        $result = $this->parseResponse($response);
        return $result ? $result : [];
    }

}
