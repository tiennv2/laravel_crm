<?php

namespace App\API;

use GuzzleHttp\Client as GuzzleClient;

class Api_collaborator_transfers
{

    private function getClient($options = [])
    {
        $api = config('api.collaborator_transfers');
        $transfers_authorization = config('api.transfers_authorization');
        $defaultOptions = [
            'base_uri' => "$api",
            'headers' => [
                'Content-Type' => 'application/json',
                'authorization' => $transfers_authorization
            ],
            'timeout' => 10,
        ];

        $options = array_merge_recursive($defaultOptions, $options);
        $client = new GuzzleClient($options);
        return $client;
    }

    private function parseResponse($response)
    {
        $body = json_decode($response->getBody(), true);
        return $body;
    }

    //

    public function transfer($request)
    {
        $response = $this->getClient()->post('api/transfers', ['json' => $request]);
        $response = $this->parseResponse($response);
        return $response ? $response : [];
    }

}