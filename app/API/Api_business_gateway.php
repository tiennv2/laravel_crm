<?php

namespace App\API;

use GuzzleHttp\Client as GuzzleClient;

class Api_business_gateway
{

    private function getClient($options = [])
    {
        $api = config('api.business_gateway');
//        $icheck_authorization = config('api.icheck-authorization');
        $defaultOptions = [
            'base_uri' => "$api",
            'headers' => [
                'Content-Type' => 'application/json',
//                'icheck-authorization' => $icheck_authorization
            ],
            'timeout' => 10,
        ];

        $options = array_merge_recursive($defaultOptions, $options);
        $client = new GuzzleClient($options);
        return $client;
    }

    private function parseResponse($response)
    {
        $body = json_decode($response->getBody(), true);
        return $body;
    }

    //Approve Role Of Product Management

    /**  Request Body
     * "businessId": number,
     * "gtin": "8938509096113",
     * "force": true /optional/
     * autoVerify: true /optional/
     * "status":number /optional/
     *  "syncRequestData"=true|false
     *  "notify"=true|false
     * "reason"=string
     */

    public function approveProductManagementRole($request)
    {
        $response = $this->getClient()->post('product/assignManagement', ['json' => $request]);
        $result = $this->parseResponse($response);
        return $result ? $result : [];
    }

//Disapprove Role Of Product Management

    /**  Request Body
     * "businessId": number,
     * "gtin": "8938509096113",
     *  "notify"=true|false
     * "reason"=string
     */
    public function disapproveProductManagementRole($request)
    {
        $response = $this->getClient()->post('product/retractManagement', ['json' => $request]);
        $result = $this->parseResponse($response);
        return $result ? $result : [];
    }


    //Approve Product Info

    /**  Request Body
     * "gtin": "8938509096113",
     * "businessId": 10,
     */
    public function approveProductInfo($request)
    {
        $response = $this->getClient()->post('product/pushToICheck', ['json' => $request]);
        $result = $this->parseResponse($response);
        return $result ? $result : [];
    }

    //Disapprove Product Info

    /**  Request Body
     * "gtin": "8938509096113",
     * "businessId": 10,
     * "reason": "Something"
     */
    public function disapproveProductInfo($request)
    {
        $response = $this->getClient()->post('product/disapproveInfo', ['json' => $request]);
        $result = $this->parseResponse($response);
        return $result ? $result : [];
    }

    /**
     * POST {business_domain}/internal/business/syncLegacyData
     * {
     * "businessId": {ID doanh nghiệp cần đồng bộ lại}
     * }
     */

    public function syncLegacyData($request)
    {
        $response = $this->getClient()->post('business/syncLegacyData', ['json' => $request]);
        $result = $this->parseResponse($response);
        return $result ? $result : [];
    }

    /**
     * POST {business_domain}/internal/subscription/active
     * {
     * "subscriptionId": {ID gói dịch vụ cần duyệt}
     * }
     */

    public function approve($request)
    {
        $response = $this->getClient()->post('subscription/activate', ['json' => $request]);
        $result = $this->parseResponse($response);
        return $result ? $result : [];
    }

    /**
     * POST {business_domain}/internal/subscription/deactivate
     * {
     * "subscriptionId": {ID gói dịch vụ cần duyệt}
     * "notify"=true|false
     * "reason"=string
     * }
     */

    public function disapprove($request)
    {
        $response = $this->getClient()->post('subscription/deactivate', ['json' => $request]);
        $result = $this->parseResponse($response);
        return $result ? $result : [];
    }

    public function getProductList($options = [])
    {
        $response = $this->getClient()->get('product/query', [
            'query' => $options
        ]);
        $result = $this->parseResponse($response);
        return $result ? $result : [];
    }

}
