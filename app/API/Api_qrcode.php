<?php

namespace App\API;

use GuzzleHttp\Client as GuzzleClient;

class Api_qrcode
{

    private function getClient($options = []) {
        $api = config('api.qrcode');
        $defaultOptions = [
            'base_uri' => "$api",
            'headers' => [
                'Content-Type'=>'application/json'
            ],
            'timeout' => 10,
        ];

        $options = array_merge_recursive($defaultOptions, $options);
        $client = new GuzzleClient($options);
        return $client;
    }

    private function parseResponse($response) {
        $body = json_decode($response->getBody(), true);
        /*if ($body['status']==403)
        {
            return $this->login('0936594446', '123123');
        }*/
        return $body['data'];
    }
    //Stamp_services Control
    public function getListStamp_services()
    {
        $response = $this->getClient()->get('stamp_services');
        $stamp_service = $this->parseResponse($response);
        return $stamp_service ? $stamp_service : [];
    }

    public function getStamp_service($id)
    {
        $response = $this->getClient()->get("stamp_services/$id");
        $stamp_service = $this->parseResponse($response);
        return $stamp_service? $stamp_service : [];
    }
    public function putStamp_service($id,$request)
    {
        $this->getClient()->put("stamp_services/$id",['json'=>$request]);

    }


    //Order Management
    public function getListOrders($options = [])
    {
        $response = $this->getClient()->get('backend/orders',[
            'query'=>$options
        ]);
        $orders = $this->parseResponse($response);
        return $orders ? $orders: [];
    }
    public function postOrder($request)
    {
        $this->getClient()->post('orders',['json'=>$request]);

    }
    public function updateOrder($id,$request)
    {
        $this->getClient()->put("orders/$id",['json'=>$request]);

    }
    public function deleteOrder($id)
    {
        $this->getClient()->delete("orders/$id");
    }
    //Company Management

    public function addCompany($request)
    {
        $this->getClient()->post('register',['json'=>$request]);

    }
    public function updateCompany($request,$id)
    {
        $this->getClient()->put("account/$id",['json'=>$request]);
    }
   /* public function approveCompany($id)
    {
        try {
            $this->getClient()->post("review-company/$id",[
                'json'=>["status"=> 1]
            ]);
        } catch (\Exception $e) {
            return $e->getTraceAsString();
        }
    }*/

    public function sendInbox($inbox)
    {
        $this->getClient()->post('inbox',['json' => $inbox]);
    }
    public function sendNotification($notification)
    {
        $this->getClient()->post('notification',['json' => $notification]);
    }
    public function getMessageListByUser($options = [])
    {
        $messages = $this->getClient()->get('admin-inbox',[
            'query'=>$options
        ]);
       /* $messages = $this->parseResponse($response);*/
        return $messages ? $messages: [];
    }

}