<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\ProductFileAgreement' => [
            'App\Listeners\CreateIcheckProduct',
        ],
        'App\Events\ProductInfoApprove' => [
            'App\Listeners\IcheckProductSync',
        ],
        'App\Events\DistributorApproveEvent' => [
            'App\Listeners\IcheckDistributorSync',
        ],
        'App\Events\DistributorDisapproveEvent' => [
            'App\Listeners\IcheckDistributorDisapproveSync',
        ],
        'App\Events\ProductSyncIcheckExpoEvent' => [
            'App\Listeners\ProductSyncIcheckExpo',
        ],
        'App\Events\ProductCertificateImagesSyncEvent' => [
            'App\Listeners\ProductCertificateImagesSync',
        ],
        'App\Events\ProductContributedInfoApprove' => [
            'App\Listeners\ProductContributedInfoSync',
        ],
        'App\Events\Business\ApproveManagementRoleOfProductsEvent' => [
            'App\Listeners\Business\ApproveManagementRoleOfProductsListener',
        ],
        'App\Events\Business\ApproveProductsInfoEvent' => [
            'App\Listeners\Business\ApproveProductsInfoListener',
        ],
        'App\Events\Business\DisapproveManagementRoleOfProductsEvent' => [
            'App\Listeners\Business\DisapproveManagementRoleOfProductsListener',
        ],
        'App\Events\Business\DisapproveProductsInfoEvent' => [
            'App\Listeners\Business\DisapproveProductsInfoListener',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
