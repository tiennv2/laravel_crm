<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Google_Client;
use Google_Service_AnalyticsReporting;

class GoogleAPIClientProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(Google_Client::class, function ($app) {
            $client = new Google_Client();
            $client->setAuthConfig($app['config']['google']['service_account_credentials']);

            return $client;
        });

        $this->app->bind(Google_Service_AnalyticsReporting::class, function ($app) {
            $client = $app[Google_Client::class];
            $client->addScope('https://www.googleapis.com/auth/analytics.readonly');

            return new Google_Service_AnalyticsReporting($client);
        });
    }

}
