<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Google\Cloud\Storage\StorageClient;
use App\Services\Flysystem\GoogleCloudStorageAdapter;
use League\Flysystem\Filesystem;

class GoogleCloudServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $factory = $this->app->make('filesystem');
        $factory->extend('gcs_business', function ($app, $config) {
            $storageClient = $app->make(StorageClient::class);
            $bucket = $storageClient->bucket($config['bucket']);

            $adapter = new GoogleCloudStorageAdapter($storageClient, $bucket);

            return new Filesystem($adapter);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(StorageClient::class, function ($app) {
            $config = $app['config']['google'];
            $storageClientConfig = [
                'keyFilePath' => $config['service_account_credentials'],
            ];
            return new StorageClient($storageClientConfig);
        });
    }

}
