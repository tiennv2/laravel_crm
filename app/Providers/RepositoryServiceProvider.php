<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app-> singleton(
            \App\Repository\Qrcode\ProductRepositoryInterface::class,
            \App\Repository\Qrcode\ProductRepository::class
        );
        $this->app-> singleton(
            \App\Repository\Business\ProductRepositoryInterface::class,
            \App\Repository\Business\ProductRepository::class
        );
        $this->app-> singleton(
            \App\Repository\Business\ProductRequestRepositoryInterface::class,
            \App\Repository\Business\ProductRequestRepository::class
        );
        $this->app-> singleton(
            \App\Repository\Business\UserRepositoryInterface::class,
            \App\Repository\Business\UserRepository::class
        );
        $this->app-> singleton(
            \App\Repository\Business\GlnRepositoryInterface::class,
            \App\Repository\Business\GlnRepository::class
        );
        $this->app-> singleton(
            \App\Repository\Collaborator\CollaboratorRepositoryInterface::class,
            \App\Repository\Collaborator\CollaboratorRepository::class
        );
        $this->app-> singleton(
            \App\Repository\Business\SubscriptionRepositoryInterface::class,
            \App\Repository\Business\SubscriptionRepository::class
        );
        $this->app-> singleton(
            \App\Repository\Business\BusinessRepositoryInterface::class,
            \App\Repository\Business\BusinessRepository::class
        );
        $this->app-> singleton(
            \App\Repository\Business\BusinessProductRepositoryInterface::class,
            \App\Repository\Business\BusinessProductRepository::class
        );
        $this->app-> singleton(
            \App\Repository\Qrcode\OrderRepositoryInterface::class,
            \App\Repository\Qrcode\OrderRepository::class
        );
    }
}
