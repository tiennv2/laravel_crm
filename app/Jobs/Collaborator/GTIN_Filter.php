<?php

namespace App\Jobs\Collaborator;

use App\API\Api_iCheck_backend;
use App\Models\Collaborator\Job;
use App\Models\NotFoundProduct;
use App\Models\Product\Product;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;

class GTIN_Filter implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $tries = 5;
    protected $filters;
    protected $job_id;
    protected $scheduled_at;
    private $api_icheck_backend;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($filters, $job_id, $scheduled_at)
    {
        $this->filters = $filters;
        $this->job_id = $job_id;
        $this->scheduled_at = $scheduled_at;
        $this->api_icheck_backend = new Api_iCheck_backend();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Product $product)
    {
        $filters = $this->filters;
        $gtins = [];
        $limit = 20000;
        if (date("Y-m-d H:i:s", strtotime($this->scheduled_at)) <= date("Y-m-d H:i:s", time())) {
            $limit = 5000;
        }

        if (array_key_exists("non_exist", $filters)) {
            $notFoundProducts = DB::connection('mongodb')->collection('p_hit')->where("status", "Assignable")->limit($limit)->get();
            foreach ($notFoundProducts->chunk(50) as $chunk) {
                foreach ($chunk as $notFoundProduct) {
                    $res = $this->api_icheck_backend->getProduct($notFoundProduct->barcode);
                    if ($res['status'] == 200) {
                        continue;
                    }
                    array_push($gtins, $notFoundProduct->barcode);
                    $notFoundProduct->status = "Disposed";
                    $notFoundProduct->save();
                }
            }
//
            $gtins = json_encode(array_unique($gtins));

        } else {
            $query_product = $product->newQuery();
            $query_product->leftJoin("p_vendor", "p_product.vendor_id", "=", "p_vendor.id")
//                ->leftJoin("p_product_info", "p_product.id", "=", "p_product_info.product_id")
//                ->leftJoin("p_category_product", "p_product.id", "=", "p_category_product.product_id")
                ->select("p_product.gtin_code")
                ->whereNull("gotten_to_contribution");
            if (array_key_exists("non_name", $filters)) {
                $query_product->where(function ($query) {
                    $query->where('product_name', '=', '')
                        ->orWhereNull('product_name');
                });
            } else {
                $query_product->where('product_name', '<>', '')->whereNotNull('product_name');
            }

            if (array_key_exists("non_images", $filters)) {
                $query_product->where(function ($query) {
                    $query->where('image_default', '=', '')
                        ->orWhereNull('image_default');
                });
            } else {
                $query_product->where('image_default', '<>', '')->whereNotNull('image_default');
            }

            if (array_key_exists("non_gln", $filters)) {
                $query_product->whereNull('vendor_id');
            } else {
                $query_product->whereNotNull('vendor_id');
            }

            if (array_key_exists("non_descriptions", $filters)) {
                $query_product->where(function ($query) {
                    $query->whereNotExists(function ($query) {
                        $query->select('id')
                            ->from('p_product_info')
                            ->whereRaw('product_id = p_product.id and attribute_id = 1');
                    });
                });
            } else {
                $query_product->where(function ($query) {
                    $query->whereExists(function ($query) {
                        $query->select('id')
                            ->from('p_product_info')
                            ->whereRaw('product_id = p_product.id and attribute_id = 1');
                    });
                });
            }

            if (array_key_exists("non_categories", $filters)) {
                $query_product->where('p_product.has_categories', '=', 0);
            } else {
                $query_product->where('p_product.has_categories', '=', 1);
            }

//            if (array_key_exists("categories", $filters)) {
//                $categories = $filters["categories"];
//                $query_product->where('p_category_product.category_id', $categories);
//            }

            if (array_key_exists("glns", $filters)) {
                $glns = $filters["glns"];
                $query_product->whereIn('p_vendor.gln_code', $glns);
            }


            $products = $query_product->orderBy("p_product.id", "desc")->limit($limit)->get();
            foreach ($products->chunk(50) as $chunk) {
                foreach ($chunk as $product) {
                    array_push($gtins, $product->gtin_code);
                }
            }

//            $query_products = array_filter($query_product->orderBy("p_product.id", "desc")->offset($offset)->limit(200)->pluck("gtin_code")->toArray());
//            if (count($query_products) == 0) {
//                break;
//            }
//            $product_gtins = array_unique($query_products);
//            $gtins = array_merge($gtins, $product_gtins);
//            $offset = $offset + 200;
//        }
            echo count(array_unique($gtins));

            $gtins = json_encode(array_unique($gtins));
        }

        Job::where("id", $this->job_id)->update(["gtins" => $gtins, "status" => Job::GOT_GTINS_STATUS]);
    }

    /**
     * Handle a job failure.
     *
     */
    public function failed($exception)
    {
        var_dump($exception->getMessage());
        Job::where("id", $this->job_id)->update(["status" => Job::FAIL_GOT_GTINS_STATUS]);
    }
}
