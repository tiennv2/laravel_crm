<?php

namespace App\Jobs\Collaborator;

use App\API\Api_iCheck_backend;
use App\Models\Business\Product;
use App\Models\Collaborator\Contributed_Information;
use App\Models\Collaborator\Gtin_Group;
use App\Models\Collaborator\Job;
use App\Models\Collaborator\Need_Update_Product;
use App\Models\Legacy_Business\Business_Product;
use App\Services\Business\BarcodeValidation;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;

class ProcessPourGtins implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $tries = 5;
    protected $i_job;
    private $barcodeValidation;
    private $api_icheck_backend;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($i_job)
    {
        $this->i_job = $i_job;
        $this->barcodeValidation = new BarcodeValidation();
        $this->api_icheck_backend = new Api_iCheck_backend();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $job = $this->i_job;
        if (array_key_exists("gtins", $job)) {
            $gtins = json_decode($job["gtins"], true);
            if (count($gtins) == 0) {
                Job::where('id', $job['id'])->update(["status" => Job::POURED_GTINS_STATUS, "search_cost" => 0]);
                return;
            }
            $group_ids = json_decode($job["group_ids"], true);
            $chunks = collect($gtins);
            $search_cost = 0;
            foreach ($chunks->chunk(20) as $chunk) {
                foreach ($chunk as $key => $gtin) {
                    //Note to p_product table gtin gotten
                    \App\Models\Product\Product::where("gtin_code", $gtin)->update(["gotten_to_contribution" => 1]);
                    if ($this->barcodeValidation->validateBarcode($gtin)) {
//                        DB::connection('icheck_collaborator')->beginTransaction();
                        //Check if need_update_product existed
                        $need_update_product = Need_Update_Product::where('gtin', $gtin)->first();
                        $contributed_product = Contributed_Information::where('gtin', $gtin)->first();
                        //Check if product exist in BusinessGateway
                        $productInBusinessGateway = Product::where('barcode', $gtin)->first();
                        //Check if product exist in Old BusinessGateway
                        $legacy_product = Business_Product::where('product_id', $gtin)->first();

                        if ($need_update_product || $contributed_product || $productInBusinessGateway || $legacy_product) {
//                            DB::connection('icheck_collaborator')->commit();
                            continue;
                        } else {
                            DB::connection('icheck_collaborator')->beginTransaction();
                            try {
                                if ($job["need_search"] == 1) {
                                    $search_result = bing_search($gtin);
                                    if (count(array_filter($search_result)) > 0) {
                                        Need_Update_Product::create(["gtin" => $gtin, "need_update" => $job["need_update"], "search_results" => json_encode($search_result), "is_active" => Need_Update_Product::ACTIVE]);
                                    } else {
                                        Need_Update_Product::create(["gtin" => $gtin, "need_update" => $job["need_update"], "is_active" => Need_Update_Product::INACTIVE]);
                                    }
                                    $search_cost += 3;
                                } else {
                                    Need_Update_Product::create(["gtin" => $gtin, "need_update" => $job["need_update"], "is_active" => Need_Update_Product::ACTIVE]);
                                }
                                //Add to gtin_group table
                                foreach ($group_ids as $group_id) {
                                    Gtin_Group::updateOrCreate(["gtin" => $gtin, "group_id" => $group_id]);
                                }
                                DB::connection('icheck_collaborator')->commit();
                            } catch (\Exception $e) {
                                DB::connection('icheck_collaborator')->rollBack();
                                echo $e->getMessage();
                            }
                        }
                    }
                }
            }

            Job::where('id', $job['id'])->update(["status" => Job::POURED_GTINS_STATUS, "search_cost" => $search_cost]);
        }
    }

    /**
     * Handle a job failure.
     *
     */
    public function failed($exception)
    {
        $job = $this->job;
        var_dump($exception->getMessage());
        Job::where('id', $job['id'])->update(["status" => Job::FAIL_POURED_GTINS_STATUS]);
    }
}
