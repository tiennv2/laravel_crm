<?php

namespace App\Console\Commands\Collaborator;

use App\API\Api_collaborator_transfers;
use App\Models\Collaborator\Collaborator;
use App\Models\Collaborator\Transaction;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CollaboratorMonthlyProfitsTransfer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'collaborator:monthlyProfitsTransfer';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Monthly Profits Transfer to Collaborators';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $currentMonth = date("m");
        $currentYear = date("Y");
        $previousMonth = 0;
        $previousYear = 0;
        if ($currentMonth == 1) {
            $previousMonth = 12;
            $previousYear = $currentYear - 1;
        } else {
            $previousMonth = $currentMonth - 1;
            $previousYear = $currentYear;
        }

        $transactions = Transaction::groupBy('collaborator_id')
            ->selectRaw('sum(amount) as sum, collaborator_id')
            ->whereMonth('created_at', $previousMonth)
            ->whereYear('created_at', $previousYear)
            ->where([['type', Transaction::TYPE_APPROVE_CONTRIBUTED_INFORMATION], ['status', 1]])
            ->pluck("sum", "collaborator_id")
            ->toArray();
        $transactions = array_unique($transactions);
        if ($transactions) {
            foreach ($transactions as $key => $value) {
                DB::connection('icheck_collaborator')->beginTransaction();
                try {
                    $collaborator = Collaborator::findOrFail($key);
                    if ($collaborator->balance >= $value) {
                        //Create record in Transaction table
                        Transaction::create([
                            'collaborator_id' => $collaborator->id,
                            'amount' => $value,
//                            'ref_id' => ,
                            'type' => Transaction::TYPE_WITHDRAW,
                            'status' => 1
                        ]);
                        //Create money transfer
                        $api_collaborator_transfer = new Api_collaborator_transfers();
                        $response = $api_collaborator_transfer->transfer([
                            "description" => "Thanh toan tien dong gop thong tin San pham iCheck trong thang $previousMonth nam $previousYear",
                            "amount" => $value * 100,
                            "recipient" => $collaborator->icheck_id,
                            "metadata" => []
                        ]);
                        if ($response['status'] != 200) {
                            dd($response);
                            DB::connection('icheck_collaborator')->rollBack();
                        }
                        $collaborator->decrement('balance', $value);
                    }

                    DB::connection('icheck_collaborator')->commit();
                    // endTransaction
                } catch (\Exception $e) {
                    DB::connection('icheck_collaborator')->rollBack();
                    echo $e->getMessage();
                }
            }
        }
    }
}
