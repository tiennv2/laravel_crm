<?php

namespace App\Console;

use App\API\Api_collaborator_transfers;
use App\Exports\Collaborator\ContributedInfomationExport;
use App\Http\Controllers\Collaborator\CollaboratorController;
use App\Jobs\Collaborator\ProcessPourGtins;
use App\Mail\Collaborator\MonthlyReviewMail;
use App\Models\Collaborator\Collaborator;
use App\Models\Collaborator\Contributed_Information;
use App\Models\Collaborator\Job;
use App\Models\Collaborator\Transaction;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
        'App\Console\Commands\Collaborator\CollaboratorMonthlyProfitsTransfer'
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            $jobs = array_filter(Job::select("id", "gtins", "need_update", "need_search", "group_ids")->where([["status", "=", Job::GOT_GTINS_STATUS], ["scheduled_at", "<", date("Y-m-d H:i:s", time())]])->get()->toArray());

            if (count($jobs) > 0) {
                foreach ($jobs as $job) {
                    ProcessPourGtins::dispatch($job);
                    Job::where('id', $job['id'])->update(["status" => Job::POURING_GTINS_STATUS]);
                }
            }
        })->everyMinute();

        $schedule->call(function () {
            $currentMonth = date("m");
            $currentYear = date("Y");
            $reviewMonth = 0;
            $reviewYear = 0;
            if ($currentMonth == 1) {
                $reviewMonth = 12;
                $reviewYear = $currentYear - 1;
            } else {
                $reviewMonth = $currentMonth - 1;
                $reviewYear = $currentYear;
            }

            $collaborator_ids = Contributed_Information::where('status', 1)->whereMonth('approved_at', $reviewMonth)->whereYear('approved_at', $reviewYear)->pluck('contributed_by')->toArray();
            $collaborator_ids = array_filter(array_unique($collaborator_ids));
            if (count($collaborator_ids) > 0) {
                foreach ($collaborator_ids as $collaborator_id) {
                    $contributed_informations = Contributed_Information::where([['status', 1], ['contributed_by', $collaborator_id]])->whereMonth('approved_at', $reviewMonth)->whereYear('approved_at', $reviewYear)->get();
                    $contributed_total = $contributed_informations->count();
                    $profits_total = 0;
                    foreach ($contributed_informations as $contributed_information) {
                        $profits_total += $contributed_information->profits;
                    }
//                    $filePath = sprintf('collaborators/%s/%s', $collaborator_id, "Transaction_Review_{$reviewMonth}_{$reviewYear}.xlsx");
//                    Excel::store(new ContributedInfomationExport($reviewYear, $reviewMonth, $contributed_informations, $profits_total), $filePath, 'gcs_business');
//                    $url = Storage::disk('gcs_business')->url($filePath);
                    //Send mail
                    $collaborator = CollaboratorController::getCollaboratorById($collaborator_id);
                    if ($collaborator && $collaborator->email) {
                        Mail::to($collaborator->email)->cc(env('ICHECK_ACCOUNTANT_EMAIL', ''))->queue(new MonthlyReviewMail($contributed_total, $profits_total, $reviewMonth, $reviewYear, $collaborator->name));
                    }
                }
            }

        })->monthlyOn(env('SEND_MAIL_TO_COLLABORATOR_DAY', ''), env('SEND_MAIL_TO_COLLABORATOR_TIME_IN_DAY', ''));

        //Monthly profit transfer
        $schedule->command('collaborator:monthlyProfitsTransfer')
            ->monthlyOn(env('SEND_PROFITS_TO_COLLABORATOR_DAY', '10'), env('SEND_PROFITS_TO_COLLABORATOR_TIME_IN_DAY', '07:00'));

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
