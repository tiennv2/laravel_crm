<?php
namespace App\Repository\Business;
interface ProductRepositoryInterface
{
    public function getList($request);
    public function excelExport($products);
    public function disapproveProductInfo($id);
}
