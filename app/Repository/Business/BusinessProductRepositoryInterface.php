<?php
namespace App\Repository\Business;
interface BusinessProductRepositoryInterface
{
    public function getList($request);
    public function excelExport($subscription);
    public function approveProductsInfo($ids, $status);
    public function disapproveProductsInfo($ids, $reason);
    public function approveManagementRoleOfProducts($ids, $verify);
    public function disapproveManagementRoleOfProducts($ids, $reason);
}
