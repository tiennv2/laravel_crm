<?php
namespace App\Repository\Business;
interface BusinessRepositoryInterface
{
    public function getList($request);
    public function excelExport($subscription);
    public function delete($id);
}
