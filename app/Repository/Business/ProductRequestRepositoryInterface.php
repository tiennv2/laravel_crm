<?php
namespace App\Repository\Business;
interface ProductRequestRepositoryInterface
{
    public function approve($id, $verify);
    public function disapprove($id, $note);
}
