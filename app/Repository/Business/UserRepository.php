<?php

namespace App\Repository\Business;

use App\Models\Business\Assigned_Roles;
use App\Models\Business\User;

class UserRepository implements UserRepositoryInterface
{

    public function getUsersByBusinessId($business_id){
        $entity_ids = Assigned_Roles::where("scope", $business_id)->pluck("entity_id")->toArray();
        $users = User::whereIn("id",$entity_ids)->get();
        return $users;
    }

    public function getBusinessIdsByIcheckId($icheck_id){
        $user = User::where("icheck_id",$icheck_id)->first();
        if($user){
            $business_ids = Assigned_Roles::where("entity_id", $user->id)->pluck("scope")->toArray();
            return $business_ids;
        }
        return [];
    }

}
