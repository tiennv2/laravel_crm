<?php

namespace App\Repository\Business;

use App\API\Api_business_gateway;
use App\API\Api_iCheck_backend;
use App\Events\ProductInfoApprove;
use App\Mail\BusinessGateway\GlnMail;
use App\Mail\BusinessGateway\ProductMail;
use App\Models\Business\Business;
use App\Models\Business\Gln;
use App\Models\Business\IcheckCountry;
use App\Models\Business\ManagementProductRequest;
use App\Models\Business\Product;
use App\Models\Business\User;
use App\Models\Icheck_newCMS\ActivityLog;
use Illuminate\Support\Facades\Mail;

class GlnRepository implements GlnRepositoryInterface
{
    private $api_icheck_backend;
    private $api_business_gateway;
    private $userRepository;
    private $productRequestRepository;

    public function __construct(UserRepositoryInterface $userRepository, ProductRequestRepositoryInterface $productRequestRepository)
    {
        $this->api_icheck_backend = new Api_iCheck_backend();
        $this->api_business_gateway = new Api_business_gateway();
        $this->userRepository = $userRepository;
        $this->productRequestRepository = $productRequestRepository;
    }

    public function updateIcheckVendor($id)
    {
        $gln = Gln::findOrFail($id);
        $country = IcheckCountry::where('alpha_2', $gln->country)->first();
        $gln_info = [];
        $gln_info['name'] = $gln->name;
        $gln_info['email'] = $gln->email;
        $gln_info['phone'] = $gln->phone_number;
        $gln_info['address'] = $gln->address;
        $gln_info['website'] = $gln->website;
        $gln_info['country_id'] = $country->id;
        $gln_info['gln_code'] = $gln->gln;
        $gln_info['prefix'] = $gln->business_code;
        $gln_info['internal_code'] = $gln->gln;
        $res = $this->api_icheck_backend->getVendorByGln(["where" => ["gln_code" => $gln->gln]]);

        if ($res['data']['items'] != []) {
            $icheck_vendor = $res['data']['items'][0];
            $icheck_id_gln = $icheck_vendor['id'];
            $result = $this->api_icheck_backend->updateVendor($icheck_vendor['id'], $gln_info);
            if ($result['status'] != 200) {
                return 'Lỗi cập nhật thông tin Doanh nghiệp sở hữu trên iCheck!';
            }
        } else {
            $result = $this->api_icheck_backend->createVendor($gln_info);
            if ($result['status'] != 200) {
                return 'Lỗi không tạo được thông tin Doanh nghiệp sở hữu trên iCheck!';
            }
            $icheck_id_gln = $result['data']['id'];
        }
        $gln->update(['icheck_id' => $icheck_id_gln]);
    }

    public function approve($id)
    {
        $gln = Gln::findOrFail($id);
        $country = IcheckCountry::where('alpha_2', $gln->country)->first();
        $gln_info = [];
        $gln_info['name'] = $gln->name;
        $gln_info['email'] = $gln->email;
        $gln_info['phone'] = $gln->phone_number;
        $gln_info['address'] = $gln->address;
        $gln_info['website'] = $gln->website;
        $gln_info['country_id'] = $country->id;
        $gln_info['gln_code'] = $gln->gln;
        $gln_info['internal_code'] = $gln->gln;
        $gln_info['is_verify'] = true;

        //
        $res = $this->api_icheck_backend->getVendorByGln(["where" => ["gln_code" => $gln->gln]]);

        if ($res['data']['items'] != []) {

            $icheck_vendor = $res['data']['items'][0];
            $icheck_id_gln = $icheck_vendor['id'];
            $result = $this->api_icheck_backend->updateVendor($icheck_vendor['id'], $gln_info);
            if ($result['status'] != 200) {
                return 'Lỗi cập nhật thông tin Doanh nghiệp sở hữu trên iCheck!';
            }
        } else {
            $result = $this->api_icheck_backend->createVendor($gln_info);
            if ($result['status'] != 200) {
                return 'Lỗi không tạo được thông tin Doanh nghiệp sở hữu trên iCheck!';
            }
            $icheck_id_gln = $result['data']['id'];
        }

        $gln->update(['status' => Gln::STATUS_APPROVE, 'icheck_id' => $icheck_id_gln]);
        //Disable and send mail to other businesses having the same GLN request
        $case_1 = Gln::where([['id', '<>', $id], ['gln', $gln->gln]])->pluck('business_id')->toArray();
        $case_2 = Gln::where([['id', '<>', $id], ['business_code', $gln->business_code]])->pluck('business_id')->toArray();
        $case_all = array_unique(array_merge($case_1, $case_2));

        if (count($case_all) > 0) {
            $note = "Đã có Doanh nghiệp khác quản lý mã GLN này!";
            foreach ($case_all as $key) {
                $business = Business::findOrFail($key);
                Gln::where([['id', '<>', $id], ['business_id', '=', $key], ['gln', $gln->gln]])
                    ->orWhere([['id', '<>', $id], ['business_id', '=', $key], ['business_code', $gln->business_code]])
                    ->update(['status' => Gln::STATUS_DISAPPROVE, 'note' => $note]);
                //Send emails
                $users = $this->userRepository->getUsersByBusinessId($business->id);
                $emails = array_filter($users->pluck("email")->toArray());
                if (count($emails) > 0) {
                    foreach ($emails as $email) {
                        Mail::to($email)->queue(new GlnMail($business, $gln, $note));
                    }
                }
            }
        }

        $productRequests = ManagementProductRequest::where([['business_id', '=', $gln->business_id]])
            ->whereNotIn('status', [1, 3])
            ->get();

        $products = Product::where([['business_id', '=', $gln->business_id], ['gln_id', '=', null]])
            ->get();

        foreach ($products as $product) {
            if ($gln->business_code and starts_with($product->barcode, $gln->business_code)) {
                $product->update(['gln_id' => $id]);
                $this->api_icheck_backend->updateProduct($product->icheck_id, ['vendor_id' => $icheck_id_gln]);
            }
        }

        foreach ($productRequests as $productRequest) {
//            $data = json_decode($productRequest->data, true);
            if ($gln->business_code and starts_with($productRequest->barcode, $gln->business_code)) {
                $this->productRequestRepository->approve($productRequest->id, $verify = 0);

//                $product_info = [];
//
//                if (array_key_exists("attributes", $data)) {
//                    $product_info['attributes'] = $data['attributes'];
//                    if ($product_info['attributes'] == '[]' || $product_info['attributes'] == null) {
//                        $product_info['attributes'] = null;
//                    } else {
//                        $product_info['attributes'] = json_encode($product_info['attributes']);
//                    }
//                } else {
//                    $product_info['attributes'] = null;
//                }
//
//
//                if (array_key_exists("certificate_id", $data)) {
//                    $product_info['certificate_id'] = $data['certificate_id'];
//                } else {
//                    $product_info['certificate_id'] = null;
//                }
//
//                if (array_key_exists("price", $data)) {
//                    $product_info['price'] = $data['price'];
//                } else {
//                    $product_info['price'] = null;
//                }
//
//                if (array_key_exists("name", $data)) {
//                    $product_info['name'] = $data['name'];
//                } else {
//                    $product_info['name'] = null;
//                }
//
//                if (array_key_exists("images", $data)) {
//                    $product_info['images'] = $data['images'];
//                    if ($product_info['images'] == '[]' || $product_info['images'] == null) {
//                        $product_info['images'] = null;
//                    } else {
//                        $product_info['images'] = json_encode($product_info['images']);
//                    }
//                } else {
//                    $product_info['images'] = null;
//                }
//
//                $product_info['barcode'] = $productRequest->barcode;
//                $product_info['business_id'] = $gln->business_id;
//                $product_info['gln_id'] = $id;
//                $product_info['status'] = 1;
//
//                $product = Product::where('barcode', $product_info['barcode'])->first();
//                $product->update($product_info);
//
//                //Update các bản ghi có barcode trùng barcode của Sản phẩm đã được duyệt
//                ManagementProductRequest::where([['barcode', '=', $product_info['barcode']], ['business_id', '=', $product_info['business_id']]])->update(['status' => 1]);
//                $ids = ManagementProductRequest::where([['barcode', '=', $product_info['barcode']], ['business_id', '<>', $product_info['business_id']]])->pluck('id');
//                if (count($ids) > 0) {
//                    foreach ($ids as $id) {
//                        $product_request = ManagementProductRequest::findOrFail($id);
//                        $business = Business::findOrFail($product_request->business_id);
//                        $product = [];
//                        $product['barcode'] = $product_request->barcode;
//                        if ($product_request->data) {
//                            $data = json_decode($product_request->data, true);
//                            if (array_key_exists("name", $data)) {
//                                $product['name'] = $data['name'];
//                            } else {
//                                $product['name'] = "";
//                            }
//                        } else {
//                            $product['name'] = "";
//                        }
//                        $note = "Sản phẩm đã có Doanh nghiệp khác quản lý!";
//
//                        $product_request->update(['status' => 3, 'note' => $note]);
//
//                        //Send emails
//                        $users = $this->userRepository->getUsersByBusinessId($business->id);
//                        $emails = array_filter($users->pluck("email")->toArray());
//                        if (count($emails) > 0) {
//                            foreach ($emails as $email) {
//                                Mail::to($email)->queue(new ProductMail($business, $product, $note));
//                            }
//                        }
//
////                        $emails = User::where([['business_id', '=', $business->id], ['deleted_at', '=', null]])->pluck('email')->toArray();
////                        if (count($emails) > 0) {
////                            foreach ($emails as $email) {
////                                Mail::to($email)->queue(new ProductMail($business, $product, $note));
////                            }
////                        }
//                    }
//                }
            }
        }
        //Activity_log
        ActivityLog::create([
            "description" => "Approve a gln",
            "subject_id" => $id,
            "subject_type" => "App\Models\Business\Gln",
            "causer_id" => auth()->user()->id,
            "causer_type" => "users"
        ]);
    }

    public function disapprove($id, $note)
    {
        $gln = Gln::findOrFail($id);
        $business = Business::findOrFail($gln->business_id);
        $gln->update(['status' => Gln::STATUS_DISAPPROVE, 'note' => $note]);

        //Send emails
        $users = $this->userRepository->getUsersByBusinessId($business->id);
        $emails = array_filter($users->pluck("email")->toArray());
        if (count($emails) > 0) {
            foreach ($emails as $email) {
                Mail::to($email)->queue(new GlnMail($business, $gln, $note));
            }
        }
        //Activity_log
        ActivityLog::create([
            "description" => "Disapprove a gln",
            "subject_id" => $id,
            "subject_type" => "App\Models\Business\Gln",
            "causer_id" => auth()->user()->id,
            "causer_type" => "users"
        ]);
    }

}
