<?php
namespace App\Repository\Business;
interface SubscriptionRepositoryInterface
{
    public function disapprove($id, $note);
    public function getList($request);
    public function excelExport($subscription);
}
