<?php

namespace App\Repository\Business;

use App\API\Api_business_gateway;
use App\API\Api_iCheck_backend;
use App\Events\Business\DisapproveManagementRoleOfProductsEvent;
use App\Mail\BusinessGateway\DisapproveMultiProductsInfoMail;
use App\Models\Business\Business;
use App\Models\Business\Business_Product;
use App\Models\Business\Gln;
use App\Models\Business\Product;
use App\Models\Icheck_newCMS\ActivityLog;
use Illuminate\Support\Facades\Mail;

class BusinessProductRepository implements BusinessProductRepositoryInterface
{
    private $userRepository;
    private $api_business_gateway;
    private $api_icheck_backend;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
        $this->api_business_gateway = new Api_business_gateway();
        $this->api_icheck_backend = new Api_iCheck_backend();
    }

    public function getList($request)
    {
        // TODO: Implement getList() method.
        $product = new Business_Product();
        $products = $product->newQuery();
        $query = [];
        $products->leftJoin('businesses', 'business_products.business_id', '=', 'businesses.id')
            ->select('business_products.*', 'businesses.name as business_name')
            ->where([['business_products.business_id', '<>', null]]);

//        if (auth()->user()->can('GATEWAY-viewLimitProduct') && auth()->user()->cannot('GATEWAY-viewProduct')) {
//            $product->where(function ($query) {
//                $query->whereExists(function ($query) {
//                    $query->select('business_id')
//                        ->from('user_has_businesses')
//                        ->where("user_has_businesses.user_id", auth()->user()->id)
//                        ->whereRaw('business_id = products.business_id');
//                });
//            });
//        }

        if ($request->input('name')) {
            $name = $request->input('name');
            $products->whereRaw('lower(data->"$.name") like lower(?)', ['%' . $name . '%']);
            $query['name'] = $name;
        }

        if ($request->input('business_id')) {
            $business_id = $request->input('business_id');
            $products->where('business_products.business_id', $business_id);
            $query['business_id'] = $business_id;
        }

        if ($request->input('barcode')) {
            $barcode = $request->input('barcode');
            $products->where('business_products.barcode', 'like', '%' . $barcode . '%');
            $query['barcode'] = $barcode;
        }

        if ($request->input('management_status') != null) {
            $management_status = $request->input('management_status');
            if ($management_status == Business_Product::MANAGEMENT_STATUS_PENDING) {
                $products->whereIn('business_products.management_status', [Business_Product::MANAGEMENT_STATUS_PENDING, Business_Product::MANAGEMENT_STATUS_NEED_UPDATE_PROOF]);
            } else {
                $products->where('business_products.management_status', $management_status);
            }
            $query['management_status'] = $management_status;
        }

        if ($request->input('info_status') != null) {
            $info_status = $request->input('info_status');
            $products->where([['business_products.info_status', $info_status], ['business_products.management_type', Business_Product::MANAGEMENT_TYPE_FULL_CONTROL], ['business_products.management_status', Business_Product::MANAGEMENT_STATUS_APPROVED]]);
            $query['info_status'] = $info_status;
        }
        //Have certificates
        if ($request->input('certificate_status') == 1) {
            $certificate_status = $request->input('certificate_status');
            $products->whereNotNull('business_products.certificates', '<>', null);
            $query['certificate_status'] = $certificate_status;
        }
        //Have not certificates
        if ($request->input('certificate_status') == 2) {
            $certificate_status = $request->input('certificate_status');
            $products->whereNull('business_products.certificates');
            $query['certificate_status'] = $certificate_status;
        }

        if ($request->input('management_type') != null) {
            $management_type = $request->input('management_type');
            $products->where('business_products.management_type', $management_type);
            $query['management_type'] = $management_type;
        }

        if ($request->input('need_recheck')) {
            $need_recheck = $request->input('need_recheck');
            $products->where('business_products.need_recheck', $need_recheck);
            $query['need_recheck'] = $need_recheck;
        } elseif ($request->input('need_recheck') === '0') {
            $need_recheck = $request->input('need_recheck');
            $products->where('business_products.need_recheck', '=', 0);
            $query['need_recheck'] = $need_recheck;
        }


        return [
            'products' => $products,
            'query' => $query
        ];
    }

    public function getStdProduct($barcode)
    {
        $stdProduct = Product::where('barcode', $barcode)->first();
        if ($stdProduct) {
            return $stdProduct;
        }
        return null;
    }

    public function excelExport($product)
    {
        $data = [];
        $product->orderBy('id', 'desc')->chunk(10, function ($products) use (&$data) {
            foreach ($products as $record) {
                //Get iCheck vendor for product
                $record->vendor_name = '';
                $stdProduct = $this->getStdProduct($record->barcode);
                if ($stdProduct && $stdProduct->gln_id) {
                    $gln = Gln::find($stdProduct->gln_id);
                    if ($gln) {
                        $record->vendor_name = $gln->name;
                    }
                }

                if ($record->vendor_name == '') {
                    $res = $this->api_icheck_backend->getProduct($record->barcode);
                    if ($res['status'] == 200 && array_key_exists("vendor", $res['data'])) {
                        if (array_key_exists("name", $res['data']['vendor'])) {
                            $record->vendor_name = $res['data']['vendor']['name'];
                        }
                    }
                }

                if ($record->created_at) {
                    $record->created_time = date("d/m/Y", strtotime($record->created_at));
                } else {
                    $record->created_time = '';
                }
                if ($record->updated_at) {
                    $record->updated_time = date("d/m/Y", strtotime($record->updated_at));
                } else {
                    $record->updated_time = '';
                }

                if ($record->certificates) {
                    $record->certificate_status = 'Có';
                } else {
                    $record->certificate_status = 'Chưa có';
                }
                if ($record->certificate_images) {
                    $record->certificate_images_status = 'Có';
                } else {
                    $record->certificate_images_status = 'Chưa có';
                }

                $record->info_status = Business_Product::$infoStatusTexts[$record->info_status];
                $record->management_status = Business_Product::$managementStatusTexts[$record->management_status];
                $record->management_type = Business_Product::$managementTypeTexts[$record->management_type];

                $productInfo = json_decode($record->product_info, true);
//Attributes
                $record->product_info = '';
                $record->certificate_info = '';
                $record->business_info = '';
                $record->trueFakeDistinguish = '';
                if ($record->attributes) {
                    $attributes = $productInfo['attributes'];
                    if (array_key_exists('1', $attributes)) {
                        /*                        $record->product_info = html_entity_decode(strip_tags(preg_replace('/(<br\s*\/?>\s*)+/', '<br>', nl2br($attributes[1]))));*/
                        $record->product_info = html_entity_decode(strip_tags($attributes[1]));
                    }
                    if (array_key_exists('2', $attributes)) {
                        $record->certificate_info = html_entity_decode(strip_tags($attributes[2]));
                    }
                    if (array_key_exists('3', $attributes)) {
                        $record->business_info = html_entity_decode(strip_tags($attributes[3]));
                    }
                    if (array_key_exists('4', $attributes)) {
                        $record->trueFakeDistinguish = html_entity_decode(strip_tags($attributes[4]));
                    }
                }
//                dd($record);
                array_push($data, [
                    'ID' => $record->id,
                    'Tên sản phẩm' => $productInfo['name'],
                    'Mã barcode' => $record->barcode,
                    'Giá' => $productInfo['price'],
                    'Thông tin Sản phẩm' => $record->product_info,
                    'Thông tin CCCN' => $record->certificate_info,
                    'Thông tin giới thiệu về doanh nghiệp sở hữu mã vạch' => $record->business_info,
                    'Phân biệt thật giả' => $record->trueFakeDistinguish,
                    'Doanh nghiệp quản lý' => $record->business_name,
                    'Doanh nghiệp sở hữu' => $record->vendor_name,
                    'Tình trạng giấy tờ' => $record->certificate_status,
                    'Tình trạng ảnh chứng chỉ Sản phẩm' => $record->certificate_images_status,
                    'Trạng thái thông tin' => $record->info_status,
                    'Trạng thái quản lý' => $record->management_status,
                    'Vai trò' => $record->management_type,
                    'Ngày tạo' => $record->created_time,
                    'Ngày cập nhật gần nhất' => $record->updated_time,
                ]);
            }
        });
        //
        return $data;
    }

    public function approveProductsInfo($product_ids, $status)
    {
        // TODO: Implement approveProductsInfo() method.
        $barcodes = [];
        foreach ($product_ids as $product_id) {
            $product = Business_Product::findOrFail($product_id);
            if ($product->management_type != Business_Product::MANAGEMENT_TYPE_FULL_CONTROL) {
                continue;
            }
            if ($product->need_recheck) {
                $product->need_recheck = 0;
                $product->original_data = null;
                $product->save();
            }
            array_push($barcodes, $product->barcode);
            //Activity_log
            ActivityLog::create([
                "description" => "Approve Product Info",
                "subject_id" => $product_id,
                "subject_type" => "App\Models\Business\Business_Product",
                "causer_id" => auth()->user()->id,
                "causer_type" => "users"
            ]);
        }
        $barcodes = array_unique($barcodes);
        if ($barcodes) {
            $data = [];
            $data['gtins'] = $barcodes;
            $data['autoVerify'] = true;
            $data['status'] = $status;
            $res = $this->api_business_gateway->approveProductInfo($data);
        }
    }

    public function disapproveProductsInfo($product_ids, $reason)
    {
        // TODO: Implement disapproveProductsInfo() method.
        if ($product_ids) {
            foreach ($product_ids as $product_id) {
                $business_product = Business_Product::findOrFail($product_id);
                if ($business_product->management_type != Business_Product::MANAGEMENT_TYPE_FULL_CONTROL) {
                    continue;
                }
                if ($business_product->autoApproved_data && $business_product->need_recheck) {
                    $business_product->update(['product_info' => $business_product->autoApproved_data, 'need_recheck' => 0, 'autoApproved_data' => null, 'note' => $reason]);
                } else {
                    $stdProduct = $this->getStdProduct($business_product->barcode);
                    if ($stdProduct) {
                        $stdProduct_info = json_encode(
                            [
                                'name' => $stdProduct->name,
                                'price' => $stdProduct->price,
                                'attributes' => $stdProduct->attributes,
                                'images' => $stdProduct->images,
                                'certificate_images' => $stdProduct->certificate_images
                            ]);
                        $business_product->update(['product_info' => $stdProduct_info, 'note' => $reason]);
                    }
                }
                $this->approveProductsInfo($product_id = [$business_product->id], Business_Product::INFO_STATUS_DISAPPROVED);
                //Activity_log
                ActivityLog::create([
                    "description" => "Disapprove Product Info",
                    "subject_id" => $product_id,
                    "subject_type" => "App\Models\Business\Business_Product",
                    "causer_id" => auth()->user()->id,
                    "causer_type" => "users"
                ]);
            }
            $business_ids = array_filter(array_unique(Business_Product::where("management_type", Business_Product::MANAGEMENT_TYPE_FULL_CONTROL)->whereIn("id", $product_ids)->pluck("business_id")->toArray()));
            if (count($business_ids) > 0) {
                foreach ($business_ids as $business_id) {
                    $business = Business::find($business_id);
                    if ($business) {
                        //Send emails
                        $products = Business_Product::where('business_id', $business_id)->whereIn('id', $product_ids)->pluck("product_info", "barcode")->toArray();
                        $mail_productData = [];
                        foreach ($products as $product) {
                            $product_info = json_decode($product['product_info'], true);
                            array_push($mail_productData, ["name" => $product_info['name'], "barcode" => $product['barcode']]);
                        }
                        $users = $this->userRepository->getUsersByBusinessId($business_id);
                        $emails = array_filter($users->pluck("email")->toArray());
                        if (count($emails) > 0) {
                            foreach ($emails as $email) {
                                Mail::to($email)->queue(new DisapproveMultiProductsInfoMail($business->name, $mail_productData, $reason));
                            }
                        }
                    } else {
                        continue;
                    }
                }
            }
        }
    }

    public function approveManagementRoleOfProducts($product_ids, $verify)
    {
        // TODO: Implement approveManagementRoleOfProduct() method.
        $business_ids = array_filter(array_unique(Business_Product::whereIn('id', $product_ids)->pluck('business_id')->toArray()));
        if ($business_ids) {
            foreach ($business_ids as $business_id) {
                $barcodes = Business_Product::where('business_id', $business_id)->whereIn('id', $product_ids)->pluck('barcode')->toArray();
                $data = [];
                $data['businessId'] = $business_id;
                $data['gtins'] = $barcodes;
//                $data['status'] = Business_Product::INFO_STATUS_PENDING;
                if ($verify == true) {
                    $data['autoVerify'] = true;
                }
                $data['syncRequestData'] = true;
                $data['notify'] = true;
                $data['reason'] = "Đã có Doanh nghiệp khác quản lý Sản phẩm này!";
                $res = $this->api_business_gateway->approveProductManagementRole($data);



            }
            foreach ($product_ids as $product_id) {
                //Activity_log
                ActivityLog::create([
                    "description" => "Approve Management Role Of Product",
                    "subject_id" => $product_id,
                    "subject_type" => "App\Models\Business\Business_Product",
                    "causer_id" => auth()->user()->id,
                    "causer_type" => "users"
                ]);
            }
        }
    }

    public function disapproveManagementRoleOfProducts($product_ids, $reason)
    {
        // TODO: Implement disapproveManagementRoleOfProducts() method.
        $business_ids = array_filter(array_unique(Business_Product::whereIn("id", $product_ids)->pluck("business_id")->toArray()));
        if (count($business_ids) > 0) {
            foreach ($business_ids as $business_id) {
                $gtins = Business_Product::where('business_id', $business_id)->whereIn("id", $product_ids)->pluck("barcode")->toArray();
                $data = [];
                $data['businessId'] = $business_id;
                $data['gtins'] = $gtins;
                $data['notify'] = true;
                $data['reason'] = $reason;
                $res = $this->api_business_gateway->disapproveProductManagementRole($data);
            }
            foreach ($product_ids as $product_id) {
                //Activity_log
                ActivityLog::create([
                    "description" => "Disapprove Management Role Of Product",
                    "subject_id" => $product_id,
                    "subject_type" => "App\Models\Business\Business_Product",
                    "causer_id" => auth()->user()->id,
                    "causer_type" => "users"
                ]);
            }
        }
    }

    public function getBusinessNameForProduct($product_id)
    {
        $business_name = '';
        $product = Business_Product::find($product_id);
        if ($product) {
            $business = Business::find($product->business_id);
            if ($business) {
                $business_name = $business->name;
            }
        }
        return $business_name;
    }

}
