<?php

namespace App\Repository\Business;

use App\API\Api_business_gateway;
use App\Models\Business\ManagementProductRequest;
use App\Models\Business\Product;

class ProductRequestRepository implements ProductRequestRepositoryInterface
{
//    private $api_icheck_backend;
//    private $userRepository;
    private $api_business_gateway;

    public function __construct()
    {
        $this->api_business_gateway = new Api_business_gateway();
//        $this->userRepository = $userRepository;
    }

    public function approve($id, $verify)
    {

//        $product_request = ManagementProductRequest::findOrFail($id);
//        $product_info = [];
//        $data = json_decode($product_request->data, true);
//        if (array_key_exists("attributes", $data)) {
//            $product_info['attributes'] = $data['attributes'];
//            if ($product_info['attributes'] == '[]' || $product_info['attributes'] == null) {
//                $product_info['attributes'] = null;
//            } else {
//                $product_info['attributes'] = json_encode($product_info['attributes']);
//            }
//        } else {
//            $product_info['attributes'] = null;
//        }
//
//        if (array_key_exists("certificate_id", $data)) {
//            $product_info['certificate_id'] = $data['certificate_id'];
//        } else {
//            $product_info['certificate_id'] = null;
//        }
//
//        if (array_key_exists("price", $data)) {
//            $product_info['price'] = $data['price'];
//        } else {
//            $product_info['price'] = null;
//        }
//
//        if (array_key_exists("name", $data)) {
//            $product_info['name'] = $data['name'];
//        } else {
//            $product_info['name'] = null;
//        }
//
////        if (array_key_exists("gln_id", $data)) {
////            $product_info['gln_id'] = $data['gln_id'];
////        } else {
////            $product_info['gln_id'] = null;
////        }
//
//        if (array_key_exists("images", $data)) {
//            $product_info['images'] = $data['images'];
//            if ($product_info['images'] == '[]' || $product_info['images'] == null) {
//                $product_info['images'] = null;
//            } else {
//                $product_info['images'] = json_encode($product_info['images']);
//            }
//        } else {
//            $product_info['images'] = null;
//        }
//
//        if (array_key_exists("certificate_images", $data)) {
//            $product_info['certificate_images'] = $data['certificate_images'];
//            if ($product_info['certificate_images'] == '[]' || $product_info['certificate_images'] == null) {
//                $product_info['certificate_images'] = null;
//                $product_info['has_certificate_images'] = 0;
//            } else {
//                $product_info['certificate_images'] = json_encode($product_info['certificate_images']);
//                $product_info['has_certificate_images'] = 1;
//            }
//        } else {
//            $product_info['certificate_images'] = null;
//            $product_info['has_certificate_images'] = 0;
//        }
//
//        $product_info['barcode'] = $product_request->barcode;
//        $product_info['business_id'] = $product_request->business_id;
//        $product_info['status'] = 1;
//
//        $product = Product::where('barcode', $product_info['barcode'])->first();
//        $product->update($product_info);
//        //Update product_count in businesses table
////        $count = Product::where("business_id", $product_request->business_id)->count();
////        Business::where("id", $product_request->business_id)->update(["product_count" => $count]);
//        //Update trạng thái các bản ghi có barcode trùng barcode của Sản phẩm đã được duyệt
//        ManagementProductRequest::where([['barcode', '=', $product_info['barcode']], ['business_id', '=', $product_info['business_id']]])->update(['status' => 1]);
//        $ids = ManagementProductRequest::where([['barcode', '=', $product_info['barcode']], ['business_id', '<>', $product_info['business_id']]])->pluck('id');
//        if (count($ids) > 0) {
//            foreach ($ids as $id) {
//                $product_request = ManagementProductRequest::findOrFail($id);
//                $business = Business::findOrFail($product_request->business_id);
//                $product = [];
//                $product['barcode'] = $product_request->barcode;
//                if ($product_request->data) {
//                    $data = json_decode($product_request->data, true);
//                    if (array_key_exists("name", $data)) {
//                        $product['name'] = $data['name'];
//                    } else {
//                        $product['name'] = "";
//                    }
//                } else {
//                    $product['name'] = "";
//                }
//                $note = "Sản phẩm đã có Doanh nghiệp khác quản lý!";
//
//                $product_request->update(['status' => 3, 'note' => $note]);
//
//                //Send emails
//                $users = $this->userRepository->getUsersByBusinessId($business->id);
//                $emails = array_filter($users->pluck("email")->toArray());
//
//                if (count($emails) > 0) {
//                    foreach ($emails as $email) {
//                        Mail::to($email)->queue(new ProductMail($business, $product, $note));
//                    }
//                }
//
//            }
//

        $requestProduct = ManagementProductRequest::find($id);
        if ($requestProduct) {
            $data = [];
            $data['businessId'] = $requestProduct->business_id;
            $data['gtins'] = [$requestProduct->barcode];
            $data['status'] = Product::STATUS_PENDING;
            if ($verify == 1) {
                $data['autoVerify'] = true;
            }
            $data['syncRequestData'] = true;
            $data['notify'] = true;
            $data['reason'] = "Đã có Doanh nghiệp khác quản lý Sản phẩm này!";
            $res = $this->api_business_gateway->approveProductManagementRole($data);
        }

    }

    public function disapprove($id, $note)
    {
        $requestProduct = ManagementProductRequest::find($id);
        if ($requestProduct) {
            $data = [];
            $data['businessId'] = $requestProduct->business_id;
            $data['gtins'] = [$requestProduct->barcode];
            $data['notify'] = true;
            if ($note) {
                $data['reason'] = $note;
            }
            $res = $this->api_business_gateway->disapproveProductManagementRole($data);
        }
    }

}
