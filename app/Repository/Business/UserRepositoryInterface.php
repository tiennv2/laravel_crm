<?php
namespace App\Repository\Business;
interface UserRepositoryInterface
{
    public function getUsersByBusinessId($business_id);
    public function getBusinessIdsByIcheckId($icheck_id);
}
