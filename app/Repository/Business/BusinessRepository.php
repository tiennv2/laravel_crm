<?php

namespace App\Repository\Business;

use App\API\Api_business_gateway;
use App\Models\Business\Business;
use App\Models\Business\Plan;
use App\Models\Business\Product;
use App\Models\Business\Subscription;
use App\Models\Icheck_newCMS\ActivityLog;
use App\User;

class BusinessRepository implements BusinessRepositoryInterface
{
//    private $api_business_gateway;
    private $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
//        $this->api_business_gateway = new Api_business_gateway();
        $this->userRepository = $userRepository;
    }

    public function getList($request)
    {
        $business = new Business();
        $business = $business->newQuery();
        if ($request->has('status') && $request->input('status') == 'deleted') {
            $business = $business->withTrashed()->newQuery();
        } else {
            $business = $business->newQuery();
        }

        $query = [];
        if (auth()->user()->can('GATEWAY-viewLimitBusiness') && auth()->user()->cannot('GATEWAY-viewBusiness')) {
            $business->select('businesses.*')->where('created_by', auth()->user()->icheck_id);
        } else {
            $business->select('businesses.*');
        }

        if ($request->input('business_id')) {
            $business_id = $request->input('business_id');
            $business->where('id', $business_id);
            $query['business_id'] = $business_id;
        }

        if ($request->input('name')) {
            $name = $request->input('name');
            $business->where('name', 'like', '%' . $name . '%');
            $query['name'] = $name;
        }

        if ($request->input('address')) {
            $address = $request->input('address');
            $business->where('address', 'like', '%' . $address . '%');
            $query['address'] = $address;
        }

        if ($request->input('tax_code')) {
            $tax_code = $request->input('tax_code');
            $business->where('tax_code', 'like', '%' . $tax_code . '%');
            $query['tax_code'] = $tax_code;
        }

        if ($request->input('phone_number')) {
            $phone = $request->input('phone_number');
            $business->where('phone_number', 'like', '%' . $phone . '%');
            $query['phone_number'] = $phone;
        }

        if ($request->input('email')) {
            $email = $request->input('email');
            $business->where('email', 'like', '%' . $email . '%');
            $query['email'] = $email;
        }
        if ($request->input('created_by')) {
            $icheck_id = $request->input('created_by');
            $business->where('created_by', '=', $icheck_id);
            $query['created_by'] = $icheck_id;
        }

        if ($request->input('assigned_to')) {
            $icheck_id = $request->input('assigned_to');
            $business_ids = $this->userRepository->getBusinessIdsByIcheckId($icheck_id);
            $business->whereIn('id', $business_ids);
            $query['assigned_to'] = $icheck_id;
        }

        if ($request->input('status')) {
            $status = $request->input('status');
            if ($status == 'deleted') {
                $business->where('deleted_at', '<>', null);
                $query['status'] = $status;
            } else {
                $business->where('status', $status);
                $query['status'] = $status;
            }
        }

        return [
            'business' => $business,
            'query' => $query
        ];

    }

    public function excelExport($business)
    {
        $data = [];
        $business->orderBy('id', 'desc')->chunk(100, function ($businesses) use (&$data) {
            foreach ($businesses as $record) {
                if ($record->created_at) {
                    $record->created_time = date("d/m/Y", strtotime($record->created_at));
                } else {
                    $record->created_time = '';
                }

                if ($record->created_by) {
                    $icheckUser = User::where('icheck_id', $record->created_by)->first();
                    if ($icheckUser) {
                        $record->created_by = $icheckUser->name;
                    }
                } else {
                    $record->created_by = '';
                }

                //Assignee
                $users = $this->userRepository->getUsersByBusinessId($record->id);
                $filtered_1 = $users->where('icheck_id', '<>', null)->first();
                if ($filtered_1) {
                    $record->assigned_to = $filtered_1->name;
                } else {
                    $record->assigned_to = '';
                }
                $filtered_2 = $users->where('icheck_id', null)->first();
                if ($filtered_2) {
                    $record->login_email = $filtered_2->email;
                } else {
                    $record->login_email = '';
                }

                //Product_managed_quantity
                $record->product_managed_quantity = Product::where('business_id', $record->id)->count();
                //Product_subscription_quantity
                $product_subscription_quantity = 0;
                $plan_ids = Subscription::where([['business_id', '=', $record->id], ['status', '=', 1], ['expires_on', '>', date('Y-m-d H:i:s')]])
                    ->get()->pluck('plan_id')->all();
                if ($plan_ids != []) {
                    foreach ($plan_ids as $plan_id) {
                        $plan = Plan::findOrFail($plan_id);
                        $product_subscription_quantity = $product_subscription_quantity + $plan->quota;
                    }
                }
                $record->product_subscription_quantity = $product_subscription_quantity;

                array_push($data, [
                    'ID' => $record->id,
                    'Tên Doanh nghiệp' => $record->name,
                    'Số lượng sản phẩm đang quản lý' => $record->product_managed_quantity,
                    'Số lượng sản phẩm tối ta được quản lý' => $record->product_subscription_quantity,
                    'Mã số thuế' => $record->tax_code,
                    'Địa chỉ' => $record->address,
                    'Email Doanh nghiệp' => $record->email,
                    'Email đăng nhập' => $record->login_email,
                    'Số điện thoại' => $record->phone_number,
                    'Website' => $record->website,
                    'Ngày tạo' => $record->created_time,
                    'Người tạo' => $record->created_by,
                    'Người hỗ trợ doanh nghiệp' => $record->assigned_to,
                ]);
            }
        });
        //
        return $data;
    }

    public function delete($id)
    {
        $business = Business::findOrFail($id);
        $business->delete();
        ActivityLog::create([
            "description" => "Delete a business",
            "subject_id" => $business->id,
            "subject_type" => "App\Models\Business\Business",
            "causer_id" => auth()->user()->id,
            "causer_type" => "users"
        ]);
    }

}
