<?php
namespace App\Repository\Business;
interface GlnRepositoryInterface
{
    public function approve($id);
    public function disapprove($id, $note);
    public function updateIcheckVendor($id);
}
