<?php

namespace App\Repository\Business;

use App\API\Api_business_gateway;
use App\Exports\Business\SubscriptionExport;
use App\Mail\BusinessGateway\SubscriptionMail;
use App\Models\Business\Business;
use App\Models\Business\ManagementProductRequest;
use App\Models\Business\Plan;
use App\Models\Business\Product;
use App\Models\Business\Subscription;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;

class SubscriptionRepository implements SubscriptionRepositoryInterface
{
    private $api_business_gateway;
    private $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->api_business_gateway = new Api_business_gateway();
        $this->userRepository = $userRepository;
    }

    public function getList($request)
    {
        $subscription = new Subscription();
        $subscription = $subscription->newQuery();
        $query = [];
        $subscription->leftJoin('businesses', 'subscriptions.business_id', '=', 'businesses.id')
            ->leftJoin('plans', 'subscriptions.plan_id', '=', 'plans.id')
            ->select('subscriptions.*', 'businesses.name as business_name', 'plans.name as plan_name', 'plans.quota as quota', 'plans.price as price');

        if ($request->input('business_id')) {
            $business_id = $request->input('business_id');
            $subscription->where('subscriptions.business_id', $business_id);
            $query['business_id'] = $business_id;
        }

        if ($request->input('plan_id')) {
            $plan_id = $request->input('plan_id');
            $subscription->where('subscriptions.plan_id', $plan_id);
            $query['plan_id'] = $plan_id;
        }
        if ($request->input('days')) {
            $days = $request->input('days');
            if($days >= 365){
                $years = number_format($days/365,0);
                $subscription->where('subscriptions.days','>', ($years - 0.5)*365)
                    ->where('subscriptions.days','<', ($years + 0.5)*365)
                ;
            } else {
                $subscription->where('subscriptions.days','=', $days);
            }

            $query['days'] = $days;
        }

        if ($request->input('status') != null) {
            $status = $request->input('status');
            $subscription->where('subscriptions.status', $status);
            $query['status'] = $status;
        }
        if ($request->input('payment_status') != null) {
            $payment_status = $request->input('payment_status');
            if ($payment_status == 0) {
                $subscription->where(
                    function ($query) {
                        $query->where('subscriptions.payment_status', 0)
                            ->orWhereNull('subscriptions.payment_status');
                    }
                );
            } else {
                $subscription->where('subscriptions.payment_status', $payment_status);
            }
            $query['payment_status'] = $payment_status;
        }

        if ($request->input('alert_status') == 1) {
            $over = date('Y-m-d H:i:s', strtotime("+ 3days"));
            $subscription->where([['subscriptions.expires_on', '<', $over], ['subscriptions.expires_on', '>', date('Y-m-d H:i:s')]]);
            $query['alert_status'] = 1;
        }

        if ($request->input('alert_status') == 2) {
            $over = date('Y-m-d H:i:s');
            $subscription->where('subscriptions.expires_on', '<', $over);
            $query['alert_status'] = 2;
        }

        return [
            'subscription' => $subscription,
            'query' => $query
        ];

    }

    public function disapprove($id, $note)
    {
        // TODO: Implement disapprove() method.
        $this->api_business_gateway->disapprove([
            "subscriptionId" => $id,
            "notify" => true,
            "reason" => $note
        ]);
//        $subscription = Subscription::findOrFail($id);
//        $data = [];
//        $data['status'] = 2;
//        $data['expires_on'] = null;
//        $data['note'] = $note;
//        $subscription->update($data);
//
//        //Dispprove Product Management Roles
//        $product_subscription_quantity = 0;
//        $product_managed_quantity = Product::where([['business_id', $subscription->business_id],['status','<>', Product::STATUS_EXPIRED]])->count();
//        $plan_ids = array_filter(
//            Subscription::where([['business_id', '=', $subscription->business_id], ['status', '=', 1], ['expires_on', '>', date('Y-m-d H:i:s')]])
//                ->get()
//                ->pluck('plan_id')
//                ->all()
//        );
//        if ($plan_ids != []) {
//            foreach ($plan_ids as $plan_id) {
//                $plan = Plan::findOrFail($plan_id);
//                $product_subscription_quantity = $product_subscription_quantity + $plan->quota;
//            }
//        }
//
//        if ($product_managed_quantity - $product_subscription_quantity > 0) {
//            $remove_number = $product_managed_quantity - $product_subscription_quantity;
//            $product_barcodes = Product::where([['business_id', $subscription->business_id],['status','<>', Product::STATUS_EXPIRED]])->pluck('barcode')->random($remove_number)->all();
//            $prepare_data = [];
//            $prepare_data['businessId'] = $subscription->business_id;
//            $prepare_data['gtins'] = $product_barcodes;
//            $prepare_data['notify'] = true;
//            $prepare_data['reason'] = "Một gói dịch vụ của Doanh nghiệp vừa bị hủy kích hoạt!";
//            $this->api_business_gateway->disapproveProductManagementRole($prepare_data);
//        }
//
//        //Disapprove Product Management Request
//        ManagementProductRequest::where('business_id', $subscription->business_id)
//            ->whereIn('status', [ManagementProductRequest::STATUS_PENDING, ManagementProductRequest::STATUS_NEED_UPDATE_PROOF])
//            ->update(['status' => ManagementProductRequest::STATUS_DISAPPROVED, 'note' => 'Do Doanh nghiệp bị hủy kích hoạt hoặc hết hạn gói dịch vụ!']);
//
//        //Send mail
//        $plan = Plan::findOrFail($subscription->plan_id);
//        $business = Business::findOrFail($subscription->business_id);
//        $content = [];
//        $content['plan_name'] = $plan->name;
//        $content['created_at'] = date("d/m/Y", strtotime($subscription->created_at));
//        $users = $this->userRepository->getUsersByBusinessId($business->id);
//        $emails = array_filter($users->pluck("email")->toArray());
//        if (count($emails) > 0) {
//            foreach ($emails as $email) {
//                Mail::to($email)->queue(new SubscriptionMail($business, $content, $note));
//            }
//        }
    }

    public function excelExport($subscription)
    {
        $data = [];
        $subscription->orderBy('id', 'desc')->chunk(50, function ($subscriptions) use (&$data) {
            foreach ($subscriptions as $record) {
                if (!$record->contract_value) {
                    $record->contract_value = '';
                }
                if ($record->days >= 365) {
                    $record->expire = number_format($record->days / 365, 0, ".", ",") . ' năm';
                } else {
                    $record->expire = number_format($record->days, 0, ".", ",") . ' ngày';
                }
                if ($record->start_time) {
                    $record->start_time = date("d/m/Y", strtotime($record->start_time));
                } else {
                    $record->start_time = '';
                }
                if ($record->expires_on) {
                    $record->expires_on = date("d/m/Y", strtotime($record->expires_on));
                } else {
                    $record->expires_on = '';
                }

                if (is_null($record->expire_type) || $record->expire_type == Subscription::EXPIRE_TYPE_IN_CONTRACT) {
                    $record->expire_type = Subscription::$expireTypeTexts[Subscription::EXPIRE_TYPE_IN_CONTRACT];
                } else {
                    $record->expire_type = Subscription::$expireTypeTexts[Subscription::EXPIRE_TYPE_DEMO];
                }

                $record->status = Subscription::$statusTexts[$record->status];

                if ($record->assigned_to) {
                    $icheckUser = User::where('icheck_id', $record->assigned_to)->first();
                    if ($icheckUser) {
                        $record->assigned_to = $icheckUser->name;
                    }
                } else {
                    $record->assigned_to = '';
                }

                if (is_null($record->payment_status)) {
                    $record->payment_status = Subscription::$paymentStatusTexts[Subscription::PAYMENT_STATUS_UNKNOWN];
                } else {
                    $record->payment_status = Subscription::$paymentStatusTexts[$record->payment_status];
                }

                array_push($data, [
                    'ID' => $record->id,
                    'Tên Doanh nghiệp' => $record->business_name,
                    'Tên gói đăng ký' => $record->plan_name,
                    'Số lượng sản phẩm' => $record->quota,
                    'Đơn giá gói dịch vụ (VNĐ/năm)' => $record->price,
                    'Giá trị theo Hợp đồng (VNĐ)' => $record->contract_value,
                    'Thời hạn gói' => $record->expire,
                    'Ngày bắt đầu Hợp đồng' => $record->start_time,
                    'Ngày hết hạn' => $record->expires_on,
                    'Loại ngày hết hạn' => $record->expire_type,
                    'Trạng thái' => $record->status,
                    'Ghi chú' => $record->note,
                    'Người phụ trách' => $record->assigned_to,
                    'Tình trạng thanh toán' => $record->payment_status
                ]);
            }
        });
        //
        return $data;
    }

}
