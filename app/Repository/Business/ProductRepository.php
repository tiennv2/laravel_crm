<?php

namespace App\Repository\Business;

use App\API\Api_iCheck_backend;
use App\Events\ProductInfoApprove;
use App\Models\Business\Business;
use App\Models\Business\Gln;
use App\Models\Business\ManagementProductRequest;
use App\Models\Business\Product;
use App\Models\Qrcode\Product_Info;

class ProductRepository implements ProductRepositoryInterface
{
    private $api_icheck_backend;

    public function __construct()
    {
        $this->api_icheck_backend = new Api_iCheck_backend();
    }

    public function getGlnByProduct($gln_id)
    {
        $gln = Gln::find($gln_id);
        if ($gln) {
            $business = Business::find($gln->business_id);
            $gln->business_name = $business->name;
            return $gln;
        }
        return false;
    }

    public function getProductInfo($product_id)
    {
        $product_info = Product_Info::where('product_id', $product_id)->get();
        return $product_info;
    }


    public function disapproveProductManagementRole($id, $note)
    {
        $product = Product::findOrFail($id);
        $data = [];
        $data['name'] = $product->name;
        $data['price'] = $product->price;
        $data['certificate_id'] = $product->certificate_id;
        $data['certificate_files'] = json_decode($product->certificate_files, true);
        $data['images'] = json_decode($product->images, true);
        $data['certificate_images'] = json_decode($product->certificate_images, true);
        $data['attributes'] = json_decode($product->attributes, true);
        $data = json_encode($data);
        $response = $this->api_icheck_backend->productVerify(['ids' => [$product->barcode], 'expired_at' => date('c', strtotime("-1 days"))]);
        if ($response['status'] == 200) {
            ManagementProductRequest::where([['business_id', '=', $product->business_id], ['barcode', '=', $product->barcode]])
                ->update(['status' => 3, 'data' => $data, 'note' => $note]);
            $product->update(['status' => 3, 'business_id' => null]);
        }
        return $product->business_id;
    }

    public function getList($request)
    {
        // TODO: Implement getList() method.
        $product = new Product();
        $product = $product->newQuery();
        $query = [];
        $product->leftJoin('businesses', 'products.business_id', '=', 'businesses.id')
            ->leftJoin('glns', 'products.gln_id', '=', 'glns.id')
            ->select('products.*', 'businesses.name as business_name', 'glns.gln as gln')
            ->where([['products.business_id', '<>', null]]);

        if (auth()->user()->can('GATEWAY-viewLimitProduct') && auth()->user()->cannot('GATEWAY-viewProduct')) {
            $product->where(function ($query) {
                $query->whereExists(function ($query) {
                    $query->select('business_id')
                        ->from('user_has_businesses')
                        ->where("user_has_businesses.user_id", auth()->user()->id)
                        ->whereRaw('business_id = products.business_id');
                });
            });
        }

        if ($request->input('name')) {
            $name = $request->input('name');
            $product->where('products.name', 'like', '%' . $name . '%');
            $query['name'] = $name;
        }

        if ($request->input('gln_id')) {
            $gln_id = $request->input('gln_id');
            $product->where('products.gln_id', '=', $gln_id);
            $query['gln_id'] = $gln_id;
        }

        if ($request->input('business_id')) {
            $business_id = $request->input('business_id');
            $product->where('products.business_id', $business_id);
            $query['business_id'] = $business_id;
        }

        if ($request->input('barcode')) {
            $barcode = $request->input('barcode');
            $product->where('products.barcode', 'like', '%' . $barcode . '%');
            $query['barcode'] = $barcode;
        }

        if ($request->input('status') != null) {
            $status = $request->input('status');
            $product->where('products.status', $status);
            $query['status'] = $status;
        }

        if ($request->input('certificate_status') == 1) {
            $certificate_status = $request->input('certificate_status');
            $product->where('products.certificate_id', null);
            $query['certificate_status'] = $certificate_status;
        }

        if ($request->input('certificate_status') == 2) {
            $certificate_status = $request->input('certificate_status');
            $product->where('products.certificate_id', '<>', null);
            $query['certificate_status'] = $certificate_status;
        }
        if ($request->input('has_certificate_images')) {
            $has_certificate_images = $request->input('has_certificate_images');
            $product->where('products.has_certificate_images', $has_certificate_images);
            $query['has_certificate_images'] = $has_certificate_images;
        }
        if ($request->input('need_recheck')) {
            $need_recheck = $request->input('need_recheck');
            $product->where('products.need_recheck', $need_recheck);
            $query['need_recheck'] = $need_recheck;
        } elseif ($request->input('need_recheck') === '0') {
            $need_recheck = $request->input('need_recheck');
            $product->where('products.need_recheck', '=', 0);
            $query['need_recheck'] = $need_recheck;
        }

        return [
            'product' => $product,
            'query' => $query
        ];
    }

    public function excelExport($product)
    {
        // TODO: Implement excelExport() method.
        $data = [];
        $product->orderBy('id', 'desc')->chunk(10, function ($products) use (&$data) {
            foreach ($products as $record) {
                //Get iCheck vendor for product
                $record->vendor_name = '';
                if ($record->gln_id) {
                    $gln = Gln::find($record->gln_id);
                    if ($gln) {
                        $record->vendor_name = $gln->name;
                    }
                }

                if ($record->vendor_name == '') {
                    $res = $this->api_icheck_backend->getProduct($record->barcode);
                    if ($res['status'] == 200 && array_key_exists("vendor", $res['data'])) {
                        if (array_key_exists("name", $res['data']['vendor'])) {
                            $record->vendor_name = $res['data']['vendor']['name'];
                        }
                    }
                }

                if ($record->created_at) {
                    $record->created_time = date("d/m/Y", strtotime($record->created_at));
                } else {
                    $record->created_time = '';
                }
                if ($record->updated_at) {
                    $record->updated_time = date("d/m/Y", strtotime($record->updated_at));
                } else {
                    $record->updated_time = '';
                }

                if ($record->certificate_id) {
                    $record->certificate_status = 'Có';
                } else {
                    $record->certificate_status = 'Chưa có';
                }
                if ($record->has_certificate_images) {
                    $record->certificate_images_status = 'Có';
                } else {
                    $record->certificate_images_status = 'Chưa có';
                }

                $record->status = Product::$statusTexts[$record->status];
//Attributes
                $record->product_info = '';
                $record->certificate_info = '';
                $record->business_info = '';
                $record->trueFakeDistinguish = '';
                if ($record->attributes) {
                    $attributes = json_decode($record->attributes, true);
                    if (array_key_exists('1', $attributes)) {
                        /*                        $record->product_info = html_entity_decode(strip_tags(preg_replace('/(<br\s*\/?>\s*)+/', '<br>', nl2br($attributes[1]))));*/
                        $record->product_info = html_entity_decode(strip_tags($attributes[1]));
                    }
                    if (array_key_exists('2', $attributes)) {
                        $record->certificate_info = html_entity_decode(strip_tags($attributes[2]));
                    }
                    if (array_key_exists('3', $attributes)) {
                        $record->business_info = html_entity_decode(strip_tags($attributes[3]));
                    }
                    if (array_key_exists('4', $attributes)) {
                        $record->trueFakeDistinguish = html_entity_decode(strip_tags($attributes[4]));
                    }
                }
//                dd($record);
                array_push($data, [
                    'ID' => $record->id,
                    'Tên sản phẩm' => $record->name,
                    'Mã barcode' => $record->barcode,
                    'Giá' => $record->price,
                    'Thông tin Sản phẩm' => $record->product_info,
                    'Thông tin CCCN' => $record->certificate_info,
                    'Thông tin Công ty' => $record->business_info,
                    'Phân biệt thật giả' => $record->trueFakeDistinguish,
                    'Doanh nghiệp quản lý' => $record->business_name,
                    'Doanh nghiệp sở hữu' => $record->vendor_name,
                    'Tình trạng giấy tờ' => $record->certificate_status,
                    'Tình trạng ảnh chứng chỉ Sản phẩm' => $record->certificate_images_status,
                    'Trạng thái' => $record->status,
                    'Ngày tạo' => $record->created_time,
                    'Ngày cập nhật gần nhất' => $record->updated_time,
                ]);
            }
        });
        //
        return $data;
    }

    public function disapproveProductInfo($id)
    {
        $product = Product::findOrFail($id);
//        dd($product);
        if ($product->original_data) {
            $original_data = [];
            $data = json_decode($product->original_data, true);
            if (array_key_exists("certificate_id", $data) and $data['certificate_id']) {
                $original_data['certificate_id'] = $data['certificate_id'];
            } else {
                $original_data['certificate_id'] = null;
            }
            if (array_key_exists("name", $data) and $data['name']) {
                $original_data['name'] = $data['name'];
            } else {
                $original_data['name'] = null;
            }

            if (array_key_exists("price", $data) and $data['price']) {
                $original_data['price'] = $data['price'];
            } else {
                $original_data['price'] = null;
            }
            if (array_key_exists("images", $data) and $data['images']) {
                $original_data['images'] = json_encode($data['images']);
            } else {
                $original_data['images'] = null;
            }

            if (array_key_exists("certificate_images", $data) and $data['certificate_images']) {
                $original_data['certificate_images'] = json_encode($data['certificate_images']);
            } else {
                $original_data['certificate_images'] = null;
            }

            if (array_key_exists("attributes", $data) && $data['attributes']) {
                $original_data['attributes'] = json_encode($data['attributes']);
            } else {
                $original_data['attributes'] = null;
            }
            $product->update($original_data);
//            dd($product);

            if ($product->need_recheck) {
                $product->need_recheck = 0;
                $product->original_data = null;
                $product->save();
            }

            event(new ProductInfoApprove($id, Product::STATUS_DISAPPROVED));
        }

    }
}
