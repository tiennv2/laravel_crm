<?php

namespace App\Repository\Collaborator;

use App\Models\Collaborator\Collaborator;
use App\User;

class CollaboratorRepository implements CollaboratorRepositoryInterface
{
    public function getAll()
    {
        // TODO: Implement getAll() method.
        $collaborators = Collaborator::all();
        $iCheckUsers = User::all();
        foreach ($collaborators as $collaborator) {
            foreach ($iCheckUsers as $user) {
                if ($collaborator->icheck_id == $user->icheck_id) {
                    $collaborator->name = $user->name;
                }
            }
        }
        return $collaborators;
    }

    public function getIcheckId($id){
        $collaborator = Collaborator::findOrFail($id);
        return $collaborator->icheck_id;
    }

    public function getCollaboratorName($collaborator_id){
        $collaborator = Collaborator::find($collaborator_id);
        if($collaborator){
            $iCheckUsers = User::all();
            $collaborator_name = $collaborator->icheck_id;
            foreach ($iCheckUsers as $user) {
                if ($collaborator->icheck_id == $user->icheck_id) {
                    $collaborator_name = $user->name;
                }
            }
            return $collaborator_name;
        }
        return '';
    }
}
