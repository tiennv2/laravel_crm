<?php
namespace App\Repository\Collaborator;
interface CollaboratorRepositoryInterface
{
    public function getAll();
    public function getIcheckId($id);
    public function getCollaboratorName($collaborator_id);
}
