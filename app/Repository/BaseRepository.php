<?php

use App\Models\Base;

class BaseRepository
{
    public function getBlankModel()
    {
        return new Base();
    }

    public function getModelClassName()
    {
        $model = $this->getBlankModel();

        return get_class($model);
    }
    public function get($order = 'id', $direction = 'asc')
    {
        $model = $this->getModelClassName();

        return $model::orderBy($order, $direction)->paginate(10);
    }

}