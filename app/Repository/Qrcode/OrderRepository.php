<?php

namespace App\Repository\Qrcode;

use App\API\Api_qrcode;
use App\Models\Icheck_newCMS\ActivityLog;
use App\Models\Qrcode\Order;

class OrderRepository implements OrderRepositoryInterface
{
    private $api_qrcode;

    public function __construct()
    {
        $this->api_qrcode = new Api_qrcode();

    }
    public function approve($id)
    {
        $order = Order::findOrFail($id);
        $order->update(['status' => Order::STATUS_APPROVED]);
        ActivityLog::create([
            "description" => "Approve an QRCODE-ORDER",
            "subject_id" => $id,
            "subject_type" => "App\Models\Qrcode\Order",
            "causer_id" => auth()->user()->id,
            "causer_type" => "users"
        ]);
        //Send Notification to Customer
        $notification = [];
        $notification['to'] = $order->user_id;
        $notification['message'] = "Đơn hàng " . $id . " đã được duyệt, hãy bắt đầu tạo lô tem và sử dụng.";
        $notification['notify_type'] = "APPROVED_ORDER";
        $notification['target_id'] = $id;
        $this->api_qrcode->sendNotification($notification);
    }
}
