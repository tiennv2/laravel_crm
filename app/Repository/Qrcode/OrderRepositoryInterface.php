<?php
namespace App\Repository\Qrcode;
interface OrderRepositoryInterface
{
    public function approve($id);
}
