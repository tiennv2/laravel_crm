<?php

namespace App\Repository\Qrcode;

use App\Models\Qrcode\City;
use App\Models\Qrcode\Country;
use App\Models\Qrcode\District;
use App\Models\Qrcode\Product;
use App\Models\Qrcode\Product_Info;
use App\Models\Qrcode\Vendor;

class ProductRepository implements ProductRepositoryInterface
{
    public function getProduct($product_id)
    {
        $product = Product::findOrFail($product_id);
        return $product;

    }

    public function getVendorByProduct($vendor_id)
    {
        $vendor = Vendor::find($vendor_id);
        if ($vendor) {
            $city = City::find($vendor->city_id);
            $district = District::find($vendor->district_id);
            $country = Country::find($vendor->country_id);
            if ($city) {
                $vendor->city_name = $city->name;
            }else{
                $vendor->city_name = '';
            }
            if ($district) {
                $vendor->district_name = $district->name;
            }else{
                $vendor->district_name = '';
            }
            if ($country) {
                $vendor->country_name = $country->name;
            }else{
                $vendor->country_name = '';
            }
        }
        return $vendor;
    }

    public function getProductInfo($product_id)
    {
        $product_info = Product_Info::where('product_id', $product_id)->get();
        return $product_info;
    }
}