<?php

use Illuminate\Support\Facades\DB;

if (!function_exists('expo_sync_product')) {
    function expo_sync_product($gtin, $image)
    {
        $product = DB::connection('icheck_business')
            ->table('products')
            ->whereNotNull('business_id')
            ->where('barcode', $gtin)
            ->first();

        if (!$product) {
            return;
        }

        $attributes = json_decode($product->attributes, true);

        $business = DB::connection('icheck_business')
            ->table('businesses')
            ->where('id', $product->business_id)
            ->first();

        if (!$business or !$business->phone_number) {
            return;
        }

        $expoAccount = DB::connection('icheck_expo')
            ->table('accounts')
            ->where('phone', $business->phone_number)
            ->first();

        if (!$expoAccount) {
            $expoAccountId = DB::connection('icheck_expo')->table('accounts')->insertGetId([
                'name' => $business->name,
                'phone' => $business->phone_number,
                'username' => $business->phone_number,
                'password' => '$2y$10$jKPhwD3odW/td/cYNyE57ubbetOGpdLyPAz3Ttu.cnh4sRjV6Ag.i',
                'email' => $business->email,
                'address' => $business->address,
                'image' => 'blank/blank.jpg',
                'type' => 5,
                'shop_id' => 1,
                'is_shop' => 1,
                'gln_code' => $business->phone_number,
                'shop_name' => $business->name,
            ]);
            $expoConfigBoothId = DB::connection('icheck_expo')->table('config_booths')->insertGetId([
                'shop_id' => 1,
                'account_id' => $expoAccountId,
                'hotline' => $business->phone_number,
                'name' => $business->name,
                'address' => $business->address,
            ]);
            $expoAccount = DB::connection('icheck_expo')
                ->table('accounts')
                ->where('id', $expoAccountId)
                ->first();
        } else {
            DB::connection('icheck_expo')->table('accounts')->where('id', $expoAccount->id)->update([
                'name' => $business->name,
                'phone' => $business->phone_number,
                'email' => $business->email,
                'address' => $business->address,
                'image' => 'blank/blank.jpg',
                'type' => 5,
                'shop_id' => 1,
                'is_shop' => 1,
                'gln_code' => $business->phone_number,
                'shop_name' => $business->name,
            ]);
            DB::connection('icheck_expo')->table('config_booths')->where('account_id', $expoAccount->id)->update([
                'shop_id' => 1,
                'account_id' => $expoAccount->id,
                'hotline' => $business->phone_number,
                'name' => $business->name,
                'address' => $business->address,
            ]);
        }

        $expoConfigBooth = DB::connection('icheck_expo')
            ->table('config_booths')
            ->where('account_id', $expoAccount->id)
            ->first();

        $newData = [
            'name' => $product->name,
            'slug' => str_slug($product->name),
            'description' => @$attributes[1],
            'code' => $gtin,
            'barcode' => $gtin,
            'price' => $product->price,
            'image' => 'https://static.icheck.com.vn/' . $image . '_original.jpg',
            'dm_cap_0_shop' => 0,
            'dm_cap_1_shop' => 0,
            'dm_cap_2_shop' => 0,
            'dm_cap_3_shop' => 0,
            'provider_id' => $expoAccount->id,
            'account_id' => $expoAccount->id,
            'shop_id' => 1,
            'status' => 2,
        ];

        $productCategory = DB::connection('icheck_collaborator')
            ->table('product_need_update')
            ->join('contributed_informations', 'product_need_update.gtin', '=', 'contributed_informations.gtin')
            ->where('product_need_update.gtin', $gtin)
            ->first();

        if ($productCategory) {
            $data = json_decode($productCategory->data);
            $categories = $data->affiliateCategories;

            if (isset($categories[0])) {
                $newData['dm_cap_0_shop'] = $categories[0];
            }

            if (isset($categories[1])) {
                $newData['dm_cap_1_shop'] = $categories[1];
            }

            if (isset($categories[2])) {
                $newData['dm_cap_2_shop'] = $categories[2];
            }

            if (isset($categories[3])) {
                $newData['dm_cap_3_shop'] = $categories[3];
            }
        }

        $expoProduct = DB::connection('icheck_expo')
            ->table('products')
            ->where('barcode', $gtin)
            ->first();

        if (!$expoProduct) {
            $expoProductId = DB::connection('icheck_expo')
                ->table('products')
                ->insertGetId($newData);
            $expoProduct = DB::connection('icheck_expo')
                ->table('products')
                ->where('id', $expoProductId)
                ->first();
        } else {
            DB::connection('icheck_expo')->table('products')->where('barcode', $gtin)->update($newData);
            $expoProduct = DB::connection('icheck_expo')
                ->table('products')
                ->where('barcode', $gtin)
                ->first();
        }


        $stores = DB::connection('icheck_gateway')
            ->table('b_local_product')
            ->join('b_shop_local', 'b_local_product.local_id', '=', 'b_shop_local.id')
            ->join('g_city', 'b_shop_local.city_id', '=', 'g_city.id')
            ->join('g_district', 'b_shop_local.district_id', '=', 'g_district.id')
            ->select("b_local_product.*", "b_shop_local.*", "g_city.name as city_name", "g_district.name as district_name")
            ->where('b_local_product.gtin_code', $gtin)
            ->whereNotNull('b_shop_local.phone')
            ->where('b_shop_local.phone', '<>', '')
            ->get();

        foreach ($stores as $store) {
            DB::connection('icheck_expo')
                ->table('expo_sale_points')
                ->insert([
                    'account_id' => $expoAccount->id,
                    'shop_id' => 1,
                    'product_id' => $expoProduct->id,
                    'booth_id' => $expoConfigBooth->id,
                    'phone' => $store->phone,
                    'name' => $store->name,
                    'city' => $store->city_name,
                    'district' => $store->district_name,
                    'address' => $store->address,
                    'price' => $store->price,
                ]);
        }

        return $expoProduct->id;
    }
}

//
if (!function_exists('expo_sync_store')) {
    function expo_sync_store($business_id)
    {

        $business = DB::connection('icheck_business')
            ->table('businesses')
            ->where('id', $business_id)
            ->first();

        if (!$business or !$business->phone_number) {
            return;
        }

        $expoAccount = DB::connection('icheck_expo')
            ->table('accounts')
            ->where('phone', $business->phone_number)
            ->first();

        if (!$expoAccount) {
            $expoAccountId = DB::connection('icheck_expo')->table('accounts')->insertGetId([
                'name' => $business->name,
                'phone' => $business->phone_number,
                'username' => $business->phone_number,
                'password' => '$2y$10$jKPhwD3odW/td/cYNyE57ubbetOGpdLyPAz3Ttu.cnh4sRjV6Ag.i',
                'email' => $business->email,
                'address' => $business->address,
                'image' => 'blank/blank.jpg',
                'type' => 5,
                'shop_id' => 1,
                'is_shop' => 1,
                'gln_code' => $business->phone_number,
                'shop_name' => $business->name,
            ]);
            $expoConfigBoothId = DB::connection('icheck_expo')->table('config_booths')->insertGetId([
                'shop_id' => 1,
                'account_id' => $expoAccountId,
                'hotline' => $business->phone_number,
                'name' => $business->name,
                'address' => $business->address,
            ]);
            $expoAccount = DB::connection('icheck_expo')
                ->table('accounts')
                ->where('id', $expoAccountId)
                ->first();
        } else {
            DB::connection('icheck_expo')->table('accounts')->where('id', $expoAccount->id)->update([
                'name' => $business->name,
                'phone' => $business->phone_number,
                'email' => $business->email,
                'address' => $business->address,
                'image' => 'blank/blank.jpg',
                'type' => 5,
                'shop_id' => 1,
                'is_shop' => 1,
                'gln_code' => $business->phone_number,
                'shop_name' => $business->name,
            ]);
            DB::connection('icheck_expo')->table('config_booths')->where('account_id', $expoAccount->id)->update([
                'shop_id' => 1,
                'account_id' => $expoAccount->id,
                'hotline' => $business->phone_number,
                'name' => $business->name,
                'address' => $business->address,
            ]);
        }

        return $expoAccount->id;
    }
}

if (!function_exists('bing_search')) {
    function bing_search($term) {
        $accessKey = env('BING_SUBSCRIPTION_KEY', '');
        $endpoint = 'https://api.cognitive.microsoft.com/bing/v7.0/search';
        $client = new \GuzzleHttp\Client([
            'headers' => [
                'Ocp-Apim-Subscription-Key' => $accessKey,
            ],
        ]);

        try {
            $response = $client->request('GET', $endpoint, ['query' => ['q' => $term]]);

            return array_map(
                function ($item) {
                    return [
                        'name' => $item['name'],
                        'url' => $item['url'],
                        'snippet' => $item['snippet'],
                    ];
                },
                array_get(json_decode($response->getBody(), true), 'webPages.value', [])
            );
        } catch (\Exception $e) {
            return [];
        }
    }
}
