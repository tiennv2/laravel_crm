## Change logs

### 2018-04-18

- Modified `gateway.c_product`

```mysql
ALTER TABLE `c_product`
ADD COLUMN `reviewed_by` VARCHAR(100) AFTER `status`,
ADD COLUMN `reviewed_at` TIMESTAMP NULL DEFAULT NULL AFTER `reviewed_by`,
MODIFY COLUMN `created_at` TIMESTAMP NULL DEFAULT NULL,
MODIFY COLUMN `updated_at` TIMESTAMP NULL DEFAULT NULL,
MODIFY COLUMN `deleted_at` TIMESTAMP NULL DEFAULT NULL;
CREATE INDEX `reviewed_by_idx` ON `c_product` (`reviewed_by`) USING BTREE;
```
