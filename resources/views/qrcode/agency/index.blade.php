@extends('qrcode.layouts.app')
@section('body_class', 'm-page--fluid m-page--loading-enabled m-page--loading m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default')

@push('scripts')
    <script type="text/javascript">
        <?php
            $sharedData = [
                'config' => [
                    'csrfToken' => csrf_token(),
                    'logoutUrl' => route('logout'),
                    'isNative' => true,
                ],
                'loggedInAccount' => auth()->check() ? auth()->user() : null,
            ];
            ?>
            window.SHARED_DATA = {!! json_encode($sharedData) !!}
    </script>
    <script src="https://cdn.jsdelivr.net/npm/tinymce@4.7.11/tinymce.min.js" type="text/javascript"></script>
    <script src="{{ mix('js/app.js') }}"></script>
@endpush

@section('content')
    <!-- begin::Page loader -->
    <div class="m-page-loader m-page-loader--base">
        <div class="m-blockui">
      <span>
        Please wait...
      </span>
            <span>
        <div class="m-loader m-loader--brand"></div>
      </span>
        </div>
    </div>
    <!-- end::page Loader -->
    <div id="app" class="m-grid m-grid--hor m-grid--root m-page">
        <app-header></app-header>
        <div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop  m-body m-page__container">
            <div class="m-grid__item m-grid__item--fluid m-wrapper">
                <!-- BEGIN: Subheader -->
                <div class="m-subheader">
                    <div class="d-flex align-items-center">
                        <div class="mr-auto">
                            <h3 class="m-subheader__title ">
                                QUẢN LÝ ĐẠI LÝ
                            </h3>
                        </div>
                        @can("QRCODE-addAgency")
                            <div style="float: right"><a href="{{route('Qrcode::agency@add')}}"
                                                         class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air"><span>
                                    <i class="la la-plus-circle"></i>
                                    <span>Thêm đại lý</span></span></a>
                            </div>
                        @endcan
                    </div>
                </div>
                <!-- END: Subheader -->
                <div class="m-content">
                    <!--Begin::Section-->
                    <div class="m-portlet m-portlet--mobile">
                        <div class="m-portlet__body">
                            @can("QRCODE-viewAgency")
                                <form class="m-form m-form--fit m--margin-bottom-20"
                                      action="{{route('Qrcode::agency@index')}}" method="get">
                                    <div class="row m--margin-bottom-20">
                                        <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile">
                                            <label>
                                                Tìm kiếm theo tên:
                                            </label>
                                            <input type="text" class="form-control m-input" name="name"
                                                   value="{{ Request::input('name') }}"
                                                   data-col-index="0">
                                        </div>
                                        <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile">
                                            <label>
                                                Tìm kiếm theo số ĐT:
                                            </label>
                                            <input type="text" class="form-control m-input" name="individual_phone"
                                                   value="{{ Request::input('individual_phone') }}"
                                                   data-col-index="4">
                                        </div>
                                        <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                                            <label>
                                                Tìm kiếm theo email:
                                            </label>
                                            <input type="text" class="form-control m-input" name="email"
                                                   value="{{ Request::input('email') }}"
                                                   data-col-index="4">
                                        </div>
                                        <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                                            <label>
                                                Ngày tạo:
                                            </label>
                                            <div class="input-daterange input-group">
                                                <input type="text" class="form-control" name="from" id="m_datepicker_1"
                                                       value="{{ Request::input('from') }}" autocomplete="off"
                                                       data-col-index="5"/>
                                                <div class="input-group-append">
													<span class="input-group-text">
														<i class="la la-ellipsis-h"></i>
													</span>
                                                </div>
                                                <input type="text" class="form-control" name="to" id="m_datepicker_1"
                                                       value="{{ Request::input('to') }}" autocomplete="off"
                                                       data-col-index="5"/>
                                            </div>
                                        </div>

                                        <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile">
                                            <label>
                                                Chọn trạng thái:
                                            </label>
                                            <select class="form-control m-input" data-col-index="7" id="status"
                                                    name="status">
                                                <option value="" selected="selected">--Tất cả--</option>
                                                <option value="0" {{Request::input('status')==="0"?'selected="selected"':''}}>
                                                    Chờ kích hoạt
                                                </option>
                                                <option value="1" {{Request::input('status')==1?'selected="selected"':''}}>
                                                    Đã kích hoạt
                                                </option>
                                                <option value="2" {{Request::input('status')==2?'selected="selected"':''}}>
                                                    Hủy kích hoạt
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row m--margin-bottom-20">
                                        <div class="col-lg-4 m--margin-bottom-10-tablet-and-mobile">
                                            <button class="btn btn-brand m-btn m-btn--icon" type="submit">
												<span>
													<i class="la la-search"></i>
													<span>
														Tìm kiếm
													</span>
												</span>
                                            </button>
                                            &nbsp;&nbsp;
                                            <button class="btn btn-secondary m-btn m-btn--icon" type="button"
                                                    onclick="window.location='{{route('Qrcode::agency@index')}}'">
												<span>
													<i class="la la-refresh"></i>
													<span>
														Làm mới
													</span>
												</span>
                                            </button>
                                        </div>
                                    </div>
                                </form>

                                <div class="m-separator m-separator--md m-separator--dashed"></div>
                                <h3 style="text-align: left">Thống kê số lượng đại lý</h3>
                                <div class="row m--margin-bottom-20" style="text-align: center">
                                    <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile">
                                    </div>
                                    <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile"
                                         style="border: solid #328EAC; margin-right: 5px">
                                        <label><strong>Tổng số đại lý</strong></label>
                                        <div>{{count($collaborator_total)}}</div>
                                    </div>
                                    <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile"
                                         style="border: solid #00CC00; margin-right: 5px">
                                        <label><strong>Đã duyệt</strong></label>
                                        <div>{{count($collaborator_actived_total)}}</div>
                                    </div>
                                    <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile"
                                         style="border: solid #C97626; margin-right: 5px">
                                        <label><strong>Chờ duyệt</strong></label>
                                        <div>{{count($collaborator_active_waiting_total)}}</div>
                                    </div>
                                    <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile"
                                         style="border: solid red; margin-right: 5px">
                                        <label><strong>Hủy duyệt</strong></label>
                                        <div>{{count($collaborator_deactived_total)}}</div>
                                    </div>
                                    <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile">
                                    </div>
                                </div>
                                <div class="m-separator m-separator--md m-separator--dashed"></div>
                            @endcan
                            @if (session('success'))
                                <div class="m-alert m-alert--outline m-alert--square m-alert--outline-2x alert alert-success alert-dismissible fade show"
                                     role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    </button>
                                    {{ session('success') }}
                                </div>
                            @endif
                            @can("QRCODE-viewAgency")
                                <div class="table-responsive">
                                    <table class="table table-striped- table-bordered table-hover table-checkable"
                                           style="margin-top: 20px;margin-left:auto;margin-right:auto;">
                                        <!--begin::Thead-->
                                        <thead>
                                        <tr>
                                            <th style="font-weight: bold">Id</th>
                                            <th style="width: 200px;font-weight: bold">Tên</th>
                                            <th style="font-weight: bold">Email</th>
                                            <th style="font-weight: bold;width: 300px">Địa chỉ</th>
                                            <th style="font-weight: bold">Số điện thoại</th>
                                            <th style="font-weight: bold">Ngày tạo</th>
                                            <th style="font-weight: bold">Trạng thái</th>
                                            <th style="font-weight: bold;text-align: center;width:150px">Sản lượng tem
                                                đã
                                                duyệt
                                            </th>
                                            <th style="font-weight: bold;width: 150px">Hành động</th>
                                        </tr>
                                        </thead>
                                        <!--end::Thead-->
                                        <!--begin::Tbody-->
                                        <tbody>
                                        @foreach($collaborators as $row)
                                            <tr>
                                                <td>{{$row->id}}</td>
                                                <td>
                                                    <a href="{{route('Qrcode::order@filter',[$row->collaborator_phone])}}">{{$row->name}}</a>
                                                </td>
                                                <td>{{$row->email}}</td>
                                                <td>{{$row->address}}</td>
                                                <td>{{$row->individual_phone}}</td>
                                                <td>{{date("d/m/Y",strtotime($row->created_at))}}</td>
                                                <td>
                                                    @if($row->status==1)
                                                        <a href="{{route('Qrcode::agency@disapprove',[$row->id])}}"><span
                                                                    class="m-badge m-badge--success m-badge--wide">Kích hoạt </span></a>
                                                    @elseif($row->status==2)
                                                        <a href="{{route('Qrcode::agency@approve',[$row->id])}}"><span
                                                                    class="m-badge m-badge--danger m-badge--wide">Hủy kích hoạt </span></a>
                                                    @else
                                                        <a href="{{route('Qrcode::agency@approve',[$row->id])}}"><span
                                                                    class="m-badge m-badge--warning m-badge--wide">Chờ kích hoạt </span></a>
                                                    @endif
                                                </td>
                                                <td style="text-align: center;">{{$row->stamp_ordered_approved_total}}</td>
                                                <td>
                                                    <button type="button"
                                                            class="btn m-btn m-btn--gradient-from-primary m-btn--gradient-to-primary"
                                                            onclick="window.location='{{route('Qrcode::agency@edit',[$row->id])}}'">
                                                        Sửa
                                                    </button>
                                                    <button type="button"
                                                            class="btn m-btn m-btn--gradient-from-danger m-btn--gradient-to-warning"
                                                            data-toggle="modal" data-target="#delete{{$row->id}}">
                                                        Xóa
                                                    </button>
                                                    <!--Modal Comfirm approve-->
                                                    <div id="approve{{$row->id}}" class="modal fade"
                                                         role="dialog">
                                                        <div class="modal-dialog">
                                                            <!-- Modal content-->
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h2 class="modal-title">Bạn đồng ý
                                                                        kích hoạt đại lý {{$row->name}}?</h2>
                                                                    <button type="button" class="close"
                                                                            data-dismiss="modal">&times;
                                                                    </button>

                                                                </div>
                                                                <form method="get"
                                                                      action="{{ route('Qrcode::agency@approve',[$row->id]) }}">
                                                                    {{ csrf_field() }}

                                                                    <div class="modal-footer">
                                                                        <button type="submit"
                                                                                class="btn btn-success">
                                                                            Đồng ý
                                                                        </button>
                                                                        <button type="button"
                                                                                class="btn btn-default"
                                                                                data-dismiss="modal">Hủy
                                                                            yêu cầu
                                                                        </button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--End Modal-->
                                                    <!--Modal Comfirm reject-->
                                                    <div id="reject{{$row->id}}" class="modal fade"
                                                         role="dialog">
                                                        <div class="modal-dialog">
                                                            <!-- Modal content-->
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h2 class="modal-title">Bạn chắc
                                                                        chắn muốn hủy kích hoạt đại lý {{$row->name}}
                                                                        ?</h2>
                                                                    <button type="button" class="close"
                                                                            data-dismiss="modal">&times;
                                                                    </button>

                                                                </div>
                                                                <form method="get"
                                                                      action="{{ route('Qrcode::agency@disapprove',[$row->id]) }}">
                                                                    {{ csrf_field() }}
                                                                    <div class="modal-footer">
                                                                        <button type="submit"
                                                                                class="btn btn-success">
                                                                            Đồng ý
                                                                        </button>
                                                                        <button type="button"
                                                                                class="btn btn-default"
                                                                                data-dismiss="modal">Hủy
                                                                            yêu cầu
                                                                        </button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--End Modal-->
                                                    <!--Modal Comfirm delete-->
                                                    <div id="delete{{$row->id}}" class="modal fade"
                                                         role="dialog">
                                                        <div class="modal-dialog">
                                                            <!-- Modal content-->
                                                            <div class="modal-content">
                                                                <div class="modal-header">

                                                                    <h2 class="modal-title">Bạn chắc
                                                                        chắn muốn xóa bỏ đại lý {{$row->name}}?</h2>
                                                                    <button type="button" class="close"
                                                                            data-dismiss="modal">&times;
                                                                    </button>
                                                                </div>
                                                                <form method="get"
                                                                      action="{{ route('Qrcode::agency@delete',[$row->id]) }}">
                                                                    {{ csrf_field() }}
                                                                    <div class="modal-footer">
                                                                        <button type="submit"
                                                                                class="btn btn-success">
                                                                            Đồng ý
                                                                        </button>
                                                                        <button type="button"
                                                                                class="btn btn-default"
                                                                                data-dismiss="modal">Hủy
                                                                            yêu cầu
                                                                        </button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--End Modal-->
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                        <!--end::Tbody-->
                                    </table>
                                </div>

                                <!--end::Table-->
                                <div style="float:right;"><?php echo $collaborators->links(); ?></div>
                            @endcan
                        </div>
                        <!--End::Section-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- <router-view></router-view>--}}
@endsection
@push('scripts')
    <script type="text/javascript" src="{{ asset('qr_code/metronic/js/vendors.bundle.js') }}"></script>
    {{-- <script type="text/javascript" src="{{ asset('qr_code/metronic/js/scripts.bundle.js') }}"></script>--}}
    <script type="text/javascript" src="{{ asset('qr_code/metronic/js/datepicker/bootstrap-datepicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('qr_code/metronic/js/datatables/datatables.bundle.js') }}"></script>
    <script type="text/javascript" src="{{ asset('qr_code/metronic/js/datatables/scrollable.js') }}"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            // Basic
            $("#status").select2({
                placeholder: 'Trạng thái',
                allowClear: true
            });

            $("select").on("select2:select", function (evt) {
                var element = evt.params.data.element;
                var $element = $(element);
                $element.detach();
                $(this).append($element);
                $(this).trigger("change");
            });


        });
    </script>

@endpush
