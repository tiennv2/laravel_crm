<!--


  iiii         CCCCCCCCCCCCChhhhhhh                                                     kkkkkkkk
 i::::i     CCC::::::::::::Ch:::::h                                                     k::::::k
  iiii    CC:::::::::::::::Ch:::::h                                                     k::::::k    TQ
         C:::::CCCCCCCC::::Ch:::::h                                                     k::::::k
iiiiiii C:::::C       CCCCCC h::::h hhhhh           eeeeeeeeeeee        cccccccccccccccc k:::::k    kkkkkkk
i:::::iC:::::C               h::::hh:::::hhh      ee::::::::::::ee    cc:::::::::::::::c k:::::k   k:::::k
 i::::iC:::::C               h::::::::::::::hh   e::::::eeeee:::::ee c:::::::::::::::::c k:::::k  k:::::k
 i::::iC:::::C               h:::::::hhh::::::h e::::::e     e:::::ec:::::::cccccc:::::c k:::::k k:::::k
 i::::iC:::::C     H         h::::::h   h::::::he:::::::eeeee::::::ec::::::c     ccccccc k::::::k:::::k
 i::::iC:::::C               h:::::h     h:::::he:::::::::::::::::e c:::::c              k:::::::::::k
 i::::iC:::::C               h:::::h     h:::::he::::::eeeeeeeeeee  c:::::c              k:::::::::::k
 i::::i C:::::C       CCCCCC h:::::h     h:::::he:::::::e           c::::::c     ccccccc k::::::k:::::k
i::::::i C:::::CCCCCCCC::::C h:::::h     h:::::he::::::::e          c:::::::cccccc:::::ck::::::k k:::::k
i::::::i  CC:::::::::::::::C h:::::h  u  h:::::h e::::::::eeeeeeee   c:::::::::::::::::ck::::::k  k:::::k
i::::::i    CCC::::::::::::C h:::::h     h:::::h  ee:::::::::::::e    cc:::::::::::::::ck::::::k   k:::::k
iiiiiiii       CCCCCCCCCCCCC hhhhhhh     hhhhhhh    eeeeeeeeeeeeee  y   cccccccccccccccckkkkkkkk    kkkkkkk


-->
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <title>@yield('title', config('app.name', 'Laravel'))</title>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <!-- Styles -->
    <link href="{{asset('qr_code/metronic/css/vendors.bundle.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('qr_code/metronic/css/style.bundle.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ mix('css/vendor.css') }}" rel="stylesheet"/>
    <link href="{{ mix('css/app.css') }}" rel="stylesheet"/>

    <base href="/">
    <link rel="icon" type="image/x-icon" href="favicon.ico">

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://ajax.googleapis.com"/>
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js"></script>
    <script>
        WebFont.load({

            google: {"families": ["Roboto:300,400,500,600,700:vietnamese"]},
            // google: {"families":["Roboto:300,400,500,700:vietnamese"]},
            active: function () {
                sessionStorage.fonts = true;
            }
        });
    </script>

    @stack('styles')
</head>
<body class="@yield('body_class')" style="padding-right: 0">
<audio id="myAudio">
    <source src="{{asset('sounds/slow-spring-board.mp3')}}" type="audio/mpeg">
</audio>
@yield('content')
@stack('scripts')
<script>
    var alert_sound = document.getElementById("myAudio");
    function playAudio() {
        alert_sound.play();
    }
    function countBusiness() {
        var url = "{{ route('Business::businesses@count')}}";
        var count = 0;
        $.ajax({
            url: url,
            method: 'GET',
            async: false,
            success: function (data) {
                count = parseInt(data);
            }
        });
        return count;
    }

    var businessNumber = countBusiness();
    var interval_obj = setInterval(function () {
        let count = countBusiness();
        if (count > businessNumber) {
            let newAdd = count - businessNumber;
            businessNumber = count;
            playAudio();
            alert(`Có ${newAdd} doanh nghiệp vừa được tạo mới!`);
        }
    }, 2000);
</script>
</body>
</html>
