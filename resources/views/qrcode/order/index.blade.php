@extends('qrcode.layouts.app')

@section('body_class', 'm-page--fluid m-page--loading-enabled m-page--loading m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default')

@push('scripts')
  <script type="text/javascript">
    <?php
      $sharedData = [
        'config' => [
          'csrfToken' => csrf_token(),
          'logoutUrl' => route('logout'),
          'isNative' => true,
        ],
        'loggedInAccount' => auth()->check() ? auth()->user() : null,
      ];
      ?>
      window.SHARED_DATA = {!! json_encode($sharedData) !!}
  </script>
  <script src="https://cdn.jsdelivr.net/npm/tinymce@4.7.11/tinymce.min.js" type="text/javascript"></script>
  <script src="{{ mix('js/app.js') }}"></script>
@endpush
@push('styles')
  <style>
    .select2 {
      width: 100% !important;
    }
  </style>
@endpush
@section('content')
  <!-- begin::Page loader -->
  <div class="m-page-loader m-page-loader--base">
    <div class="m-blockui">
      <span>
        Please wait...
      </span>
      <span>
        <div class="m-loader m-loader--brand"></div>
      </span>
    </div>
  </div>
  <!-- end::Page Loader -->
  <div id="app" class="m-grid m-grid--hor m-grid--root m-page">
    <div class="m-header__bottom">
      <div class="m-container m-container--responsive m-container--xxl m-container--full-height m-page__container">
        <div class="m-stack m-stack--ver m-stack--desktop">
          <div class="m-stack__item m-stack__item--middle m-stack__item--fluid">
            <app-header></app-header>
          </div>
        </div>
      </div>
    </div>

    <div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop  m-body m-page__container">
      <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader ">
          <div class="d-flex align-items-center">
            <div class="mr-auto">
              <h3 class="m-subheader__title ">
                QUẢN LÝ ĐƠN HÀNG
              </h3>
            </div>
            @can("QRCODE-addOrder")
              <div><a href="{{route('Qrcode::order@add')}}"
                      class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air"><span>
                                    <i class="la la-cart-plus"></i>
                                    <span>Thêm đơn hàng</span></span></a>
              </div>
            @endcan
          </div>
        </div>
        <!-- END: Subheader -->
        @can("QRCODE-viewOrder")
          <div class="m-content">
            <!--Begin::Section-->
            <div class="m-portlet m-portlet--mobile">
              <div class="m-portlet__body">
                <form class="m-form m-form--fit m--margin-bottom-20"
                      action="{{route('Qrcode::order@index')}}" method="get">
                  <!-- Search Field -->
                  <div class="row m--margin-bottom-20">
                    <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile">
                      <input class="form-control" type="text" name="name"
                             placeholder="Tìm theo tên đối tác"
                             value="{{ Request::input('name') }}"/>
                    </div>

                    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                      <select class="form-control" id="services" name="services[]"
                              multiple="multiple">
                        <option value="">--Tất cả--</option>
                        @foreach($services as $service)
                          <option
                            value="{{$service->id}}"{{(Request::input('services')!= null and in_array($service->id,Request::input('services')))?'selected="selected"':''}}>{{$service->service_name}}</option>
                        @endforeach

                      </select>
                    </div>

                    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">

                      <div class="input-daterange input-group">
                                            <span class="input-group-addon">
												Ngày tạo: </span>
                        <input type="text" class="form-control" name="from" id="m_datepicker_1"
                               value="{{ Request::input('from') }}" autocomplete="off">
                        <span class="input-group-text">
														<i class="la la-ellipsis-h"></i>
													</span>
                        <input type="text" class="form-control" name="to" id="m_datepicker_1"
                               value="{{ Request::input('to') }}" autocomplete="off">
                      </div>
                    </div>
                    <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile">
                      <select class="form-control" id="status" name="status">
                        <option value="">Chọn trạng thái</option>
                        <option value="0" {{Request::input('status')=== '0'?'selected="selected"':''}}>
                          Chờ duyệt
                        </option>
                        <option value="1" {{Request::input('status')== 1?'selected="selected"':''}}>
                          Đã duyệt
                        </option>
                        <option value="2" {{Request::input('status')== 2?'selected="selected"':''}}>
                          Hủy duyệt
                        </option>
                      </select>
                    </div>
                    <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile">
                      <select class="payment_status form-control m-input" data-col-index="7"

                              name="payment_status">
                        <option value="" selected="selected">--Tất cả--</option>
                        {{--<option--}}
                        {{--value="{{\App\Models\Qrcode\Order::PAYMENT_STATUS_UNKNOWN}}" {{(Request::input('payment_status') != "" && Request::input('payment_status') == \App\Models\Qrcode\Order::PAYMENT_STATUS_UNKNOWN) ?'selected="selected"':''}}>--}}
                        {{--Chưa rõ--}}
                        {{--</option>--}}
                        <option
                          value="{{\App\Models\Qrcode\Order::PAYMENT_STATUS_FALSE}}" {{Request::input('payment_status')== \App\Models\Qrcode\Order::PAYMENT_STATUS_FALSE ?'selected="selected"':''}}>
                          Chưa thanh toán
                        </option>
                        <option
                          value="{{\App\Models\Qrcode\Order::PAYMENT_STATUS_TRUE}}" {{Request::input('payment_status')== \App\Models\Qrcode\Order::PAYMENT_STATUS_TRUE ?'selected="selected"':''}}>
                          Đã thanh toán
                        </option>

                      </select>
                    </div>
                  </div>
                  {{--  <div class="m-separator m-separator--md m-separator--dashed"></div>--}}
                  <div class="row m--margin-bottom-20">
                    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                      <select class="form-control" id="user_created" name="user_created">
                        <option value=""></option>
                        <option value="admin" {{Request::input('user_created')=="admin"?'selected="selected"':''}}>
                          Admin iCheck
                        </option>
                        @foreach($users as $user)
                          <option value="{{$user->id}}"
                            {{Request::input('user_created')==$user->id?'selected="selected"':''}}>{{$user->name}}</option>
                        @endforeach
                      </select>
                    </div>
                    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                      <select class="form-control" id="account_created" name="account_created">
                        <option value=""></option>
                        @foreach($accounts as $account)
                          <option value="{{$account->id}}"
                            {{Request::input('account_created')==$account->id?'selected="selected"':''}}>{{$account->name}}</option>
                        @endforeach
                      </select>
                    </div>
                    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                      <select class="form-control" id="user_approve" name="user_approve">
                        <option value=""></option>
                        <option value="admin" {{Request::input('user_approve')=="admin"?'selected="selected"':''}}>
                          Admin iCheck
                        </option>
                        @foreach($users as $user)
                          <option value="{{$user->id}}"
                            {{Request::input('user_approve')==$user->id?'selected="selected"':''}}>{{$user->name}}</option>
                        @endforeach
                      </select>
                    </div>
                    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                      <select class="form-control" id="account_group" name="account_group">
                        <option value=""></option>
                        <option value="icheck" {{Request::input('account_group')=="icheck"?'selected="selected"':''}}>
                          iCheck
                        </option>
                        @foreach($users as $user)
                          <option value="{{$user->collaborator_phone}}"
                            {{((isset($collaborator_phone) && $collaborator_phone==$user->collaborator_phone)|| Request::input('account_group')==$user->collaborator_phone)?'selected="selected"':''}}>{{$user->name}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="row m--margin-bottom-20">
                    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                      <button class="btn btn-brand m-btn m-btn--icon" type="submit">
												<span>
													<i class="la la-search"></i>
													<span>
														Tìm kiếm
													</span>
												</span>
                      </button>
                      &nbsp;&nbsp;
                      <button class="btn btn-secondary m-btn m-btn--icon" type="button"
                              onclick="window.location='{{route('Qrcode::order@index')}}'">
												<span>
													<i class="la la-refresh"></i>
													<span>
														Làm mới
													</span>
												</span>
                      </button>
                    </div>
                  </div>
                </form>
                <div class="m-separator m-separator--md m-separator--dashed"></div>
                <h3 style="text-align: left">Thống kê số lượng đơn hàng</h3>
                <div class="row m--margin-bottom-20" style="text-align: center">
                  <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile">
                  </div>
                  <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile"
                       style="border: solid #328EAC; margin-right: 5px">
                    <label><strong>Tổng số đơn hàng</strong></label>
                    <div>{{number_format($order_total,0,".",",")}}</div>
                  </div>
                  <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile"
                       style="border: solid #00CC00; margin-right: 5px">
                    <label><strong>Đã duyệt</strong></label>
                    <div>{{number_format($order_approved_total,0,".",",")}}</div>
                  </div>
                  <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile"
                       style="border: solid #C97626; margin-right: 5px">
                    <label><strong>Chờ duyệt</strong></label>
                    <div>{{number_format($order_approve_waiting_total,0,".",",")}}</div>
                  </div>
                  <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile"
                       style="border: solid red; margin-right: 5px">
                    <label><strong>Hủy duyệt</strong></label>
                    <div>{{number_format($order_disapproved_total,0,".",",")}}</div>
                  </div>
                  <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile">
                  </div>
                </div>
                <div class="m-separator m-separator--md m-separator--dashed"></div>
                <h3 style="text-align: left">Thống kê số lượng tem đặt hàng</h3>
                <div class="row m--margin-bottom-20" style="text-align: center">
                  <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile">
                  </div>
                  <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile"
                       style="border: solid #328EAC; margin-right: 5px">
                    <label><strong>Tổng số tem</strong></label>
                    <div>{{number_format($stamp_ordered_total,0,".",",")}}</div>
                  </div>
                  <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile"
                       style="border: solid #00CC00; margin-right: 5px">
                    <label><strong>Đã duyệt</strong></label>
                    <div>{{number_format($stamp_ordered_approved_total,0,".",",")}}</div>
                  </div>
                  <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile"
                       style="border: solid #C97626; margin-right: 5px">
                    <label><strong>Chờ duyệt</strong></label>
                    <div>{{number_format($stamp_ordered_waiting_total,0,".",",")}}</div>
                  </div>
                  <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile"
                       style="border: solid red; margin-right: 5px">
                    <label><strong>Hủy duyệt</strong></label>
                    <div>{{number_format($stamp_ordered_disapproved_total,0,".",",")}}</div>
                  </div>
                  <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile">
                  </div>
                </div>
                <div class="m-separator m-separator--md m-separator--dashed"
                     style="margin-bottom: 0px"></div>

                @if (session('success'))
                  <div
                    class="m-alert m-alert--outline m-alert--square m-alert--outline-2x alert alert-success alert-dismissible fade show"
                    role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    </button>
                    {{ session('success') }}
                  </div>
                @endif
              <!--begin::Table-->
                @can("QRCODE-exportExcelOrder")
                  <div style="text-align: right"><a
                      href="{{route('Qrcode::order@export_excel',$query)}}"
                      class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air"><span>
                                    <i class="la la-cart-plus"></i>
                                    <span>Xuất file Excel</span></span></a>
                  </div>
                @endcan
                <div class="table-responsive">
                  <table class="table table-striped- table-bordered table-hover table-checkable"
                         style="margin-top: 55px" width="1800px">
                    <thead>
                    <tr>
                      <th style="font-weight: bold">Id</th>
                      <th style="font-weight: bold; min-width: 300px">Tên đối tác</th>
                      <th style="font-weight: bold">Số lượng code</th>
                      <th style="font-weight: bold; min-width: 100px">Giá trị code</th>
                      <th style="font-weight: bold; min-width: 100px">Số lượng in</th>
                      <th style="font-weight: bold; min-width: 100px">Giá trị in</th>
                      <th style="font-weight: bold; min-width: 100px">Dịch vụ</th>
                      <th style="font-weight: bold; min-width: 150px">Trạng thái đơn hàng</th>
                      <th style="font-weight: bold; min-width: 180px">Trạng thái thanh toán</th>
                      <th style="font-weight: bold">Ngày tạo</th>
                      <th style="font-weight: bold; min-width: 200px">Người tạo</th>
                      <th style="font-weight: bold; min-width: 200px">Người duyệt</th>
                      <th style="font-weight: bold;text-align: center">Hành động</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($orders as $row)
                      <tr role="row">
                        <td class="id">{{$row->id}}</td>
                        <td>{{$row->account_name}}</td>
                        <td>
                          @if($row->status==0 && auth()->user()->can('QRCODE-updateOrder'))
                            <a href="#" data-toggle="modal"
                               data-target="#edit{{$row->id}}">{{number_format($row->quantity,0,".",",")}}</a>
                          @else
                            {{number_format($row->quantity,0,".",",")}}
                          @endif
                        </td>
                        <td class="edit_order_value" style="cursor: pointer">
                          @if($row->status != 0 && auth()->user()->can('QRCODE-updateOrder'))
                            <span
                              class="formatted_order_value">{{number_format($row->order_value)}}</span>
                            <input type="number" class="order_value form-control m-input" name="order_value" min="0"
                                   value="{{$row->order_value}}"
                                   data-col-index="0">
                          @endif
                        </td>
                        <td class="edit_print_quantity" style="cursor: pointer">
                          @if($row->status != 0 && auth()->user()->can('QRCODE-updateOrder'))
                            <span
                              class="formatted_print_quantity">{{number_format($row->print_quantity,0,".",",")}}</span>
                            <input type="number" class="print_quantity form-control m-input" name="print_quantity"
                                   min="0"
                                   value="{{$row->print_quantity}}"
                                   data-col-index="0">
                          @endif
                        </td>
                        <td class="edit_print_value" style="cursor: pointer">
                          @if($row->status != 0 && auth()->user()->can('QRCODE-updateOrder'))
                            <span
                              class="formatted_print_value">{{number_format($row->print_value,0,".",",")}}</span>
                            <input type="number" class="print_value form-control m-input" name="print_value" min="0"
                                   value="{{$row->print_value}}"
                                   data-col-index="0">
                          @endif
                        </td>
                        <td>@foreach($services as $service)
                            @if(strpos($row->services,strval($service->id))!== false)
                              {{$service->service_name}};
                            @endif
                          @endforeach
                        </td>
                        <td class="status">
                          @if($row->status==\App\Models\Qrcode\Order::STATUS_APPROVED)
                            <span class="m-badge m-badge--success m-badge--wide">
									Đã duyệt </span>
                          @elseif($row->status==\App\Models\Qrcode\Order::STATUS_DISAPPROVED)
                            <span class="m-badge m-badge--danger m-badge--wide">
									Đã hủy duyệt </span>
                          @else
                            <span class="m-badge m-badge--warning m-badge--wide">
									Chờ duyệt </span>
                          @endif
                        </td>
                        <td>
                          <select id="payment_status_{{$row->id}}"
                                  @if(auth()->user()->cannot('QRCODE-updatePaymentStatus'))
                                  disabled
                                  @endif
                                  class="form-control m-input" name="payment_status"
                          >

                            {{--<option value="{{\App\Models\Qrcode\Order::PAYMENT_STATUS_UNKNOWN}}"--}}
                                    {{--@if($row->payment_status == \App\Models\Qrcode\Order::PAYMENT_STATUS_UNKNOWN || $row->payment_status == null) selected @endif>--}}
                              {{--Chưa rõ--}}
                            {{--</option>--}}
                            <option value="{{\App\Models\Qrcode\Order::PAYMENT_STATUS_FALSE}}"
                                    @if($row->payment_status == \App\Models\Qrcode\Order::PAYMENT_STATUS_FALSE) selected @endif>
                              Chưa thanh toán
                            </option>
                            <option value="{{\App\Models\Qrcode\Order::PAYMENT_STATUS_TRUE}}"
                                    @if($row->payment_status == \App\Models\Qrcode\Order::PAYMENT_STATUS_TRUE) selected @endif>
                              Đã thanh toán
                            </option>
                          </select>
                        </td>
                        <td>{{date("d/m/Y",strtotime($row->created_at))}}</td>
                        <td>
                          @php
                            $a = null;
                        foreach($create_order_history as $history){
                            if($row->id==$history->order_id)
                                 $a = $history->collaborator_id;
                          }

                        if($row->user_created==null && $a != null){
                        foreach($users as $user){
                                if($user->id==$a)
                                    echo $user->name;
                                }
                        }
                            elseif($row->user_created==null && $a==null) {
                            echo 'Admin iCheck';
                             }
                        else{
                            foreach($accounts as $account){
                                if($row->user_created==$account->id)
                                    echo $account->name;
                                }
                        }
                          @endphp
                        </td>
                        <td>
                          @php
                            if($row->status!=0)
                            {
                                $b = null;
                            foreach($approve_order_history as $history){
                                if($row->id==$history->order_id)
                                     $b = $history->collaborator_id;
                              }

                            if($b != null){
                            foreach($users as $user){
                                    if($user->id==$b)
                                        echo $user->name;
                                    }
                            }
                            else{
                                echo 'Admin iCheck';
                            }
                            }
                          @endphp
                        </td>
                        <td style="text-align: center" class="order_action">
                          @if($row->status==0)
                            <div
                              class="m-dropdown m-dropdown--up m-dropdown--inline m-dropdown--small m-dropdown--arrow m-dropdown--align-right"
                              m-dropdown-toggle="hover">
                              <a class="m-dropdown__toggle btn btn-brand dropdown-toggle">
                              </a>
                              <div class="m-dropdown__wrapper">
                                <div class="m-dropdown__inner">
                                  <div class="m-dropdown__body">
                                    <div class="m-dropdown__content">
                                      <ul class="m-nav">
                                        {{--<li class="m-nav__item"
                                            style="margin-bottom: 5px">
                                            <a data-toggle="modal"
                                               data-target="#edit{{$row->id}}"
                                               class="m-nav__link">
                                                <i class="m-nav__link-icon flaticon-edit"></i>
                                                <span class="m-nav__link-text">Sửa</span>
                                            </a>
                                        </li>--}}
                                        @can("QRCODE-approveOrder")
                                          <li class="m-nav__item"
                                              style="margin-bottom:10px">
                                            <a href="{{route('Qrcode::order@approve',[$row->id])}}"
                                               class="m-nav__link">
                                              <i class="m-nav__link-icon la la-check"></i>
                                              <span class="m-nav__link-text">Duyệt</span>
                                            </a>
                                          </li>
                                        @endcan
                                        {{--<li class="m-nav__separator m-nav__separator--fit"></li>--}}
                                        @can("QRCODE-disapproveOrder")
                                          <li class="m-nav__item">
                                            <a href="{{route('Qrcode::order@disapprove',[$row->id])}}"
                                               class="m-nav__link">
                                              <i class="m-nav__link-icon la la-close"></i>
                                              <span class="m-nav__link-text">Hủy duyệt</span>
                                            </a>
                                          </li>
                                        @endcan
                                      </ul>
                                    </div>
                                  </div>
                                </div>
                                <span class="m-dropdown__arrow m-dropdown__arrow--right"></span>
                              </div>
                            </div>
                        @endif
                        <!--Modal Comfirm approve-->
                          <div id="approve{{$row->id}}" class="modal fade" tabindex="-1"
                               role="dialog" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h2 class="modal-title">Bạn đồng ý duyệt đơn
                                    hàng này?</h2>
                                  <button type="button" class="close"
                                          data-dismiss="modal"
                                          aria-hidden="true"></button>
                                </div>
                                <form method="get"
                                      action="{{ route('Qrcode::order@approve',[$row->id]) }}">
                                  {{ csrf_field() }}
                                  <div class="modal-footer">
                                    <button type="submit"
                                            class="btn btn-success">Đồng ý
                                    </button>
                                    <button type="button"
                                            class="btn btn-default"
                                            data-dismiss="modal">Hủy yêu cầu
                                    </button>
                                  </div>
                                </form>
                              </div>
                            </div>
                          </div>
                          <!--End Modal-->
                          <!--Modal Comfirm reject-->
                          <div id="reject{{$row->id}}" class="modal fade" role="dialog"
                               aria-hidden="true">
                            <div class="modal-dialog">
                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h2 class="modal-title">Bạn chắc chắn muốn bỏ
                                    duyệt đơn hàng này?</h2>
                                  <button type="button" class="close"
                                          data-dismiss="modal"
                                          aria-hidden="true"></button>
                                  </button>
                                </div>
                                <form method="get"
                                      action="{{ route('Qrcode::order@disapprove',[$row->id]) }}">
                                  {{ csrf_field() }}
                                  <div class="modal-footer">
                                    <button type="submit"
                                            class="btn btn-success">Đồng ý
                                    </button>
                                    <button type="button"
                                            class="btn btn-default"
                                            data-dismiss="modal">Hủy yêu cầu
                                    </button>
                                  </div>
                                </form>
                              </div>
                            </div>
                          </div>
                          <!--End Modal-->
                          <!--Modal Edit Order-->
                          <div class="modal fade bs-modal-lg" id="edit{{$row->id}}"
                               tabindex="-1"
                               role="dialog"
                               aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                              <div class="modal-content">
                                <form class="form-horizontal" id="modal" role="form"
                                      method="POST" enctype="multipart/form-data"
                                      action="{{route('Qrcode::order@update', [$row->id])}}">{{ csrf_field() }}
                                  <input type="hidden" name="_method" value="PUT">
                                  <div class="modal-header">
                                    <h2>Sửa đơn hàng</h2>
                                    <button type="button" class="close"
                                            data-dismiss="modal"
                                            aria-hidden="true"></button>
                                  </div>
                                  <div class="modal-body">
                                    <div class="portlet box green ">
                                      <div class="portlet-body form">
                                        <div class="form-body">
                                          <div class="form-group m-form__group row">
                                            <label class="col-form-label col-lg-3 col-sm-12"><strong>Nhập
                                                số
                                                lượng</strong></label>
                                            <div class="col-lg-3 col-md-9 col-sm-12">
                                              <input type="number"
                                                     name="quantity"
                                                     min="1"
                                                     class="form-control"
                                                     placeholder="Nhập số lượng"
                                                     value="{{$row->quantity}}"
                                                     required>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="submit"
                                            class="btn btn-success">
                                      Cập nhật
                                    </button>
                                    <button type="button" class="btn default"
                                            data-dismiss="modal">
                                      Hủy
                                    </button>
                                  </div>
                                </form>
                              </div>
                            </div>
                          </div>
                        </td>
                        <!--End Modal-->
                      </tr>
                    @endforeach
                    </tbody>
                  </table>
                </div>
                <!--end::Table-->
                <div style="float:right;"><?php echo $orders->links(); ?></div>
              </div>
            </div>
          </div>
        @endcan
      </div>
    </div>
  </div>
  <router-view></router-view>
@endsection
@push('scripts')
  <script type="text/javascript" src="{{ asset('qr_code/metronic/js/vendors.bundle.js') }}"></script>
  <script type="text/javascript" src="{{ asset('qr_code/metronic/js/datepicker/bootstrap-datepicker.js') }}"></script>
  <script>
    $("input[name='order_value']").hide();
    $("input[name='print_value']").hide();
    $("input[name='print_quantity']").hide();
    $(document).ready(function () {
      // Basic
      $("#status").select2({
        placeholder: 'Chọn trạng thái',
        allowClear: true
      });
      $("#user_created").select2({
        placeholder: 'Chọn người tạo là admin',
        allowClear: true
      });
      $("#user_approve").select2({
        placeholder: 'Chọn người duyệt',
        allowClear: true
      });
      $("#account_created").select2({
        placeholder: 'Chọn người tạo là khách hàng',
        allowClear: true
      });
      $("#account_group").select2({
        placeholder: 'Chọn nhóm khách hàng thuộc đại lý',
        allowClear: true
      });
      $("#services").select2({
        placeholder: 'Chọn dịch vụ'
      });
      $(".payment_status").select2({
        placeholder: 'Trạng thái thanh toán'
      });
      $("select").on("select2:select", function (evt) {
        var element = evt.params.data.element;
        var $element = $(element);
        $element.detach();
        $(this).append($element);
        $(this).trigger("change");
      });
    });

  </script>
  <script>
    $("input[name='order_value']").change(function () {
      var $this = $(this);
      var order_id = $this.closest("tr").find(".id").text();
      var order_value = $this.val();
      var url = "{{ route('Qrcode::order@updateOrderValue')}}";
      $.ajax({
        url: url,
        method: 'POST',
        data: {
          order_id: order_id,
          order_value: order_value,
        },
        success: function (value) {
          let format_value = value.replace(/[\D\s\._\-]+/g, "");
          format_value = format_value ? parseInt(format_value, 10) : 0;
          $this.siblings(".formatted_order_value").html(function () {
            return (format_value === 0) ? "0" : format_value.toLocaleString("en-US");
          });

          $this.hide();
        },
        error: function (error) {
          console.log(error);
        }
      });
    });

    $(".edit_order_value").click(function () {
      if ($(this).children(".order_value").css('display') === 'none') {
        $(this).children(".order_value").show();
      }
    });
    $(document).click(function (e) {
      if ($(e.target).children(".order_value").length === 0 && $(e.target).siblings(".order_value").length === 0 && !$(e.target).hasClass("order_value")) {
        $(".order_value").hide();
      }
    });
  </script>
  <script>
    $("input[name='print_value']").change(function () {
      var $this = $(this);
      var order_id = $this.closest("tr").find(".id").text();
      var print_value = $this.val();
      var url = "{{ route('Qrcode::order@updatePrintValue')}}";
      $.ajax({
        url: url,
        method: 'POST',
        data: {
          order_id: order_id,
          print_value: print_value,
        },
        success: function (value) {
          let format_value = value.replace(/[\D\s\._\-]+/g, "");
          format_value = format_value ? parseInt(format_value, 10) : 0;
          $this.siblings(".formatted_print_value").html(function () {
            return (format_value === 0) ? "0" : format_value.toLocaleString("en-US");
          });

          $this.hide();
        },
        error: function (error) {
          console.log(error);
        }
      });
    });

    $(".edit_print_value").click(function () {
      if ($(this).children(".print_value").css('display') === 'none') {
        $(this).children(".print_value").show();
      }
    });
    $(document).click(function (e) {
      if ($(e.target).children(".print_value").length === 0 && $(e.target).siblings(".print_value").length === 0 && !$(e.target).hasClass("print_value")) {
        $(".print_value").hide();
      }
    });
  </script>
  <script>
    $("input[name='print_quantity']").change(function () {
      var $this = $(this);
      var order_id = $this.closest("tr").find(".id").text();
      var print_quantity = $this.val();
      var url = "{{ route('Qrcode::order@updatePrintQuantity')}}";
      $.ajax({
        url: url,
        method: 'POST',
        data: {
          order_id: order_id,
          print_quantity: print_quantity,
        },
        success: function (value) {
          let format_value = value.replace(/[\D\s\._\-]+/g, "");
          format_value = format_value ? parseInt(format_value, 10) : 0;
          $this.siblings(".formatted_print_quantity").html(function () {
            return (format_value === 0) ? "0" : format_value.toLocaleString("en-US");
          });

          $this.hide();
        },
        error: function (error) {
          console.log(error);
        }
      });
    });

    $(".edit_print_quantity").click(function () {
      if ($(this).children(".print_quantity").css('display') === 'none') {
        $(this).children(".print_quantity").show();
      }
    });
    $(document).click(function (e) {
      if ($(e.target).children(".print_quantity").length === 0 && $(e.target).siblings(".print_quantity").length === 0 && !$(e.target).hasClass("print_quantity")) {
        $(".print_quantity").hide();
      }
    });

    //Update Payment_status
    $("select[name='payment_status']").change(function () {
      var cur_select = $(this);
      var payment_status = parseInt(this.value);
      var order_id = this.id.split("_").pop();
      var url = "{{ route('Qrcode::order@updatePaymentStatus',":id")}}";
      url = url.replace(':id', order_id);

      $.ajax({
        url: url.replace(':id', order_id),
        method: 'POST',
        data: {
          payment_status: payment_status,
        },
        success: function (data) {
          if(data === "{{\App\Models\Business\Subscription::SUBSCRIPTION_STATUS_APPROVED}}") {
            cur_select.closest('tr').find('.status').html('<span class="m-badge m-badge--success m-badge--wide">Đã duyệt </span>');
            cur_select.closest('tr').find('.order_action').html('');
          }
        },
        error: function () {
          console.log('fail');
        }
      });
    });
  </script>
@endpush
