@extends('qrcode.layouts.app')
@section('body_class', 'm-page--fluid m-page--loading-enabled m-page--loading m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default')

@push('scripts')
    <script type="text/javascript">
        <?php
            $sharedData = [
                'config' => [
                    'csrfToken' => csrf_token(),
                    'logoutUrl' => route('logout'),
                    'isNative' => true,
                ],
                'loggedInAccount' => auth()->check() ? auth()->user() : null,
            ];
            ?>
            window.SHARED_DATA = {!! json_encode($sharedData) !!}
    </script>
    <script src="https://cdn.jsdelivr.net/npm/tinymce@4.7.11/tinymce.min.js" type="text/javascript"></script>
    <script src="{{ mix('js/app.js') }}"></script>
@endpush

@section('content')
    <!-- begin::Page loader -->
    <div class="m-page-loader m-page-loader--base">
        <div class="m-blockui">
      <span>
        Please wait...
      </span>
            <span>
        <div class="m-loader m-loader--brand"></div>
      </span>
        </div>
    </div>
    <!-- end::Page Loader -->
    <div id="app" class="m-grid m-grid--hor m-grid--root m-page">
        <app-header></app-header>
        <div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop 	m-container m-container--responsive m-container--xxl m-page__container m-body">
            <div class="m-grid__item m-grid__item--fluid m-wrapper">
                <div class="m-content">
                    <div class="m-portlet">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text">
                                        <a href="{{route('Qrcode::order@index')}}">
                                            <i class="la la-arrow-left"></i>
                                        </a>
                                        Thêm mới đơn hàng
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <form method="POST" enctype="multipart/form-data"
                              action="{{route('Qrcode::order@store') }}"
                              class="m-form m-form--fit m-form--label-align-right">
                            {{ csrf_field() }}
                            <div class="m-portlet__body">
                                <!------------------------- Company-------------------->
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">
                                        Chọn doanh nghiệp *
                                    </label>
                                    <div class="col-lg-4 col-md-9 col-sm-12">
                                        <select name="user_id" class="form-control m-input" id="company"
                                                data-placeholder="Chọn doanh nghiệp" required>
                                            <option value="">--Chọn doanh nghiệp--</option>
                                            @foreach($companies as $company)
                                                <option value="{{$company->id}}" @if(old('user_id')!=null) @if($company->id==old('user_id') ){{ 'selected' }}@endif @endif
                                                >{{$company->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">
                                        Chọn dịch vụ *
                                    </label>
                                    <div class="m-checkbox m-checkbox--state-primary">
                                        <div class="m-checkbox-inline">
                                            @foreach($services as $service)
                                                <label class="m-checkbox">
                                                    <input type="checkbox" id="" name="services[]"
                                                           value="{{$service->id}}"
                                                           class="js-checkbox">{{$service->service_name}}<span></span>
                                                </label>
                                            @endforeach
                                        </div>
                                    </div>

                                    @if ($errors->has('services'))
                                        <div class="help-block"
                                             style="color: red"> <i class="flaticon-alert-1"></i> {{ $errors->first('services') }}</div>
                                    @endif
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">
                                        Số lượng *
                                    </label>
                                    <div class="col-lg-4 col-md-9 col-sm-12">
                                        <input type="number" id="quantity" name="quantity"
                                               class="form-control" min="1"
                                               value="{{ old('quantity')?:""}}" required/></div>
                                    @if ($errors->has('quantity'))
                                        <div class="form-control-feedback">
                                            <i class="icon-notification2"></i>
                                        </div>
                                        <div class="help-block">{{ $errors->first('quantity') }}</div>
                                    @endif
                                </div>
                                <div class="m-portlet__foot m-portlet__foot--fit">
                                    <div class="m-form__actions m-form__actions">
                                        <div class="row">
                                            <div class="col-lg-9 ml-lg-auto">
                                                <button type="submit" class="btn btn-success">
                                                    Thêm mới
                                                </button>
                                                <button type="reset" class="btn btn-secondary">
                                                    Nhập lại
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>

                        <!-- /main content -->
                    </div>
                    <!-- /page content -->
                </div>
            </div>

        </div>
    </div>
    {{--<router-view></router-view>--}}
@endsection

@push('scripts')
    <script type="text/javascript" src="{{ asset('qr_code/metronic/js/vendors.bundle.js') }}"></script>
    <script type="text/javascript" src="{{ asset('qr_code/metronic/js/scripts.bundle.js') }}"></script>
    <script>
        $(document).ready(function () {
            // Basic
            $("#company").select2();

        });
    </script>
@endpush