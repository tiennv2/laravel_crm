@extends('qrcode.layouts.app')
@section('body_class', 'm-page--fluid m-page--loading-enabled m-page--loading m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default')

@push('scripts')
    <script type="text/javascript">
        <?php
            $sharedData = [
                'config' => [
                    'csrfToken' => csrf_token(),
                    'logoutUrl' => route('logout'),
                    'isNative' => true,
                ],
                'loggedInAccount' => auth()->check() ? auth()->user() : null,
            ];
            ?>
            window.SHARED_DATA = {!! json_encode($sharedData) !!}
    </script>
    <script src="https://cdn.jsdelivr.net/npm/tinymce@4.7.11/tinymce.min.js" type="text/javascript"></script>
    <script src="{{ mix('js/app.js') }}"></script>
@endpush

@section('content')
    <!-- begin::Page loader -->
    <div class="m-page-loader m-page-loader--base">
        <div class="m-blockui">
      <span>
        Please wait...
      </span>
            <span>
        <div class="m-loader m-loader--brand"></div>
      </span>
        </div>
    </div>
    <!-- end::page Loader -->
    <div id="app" class="m-grid m-grid--hor m-grid--root m-page">
        <app-header></app-header>
        <div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop  m-body m-page__container">
            <div class="m-grid__item m-grid__item--fluid m-wrapper">
                <!-- BEGIN: Subheader -->
                <div class="m-subheader">
                    <div class="d-flex align-items-center">
                        <div class="mr-auto">
                            <h3 class="m-subheader__title">
                                QUẢN LÝ FILE THIẾT KẾ
                            </h3>
                        </div>
                        @can("QRCODE-addTemplate")
                            <div style="float: right"><a href="{{route('Qrcode::template@add')}}"
                                                         class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air"><span>
                                    <i class="la la-plus-circle"></i>
                                    <span>Thêm file thiết kế</span></span></a>
                            </div>
                        @endcan
                    </div>
                </div>
                <!-- END: Subheader -->
                @can("QRCODE-viewTemplate")
                    <div class="m-content">
                        <!--Begin::Section-->
                        <div class="m-portlet m-portlet--mobile">
                            <div class="m-portlet__body">
                                <form class="m-form m-form--fit m--margin-bottom-20"
                                      action="{{route('Qrcode::template@index')}}" method="get">
                                    <div class="row m--margin-bottom-20">
                                        <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                                            <label>
                                                Tìm kiếm theo tên thiết kế:
                                            </label>
                                            <input type="text" class="form-control m-input" name="name"
                                                   value="{{ Request::input('name') }}" placeholder="Nhập tên thiết kế"
                                                   data-col-index="0">
                                        </div>
                                        <div class="col-lg-4 m--margin-bottom-10-tablet-and-mobile">
                                            <label>
                                                Tìm kiếm theo khách hàng:
                                            </label>
                                            <select class="form-control m-input" data-col-index="2" id="account_id"
                                                    name="account_id">
                                                <option value="" selected="selected"></option>
                                                @foreach($accounts as $account)
                                                    <option value="{{$account->id}}"
                                                            {{Request::input('account_id')==$account->id?'selected="selected"':''}}>{{$account->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile"
                                             style="padding-top: 25px">
                                            <button class="btn btn-brand m-btn m-btn--icon" type="submit">
												<span>
													<i class="la la-search"></i>
													<span>
														Tìm kiếm
													</span>
												</span>
                                            </button>
                                            &nbsp;&nbsp;
                                            <button class="btn btn-secondary m-btn m-btn--icon" type="button"
                                                    onclick="window.location='{{route('Qrcode::template@index')}}'">
												<span>
													<i class="la la-refresh"></i>
													<span>
														Làm mới
													</span>
												</span>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                                @if (session('success'))
                                    <div class="m-alert m-alert--outline m-alert--square m-alert--outline-2x alert alert-success alert-dismissible fade show"
                                         role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        </button>
                                        {{ session('success') }}
                                    </div>
                                @endif
                                <div class="table-responsive">
                                    <table class="table table-striped- table-bordered table-hover table-checkable"
                                           style="margin-top: 50px; width:1500px;margin-left:auto;margin-right:auto;">
                                        <!--begin::Thead-->
                                        <thead>
                                        <tr>
                                            <th style="font-weight: bold">Id</th>
                                            <th style="width: 200px;font-weight: bold">Tên thiết kế</th>
                                            <th style="width: 400px;font-weight: bold">Files</th>
                                            <th style="font-weight: bold">Tên khách hàng</th>
                                            <th style="font-weight: bold">Ngày tạo</th>
                                            <th style="width: 120px;font-weight: bold">Ngày cập nhật gần nhất</th>
                                            <th style="width: 60px;font-weight: bold">Hành động</th>
                                        </tr>
                                        </thead>
                                        <!--end::Thead-->
                                        <!--begin::Tbody-->
                                        <tbody>
                                        @foreach($templates as $row)
                                            <tr>
                                                <td>{{$row->id}}</td>
                                                <td>
                                                    <a href="#" data-toggle="modal"
                                                       data-target="#batch_show{{$row->id}}"
                                                       onclick="getBatches({{$row->id}})">{{$row->name}}</a>
                                                    <!-- Batch Show Modal-->
                                                    <div id="batch_show{{$row->id}}" class="modal fade test"
                                                         role="dialog">
                                                        <div class="modal-dialog modal-lg" role="document">
                                                            <!-- Modal content-->
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h4>Danh sách các lô tem sử dụng thiết kế
                                                                        "{{$row->name}}"</h4>
                                                                    <button type="button" class="close"
                                                                            data-dismiss="modal">&times;
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body"
                                                                     style="height:400px;overflow-y: scroll;">
                                                                    <table class="table">
                                                                        <thead>
                                                                        <tr>
                                                                            <th style="font-weight: bold">Id</th>
                                                                            <th style="font-weight: bold">Tên lô</th>
                                                                            <th style="font-weight: bold">Số lượng tem
                                                                            </th>
                                                                            <th style="font-weight: bold">Ngày tạo</th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody id="content_modal{{$row->id}}">

                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button"
                                                                            class="btn btn-default"
                                                                            data-dismiss="modal">Hủy bỏ
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--End Modal-->
                                                </td>
                                                <td>
                                                    @if($row->attachments != null)
                                                        @php
                                                            $attachments = explode(',',$row->attachments);
                                                        @endphp
                                                        @foreach( $attachments as $attachment)
                                                            @php
                                                                $attachment_split = explode('/',$attachment);
                                                                $attachment_name = end($attachment_split);
                                                                  $path = $attachment;
                                                                  $types = ['jpg','JPG','png','PNG','gif','GIF'];
                                                                  $attachment_name_split = explode('.',$attachment_name);
                                                            $extension = end($attachment_name_split);
                                                            @endphp
                                                            @if(in_array($extension,$types))
                                                                <span><a href="{{$path}}"><img src="{{$path}}"
                                                                                               width="70px"/></a></span>
                                                            @else
                                                                <span><a href="{{$path}}">{{$attachment_name}}</a></span>
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                </td>
                                                <td>
                                                    @foreach($accounts as $account)
                                                        @if($account->id == $row->account_id)
                                                            {{$account->name}}
                                                        @endif
                                                    @endforeach
                                                </td>
                                                <td>{{date("d/m/Y",strtotime($row->created_at))}}</td>
                                                <td style="text-align: center;">{{date("d/m/Y",strtotime($row->updated_at))}}</td>
                                                <td>
                                                    <div class="m-dropdown m-dropdown--up m-dropdown--inline m-dropdown--small m-dropdown--arrow m-dropdown--align-right"
                                                         m-dropdown-toggle="hover">
                                                        <a class="m-dropdown__toggle btn btn-brand dropdown-toggle">
                                                        </a>
                                                        <div class="m-dropdown__wrapper">
                                                            <div class="m-dropdown__inner">
                                                                <div class="m-dropdown__body">
                                                                    <div class="m-dropdown__content">
                                                                        <ul class="m-nav">
                                                                            @can("QRCODE-updateTemplate")
                                                                                <li class="m-nav__item"
                                                                                    style="margin-bottom: 5px">
                                                                                    <a href="{{route('Qrcode::template@edit',[$row->id])}}"
                                                                                       class="m-nav__link">
                                                                                        <i class="m-nav__link-icon la la-edit"></i>
                                                                                        <span class="m-nav__link-text">Cập nhật</span>
                                                                                    </a>
                                                                                </li>
                                                                            @endcan
                                                                            @can("QRCODE-delTemplate")
                                                                                <li class="m-nav__item"
                                                                                    style="margin-bottom: 5px">
                                                                                    <a href="{{route('Qrcode::template@delete',[$row->id])}}"
                                                                                       class="m-nav__link"
                                                                                       onclick="return delConfirm();">
                                                                                        <i class="m-nav__link-icon la la-remove"></i>
                                                                                        <span class="m-nav__link-text">Xóa bỏ</span>
                                                                                    </a>
                                                                                </li>
                                                                            @endcan
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <span class="m-dropdown__arrow m-dropdown__arrow--right"></span>
                                                        </div>
                                                    </div>
                                                    <!--end: Dropdown-->
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                        <!--end::Tbody-->
                                    </table>
                                </div>
                                <!--end::Table-->
                                <div style="float:right;"><?php echo $templates->links(); ?></div>
                            </div>
                            <!--End::Section-->
                        </div>
                    </div>
                @endcan
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript" src="{{ asset('qr_code/metronic/js/vendors.bundle.js') }}"></script>
    <script type="text/javascript" src="{{ asset('qr_code/metronic/js/datatables/datatables.bundle.js') }}"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            // Basic
            $("#city").select2({
                placeholder: 'Tỉnh/thành phố',
                allowClear: true
            });
            $("#account_id").select2({
                placeholder: 'Chọn tên khách hàng',
                allowClear: true
            });
            $("select").on("select2:select", function (evt) {
                var element = evt.params.data.element;
                var $element = $(element);
                $element.detach();
                $(this).append($element);
                $(this).trigger("change");
            });


        });
    </script>

    <script>
        function getBatches(template_id) {
            var url = "{{ route('Qrcode::template@getBatchesByTemplate')}}";
            var token = $("input[name='_token']").val();
            $.ajax({
                url: url,
                method: 'GET',
                data: {
                    template_id: template_id,
                    _token: token
                },
                success: function (data) {
                    $(`#content_modal${template_id}`).html('');
                    $.each(data, function (key, value) {
                        $(`#content_modal${template_id}`).append(
                            "<tr>" +
                            "<td>" + value.id + "</td>" +
                            "<td>" + value.name + "</td>" +
                            "<td>" + value.quantity + "</td>" +
                            "<td>" + value.created_time + "</td>" +
                            "</tr>"
                        );
                    });
                }
            });
        }
    </script>
    <script>
        function delConfirm() {
            var conf = confirm("Bạn chắc chắn muốn xoá?");
            return conf;
        }

    </script>
@endpush
