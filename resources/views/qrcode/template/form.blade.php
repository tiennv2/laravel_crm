@extends('qrcode.layouts.app')
@section('body_class', 'm-page--fluid m-page--loading-enabled m-page--loading m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default')
@push('scripts')
    <script type="text/javascript">
        <?php
            $sharedData = [
                'config' => [
                    'csrfToken' => csrf_token(),
                    'logoutUrl' => route('logout'),
                    'isNative' => true,
                ],
                'loggedInAccount' => auth()->check() ? auth()->user() : null,
            ];
            ?>
            window.SHARED_DATA = {!! json_encode($sharedData) !!}
    </script>
    <script src="https://cdn.jsdelivr.net/npm/tinymce@4.7.11/tinymce.min.js" type="text/javascript"></script>
    <script src="{{ mix('js/app.js') }}"></script>
@endpush

@section('content')
    <!-- begin::Page loader -->
    <div class="m-page-loader m-page-loader--base">
        <div class="m-blockui">
      <span>
        Please wait...
      </span>
            <span>
        <div class="m-loader m-loader--brand"></div>
      </span>
        </div>
    </div>
    <!-- end::Page Loader -->
    <div id="app" class="m-grid m-grid--hor m-grid--root m-page">
        <app-header></app-header>
        <div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop 	m-container m-container--responsive m-container--xxl m-page__container m-body">
            <div class="m-grid__item m-grid__item--fluid m-wrapper">
                <div class="m-content">
                    <div class="m-portlet">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text">
                                        <a href="{{route('Qrcode::template@index')}}">
                                            <i class="la la-arrow-left"></i>
                                        </a>
                                        {{ isset($template) ? 'Cập nhật file thiết kế' : 'Thêm mới file thiết kế' }}
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <!-- /page header -->
                        @if (session('success'))
                            <div class="m-alert m-alert--outline m-alert--square m-alert--outline-2x alert alert-success alert-dismissible fade show"
                                 role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                </button>
                                {{ session('success') }}
                            </div>
                        @endif
                        <form method="POST" enctype="multipart/form-data" name="form"
                              class="m-form m-form--fit m-form--label-align-right" onsubmit=" return check()"
                              action="{{ isset($template) ? route('Qrcode::template@update', [$template->id] ): route('Qrcode::template@store') }}">
                            {{ csrf_field() }}
                            @if (isset($template))
                                <input type="hidden" name="_method" value="PUT">
                            @endif
                            <div class="m-portlet__body">
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">Tên thiết kế *</label>
                                    <div class="col-lg-6 col-md-9 col-sm-12">
                                        <input type="text" id="name" name="name"
                                               class="form-control" value="{{ old('name') ?: @$template->name }}" required/>
                                    </div>
                                    @if ($errors->has('name'))
                                        <div class="form-control-feedback">
                                            <i class="icon-notification2"></i>
                                        </div>
                                        <div class="help-block"
                                             style="color: red">{{ $errors->first('name') }}</div>
                                    @endif
                                </div>
                                <!------------------------- Company -------------------->
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">Chọn khách hàng *</label>
                                    <div class="col-lg-6 col-md-9 col-sm-12">
                                        <select name="account_id" class="form-control js-select-1"
                                                data-placeholder="Chọn khách hàng">
                                            <option value="" selected="selected">--Chọn--</option>
                                            @foreach($accounts as $account)
                                                <option value="{{$account->id}}" {{(isset($template) and ($account->id==$template->account_id))?'selected="selected"' :''}}>{{$account->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @if ($errors->has('account_id'))
                                        <div class="form-control-feedback">
                                            <i class="icon-notification2"></i>
                                        </div>
                                        <div class="help-block"
                                             style="color: red">{{ $errors->first('account_id') }}</div>
                                    @endif
                                </div>
                                <!------------------------- Batch -------------------->
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">Chọn lô tem </label>
                                    <div class="col-lg-6 col-md-9 col-sm-12">
                                        <select class="form-control js-select-2"
                                                name="batch_id[]" multiple="multiple">
                                            @if(isset($template))
                                                @foreach($batches as $batch)
                                                    <option value="{{$batch->id}}" {{(in_array($batch->id,$template_batches))?'selected="selected"' :''}}>{{$batch->name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                    @if ($errors->has('batch_id'))
                                        <div class="form-control-feedback">
                                            <i class="icon-notification2"></i>
                                        </div>
                                        <div class="help-block"
                                             style="color: red">{{ $errors->first('account_id') }}</div>
                                    @endif
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">Chọn files *</label>
                                    <div class="col-lg-6 col-md-9 col-sm-12">
                                        <input type="file" id="file" name="attachments[]" multiple="multiple"
                                               class="form-control"/>
                                        @if ($errors->has("attachments.0")||$errors->has("attachments.1")||$errors->has("attachments.2")||$errors->has("attachments.3")||$errors->has("attachments.4"))
                                            <div class="form-control-feedback">
                                                <i class="icon-notification2" style="color: red"></i>
                                            </div>
                                            <div class="help-block"
                                                 style="color: red">
                                                {{"Sai định dạng file upload hoặc có một file upload dung lượng quá 5 MB. Vui lòng kiểm tra lại"}}</div>
                                        @elseif($errors->has("attachments_over"))
                                            <div class="form-control-feedback">
                                                <i class="icon-notification2" style="color: red"></i>
                                            </div>
                                            <div class="help-block"
                                                 style="color: red">
                                                {{ $errors->first('attachments_over')}}</div>
                                        @endif
                                        @if(isset($template))
                                            <input type="hidden" name="remove_files" id="remove_files" value="">
                                            @php
                                                echo "<br>";

                                            @endphp
                                            <div id='file'>
                                                <strong>
                                                    Các files đã upload
                                                </strong>
                                                @php
                                                    $a = $template->attachments;
                                                    $attachments = explode(',',$a);
                                                @endphp
                                                @if($attachments[0]!='')
                                                    @foreach ($attachments as $attachment)
                                                        <div id="{{$attachment}}">
                                                            @php
                                                                $attachment_split = explode('/',$attachment);
                                                                $attachment_name = end($attachment_split);
                                                            @endphp
                                                            @if($attachment_name!='')
                                                                <a href="{{$attachment}}">{{$attachment_name}}</a>
                                                                <a class="btn" href="javascript:;"><i
                                                                            class="la la-trash"
                                                                            onclick="del('{{$attachment}}')"></i></a>
                                                        </div>
                                                        @endif
                                                    @endforeach
                                                @else
                                                    <div>Chưa có</div>
                                                @endif
                                            </div>
                                        @endif
                                        <span style="font-style: italic">Giữ phím Ctrl để chọn nhiều file (tối đa không quá 5 files).
                                        Mỗi file không quá 5MB và File upload phải là một tập tin có định dạng: jpg, jpeg, gif, png, ai,zip,rar</span>
                                    </div>
                                </div>

                                <div class="m-portlet__foot m-portlet__foot--fit">
                                    <div class="m-form__actions m-form__actions">
                                        <div class="row">
                                            <div class="col-lg-9 ml-lg-auto">
                                                <button type="submit" class="btn btn-success">
                                                    {{ isset($template) ? 'Cập nhật' : 'Thêm mới' }}
                                                </button>
                                                <button type="reset" class="btn btn-secondary">
                                                    Nhập lại
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <router-view></router-view>
@endsection

@push('scripts')
    <script type="text/javascript" src="{{ asset('qr_code/metronic/js/vendors.bundle.js') }}"></script>
    <script type="text/javascript" src="{{ asset('qr_code/metronic/js/scripts.bundle.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('.js-select-1').select2(
                {
                    placeholder: 'Chọn khách hàng',
                    allowClear: true
                }
            );
            $('.js-select-2').select2(
                {
                    placeholder: 'Chọn lô tem',
                    allowClear: true
                }
            );

        });
    </script>

    <script type="text/javascript">
        var url = "{{ route('Qrcode::template@getBatchesByAccount')}}";
        $("select[name='account_id']").change(function () {
            var account_id = $(this).val();
            var token = $("input[name='_token']").val();
            $.ajax({
                url: url,
                method: 'GET',
                data: {
                    account_id: account_id,
                    _token: token
                },
                success: function (data) {
                    $("select[name='batch_id[]']").html('');
                    $.each(data, function (key, value) {
                        $("select[name='batch_id[]']").append(
                            "<option value=" + value.id + ">" + value.name + "</option>"
                        );
                    });
                }
            });
        });
    </script>
    <script>
        function del(i) {
            document.getElementById(i).style.display = "none";
            var x = document.getElementById("remove_files").value;

            if (x === '') {
                document.getElementById("remove_files").value = x.concat("", i);
            }
            else {
                document.getElementById("remove_files").value = x.concat(",", i);
            }

            var y = document.getElementById("remove_files").value;


        }

        function check() {
            var $fileUpload = $("input[type='file']");
            if (parseInt($fileUpload.get(0).files.length) > 5) {
                alert("Không được upload quá 5 files");
                return false;
            }
            return true;
        }
    </script>
@endpush
