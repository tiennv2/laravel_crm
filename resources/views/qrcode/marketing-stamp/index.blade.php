@extends('qrcode.layouts.app')

@section('body_class', 'm-page--fluid m-page--loading-enabled m-page--loading m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default')

@push('scripts')
  <script type="text/javascript">
    <?php
      $sharedData = [
        'config' => [
          'csrfToken' => csrf_token(),
          'logoutUrl' => route('logout'),
          'isNative' => true,
        ],
        'loggedInAccount' => auth()->check() ? auth()->user() : null,
      ];
      ?>
      window.SHARED_DATA = {!! json_encode($sharedData) !!}
  </script>
  <script src="https://cdn.jsdelivr.net/npm/tinymce@4.7.11/tinymce.min.js" type="text/javascript"></script>
  <script src="{{ mix('js/app.js') }}"></script>
@endpush

@section('content')
  <!-- begin::Page loader -->
  <div class="m-page-loader m-page-loader--base">
    <div class="m-blockui">
      <span>
        Please wait...
      </span>
      <span>
        <div class="m-loader m-loader--brand"></div>
      </span>
    </div>
  </div>
  <!-- end::Page Loader -->
  <div id="app" class="m-grid m-grid--hor m-grid--root m-page">
    <div class="m-header__bottom">
      <div class="m-container m-container--responsive m-container--xxl m-container--full-height m-page__container">
        <div class="m-stack m-stack--ver m-stack--desktop">
          <div class="m-stack__item m-stack__item--middle m-stack__item--fluid">
            <app-header></app-header>
          </div>
        </div>
      </div>
    </div>

    <div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop  m-body m-page__container">
      <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader ">
          <div class="d-flex align-items-center">
            <div class="mr-auto">
              <h3 class="m-subheader__title ">
                QUẢN LÝ TEM MARKETING
              </h3>
            </div>
          </div>
        </div>
        <!-- END: Subheader -->
        @can("QRCODE-viewOrder")
          <div class="m-content">
            <!--Begin::Section-->
            <div class="m-portlet m-portlet--mobile">
              <div class="m-portlet__body">
                <form class="m-form m-form--fit m--margin-bottom-20"
                      action="{{route('Qrcode::marketingStamp@index')}}" method="get">
                  <!-- Search Field -->
                  <div class="row m--margin-bottom-20">
                    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                      <label>
                        Tìm theo Doanh nghiệp:
                      </label>
                      <select class="form-control" id="account_id" name="account_id">
                        <option value=""></option>
                        @foreach($accounts as $account)
                          <option value="{{$account->id}}"
                            {{Request::input('account_id')==$account->id?'selected="selected"':''}}>{{$account->name}}</option>
                        @endforeach
                      </select>
                    </div>
                    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                      <label>
                        Tìm theo tên tem:
                      </label>
                      <input class="form-control" type="text" name="name"
                             placeholder="Tìm theo tên tem"
                             value="{{ Request::input('name') }}"/>
                    </div>
                    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                      <label>
                        Tìm theo tên Sản phẩm:
                      </label>
                      <input class="form-control" type="text" name="product_name"
                             placeholder="Tìm theo tên sản phẩm"
                             value="{{ Request::input('product_name') }}"/>
                    </div>
                    <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile">
                      <label>
                        Trạng thái:
                      </label>
                      <select class="form-control" id="status" name="status">
                        <option value="">Chọn trạng thái</option>
                        <option value="pending" {{Request::input('status')== 'pending'?'selected="selected"':''}}>
                          Chờ duyệt
                        </option>
                        <option
                          value="request_expired" {{Request::input('status')== 'request_expired'?'selected="selected"':''}}>
                          Chờ gia hạn
                        </option>
                        <option value="approved" {{Request::input('status')== 'approved'?'selected="selected"':''}}>
                          Đã duyệt
                        </option>
                        <option value="reject" {{Request::input('status')== 'reject'?'selected="selected"':''}}>
                          Hủy duyệt
                        </option>
                        <option value="expired" {{Request::input('status')== 'expired'?'selected="selected"':''}}>
                          Hết hạn
                        </option>
                      </select>
                    </div>
                  </div>

                  <div class="row m--margin-bottom-20">
                    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                      <label>
                        Ngày kích hoạt:
                      </label>
                      <div class="input-daterange input-group">
                        <input type="text" class="form-control" name="active_from"
                               id="m_datepicker_1"
                               value="{{ Request::input('active_from') }}" autocomplete="off"
                               placeholder="Từ"
                               data-col-index="5"/>
                        <div class="input-group-append">
													<span class="input-group-text">
														<i class="la la-ellipsis-h"></i>
													</span>
                        </div>
                        <input type="text" class="form-control" name="active_to"
                               id="m_datepicker_1"
                               value="{{ Request::input('active_to') }}" autocomplete="off"
                               placeholder="Đến"
                               data-col-index="5"/>
                      </div>
                    </div>
                    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                      <label>
                        Ngày hết hạn:
                      </label>
                      <div class="input-daterange input-group">
                        <input type="text" class="form-control" name="expired_from"
                               id="m_datepicker_1"
                               value="{{ Request::input('expired_from') }}" autocomplete="off"
                               placeholder="Từ"
                               data-col-index="5"/>
                        <div class="input-group-append">
													<span class="input-group-text">
														<i class="la la-ellipsis-h"></i>
													</span>
                        </div>
                        <input type="text" class="form-control" name="expired_to"
                               id="m_datepicker_1"
                               value="{{ Request::input('expired_to') }}" autocomplete="off"
                               placeholder="Đến"
                               data-col-index="5"/>
                      </div>
                    </div>
                    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                      <label style="color: white">
                        Hành động:
                      </label>
                      <div>
                        <button class="btn btn-brand m-btn m-btn--icon" type="submit">
												<span>
													<i class="la la-search"></i>
													<span>
														Tìm kiếm
													</span>
												</span>
                        </button>
                        &nbsp;&nbsp;
                        <button class="btn btn-secondary m-btn m-btn--icon" type="button"
                                onclick="window.location='{{route('Qrcode::marketingStamp@index')}}'">
												<span>
													<i class="la la-refresh"></i>
													<span>
														Làm mới
													</span>
												</span>
                        </button>
                      </div>
                    </div>
                  </div>
                </form>
                @if (session('success'))
                  <div
                    class="m-alert m-alert--outline m-alert--square m-alert--outline-2x alert alert-success alert-dismissible fade show"
                    role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    </button>
                    {{ session('success') }}
                  </div>
                @endif
                <div
                  style="width:150px;text-align:center;margin-bottom: 0;margin-top: 50px;font-weight: bold;font-style: italic;border: solid">
                  Có tổng số {{$count_record}} bản ghi
                </div>
                <!--begin::Table-->
                <div class="table-responsive">
                  <table class="table table-striped- table-bordered table-hover table-checkable"
                         style="width: 2500px">
                    <thead>
                    <tr>
                      <th style="font-weight: bold">Id</th>
                      <th style="font-weight: bold;width:300px;">Tên Doanh nghiệp</th>
                      <th style="font-weight: bold;width:200px;">Tên tem</th>
                      <th style="font-weight: bold;width:280px;">Tên thiết kế</th>
                      <th style="font-weight: bold;width:300px;">Tên Sản phẩm</th>
                      <th style="font-weight: bold;width:150px;">Code</th>
                      <th style="font-weight: bold;width:300px;">Tin nhắn</th>
                      <th style="font-weight: bold">Hiển thị thông tin Sản phẩm</th>
                      <th style="font-weight: bold">Hiển thị NSX</th>
                      <th style="font-weight: bold">Hiển thị Sản phẩm liên quan</th>
                      <th style="font-weight: bold">Hiển thị Điểm bán</th>
                      <th style="font-weight: bold">Hiển thị Thông báo</th>
                      <th style="font-weight: bold">Hiển thị giá</th>
                      <th style="font-weight: bold">Hiển thị giới thiệu Sản phẩm</th>
                      <th style="font-weight: bold;width:200px;">Trạng thái</th>
                      <th style="font-weight: bold">Ngày kích hoạt</th>
                      <th style="font-weight: bold">Ngày hết hạn</th>
                      <th style="font-weight: bold;width:100px;">Thời hạn muốn gia hạn</th>
                      <th style="font-weight: bold;width:250px;text-align: center">Hành động</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($static_stamps as $row)
                      <tr role="row">
                        <td class="id">{{$row->id}}</td>
                        <td>{{$row->account_name}}</td>
                        <td>
                          @if($row->type == "link")
                            <a href="{{$row->name}}" target="_blank">{{$row->name}}</a>
                          @else
                            {{$row->name}}
                          @endif
                        </td>
                        <td>
                          <a href="{{$row->template_image}}" target="_blank">
                            {{$row->template_name}}
                          </a>
                        </td>
                        <td>
                          <a href="{{route('Qrcode::product@get_upload_product_file',[$row->product_id])}}"
                             target="_blank">{{$row->product_name}}</a>
                        </td>
                        <td>{{$row->code}}</td>
                        <td>{{$row->message}}</td>
                        <td>
                          @if($row->type == "product")
                            <span class="m-switch m-switch--icon">
                                                        <label>
                                                            <input type="checkbox"
                                                                   @if($row->show_product == 1) checked="checked"
                                                                   @endif name="show_product"
                                                                   @cannot("QRCODE-modifyMarketingStampProperty")
                                                                   disabled
                                                                   @endcannot
                                                                   value="{{$row->show_product}}">
                                                            <span></span>
                                                        </label>
                                                    </span>
                          @endif
                        </td>
                        <td>
                          @if($row->type == "product")
                            <span class=" m-switch m-switch--icon">
                                                        <label>
                                                            <input type="checkbox"
                                                                   @if($row->show_vendor == 1) checked="checked"
                                                                   @endif name="show_vendor"
                                                                   @cannot("QRCODE-modifyMarketingStampProperty")
                                                                   disabled
                                                                   @endcannot
                                                                   value="{{$row->show_vendor}}">
                                                            <span></span>
                                                        </label>
                                                    </span>
                          @endif
                        </td>
                        <td>
                          @if($row->type == "product")
                            <span class="m-switch m-switch--icon">
                                                        <label>
                                                            <input type="checkbox"
                                                                   @if($row->show_related == 1) checked="checked"
                                                                   @endif name="show_related"
                                                                   @cannot("QRCODE-modifyMarketingStampProperty")
                                                                   disabled
                                                                   @endcannot
                                                                   value="{{$row->show_related}}">
                                                            <span></span>
                                                        </label>
                                                    </span>
                          @endif
                        </td>
                        <td>
                          @if($row->type == "product")
                            <span class="m-switch m-switch--icon">
                                                        <label>
                                                            <input type="checkbox"
                                                                   @if($row->show_store == 1) checked="checked"
                                                                   @endif name="show_store"
                                                                   @cannot("QRCODE-modifyMarketingStampProperty")
                                                                   disabled
                                                                   @endcannot
                                                                   value="{{$row->show_store}}">
                                                            <span></span>
                                                        </label>
                                                    </span>
                          @endif
                        </td>
                        <td>
                          @if($row->type == "product")
                            <span class="m-switch m-switch--icon">
                                                        <label>
                                                            <input type="checkbox"
                                                                   @if($row->show_message == 1) checked="checked"
                                                                   @endif name="show_message"
                                                                   @cannot("QRCODE-modifyMarketingStampProperty")
                                                                   disabled
                                                                   @endcannot
                                                                   value="{{$row->show_message}}">
                                                            <span></span>
                                                        </label>
                                                    </span>
                          @endif
                        </td>
                        <td>
                          @if($row->type == "product")
                            <span class="m-switch m-switch--icon">
                                                        <label>
                                                            <input type="checkbox"
                                                                   @if($row->show_price == 1) checked="checked"
                                                                   @endif name="show_price"
                                                                   @cannot("QRCODE-modifyMarketingStampProperty")
                                                                   disabled
                                                                   @endcannot
                                                                   value="{{$row->show_price}}">
                                                            <span></span>
                                                        </label>
                                                    </span>
                          @endif
                        </td>
                        <td>
                          @if($row->type == "product")
                            <span class="m-switch m-switch--icon">
                                                        <label>
                                                            <input type="checkbox"
                                                                   @if($row->show_info == 1) checked="checked"
                                                                   @endif name="show_info"
                                                                   @cannot("QRCODE-modifyMarketingStampProperty")
                                                                   disabled
                                                                   @endcannot
                                                                   value="{{$row->show_info}}">
                                                            <span></span>
                                                        </label>
                                                    </span>
                          @endif
                        </td>
                        <td>
                          @if($row->status=="approved")
                            <span class="m-badge m-badge--success m-badge--wide">
									Đã duyệt </span>
                          @elseif($row->status=="reject")
                            <span class="m-badge m-badge--danger m-badge--wide">
									Đã hủy duyệt </span>
                          @elseif($row->status=="pending")
                            <span class="m-badge m-badge--warning m-badge--wide">
									Chờ duyệt </span>
                          @elseif($row->status=="expired")
                            <span class="m-badge m-badge--danger m-badge--wide">
									Hết hạn </span>
                          @elseif($row->status=="deactive")
                            <span class="m-badge m-badge--danger m-badge--wide">
									Ngừng kích hoạt </span>
                          @endif
                          @if($row->request_expired)
                            <div class="request_expired_div">
                                                        <span class="m-badge m-badge--warning m-badge--wide">
									Chờ gia hạn </span><span style="cursor: pointer" title="Xóa gia hạn"
                                           class="delete_request_expired m-badge m-badge--danger m-badge--wide">
									X </span></div>
                          @endif
                        </td>
                        <td>
                          @if($row->active_time)
                            {{--{{date("d/m/Y",strtotime($row->active_time))}}--}}
                            <input type="date" class="form-control" name="active_time"
                                   disabled
                                   value="{{date("Y-m-d",strtotime($row->active_time))}}"
                                   autocomplete="off"/>
                          @endif
                        </td>
                        <td>
                          {{--@if($row->expired_time)--}}
                          {{--{{date("d/m/Y",strtotime($row->expired_time))}}--}}
                          {{--@endif--}}
                          @if($row->expired_time)
                            <input type="date" class="form-control" name="expired_time"
                                   id="expired_time_{{$row->id}}"
                                   value="{{date("Y-m-d",strtotime($row->expired_time))}}"
                                   @if(auth()->user()->cannot('QRCODE-updateExpiredTimeMarketingStamp') || $row->status=="expired")
                                   disabled
                                   @endif
                                   autocomplete="off"/>
                          @endif
                        </td>
                        <td class="request_expired">
                          @if($row->request_expired)
                            {{$row->request_expired}} năm
                          @endif
                        </td>
                        <td>
                          @if($row->status == "expired")
                            @can("QRCODE-updateExpiredTimeMarketingStamp")
                              <a data-toggle="modal" data-target="#expandExpire{{$row->id}}"
                                 title="Gia hạn"
                                 class="btn btn-info m-btn m-btn--icon m-btn--icon-only m-btn--pill">
                                <i class="la la-expand"></i>
                              </a>
                            @endcan
                          @endif
                          @if(!$row->expired && $row->status != "approved" || !$row->expired && $row->request_expired || $row->expired && $row->request_expired)
                            @can("QRCODE-approveMarketingStamp")
                              <a href="{{route('Qrcode::marketingStamp@approve',[$row->id])}}"
                                 title="Duyệt"
                                 class="approve btn btn-success m-btn m-btn--icon m-btn--icon-only m-btn--pill">
                                <i class="la la-check"></i>
                              </a>
                            @endcan
                          @endif
                          @if(!$row->expired && $row->status != "disapproved"|| $row->expired && $row->request_expired)
                            @can("QRCODE-disapproveMarketingStamp")
                              <a href="{{route('Qrcode::marketingStamp@disapprove',[$row->id])}}"
                                 title="Hủy duyệt"
                                 class="btn btn-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill">
                                <i class="la la-close"></i>
                              </a>
                            @endcan
                          @endif
                          @can("QRCODE-delMarketingStamp")
                            <button
                              title="Xóa bỏ"
                              type="button"
                              class="delete_stamp btn btn-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill">
                              <i class="fa fa-trash-o"></i>
                            </button>
                        @endcan
                        <!--Modal expand expire-->
                          <div class="modal fade bs-modal-lg" id="expandExpire{{$row->id}}"
                               tabindex="-1"
                               role="dialog"
                               aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                              <div class="modal-content">
                                <form class="form-horizontal" id="modal" role="form"
                                      method="post"
                                      action="{{route('Qrcode::marketingStamp@expandExpire',[$row->id])}}">
                                  {{ csrf_field() }}
                                  <div class="modal-header">
                                    <h2>Gia hạn tem </h2>
                                    <button type="button" class="close"
                                            data-dismiss="modal"
                                            aria-hidden="true"></button>
                                  </div>
                                  <div class="modal-body">
                                    <div class="portlet box green ">
                                      <div class="portlet-body form">
                                        <div class="form-body">
                                          <div class="form-group m-form__group row">
                                            <label class="col-form-label col-lg-3 col-sm-12"><strong>Nhập số năm muốn
                                                gia
                                                hạn</strong></label>
                                            <div class="col-lg-3 col-md-9 col-sm-12">
                                              <input type="number"
                                                     name="request_expired"
                                                     min="1"
                                                     class="form-control"
                                                     placeholder="Nhập số năm"
                                                     required>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="submit"
                                            class="btn btn-success">
                                      Cập nhật
                                    </button>
                                    <button type="button" class="btn default"
                                            data-dismiss="modal">
                                      Hủy
                                    </button>
                                  </div>
                                </form>
                              </div>
                            </div>
                          </div>
                          <!--End Modal-->
                        </td>
                    @endforeach
                    </tbody>
                  </table>
                </div>
                <!--end::Table-->
                <div style="float:right;"><?php echo $static_stamps->links(); ?></div>
              </div>
            </div>
          </div>
        @endcan
      </div>
    </div>
  </div>
  <router-view></router-view>
@endsection
@push('scripts')
  <script type="text/javascript" src="{{ asset('qr_code/metronic/js/vendors.bundle.js') }}"></script>
  <script type="text/javascript" src="{{ asset('qr_code/metronic/js/scripts.bundle.js') }}"></script>
  <script type="text/javascript" src="{{ asset('qr_code/metronic/js/datepicker/bootstrap-datepicker.js') }}"></script>

  <script>
    $(document).ready(function () {
      // Basic
      $("#status").select2({
        placeholder: 'Chọn trạng thái',
        allowClear: true
      });
      $("#account_id").select2({
        placeholder: 'Chọn Doanh nghiệp',
        allowClear: true
      });

      $("select").on("select2:select", function (evt) {
        var element = evt.params.data.element;
        var $element = $(element);
        $element.detach();
        $(this).append($element);
        $(this).trigger("change");
      });
    });
    $(".modal").on('hidden.bs.modal', function () {
      document.body.style.padding = 0;
    });

    function delItem() {
      let conf = confirm("Bạn chắc chắn muốn xoá không?");
      return conf;
    }

    $('input[name="show_product"]').click(function () {
      let status = $(this).is(':checked');
      let row = $(this).closest("tr");    // Find the row
      let id = row.find(".id").text(); // Find the text
      var url = "";
      if (status) {
        url = "{{ route('Qrcode::marketingStamp@activeShowProduct',':id')}}";
      } else {
        url = "{{ route('Qrcode::marketingStamp@deactiveShowProduct',':id')}}";
      }
      url = url.replace(':id', id);
      $.ajax({
        url: url,
        method: 'GET',
      });
    });
    $('input[name="show_vendor"]').click(function () {
      let status = $(this).is(':checked');
      let row = $(this).closest("tr");    // Find the row
      let id = row.find(".id").text(); // Find the text
      var url = "";
      if (status) {
        url = "{{ route('Qrcode::marketingStamp@activeShowVendor',':id')}}";
      } else {
        url = "{{ route('Qrcode::marketingStamp@deactiveShowVendor',':id')}}";
      }
      url = url.replace(':id', id);
      $.ajax({
        url: url,
        method: 'GET',
      });
    });
    $('input[name="show_related"]').click(function () {
      let status = $(this).is(':checked');
      let row = $(this).closest("tr");    // Find the row
      let id = row.find(".id").text(); // Find the text
      var url = "";
      if (status) {
        url = "{{ route('Qrcode::marketingStamp@activeShowRelated',':id')}}";
      } else {
        url = "{{ route('Qrcode::marketingStamp@deactiveShowRelated',':id')}}";
      }
      url = url.replace(':id', id);
      $.ajax({
        url: url,
        method: 'GET',
      });
    });
    $('input[name="show_store"]').click(function () {
      let status = $(this).is(':checked');
      let row = $(this).closest("tr");    // Find the row
      let id = row.find(".id").text(); // Find the text
      var url = "";
      if (status) {
        url = "{{ route('Qrcode::marketingStamp@activeShowStore',':id')}}";
      } else {
        url = "{{ route('Qrcode::marketingStamp@deactiveShowStore',':id')}}";
      }
      url = url.replace(':id', id);
      $.ajax({
        url: url,
        method: 'GET',
      });
    });
    $('input[name="show_message"]').click(function () {
      let status = $(this).is(':checked');
      let row = $(this).closest("tr");    // Find the row
      let id = row.find(".id").text(); // Find the text
      var url = "";
      if (status) {
        url = "{{ route('Qrcode::marketingStamp@activeShowMessage',':id')}}";
      } else {
        url = "{{ route('Qrcode::marketingStamp@deactiveShowMessage',':id')}}";
      }
      url = url.replace(':id', id);
      $.ajax({
        url: url,
        method: 'GET',
      });
    });
    $('input[name="show_price"]').click(function () {
      let status = $(this).is(':checked');
      let row = $(this).closest("tr");    // Find the row
      let id = row.find(".id").text(); // Find the text
      var url = "";
      if (status) {
        url = "{{ route('Qrcode::marketingStamp@activeShowPrice',':id')}}";
      } else {
        url = "{{ route('Qrcode::marketingStamp@deactiveShowPrice',':id')}}";
      }
      url = url.replace(':id', id);
      $.ajax({
        url: url,
        method: 'GET',
      });
    });
    $('input[name="show_info"]').click(function () {
      let status = $(this).is(':checked');
      let row = $(this).closest("tr");    // Find the row
      let id = row.find(".id").text(); // Find the text
      var url = "";
      if (status) {
        url = "{{ route('Qrcode::marketingStamp@activeShowInfo',':id')}}";
      } else {
        url = "{{ route('Qrcode::marketingStamp@deactiveShowInfo',':id')}}";
      }
      url = url.replace(':id', id);
      $.ajax({
        url: url,
        method: 'GET',
      });
    });
    $('.delete_request_expired').click(function () {
      let row = $(this).closest("tr");    // Find the row
      let id = row.find(".id").text(); // Find the text
      var url = "{{ route('Qrcode::marketingStamp@delRequestExpired',':id')}}";
      url = url.replace(':id', id);
      $.ajax({
        url: url,
        method: 'GET',
        success: function (data) {
          let request_expired = parseInt(data);
          if (request_expired === 0) {
            let request_expired_div = row.find(".request_expired_div");
            request_expired_div.html("");
            let request_expired = row.find(".request_expired");
            let approve_button = row.find(".approve");
            request_expired.html("");
            approve_button.remove();
          }
        }
      });
    });

    function checkSync(id) {
      var check_url = "{{ route('Qrcode::marketingStamp@getById',':id')}}";
      var checkResult = null;
      check_url = check_url.replace(':id', id);
      $.ajax({
        url: check_url,
        method: 'GET',
        async: false,
        success: function (data) {
          checkResult = parseInt(data);
        }
      });
      return checkResult;
    }

    //Delete stamp
    $(".delete_stamp").click(function () {
      let conf = delItem();
      if (conf !== true) {
        return false;
      }
      var delete_button = $(this);    // Find the row
      var id = delete_button.closest("tr").find(".id").text();
      var delete_url = "{{ route('Qrcode::marketingStamp@delete',':id')}}";
      delete_url = delete_url.replace(':id', id);
      $.ajax({
        url: delete_url,
        method: 'GET',
        async: false,
      });
      var interval_obj = setInterval(function () {
        let checkResult = checkSync(id);
        if (checkResult === -1) {
          delete_button.closest("tr").remove();
          clearInterval(interval_obj);
        }
      }, 1000);
    });
    //Change expired_time
    $("input[name='expired_time']").change(function () {
      var expired_time = this.value;
      var q = new Date();
      var m = q.getMonth();
      var d = q.getDate();
      var y = q.getFullYear();

      var today = new Date(y, m, d);
      if (expired_time === "") {
        return false;
      }
      if (Date.parse(expired_time.concat(" 00:00:00")) <= today) {
        alert("Không thể áp dụng ngày hết hạn này. Ngày hết hạn phải lớn hơn ngày hiện tại!");
        return false;
      }
//            console.log(typeof(expired_time));
      var id = this.id.split("_").pop();
      var url = "{{ route('Qrcode::marketingStamp@updateExpiredTime',":id")}}";
      $.ajax({
        url: url.replace(':id', id),
        method: 'POST',
        data: {
          expired_time: expired_time,
        },
        success: function (data) {
//                    console.log(data);
        },
        error: function () {
          console.log('fail');
        }
      });
    });

  </script>
@endpush
