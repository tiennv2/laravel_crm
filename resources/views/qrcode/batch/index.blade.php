@extends('qrcode.layouts.app')

@section('body_class', 'm-page--fluid m-page--loading-enabled m-page--loading m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default')

@push('scripts')
  <script type="text/javascript">
    <?php
      $sharedData = [
        'config' => [
          'csrfToken' => csrf_token(),
          'logoutUrl' => route('logout'),
          'isNative' => true,
        ],
        'loggedInAccount' => auth()->check() ? auth()->user() : null,
      ];
      ?>
      window.SHARED_DATA = {!! json_encode($sharedData) !!}
  </script>
  <script src="https://cdn.jsdelivr.net/npm/tinymce@4.7.11/tinymce.min.js" type="text/javascript"></script>
  <script src="{{ mix('js/app.js') }}"></script>
@endpush

@section('content')
  <!-- begin::Page loader -->
  <div class="m-page-loader m-page-loader--base">
    <div class="m-blockui">
      <span>
        Please wait...
      </span>
      <span>
        <div class="m-loader m-loader--brand"></div>
      </span>
    </div>
  </div>
  <!-- end::Page Loader -->
  <div id="app" class="m-grid m-grid--hor m-grid--root m-page">
    <div class="m-header__bottom">
      <div class="m-container m-container--responsive m-container--xxl m-container--full-height m-page__container">
        <div class="m-stack m-stack--ver m-stack--desktop">
          <div class="m-stack__item m-stack__item--middle m-stack__item--fluid">
            <app-header></app-header>
          </div>
        </div>
      </div>
    </div>

    <div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop  m-body m-page__container">
      <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader ">
          <div class="d-flex align-items-center">
            @if(array_key_exists('account_id', $query))
              <div class="mr-auto">
                <h3 class="m-subheader__title">
                  <a href="{{url()->previous()}}">
                    <i class="la la-arrow-left"></i>
                  </a>
                  CHI TIẾT LÔ TEM THEO ĐỐI TÁC "{{$filtered_account_name}}"
                </h3>
              </div>
              <div style="float: right">
                @can("QRCODE-viewInboxBusiness")
                  <button type="button" class="btn btn-success" data-toggle="modal"
                          data-target="#message_list"
                          onclick="getMessages(10,{{$query['account_id']}})">
                    Tin đã gửi
                  </button>
                @endcan
              </div>
            @elseif(Request::input('parent_id'))
              <div class="mr-auto">
                <h3 class="m-subheader__title">
                  <a href="{{route('Qrcode::batch@index')}}">
                    <i class="la la-arrow-left"></i>
                  </a>
                  CHI TIẾT TÁCH LÔ TEM "{{Request::input('parent_name')}}"
                </h3>
              </div>
            @else
              <div class="mr-auto">
                <h3 class="m-subheader__title">
                  QUẢN LÝ LÔ TEM
                </h3>
              </div>
            @endif
          </div>
        </div>
        <!-- END: Subheader -->
        <div class="m-content">
          <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__body">
              <!--Begin::Search-->
              <form class="m-form m-form--fit m--margin-bottom-20"
                    action="{{route('Qrcode::batch@index')}}" method="get">
                <!-- Search Field -->
                @if(Request::input('parent_id'))
                  <input type="hidden" name="parent_id" value="{{Request::input('parent_id')}}"/>
                  <input type="hidden" name="parent_name" value="{{Request::input('parent_name')}}"/>
                @endif
                <div class="row m--margin-bottom-20">
                  <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile">
                    <label>Tìm kiếm theo tên lô</label>
                    <input class="form-control" type="text" name="batch_name"
                           value="{{ Request::input('batch_name') }}"/>
                  </div>
                  @if(array_key_exists('account_id', $query) || array_key_exists('parent_id', $query))
                    <input type="hidden" name="account_id"
                           value="{{ Request::input('account_id') }}"/>
                  @else
                    <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile">
                      <label>Tìm kiếm theo đối tác</label>
                      <input class="form-control" type="text" name="account_name"
                             value="{{ Request::input('account_name') }}"/>
                    </div>
                  @endif
                  <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile">
                    <label>Dịch vụ</label>
                    <select class="form-control" id="services" name="services[]"
                            multiple="multiple">
                      <option value="">--Tất cả--</option>
                      @foreach($services as $service)
                        <option
                          value="{{$service->id}}"{{(Request::input('services')!= null and in_array($service->id,Request::input('services')))?'selected="selected"':''}}>{{$service->service_name}}</option>
                      @endforeach

                    </select>
                  </div>
                  <div class="col-lg-4 m--margin-bottom-10-tablet-and-mobile">
                    <label>Ngày tạo </label>
                    <div class="input-daterange input-group">
                      <input type="text" class="form-control" name="from" id="m_datepicker_1"
                             value="{{ Request::input('from') }}" autocomplete="off">
                      <span class="input-group-text">
														<i class="la la-ellipsis-h"></i>
													</span>
                      <input type="text" class="form-control" name="to" id="m_datepicker_1"
                             value="{{ Request::input('to') }}" autocomplete="off">
                    </div>
                  </div>
                  <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile">
                    <label>Trạng thái </label>
                    <select class="form-control" id="status" name="status">
                      <option value="">Chọn trạng thái</option>
                      <option value="0" {{Request::input('status')=== '0'?'selected="selected"':''}}>
                        Chưa thanh toán
                      </option>
                      <option value="1" {{Request::input('status')== 1?'selected="selected"':''}}>
                        Đã thanh toán
                      </option>
                      <option value="2" {{Request::input('status')== 2?'selected="selected"':''}}>
                        Đã kích hoạt
                      </option>
                      <option value="lock" {{Request::input('status')== 'lock'?'selected="selected"':''}}>
                        Bị khóa
                      </option>
                    </select>
                  </div>
                </div>
                <div class="row m--margin-bottom-20">
                  <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                    <label>Tình trạng giấy tờ</label>
                    <select class="form-control" id="attachment_status" name="attachment_status">
                      <option value=""></option>
                      <option value="0" {{Request::input('attachment_status')=== '0'?'selected="selected"':''}}>
                        Chưa có giấy tờ
                      </option>
                      <option value="2" {{Request::input('attachment_status')== 2?'selected="selected"':''}}>
                        Chưa có giấy tờ quá 30 ngày
                      </option>
                      <option value="1" {{Request::input('attachment_status')== 1?'selected="selected"':''}}>
                        Có giấy tờ
                      </option>
                    </select>
                  </div>
                  <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile">
                    <label>Trạng thái sản phẩm</label>
                    <select class="form-control" id="approved_file" name="approved_file">
                      <option value="">Chọn trạng thái</option>
                      <option value="0" {{Request::input('approved_file')=== "0"?'selected="selected"':''}}>
                        Chờ duyệt
                      </option>
                      <option value="1" {{Request::input('approved_file')== 1?'selected="selected"':''}}>
                        Đã duyệt
                      </option>
                      <option value="2" {{Request::input('approved_file')== 2?'selected="selected"':''}}>
                        Thêm file mới
                      </option>
                    </select>
                  </div>
                  <div class="col-lg-5 m--margin-bottom-10-tablet-and-mobile">
                    <label style="color: white">Hành động </label>
                    <div>
                      <button class="btn btn-brand m-btn m-btn--icon" type="submit">
												<span>
													<i class="la la-search"></i>
													<span>
														Tìm kiếm
													</span>
												</span>
                      </button>
                      @if(array_key_exists('account_id', $query)==true)
                        <button class="btn btn-secondary m-btn m-btn--icon" type="button"
                                onclick="window.location='{{route('Qrcode::batch@index')}}?account_id={{$query['account_id']}}'">
												<span>
												<i class="la la-refresh"></i>
													<span>
														Làm mới
													</span>
												</span>
                        </button>
                      @elseif(array_key_exists('parent_id', $query)==true)
                        <button class="btn btn-secondary m-btn m-btn--icon" type="button"
                                onclick="window.location='{{route('Qrcode::batch@index')}}?parent_id={{Request::input('parent_id')}}&parent_name={{Request::input('parent_name')}}'">
												<span>
												<i class="la la-refresh"></i>
													<span>
														Làm mới
													</span>
												</span>
                        </button>
                      @else
                        <button class="btn btn-secondary m-btn m-btn--icon" type="button"
                                onclick="window.location='{{route('Qrcode::batch@index')}}'">
												<span>
												<i class="la la-refresh"></i>
													<span>
														Làm mới
													</span>
												</span>
                        </button>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="m-separator m-separator--md m-separator--dashed"></div>
              </form>
              @if (session('success'))
                <div
                  class="m-alert m-alert--outline m-alert--square m-alert--outline-2x alert alert-success alert-dismissible fade show"
                  role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  </button>
                  {{ session('success') }}
                </div>
            @endif
            <!--begin::Table-->
              <div class="table-responsive">
                <table class="table table-striped- table-bordered table-hover table-checkable"
                       style="width:2000px;margin-left:auto;margin-right:auto;">
                  <thead>
                  <tr>
                    <th style="font-weight: bold;vertical-align: middle">Id</th>
                    <th style="font-weight: bold;width:200px;vertical-align: middle">Tên lô tem</th>
                    <th style="font-weight: bold;width:80px;vertical-align: middle">Tải về lô tem
                    </th>
                    <th style="font-weight: bold;width:300px;vertical-align: middle">Tên đối tác
                    </th>
                    <th style="font-weight: bold;vertical-align: middle;width:150px;">Số lượng</th>
                    <th style="font-weight: bold;width:150px;vertical-align: middle">Dịch vụ</th>
                    <th style="font-weight: bold;vertical-align: middle;width:130px">Trạng thái</th>
                    <th style="font-weight: bold;width:200px;vertical-align: middle">Serial</th>
                    <th style="font-weight: bold;width:100px;vertical-align: middle">Ngày tạo</th>
                    <th style="font-weight: bold;width:200px;vertical-align: middle">Sản phẩm</th>
                    <th style="font-weight: bold;width:200px;vertical-align: middle">File đính kèm
                    </th>
                    <th style="font-weight: bold;width:200px;vertical-align: middle">Mẫu thiết kế
                    </th>
                    <th style="font-weight: bold;width:120px;vertical-align: middle">Trạng thái
                      duyệt sản phẩm
                    </th>
                    <th style="font-weight: bold;width:120px;vertical-align: middle">Gửi thông báo
                    </th>
                    <th style="font-weight: bold;vertical-align: middle">Khóa/Mở khóa lô</th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($batches as $row)
                    <tr role="row">
                      <td>{{$row->id}}</td>
                      <td>
                        @if(!is_null($row->active_left))
                          <a
                            href="{{route('Qrcode::batch@index')}}?parent_id={{$row->id}}&parent_name={{$row->name}}">{{$row->name}}</a>
                        @else
                          {{$row->name}}
                        @endif
                      </td>
                      <td style="text-align: center">
                        @can("QRCODE-downloadBatch")
                          <a href="#" data-toggle="modal" data-target="#download{{$row->id}}">
                            <i class="flaticon-download"></i>
                          </a>
                      @endcan
                      <!--Modal Download-->
                        <div id="download{{$row->id}}" class="modal fade">
                          <div class="modal-dialog">
                            <!-- Modal content-->
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title">Chọn các cột cần
                                  xuất dữ liệu</h5>
                                <button type="button" class="close"
                                        data-dismiss="modal">&times
                                </button>
                              </div>
                              <form method="post" onsubmit=" return check()"
                                    action="{{ route('Qrcode::batch@download',[$row->id])}}">
                                {{ csrf_field() }}
                                <div class="modal-body">
                                  <div class="m-form__group form-group">
                                    <div class="m-checkbox-inline">
                                      <label class="m-checkbox">
                                        <input type="checkbox" name="fields[]"
                                               value="serial">
                                        SERIAL
                                        <span></span>
                                      </label>
                                      <label class="m-checkbox">
                                        <input type="checkbox" name="fields[]"
                                               value="qrm">
                                        QRM
                                        <span></span>
                                      </label>
                                      <label class="m-checkbox">
                                        <input type="checkbox" name="fields[]"
                                               value="qri">
                                        QRI
                                        <span></span>
                                      </label>
                                      <label class="m-checkbox">
                                        <input type="checkbox" name="fields[]"
                                               value="sms">
                                        SMS
                                        <span></span>
                                      </label>
                                    </div>
                                  </div>
                                  @if ($errors->has('fields'))
                                    <div class="form-control-feedback">
                                      <i class="icon-notification2"></i>
                                    </div>
                                    <div class="help-block"
                                         style="color: red">{{ $errors->first('fields') }}</div>
                                  @endif
                                  <input type="hidden" name="start"
                                         value="{{$row->id_start}}">
                                  <input type="hidden" name="end"
                                         value="{{$row->id_end}}">
                                  <input type="hidden" name="batch_id"
                                         value="{{$row->prefix_id}}">
                                  <input type="hidden" name="prefix"
                                         value="{{$row->prefix}}">
                                </div>
                                <div class="modal-footer">
                                  <button type="submit"
                                          class="btn btn-primary">
                                    Tải về
                                  </button>
                                  <button type="button" onclick="removeChecked()"
                                          class="btn btn-link"
                                          data-dismiss="modal">Hủy bỏ
                                  </button>
                                </div>
                              </form>
                            </div>
                          </div>
                          <script>
                            function removeChecked() {
                              $(":checkbox:checked").each(function () {
                                this.click();
                              });
                            }
                          </script>
                        </div>
                        <!--End Modal-->
                      </td>
                      <td>
                        <a
                          href="{{route('Qrcode::batch@index')}}?account_id={{$row->user_id}}">{{$row->account_name}}</a>
                      </td>
                      <td>{{number_format($row->quantity,0,".",",")}}</td>
                      <td>@foreach($services as $service)
                          @if(strpos($row->services,strval($service->id))!== false)
                            {{$service->service_name}};
                          @endif
                        @endforeach
                      </td>
                      <td>
                        @if($row->status==1 && $row->active_left > 0)
                          <span class="m-badge m-badge--warning">
									Đã kích hoạt </span>
                        @elseif($row->status==2)
                          <span class="m-badge m-badge--success">
									Đã kích hoạt </span>
                        @elseif($row->status==1 && $row->active_left ==null)
                          <span class="m-badge m-badge--warning">
									Đã thanh toán </span>
                        @else
                          <span class="m-badge m-badge--metal">
									Chưa thanh toán </span>
                        @endif
                        @if($row->is_deleted==1)
                          <span class="m-badge m-badge--danger">
									Bị khóa </span>
                        @endif
                      </td>
                      <td>{{$row->prefix}}-{{$row->id_start}} <span
                          style="font-weight: bold">đến</span> {{$row->prefix}}
                        -{{$row->id_end}}</td>
                      <td>{{date("d/m/Y",strtotime($row->created_time))}}</td>
                      <td>
                        <a
                          href="{{route('Qrcode::product@get_upload_product_file',[$row->product_id])}}">{{$row->product_name}}</a>
                      </td>
                      <td>
                        @foreach( $product_attachments as $product_attachment)
                          @php
                            $attachment_split = explode('/',$product_attachment->path);
                            $attachment_name = end($attachment_split);
                              $path = $product_attachment->path;
                          @endphp
                          @if($row->product_id == $product_attachment->product_id && $attachment_name!='')
                            <div><a href="{{$path}}">
                                @php
                                  echo substr($attachment_name,0,25)."...";
                                @endphp
                              </a></div>
                          @endif
                        @endforeach
                      </td>
                      <td>
                        @can("QRCODE-viewTemplate")
                          @foreach($row->templates as $template)
                            <a href="#" data-toggle="modal"
                               data-target="#template_detail{{$template->id}}">
                              {{$template->name}}
                            </a>;
                            <!--Modal Template_Detail-->
                            <div id="template_detail{{$template->id}}" class="modal fade">
                              <div class="modal-dialog">
                                <!-- Modal content-->
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h3>Chi tiết files thiết kế "{{$template->name}}
                                      "</h3>
                                    <button type="button" class="close"
                                            data-dismiss="modal">&times
                                    </button>
                                  </div>
                                  <div class="modal-body">
                                    @if($template->attachments != null)
                                      @php
                                        $attachments = explode(',',$template->attachments);
                                      @endphp
                                      @foreach( $attachments as $attachment)
                                        @php
                                          $attachment_split = explode('/',$attachment);
                                          $attachment_name = end($attachment_split);
                                            $path = $attachment;
                                            $types = ['jpg','JPG','png','PNG','gif','GIF'];
                                            $attachment_name_split = explode('.',$attachment_name);
                                      $extension = end($attachment_name_split);
                                        @endphp
                                        @if(in_array($extension,$types))
                                          <span><a href="{{$path}}"><img
                                                src="{{$path}}"
                                                width="150px"/></a></span>
                                        @else
                                          <span><a href="{{$path}}">{{$attachment_name}}</a></span>
                                        @endif
                                      @endforeach
                                    @endif
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button"
                                            class="btn btn-link"
                                            data-dismiss="modal">Đóng
                                    </button>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <!--End Modal-->
                          @endforeach
                        @endcan
                      </td>
                      <td>
                        @if($row->product_id!=null)
                          @if($row->approved_file==1)
                            @if(auth()->user()->can('QRCODE-disapproveProduct'))
                              <a href="{{route('Qrcode::product@disapprove_product_file',[$row->product_id])}}">
                                <span class="m-badge m-badge--success">Đã duyệt</span></a>
                            @else
                              <span class="m-badge m-badge--success">Đã duyệt</span>
                            @endif
                          @elseif($row->approved_file==2)
                            @if(auth()->user()->can('QRCODE-approveProduct'))
                              <a href="{{route('Qrcode::product@approve_product_file',[$row->product_id])}}">
                                                                <span class="m-badge m-badge--danger"
                                                                      style="background-color: #f28c89">Thêm file mới </span></a>
                            @else
                              <span class="m-badge m-badge--danger"
                                    style="background-color: #f28c89">Thêm file mới </span>
                            @endif
                          @else
                            @if(auth()->user()->can('QRCODE-approveProduct'))
                              <a href="{{route('Qrcode::product@approve_product_file',[$row->product_id])}}">
                                <span class="m-badge m-badge--warning">Chờ duyệt </span></a>
                            @else
                              <span class="m-badge m-badge--warning">Chờ duyệt </span>
                            @endif
                          @endif
                        @endif
                      </td>
                      <td style="text-align: center">
                        @if($row->product_id!=null)
                          @can("QRCODE-inboxBusiness")
                            <button type="button" data-toggle="modal"
                                    data-target="#inbox{{$row->id}}"
                                    class="btn btn-success">
                              <i class="flaticon-mail-1"></i>
                            </button>
                        @endcan
                      @endif
                      <!--Modal Inbox-->
                        <div id="inbox{{$row->id}}" class="modal fade test"
                             role="dialog">
                          <div class="modal-dialog" role="document">
                            <!-- Modal content-->
                            <div class="modal-content">
                              <div class="modal-header">
                                <h3>Gửi thông báo tới "{{$filtered_account_name}}" về lô
                                  tem "{{$row->name}}"</h3>
                                <button type="button" class="close"
                                        data-dismiss="modal">&times;
                                </button>
                              </div>
                              <form method="post"
                                    action="{{ route('Qrcode::company@inbox',[$row->user_id]) }}">
                                {{ csrf_field() }}
                                <div class="modal-body">
                                                                   <textarea class="form-control" rows="5"
                                                                             placeholder="Nhập tin nhắn"
                                                                             name="message" required></textarea>
                                </div>
                                <div class="modal-footer">
                                  <button type="submit"
                                          class="btn btn-success">
                                    Gửi đi
                                  </button>
                                  <button type="button"
                                          class="btn btn-default"
                                          data-dismiss="modal">Hủy bỏ
                                  </button>
                                </div>
                              </form>
                            </div>
                          </div>
                        </div>
                        <!--End Modal-->
                      </td>
                      <td style="text-align: center">
                        @if($row->is_deleted == 0)
                          @can("QRCODE-lockBatch")
                            <button type="button" title="Khóa lô"
                                    onclick="window.location='{{route('Qrcode::batch@lock_batch',$row->id)}}'"
                                    class="btn btn-danger">
                              <i class="la la-unlock"></i>
                            </button>
                          @endcan
                        @else
                          @can("QRCODE-unlockBatch")
                            <button type="button" title="Mở khóa lô"
                                    onclick="window.location='{{route('Qrcode::batch@unlock_batch',$row->id)}}'"
                                    class="btn btn-success">
                              <i class="la la-unlock-alt"></i>
                            </button>
                          @endcan
                        @endif
                      </td>

                    </tr>
                  @endforeach
                  </tbody>
                </table>
              </div>
              <!--end::Table-->
              <div style="float:right;"><?php echo $batches->links(); ?></div>
            </div>
          </div>
          <!--begin::Message Modal-->
          <div class="modal fade" id="message_list" tabindex="-1" role="dialog"
               aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title">
                    <strong>Danh sách tin nhắn đã gửi </strong><span
                      id="list-message-modal-title"></span>
                  </h5>

                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">
												&times;
											</span>
                  </button>
                </div>
                <div class="modal-body" style="height:200px;overflow-y: scroll;" id="content_modal">
                  {{"Vui lòng chờ ..."}}
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    Đóng
                  </button>
                </div>
              </div>
            </div>
          </div>
          <!--end::Modal-->
        </div>
      </div>
    </div>
  </div>
  <router-view></router-view>
@endsection
@push('scripts')
  <script type="text/javascript" src="{{ asset('qr_code/metronic/js/vendors.bundle.js') }}"></script>
  <script type="text/javascript" src="{{ asset('qr_code/metronic/js/scripts.bundle.js') }}"></script>
  <script type="text/javascript" src="{{ asset('qr_code/metronic/js/datepicker/bootstrap-datepicker.js') }}"></script>
  <script>
    $(document).ready(function () {
      // Basic
      $("#status").select2({
        placeholder: 'Chọn trạng thái',
        allowClear: true
      });
      $("#approved_file").select2({
        placeholder: 'Chọn trạng thái',
        allowClear: true
      });
      $("#attachment_status").select2({
        placeholder: 'Chọn tình trạng',
        allowClear: true
      });
      $("#services").select2({
        placeholder: 'Chọn dịch vụ'
      });
      $("select").on("select2:select", function (evt) {
        var element = evt.params.data.element;
        var $element = $(element);
        $element.detach();
        $(this).append($element);
        $(this).trigger("change");
      });
    });

    function getMessages(limit, account_id) {
      var url = "{{ route('Qrcode::company@admin-inbox')}}";
      $.ajax({
        url: url,
        method: 'GET',
        data: {
          user: account_id,
          limit: limit
        },
        success: function (data) {
          $("#content_modal").html('');
          $("#list-message-modal-title").html('');
          var total = data.data.total;
          $("#list-message-modal-title").append(
            "<span>" + "(" + total + ")" + "</span>"
          );
          $.each(data.data.messages, function (key, value) {
            value.ins_dt = value.ins_dt.slice(0, 10);
            $("#content_modal").append(
              "<p><i class=\"flaticon-multimedia-5\"></i>" + " " + value.message + " " + "(" + value.ins_dt + ")" + "<p>"
            );
          });
          $("#content_modal").append(
            "<button type=\"button\" class=\"btn\" id = \"more\" onclick=\"getMessages(" + limit + " + 10, " + account_id + ")\" style=\"border: none;\">" + "Xem thêm" + "</button>"
          );
          if (limit > total) {
            $("#more").hide();
          }
        }
      });
    }

  </script>
  <script>
    function check() {
      var checkboxes = document.querySelectorAll('input[type=checkbox]:checked');
      if (parseInt(checkboxes.length) === 0) {
        alert("Bạn cần chọn một cột để xuất dữ liệu!");
        return false;
      }
      return true;
    }

  </script>
@endpush
