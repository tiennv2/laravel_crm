@extends('qrcode.layouts.app')

@section('body_class', 'm-page--fluid m-page--loading-enabled m-page--loading m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default')

@push('scripts')
    <script type="text/javascript">
        <?php
            $sharedData = [
                'config' => [
                    'csrfToken' => csrf_token(),
                    'logoutUrl' => route('logout'),
                    'isNative' => true,
                ],
                'loggedInAccount' => auth()->check() ? auth()->user() : null,
            ];
            ?>
            window.SHARED_DATA = {!! json_encode($sharedData) !!}
    </script>
    <script src="https://cdn.jsdelivr.net/npm/tinymce@4.7.11/tinymce.min.js" type="text/javascript"></script>
    <script src="{{ mix('js/app.js') }}"></script>
@endpush

@section('content')
    <!-- begin::Page loader -->
    <div class="m-page-loader m-page-loader--base">
        <div class="m-blockui">
      <span>
        Please wait...
      </span>
            <span>
        <div class="m-loader m-loader--brand"></div>
      </span>
        </div>
    </div>
    <!-- end::Page Loader -->
    <div id="app" class="m-grid m-grid--hor m-grid--root m-page">
        <div class="m-header__bottom">
            <div class="m-container m-container--responsive m-container--xxl m-container--full-height m-page__container">
                <div class="m-stack m-stack--ver m-stack--desktop">
                    <div class="m-stack__item m-stack__item--middle m-stack__item--fluid">
                        <app-header></app-header>
                    </div>
                </div>
            </div>
        </div>

        <div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop  m-body m-page__container">
            <div class="m-grid__item m-grid__item--fluid m-wrapper">
                <!-- BEGIN: Subheader -->
                <div class="m-subheader ">
                    <div class="d-flex align-items-center">
                        <div class="mr-auto">
                            <h3 class="m-subheader__title ">
                                <a href="{{route('Qrcode::company@index')}}">
                                    <i class="la la-arrow-left"></i>
                                </a>
                                DANH SÁCH LÔ TEM THIẾU GIẤY TỜ QUÁ 30 NGÀY KỂ TỪ NGÀY KÍCH HOẠT
                            </h3>
                        </div>

                    </div>
                </div>
                <!-- END: Subheader -->
                <div class="m-content">
                    <div class="m-portlet m-portlet--mobile">
                        <div class="m-portlet__body">
                            <!--Begin::Search-->
                            <form class="m-form m-form--fit m--margin-bottom-20"
                                  action="{{route('Qrcode::batch@batches_without_attachments')}}" method="get">
                                <!-- Search Field -->
                                <div class="row m--margin-bottom-20">
                                    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                                        <label>Tìm kiếm theo tên lô tem</label>
                                        <input class="form-control" type="text" name="batch_name"
                                               value="{{ Request::input('batch_name') }}"/>
                                    </div>
                                    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                                        <label>Tìm kiếm theo tên đối tác</label>
                                        <input class="form-control" type="text" name="account_name"
                                               value="{{ Request::input('account_name') }}"/>
                                    </div>

                                    <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile">
                                        <label>Dịch vụ</label>
                                        <select class="form-control" id="services" name="services[]"
                                                multiple="multiple">
                                            <option value="">--Tất cả--</option>
                                            @foreach($services as $service)
                                                <option value="{{$service->id}}"{{(Request::input('services')!= null and in_array($service->id,Request::input('services')))?'selected="selected"':''}}>{{$service->service_name}}</option>
                                            @endforeach

                                        </select>
                                    </div>
                                    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                                        <label>Ngày tạo </label>
                                        <div class="input-daterange input-group">
                                            <input type="text" class="form-control" name="from" id="m_datepicker_1"
                                                   value="{{ Request::input('from') }}" autocomplete="off">
                                            <span class="input-group-text">
														<i class="la la-ellipsis-h"></i>
													</span>
                                            <input type="text" class="form-control" name="to" id="m_datepicker_1"
                                                   value="{{ Request::input('to') }}" autocomplete="off">
                                        </div>
                                    </div>

                                </div>
                                <div class="m-separator m-separator--md m-separator--dashed"></div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <button class="btn btn-brand m-btn m-btn--icon" type="submit">
												<span>
													<i class="la la-search"></i>
													<span>
														Tìm kiếm
													</span>
												</span>
                                        </button>
                                        &nbsp;&nbsp;
                                        <button class="btn btn-secondary m-btn m-btn--icon" type="button"
                                                onclick="window.location='{{route('Qrcode::batch@batches_without_attachments')}}'">
												<span>
													<i class="la la-refresh"></i>
													<span>
														Làm mới
													</span>
												</span>
                                        </button>
                                    </div>
                                </div>
                            </form>
                            @if (session('success'))
                                <div class="m-alert m-alert--outline m-alert--square m-alert--outline-2x alert alert-success alert-dismissible fade show"
                                     role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    </button>
                                    {{ session('success') }}
                                </div>
                        @endif
                        <!--begin::Table-->
                            <table class="table table-striped- table-bordered table-hover table-checkable">
                                <thead>
                                <tr>
                                    <th style="font-weight: bold">Id</th>
                                    <th style="font-weight: bold">Tên đối tác</th>
                                    <th style="font-weight: bold">Tên lô tem</th>
                                    <th style="font-weight: bold">Số lượng</th>
                                    <th style="font-weight: bold">Dịch vụ</th>
                                    <th style="font-weight: bold">Trạng thái</th>
                                    <th style="font-weight: bold">Ngày tạo</th>
                                    <th style="font-weight: bold">Sản phẩm</th>
                                    <th style="font-weight: bold">File đính kèm</th>
                                    <th style="font-weight: bold;">Gửi thông báo</th>
                                    <th style="font-weight: bold">Khóa/Mở khóa lô</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($batches as $row)
                                    <tr role="row">
                                        <td>{{$row->id}}</td>
                                        <td>{{$row->account_name}}</td>
                                        <td>{{$row->name}}</td>
                                        <td>{{number_format($row->quantity,0,".",",")}}</td>
                                        <td>@foreach($services as $service)
                                                @if(strpos($row->services,strval($service->id))!== false)
                                                    {{$service->service_name}};
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>
                                            @if($row->status==1)
                                                <span class="m-badge m-badge--success m-badge--wide">
									Đã thanh toán </span>
                                            @elseif($row->status==2)
                                                <span class="m-badge m-badge--danger m-badge--wide"
                                                      style="background-color: #62a8ea">
									Đã kích hoạt </span>
                                            @else
                                                <span class="m-badge m-badge--warning m-badge--wide">
									Chưa thanh toán </span>
                                            @endif
                                        </td>
                                        <td>{{date("d/m/Y",strtotime($row->created_time))}}</td>
                                        <td>{{$row->product_name}}</td>
                                        <td>
                                            @foreach( $product_attachments as $product_attachment)
                                                @php
                                                    $attachment_split = explode('/',$product_attachment->path);
                                                    $attachment_name = end($attachment_split);
                                                      $path = $product_attachment->path;
                                                @endphp
                                                @if($row->product_id == $product_attachment->product_id && $attachment_name!='')
                                                    <div><a href="{{$path}}">{{$attachment_name}}</a></div>
                                                @endif
                                            @endforeach
                                        </td>
                                        <td style="text-align: center">
                                            @if($row->product_id!=null)
                                                <button type="button" data-toggle="modal"
                                                        data-target="#inbox{{$row->id}}"
                                                        class="btn btn-success">
                                                    <i class="flaticon-mail-1"></i>
                                                </button>
                                        @endif
                                        <!--Modal Inbox-->
                                            <div id="inbox{{$row->id}}" class="modal fade test"
                                                 role="dialog">
                                                <div class="modal-dialog modal-lg" role="document">
                                                    <!-- Modal content-->
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h3>Gửi thông báo tới doanh
                                                                nghiệp "{{$row->account_name}}" về lô tem
                                                                "{{$row->name}}
                                                                "</h3>
                                                            <button type="button" class="close"
                                                                    data-dismiss="modal">&times;
                                                            </button>
                                                        </div>
                                                        <form method="post"
                                                              action="{{ route('Qrcode::company@inbox',[$row->user_id]) }}">
                                                            {{ csrf_field() }}
                                                            <div class="modal-body">
                                                                   <textarea class="form-control" rows="5"
                                                                             placeholder="Nhập tin nhắn"
                                                                             name="message" required></textarea>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="submit"
                                                                        class="btn btn-success">
                                                                    Gửi đi
                                                                </button>
                                                                <button type="button"
                                                                        class="btn btn-default"
                                                                        data-dismiss="modal">Hủy bỏ
                                                                </button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--End Modal-->
                                        </td>
                                        <td style="text-align: center">
                                            @if($row->is_deleted == 0)
                                                <button type="button" title="Khóa lô"
                                                        onclick="window.location='{{route('Qrcode::batch@lock_batch',$row->id)}}'"
                                                        class="btn btn-danger">
                                                    <i class="la la-unlock"></i>
                                                </button>
                                            @else
                                                <button type="button" title="Mở khóa lô"
                                                        onclick="window.location='{{route('Qrcode::batch@unlock_batch',$row->id)}}'"
                                                        class="btn btn-success">
                                                    <i class="la la-unlock-alt"></i>
                                                </button></td>
                                        @endif
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <!--end::Table-->
                            <div style="float:right;"><?php echo $batches->links(); ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <router-view></router-view>
@endsection
@push('scripts')
    <script type="text/javascript" src="{{ asset('qr_code/metronic/js/vendors.bundle.js') }}"></script>
    <script type="text/javascript" src="{{ asset('qr_code/metronic/js/scripts.bundle.js') }}"></script>
    <script type="text/javascript" src="{{ asset('qr_code/metronic/js/datepicker/bootstrap-datepicker.js') }}"></script>
    <script>
        $(document).ready(function () {
            // Basic
            $("#status").select2({
                placeholder: 'Chọn trạng thái',
                allowClear: true
            });
            $("#attachment_status").select2({
                placeholder: 'Chọn tình trạng',
                allowClear: true
            });
            $("#services").select2({
                placeholder: 'Chọn dịch vụ'
            });
            $("select").on("select2:select", function (evt) {
                var element = evt.params.data.element;
                var $element = $(element);
                $element.detach();
                $(this).append($element);
                $(this).trigger("change");
            });
        });

    </script>
@endpush