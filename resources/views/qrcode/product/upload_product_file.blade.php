@extends('qrcode.layouts.app')
@section('body_class', 'm-page--fluid m-page--loading-enabled m-page--loading m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default')
@push('scripts')
    <script type="text/javascript">
        <?php
            $sharedData = [
                'config' => [
                    'csrfToken' => csrf_token(),
                    'logoutUrl' => route('logout'),
                    'isNative' => true,
                ],
                'loggedInAccount' => auth()->check() ? auth()->user() : null,
            ];
            ?>
            window.SHARED_DATA = {!! json_encode($sharedData) !!}
    </script>
    <script src="https://cdn.jsdelivr.net/npm/tinymce@4.7.11/tinymce.min.js" type="text/javascript"></script>
    <script src="{{ mix('js/app.js') }}"></script>
@endpush

@section('content')
    <!-- begin::Page loader -->
    <div class="m-page-loader m-page-loader--base">
        <div class="m-blockui">
      <span>
        Please wait...
      </span>
            <span>
        <div class="m-loader m-loader--brand"></div>
      </span>
        </div>
    </div>
    <!-- end::Page Loader -->
    <div id="app" class="m-grid m-grid--hor m-grid--root m-page">
        <app-header></app-header>
        <div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop m-container m-container--responsive m-container--xxl m-page__container m-body">
            <div class="m-grid__item m-grid__item--fluid m-wrapper">
                <div class="m-content">
                    <div class="m-portlet">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h2 class="m-portlet__head-text">
                                        <a href="{{url()->previous()}}">
                                            <i class="la la-arrow-left"></i>
                                        </a>
                                        {{ "Sản phẩm '$product->name'"}}
                                    </h2>
                                </div>
                            </div>
                        </div>


                        <!-- /page header -->
                        <div class="m-portlet__body">
                            <div class="m-form m-form--fit m-form--label-align-right">
                                <h4>Thông tin sản phẩm</h4>
                                <div class="form-group m-form__group row" style="padding: 0">
                                    <label class="col-lg-3 col-sm-12">Ảnh sản phẩm:</label>
                                    <div class="col-lg-6 col-md-9 col-sm-12">
                                        <img src="https://static.icheck.com.vn/{{$product->image}}_thumb_large.jpg" width="200px">
                                    </div>
                                </div>
                                <div class="form-group m-form__group row" style="padding: 0">
                                    <label class="col-lg-3 col-sm-12">Tên sản phẩm:</label>
                                    <div class="col-lg-6 col-md-9 col-sm-12">
                                        {{$product->name}}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row" style="padding: 0">
                                    <label class="col-lg-3 col-sm-12">Mã sản phẩm:</label>
                                    <div class="col-lg-6 col-md-9 col-sm-12" style="padding:auto">
                                        {{$product->sku}}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row" style="padding: 0">
                                    <label class="col-lg-3 col-sm-12">Giá sản phẩm:</label>
                                    <div class="col-lg-6 col-md-9 col-sm-12" style="padding:auto">
                                        {{$product->price}}
                                    </div>
                                </div>
                            </div>
                            <div class="m-form m-form--fit m-form--label-align-right">
                                <h4>Thông tin nhà sản xuất</h4>
                                <div class="form-group m-form__group row" style="padding: 0">
                                    <label class="col-lg-3 col-sm-12">Tên nhà sản xuất:</label>
                                    <div class="col-lg-6 col-md-9 col-sm-12" style="padding:auto">
                                        {{$vendor->name}}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row" style="padding: 0">
                                    <label class="col-lg-3 col-sm-12">Địa chỉ:</label>
                                    <div class="col-lg-6 col-md-9 col-sm-12" style="padding:auto">
                                        {{$vendor->address}}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row" style="padding: 0">
                                    <label class="col-lg-3 col-sm-12">Quận/Huyện:</label>
                                    <div class="col-lg-6 col-md-9 col-sm-12" style="padding:auto">
                                        {{$vendor->district_name}}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row" style="padding: 0">
                                    <label class="col-lg-3 col-sm-12">Tỉnh/Thành phố:</label>
                                    <div class="col-lg-6 col-md-9 col-sm-12" style="padding:auto">
                                        {{$vendor->city_name}}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row" style="padding: 0">
                                    <label class="col-lg-3 col-sm-12">Website:</label>
                                    <div class="col-lg-6 col-md-9 col-sm-12" style="padding:auto">
                                        {{$vendor->website}}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row" style="padding: 0">
                                    <label class="col-lg-3 col-sm-12">Email:</label>
                                    <div class="col-lg-6 col-md-9 col-sm-12" style="padding:auto">
                                        {{$vendor->email}}
                                    </div>
                                </div>
                                <div class="form-group m-form__group row" style="padding: 0">
                                    <label class="col-lg-3 col-sm-12">Số điện thoại:</label>
                                    <div class="col-lg-6 col-md-9 col-sm-12" style="padding:auto">
                                        {{$vendor->phone}}
                                    </div>
                                </div>
                            </div>
                            @if(count($product_info)>0)
                                <div class="m-form m-form--fit m-form--label-align-right">
                                    <h4>Mô tả sản phẩm</h4>
                                    @foreach($product_info as $info)
                                        <div class="form-group m-form__group row" style="padding: 0">
                                            <label class="col-lg-3 col-sm-12">{{$info->title}}:</label>
                                            <div class="col-lg-6 col-md-9 col-sm-12" style="padding:auto">
                                                {!!$info->content!!}
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            @endif

                            @if (session('success'))
                                <div class="m-alert m-alert--outline m-alert--square m-alert--outline-2x alert alert-success alert-dismissible fade show"
                                     role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    </button>
                                    {{ session('success') }}
                                </div>
                            @endif
                            @can("QRCODE-uploadProductFiles")
                                <form method="POST" enctype="multipart/form-data" name="form" onsubmit="return check()"
                                      class="m-form m-form--fit m-form--label-align-right"
                                      action="{{route('Qrcode::product@post_upload_product_file',$product->id) }}">
                                    {{ csrf_field() }}
                                    <h4>Upload files sản phẩm</h4>
                                    <div class="form-group m-form__group row">
                                        <label class="col-form-label col-lg-3 col-sm-12">Chọn files *</label>
                                        <div class="col-lg-6 col-md-9 col-sm-12">
                                            <input type="file" id="file" name="attachments[]" multiple="multiple"
                                                   class="form-control"/>
                                            @if ($errors->has("attachments.0")||$errors->has("attachments.1")||$errors->has("attachments.2")||$errors->has("attachments.3")||$errors->has("attachments.4"))
                                                <div class="form-control-feedback">
                                                    <i class="icon-notification2" style="color: red"></i>
                                                </div>
                                                <div class="help-block"
                                                     style="color: red">
                                                    {{"Sai định dạng file upload hoặc có một file upload dung lượng quá 20 MB. Vui lòng kiểm tra lại"}}</div>
                                            @elseif($errors->has("attachments_over"))
                                                <div class="form-control-feedback">
                                                    <i class="icon-notification2" style="color: red"></i>
                                                </div>
                                                <div class="help-block"
                                                     style="color: red">
                                                    {{ $errors->first('attachments_over')}}</div>
                                            @endif
                                            <input type="hidden" name="remove_files" id="remove_files" value="">
                                            @php
                                                echo "<br>";
                                            @endphp
                                            <div id='file'>
                                                <strong>
                                                    Các files đã upload
                                                </strong>
                                                @if(count($product_attachments)> 0)
                                                    @foreach ($product_attachments as $attachment)
                                                        <div id="{{$attachment}}">
                                                            @php
                                                                $attachment_split = explode('/',$attachment);
                                                                $attachment_name = end($attachment_split);
                                                            @endphp
                                                            @if($attachment_name!='')
                                                                <a href="{{$attachment}}">{{$attachment_name}}</a>
                                                                <a class="btn" href="javascript:;"><i
                                                                            class="la la-trash"
                                                                            onclick="del('{{$attachment}}')"></i></a>
                                                        </div>
                                                        @endif
                                                    @endforeach
                                                @else
                                                    <div>Chưa có</div>
                                                @endif
                                            </div>
                                            <span style="font-style: italic">Giữ phím Ctrl để chọn nhiều file (tối đa không quá 5 files).
                                        Mỗi file không quá 20MB và File upload phải là một tập tin có định dạng: jpg, jpeg, bmp,
                                            xls, xlsx, pdf, doc, docx, zip, rar</span>
                                        </div>
                                    </div>
                                    <div class="m-portlet__foot m-portlet__foot--fit">
                                        <div class="m-form__actions m-form__actions">
                                            <div class="row">
                                                <div class="col-lg-9 ml-lg-auto">
                                                    <button type="submit" class="btn btn-success">
                                                        Cập nhật
                                                    </button>
                                                    <button type="button" class="btn btn-secondary">
                                                        Hủy bỏ
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            @endcan
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <router-view></router-view>
@endsection

@push('scripts')
    <script type="text/javascript" src="{{ asset('qr_code/metronic/js/vendors.bundle.js') }}"></script>
    <script type="text/javascript" src="{{ asset('qr_code/metronic/js/scripts.bundle.js') }}"></script>
    <script>
        function del(i) {
            document.getElementById(i).style.display = "none";
            var x = document.getElementById("remove_files").value;

            if (x === '') {
                document.getElementById("remove_files").value = x.concat("", i);
            }
            else {
                document.getElementById("remove_files").value = x.concat(",", i);
            }

            var y = document.getElementById("remove_files").value;


        }

        function check() {
            var $fileUpload = $("input[type='file']");
            if (parseInt($fileUpload.get(0).files.length) > 5) {
                alert("Không được upload quá 5 files");
                return false;
            }
            return true;
        }
    </script>
@endpush
