@extends('qrcode.layouts.app')
@section('body_class', 'm-page--fluid m-page--loading-enabled m-page--loading m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default')

@push('scripts')
    <script type="text/javascript">
        <?php
            $sharedData = [
                'config' => [
                    'csrfToken' => csrf_token(),
                    'logoutUrl' => route('logout'),
                    'isNative' => true,
                ],
                'loggedInAccount' => auth()->check() ? auth()->user() : null,
            ];
            ?>
            window.SHARED_DATA = {!! json_encode($sharedData) !!}
    </script>
    <script src="https://cdn.jsdelivr.net/npm/tinymce@4.7.11/tinymce.min.js" type="text/javascript"></script>
    <script src="{{ mix('js/app.js') }}"></script>
@endpush

@section('content')
    <!-- begin::Page loader -->
    <div class="m-page-loader m-page-loader--base">
        <div class="m-blockui">
      <span>
        Please wait...
      </span>
            <span>
        <div class="m-loader m-loader--brand"></div>
      </span>
        </div>
    </div>
    <!-- end::page Loader -->
    <div id="app" class="m-grid m-grid--hor m-grid--root m-page">
        <app-header></app-header>
        <div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop  m-body m-page__container">
            <div class="m-grid__item m-grid__item--fluid m-wrapper">
                <!-- BEGIN: Subheader -->
                <div class="m-subheader">
                    <div class="d-flex align-items-center">
                        <div class="mr-auto">
                            <h3 class="m-subheader__title">
                                QUẢN LÝ DOANH NGHIỆP
                            </h3>
                        </div>
                        @can("QRCODE-addBusiness")
                            <div style="float: right"><a href="{{route('Qrcode::company@add')}}"
                                                         class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air"><span>
                                    <i class="la la-plus-circle"></i>
                                    <span>Thêm doanh nghiệp</span></span></a>
                            </div>
                        @endcan
                    </div>
                </div>
                <!-- END: Subheader -->
                @can("QRCODE-viewBusiness")
                    <div class="m-content">
                        <!--Begin::Section-->
                        <div class="m-portlet m-portlet--mobile">
                            <div class="m-portlet__body">
                                <form class="m-form m-form--fit m--margin-bottom-20"
                                      action="{{route('Qrcode::company@index')}}" method="get">
                                    <div class="row m--margin-bottom-20">
                                        <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile">
                                            <label>
                                                Tìm kiếm theo tên:
                                            </label>
                                            <input type="text" class="form-control m-input" name="account_name"
                                                   value="{{ Request::input('account_name') }}"
                                                   data-col-index="0">
                                        </div>
                                        <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile">
                                            <label>
                                                Tìm kiếm theo địa chỉ:
                                            </label>
                                            <input type="text" class="form-control m-input" name="address"
                                                   value="{{ Request::input('address') }}"
                                                   data-col-index="1">
                                        </div>
                                        <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                                            <label>
                                                Tìm kiếm theo số điện thoại:
                                            </label>
                                            <input type="text" class="form-control m-input" name="phone"
                                                   value="{{ Request::input('phone') }}"
                                                   data-col-index="4">
                                        </div>
                                        <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                                            <label>
                                                Tìm kiếm theo email:
                                            </label>
                                            <input type="text" class="form-control m-input" name="email"
                                                   value="{{ Request::input('email') }}"
                                                   data-col-index="4">
                                        </div>
                                        <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile">
                                            <label>
                                                Tìm kiếm theo website:
                                            </label>
                                            <input type="text" class="form-control m-input" name="website"
                                                   value="{{ Request::input('website') }}"
                                                   data-col-index="4">
                                        </div>
                                    </div>
                                    <div class="row m--margin-bottom-20">
                                        <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile">
                                            <label>
                                                Chọn tỉnh/thành phố:
                                            </label>
                                            <select class="form-control m-input" data-col-index="2" id="city"
                                                    name="account_city_id">
                                                <option value="">--Tất cả--</option>
                                                @foreach($cities as $city)
                                                    <option value="{{$city->id}}"
                                                            {{Request::input('account_city_id')==$city->id?'selected="selected"':''}}>{{$city->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile">
                                            <label>
                                                Chọn quận huyện:
                                            </label>
                                            <select class="form-control m-input" data-col-index="6"
                                                    name="account_district_id" id="district">
                                                @if(isset($districts))
                                                    <option value="">--Tất cả--</option>
                                                    @foreach($districts as $district)
                                                        <option value="{{$district->id}}"
                                                                {{Request::input('account_district_id')==$district->id?'selected="selected"':''}}>{{$district->name}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                        <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                                            <label>
                                                Ngày tạo:
                                            </label>
                                            <div class="input-daterange input-group">
                                                <input type="text" class="form-control" name="from" id="m_datepicker_1"
                                                       value="{{ Request::input('from') }}" autocomplete="off"
                                                       data-col-index="5"/>
                                                <div class="input-group-append">
													<span class="input-group-text">
														<i class="la la-ellipsis-h"></i>
													</span>
                                                </div>
                                                <input type="text" class="form-control" name="to" id="m_datepicker_1"
                                                       value="{{ Request::input('to') }}" autocomplete="off"
                                                       data-col-index="5"/>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                                            <label>
                                                Người tạo:
                                            </label>
                                            <select class="form-control m-input" data-col-index="7" id="user"
                                                    name="user_created">
                                                <option value=""></option>
                                                <option value="admin" {{Request::input('user_created')=="admin"?'selected="selected"':''}}>
                                                    iCheck
                                                </option>
                                                @foreach($collaborators as $collaborator)
                                                    <option value="{{$collaborator->collaborator_phone}}"
                                                            {{Request::input('user_created')==$collaborator->collaborator_phone?'selected="selected"':''}}>{{$collaborator->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile">
                                            <label>
                                                Chọn trạng thái:
                                            </label>
                                            <select class="form-control m-input" data-col-index="7" id="status"
                                                    name="status">
                                                <option value="" selected="selected">--Tất cả--</option>
                                                <option value="0" {{Request::input('status')==="0"?'selected="selected"':''}}>
                                                    Chờ kích hoạt
                                                </option>
                                                <option value="1" {{Request::input('status')==1?'selected="selected"':''}}>
                                                    Đã kích hoạt
                                                </option>
                                                <option value="2" {{Request::input('status')==2?'selected="selected"':''}}>
                                                    Hủy kích hoạt
                                                </option>
                                                <option value="lock" {{Request::input('status')=='lock'?'selected="selected"':''}}>
                                                    Bị khóa
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row m--margin-bottom-20">
                                        <div class="col-lg-12">
                                            <button class="btn btn-brand m-btn m-btn--icon" type="submit">
												<span>
													<i class="la la-search"></i>
													<span>
														Tìm kiếm
													</span>
												</span>
                                            </button>
                                            &nbsp;&nbsp;
                                            <button class="btn btn-secondary m-btn m-btn--icon" type="button"
                                                    onclick="window.location='{{route('Qrcode::company@index')}}'">
												<span>
													<i class="la la-refresh"></i>
													<span>
														Làm mới
													</span>
												</span>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="m-separator m-separator--md m-separator--dashed"></div>
                                    <h3 style="text-align: left">Thống kê số lượng khách hàng</h3>
                                    <div class="row m--margin-bottom-20" style="text-align: center">
                                        <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile">
                                        </div>
                                        <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile"
                                             style="border: solid #328EAC; margin-right: 5px">
                                            <label><strong>Tổng số khách hàng</strong></label>
                                            <div>{{number_format($company_total,0,".",",")}}</div>
                                        </div>
                                        <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile"
                                             style="border: solid #00CC00; margin-right: 5px">
                                            <label><strong>Đã kích hoạt</strong></label>
                                            <div>{{number_format($company_actived_total,0,".",",")}}</div>
                                        </div>
                                        <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile"
                                             style="border: solid #C97626; margin-right: 5px">
                                            <label><strong>Chờ kích hoạt</strong></label>
                                            <div>{{number_format($company_active_waiting_total,0,".",",")}}</div>
                                        </div>
                                        <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile"
                                             style="border: solid red; margin-right: 5px">
                                            <label><strong>Hủy kích hoạt</strong></label>
                                            <div>{{number_format($company_deactived_total,0,".",",")}}</div>
                                        </div>
                                        <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile">
                                        </div>
                                    </div>

                                    <div class="m-separator m-separator--md m-separator--dashed"></div>
                                </form>
                                <div>
                                    <a href="{{route('Qrcode::batch@index')}}?attachment_status=2"><h5>Có tổng
                                            số {{$active_batches_without_attachments_after_30days}} lô tem thiếu giấy tờ
                                            sau
                                            30 ngày, kể từ ngày kích hoạt!</h5></a>
                                </div>
                                @if (session('success'))
                                    <div class="m-alert m-alert--outline m-alert--square m-alert--outline-2x alert alert-success alert-dismissible fade show"
                                         role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        </button>
                                        {{ session('success') }}
                                    </div>
                                @endif
                                <div class="table-responsive">
                                    <table class="table table-striped- table-bordered table-hover table-checkable"
                                           style="margin-top: 100px; width:2100px;margin-left:auto;margin-right:auto;">
                                        <!--begin::Thead-->
                                        <thead>
                                        <tr>
                                            <th style="font-weight: bold">Id</th>
                                            <th style="width: 400px;font-weight: bold">Tên</th>
                                            <th style="width: 400px;font-weight: bold">Địa chỉ</th>
                                            <th style="font-weight: bold">Tỉnh/TP</th>
                                            <th style="font-weight: bold">Huyện/Quận</th>
                                            <th style="font-weight: bold">Điện thoại</th>
                                            <th style="font-weight: bold">Email</th>
                                            <th style="font-weight: bold">Website</th>
                                            <th style="font-weight: bold">Ngày tạo</th>
                                            <th style="width: 400px;font-weight: bold">Người tạo</th>
                                            <th style="width: 200px;font-weight: bold">Trạng thái</th>
                                            <th style="width: 400px;font-weight: bold">Ghi chú</th>
                                            <th style="width: 100px;font-weight: bold">Số lô tem đã kích hoạt</th>
                                            <th style="width: 100px;font-weight: bold">Số lô tem thiếu giấy tờ</th>
                                            <th style="font-weight: bold">Hành động</th>
                                        </tr>
                                        </thead>
                                        <!--end::Thead-->
                                        <!--begin::Tbody-->
                                        <tbody>
                                        @foreach($companies as $row)
                                            <tr>
                                                <td>{{$row->id}}</td>
                                                <td>
                                                    <a href="{{route('Qrcode::batch@index')}}?account_id={{$row->id}}">{{$row->name}}</a>
                                                </td>
                                                <td>{{$row->address}}</td>
                                                <td>{{$row->city_name}}</td>
                                                <td>{{$row->district_name}}</td>
                                                <td>{{$row->phone}}</td>
                                                <td>{{$row->email}}</td>
                                                <td>{{$row->website}}</td>
                                                <td>{{date("d/m/Y",strtotime($row->created_at))}}</td>
                                                <td>
                                                    @php
                                                        $a = 1;
                                                        foreach($collaborators as $collaborator){
                                                           if($row->collaborator_phone == $collaborator->collaborator_phone)
                                                           {
                                                                $a++;
                                                               echo $collaborator->name;
                                                                 break;
                                                            }else{
                                                                continue;
                                                            }
                                                        }
                                                        if($a==1){
                                                            echo 'iCheck';}
                                                    @endphp
                                                </td>
                                                <td>
                                                    @if($row->status==1 && $row->deleted_at==null)
                                                        <a href="{{route('Qrcode::company@disapprove',[$row->id])}}"><span
                                                                    class="m-badge m-badge--success m-badge--wide">Kích hoạt </span></a>
                                                    @elseif($row->status==2 && $row->deleted_at==null)
                                                        <a href="{{route('Qrcode::company@approve',[$row->id])}}"><span
                                                                    class="m-badge m-badge--danger m-badge--wide">Hủy kích hoạt </span></a>
                                                    @elseif($row->status==0 && $row->deleted_at==null)
                                                        <a href="{{route('Qrcode::company@approve',[$row->id])}}"><span
                                                                    class="m-badge m-badge--warning m-badge--wide">Chờ kích hoạt </span></a>
                                                    @elseif($row->deleted_at!=null)
                                                        <span class="m-badge m-badge--danger m-badge--wide">Bị khóa</span>
                                                    @endif
                                                </td>
                                                <td>{{$row->note}}</td>
                                                <td>
                                                    @php
                                                        $i=0;
                                                        foreach($active_batches as $batch){
                                                            if($row->id == $batch->user_id){
                                                                $i++;
                                                            }
                                                        }
                                                        echo $i;
                                                    @endphp
                                                </td>
                                                <td>
                                                    @php
                                                        $i=0;
                                                        foreach($active_batches_without_attachments as $batch){
                                                            if($row->id == $batch->user_id){
                                                                $i++;
                                                            }
                                                        }
                                                        echo $i;
                                                    @endphp
                                                </td>
                                                <td>
                                                    <div class="m-dropdown m-dropdown--up m-dropdown--inline m-dropdown--small m-dropdown--arrow m-dropdown--align-right"
                                                         m-dropdown-toggle="hover">
                                                        <a class="m-dropdown__toggle btn btn-brand dropdown-toggle">
                                                        </a>
                                                        <div class="m-dropdown__wrapper">
                                                            <div class="m-dropdown__inner">
                                                                <div class="m-dropdown__body" style="padding:10px">
                                                                    <div class="m-dropdown__content">
                                                                        <ul class="m-nav">
                                                                            @if($row->deleted_at==null)
                                                                                @can("QRCODE-lockBusiness")
                                                                                    <li class="m-nav__item"
                                                                                        style="margin-bottom: 5px">
                                                                                        <a href="{{route('Qrcode::company@lock',[$row->id])}}"
                                                                                           class="m-nav__link">
                                                                                            <i class="m-nav__link-icon la la-unlock"></i>
                                                                                            <span class="m-nav__link-text">Khóa tài khoản</span>
                                                                                        </a>
                                                                                    </li>
                                                                                @endcan
                                                                            @else
                                                                                @can("QRCODE-unlockBusiness")
                                                                                    <li class="m-nav__item"
                                                                                        style="margin-bottom: 5px">
                                                                                        <a href="{{route('Qrcode::company@unlock',[$row->id])}}"
                                                                                           class="m-nav__link">
                                                                                            <i class="m-nav__link-icon la la-unlock-alt"></i>
                                                                                            <span class="m-nav__link-text">Mở khóa tài khoản</span>
                                                                                        </a>
                                                                                    </li>
                                                                                @endcan
                                                                            @endif
                                                                            {{--  <li class="m-nav__separator m-nav__separator--fit"></li>--}}
                                                                            <li class="m-nav__item"
                                                                                style="margin-bottom: 5px">
                                                                                <a data-toggle="modal" href=""
                                                                                   data-target="#information_detail{{$row->id}}"
                                                                                   class="m-nav__link">
                                                                                    <i class="m-nav__link-icon flaticon-file"></i>
                                                                                    <span class="m-nav__link-text">Thông tin hồ sơ</span>
                                                                                </a>
                                                                            </li>
                                                                            @can("QRCODE-updateBusiness")
                                                                                <li class="m-nav__item"
                                                                                    style="margin-bottom: 5px">
                                                                                    <a href="{{route('Qrcode::company@edit',[$row->id])}}"
                                                                                       class="m-nav__link">
                                                                                        <i class="m-nav__link-icon la la-edit"></i>
                                                                                        <span class="m-nav__link-text">Sửa thông tin tài khoản</span>
                                                                                    </a>
                                                                                </li>
                                                                            @endcan
                                                                            @can("QRCODE-noteBusiness")
                                                                                <li class="m-nav__item"
                                                                                    style="margin-bottom: 5px">
                                                                                    <a data-toggle="modal" href=""
                                                                                       data-target="#note{{$row->id}}"
                                                                                       class="m-nav__link">
                                                                                        <i class="m-nav__link-icon flaticon-notes"></i>
                                                                                        <span class="m-nav__link-text">Ghi chú</span>
                                                                                    </a>
                                                                                </li>
                                                                            @endcan
                                                                            @can("QRCODE-viewInboxBusiness")
                                                                                <li class="m-nav__item"
                                                                                    style="margin-bottom: 5px">
                                                                                    <a data-toggle="modal" href=""
                                                                                       data-target="#message{{$row->id}}"
                                                                                       onclick="getMessages(10,{{$row->id}})"
                                                                                       class="m-nav__link">
                                                                                        <i class="m-nav__link-icon flaticon-mail"></i>
                                                                                        <span class="m-nav__link-text">Tin nhắn đã gửi</span>
                                                                                    </a>
                                                                                </li>
                                                                            @endcan
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <span class="m-dropdown__arrow m-dropdown__arrow--right"></span>
                                                        </div>
                                                    </div>
                                                    <!--end: Dropdown-->
                                                    <!--Modal Comfirm delete-->
                                                {{--<div id="lock{{$row->id}}" class="modal fade"
                                                     role="dialog">
                                                    <div class="modal-dialog">
                                                        <!-- Modal content-->
                                                        <div class="modal-content">
                                                            <div class="modal-header">

                                                                <h2 class="modal-title">Bạn chắc
                                                                    chắn muốn khóa tài khoản của doanh
                                                                    nghiệp {{$row->name}}?</h2>
                                                                <button type="button" class="close"
                                                                        data-dismiss="modal">&times;
                                                                </button>
                                                            </div>
                                                            <form method="get"
                                                                  action="{{ route('Qrcode::company@delete',[$row->id]) }}">
                                                                {{ csrf_field() }}
                                                                <div class="modal-footer">
                                                                    <button type="submit"
                                                                            class="btn btn-success">
                                                                        Đồng ý
                                                                    </button>
                                                                    <button type="button"
                                                                            class="btn btn-default"
                                                                            data-dismiss="modal">Hủy
                                                                        yêu cầu
                                                                    </button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>--}}
                                                <!--End Modal-->
                                                    <!--Modal Information_detail-->
                                                    <div id="information_detail{{$row->id}}" class="modal fade test"
                                                         role="dialog">
                                                        <div class="modal-dialog modal-lg" role="document">
                                                            <!-- Modal content-->
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h3>Thông tin chi tiết doanh
                                                                        nghiệp:"{{$row->name}}"</h3>
                                                                    <button type="button" class="close"
                                                                            data-dismiss="modal">&times;
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    @if($row->attachments!=null)
                                                                        @php
                                                                            $attachments = explode(',',$row->attachments);
                                                                        @endphp
                                                                        @for ($i = 0; $i<count($attachments); $i++)
                                                                            @php
                                                                                $attachment_split = explode('/',$attachments[$i]);
                                                                                $attachment_name = end($attachment_split);@endphp
                                                                            @if($attachment_name!='')
                                                                                <div>
                                                                                    <a href="{{$attachments[$i]}}">{{$attachment_name}}</a>
                                                                                </div>;
                                                                            @endif
                                                                        @endfor
                                                                    @else
                                                                        {{'Chưa có dữ liệu'}}
                                                                    @endif
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" data-toggle="modal"
                                                                            data-target="#inbox{{$row->id}}"
                                                                            class="btn btn-success">
                                                                        Gửi thông báo
                                                                    </button>
                                                                    <button type="button"
                                                                            class="btn btn-default"
                                                                            data-dismiss="modal">Trở về
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--End Modal-->
                                                    <!--Modal Inbox-->
                                                    <div id="inbox{{$row->id}}" class="modal fade test"
                                                         role="dialog">
                                                        <div class="modal-dialog" role="document">
                                                            <!-- Modal content-->
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h3>Gửi thông báo tới doanh
                                                                        nghiệp:"{{$row->name}}"</h3>
                                                                    <button type="button" class="close"
                                                                            data-dismiss="modal">&times;
                                                                    </button>
                                                                </div>
                                                                <form method="post"
                                                                      action="{{ route('Qrcode::company@inbox',[$row->id]) }}">
                                                                    {{ csrf_field() }}
                                                                    <div class="modal-body">
                                                                   <textarea class="form-control" rows="5"
                                                                             placeholder="Nhập tin nhắn"
                                                                             name="message" required></textarea>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="submit"
                                                                                class="btn btn-success">
                                                                            Gửi đi
                                                                        </button>
                                                                        <button type="button"
                                                                                class="btn btn-default"
                                                                                data-dismiss="modal">Hủy bỏ
                                                                        </button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--End Modal-->
                                                    <!--Modal Note-->
                                                    <div id="note{{$row->id}}" class="modal fade test"
                                                         role="dialog">
                                                        <div class="modal-dialog modal-lg" role="document">
                                                            <!-- Modal content-->
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h3>Nhập ghi chú cho doanh nghiệp:"{{$row->name}}
                                                                        "</h3>
                                                                    <button type="button" class="close"
                                                                            data-dismiss="modal">&times;
                                                                    </button>
                                                                </div>
                                                                <form method="post"
                                                                      action="{{ route('Qrcode::company@note',[$row->id]) }}">
                                                                    {{ csrf_field() }}
                                                                    <div class="modal-body">
                                                                   <textarea class="form-control" rows="5"
                                                                             placeholder="Nhập ghi chú"
                                                                             name="note"
                                                                             required>{{$row->note}}</textarea>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="submit"
                                                                                class="btn btn-success">
                                                                            Lưu lại
                                                                        </button>
                                                                        <button type="button"
                                                                                class="btn btn-default"
                                                                                data-dismiss="modal">Hủy bỏ
                                                                        </button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--End Modal-->
                                                    <!--begin::Message Modal-->
                                                    <div class="modal fade" id="message{{$row->id}}" tabindex="-1"
                                                         role="dialog"
                                                         aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title">
                                                                        <strong>Danh sách tin nhắn đã gửi </strong><span
                                                                                id="list-message-modal-title{{$row->id}}"></span>
                                                                    </h5>
                                                                    <button type="button" class="close"
                                                                            data-dismiss="modal"
                                                                            aria-label="Close">
											<span aria-hidden="true">
												&times;
											</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body"
                                                                     style="height:200px;overflow-y: scroll;"
                                                                     id="content_modal{{$row->id}}">
                                                                    <span>{{"Đang tải "}}</span>
                                                                    <span class="m-loader m-loader--brand"
                                                                          style="width: 100px; display: inline-block;"></span>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-secondary"
                                                                            data-dismiss="modal">
                                                                        Đóng
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--end::Modal-->
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                        <!--end::Tbody-->
                                    </table>
                                </div>
                                <!--end::Table-->
                                <div style="float:right;"><?php echo $companies->links(); ?></div>
                            </div>
                            <!--End::Section-->
                        </div>
                    </div>
                @endcan
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript" src="{{ asset('qr_code/metronic/js/vendors.bundle.js') }}"></script>
    <script type="text/javascript" src="{{ asset('qr_code/metronic/js/datepicker/bootstrap-datepicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('qr_code/metronic/js/datatables/datatables.bundle.js') }}"></script>
    <script type="text/javascript" src="{{ asset('qr_code/metronic/js/datatables/scrollable.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            // Basic
            $("#city").select2({
                placeholder: 'Tỉnh/thành phố',
                allowClear: true
            });
            $("#district").select2({
                placeholder: 'Huyện/quận',
                allowClear: true
            });
            $("#status").select2({
                placeholder: 'Trạng thái',
                allowClear: true
            });
            $("#user").select2({
                placeholder: 'Người tạo',
                allowClear: true
            });
            $("select").on("select2:select", function (evt) {
                var element = evt.params.data.element;
                var $element = $(element);
                $element.detach();
                $(this).append($element);
                $(this).trigger("change");
            });


        });
    </script>

    <script type="text/javascript">
        var url = "{{ route('Qrcode::district@showDistrictsInCity')}}";
        $("select[name='account_city_id']").change(function () {
            var city_id = $(this).val();
            var token = $("input[name='_token']").val();
            $.ajax({
                url: url,
                method: 'POST',
                data: {
                    city_id: city_id,
                    _token: token
                },
                success: function (data) {
                    $("select[name='account_district_id']").html('');
                    $("select[name='account_district_id']").append(
                        "<option value=''>" + "" + "</option>"
                    );
                    $.each(data, function (key, value) {
                        $("select[name='account_district_id']").append(
                            "<option value=" + value.id + ">" + value.name + "</option>"
                        );
                    });
                }
            });
        });
    </script>
    <script>
        function getMessages(limit, account_id) {
            var url = "{{ route('Qrcode::company@admin-inbox')}}";
            $.ajax({
                url: url,
                method: 'GET',
                data: {
                    user: account_id,
                    limit: limit
                },
                success: function (data) {
                    $(`#content_modal${account_id}`).html('');
                    $(`#list-message-modal-title${account_id}`).html('');
                    var total = data.data.total;
                    $(`#list-message-modal-title${account_id}`).append(
                        "<span>" + "(" + total + ")" + "</span>"
                    );
                    $.each(data.data.messages, function (key, value) {
                        value.ins_dt = value.ins_dt.slice(0, 10);
                        $(`#content_modal${account_id}`).append(
                            "<p><i class=\"flaticon-multimedia-5\"></i>" + " " + value.message + " " + "(" + value.ins_dt + ")" + "<p>"
                        );
                    });
                    $(`#content_modal${account_id}`).append(
                        "<button type=\"button\" class=\"btn\" id = \"more\" onclick=\"getMessages(" + limit + " + 10, " + account_id + ")\" style=\"border: none;\">" + "Xem thêm" + "</button>"
                    );
                    if (limit > total) {
                        $("#more").hide();
                    }
                }
            });
        }
    </script>
@endpush
