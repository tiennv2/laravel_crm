@extends('qrcode.layouts.app')
@section('body_class', 'm-page--fluid m-page--loading-enabled m-page--loading m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default')
@push('scripts')
    <script type="text/javascript">
        <?php
            $sharedData = [
                'config' => [
                    'csrfToken' => csrf_token(),
                    'logoutUrl' => route('logout'),
                    'isNative' => true,
                ],
                'loggedInAccount' => auth()->check() ? auth()->user() : null,
            ];
            ?>
            window.SHARED_DATA = {!! json_encode($sharedData) !!}
    </script>
    <script src="https://cdn.jsdelivr.net/npm/tinymce@4.7.11/tinymce.min.js" type="text/javascript"></script>
    <script src="{{ mix('js/app.js') }}"></script>
@endpush

@section('content')
    <!-- begin::Page loader -->
    <div class="m-page-loader m-page-loader--base">
        <div class="m-blockui">
      <span>
        Please wait...
      </span>
            <span>
        <div class="m-loader m-loader--brand"></div>
      </span>
        </div>
    </div>
    <!-- end::Page Loader -->
    <div id="app" class="m-grid m-grid--hor m-grid--root m-page">
        <app-header></app-header>
        <div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop 	m-container m-container--responsive m-container--xxl m-page__container m-body">
            <div class="m-grid__item m-grid__item--fluid m-wrapper">
                <div class="m-content">
                    <div class="m-portlet">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text">
                                        <a href="{{route('Qrcode::company@index')}}">
                                            <i class="la la-arrow-left"></i>
                                        </a>
                                        {{ isset($company) ? 'Sửa thông tin doanh nghiệp ' : 'Thêm mới doanh nghiệp' }}
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <!-- /page header -->
                        @if (session('success'))
                            <div class="m-alert m-alert--outline m-alert--square m-alert--outline-2x alert alert-success alert-dismissible fade show"
                                 role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                </button>
                                {{ session('success') }}
                            </div>
                        @endif
                        <form method="POST" enctype="multipart/form-data" name="form"
                              class="m-form m-form--fit m-form--label-align-right"
                              action="{{ isset($company) ? route('Qrcode::company@update', [$company->id] ): route('Qrcode::company@store') }}">
                            {{ csrf_field() }}
                            @if (isset($company))
                                <input type="hidden" name="_method" value="PUT">
                            @endif
                            <div class="m-portlet__body">
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">Tên
                                        đăng nhập *</label>
                                    <div class="col-lg-4 col-md-9 col-sm-12">
                                        <input type="text" id="username" name="username"
                                               class="form-control" {{(isset($company))?'disabled' :''}}
                                               value="{{ old('username') ?: @$company->username }}"
                                               required/>
                                    </div>
                                    @if ($errors->has('username'))
                                        <div class="form-control-feedback">
                                            <i class="icon-notification2"></i>
                                        </div>
                                        <div class="help-block"
                                             style="color: red">{{ $errors->first('username') }}</div>
                                    @endif
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">Tên công
                                        ty *</label>
                                    <div class="col-lg-4 col-md-9 col-sm-12">
                                        <input type="text" id="name" name="name" class="form-control"
                                               value="{{ old('name') ?: @$company->name }}" required/>
                                    </div>
                                    @if ($errors->has('name'))
                                        <div class="form-control-feedback">
                                            <i class="icon-notification2"></i>
                                        </div>
                                        <div class="help-block" style="color: red">{{ $errors->first('name') }}</div>
                                    @endif
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">Địa
                                        chỉ *</label>
                                    <div class="col-lg-4 col-md-9 col-sm-12">
                                        <input type="text" id="address" name="address"
                                               class="form-control"
                                               value="{{ old('address') ?: @$company->address}}"
                                               required/>
                                    </div>
                                    @if ($errors->has('address'))
                                        <div class="form-control-feedback">
                                            <i class="icon-notification2"></i>
                                        </div>
                                        <div class="help-block" style="color: red">{{ $errors->first('address') }}</div>
                                    @endif
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">Phone *</label>
                                    <div class="col-lg-4 col-md-9 col-sm-12">
                                        <input type="text" id="phone" name="phone" class="form-control"
                                               value="{{ old('phone') ?: @$company->phone}}" required/>
                                    </div>
                                    @if ($errors->has('phone'))
                                        <div class="form-control-feedback">
                                            <i class="icon-notification2"></i>
                                        </div>
                                        <div class="help-block" style="color: red">{{ $errors->first('phone') }}</div>
                                    @endif
                                </div>
                                <!------------------ Email--------------->
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">Email *</label>
                                    <div class="col-lg-4 col-md-9 col-sm-12">
                                        <input type="email" id="email" name="email" class="form-control"
                                               value="{{ old('email') ?: @$company->email }}" required/>
                                    </div>
                                    @if ($errors->has('email'))
                                        <div class="form-control-feedback">
                                            <i class="icon-notification2"></i>
                                        </div>
                                        <div class="help-block" style="color: red">{{ $errors->first('email') }}</div>
                                    @endif
                                </div>
                                <!------------------ Website--------------->
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">Website </label>
                                    <div class="col-lg-4 col-md-9 col-sm-12">
                                        <input type="text" id="website" name="website"
                                               class="form-control"
                                               value="{{ old('website') ?: @$company->website }}"/>
                                    </div>
                                    @if ($errors->has('website'))
                                        <div class="form-control-feedback">
                                            <i class="icon-notification2"></i>
                                        </div>
                                        <div class="help-block" style="color: red">{{ $errors->first('website') }}</div>
                                    @endif
                                </div>
                                <!------------------------- City-------------------->
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">Chọn
                                        tỉnh/thành phố *</label>
                                    <div class="col-lg-4 col-md-9 col-sm-12">
                                        <select name="city_id" class="form-control" id="city"
                                                data-placeholder="Chọn thành phố">
                                            <option value="" selected="selected">--Chọn--</option>
                                            @foreach($cities as $city)
                                                <option value="{{$city->id}}" {{(isset($company) and ($city->id==$company->city_id))?'selected="selected"' :''}}>{{$city->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @if ($errors->has('city_id'))
                                        <div class="form-control-feedback">
                                            <i class="icon-notification2"></i>
                                        </div>
                                        <div class="help-block"
                                             style="color: red">{{ $errors->first('city_id') }}</div>
                                    @endif
                                </div>

                                <!-------------------------District-------------------->
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">Chọn
                                        quận/huyện *</label>
                                    <div class="col-lg-4 col-md-9 col-sm-12">
                                        <select name="district_id" class="form-control" id="district"
                                                data-placeholder="Chọn quận/huyện">
                                            @if(isset($company))
                                                @foreach($districts as $district)
                                                    <option value="{{$district->id}}" {{(($district->id==$company->district_id))?'selected="selected"' :''}}>{{$district->name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                    @if ($errors->has('district_id'))
                                        <div class="form-control-feedback">
                                            <i class="icon-notification2"></i>
                                        </div>
                                        <div class="help-block"
                                             style="color: red">{{ $errors->first('district_id') }}</div>
                                    @endif
                                </div>
                                <!------------------------- Password-------------------->

                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">Mật
                                        khẩu *</label>
                                    <div class="col-lg-4 col-md-9 col-sm-12">
                                        <input type="password" id="password" name="password"
                                               class="form-control" {{(!isset($company))?'required' :''}}/>
                                    </div>
                                    @if ($errors->has('password'))
                                        <div class="form-control-feedback">
                                            <i class="icon-notification2"></i>
                                        </div>
                                        <div class="help-block"
                                             style="color: red">{{ $errors->first('password') }}</div>
                                    @endif
                                </div>

                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">Xác nhận
                                        Mật khẩu *</label>
                                    <div class="col-lg-4 col-md-9 col-sm-12">
                                        <input type="password" id="confirm_password"
                                               name="confirm_password"
                                               class="form-control" {{(!isset($company))?'required' :''}}/>
                                    </div>
                                    @if ($errors->has('confirm_password'))
                                        <div class="form-control-feedback">
                                            <i class="icon-notification2"></i>
                                        </div>
                                        <div class="help-block"
                                             style="color: red">{{ $errors->first('confirm_password') }}</div>
                                    @endif
                                </div>
                                <div class="m-portlet__foot m-portlet__foot--fit">
                                    <div class="m-form__actions m-form__actions">
                                        <div class="row">
                                            <div class="col-lg-9 ml-lg-auto">
                                                <button type="submit" class="btn btn-success">
                                                    {{ isset($company) ? 'Cập nhật' : 'Thêm mới' }}
                                                </button>
                                                <button type="reset" class="btn btn-secondary">
                                                    Nhập lại
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <router-view></router-view>
@endsection

@push('scripts')
    <script type="text/javascript" src="{{ asset('qr_code/metronic/js/vendors.bundle.js') }}"></script>
    <script type="text/javascript" src="{{ asset('qr_code/metronic/js/scripts.bundle.js') }}"></script>
    <script>
        $(document).ready(function () {
            // Basic
            $('#city').select2();

        });
        $(document).ready(function () {
            // Basic
            $('#district').select2();

        });
    </script>

    <script type="text/javascript">
        var url = "{{ route('Qrcode::district@showDistrictsInCity')}}";
        $("select[name='city_id']").change(function () {
            var city_id = $(this).val();
            var token = $("input[name='_token']").val();
            $.ajax({
                url: url,
                method: 'POST',
                data: {
                    city_id: city_id,
                    _token: token
                },
                success: function (data) {
                    $("select[name='district_id']").html('');
                    $("select[name='district_id']").append(
                        "<option value=''" + ">" + '--Chọn quận/huyện--' + "</option>"
                    );
                    $.each(data, function (key, value) {
                        $("select[name='district_id']").append(
                            "<option value=" + value.id + ">" + value.name + "</option>"
                        );
                    });
                }
            });
        });
    </script>
@endpush
