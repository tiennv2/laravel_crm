<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title>Welcome to iCheck</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
</head>
<body>
<p><strong>Chào mừng bạn đến với Hệ thống tem xác thực điện tử iCheck!</strong></p>
<p>Tài khoản đã đăng ký với tên đăng nhập: "{{$accountName}}" của bạn đã được duyệt. Tiếp tục đăng nhập để sử dụng.</p>
<p>Click vào link <a href="https://qrcode.icheck.vn:86/login">https://qrcode.icheck.vn:86/login</a> để đăng nhập Qrcode</p>

</body>
<!-- END BODY -->
</html>