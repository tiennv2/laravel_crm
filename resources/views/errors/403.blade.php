@extends('layouts.app')

@section('body_class', 'm--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default')

@section('content')
  <!-- begin:: Page -->
  <div class="m-grid m-grid--hor m-grid--root m-page">
    <div class="m-grid__item m-grid__item--fluid m-grid  m-error-1" style="background-image: url({{ asset('img/authorization-bg.jpg') }});">
      <div class="m-error_container">
        <p class="m-error_desc" style="font-family: Tahoma sans-serif;margin-top: 200px">
          Xin lỗi, bạn không có quyền thực hiện hành động này!
        </p>
      </div>
    </div>
  </div>
  <!-- end:: Page -->
@endsection
