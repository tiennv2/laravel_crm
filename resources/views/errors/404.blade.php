@extends('layouts.app')

@section('body_class', 'm--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default')

@section('content')
  <!-- begin:: Page -->
  <div class="m-grid m-grid--hor m-grid--root m-page">
    <div class="m-grid__item m-grid__item--fluid m-grid  m-error-6" style="background-image: url({{ asset('img/error-bg.jpg') }});">
      <div class="m-error_container">
        <div class="m-error_subtitle m--font-light">
          <h1>Oops...</h1>
        </div>
        <p class="m-error_description m--font-light">
          Looks like something went wrong.<br>
          We're working on it
        </p>
      </div>
    </div>
  </div>
  <!-- end:: Page -->
@endsection
