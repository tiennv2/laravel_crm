@extends('qrcode.layouts.app')
@push('styles')
    <style>
        .help-block {
            color: red;
        }
    </style>

@endpush
@section('body_class', 'm-page--fluid m-page--loading-enabled m-page--loading m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default')
@push('scripts')
    <script type="text/javascript">
        <?php
            $sharedData = [
                'config' => [
                    'csrfToken' => csrf_token(),
                    'logoutUrl' => route('logout'),
                    'isNative' => true,
                ],
                'loggedInAccount' => auth()->check() ? auth()->user() : null,
            ];
            ?>
            window.SHARED_DATA = {!! json_encode($sharedData) !!}
    </script>
    <script src="https://cdn.jsdelivr.net/npm/tinymce@4.7.11/tinymce.min.js" type="text/javascript"></script>
    <script src="{{ mix('js/app.js') }}"></script>
@endpush

@section('content')
    <!-- begin::Page loader -->
    <div class="m-page-loader m-page-loader--base">
        <div class="m-blockui">
      <span>
        Please wait...
      </span>
            <span>
        <div class="m-loader m-loader--brand"></div>
      </span>
        </div>
    </div>
    <!-- end::Page Loader -->
    <div id="app" class="m-grid m-grid--hor m-grid--root m-page">
        <app-header></app-header>
        <div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop 	m-container m-container--responsive m-container--xxl m-page__container m-body">
            <div class="m-grid__item m-grid__item--fluid m-wrapper">
                <div class="m-content">
                    <div class="m-portlet">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text">
                                        <a href="{{route('User_Management::role@index')}}">
                                            <i class="la la-arrow-left"></i>
                                        </a>
                                        {{ isset($role) ? 'Sửa nhóm quyền' : 'Thêm mới nhóm quyền' }}
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <!-- /page header -->
                        @if (session('success'))
                            <div class="m-alert m-alert--outline m-alert--square m-alert--outline-2x alert alert-success alert-dismissible fade show"
                                 role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                </button>
                                {{ session('success') }}
                            </div>
                        @endif
                        <form method="POST" enctype="multipart/form-data" name="form"
                              class="m-form m-form--fit m-form--label-align-right"
                              action="{{ isset($role) ? route('User_Management::role@update', [$role->id] ): route('User_Management::role@store') }}">
                            {{ csrf_field() }}
                            @if (isset($role))
                                <input type="hidden" name="_method" value="PUT">
                            @endif
                            <div class="m-portlet__body">
                                <!------------------ Name--------------->
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-2 col-sm-12">Tên *</label>
                                    <div class="col-lg-6 col-md-9 col-sm-12">
                                        <input type="text" id="name" name="name"
                                               class="form-control"
                                               value="{{ old('name') ?: @$role->name }}"
                                               required/>
                                    </div>
                                    @if ($errors->has('name'))
                                        <div class="help-block">{{ $errors->first('name') }}</div>
                                    @endif
                                </div>
                                <!------------------ Description--------------->
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-2 col-sm-12">Mô tả </label>
                                    <div class="col-lg-6 col-md-9 col-sm-12">
                                        <input type="text" id="description" name="description"
                                               class="form-control"
                                               value="{{ old('description') ?: @$role->description }}"
                                        />
                                    </div>
                                    @if ($errors->has('description'))
                                        <div class="help-block">{{ $errors->first('description') }}</div>
                                    @endif
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-2 col-sm-12">
                                        Chọn quyền
                                    </label>
                                    <div class="m-checkbox-inline col-lg-9 col-md-9 col-sm-12"
                                         style="margin-left: 14px;">
                                        <div>
                                            <label class="m-checkbox" style="font-weight: bold;font-style: italic">
                                                <input type="checkbox" name="check_all">
                                                Chọn tất cả
                                                <span></span>
                                            </label>
                                        </div>
                                        @foreach($permissions as $permission)
                                            <div style="display: inline-block;width: 320px;vertical-align: top">
                                            <label class="m-checkbox" title="{{$permission->description}}">
                                                <input type="checkbox" name="permissions[]"
                                                       value="{{$permission->id}}"
                                                       @if(isset($rolePermissions) && in_array($permission->id,$rolePermissions))
                                                       checked
                                                        @endif
                                                >
                                                {{$permission->description}}
                                                <span></span>
                                            </label>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="m-portlet__foot m-portlet__foot--fit">
                                    <div class="m-form__actions m-form__actions">
                                        <div class="row">
                                            <div class="col-lg-9 ml-lg-auto">
                                                <button type="submit" class="btn btn-success">
                                                    {{ isset($role) ? 'Cập nhật' : 'Thêm mới' }}
                                                </button>
                                                <button type="reset" class="btn btn-secondary">
                                                    Nhập lại
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script type="text/javascript" src="{{ asset('qr_code/metronic/js/vendors.bundle.js') }}"></script>
    {{--<script type="text/javascript" src="{{ asset('qr_code/metronic/js/scripts.bundle.js') }}"></script>--}}
    <script>
        $(document).ready(function () {
            $('input[name="check_all"]').click(function () {
                let status = $(this).is(':checked');
                $('input[type="checkbox"]').prop('checked', status);
            });
        });
    </script>
@endpush
