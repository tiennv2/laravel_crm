@extends('qrcode.layouts.app')
@section('body_class', 'm-page--fluid m-page--loading-enabled m-page--loading m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default')

@push('scripts')
  <script type="text/javascript">
    <?php
      $sharedData = [
        'config' => [
          'csrfToken' => csrf_token(),
          'logoutUrl' => route('logout'),
          'isNative' => true,
        ],
        'loggedInAccount' => auth()->check() ? auth()->user() : null,
      ];
      ?>
      window.SHARED_DATA = {!! json_encode($sharedData) !!}
  </script>
  <script src="https://cdn.jsdelivr.net/npm/tinymce@4.7.11/tinymce.min.js" type="text/javascript"></script>
  <script src="{{ mix('js/app.js') }}"></script>
@endpush

@section('content')
  <!-- begin::Page loader -->
  <div class="m-page-loader m-page-loader--base">
    <div class="m-blockui">
      <span>
        Please wait...
      </span>
      <span>
        <div class="m-loader m-loader--brand"></div>
      </span>
    </div>
  </div>
  <!-- end::page Loader -->
  <div id="app" class="m-grid m-grid--hor m-grid--root m-page">
    <app-header></app-header>
    <div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop  m-body m-page__container">
      <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader">
          <div class="d-flex align-items-center">
            <div class="mr-auto">
              <h3 class="m-subheader__title">
                QUẢN LÝ NHÓM QUYỀN
              </h3>
            </div>
            <div style="float: right"><a href="{{route('User_Management::role@add')}}"
                                         class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air"><span>
                                    <i class="la la-plus-circle"></i>
                                    <span>Thêm mới nhóm quyền</span></span></a>
            </div>
          </div>
        </div>
        <!-- END: Subheader -->
        <div class="m-content">
          <!--Begin::Section-->
          <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__body">
              @if (session('success'))
                <div
                  class="m-alert m-alert--outline m-alert--square m-alert--outline-2x alert alert-success alert-dismissible fade show"
                  role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  </button>
                  {{ session('success') }}
                </div>
              @endif
              <div class="table-responsive">
                <table class="table table-striped- table-bordered table-hover table-checkable"
                       style="margin-top: 20px;margin-left:auto;margin-right:auto;">
                  <!--begin::Thead-->
                  <thead>
                  <tr>
                    <th style="font-weight: bold">Id</th>
                    <th style="width: 300px;font-weight: bold">Tên</th>
                    <th style="font-weight: bold;width: 300px">Mô tả</th>
                    <th style="font-weight: bold">Ngày tạo</th>
                    <th style="font-weight: bold">Hành động</th>
                  </tr>
                  </thead>
                  <!--end::Thead-->
                  <!--begin::Tbody-->
                  <tbody>
                  @foreach($roles as $role)
                    <tr>
                      <td>{{$role->id}}</td>
                      <td>{{$role->name}}</td>
                      <td>{{$role->description}}</td>
                      <td>{{date("d/m/Y",strtotime($role->created_at))}}</td>
                      <td>
                        @if($role->canEdit)
                          <button type="button"
                                  onclick="window.location='{{route('User_Management::role@edit',[$role->id])}}'"
                                  class="btn m-btn m-btn--pill m-btn--gradient-from-info m-btn--gradient-to-warning">
                            Sửa
                          </button>
                        @endif
                        @can("SYSTEM-delRole")
                          <a href="{{route('User_Management::role@delete',[$role->id])}}"
                            title="Xóa bỏ"
                            onclick="return delItem();"
                            class="btn m-btn m-btn--pill m-btn--gradient-from-danger m-btn--gradient-to-warning">
                            Xóa
                          </a>
                        @endcan
                      </td>
                    </tr>
                  @endforeach
                  </tbody>
                  <!--end::Tbody-->
                </table>
              </div>
              <!--end::Table-->
              <div style="float:right;"><?php echo $roles->links(); ?></div>
            </div>
            <!--End::Section-->
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
@push('scripts')
  <script type="text/javascript" src="{{ asset('qr_code/metronic/js/vendors.bundle.js') }}"></script>
  <script type="text/javascript">
    function delItem() {
      let conf = confirm("Bạn chắc chắn muốn xoá nhóm quyền này không?");
      return conf;
    }
  </script>

@endpush
