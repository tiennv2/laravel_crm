@extends('qrcode.layouts.app')
@section('body_class', 'm-page--fluid m-page--loading-enabled m-page--loading m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default')

@push('scripts')
    <script type="text/javascript">
        <?php
            $sharedData = [
                'config' => [
                    'csrfToken' => csrf_token(),
                    'logoutUrl' => route('logout'),
                    'isNative' => true,
                ],
                'loggedInAccount' => auth()->check() ? auth()->user() : null,
            ];
            ?>
            window.SHARED_DATA = {!! json_encode($sharedData) !!}
    </script>
    <script src="https://cdn.jsdelivr.net/npm/tinymce@4.7.11/tinymce.min.js" type="text/javascript"></script>
    <script src="{{ mix('js/app.js') }}"></script>
@endpush

@section('content')
    <!-- begin::Page loader -->
    <div class="m-page-loader m-page-loader--base">
        <div class="m-blockui">
      <span>
        Please wait...
      </span>
            <span>
        <div class="m-loader m-loader--brand"></div>
      </span>
        </div>
    </div>
    <!-- end::page Loader -->
    <div id="app" class="m-grid m-grid--hor m-grid--root m-page">
        <app-header></app-header>
        <div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop  m-body m-page__container">
            <div class="m-grid__item m-grid__item--fluid m-wrapper">
                <!-- BEGIN: Subheader -->
                <div class="m-subheader">
                    <div class="d-flex align-items-center">
                        <div class="mr-auto">
                            <h3 class="m-subheader__title">
                                QUẢN LÝ NGƯỜI DÙNG HỆ THỐNG
                            </h3>
                        </div>
                        @can("SYSTEM-addUser")
                            <div style="float: right"><a href="{{route('User_Management::user@add')}}"
                                                         class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air"><span>
                                    <i class="la la-plus-circle"></i>
                                    <span>Thêm mới người dùng</span></span></a>
                            </div>
                        @endcan
                    </div>
                </div>
                <!-- END: Subheader -->
                @can("SYSTEM-viewUser")
                    <div class="m-content">
                        <!--Begin::Section-->
                        <div class="m-portlet m-portlet--mobile">
                            <div class="m-portlet__body">
                                <form class="m-form m-form--fit m--margin-bottom-20"
                                      action="{{route('User_Management::user@index')}}" method="get">
                                    <div class="row m--margin-bottom-20">
                                        <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                                            <input type="text" class="form-control m-input" name="name"
                                                   value="{{ Request::input('name') }}"
                                                   placeholder="Tìm kiếm theo tên người dùng"
                                                   data-col-index="0">
                                        </div>
                                        <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                                            <input type="text" class="form-control m-input" name="icheck_id"
                                                   value="{{ Request::input('icheck_id') }}"
                                                   placeholder="Tìm kiếm ICHECK_ID"
                                                   data-col-index="0">
                                        </div>
                                        <div class="col-lg-4 m--margin-bottom-10-tablet-and-mobile">
                                            <button class="btn btn-brand m-btn m-btn--icon" type="submit">
												<span>
													<i class="la la-search"></i>
													<span>
														Tìm kiếm
													</span>
												</span>
                                            </button>
                                            &nbsp;&nbsp;
                                            <button class="btn btn-secondary m-btn m-btn--icon" type="button"
                                                    onclick="window.location='{{route('User_Management::user@index')}}'">
												<span>
													<i class="la la-refresh"></i>
													<span>
														Làm mới
													</span>
												</span>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                                @if (session('success'))
                                    <div class="m-alert m-alert--outline m-alert--square m-alert--outline-2x alert alert-success alert-dismissible fade show"
                                         role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        </button>
                                        {{ session('success') }}
                                    </div>
                                @endif
                                <div class="table-responsive">
                                    <table class="table table-striped- table-bordered table-hover table-checkable"
                                           style="margin-top: 20px;margin-left:auto;margin-right:auto;">
                                        <!--begin::Thead-->
                                        <thead>
                                        <tr>
                                            <th style="font-weight: bold">Id</th>
                                            <th style="width: 300px;font-weight: bold">Icheck_ID</th>
                                            <th style="font-weight: bold;width: 300px">Tên</th>
                                            <th style="font-weight: bold;width: 300px">Nhóm quyền</th>
                                            <th style="font-weight: bold">Ngày tạo</th>
                                            <th style="font-weight: bold">Hành động</th>
                                        </tr>
                                        </thead>
                                        <!--end::Thead-->
                                        <!--begin::Tbody-->
                                        <tbody>
                                        @foreach($users as $user)
                                            <tr>
                                                <td>{{$user->id}}</td>
                                                <td>{{$user->icheck_id}}</td>
                                                <td>{{$user->name}}</td>
                                                <td>
                                                    @foreach($user->role_names as $role_name)
                                                        {{$role_name}};
                                                    @endforeach
                                                </td>
                                                <td>{{date("d/m/Y",strtotime($user->created_at))}}</td>
                                                <td>
                                                    @can("SYSTEM-updateUser")
                                                        <button type="button"
                                                                onclick="window.location='{{route('User_Management::user@edit',[$user->id])}}'"
                                                                class="btn m-btn m-btn--pill m-btn--gradient-from-info m-btn--gradient-to-warning">
                                                            Phân quyền
                                                        </button>
                                                    @endcan
                                                    @can("SYSTEM-delUser")
                                                        <a onclick="return delUser();"
                                                           href="{{route('User_Management::user@delete',[$user->id])}}"
                                                           class="btn m-btn m-btn--pill m-btn--gradient-from-danger m-btn--gradient-to-warning">
                                                            Xóa bỏ
                                                        </a>
                                                    @endcan
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                        <!--end::Tbody-->
                                    </table>
                                </div>
                                <!--end::Table-->
                                <div style="float:right;"><?php echo $users->links(); ?></div>
                            </div>
                            <!--End::Section-->
                        </div>
                    </div>
                @endcan
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript" src="{{ asset('qr_code/metronic/js/vendors.bundle.js') }}"></script>
    <script type="text/javascript">
        function delUser() {
            var conf = confirm("Bạn chắc chắn muốn xoá người dùng này?");
            return conf;
        }
    </script>

@endpush
