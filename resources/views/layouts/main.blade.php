<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title>iCheck | Admin</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet"
          type="text/css"/>
    <link href="{{asset('qr_code/global/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{asset('qr_code/global/plugins/simple-line-icons/simple-line-icons.min.css')}}"
          rel="stylesheet" type="text/css"/>
    <link href="{{asset('qr_code/global/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{asset('qr_code/global/plugins/uniform/css/uniform.default.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{asset('qr_code/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css')}}}"
          rel="stylesheet" type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link rel="stylesheet" type="text/css" href="{{asset('qr_code/global/plugins/select2/select2.css')}}"/>
    <link rel="stylesheet" type="text/css"
          href="{{asset('qr_code/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css')}}"/>
    <link rel="stylesheet" type="text/css"
          href="{{asset('qr_code/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css')}}"/>
    {{--<link rel="stylesheet" type="text/css" href="{{asset('qr_code/global/plugins/clockface/css/clockface.css')}}"/>--}}
    <link rel="stylesheet" type="text/css" href="{{asset('qr_code/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('qr_code/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('qr_code/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('qr_code/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css')}}"/>
    <!-- END PAGE LEVEL STYLES -->
    <!-- BEGIN THEME STYLES -->
    <link href="{{asset('qr_code/global/css/components.css')}}" id="style_components" rel="stylesheet"
          type="text/css"/>
    <link href="{{asset('qr_code/global/css/plugins.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('qr_code/admin/layout/css/layout.css')}}" rel="stylesheet" type="text/css"/>
    <link id="style_color" href="{{asset('qr_code/admin/layout/css/themes/darkblue.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{asset('qr_code/admin/layout/css/custom.css')}}" rel="stylesheet" type="text/css"/>
    {{--<link rel="stylesheet" type="text/css" href="{{asset('qr_code/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}"/>--}}
    <!-- END THEME STYLES -->
   {{-- <link rel="shortcut icon" href="favicon.ico"/>--}}
    <!--AFFILIATE-->
    <link href="{{asset('affiliate/css/bootstrap-select.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('affiliate/css/multi-select.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('affiliate/css/select2.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('affiliate/css/select2-bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
    <style>
        label {
            font-weight: bold;
        }
        th,td{
            text-align: center;
        }
    </style>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
<!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
<!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
<!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
<!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
<!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
<!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<!-- BEGIN HEADER -->
@include("partials/header")
<!-- END HEADER -->
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN SIDEBAR -->
@include("partials/sidebar")
    <!-- END SIDEBAR -->
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
        @yield('content')
            <!-- END PAGE CONTENT-->
        </div>
    </div>
    <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
@include("partials/footer")
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<script src="{{asset('qr_code/global/plugins/jquery.min.js')}}" type="text/javascript"></script>
<script src="{{asset('qr_code/global/plugins/jquery-migrate.min.js')}}" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="{{asset('qr_code/global/plugins/jquery-ui/jquery-ui.min.js')}}" type="text/javascript"></script>
<script src="{{asset('qr_code/global/plugins/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('qr_code/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js')}}"
        type="text/javascript"></script>
<script src="{{asset('qr_code/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}"
        type="text/javascript"></script>
<script src="{{asset('qr_code/global/plugins/jquery.blockui.min.js')}}" type="text/javascript"></script>
<script src="{{asset('qr_code/global/plugins/jquery.cokie.min.js')}}" type="text/javascript"></script>
<script src="{{asset('qr_code/global/plugins/uniform/jquery.uniform.min.js')}}" type="text/javascript"></script>
<script src="{{asset('qr_code/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}"
        type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="{{asset('qr_code/global/plugins/select2/select2.min.js')}}"></script>
<script type="text/javascript" src="{{asset('qr_code/global/plugins/bootstrap-select/bootstrap-select.min.js')}}"></script>
<script type="text/javascript" src="{{asset('qr_code/global/plugins/jquery-multi-select/js/jquery.multi-select.js')}}"></script>
{{--<script type="text/javascript"
        src="{{asset('qr_code/global/plugins/datatables/media/js/jquery.dataTables.min.js')}}"></script>--}}
<script type="text/javascript"
        src="{{asset('qr_code/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js')}}"></script>
<script type="text/javascript" src="{{asset('qr_code/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js')}}"></script>
<script type="text/javascript" src="{{asset('qr_code/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
<script type="text/javascript" src="{{asset('qr_code/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}"></script>
{{--<script type="text/javascript" src="{{asset('qr_code/global/plugins/clockface/js/clockface.js')}}"></script>--}}
<script type="text/javascript" src="{{asset('qr_code/global/plugins/bootstrap-daterangepicker/moment.min.js')}}"></script>
<script type="text/javascript" src="{{asset('qr_code/global/plugins/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('qr_code/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')}}"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{asset('qr_code/global/scripts/metronic.js')}}" type="text/javascript"></script>
<script src="{{asset('qr_code/admin/layout/scripts/layout.js')}}" type="text/javascript"></script>
<script src="{{asset('qr_code/admin/layout/scripts/demo.js')}}" type="text/javascript"></script>
<script src="{{asset('qr_code/admin/pages/scripts/table-advanced.js')}}" type="text/javascript"></script>
<script src="{{asset('qr_code/admin/layout/scripts/quick-sidebar.js')}}" type="text/javascript"></script>
<script src="{{asset('qr_code/admin/pages/scripts/components-dropdowns.js')}}"></script>
<script src="{{asset('qr_code/admin/pages/scripts/components-pickers.js')}}"></script>
<!--AFILIATE -->
<script src="{{asset('affiliate/js/bootstrap-select.min.js')}}"></script>
<script src="{{asset('affiliate/js/jquery.multi-select.js')}}"></script>
<script src="{{asset('affiliate/js/select2.full.min.js')}}"></script>
<script src="{{asset('affiliate/js/components-multi-select.min.js')}}"></script>

<script>
    jQuery(document).ready(function () {
        Metronic.init(); // init metronic core components
        Layout.init(); // init current layout
        QuickSidebar.init(); // init quick sidebar
        Demo.init(); // init demo features
        TableAdvanced.init();
        ComponentsDropdowns.init();
        ComponentsPickers.init();
    });
</script>
<script>

</script>
</body>
<!-- END BODY -->
</html>