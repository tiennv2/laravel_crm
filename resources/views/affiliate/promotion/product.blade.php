@extends('qrcode.layouts.app')
@push("styles")
    <style>
        label {
            font-weight: bold;
        }

        th, td {
            text-align: center;
        }
    </style>
@endpush
@section('body_class', 'm-page--fluid m-page--loading-enabled m-page--loading m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default')

@push('scripts')
    <script type="text/javascript">
        <?php
            $sharedData = [
                'config' => [
                    'csrfToken' => csrf_token(),
                    'logoutUrl' => route('logout'),
                    'isNative' => true,
                ],
                'loggedInAccount' => auth()->check() ? auth()->user() : null,
            ];
            ?>
            window.SHARED_DATA = {!! json_encode($sharedData) !!}
    </script>
    <script src="https://cdn.jsdelivr.net/npm/tinymce@4.7.11/tinymce.min.js" type="text/javascript"></script>
    <script src="{{ mix('js/app.js') }}"></script>
@endpush

@section('content')
    <!-- begin::Page loader -->
    <div class="m-page-loader m-page-loader--base">
        <div class="m-blockui">
      <span>
        Please wait...
      </span>
            <span>
        <div class="m-loader m-loader--brand"></div>
      </span>
        </div>
    </div>
    <!-- end::Page Loader -->
    <div id="app" class="m-grid m-grid--hor m-grid--root m-page">
        <div class="m-header__bottom">
            <div class="m-container m-container--responsive m-container--xxl m-container--full-height m-page__container">
                <div class="m-stack m-stack--ver m-stack--desktop">
                    <div class="m-stack__item m-stack__item--middle m-stack__item--fluid">
                        <app-header></app-header>
                    </div>
                </div>
            </div>
        </div>
        <div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop  m-body m-page__container">
            <div class="m-grid__item m-grid__item--fluid m-wrapper">
                <!-- BEGIN: Subheader -->
                <div class="m-subheader ">
                    <div class="d-flex align-items-center">
                        <div class="mr-auto">
                            <h3 class="m-subheader__title ">
                                <a href="{{route('AFFILIATE::promotion@index')}}">
                                    <i class="la la-arrow-left"></i>
                                </a>
                                DANH SÁCH SẢN PHẨM ĐỀ CỬ
                            </h3>
                        </div>
                    </div>
                </div>
                <!-- END: Subheader -->
                <div class="m-content">
                    <!--Begin::Section-->
                    <div class="m-portlet m-portlet--mobile">
                        <div class="m-portlet__body">
                            @if (session('success'))
                                <div class="m-alert m-alert--outline m-alert--square m-alert--outline-2x alert alert-success alert-dismissible fade show"
                                     role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    </button>
                                    {{ session('success') }}
                                </div>
                            @endif
                        <!--begin::Table-->
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th style="font-weight: bold">STT</th>
                                        <th style="width:200px;font-weight: bold">Tên sản phẩm</th>
                                        <th style="font-weight: bold">Ảnh sản phẩm</th>
                                        <th style="width: 120px;font-weight: bold">Giá sản phẩm trong khi khuyến mãi
                                        </th>
                                        <th style="width: 120px;font-weight: bold">Giá sản phẩm trước khuyến mãi</th>
                                        <th style="width: 150px;font-weight: bold">Trạng thái</th>
                                        <th style="font-weight: bold">Số lượng đã bán</th>
                                        <th style="font-weight: bold">Số lượng tối đa sẽ bán</th>
                                        <th style="font-weight: bold">Thứ tự</th>
                                        <th style="font-weight: bold">Hành động</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php
                                        if(Request::input('page')==null)
                                        {
                                            $i=1;
                                        }
                                          else{$i=(Request::input('page')-1)*10+1;}
                                    @endphp

                                    @foreach($products as $key)
                                        <tr role="row">
                                            <td>{{$i++}}</td>
                                            <td style="text-align: left">{{$key['name']}}</td>
                                            <td><img width="100px"
                                                     src="{{'https://static.icheck.com.vn/'}}{{$key['thumbnail']}}{{'_thumb_small.jpg'}}"/>
                                            </td>
                                            <td>{{number_format($key['price'],2)}}{{" đ"}}</td>
                                            <td>{{number_format($key['price_before_promotion'],2)}}{{" đ"}}</td>
                                            <td>
                                                @if($key['promotional_approve']==false)
                                                    <span class="m-badge m-badge--danger m-badge--wide">
									Chưa duyệt </span>
                                                @else
                                                    <span class="m-badge m-badge--success m-badge--wide">
									Đã duyệt </span>
                                                @endif
                                            </td>
                                            <td>{{$key['quantity']}}</td>
                                            <td>{{$key['quantity_max']}}</td>
                                            <td><input width="20px" type="number"
                                                       @if(auth()->user()->cannot('AFFILIATE-sortNominateProduct'))
                                                       disabled
                                                       @endif
                                                       id="{{$key['promotional_product_id']}}"
                                                       value="{{$key['promotional_order']}}" min="1"
                                                       placeholder="Nhập thứ tự">
                                            </td>
                                            <td class="btn-group btn-group-sm btn-group-solid">
                                                @if($key['promotional_approve']==false)
                                                    @can("AFFILIATE-activeProduct")
                                                        <button type="button"
                                                                class="btn m-btn m-btn--gradient-from-success m-btn--gradient-to-accent"
                                                                onclick="window.location='{{route('AFFILIATE::product@active',[$key['promotional_product_id']])}}'">
                                                            Duyệt
                                                        </button>
                                                    @endcan
                                                @else
                                                    @can("AFFILIATE-deactiveProduct")
                                                        <button type="button"
                                                                class="btn m-btn m-btn--gradient-from-danger m-btn--gradient-to-accent"
                                                                onclick="window.location='{{route('AFFILIATE::product@deactive',[$key['promotional_product_id']])}}'">
                                                            Bỏ duyệt
                                                        </button>
                                                    @endcan
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!--end::Table-->
                            @include("partials/pagination")
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <router-view></router-view>
@endsection
@push('scripts')
    <script>
        $(document).ready(function () {
            $("input").change(function () {
                var order_value = this.value;
                var product_id = this.id;
                var url = "{{ route('AFFILIATE::promotion@nominate_order',":id")}}";
                real_url = url.replace(':id', product_id);
                $.ajax({
                    url: real_url,
                    method: 'POST',
                    data: {
                        order: order_value,
                    },
                    success: function (msg) {
                        console.log('ok');
                    },
                    error: function (msg) {
                        console.log('fail');
                    }
                });
            });
        });

    </script>
@endpush