@extends('qrcode.layouts.app')
@section('body_class', 'm-page--fluid m-page--loading-enabled m-page--loading m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default')
@push('scripts')
    <script type="text/javascript">
        <?php
            $sharedData = [
                'config' => [
                    'csrfToken' => csrf_token(),
                    'logoutUrl' => route('logout'),
                    'isNative' => true,
                ],
                'loggedInAccount' => auth()->check() ? auth()->user() : null,
            ];
            ?>
            window.SHARED_DATA = {!! json_encode($sharedData) !!}
    </script>
    <script src="https://cdn.jsdelivr.net/npm/tinymce@4.7.11/tinymce.min.js" type="text/javascript"></script>
    <script src="{{ mix('js/app.js') }}"></script>
@endpush

@section('content')
    <!-- begin::Page loader -->
    <div class="m-page-loader m-page-loader--base">
        <div class="m-blockui">
      <span>
        Please wait...
      </span>
            <span>
        <div class="m-loader m-loader--brand"></div>
      </span>
        </div>
    </div>
    <!-- end::Page Loader -->
    <div id="app" class="m-grid m-grid--hor m-grid--root m-page">
        <app-header></app-header>
        <div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop 	m-container m-container--responsive m-container--xxl m-page__container m-body">
            <div class="m-grid__item m-grid__item--fluid m-wrapper">
                <div class="m-content">
                    <div class="m-portlet">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text">
                                        <a href="{{route('AFFILIATE::promotion@index')}}">
                                            <i class="la la-arrow-left"></i>
                                        </a>
                                        {{ isset($promotion) ? 'Sửa nội dung chiến dịch ' : 'Thêm mới chiến dịch' }}
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <!-- /page header -->

                        <form method="POST" enctype="multipart/form-data" name="form"
                              class="m-form m-form--fit m-form--label-align-right"
                              action="{{ isset($promotion) ? route('AFFILIATE::promotion@update',[$promotion['id']] ): route('AFFILIATE::promotion@store') }}">
                            {{ csrf_field() }}
                            @if (isset($promotion))
                                <input type="hidden" name="_method" value="PUT">
                            @endif
                            <div class="m-portlet__body">
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-3 col-sm-12"><strong>Tênchiến dịch *</strong></label>
                                    <div class="col-lg-5 col-md-9 col-sm-12">
                                        <input type="text" id="username" name="name"
                                               class="form-control"
                                               value="{{ old('name') ?: @$promotion['name'] }}"
                                               placeholder="Tên chiến dịch" required/>
                                    </div>
                                    @if ($errors->has('name'))
                                        <div class="form-control-feedback">
                                            <i class="icon-notification2"></i>
                                        </div>
                                        <div class="help-block" style="color: red">{{ $errors->first('name') }}</div>
                                    @endif
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-3 col-sm-12"><strong>Mô tả chung *</strong></label>
                                    <div class="col-lg-5 col-md-9 col-sm-12">
                                         <textarea class="form-control" rows="3" placeholder="Mô tả chung"
                                                   name="description" required>{{old('description')?:@$promotion['description']}}</textarea>
                                    </div>
                                    @if ($errors->has('description'))
                                        <div class="form-control-feedback">
                                            <i class="icon-notification2"></i>
                                        </div>
                                        <div class="help-block" style="color: red">{{ $errors->first('description') }}</div>
                                    @endif
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-3 col-sm-12" for="inputFile"><strong>Banner chiến dịch *</strong></label>
                                    <div class="col-lg-5 col-md-9 col-sm-12">
                                        <input type="file" name="banner" id="inputFile" value="{{ old('banner') ?: "" }}" required>
                                        @if(isset($promotion))
                                            <div>Banner hiện tại</div>
                                            <img width="100px"
                                                 src="{{'https://static.icheck.com.vn/'}}{{$promotion['banner']}}{{'_thumb_small.jpg'}}"/>
                                        @endif
                                    </div>
                                    @if ($errors->has('banner'))
                                        <div class="form-control-feedback">
                                            <i class="icon-notification2"></i>
                                        </div>
                                        <div class="help-block" style="color: red">{{ $errors->first('banner') }}</div>
                                    @endif
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-3 col-sm-12"><strong>Điều kiện tham gia chiến dịch *</strong></label>
                                    <div class="col-lg-5 col-md-9 col-sm-12">
                                        <textarea class="form-control" rows="3" placeholder="Điều kiện tham gia"
                                                  name="condition" required>{{old('condition')?:@$promotion['condition']}}</textarea>
                                    </div>
                                    @if ($errors->has('condition'))
                                        <div class="form-control-feedback">
                                            <i class="icon-notification2"></i>
                                        </div>
                                        <div class="help-block" style="color: red">{{ $errors->first('condition') }}</div>
                                    @endif
                                </div>
                                <!------------------ Time start--------------->
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-3 col-sm-12"><strong>Thời gian bắt đầu *</strong></label>
                                    <div class="col-lg-3 col-md-9 col-sm-12">
                                        <input type="text" name="time_start" class="form-control" id="m_datepicker_1" placeholder="Thời gian bắt đầu"
                                               value="{{!isset($promotion)?old('time_start'):@date("m/d/Y", strtotime($promotion['start_time'])) }}" required/>
                                    </div>
                                    @if ($errors->has('time_start'))
                                        <div class="form-control-feedback">
                                            <i class="icon-notification2"></i>
                                        </div>
                                        <div class="help-block" style="color: red">{{ $errors->first('time_start') }}</div>
                                    @endif
                                </div>
                                <!------------------ Time end--------------->
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-3 col-sm-12"><strong>Thời gian kết thúc *</strong></label>
                                    <div class="col-lg-3 col-md-9 col-sm-12">
                                        <input type="text" name="time_end" class="form-control" id="m_datepicker_1" placeholder="Thời gian kết thúc"
                                               value="{{!isset($promotion)?old('time_end'):@date("m/d/Y", strtotime($promotion['end_time'])) }}" required/>
                                    </div>
                                    @if ($errors->has('time_end'))
                                        <div class="form-control-feedback">
                                            <i class="icon-notification2"></i>
                                        </div>
                                        <div class="help-block" style="color: red">{{ $errors->first('time_end') }}</div>
                                    @endif
                                </div>
                                <!-------------------------Nominate------------------->
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-3 col-sm-12"><strong>Chọn nhóm sản phẩm tham gia *</strong></label>
                                    <div class="col-lg-5 col-md-9 col-sm-12">
                                        <select id="nominate_product" class="form-control" name="categories[]" multiple data-actions-box="true" required>
                                            @foreach($categories as $category)
                                                <option value="{{$category['id']}}"
                                                @if(old('categories')!=null)
                                                    @if(in_array($category['id'],old('categories') )== true){{ 'selected' }}@endif
                                                        @endif
                                                        {{ (isset($promotion) and in_array($category['id'],$promotion['categories'] )== true) ? ' selected="selected"' : ''  }}>{{$category['name']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @if ($errors->has('categories'))
                                        <div class="form-control-feedback">
                                            <i class="icon-notification2"></i>
                                        </div>
                                        <div class="help-block"
                                             style="color: red">{{ $errors->first('categories') }}</div>
                                    @endif
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-3 col-sm-12"><strong>Chiết khấu nhỏ nhất (%) *</strong></label>
                                    <div class="col-lg-3 col-md-9 col-sm-12">
                                        <input type="number" name="discount_min"
                                               class="form-control"
                                               value="{{ old('discount_min') ?: @$promotion['discount_min'] }}" min="0" max="100"
                                               placeholder="Chiết khấu nhỏ nhất" required/>
                                    </div>
                                    @if ($errors->has('discount_min'))
                                        <div class="form-control-feedback">
                                            <i class="icon-notification2"></i>
                                        </div>
                                        <div class="help-block" style="color: red">{{ $errors->first('discount_min') }}</div>
                                    @endif
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-3 col-sm-12"><strong>Chiết khấu cao nhất (%) *</strong></label>
                                    <div class="col-lg-3 col-md-9 col-sm-12">
                                        <input type="text" name="discount_max"
                                               class="form-control"
                                               value="{{ old('discount_max') ?: @$promotion['discount_max'] }}" min="0" max="100"
                                               placeholder="Chiết khấu lớn nhất" required/>
                                    </div>
                                    @if ($errors->has('discount_max'))
                                        <div class="form-control-feedback">
                                            <i class="icon-notification2"></i>
                                        </div>
                                        <div class="help-block" style="color: red">{{ $errors->first('discount_max') }}</div>
                                    @endif
                                </div>

                                <div class="m-portlet__foot m-portlet__foot--fit">
                                    <div class="m-form__actions m-form__actions">
                                        <div class="row">
                                            <div class="col-lg-9 ml-lg-auto">
                                                <button type="submit" class="btn btn-success">
                                                    {{ isset($promotion) ? 'Cập nhật' : 'Thêm mới' }}
                                                </button>
                                                <button type="reset" class="btn btn-secondary">
                                                    Nhập lại
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <router-view></router-view>
@endsection

@push('scripts')
    <script type="text/javascript" src="{{ asset('qr_code/metronic/js/vendors.bundle.js') }}"></script>
    <script type="text/javascript" src="{{ asset('qr_code/metronic/js/scripts.bundle.js') }}"></script>
    <script type="text/javascript" src="{{ asset('qr_code/metronic/js/datepicker/bootstrap-datepicker.js') }}"></script>
    {{--<script type="text/javascript" src="{{ asset('qr_code/metronic/js/bootstrap-select.js') }}"></script>--}}
    <script>
        $(document).ready(function () {
            $("#nominate_product").select2({
                placeholder: "--Chọn--"
            });


        });

    </script>
@endpush