@extends('qrcode.layouts.app')
@push("styles")
    <style>
        label {
            font-weight: bold;
        }

        th, td {
            text-align: center;
        }
    </style>
@endpush
@section('body_class', 'm-page--fluid m-page--loading-enabled m-page--loading m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default')

@push('scripts')
    <script type="text/javascript">
        <?php
            $sharedData = [
                'config' => [
                    'csrfToken' => csrf_token(),
                    'logoutUrl' => route('logout'),
                    'isNative' => true,
                ],
                'loggedInAccount' => auth()->check() ? auth()->user() : null,
            ];
            ?>
            window.SHARED_DATA = {!! json_encode($sharedData) !!}
    </script>
    <script src="https://cdn.jsdelivr.net/npm/tinymce@4.7.11/tinymce.min.js" type="text/javascript"></script>
    <script src="{{ mix('js/app.js') }}"></script>
@endpush

@section('content')
    <!-- begin::Page loader -->
    <div class="m-page-loader m-page-loader--base">
        <div class="m-blockui">
      <span>
        Please wait...
      </span>
            <span>
        <div class="m-loader m-loader--brand"></div>
      </span>
        </div>
    </div>
    <!-- end::Page Loader -->
    <div id="app" class="m-grid m-grid--hor m-grid--root m-page">
        <div class="m-header__bottom">
            <div class="m-container m-container--responsive m-container--xxl m-container--full-height m-page__container">
                <div class="m-stack m-stack--ver m-stack--desktop">
                    <div class="m-stack__item m-stack__item--middle m-stack__item--fluid">
                        <app-header></app-header>
                    </div>
                </div>
            </div>
        </div>
        <div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop  m-body m-page__container">
            <div class="m-grid__item m-grid__item--fluid m-wrapper">
                <!-- BEGIN: Subheader -->
                <div class="m-subheader ">
                    <div class="d-flex align-items-center">
                        <div class="mr-auto">
                            <h3 class="m-subheader__title ">
                                QUẢN LÝ CHIẾN DỊCH AFFILIATE
                            </h3>
                        </div>
                        @can("AFFILIATE-addPromotion")
                            <div><a href="{{route('AFFILIATE::promotion@add')}}"
                                    class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air"><span>
                                    <i class="la la-diamond"></i>
                                    <span>Thêm mới chiến dịch</span></span></a>
                            </div>
                        @endcan
                    </div>
                </div>
                <!-- END: Subheader -->
                @can("AFFILIATE-viewPromotion")
                    <div class="m-content">
                        <!--Begin::Section-->
                        <div class="m-portlet m-portlet--mobile">
                            <div class="m-portlet__body">
                                @if (session('success'))
                                    <div class="m-alert m-alert--outline m-alert--square m-alert--outline-2x alert alert-success alert-dismissible fade show"
                                         role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        </button>
                                        {{ session('success') }}
                                    </div>
                                @endif
                            <!--begin::Table-->
                                <table class="table table-striped- table-bordered table-hover table-checkable">
                                    <thead>
                                    <tr>
                                        <th style="font-weight: bold;">STT</th>
                                        <th style="font-weight: bold;">Tên chương trình</th>
                                        <th style="font-weight: bold;">Thời gian bắt đầu</th>
                                        <th style="font-weight: bold;">Thời gian kết thúc</th>
                                        <th style="font-weight: bold;">Trạng thái chương trình</th>
                                        <th style="font-weight: bold;">Hành động</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php
                                        if(Request::input('page')==null)
                                        {
                                            $i=1;
                                        }
                                          else{$i=(Request::input('page')-1)*10+1;}
                                    @endphp

                                    @foreach($promotions as $key)
                                        <tr role="row">
                                            <td>{{$i++}}</td>
                                            <td style="text-align: left">{{$key['name']}}</td>
                                            <td>{{date("d/m/Y", strtotime($key['start_time']))}}</td>
                                            <td>{{date("d/m/Y", strtotime($key['end_time']))}}</td>
                                            <td>
                                                @if($key['actived']==false)
                                                    <span class="m-badge m-badge--danger m-badge--wide">
									Ngừng chạy </span>
                                                @else
                                                    <span class="m-badge m-badge--success m-badge--wide">
									Chạy </span>
                                                @endif
                                            </td>
                                            <td>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-brand dropdown-toggle"
                                                            data-toggle="dropdown" aria-haspopup="true"
                                                            aria-expanded="false">
                                                    </button>
                                                    <div class="dropdown-menu dropdown-menu-right">
                                                        @if(strtotime($key['start_time'])>strtotime(date("m/d/Y h:i:s a")))
                                                            @can("AFFILIATE-updatePromotion")
                                                                <button class="btn dropdown-item" type="button"
                                                                        onclick="window.location='{{route('AFFILIATE::promotion@edit',[$key['id']])}}'">
                                                                    <i class="m-nav__link-icon flaticon-edit-1"></i>Cập
                                                                    nhật
                                                                </button>
                                                            @endcan
                                                            @can("AFFILIATE-approvePromotion")
                                                                <button class="btn dropdown-item" type="button"
                                                                        onclick="window.location='{{route('AFFILIATE::promotion@approve',[$key['id']])}}'">
                                                                    <i class="m-nav__link-icon la la-check"></i>Duyệt
                                                                </button>
                                                            @endcan
                                                            @can("AFFILIATE-delPromotion")
                                                                <button class="btn dropdown-item" type="button"
                                                                        data-toggle="modal"
                                                                        data-target="#delete{{$key['id']}}">
                                                                    <i class="m-nav__link-icon flaticon-delete-1"></i>Xóa
                                                                    bỏ
                                                                </button>
                                                            @endcan
                                                        @endif
                                                        @can("AFFILIATE-viewDetailPromotion")
                                                            <button class="btn dropdown-item" type="button"
                                                                    onclick="window.location='{{route('AFFILIATE::promotion@detail', [$key['id']])}}'">
                                                                <i class="m-nav__link-icon flaticon-information"></i>Chi
                                                                tiết
                                                                chiến dịch
                                                            </button>
                                                        @endcan
                                                        @can("AFFILIATE-viewNominateProductOfPromotion")
                                                            <button class="btn dropdown-item" type="button"
                                                                    onclick="window.location='{{route('AFFILIATE::promotion@nominate', [$key['id']])}}'">
                                                                <i class="m-nav__link-icon flaticon-cart"></i> Sản phẩm
                                                                đề
                                                                cử
                                                            </button>
                                                        @endcan
                                                        @if($key['actived']==false)
                                                            @can("AFFILIATE-activePromotion")
                                                                <button class="btn dropdown-item" type="button"
                                                                        onclick="window.location='{{route('AFFILIATE::promotion@active',[$key['id']])}}'">
                                                                    <i class="m-nav__link-icon la la-fast-forward"></i>Chạy
                                                                </button>
                                                            @endcan
                                                        @else
                                                            @can("AFFILIATE-deactivePromotion")
                                                                <button class="btn dropdown-item" type="button"
                                                                        onclick="window.location='{{route('AFFILIATE::promotion@deactive',[$key['id']])}}'">
                                                                    <i class="m-nav__link-icon flaticon-circle"></i>Ngừng
                                                                    chạy
                                                                </button>
                                                            @endcan
                                                        @endif
                                                    </div>
                                                </div>
                                                <!--Modal Comfirm delete-->
                                                <div id="delete{{$key['id']}}" class="modal fade" role="dialog">
                                                    <div class="modal-dialog">
                                                        <!-- Modal content-->
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h3 class="modal-title">Bạn chắc chắn muốn xóa bỏ chiến
                                                                    dịch {{$key['name']}}?</h3>
                                                                <button type="button" class="close"
                                                                        data-dismiss="modal">
                                                                    &times;
                                                                </button>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <form method="get"
                                                                      action="{{ route('AFFILIATE::promotion@delete',[$key['id']]) }}">
                                                                    {{ csrf_field() }}
                                                                    <button type="submit" class="btn btn-success">Đồng ý
                                                                    </button>
                                                                    <button type="button" class="btn btn-default"
                                                                            data-dismiss="modal">Hủy yêu cầu
                                                                    </button>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--End Modal Comfirm delete-->
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <!--end::Table-->
                                @include("partials/pagination")
                            </div>
                        </div>
                    </div>
                @endcan
            </div>
        </div>
    </div>
    <router-view></router-view>
@endsection
