@extends('qrcode.layouts.app')
@push('styles')
    <style>
        label {
            font-weight: bold;
        }

    </style>
@endpush
@section('body_class', 'm-page--fluid m-page--loading-enabled m-page--loading m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default')
@push('scripts')
    <script type="text/javascript">
        <?php
            $sharedData = [
                'config' => [
                    'csrfToken' => csrf_token(),
                    'logoutUrl' => route('logout'),
                    'isNative' => true,
                ],
                'loggedInAccount' => auth()->check() ? auth()->user() : null,
            ];
            ?>
            window.SHARED_DATA = {!! json_encode($sharedData) !!}
    </script>
    <script src="https://cdn.jsdelivr.net/npm/tinymce@4.7.11/tinymce.min.js" type="text/javascript"></script>
    <script src="{{ mix('js/app.js') }}"></script>
@endpush

@section('content')
    <!-- begin::Page loader -->
    <div class="m-page-loader m-page-loader--base">
        <div class="m-blockui">
      <span>
        Please wait...
      </span>
            <span>
        <div class="m-loader m-loader--brand"></div>
      </span>
        </div>
    </div>
    <!-- end::Page Loader -->
    <div id="app" class="m-grid m-grid--hor m-grid--root m-page">
        <app-header></app-header>
        <div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop 	m-container m-container--responsive m-container--xxl m-page__container m-body">
            <div class="m-grid__item m-grid__item--fluid m-wrapper">
                <div class="m-content">
                    <div class="m-portlet">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text">
                                        <a href="{{route('AFFILIATE::promotion@index')}}">
                                            <i class="la la-arrow-left"></i>
                                        </a>
                                        Chi tiết chiến dịch: "{{$promotion['name']}}"
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <!-- /page header -->

                        <div class="m-portlet__body">
                            <div class="form-group m-form__group row">
                                <label>Tên
                                    chiến dịch: </label>
                                <div class="col-lg-5 col-md-9 col-sm-12">
                                    {{$promotion['name'] }}
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label>Mô tả chung: </label>
                                <div class="col-lg-5 col-md-9 col-sm-12">
                                    {{$promotion['description']}}
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label>Banner chiến dịch: </label>
                                <div class="col-lg-5 col-md-9 col-sm-12">
                                    <img width="150px"
                                         src="{{'https://static.icheck.com.vn/'}}{{$promotion['banner']}}{{'_thumb_small.jpg'}}"/>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label>Điều kiện tham gia chiến dịch: </label>
                                <div class="col-lg-5 col-md-9 col-sm-12">
                                    {{$promotion['condition']}}
                                </div>
                            </div>
                            <!------------------ Time start--------------->
                            <div class="form-group m-form__group row">
                                <label>Thời gian bắt đầu: </label>
                                <div class="col-lg-3 col-md-9 col-sm-12">
                                    {{date("d/m/Y", strtotime($promotion['start_time']))}}
                                </div>
                            </div>
                            <!------------------ Time end--------------->
                            <div class="form-group m-form__group row">
                                <label>Thời gian kết thúc: </label>
                                <div class="col-lg-3 col-md-9 col-sm-12">
                                    {{date("d/m/Y", strtotime($promotion['end_time']))}}
                                </div>
                            </div>
                            <!-------------------------Nominate------------------->
                            <div class="form-group m-form__group row">
                                <label>Nhóm sản phẩm tham gia:</label>
                                <div class="col-lg-5 col-md-9 col-sm-12">
                                    @foreach($promotion['categories'] as $category)
                                        @foreach($categories as $cate)
                                            @if($category==$cate['id'])
                                                {{$cate['name']}};
                                            @endif
                                        @endforeach
                                    @endforeach
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label>Chiết khấu nhỏ nhất: </label>
                                <div class="col-lg-3 col-md-9 col-sm-12">
                                    {{$promotion['discount_min'] }}%
                                </div>

                            </div>
                            <div class="form-group m-form__group row">
                                <label>Chiết khấu cao nhất: </label>
                                <div class="col-lg-3 col-md-9 col-sm-12">
                                    {{$promotion['discount_max'] }}%
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label>Trạng thái chiến dịch:</label>
                                <div class="col-lg-3 col-md-9 col-sm-12">
                                    @if($promotion['actived']==true)
                                        <div>{{'Đã duyệt'}}</div>
                                    @else
                                        <div>{{'Chưa duyệt'}}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="m-portlet__foot m-portlet__foot--fit">
                                <div class="m-form__actions m-form__actions">
                                    <div class="row">
                                        <div class="col-lg-9 ml-lg-auto">
                                            @if(strtotime($promotion['start_time'])>strtotime(date("m/d/Y h:i:s a")))
                                                <button type="button" class="btn btn-success"
                                                        onclick="window.location='{{route('AFFILIATE::promotion@edit', [$promotion['id']])}}'">
                                                    Cập nhật
                                                </button>
                                            @endif
                                            <button type="button" class="btn btn-secondary"
                                                    onclick="window.location='{{route('AFFILIATE::promotion@index')}}'">
                                                Trở về
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <router-view></router-view>
@endsection

@push('scripts')
    <script type="text/javascript" src="{{ asset('qr_code/metronic/js/vendors.bundle.js') }}"></script>
    <script type="text/javascript" src="{{ asset('qr_code/metronic/js/scripts.bundle.js') }}"></script>
@endpush