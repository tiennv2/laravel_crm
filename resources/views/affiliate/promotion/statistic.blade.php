@extends('qrcode.layouts.app')
@push("styles")
    <style>
        label {
            font-weight: bold;
        }

        th, td {
            text-align: center;
        }
    </style>
@endpush
@section('body_class', 'm-page--fluid m-page--loading-enabled m-page--loading m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default')

@push('scripts')
    <script type="text/javascript">
        <?php
            $sharedData = [
                'config' => [
                    'csrfToken' => csrf_token(),
                    'logoutUrl' => route('logout'),
                    'isNative' => true,
                ],
                'loggedInAccount' => auth()->check() ? auth()->user() : null,
            ];
            ?>
            window.SHARED_DATA = {!! json_encode($sharedData) !!}
    </script>
    <script src="https://cdn.jsdelivr.net/npm/tinymce@4.7.11/tinymce.min.js" type="text/javascript"></script>
    <script src="{{ mix('js/app.js') }}"></script>
@endpush

@section('content')
    <!-- begin::Page loader -->
    <div class="m-page-loader m-page-loader--base">
        <div class="m-blockui">
      <span>
        Please wait...
      </span>
            <span>
        <div class="m-loader m-loader--brand"></div>
      </span>
        </div>
    </div>
    <!-- end::Page Loader -->
    <div id="app" class="m-grid m-grid--hor m-grid--root m-page">
        <div class="m-header__bottom">
            <div class="m-container m-container--responsive m-container--xxl m-container--full-height m-page__container">
                <div class="m-stack m-stack--ver m-stack--desktop">
                    <div class="m-stack__item m-stack__item--middle m-stack__item--fluid">
                        <app-header></app-header>
                    </div>
                </div>
            </div>
        </div>
        <div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop  m-body m-page__container">
            <div class="m-grid__item m-grid__item--fluid m-wrapper">
                <!-- BEGIN: Subheader -->
                <div class="m-subheader ">
                    <div class="d-flex align-items-center">
                        <div class="mr-auto">
                            <h3 class="m-subheader__title ">
                                SỐ LIỆU THỐNG KÊ
                            </h3>
                        </div>
                    </div>
                </div>
                <!-- END: Subheader -->
                <div class="m-content">

                    <!--Begin::Section-->
                    <div class="m-portlet m-portlet--mobile">
                        <div class="m-portlet__body">
                            <!-- Search Field -->
                            <form class="m-form m-form--fit m--margin-bottom-20"
                                  action="{{route('AFFILIATE::promotion@statistic_filter')}}" method="get">
                                <div class="row m--margin-bottom-20">
                                    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                                        <label style="font-weight: bold">Lọc theo ngày</label>
                                        <div class="input-daterange input-group">
                                            <input type="text" class="form-control" name="from" id="m_datepicker_1"
                                                   value="{{ Request::input('from') }}" autocomplete="off">
                                            <span class="input-group-text">
														<i class="la la-ellipsis-h"></i>
													</span>
                                            <input type="text" class="form-control" name="to" id="m_datepicker_1"
                                                   value="{{ Request::input('to') }}" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="col-lg-9 m--margin-bottom-10-tablet-and-mobile">
                                        <label style="font-weight: bold"></label>
                                        <div style="text-align: center">
                                            @if($increase===0)
                                                <h5>Ngày {{date('d/m/Y',strtotime("-1 days"))}} có tổng đơn hàng
                                                    bằng ngày trước đó.</h5>
                                            @elseif($increase > 0)
                                                <h5>Tổng số đơn hàng ngày {{date('d/m/Y',strtotime("-1 days"))}}
                                                    <strong>tăng</strong>
                                                    so với ngày trước đó là {{$increase}} đơn hàng.</h5>
                                            @else
                                                <h5>Tổng số đơn hàng ngày {{date('d/m/Y',strtotime("-1 days"))}}
                                                    <strong>giảm</strong>
                                                    so với ngày trước đó là {{$increase}} đơn hàng.</h5>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="m-separator m-separator--md m-separator--dashed"></div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <button class="btn btn-brand m-btn m-btn--icon" type="submit">
												<span>
													<i class="la la-search"></i>
													<span>
														Tìm kiếm
													</span>
												</span>
                                        </button>
                                        &nbsp;&nbsp;
                                        <button class="btn btn-secondary m-btn m-btn--icon" type="button"
                                                onclick="window.location='{{route('AFFILIATE::promotion@statistics')}}'">
												<span>
													<i class="la la-refresh"></i>
													<span>
														Làm mới
													</span>
												</span>
                                        </button>
                                    </div>
                                </div>
                            </form>
                            <!--begin::Table-->
                            <div class="table-responsive">
                                <table class="table table-striped- table-bordered table-hover table-checkable">
                                    <thead>
                                    <tr>
                                        <th style="font-weight: bold;">Tổng số đơn hàng</th>
                                        <th style="font-weight: bold;">Tổng số tiền từ những đơn hàng</th>
                                        <th style="font-weight: bold;">Tổng số tiền chiết khấu</th>
                                        <th style="font-weight: bold;">Số lượng đơn hàng thành công</th>
                                        <th style="font-weight: bold;">Tổng số tiền từ những đơn hàng thành công</th>
                                        <th style="font-weight: bold;">Tổng chiết khấu nhận được từ đơn hàng thành
                                            công
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr role="row">
                                        <td>{{number_format($statistics['order_count'],0,".",",")}}</td>
                                        <td>{{number_format($statistics['order_amount'],0,".",",").' đ'}}</td>
                                        <td>{{number_format($statistics['icheck_order_amount'],0,".",",").' đ'}}</td>
                                        <td>{{number_format($statistics['order_success_count'],0,".",",")}}</td>
                                        <td>{{number_format($statistics['order_success_amount'],0,".",",").' đ'}}</td>
                                        <td>{{number_format($statistics['icheck_order_success_amount'],0,".",",").' đ'}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!--end::Table-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <router-view></router-view>
@endsection
@push('scripts')
    <script type="text/javascript" src="{{ asset('qr_code/metronic/js/vendors.bundle.js') }}"></script>
    <script type="text/javascript" src="{{ asset('qr_code/metronic/js/datepicker/bootstrap-datepicker.js') }}"></script>
@endpush