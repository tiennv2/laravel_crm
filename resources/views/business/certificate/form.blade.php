@extends('qrcode.layouts.app')
@push('styles')
    <style>
        .help-block {
            color: red;
        }

        .heroes {
            margin: 0 0 2em 0;
            padding: 0;
            width: 30em;
        }

        .heroes li {
            position: relative;
            left: 0;
            color: #00c5dc;
            margin: .5em;
            padding: .3em .3em;
            height: auto;
            min-height: 3em;
            border-radius: 4px;
            border-color: #00c5dc;
            border-width: 2px;
            border-style: solid;
        }
    </style>
@endpush
@section('body_class', 'm-page--fluid m-page--loading-enabled m-page--loading m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default')
@push('scripts')
    <script type="text/javascript">
        <?php
            $sharedData = [
                'config' => [
                    'csrfToken' => csrf_token(),
                    'logoutUrl' => route('logout'),
                    'isNative' => true,
                ],
                'loggedInAccount' => auth()->check() ? auth()->user() : null,
            ];
            ?>
            window.SHARED_DATA = {!! json_encode($sharedData) !!}
    </script>
    <script src="https://cdn.jsdelivr.net/npm/tinymce@4.7.11/tinymce.min.js" type="text/javascript"></script>
    <script src="{{ mix('js/app.js') }}"></script>
@endpush

@section('content')
    <!-- begin::Page loader -->
    <div class="m-page-loader m-page-loader--base">
        <div class="m-blockui">
      <span>
        Please wait...
      </span>
            <span>
        <div class="m-loader m-loader--brand"></div>
      </span>
        </div>
    </div>
    <!-- end::Page Loader -->
    <div id="app" class="m-grid m-grid--hor m-grid--root m-page">
        <app-header></app-header>
        <div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop 	m-container m-container--responsive m-container--xxl m-page__container m-body">
            <div class="m-grid__item m-grid__item--fluid m-wrapper">
                <div class="m-content">
                    <div class="m-portlet">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text">
                                        <a href="{{route('Business::certificate@index')}}">
                                            <i class="la la-arrow-left"></i>
                                        </a>
                                        {{ isset($certificate) ? 'Sửa thông tin giấy tờ ' : 'Thêm mới giấy tờ' }}
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <!-- /page header -->
                        @if (session('success'))
                            <div class="m-alert m-alert--outline m-alert--square m-alert--outline-2x alert alert-success alert-dismissible fade show"
                                 role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                </button>
                                {{ session('success') }}
                            </div>
                        @endif
                        <form method="POST" enctype="multipart/form-data" name="form"
                              {{--onsubmit="return validateForm()"--}}
                              class="m-form m-form--fit m-form--label-align-right"
                              action="{{isset($certificate) ? route('Business::certificate@update', [$certificate->id] ): route('Business::certificate@store') }}">
                            {{ csrf_field() }}
                            @if (isset($certificate))
                                <input type="hidden" name="_method" value="PUT">
                            @endif
                            <div class="m-portlet__body">
                                <!------------------ Business--------------->
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">Chọn doanh nghiệp *</label>
                                    <div class="col-lg-6 col-md-9 col-sm-12">
                                        <select class="form-control m-input" data-col-index="7" id="business_id"
                                                name="business_id"
                                                {{(isset($certificate))?'disabled="true"' :''}}
                                                required>
                                            <option value=""></option>
                                            @foreach($businesses as $business)
                                                <option value="{{$business->id}}"
                                                        @if( old('business_id') == $business->id) selected="selected" @endif
                                                        {{(isset($certificate) and ($business->id==$certificate->business_id))?'selected="selected"' :''}}
                                                >{{$business->name}}</option>
                                            @endforeach
                                        </select>
                                        @if(isset($certificate))
                                            <input type="hidden" name="business_id"
                                                   value="{{$certificate->business_id}}">
                                            <input type="hidden" name="certificate_id"
                                                   value="{{$certificate->id}}">
                                        @else
                                            <input type="hidden" name="certificate_id"
                                                   value="">
                                        @endif
                                    </div>
                                    @if ($errors->has('business_id'))
                                        <div class="help-block">{{ $errors->first('business_id') }}</div>
                                    @endif
                                </div>
                                <!------------------ Name--------------->
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">Tên giấy tờ *</label>
                                    <div class="col-lg-6 col-md-9 col-sm-12">
                                        <input type="text" id="name" name="name"
                                               class="form-control"
                                               value="{{ old('name') ?: @$certificate->name }}"
                                               required/>
                                        {{--<p style="margin-top: 5px;margin-bottom: 0;font-family: Arial, sans-serif;color: #7e8c9e;font-size: 12px">--}}
                                            {{--Tên giấy tờ chỉ bao gồm các chữ cái từ a-z,--}}
                                            {{--các số từ 0-9 và ký tự '-' (không bắt đầu hay kết thúc bằng ký tự '-').--}}
                                            {{--Tên giấy tờ phải là duy nhất trên hệ thống.--}}
                                        {{--</p>--}}
                                    </div>
                                    @if ($errors->has('name'))
                                        <div class="help-block">{{ $errors->first('name') }}</div>
                                    @endif
                                </div>
                                <!------------------Description--------------->
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">Mô tả</label>
                                    <div class="col-lg-6 col-md-9 col-sm-12">
                                       <textarea class="form-control" rows="3" placeholder="Mô tả "
                                                 name="description">{{old('description')?:@$certificate['description']}}</textarea>
                                    </div>
                                    @if ($errors->has('description'))
                                        <div class="help-block">{{ $errors->first('description') }}</div>
                                    @endif
                                </div>
                                <!------------------Upload files--------------->
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">Upload files</label>
                                    <div class="col-lg-6 col-md-9 col-sm-12">
                                        <label for="file"
                                               style="cursor: pointer;padding:8px;background-color: #0aa7ef;color: white;border-radius: 20px"><i
                                                    class="la la-cloud-upload"></i> Chọn
                                            tập tin</label>
                                    </div>
                                    <input type="file" name="files[]"
                                           id="file"
                                           class="upload"
                                           style="display: none!important;"
                                           accept=".png,.jpg,.jpeg,.gif,.pdf,.doc,.docx,.xls, .xlsx"
                                           multiple>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label style="color: white" class="col-form-label col-lg-3 col-sm-12">Upload
                                        files</label>
                                    <div class="col-lg-6 col-md-9 col-sm-12">
                                        <ol class="uploadFiles_list heroes">
                                            @if(isset($certificate->files))
                                                @foreach($certificate->files as $file)
                                                    <li>
                                                        <div style="text-align: left;display: inline-block; width:70%">{{$file[0]}}</div>
                                                        <input type="hidden" name="uploaded_files[]"
                                                               value="{{$file[1]}}">
                                                        <div style="text-align: right;display: inline-block;float: right;">
                                                            <a href="{{$file[2]}}" target="_blank" data-toggle="tooltip"
                                                               title="Xem"
                                                               class="btn btn-outline-accent m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air">
                                                                <i class="la la-eye"></i></a>
                                                            <a href="javascript:void(0);"
                                                               onclick="return removeItem($(this));"
                                                               data-toggle="tooltip" title="Xóa"
                                                               class="btn btn-outline-accent m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air">
                                                                <i class="la la-remove"></i></a>
                                                        </div>
                                                    </li>
                                                @endforeach
                                            @endif
                                        </ol>
                                    </div>
                                </div>
                                <div class="m-portlet__foot m-portlet__foot--fit">
                                    <div class="m-form__actions m-form__actions">
                                        <div class="row">
                                            <div class="col-lg-12 ml-lg-auto" style="text-align: center">
                                                <button type="submit" id="btn-submit"
                                                        class="btn m-btn btn-success">
                                                    {{ isset($certificate) ? 'Cập nhật' : 'Thêm mới' }}
                                                </button>
                                                <button type="reset" class="btn m-btn btn-secondary">
                                                    {{ isset($certificate) ? 'Hủy' : 'Nhập lại' }}
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript" src="{{ asset('qr_code/metronic/js/vendors.bundle.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            // Basic
            $("#business_id").select2({
                placeholder: 'Chọn doanh nghiệp',
                allowClear: true
            });

            $("select").on("select2:select", function (evt) {
                var element = evt.params.data.element;
                var $element = $(element);
                $element.detach();
                $(this).append($element);
                $(this).trigger("change");
            });
        });

        //        $("#btn-submit").click(function () {
        //            $(this).addClass("m-loader m-loader--light m-loader--right");
        //        });

        function validateForm() {
            let certificate_name = document.forms["form"]["name"].value;
            let certificate_id = document.forms["form"]["certificate_id"].value;
            let url = "{{ route('Business::certificate@checkUniqueName')}}";
            let token = $("input[name='_token']").val();
            let pattern = /^[-a-z0-9]+$/;
            if (pattern.test(certificate_name) && !certificate_name.startsWith("-") && !certificate_name.endsWith("-")) {
                let checkUnique = false;
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {
                        certificate_name: certificate_name,
                        certificate_id: certificate_id,
                        _token: token
                    },
                    async: false,
                    success: function (data) {
                        console.log(data);
                        let message = data.message;
                        if (message === 'valid') {
                            checkUnique = true;
                        }
                    }
                });
                if (checkUnique) {
                    $("#btn-submit").addClass("m-loader m-loader--light m-loader--right");
                    return true;
                } else {
                    alert('Tên giấy tờ đã tồn tại, hãy chọn một tên khác!');
                    return false;
                }
            }
            else {
                alert('Sai định dạng tên giấy tờ!');
                return false;
            }
        }

        //Upload files
        $(".upload").change(function () {
            let spinner = `<div class="spinner m-loader m-loader--primary" style="width: 30px; display: inline-block;">
                            </div>`;
            let x = document.getElementById("file");
            $(".uploadFiles_list").append(`${spinner}`);
            var files = x.files;
            var formData = new FormData();
            // Loop through each of the selected files.
            for (let i = 0; i < files.length; i++) {
                let file = files[i];

                // Add the file to the request.
                formData.append('files[]', file, file.name);

            }

            var url = "{{ route('Business::upload')}}";
//            var token = $("input[name='_token']").val();
            $.ajax({
                url: url,
                method: 'POST',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    $("div").remove(".spinner");
                    $.each(data, function (key, value) {
                        let file = JSON.parse(value);
                        appendData(file);
                    });
                }
            });
        });
        var removeItem = function (e) {
            e.closest('li').remove();
        };

        function appendData(file) {
            for (let i = 0; i < file.length; i++) {
                let file_name = file[i][0].split("_").pop();
                let html = `<li>
                                <div style="text-align: left;display: inline-block;width:70%">${file_name}</div>
                                <input type="hidden" name="uploaded_files[]" value="${file[i][0]}">
                                <div style="text-align: right;display: inline-block;float: right;">
                                    <a href="${file[i][1]}" target="_blank" data-toggle="tooltip" title="Xem" class="btn btn-outline-accent m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air"> <i class="la la-eye"></i></a>
                                    <a href="javascript:void(0);" onclick="return removeItem($(this));" data-toggle="tooltip" title="Xóa" class="btn btn-outline-accent m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air"> <i class="la la-remove"></i></a>
                                </div>
                           </li>`;
                $(".uploadFiles_list").append(`${html}`);
            }
        }
    </script>

@endpush
