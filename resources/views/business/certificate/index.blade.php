@extends('qrcode.layouts.app')
@push('styles')
    <style>
        .heroes {
            margin: 0 0 2em 0;
            padding: 0;
            width: 30em;
        }

        .heroes li {
            position: relative;
            left: 0;
            color: #00c5dc;
            margin: .5em;
            padding: .3em .3em;
            height: auto;
            min-height: 3em;
            border-radius: 4px;
            border-color: #00c5dc;
            border-width: 2px;
            border-style: solid;
        }
    </style>
@endpush
@section('body_class', 'm-page--fluid m-page--loading-enabled m-page--loading m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default')

@push('scripts')
    <script type="text/javascript">
        <?php
            $sharedData = [
                'config' => [
                    'csrfToken' => csrf_token(),
                    'logoutUrl' => route('logout'),
                    'isNative' => true,
                ],
                'loggedInAccount' => auth()->check() ? auth()->user() : null,
            ];
            ?>
            window.SHARED_DATA = {!! json_encode($sharedData) !!}
    </script>
    <script src="https://cdn.jsdelivr.net/npm/tinymce@4.7.11/tinymce.min.js" type="text/javascript"></script>
    <script src="{{ mix('js/app.js') }}"></script>
@endpush

@section('content')
    <!-- begin::Page loader -->
    <div class="m-page-loader m-page-loader--base">
        <div class="m-blockui">
      <span>
        Please wait...
      </span>
            <span>
        <div class="m-loader m-loader--brand"></div>
      </span>
        </div>
    </div>
    <!-- end::page Loader -->
    <div id="app" class="m-grid m-grid--hor m-grid--root m-page">
        <app-header></app-header>
        <div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop  m-body m-page__container">
            <div class="m-grid__item m-grid__item--fluid m-wrapper">
                <!-- BEGIN: Subheader -->
                <div class="m-subheader">
                    <div class="d-flex align-items-center">
                        <div class="mr-auto">
                            <h3 class="m-subheader__title">
                                QUẢN LÝ GIẤY TỜ
                            </h3>
                        </div>
                        @can("GATEWAY-addCertificate")
                            <div style="float: right"><a href="{{route('Business::certificate@add')}}"
                                                         class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air"><span>
                                    <i class="la la-plus-circle"></i>
                                    <span>Thêm giấy tờ</span></span></a>
                            </div>
                        @endcan
                    </div>
                </div>
                <!-- END: Subheader -->
                @can("GATEWAY-viewCertificate")
                <div class="m-content">
                    <!--Begin::Section-->
                    <div class="m-portlet m-portlet--mobile">
                        <div class="m-portlet__body">
                            <form class="m-form m-form--fit m--margin-bottom-20"
                                  action="{{route('Business::certificate@index')}}" method="get">
                                <div class="row m--margin-bottom-20">
                                    <div class="col-lg-4 m--margin-bottom-10-tablet-and-mobile">
                                        <label>
                                            Tìm kiếm theo doanh nghiệp:
                                        </label>
                                        <select class="form-control m-input" data-col-index="7" id="business_id"
                                                name="business_id">
                                            <option value="" selected="selected"></option>
                                            @foreach($businesses as $business)
                                                <option value="{{$business->id}}"
                                                        {{Request::input('business_id')==$business->id?'selected="selected"':''}}
                                                >{{$business->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                                        <label>
                                            Tìm kiếm tên giấy tờ:
                                        </label>
                                        <input type="text" class="form-control m-input" name="name"
                                               value="{{ Request::input('name') }}"
                                               data-col-index="0">
                                    </div>
                                    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                                        <label>
                                            Tìm kiếm mô tả giấy tờ:
                                        </label>
                                        <input type="text" class="form-control m-input" name="description"
                                               value="{{ Request::input('description') }}"
                                               data-col-index="4">
                                    </div>

                                    <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile">
                                        <label>
                                            Chọn trạng thái giấy tờ:
                                        </label>
                                        <select class="form-control m-input" data-col-index="7" id="status"
                                                name="status">
                                            <option value="" selected="selected">--Tất cả--</option>
                                            <option value="0" {{Request::input('status')==="0"?'selected="selected"':''}}>
                                                Chờ duyệt
                                            </option>
                                            <option value="1" {{Request::input('status')==1?'selected="selected"':''}}>
                                                Đã duyệt
                                            </option>
                                            <option value="2" {{Request::input('status')==2?'selected="selected"':''}}>
                                                Đã hủy duyệt
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row m--margin-bottom-20">
                                    <div class="col-lg-4 m--margin-bottom-10-tablet-and-mobile">
                                        <button class="btn btn-brand m-btn m-btn--icon" type="submit">
												<span>
													<i class="la la-search"></i>
													<span>
														Tìm kiếm
													</span>
												</span>
                                        </button>
                                        &nbsp;&nbsp;
                                        <button class="btn btn-secondary m-btn m-btn--icon" type="button"
                                                onclick="window.location='{{route('Business::certificate@index')}}'">
												<span>
													<i class="la la-refresh"></i>
													<span>
														Làm mới
													</span>
												</span>
                                        </button>
                                    </div>
                                </div>
                            </form>
                            @if (session('success'))
                                <div class="m-alert m-alert--outline m-alert--square m-alert--outline-2x alert alert-success alert-dismissible fade show"
                                     role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    </button>
                                    {{ session('success') }}
                                </div>
                            @endif
                            <div class="table-responsive">
                                <table class="table table-striped- table-bordered table-hover table-checkable"
                                       style="margin-top: 20px;margin-left:auto;margin-right:auto;width: 1100px">
                                    <!--begin::Thead-->
                                    <thead>
                                    <tr>
                                        <th style="font-weight: bold">Id</th>
                                        <th style="width: 300px;font-weight: bold">Tên doanh nghiệp</th>
                                        <th style="width: 200px;font-weight: bold">Tên giấy tờ</th>
                                        <th style="width: 300px;font-weight: bold">Mô tả</th>
                                        {{--<th style="width: 150px;font-weight: bold">Trạng thái</th>--}}
                                        <th style="font-weight: bold;width: 80px">Ngày tạo</th>
                                        <th style="font-weight: bold;width: 150px">Hành động</th>
                                    </tr>
                                    </thead>
                                    <!--end::Thead-->
                                    <!--begin::Tbody-->
                                    <tbody>
                                    @foreach($certificates as $certificate)
                                        <tr>
                                            <td>{{$certificate->id}}</td>
                                            <td>{{$certificate->business_name}}</td>
                                            <td>{{$certificate->name}}</td>
                                            <td>{{$certificate->short_description}}</td>
                                            {{--<td>--}}
                                            {{--@if($certificate->status==0)--}}
                                            {{--<a href="{{route('Business::certificate@approve',[$certificate->id])}}"><span--}}
                                            {{--class="m-badge m-badge--warning m-badge--wide">Chờ duyệt </span></a>--}}
                                            {{--@elseif($certificate->status==1)--}}
                                            {{--<a href="{{route('Business::certificate@disapprove',[$certificate->id])}}"><span--}}
                                            {{--class="m-badge m-badge--success m-badge--wide">Đã duyệt </span></a>--}}
                                            {{--@else--}}
                                            {{--<a href="{{route('Business::certificate@approve',[$certificate->id])}}"><span--}}
                                            {{--class="m-badge m-badge--danger m-badge--wide">Đã hủy duyệt</span></a>--}}
                                            {{--@endif--}}
                                            {{--</td>--}}
                                            <td>{{date("d/m/Y",strtotime($certificate->created_at))}}</td>
                                            <td>
                                                @can("GATEWAY-updateCertificate")
                                                <button
                                                        type="button"
                                                        onclick="window.location='{{route('Business::certificate@edit',[$certificate->id])}}'"
                                                        class="btn m-btn--pill m-btn--air btn-outline-info btn-sm"
                                                >Chi tiết
                                                </button>
                                                @endcan
                                                <!--Modal-->
                                                <div id="file{{$certificate->id}}" class="modal fade"
                                                     role="dialog">
                                                    <div class="modal-dialog">
                                                        <!-- Modal content-->
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h1 class="modal-title">Files tài liệu của
                                                                    "{{$certificate->business_name}}"</h1>
                                                                <button type="button" class="close"
                                                                        data-dismiss="modal">&times;
                                                                </button>
                                                            </div>
                                                            <form method="POST"
                                                                  action="{{ route('Business::subscription@updateContract')}}">
                                                                <div class="modal-body">
                                                                    <input type="hidden" name="subscription_id"
                                                                           value="{{$certificate->id}}">
                                                                    <div>
                                                                        <ol class="heroes upload_files{{$certificate->id}}_input_list">

                                                                        </ol>
                                                                    </div>
                                                                    <div style="text-align: center">
                                                                        <label for="upload_files{{$certificate->id}}_input"
                                                                               style="cursor: pointer;padding:8px;background-color: #0aa7ef;color: white;border-radius: 20px"><i
                                                                                    class="la la-cloud-upload"></i> Chọn
                                                                            tập tin</label>
                                                                        <input type="file" name="contract_files[]"
                                                                               id="upload_files{{$certificate->id}}_input"
                                                                               class="upload"
                                                                               style="display: none!important;"
                                                                               accept=".png,.jpg,.jpeg,.gif,.pdf,.doc,.docx"
                                                                               multiple>
                                                                    </div>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="submit"
                                                                            class="btn btn-success">
                                                                        Cập nhật
                                                                    </button>
                                                                    <button type="button"
                                                                            class="btn btn-default"
                                                                            data-dismiss="modal">Hủy bỏ
                                                                    </button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--End Modal-->
                                            {{--<button type="button" class="btn m-btn--pill m-btn--air btn-outline-danger btn-sm"--}}
                                            {{--data-toggle="modal" data-target="#delete{{$certificate->id}}"--}}
                                            {{-->--}}
                                            {{--Xóa--}}
                                            {{--</button>--}}
                                            <!--Modal Comfirm delete-->
                                                <div id="delete{{$certificate->id}}" class="modal fade"
                                                     role="dialog">
                                                    <div class="modal-dialog">
                                                        <!-- Modal content-->
                                                        <div class="modal-content">
                                                            <div class="modal-header">

                                                                <h2 class="modal-title">Bạn chắc
                                                                    chắn muốn xóa bỏ ?</h2>
                                                                <button type="button" class="close"
                                                                        data-dismiss="modal">&times;
                                                                </button>
                                                            </div>
                                                            <form method="get"
                                                                  action="{{ route('Business::certificate@delete',[$certificate->id]) }}">
                                                                {{ csrf_field() }}
                                                                <div class="modal-footer">
                                                                    <button type="submit"
                                                                            class="btn btn-success">
                                                                        Đồng ý
                                                                    </button>
                                                                    <button type="button"
                                                                            class="btn btn-default"
                                                                            data-dismiss="modal">Hủy
                                                                        yêu cầu
                                                                    </button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--End Modal-->
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <!--end::Tbody-->
                                </table>
                            </div>
                            <!--end::Table-->
                            <div style="float:right;"><?php echo $certificates->links(); ?></div>
                        </div>
                        <!--End::Section-->
                    </div>
                </div>
                @endcan
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript" src="{{ asset('qr_code/metronic/js/vendors.bundle.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            // Basic
            $("#status").select2({
                placeholder: 'Chọn trạng thái',
                allowClear: true
            });
            $("#business_id").select2({
                placeholder: 'Chọn doanh nghiệp',
                allowClear: true
            });
            $("select").on("select2:select", function (evt) {
                var element = evt.params.data.element;
                var $element = $(element);
                $element.detach();
                $(this).append($element);
                $(this).trigger("change");
            });


        });

        //Fill Data to Modal
        $(".certificate_modal").click(function () {
            let id = $(this).attr('id');
            $(`.${id}_input_list`).empty();
            let value = $(this).attr('value');
            let file = JSON.parse(value);
            appendData(id, file);
        });

        //Upload files
        $(".upload").change(function () {
            let id = $(this).attr('id');
            let x = document.getElementById(id);
            let spinner = `<div class="spinner m-loader m-loader--primary" style="width: 30px; display: inline-block;">
                            </div>`;
            $(`.${id}_list`).append(`${spinner}`);
            var files = x.files;
            var formData = new FormData();
            // Loop through each of the selected files.
            for (let i = 0; i < files.length; i++) {
                let file = files[i];
                // Check the file type.
//                if (!file.type.match('image.*')) {
//                    continue;
//                }

                // Add the file to the request.
                formData.append('files[]', file, file.name);

            }

            var url = "";
//            var token = $("input[name='_token']").val();
            $.ajax({
                url: url,
                method: 'POST',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    $("div").remove(".spinner");
                    append_id = id.replace("_input", "");
                    $.each(data, function (key, value) {
                        let file = JSON.parse(value);
                        appendData(append_id, file);
                    });
                }
            });
        });
        var removeItem = function (e) {
            e.closest('li').remove();
        };

        function appendData(id, file) {
            for (let i = 0; i < file.length; i++) {
                let file_name = file[i][0].split("_").pop();
                let html = `<li>
                                <div style="text-align: left;display: inline-block;width: 70%">${file_name}</div>
                                <input type="hidden" name="uploaded_files[]" value="${file[i][0]}">
                                <div style="text-align: right;display: inline-block;float: right;">
                                    <a href="${file[i][1]}" target="_blank" data-toggle="tooltip" title="Xem" class="btn btn-outline-accent m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air"> <i class="la la-eye"></i></a>

                                </div>
                           </li>`;
                $(`.${id}_input_list`).append(`${html}`);
            }
        }

    </script>
@endpush
