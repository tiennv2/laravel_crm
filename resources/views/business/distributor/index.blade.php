@extends('qrcode.layouts.app')
@section('body_class', 'm-page--fluid m-page--loading-enabled m-page--loading m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default')
@push('styles')
  <style>
    .select2 {
      width: 100% !important;
    }
  </style>
@endpush
@push('scripts')
  <script type="text/javascript">
    <?php
      $sharedData = [
        'config' => [
          'csrfToken' => csrf_token(),
          'logoutUrl' => route('logout'),
          'isNative' => true,
        ],
        'loggedInAccount' => auth()->check() ? auth()->user() : null,
      ];
      ?>
      window.SHARED_DATA = {!! json_encode($sharedData) !!}
  </script>
  <script src="https://cdn.jsdelivr.net/npm/tinymce@4.7.11/tinymce.min.js" type="text/javascript"></script>
  <script src="{{ mix('js/app.js') }}"></script>
@endpush

@section('content')
  <!-- begin::Page loader -->
  <div class="m-page-loader m-page-loader--base">
    <div class="m-blockui">
      <span>
        Please wait...
      </span>
      <span>
        <div class="m-loader m-loader--brand"></div>
      </span>
    </div>
  </div>
  <!-- end::page Loader -->
  <div id="app" class="m-grid m-grid--hor m-grid--root m-page">
    <app-header></app-header>
    <div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop  m-body m-page__container">
      <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader">
          <div class="d-flex align-items-center">
            <div class="mr-auto">
              <h3 class="m-subheader__title ">
                QUẢN LÝ NHÀ PHÂN PHỐI
              </h3>
            </div>
            @can("GATEWAY-addDistributor")
              <div style="float: right"><a href="{{route('Business::distributor@add')}}"
                                           class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air"><span>
                        <i class="la la-plus-circle"></i>
                        <span>Thêm Nhà phân phối</span></span></a>
              </div>
            @endcan
          </div>
        </div>
        <!-- END: Subheader -->
        @can("GATEWAY-viewDistributor")
          <div class="m-content">
            <!--Begin::Section-->
            <div class="m-portlet m-portlet--mobile">
              <div class="m-portlet__body">
                <form class="m-form m-form--fit m--margin-bottom-20"
                      action="{{route('Business::distributor@index')}}" method="get">
                  <div class="row m--margin-bottom-20">
                    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                      <label>
                        Tìm kiếm theo doanh nghiệp:
                      </label>
                      <select class="form-control m-input" data-col-index="7" id="business_id"
                              name="business_id">
                        <option value="" selected="selected"></option>
                        @foreach($businesses as $business)
                          <option value="{{$business->id}}"
                            {{Request::input('business_id')==$business->id?'selected="selected"':''}}
                          >{{$business->name}}</option>
                        @endforeach
                      </select>
                    </div>
                    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                      <label>
                        Tên Nhà phân phối:
                      </label>
                      <input type="text" class="form-control m-input" name="name"
                             value="{{ Request::input('name') }}"
                             data-col-index="0">
                    </div>
                    <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile">
                      <label>
                        Địa chỉ:
                      </label>
                      <input type="text" class="form-control m-input" name="address"
                             value="{{ Request::input('address') }}"
                             data-col-index="0">
                    </div>
                    <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile">
                      <label>
                        Số điện thoại:
                      </label>
                      <input type="text" class="form-control m-input" name="phone"
                             value="{{ Request::input('phone') }}"
                             data-col-index="4">
                    </div>

                    <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile">
                      <label>
                        Email:
                      </label>
                      <input type="text" class="form-control m-input" name="email"
                             value="{{ Request::input('email') }}"
                             data-col-index="4">
                    </div>
                  </div>
                  <div class="row m--margin-bottom-20">
                    <div class="col-lg-4 m--margin-bottom-10-tablet-and-mobile">
                      <label>
                        Ngày tạo:
                      </label>
                      <div class="input-daterange input-group">
                        <input type="text" class="form-control" name="from" id="m_datepicker_1"
                               value="{{ Request::input('from') }}" autocomplete="off"
                               placeholder="Từ"
                               data-col-index="5"/>
                        <div class="input-group-append">
													<span class="input-group-text">
														<i class="la la-ellipsis-h"></i>
													</span>
                        </div>
                        <input type="text" class="form-control" name="to" id="m_datepicker_1"
                               value="{{ Request::input('to') }}" autocomplete="off"
                               placeholder="Đến"
                               data-col-index="5"/>
                      </div>
                    </div>
                    <div class="col-lg-4 m--margin-bottom-10-tablet-and-mobile">
                      <label style="color: white">
                        Ngày tạo:
                      </label>
                      <div>
                        <button class="btn btn-brand m-btn m-btn--icon" type="submit">
												<span>
													<i class="la la-search"></i>
													<span>
														Tìm kiếm
													</span>
												</span>
                        </button>
                        &nbsp;&nbsp;
                        <button class="btn btn-secondary m-btn m-btn--icon" type="button"
                                onclick="window.location='{{route('Business::distributor@index')}}'">
												<span>
													<i class="la la-refresh"></i>
													<span>
														Làm mới
													</span>
												</span>
                        </button>
                      </div>
                    </div>
                  </div>
                </form>

                @if (session('success'))
                  <div
                    class="m-alert m-alert--outline m-alert--square m-alert--outline-2x alert alert-success alert-dismissible fade show"
                    role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    </button>
                    {{ session('success') }}
                  </div>
                @endif
                <div
                  style="width:200px;text-align:center;padding:5px;margin-bottom: 0;margin-top: 50px;margin-left: 50px;font-weight: bold;font-style: italic;border: solid 1px;border-radius: 6px">
                  Đang hiển thị {{$count_record_in_page}}/{{$count_record_total}} bản ghi
                </div>
                <div class="table-responsive">
                  <table class="table table-striped- table-bordered table-hover table-checkable"
                         style="margin-top: 20px;margin-left:auto;margin-right:auto;width: 1600px">
                    <!--begin::Thead-->
                    <thead>
                    <tr>
                      <th style="width: 50px;font-weight: bold">Id</th>
                      <th style="width: 400px;font-weight: bold">Tên</th>
                      <th style="width: 400px;font-weight: bold">Của Doanh nghiệp</th>
                      <th style="font-weight: bold;width: 300px">Địa chỉ</th>
                      <th style="font-weight: bold;width: 300px">Email</th>
                      <th style="width: 150px;font-weight: bold">Số điện thoại</th>
                      {{--<th style="width: 150px;font-weight: bold">Trạng thái</th>--}}
                      <th style="font-weight: bold;width: 100px">Ngày tạo</th>
                      <th style="font-weight: bold;width: 150px">Hành động</th>
                    </tr>
                    </thead>
                    <!--end::Thead-->
                    <!--begin::Tbody-->
                    <tbody>
                    @foreach($distributors as $distributor)
                      <tr>
                        <td>{{$distributor->id}}</td>
                        <td>
                          {{--@if(auth()->user()->can('GATEWAY-updateDistributor'))--}}
                          {{--<a href="{{route('Business::distributor@edit',[$distributor->id])}}">{{$distributor->name}}</a>--}}
                          {{--@else--}}
                          {{--{{$distributor->name}}--}}
                          {{--@endif--}}
                          <a
                            href="{{route('Business::distributor@edit',[$distributor->id])}}">{{$distributor->name}}</a>
                        </td>
                        <td>
                          <a href="{{route('Business::businesses@index')}}?business_id={{$distributor->business_id}}"
                             target="_blank">
                            {{$distributor->business_name}}
                          </a>
                        </td>
                        <td>{{$distributor->address}}</td>
                        <td>{{$distributor->email}}</td>
                        <td>{{$distributor->phone}}</td>
                        {{--<td>--}}
                        {{--@if($distributor->status==0)--}}
                        {{--<span class="m-badge m-badge--warning m-badge--wide">Chờ duyệt </span>--}}
                        {{--@elseif($distributor->status==1)--}}
                        {{--<span class="m-badge m-badge--success m-badge--wide">Đã duyệt </span>--}}
                        {{--@endif--}}
                        {{--</td>--}}
                        <td>{{date("d/m/Y",strtotime($distributor->created_at))}}</td>
                        <td>
                          @can("GATEWAY-delDistributor")
                            <a onclick="return delItem();"
                               href='{{route('Business::distributor@delete',[$distributor->id])}}'
                               title="Xóa"
                               class="btn btn-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill">
                              <i class="fa fa-trash-o"></i>
                            </a>
                          @endcan
                          {{--<button--}}
                          {{--type="button"--}}
                          {{--onclick="window.location='{{route('Business::distributor@approve',[$distributor->id])}}'"--}}
                          {{--title="Duyệt"--}}
                          {{--class="btn btn-success m-btn m-btn--icon m-btn--icon-only m-btn--pill">--}}
                          {{--<i class="la la-check"></i>--}}
                          {{--</button>--}}
                        </td>
                      </tr>
                    @endforeach
                    </tbody>
                    <!--end::Tbody-->
                  </table>
                </div>
                <!--end::Table-->
                <div style="float:right;"><?php echo $distributors->links(); ?></div>
              </div>
              <!--End::Section-->
            </div>
          </div>
        @endcan
      </div>
    </div>
  </div>
@endsection
@push('scripts')
  <script type="text/javascript" src="{{ asset('qr_code/metronic/js/vendors.bundle.js') }}"></script>
  <script type="text/javascript" src="{{ asset('qr_code/metronic/js/datepicker/bootstrap-datepicker.js') }}"></script>
  <script type="text/javascript" src="{{ asset('qr_code/metronic/js/datatables/datatables.bundle.js') }}"></script>
  <script type="text/javascript" src="{{ asset('qr_code/metronic/js/datatables/scrollable.js') }}"></script>
  <script type="text/javascript">

    $(document).ready(function () {
      // Basic
      $("#business_id").select2({
        placeholder: 'Chọn Doanh nghiệp',
        allowClear: true
      });

      $("select").on("select2:select", function (evt) {
        var element = evt.params.data.element;
        var $element = $(element);
        $element.detach();
        $(this).append($element);
        $(this).trigger("change");
      });


    });

    function delItem() {
      let conf = confirm("Bạn chắc chắn muốn xoá Nhà phân phối này?");
      return conf;
    }

  </script>

@endpush
