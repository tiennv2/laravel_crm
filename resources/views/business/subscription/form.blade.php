@extends('qrcode.layouts.app')
@push('styles')
  <style>
    .help-block {
      color: red;
    }
  </style>

@endpush
@section('body_class', 'm-page--fluid m-page--loading-enabled m-page--loading m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default')
@push('scripts')
  <script type="text/javascript">
    <?php
      $sharedData = [
        'config' => [
          'csrfToken' => csrf_token(),
          'logoutUrl' => route('logout'),
          'isNative' => true,
        ],
        'loggedInAccount' => auth()->check() ? auth()->user() : null,
      ];
      ?>
      window.SHARED_DATA = {!! json_encode($sharedData) !!}
  </script>
  <script src="https://cdn.jsdelivr.net/npm/tinymce@4.7.11/tinymce.min.js" type="text/javascript"></script>
  <script src="{{ mix('js/app.js') }}"></script>
@endpush

@section('content')
  <!-- begin::Page loader -->
  <div class="m-page-loader m-page-loader--base">
    <div class="m-blockui">
      <span>
        Please wait...
      </span>
      <span>
        <div class="m-loader m-loader--brand"></div>
      </span>
    </div>
  </div>
  <!-- end::Page Loader -->
  <div id="app" class="m-grid m-grid--hor m-grid--root m-page">
    <app-header></app-header>
    <div
      class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop 	m-container m-container--responsive m-container--xxl m-page__container m-body">
      <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-content">
          <div class="m-portlet">
            <div class="m-portlet__head">
              <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                  <h3 class="m-portlet__head-text">
                    <a href="{{route('Business::subscription@index')}}">
                      <i class="la la-arrow-left"></i>
                    </a>
                    {{ isset($subscription) ? 'Sửa thông tin Gói dịch vụ của Doanh nghiệp' : 'Đăng ký mới Gói dịch vụ' }}
                  </h3>
                </div>
              </div>
            </div>
            <!-- /page header -->
            @if (session('success'))
              <div
                class="m-alert m-alert--outline m-alert--square m-alert--outline-2x alert alert-success alert-dismissible fade show"
                role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                </button>
                {{ session('success') }}
              </div>
            @endif
            <form method="POST" enctype="multipart/form-data" name="form"
                  class="m-form m-form--fit m-form--label-align-right"
                  action="{{ isset($subscription) ? route('Business::subscription@update', [$subscription->id] ): route('Business::subscription@store') }}">
              {{ csrf_field() }}
              @if (isset($subscription))
                <input type="hidden" name="_method" value="PUT">
              @endif
              <div class="m-portlet__body">
                <!------------------ Business--------------->
                <div class="form-group m-form__group row">
                  <label class="col-form-label col-lg-3 col-sm-12">Chọn doanh nghiệp *</label>
                  <div class="col-lg-4 col-md-9 col-sm-12">
                    <select class="form-control m-input" data-col-index="7" id="business_id"
                            name="business_id"
                            required>
                      <option value=""></option>
                      @foreach($businesses as $business)
                        <option value="{{$business->id}}"
                                @if (isset($subscription) && $subscription->business_id==$business->id)
                                selected
                          @endif
                        >
                          {{$business->name}}
                        </option>
                      @endforeach
                    </select>
                  </div>
                  @if ($errors->has('business_id'))
                    <div class="help-block">{{ $errors->first('business_id') }}</div>
                  @endif
                </div>
                <!------------------ Plan--------------->
                <div class="form-group m-form__group row">
                  <label class="col-form-label col-lg-3 col-sm-12">Chọn gói dịch vụ *</label>
                  <div class="col-lg-4 col-md-9 col-sm-12">
                    <select class="form-control m-input" data-col-index="7" name="plan_id"
                            @if (isset($subscription) && $subscription->status == \App\Models\Business\Subscription::SUBSCRIPTION_STATUS_APPROVED) disabled
                            @else
                            required
                      @endif>
                      <option value=""></option>
                      @foreach($plans as $plan)
                        <option value="{{$plan->id}}"
                                @if (isset($subscription) && $subscription->plan_id==$plan->id)
                                selected
                          @endif
                        >
                          {{$plan->name}} ({{$plan->quota}} sản
                          phẩm)
                        </option>
                      @endforeach
                    </select>
                    @if (isset($subscription) && $subscription->status == \App\Models\Business\Subscription::SUBSCRIPTION_STATUS_APPROVED)
                      <input type="hidden" name="plan_id" value="{{$subscription->plan_id}}">
                    @endif
                  </div>
                  @if ($errors->has('plan_id'))
                    <div class="help-block">{{ $errors->first('plan_id') }}</div>
                  @endif
                </div>
                <!------------------Contract Period--------------->
                <div class="form-group m-form__group row">
                  <label class="col-form-label col-lg-3 col-sm-12">Thời hạn Hợp đồng *</label>
                  <div class="col-lg-4 col-md-9 col-sm-12">
                    <select class="form-control m-input" data-col-index="7" name="days"
                            required>
                      <option value=""></option>
                      <option value="7" @if (isset($subscription) && $subscription->days==7)selected @endif>7 ngày
                      </option>
                      <option value="30" @if (isset($subscription) && $subscription->days==30)selected @endif>30 ngày
                      </option>
                      <option value="365" @if (isset($subscription) && $subscription->days==365)selected @endif>1 năm
                      </option>
                      <option value="730" @if (isset($subscription) && $subscription->days==730)selected @endif>2 năm
                      </option>
                      <option value="1095" @if (isset($subscription) && $subscription->days==1095)selected @endif>3
                        năm
                      </option>
                      <option value="1460" @if (isset($subscription) && $subscription->days==1460)selected @endif>4
                        năm
                      </option>
                      <option value="1825" @if (isset($subscription) && $subscription->days==1825)selected @endif>5
                        năm
                      </option>
                    </select>
                  </div>
                  @if ($errors->has('days'))
                    <div class="help-block">{{ $errors->first('days') }}</div>
                  @endif
                </div>
                <!------------------Giá trị Hợp đồng--------------->
                <div class="form-group m-form__group row">
                  <label class="col-form-label col-lg-3 col-sm-12">Giá trị theo Hợp đồng </label>
                  <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="number" class="form-control m-input" name="contract_value" min="0"
                           value="{{ old('contract_value') ?: @$subscription->contract_value}}"
                           data-col-index="0">
                  </div>
                </div>
                <!------------------Ngày bắt đầu Hợp đồng--------------->
                <div class="form-group m-form__group row">
                  <label class="col-form-label col-lg-3 col-sm-12">Ngày bắt đầu hợp đồng </label>
                  <div class="col-lg-4 col-md-9 col-sm-12">
                    {{--<input class="form-control m-input" type="date" name="start_time"--}}
                    {{--value="{{ old('start_time') ?: @$subscription->start_time}}"/>--}}

                    <input type="text" class="start_time form-control" name="start_time"
                           data-date-format="dd/mm/yyyy"
                           value="{{ old('start_time') ?: @$subscription->start_time}}" autocomplete="off"
                           data-col-index="5"/>

                  </div>
                  @if ($errors->has('start_time'))
                    <div class="help-block">{{ $errors->first('start_time') }}</div>
                  @endif
                </div>
                <!------------------Ngày hết hạn--------------->
                <div class="form-group m-form__group row">
                  <label class="col-form-label col-lg-3 col-sm-12">Ngày hết hạn hợp đồng *</label>
                  <div class="col-lg-4 col-md-9 col-sm-12">
                    {{--<input class="form-control m-input" type="date" name="expires_on"--}}
                    {{--value="{{ old('expires_on') ?: @$subscription->expires_on}}" required/>--}}

                    <input type="text" class="contract_expiration form-control" name="contract_expiration"
                           data-date-format="dd/mm/yyyy"
                           value="{{ old('contract_expiration') ?: @$subscription->contract_expiration}}"
                           autocomplete="off" required
                           data-col-index="5"/>
                  </div>
                  @if ($errors->has('contract_expiration'))
                    <div class="help-block">{{ $errors->first('contract_expiration') }}</div>
                  @endif
                </div>
                <!------------------Demo Period--------------->
                @if(!isset($subscription))
                  <div class="demo-period form-group m-form__group row">
                    <label class="col-form-label col-lg-3 col-sm-12">Thời hạn Demo </label>
                    <div class="col-lg-4 col-md-9 col-sm-12">
                      <select class="form-control m-input"
                              data-col-index="7" name="demo_days"
                              @if(isset($subscription) && $subscription->payment_status == \App\Models\Business\Subscription::PAYMENT_STATUS_TRUE)
                              disabled
                              @endif
                              required
                      >
                        <option value=""></option>
                        <option value="7">7 ngày
                        </option>
                        <option value="30">30 ngày
                        </option>
                      </select>
                    </div>
                  </div>
                @endif
              <!------------------Ngày hết demo--------------->
                <div class="demo-expired form-group m-form__group row">
                  <label class="col-form-label col-lg-3 col-sm-12">Ngày hết hạn demo @if(!isset($subscription) || isset($subscription) && $subscription->trial_expiration) * @endif</label>
                  <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="text" class="trial_expiration form-control" name="trial_expiration"
                           data-date-format="dd/mm/yyyy"
                           value="{{ old('trial_expiration') ?: @$subscription->trial_expiration}}" autocomplete="off"
                           {{--@if(isset($subscription) && $subscription->payment_status == \App\Models\Business\Subscription::PAYMENT_STATUS_TRUE)--}}
                           {{--disabled--}}
                           {{--@endif--}}
                           @if(!isset($subscription) || isset($subscription) && $subscription->trial_expiration)
                           required
                           @endif
                           data-col-index="5"/>
                  </div>
                  @if ($errors->has('trial_expiration'))
                    <div class="help-block">{{ $errors->first('trial_expiration') }}</div>
                  @endif
                </div>

                <!------------------Loại ngày hết hạn--------------->
                {{--<div class="form-group m-form__group row">--}}
                {{--<label class="col-form-label col-lg-3 col-sm-12">Loại ngày hết hạn *</label>--}}
                {{--<div class="col-lg-4 col-md-9 col-sm-12">--}}
                {{--<select class="form-control m-input" data-col-index="7" name="expire_type"--}}
                {{--required>--}}
                {{--<option value="{{\App\Models\Business\Subscription::EXPIRE_TYPE_IN_CONTRACT}}"--}}
                {{--@if (isset($subscription) && $subscription->expire_type==\App\Models\Business\Subscription::EXPIRE_TYPE_IN_CONTRACT)selected @endif>--}}
                {{--Theo hợp đồng--}}
                {{--</option>--}}
                {{--<option value="{{\App\Models\Business\Subscription::EXPIRE_TYPE_DEMO}}"--}}
                {{--@if (isset($subscription) && $subscription->expire_type===\App\Models\Business\Subscription::EXPIRE_TYPE_DEMO)selected @endif>--}}
                {{--Demo--}}
                {{--</option>--}}
                {{--</select>--}}
                {{--</div>--}}
                {{--@if ($errors->has('expire_type'))--}}
                {{--<div class="help-block">{{ $errors->first('expire_type') }}</div>--}}
                {{--@endif--}}
                {{--</div>--}}
              <!------------------Nhân viên chăm sóc khách hàng--------------->
                <div class="form-group m-form__group row">
                  <label class="col-form-label col-lg-3 col-sm-12">Người phụ trách </label>
                  <div class="col-lg-4 col-md-9 col-sm-12">
                    <select class="supporter form-control m-input" name="assigned_to">
                      <option>Chưa rõ</option>
                      @foreach($icheckUsers as $user)
                        <option value="{{$user->icheck_id}}"
                                @if(isset($subscription) && $subscription->assigned_to == $user->icheck_id) selected @endif
                        >{{$user->name}} ({{$user->icheck_id}})
                        </option>
                      @endforeach
                    </select>
                  </div>
                </div>
                {{--@endif--}}
                <div class="m-portlet__foot m-portlet__foot--fit">
                  <div class="m-form__actions m-form__actions">
                    <div class="row">
                      <div class="col-lg-9 ml-lg-auto">
                        <button type="submit" class="btn btn-success">
                          {{ isset($subscription) ? 'Cập nhật' : 'Thêm mới' }}
                        </button>
                        <button type="reset" class="btn btn-secondary">
                          {{ isset($subscription) ? 'Hủy' : 'Nhập lại' }}
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
@push('scripts')
  <script type="text/javascript" src="{{ asset('qr_code/metronic/js/vendors.bundle.js') }}"></script>
  <script type="text/javascript">
    $(document).ready(function () {
// Basic
      $("#business_id").select2({
        placeholder: 'Chọn doanh nghiệp',
        allowClear: true
      });
      $(".supporter").select2({
        placeholder: 'Chọn người phụ trách',
        allowClear: true
      });

      $("select").on("select2:select", function (evt) {
        var element = evt.params.data.element;
        var $element = $(element);
        $element.detach();
        $(this).append($element);
        $(this).trigger("change");
      });
    });
    if ($("select[name='days']").val() == 7) {
      $(".demo-period").hide();
      $(".demo-expired").hide();
    }

    // Update Times for Subscription
    $("select[name='days']").change(function () {
      $(".demo-period").show();
      $(".demo-expired").show();
      let days = $(this).val();
      if (!days) {
        $("input[name='start_time']").val("");
        $("input[name='contract_expiration']").val("");
        return false;
      }
      if (parseInt(days) === 7) {
        $("input[name='trial_expiration']").val("");
        $("select[name='demo_days']").val("");
        $(".demo-period").hide();
        $(".demo-expired").hide();
      }
      let currentDate = formatDate(new Date());
      $("input[name='start_time']").val(currentDate);
      let today = new Date();
      today.setDate(today.getDate() + parseInt(days));
      let expire_date = formatDate(today);
      $("input[name='contract_expiration']").val(expire_date);
    });

    $("select[name='demo_days']").change(function () {
      let days = $(this).val();
      if (!days) {
        $("input[name='trial_expiration']").val("");
        return false;
      }
      let today = new Date();
      today.setDate(today.getDate() + parseInt(days));
      let expire_demoDate = formatDate(today);
      $("input[name='trial_expiration']").val(expire_demoDate);
    });

    function formatDate(date) {
      let twoDigitMonth = date.getMonth() + 1 + "";
      if (twoDigitMonth.length === 1) {
        twoDigitMonth = "0" + twoDigitMonth;
      }
      return (date.getDate() + "/" + twoDigitMonth + "/" + date.getFullYear());
    }

    $(".start_time").datepicker({
      dateFormat: 'dd/mm/yy',
      changeMonth: true,
      changeYear: true,
      todayHighlight: !0,
      orientation: "bottom left",
      templates: {leftArrow: '<i class="la la-angle-left"></i>', rightArrow: '<i class="la la-angle-right"></i>'}
    });

    $(".contract_expiration").datepicker({
      dateFormat: 'dd/mm/yy',
      changeMonth: true,
      changeYear: true,
      todayHighlight: !0,
      orientation: "bottom left",
      templates: {leftArrow: '<i class="la la-angle-left"></i>', rightArrow: '<i class="la la-angle-right"></i>'}
    });

    $(".trial_expiration").datepicker({
      dateFormat: 'dd/mm/yy',
      changeMonth: true,
      changeYear: true,
      todayHighlight: !0,
      orientation: "bottom left",
      templates: {leftArrow: '<i class="la la-angle-left"></i>', rightArrow: '<i class="la la-angle-right"></i>'}
    });

  </script>

@endpush
