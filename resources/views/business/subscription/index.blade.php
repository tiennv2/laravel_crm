@extends('qrcode.layouts.app')
@push('styles')
    <style>
        .heroes {
            margin: 0 0 2em 0;
            padding: 0;
            width: 30em;
        }

        .heroes li {
            position: relative;
            left: 0;
            color: #00c5dc;
            margin: .5em;
            padding: .3em .3em;
            height: auto;
            min-height: 3em;
            border-radius: 4px;
            border-color: #00c5dc;
            border-width: 2px;
            border-style: solid;
        }

        img {
            cursor: zoom-in;
        }

        body {
            overflow-x: hidden;
        }

        .select2 {
            width: 100% !important;
        }
    </style>
@endpush
@section('body_class', 'm-page--fluid m-page--loading-enabled m-page--loading m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default')

@push('scripts')
    <script type="text/javascript">
        <?php
            $sharedData = [
                'config' => [
                    'csrfToken' => csrf_token(),
                    'logoutUrl' => route('logout'),
                    'isNative' => true,
                ],
                'loggedInAccount' => auth()->check() ? auth()->user() : null,
            ];
            ?>
            window.SHARED_DATA = {!! json_encode($sharedData) !!}
    </script>
    <script src="https://cdn.jsdelivr.net/npm/tinymce@4.7.11/tinymce.min.js" type="text/javascript"></script>
    <script src="{{ mix('js/app.js') }}"></script>
@endpush

@section('content')
    <!-- begin::Page loader -->
    <div class="m-page-loader m-page-loader--base">
        <div class="m-blockui">
      <span>
        Please wait...
      </span>
            <span>
        <div class="m-loader m-loader--brand"></div>
      </span>
        </div>
    </div>
    <!-- end::page Loader -->
    <div id="app" class="m-grid m-grid--hor m-grid--root m-page">
        <app-header></app-header>
        <div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop  m-body m-page__container">
            <div class="m-grid__item m-grid__item--fluid m-wrapper">
                <!-- BEGIN: Subheader -->
                <div class="m-subheader">
                    <div class="d-flex align-items-center">
                        <div class="mr-auto">
                            <h3 class="m-subheader__title">
                                QUẢN LÝ ĐĂNG KÝ GÓI DỊCH VỤ
                            </h3>
                            @can("GATEWAY-viewPlan")
                                <a href="{{route('Business::plan@index')}}" target="_blank"
                                   class="btn btn-outline-brand m-btn btn-sm m-btn--icon m-btn--pill"><span>
                                    <span style="font-size: 14px">Xem danh sách Gói dịch vụ</span></span></a>
                            @endcan
                        </div>
                        @can("GATEWAY-addSubscription")
                            <div style="float: right">
                                <a href="{{route('Business::subscription@add')}}"
                                   class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air"><span>
                                    <i class="la la-plus-circle"></i>
                                    <span>Đăng ký gói dịch vụ</span></span></a>
                            </div>
                        @endcan
                    </div>
                </div>
                <!-- END: Subheader -->
                @can("GATEWAY-viewSubscription")
                    <div class="m-content">
                        <!--Begin::Section-->
                        <div class="m-portlet m-portlet--mobile">
                            <div class="m-portlet__body">
                                <form class="m-form m-form--fit m--margin-bottom-20"
                                      action="{{route('Business::subscription@index')}}" method="get">
                                    <div class="row m--margin-bottom-20">
                                        <div class="col-lg-4 m--margin-bottom-10-tablet-and-mobile">
                                            <label>
                                                Tìm kiếm theo doanh nghiệp:
                                            </label>
                                            <select class="business_id form-control m-input" data-col-index="7"
                                                    name="business_id">
                                                <option value="" selected="selected"></option>
                                                @foreach($businesses as $business)
                                                    <option value="{{$business->id}}"
                                                            {{Request::input('business_id')==$business->id?'selected="selected"':''}}
                                                    >{{$business->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                                            <label>
                                                Tìm kiếm theo gói dịch vụ:
                                            </label>
                                            <select class="plan_id form-control m-input" data-col-index="7"
                                                    name="plan_id">
                                                <option value="" selected="selected"></option>
                                                @foreach($plans as $plan)
                                                    <option value="{{$plan->id}}"
                                                            {{Request::input('plan_id')==$plan->id?'selected="selected"':''}}
                                                    >{{$plan->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile">
                                            <label>
                                                Chọn trạng thái:
                                            </label>
                                            <select class="status form-control m-input" data-col-index="7"
                                                    name="status">
                                                <option value="" selected="selected">--Tất cả--</option>
                                                <option value="0" {{Request::input('status')==="0"?'selected="selected"':''}}>
                                                    Chờ kích hoạt
                                                </option>
                                                <option value="1" {{Request::input('status')== 1?'selected="selected"':''}}>
                                                    Kích hoạt
                                                </option>
                                                <option value="2" {{Request::input('status')== 2?'selected="selected"':''}}>
                                                    Hủy kích hoạt
                                                </option>
                                            </select>
                                        </div>
                                        <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                                            <label>
                                                Chọn trạng thái thanh toán:
                                            </label>
                                            <select class="payment_status form-control m-input" data-col-index="7"
                                                    name="payment_status">
                                                <option value="" selected="selected">--Tất cả--</option>
                                                {{--<option--}}
                                                {{--value="{{\App\Models\Business\Subscription::PAYMENT_STATUS_UNKNOWN}}" {{(Request::input('payment_status') != "" && Request::input('payment_status') == \App\Models\Business\Subscription::PAYMENT_STATUS_UNKNOWN) ?'selected="selected"':''}}>--}}
                                                {{--Chưa rõ--}}
                                                {{--</option>--}}
                                                <option
                                                        value="{{\App\Models\Business\Subscription::PAYMENT_STATUS_FALSE}}" {{Request::input('payment_status')== \App\Models\Business\Subscription::PAYMENT_STATUS_FALSE ?'selected="selected"':''}}>
                                                    Chưa thanh toán
                                                </option>
                                                {{--<option--}}
                                                {{--value="{{\App\Models\Business\Subscription::PAYMENT_STATUS_PARTIAL}}" {{Request::input('payment_status')== \App\Models\Business\Subscription::PAYMENT_STATUS_PARTIAL ?'selected="selected"':''}}>--}}
                                                {{--Thanh toán một phần--}}
                                                {{--</option>--}}
                                                <option
                                                        value="{{\App\Models\Business\Subscription::PAYMENT_STATUS_TRUE}}" {{Request::input('payment_status')== \App\Models\Business\Subscription::PAYMENT_STATUS_TRUE ?'selected="selected"':''}}>
                                                    Đã thanh toán
                                                </option>

                                            </select>
                                        </div>
                                        <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                                            <label>
                                                Thời hạn gói:
                                            </label>
                                            <select class="days form-control m-input" data-col-index="7"
                                                    name="days">
                                                <option value="" selected="selected">--Tất cả--</option>
                                                <option value="7" {{Request::input('days')== 7 ?'selected="selected"':''}}>
                                                    7 ngày
                                                </option>
                                                <option value="30" {{Request::input('days')== 30 ?'selected="selected"':''}}>
                                                    30 ngày
                                                </option>
                                                <option value="365" {{Request::input('days')== 365 ?'selected="selected"':''}}>
                                                    1 năm
                                                </option>
                                                <option value="730" {{Request::input('days')== 730 ?'selected="selected"':''}}>
                                                    2 năm
                                                </option>
                                                <option value="1095" {{Request::input('days')== 1095 ?'selected="selected"':''}}>
                                                    3 năm
                                                </option>
                                                <option value="1460" {{Request::input('days')== 1460 ?'selected="selected"':''}}>
                                                    4 năm
                                                </option>
                                                <option value="1825" {{Request::input('days')== 1825 ?'selected="selected"':''}}>
                                                    5 năm
                                                </option>
                                            </select>
                                        </div>
                                        <div class="col-lg-4 m--margin-bottom-10-tablet-and-mobile">
                                            <label style="color: white">Hành động </label>
                                            <div>
                                                <button class="btn btn-brand m-btn m-btn--icon" type="submit">
												<span>
													<i class="la la-search"></i>
													<span>
														Tìm kiếm
													</span>
												</span>
                                                </button>
                                                &nbsp;&nbsp;
                                                <button class="btn btn-secondary m-btn m-btn--icon" type="button"
                                                        onclick="window.location='{{route('Business::subscription@index')}}'">
												<span>
													<i class="la la-refresh"></i>
													<span>
														Làm mới
													</span>
												</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <div class="row m--margin-bottom-20">
                                    <div class="col-lg-9 m--margin-bottom-10-tablet-and-mobile">
                                    </div>
                                    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile" style="border: solid">
                                        @if($approve_waiting_number)
                                            <a href="{{route('Business::subscription@index')}}?status=0"><h5
                                                        style="color: blue">Có {{$approve_waiting_number}} gói dịch vụ
                                                    đang chờ duyệt</h5>
                                            </a>
                                        @endif
                                        @if($alert_number)
                                            <a href="{{route('Business::subscription@index')}}?alert_status=1"><h5
                                                        style="color: red">Có {{$alert_number}} gói dịch vụ sắp hết
                                                    hạn</h5>
                                            </a>
                                        @endif
                                        @if($expired_number)
                                            <a href="{{route('Business::subscription@index')}}?alert_status=2"><h5
                                                        style="color: black">Có {{$expired_number}} gói dịch vụ đã hết
                                                    hạn</h5></a>
                                        @endif
                                    </div>
                                </div>
                                @if (session('success'))
                                    <div
                                            class="m-alert m-alert--outline m-alert--square m-alert--outline-2x alert alert-success alert-dismissible fade show"
                                            role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        </button>
                                        {{ session('success') }}
                                    </div>
                                @endif
                                <div style="margin-bottom: 0;margin-top: 50px;margin-left: 50px;">
                  <span
                          style="width:200px;text-align:center;padding:6px;font-weight: bold;font-style: italic;border: solid 1px;border-radius: 6px">
                    Đang hiển thị {{$count_record_in_page}}/{{$count_record_total}} bản ghi
                  </span>
                                    <span style="margin-left: 5px">
                    <a href="{{route('Business::subscription@excelExport',$query)}}"
                       class="btn btn-info m-btn btn-sm 	m-btn m-btn--icon m-btn--pill">
                      <span>
													<i class="la la-cloud-download"></i>
													<span>
														Xuất Excel
													</span>
                      </span>
                    </a>
                  </span>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-striped- table-bordered table-hover table-checkable"
                                           style="margin-top: 20px;margin-left:auto;margin-right:auto;">
                                        <!--begin::Thead-->
                                        <thead>
                                        <tr>
                                            <th style="font-weight: bold">Id</th>
                                            <th style="min-width: 300px;font-weight: bold">Doanh nghiệp</th>
                                            <th style="min-width:150px;font-weight: bold">Tên gói đăng ký</th>
                                            <th style="font-weight: bold;min-width: 80px">Số lượng sản phẩm</th>
                                            <th style="font-weight: bold;">Ảnh Barcode 1 Sản phẩm của Doanh nghiệp</th>
                                            <th style="min-width:100px;font-weight: bold">Đơn giá gói dịch vụ
                                                (VNĐ/năm)
                                            </th>
                                            <th style="font-weight: bold;text-align: center;min-width: 150px">Giá trị
                                                theo Hợp đồng (VNĐ)
                                            </th>
                                            <th style="font-weight: bold">Thời hạn gói</th>
                                            <th style="min-width: 130px;font-weight: bold">Ngày bắt đầu Hợp đồng</th>
                                            <th style="min-width: 130px;font-weight: bold">Ngày hết hạn Hợp đồng</th>
                                            <th style="min-width: 130px;font-weight: bold">Ngày hết hạn Demo</th>
                                            <th style="font-weight: bold;min-width: 100px">Ngày hết hạn có hiệu lực</th>
                                            <th style="font-weight: bold;min-width: 120px">Tình trạng thanh toán</th>
                                            <th style="font-weight: bold;">Files hợp đồng</th>
                                            <th style="font-weight: bold;min-width: 120px">Trạng thái</th>
                                            <th style="font-weight: bold;min-width: 180px">Người phụ trách</th>
                                            <th style="font-weight: bold;min-width: 150px">Hành động</th>
                                        </tr>
                                        </thead>
                                        <!--end::Thead-->
                                        <!--begin::Tbody-->
                                        <tbody>
                                        @foreach($subscriptions as $subscription)
                                            <tr
                                                    @if( $subscription->expires_on && date('Y-m-d H:i:s',strtotime($subscription->expires_on)) < date('Y-m-d H:i:s', strtotime("+ 3days"))) style="background-color: burlywood" @endif>
                                                <td class="id">{{$subscription->id}}</td>
                                                <td>
                                                    <div
                                                            class="m-dropdown m-dropdown--up m-dropdown--inline m-dropdown--small m-dropdown--arrow m-dropdown--align-left"
                                                            m-dropdown-toggle="hover">
                                                        <a class="m-dropdown__toggle btn btn-silver dropdown-toggle">
                                                            {{$subscription->business_name}}
                                                        </a>
                                                        <div class="m-dropdown__wrapper" style="width: 250px">
                                                            <div class="m-dropdown__inner">
                                                                <div class="m-dropdown__body" style="padding:10px;">
                                                                    <div class="m-dropdown__content">
                                                                        <ul class="m-nav">
                                                                            @can("GATEWAY-viewProduct")
                                                                                <li class="m-nav__item"
                                                                                    style="margin-bottom: 5px">
                                                                                    <a
                                                                                            href="{{route('Business::product@index')}}?business_id={{$subscription->business_id}}"
                                                                                            target="_blank"
                                                                                            class="m-nav__link">
                                                                                        <span class="m-nav__link-text">Xem danh sách sản phẩm đang quản lý</span>
                                                                                    </a>
                                                                                </li>
                                                                            @endcan
                                                                            @can("GATEWAY-viewBusiness")
                                                                                <li class="m-nav__item"
                                                                                    style="margin-bottom: 5px">
                                                                                    <a
                                                                                            href="{{route('Business::businesses@index')}}?business_id={{$subscription->business_id}}"
                                                                                            target="_blank"
                                                                                            class="m-nav__link">
                                                                                        <span class="m-nav__link-text">Xem thông tin Doanh nghiệp</span>
                                                                                    </a>
                                                                                </li>
                                                                            @endcan
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <span class="m-dropdown__arrow m-dropdown__arrow--left"></span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    @if(auth()->user()->can('GATEWAY-updateSubscription'))
                                                        @if($subscription->status == \App\Models\Business\Subscription::SUBSCRIPTION_STATUS_PENDING || $subscription->expires_on && date('Y-m-d H:i:s',strtotime($subscription->expires_on)) > date('Y-m-d H:i:s') || $subscription->expire_type===\App\Models\Business\Subscription::EXPIRE_TYPE_DEMO)
                                                            <a
                                                                    href="{{route('Business::subscription@edit',[$subscription->id])}}"
                                                                    title="Click để sửa thông tin gói Dịch vụ">{{$subscription->plan_name}}</a>
                                                        @else
                                                            {{$subscription->plan_name}}
                                                        @endif
                                                    @else
                                                        {{$subscription->plan_name}}
                                                    @endif
                                                </td>
                                                <td>{{number_format($subscription->quota,0,".",",")}}</td>
                                                <td>{!! $subscription->barcode_image !!}</td>
                                                <td>{{number_format($subscription->price,0,".",",")}}</td>
                                                <td class="edit_contract_value" style="cursor: pointer">
                          <span
                                  class="formatted_contract_value">{{number_format($subscription->contract_value,0,".",",")}}</span>
                                                    @can("GATEWAY-updateSubscription")
                                                        <input type="number" class="contract_value form-control m-input"
                                                               name="contractValue"
                                                               min="0"
                                                               value="{{$subscription->contract_value}}"
                                                               data-col-index="0">
                                                    @endcan
                                                </td>
                                                <td>
                                                    @if($subscription->days >=365)
                                                        {{number_format($subscription->days/365,0,".",",")}} năm
                                                    @else
                                                        {{number_format($subscription->days,0,".",",")}} ngày
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($subscription->start_time)
                                                        {{date("d/m/Y",strtotime($subscription->start_time))}}
                                                        {{--<input type="text" class="start_time form-control" name="start_time"--}}
                                                        {{--data-date-format="dd/mm/yyyy"--}}
                                                        {{--value="{{date("d/m/Y",strtotime($subscription->start_time))}}" autocomplete="off"--}}
                                                        {{--@cannot("GATEWAY-updateSubscription")--}}
                                                        {{--disabled--}}
                                                        {{--@endcannot--}}
                                                        {{--data-col-index="5"/>--}}
                                                        {{--@else--}}
                                                        {{--<input type="text" class="start_time form-control" name="start_time"--}}
                                                        {{--data-date-format="dd/mm/yyyy"--}}
                                                        {{--value="" autocomplete="off"--}}
                                                        {{--@cannot("GATEWAY-updateSubscription")--}}
                                                        {{--disabled--}}
                                                        {{--@endcannot--}}
                                                        {{--data-col-index="5"/>--}}
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($subscription->contract_expiration)
                                                        {{date("d/m/Y",strtotime($subscription->contract_expiration))}}
                                                        {{--@if($subscription->expires_on)--}}
                                                        {{--<input type="text" class="expires_on form-control" name="expires_on"--}}
                                                        {{--data-date-format="dd/mm/yyyy"--}}
                                                        {{--value="{{date("d/m/Y",strtotime($subscription->expires_on))}}" autocomplete="off"--}}
                                                        {{--@cannot("GATEWAY-updateSubscription")--}}
                                                        {{--disabled--}}
                                                        {{--@endcannot--}}
                                                        {{--data-col-index="5"/>--}}
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($subscription->trial_expiration)
                                                        {{date("d/m/Y",strtotime($subscription->trial_expiration))}}
                                                        {{--@if($subscription->expires_on)--}}
                                                        {{--<input type="text" class="expires_on form-control" name="expires_on"--}}
                                                        {{--data-date-format="dd/mm/yyyy"--}}
                                                        {{--value="{{date("d/m/Y",strtotime($subscription->expires_on))}}" autocomplete="off"--}}
                                                        {{--@cannot("GATEWAY-updateSubscription")--}}
                                                        {{--disabled--}}
                                                        {{--@endcannot--}}
                                                        {{--data-col-index="5"/>--}}
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($subscription->expires_on)
                                                        <span
                                                                style="font-weight: bold">{{date("d/m/Y",strtotime($subscription->expires_on))}}</span>
                                                        {{--@if($subscription->status==\App\Models\Business\Subscription::SUBSCRIPTION_STATUS_APPROVED)--}}
                                                        {{--@if($subscription->expire_type == \App\Models\Business\Subscription::EXPIRE_TYPE_IN_CONTRACT || $subscription->expire_type == null)--}}
                                                        {{--Theo Hợp đồng--}}
                                                        {{--@elseif($subscription->expire_type === \App\Models\Business\Subscription::EXPIRE_TYPE_DEMO)--}}
                                                        {{--Demo--}}
                                                        {{--@endif--}}
                                                        {{--<select class="expire_type form-control m-input"--}}
                                                        {{--@cannot("GATEWAY-updateSubscription")--}}
                                                        {{--disabled--}}
                                                        {{--@endcannot--}}
                                                        {{--name="expire_type">--}}
                                                        {{--<option value="{{\App\Models\Business\Subscription::EXPIRE_TYPE_IN_CONTRACT}}"--}}
                                                        {{--@if($subscription->expire_type == \App\Models\Business\Subscription::EXPIRE_TYPE_IN_CONTRACT || $subscription->expire_type == null) selected @endif>--}}
                                                        {{--Theo Hợp đồng--}}
                                                        {{--</option>--}}
                                                        {{--<option value="{{\App\Models\Business\Subscription::EXPIRE_TYPE_DEMO}}"--}}
                                                        {{--@if($subscription->expire_type === \App\Models\Business\Subscription::EXPIRE_TYPE_DEMO) selected @endif>--}}
                                                        {{--Demo--}}
                                                        {{--</option>--}}
                                                        {{--</select>--}}
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($subscription->payment_status==\App\Models\Business\Subscription::PAYMENT_STATUS_TRUE)
                                                        Đã thanh toán
                                                    @elseif($subscription->payment_status==\App\Models\Business\Subscription::PAYMENT_STATUS_FALSE)
                                                        Chưa thanh toán
                                                    @endif

                                                    {{--<select id="payment_status_{{$subscription->id}}"--}}
                                                    {{--@if(auth()->user()->cannot('GATEWAY-updatePaymentStatus'))--}}
                                                    {{--disabled--}}
                                                    {{--@endif--}}
                                                    {{--class="payment form-control m-input"--}}
                                                    {{--class="payment form-control m-input" name="payment_status"--}}
                                                    {{-->--}}

                                                    {{--<option value="{{\App\Models\Business\Subscription::PAYMENT_STATUS_UNKNOWN}}"--}}
                                                    {{--@if($subscription->payment_status == \App\Models\Business\Subscription::PAYMENT_STATUS_UNKNOWN || $subscription->payment_status == null) selected @endif>--}}
                                                    {{--Chưa rõ--}}
                                                    {{--</option>--}}
                                                    {{--<option value="{{\App\Models\Business\Subscription::PAYMENT_STATUS_FALSE}}"--}}
                                                    {{--@if($subscription->payment_status == \App\Models\Business\Subscription::PAYMENT_STATUS_FALSE) selected @endif>--}}
                                                    {{--Chưa thanh toán--}}
                                                    {{--</option>--}}
                                                    {{--<option value="{{\App\Models\Business\Subscription::PAYMENT_STATUS_PARTIAL}}"--}}
                                                    {{--@if($subscription->payment_status == \App\Models\Business\Subscription::PAYMENT_STATUS_PARTIAL) selected @endif>--}}
                                                    {{--Thanh toán một phần--}}
                                                    {{--</option>--}}
                                                    {{--<option value="{{\App\Models\Business\Subscription::PAYMENT_STATUS_TRUE}}"--}}
                                                    {{--@if($subscription->payment_status == \App\Models\Business\Subscription::PAYMENT_STATUS_TRUE) selected @endif>--}}
                                                    {{--Đã thanh toán--}}
                                                    {{--</option>--}}
                                                    {{--</select>--}}
                                                </td>
                                                <td>
                                                    <button
                                                            type="button"
                                                            @if(auth()->user()->cannot('GATEWAY-viewSubscriptionContract'))
                                                            disabled
                                                            @endif
                                                            value="{{$subscription->contract_files}}"
                                                            data-target="#file{{$subscription->id}}"
                                                            class="contract_modal btn m-btn--pill m-btn--air btn-outline-info btn-sm"
                                                            id="upload_files{{$subscription->id}}"
                                                            data-toggle="modal">Chi tiết
                                                    </button>
                                                    <!--Modal-->
                                                    <div id="file{{$subscription->id}}" class="modal fade"
                                                         role="dialog">
                                                        <div class="modal-dialog">
                                                            <!-- Modal content-->
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h1 class="modal-title">Files hợp đồng của
                                                                        "{{$subscription->business_name}}"</h1>
                                                                    <button type="button" class="close"
                                                                            data-dismiss="modal">&times;
                                                                    </button>
                                                                </div>
                                                                <form method="POST"
                                                                      action="{{ route('Business::subscription@updateContract')}}">
                                                                    <div class="modal-body">
                                                                        <input type="hidden" name="subscription_id"
                                                                               value="{{$subscription->id}}">
                                                                        <div>
                                                                            <ol class="heroes upload_files{{$subscription->id}}_input_list">

                                                                            </ol>
                                                                        </div>
                                                                        @if(auth()->user()->can('GATEWAY-updateSubscriptionContract'))
                                                                            <div style="text-align: center">
                                                                                <label for="upload_files{{$subscription->id}}_input"
                                                                                       style="cursor: pointer;padding:8px;background-color: #0aa7ef;color: white;border-radius: 20px"><i
                                                                                            class="la la-cloud-upload"></i>
                                                                                    Chọn
                                                                                    tập tin</label>
                                                                                <input type="file" name="files[]"
                                                                                       id="upload_files{{$subscription->id}}_input"
                                                                                       class="upload"
                                                                                       style="display: none!important;"
                                                                                       accept=".png,.jpg,.jpeg,.gif,.pdf,.doc,.docx"
                                                                                       multiple>
                                                                            </div>
                                                                        @endif
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        @if(auth()->user()->can('GATEWAY-updateSubscriptionContract'))
                                                                            <button type="submit"
                                                                                    class="btn btn-success">
                                                                                Cập nhật
                                                                            </button>
                                                                        @endif
                                                                        <button type="button"
                                                                                class="btn btn-default"
                                                                                data-dismiss="modal"> Đóng
                                                                        </button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--End Modal-->
                                                </td>
                                                <td>
                                                    @if($subscription->status==0)
                                                        <span class="m-badge m-badge--warning m-badge--wide"><a
                                                                    data-toggle="modal"
                                                                    data-target="#note{{$subscription->id}}">Chờ kích hoạt </a></span>
                                                    @elseif($subscription->status==1)
                                                        <span class="m-badge m-badge--success m-badge--wide"><a
                                                                    data-toggle="modal"
                                                                    data-target="#note{{$subscription->id}}">Kích hoạt </a></span>
                                                    @else
                                                        <span class="m-badge m-badge--danger m-badge--wide"><a
                                                                    data-toggle="modal"
                                                                    data-target="#note{{$subscription->id}}">Hủy kích hoạt</a></span>
                                                    @endif
                                                    @if($subscription->is_legacy_subscription)
                                                        <p style="color: #0a6ebd;padding-bottom: 0; margin: 2px 0 0 0; text-align: center">{{'Gói đồng bộ'}}</p>
                                                @endif
                                                <!--Modal update Note-->
                                                    <div id="note{{$subscription->id}}" class="modal fade"
                                                         role="dialog">
                                                        <div class="modal-dialog">
                                                            <!-- Modal content-->
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h3>GHI CHÚ</h3>
                                                                    <button type="button" class="close"
                                                                            data-dismiss="modal">&times;
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <div class="form-group m-form__group row">
                                      <textarea class="form-control" rows="5"
                                                onchange="updateNote(this.value,{{$subscription->id}})"
                                                placeholder="Ghi chú về gói dịch vụ này..."
                                                name="note">{{$subscription->note}}</textarea>
                                                                    </div>
                                                                </div>

                                                                <div class="modal-footer">
                                                                    <button type="button"
                                                                            class="btn btn-default"
                                                                            data-dismiss="modal">Đóng
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--End Modal-->
                                                </td>
                                                <td>
                                                    <select class="supporter form-control m-input"
                                                            @cannot("GATEWAY-updateSubscription")
                                                            disabled
                                                            @endcannot
                                                            name="assignee_icheck_id">
                                                        <option>Chưa rõ</option>
                                                        @foreach($icheckUsers as $user)
                                                            <option value="{{$user->icheck_id}}"
                                                                    @if($subscription->assigned_to == $user->icheck_id) selected @endif
                                                            >{{$user->name}} ({{$user->icheck_id}})
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </td>
                                                <td>
                                                    @can("GATEWAY-updatePaymentStatus")
                                                        @if($subscription->days !=7 && $subscription->payment_status==\App\Models\Business\Subscription::PAYMENT_STATUS_FALSE && $subscription->status > \App\Models\Business\Subscription::SUBSCRIPTION_STATUS_PENDING)
                                                            @if( $subscription->expires_on && $subscription->contract_expiration && $subscription->expires_on <= $subscription->contract_expiration && $subscription->contract_expiration >= date("Y-m-d h:i:s")|| $subscription->expires_on && date('Y-m-d H:i:s',strtotime($subscription->expires_on)) > date('Y-m-d H:i:s'))
                                                                <button
                                                                        type="button"
                                                                        onclick="window.location='{{ route('Business::subscription@confirmPaymentStatus',$subscription->id)}}'"
                                                                        title="Xác nhận đã thanh toán"
                                                                        class="btn btn-info m-btn m-btn--icon m-btn--icon-only m-btn--pill">
                                                                    <i class="la la-dollar"></i>
                                                                </button>
                                                                {{--@elseif($subscription->expires_on && date('Y-m-d H:i:s',strtotime($subscription->expires_on)) < date('Y-m-d H:i:s'))--}}
                                                                {{--<button--}}
                                                                {{--type="button"--}}
                                                                {{--data-toggle="modal"--}}
                                                                {{--data-target="#confirmPayment{{$subscription->id}}"--}}
                                                                {{--title="Xác nhận đã thanh toán"--}}
                                                                {{--class="btn btn-info m-btn m-btn--icon m-btn--icon-only m-btn--pill">--}}
                                                                {{--<i class="la la-dollar"></i>--}}
                                                                {{--</button>--}}
                                                                {{--<!--Modal Comfirm approve-->--}}
                                                                {{--<div id="confirmPayment{{$subscription->id}}"--}}
                                                                {{--class="modal fade"--}}
                                                                {{--role="dialog">--}}
                                                                {{--<div class="modal-dialog">--}}
                                                                {{--<!-- Modal content-->--}}
                                                                {{--<div class="modal-content">--}}
                                                                {{--<div class="modal-header">--}}
                                                                {{--<h3>Gói dịch vụ này đã hết hạn, hãy đặt--}}
                                                                {{--lại ngày hết hạn Hợp đồng gói dịch--}}
                                                                {{--vụ--}}
                                                                {{--này ?</h3>--}}
                                                                {{--<button type="button" class="close"--}}
                                                                {{--data-dismiss="modal">&times;--}}
                                                                {{--</button>--}}
                                                                {{--</div>--}}
                                                                {{--<form method="post"--}}
                                                                {{--action="{{route('Business::subscription@confirmPaymentStatusAfterExpired',[$subscription->id])}}">--}}
                                                                {{--{{ csrf_field() }}--}}
                                                                {{--<div class="modal-body">--}}
                                                                {{--<div class="form-group m-form__group row">--}}
                                                                {{--<label class="col-form-label col-lg-5 col-sm-12">Ngày--}}
                                                                {{--hết hạn Hợp đồng</label>--}}
                                                                {{--<div class="col-lg-5 col-md-9 col-sm-12">--}}
                                                                {{--<input type="text"--}}
                                                                {{--class="expires_on form-control"--}}
                                                                {{--name="contract_expiration"--}}
                                                                {{--autocomplete="off"--}}
                                                                {{--data-date-format="dd/mm/yyyy"--}}
                                                                {{--data-col-index="5"--}}
                                                                {{--required/>--}}
                                                                {{--</div>--}}
                                                                {{--<p style="margin-top: 15px; color: red">--}}
                                                                {{--Chú ý: Ngày hết hạn hợp đồng--}}
                                                                {{--phải lớn hơn ngày hiện tại!--}}
                                                                {{--</p>--}}
                                                                {{--</div>--}}

                                                                {{--</div>--}}
                                                                {{--<div class="modal-footer">--}}
                                                                {{--<button type="submit" disabled--}}
                                                                {{--class="confirmPaymentStatusAfterExpired btn btn-success">--}}
                                                                {{--Đồng ý--}}
                                                                {{--</button>--}}
                                                                {{--<button type="button"--}}
                                                                {{--class="btn btn-default"--}}
                                                                {{--data-dismiss="modal">Đóng--}}
                                                                {{--</button>--}}
                                                                {{--</div>--}}
                                                                {{--</form>--}}
                                                                {{--</div>--}}
                                                                {{--</div>--}}
                                                                {{--</div>--}}
                                                                {{--<!--End Modal-->--}}
                                                            @endif
                                                        @endif
                                                        @if($subscription->days !=7 && $subscription->payment_status==\App\Models\Business\Subscription::PAYMENT_STATUS_TRUE && $subscription->payment_status!=\App\Models\Business\Subscription::SUBSCRIPTION_STATUS_PENDING)
                                                            <button
                                                                    type="button"
                                                                    {{--@if(auth()->user()->cannot('GATEWAY-approveSubscription'))--}}
                                                                    {{--disabled--}}
                                                                    {{--@endif--}}
                                                                    onclick="window.location='{{ route('Business::subscription@notConfirmPaymentStatus',$subscription->id)}}'"
                                                                    title="Xác nhận chưa thanh toán"
                                                                    class="btn btn-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill">
                                                                <i class="la la-dollar"></i>
                                                            </button>
                                                        @endif
                                                    @endcan
                                                    @if($subscription->status==\App\Models\Business\Subscription::SUBSCRIPTION_STATUS_PENDING || $subscription->expires_on && date('Y-m-d H:i:s',strtotime($subscription->expires_on)) > date('Y-m-d H:i:s') && $subscription->status==\App\Models\Business\Subscription::SUBSCRIPTION_STATUS_DISAPPROVED || ($subscription->status==\App\Models\Business\Subscription::SUBSCRIPTION_STATUS_DISAPPROVED) && $subscription->expires_on && $subscription->contract_expiration && $subscription->contract_expiration >= date("Y-m-d h:i:s"))
                                                        @can("GATEWAY-approveSubscription")
                                                            <button
                                                                    type="button"
                                                                    @if(auth()->user()->cannot('GATEWAY-approveSubscription'))
                                                                    disabled
                                                                    @endif
                                                                    data-toggle="modal"
                                                                    data-target="#approve{{$subscription->id}}"
                                                                    title="Kích hoạt"
                                                                    class="btn btn-success m-btn m-btn--icon m-btn--icon-only m-btn--pill">
                                                                <i class="la la-check"></i>
                                                            </button>
                                                        @endcan
                                                    <!--Modal Comfirm approve-->
                                                        <div id="approve{{$subscription->id}}" class="modal fade"
                                                             role="dialog">
                                                            <div class="modal-dialog">
                                                                <!-- Modal content-->
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h3>Bạn chắc
                                                                            chắn muốn kích hoạt Gói dịch vụ cho
                                                                            "{{$subscription->business_name}}"?</h3>
                                                                        <button type="button" class="close"
                                                                                data-dismiss="modal">&times;
                                                                        </button>
                                                                    </div>
                                                                    <form method="post"
                                                                          action="{{route('Business::subscription@approve',[$subscription->id])}}">
                                                                        {{ csrf_field() }}
                                                                        <div class="modal-body">
                                                                            <div class="form-group m-form__group row">
                                                                                <label class="col-form-label col-lg-5 col-sm-12">Ngày
                                                                                    bắt đầu Hợp đồng </label>
                                                                                <div class="col-lg-4 col-md-9 col-sm-12">
                                                                                    <input type="text"
                                                                                           class="start_time form-control"
                                                                                           name="approve_start_time"
                                                                                           autocomplete="off"
                                                                                           data-date-format="dd/mm/yyyy"
                                                                                           data-col-index="5"/>
                                                                                </div>
                                                                            </div>
                                                                            @if($subscription->days != 7)
                                                                                <div class="form-group m-form__group row">
                                                                                    <label class="col-form-label col-lg-5 col-sm-12">Ngày
                                                                                        hết hạn demo *</label>
                                                                                    <div class="col-lg-4 col-md-9 col-sm-12">
                                                                                        <input type="text"
                                                                                               class="trial_expiration form-control"
                                                                                               name="approve_trial_expiration"
                                                                                               autocomplete="off"
                                                                                               data-date-format="dd/mm/yyyy"
                                                                                               required
                                                                                               data-col-index="5"/>
                                                                                    </div>
                                                                                </div>
                                                                            @endif
                                                                            <div class="form-group m-form__group row">
                                                                                <label class="col-form-label col-lg-5 col-sm-12">Ngày
                                                                                    hết hạn Hợp đồng </label>
                                                                                <div class="col-lg-4 col-md-9 col-sm-12">
                                                                                    <input type="text"
                                                                                           class="contract_expiration form-control"
                                                                                           name="approve_contract_expiration"
                                                                                           autocomplete="off"
                                                                                           data-date-format="dd/mm/yyyy"
                                                                                           data-col-index="5"/>

                                                                                </div>
                                                                                {{--<div class="col-lg-3 col-md-9 col-sm-12">--}}
                                                                                {{--<span style="font-style: italic; color: red">(Bắt buộc)</span>--}}
                                                                                {{--</div>--}}
                                                                            </div>
                                                                            {{--<div class="form-group m-form__group row">--}}
                                                                            {{--<label class="col-form-label col-lg-5 col-sm-12">Loại ngày hết hạn </label>--}}
                                                                            {{--<div class="col-lg-5 col-md-9 col-sm-12">--}}
                                                                            {{--<select class="form-control m-input" data-col-index="7" name="expire_type"--}}
                                                                            {{--required>--}}
                                                                            {{--<option--}}
                                                                            {{--value="{{\App\Models\Business\Subscription::EXPIRE_TYPE_IN_CONTRACT}}">--}}
                                                                            {{--Theo hợp đồng--}}
                                                                            {{--</option>--}}
                                                                            {{--<option value="{{\App\Models\Business\Subscription::EXPIRE_TYPE_DEMO}}">--}}
                                                                            {{--Demo--}}
                                                                            {{--</option>--}}
                                                                            {{--</select>--}}
                                                                            {{--</div>--}}
                                                                            {{--</div>--}}
                                                                            <div class="form-group m-form__group row">
                                                                                <p>
                                                                                    Nếu không chọn ngày bắt đầu Hợp đồng
                                                                                    thì
                                                                                    ngày bắt đầu Hợp đồng sẽ được tự
                                                                                    động
                                                                                    tính là
                                                                                    ngày hôm nay {{date('d/m/Y')}}.
                                                                                </p>
                                                                                <p>
                                                                                    Nếu không chọn ngày hết hạn Hợp đồng
                                                                                    thì
                                                                                    ngày hết hạn sẽ được tự
                                                                                    động
                                                                                    tính là
                                                                                    ngày {{date('d/m/Y', strtotime("+$subscription->days days"))}}
                                                                                    (được tính cách lấy ngày hôm nay
                                                                                    cộng
                                                                                    với thời hạn gói dịch vụ).
                                                                                </p>
                                                                            </div>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="submit"
                                                                                    class="btn btn-success">
                                                                                Đồng ý
                                                                            </button>
                                                                            <button type="button"
                                                                                    class="btn btn-default"
                                                                                    data-dismiss="modal">Đóng
                                                                            </button>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--End Modal-->
                                                    @endif
                                                    @if($subscription->status==0 || $subscription->status==1)
                                                        @can("GATEWAY-disapproveSubscription")
                                                            <button
                                                                    type="button"
                                                                    @if(auth()->user()->cannot('GATEWAY-disapproveSubscription'))
                                                                    disabled
                                                                    @endif
                                                                    data-toggle="modal"
                                                                    data-target="#disapprove{{$subscription->id}}"
                                                                    title="Hủy kích hoạt"
                                                                    class="btn btn-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill">
                                                                <i class="la la-remove"></i>
                                                            </button>
                                                        @endcan
                                                    <!--Modal Comfirm Disapprove-->
                                                        <div id="disapprove{{$subscription->id}}" class="modal fade"
                                                             role="dialog">
                                                            <div class="modal-dialog">
                                                                <!-- Modal content-->
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h3>Bạn chắc
                                                                            chắn muốn hủy kích hoạt Gói dịch vụ cho
                                                                            "{{$subscription->business_name}}"?</h3>
                                                                        <button type="button" class="close"
                                                                                data-dismiss="modal">&times;
                                                                        </button>
                                                                    </div>
                                                                    <form method="post"
                                                                          action="{{route('Business::subscription@disapprove',[$subscription->id])}}">
                                                                        {{ csrf_field() }}
                                                                        <div class="modal-body">
                                                                            <div class="form-group m-form__group row">
                                                                                <textarea class="form-control" rows="5"
                                                                                          placeholder="Nhập nguyên nhân hủy kích hoạt Gói dịch vụ"
                                                                                          name="note"
                                                                                          required></textarea>
                                                                            </div>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="submit"
                                                                                    class="submit_button btn btn-success">
                                                                                Đồng ý
                                                                            </button>
                                                                            <button type="button"
                                                                                    class="btn btn-default"
                                                                                    data-dismiss="modal">Đóng
                                                                            </button>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--End Modal-->
                                                    @endif
                                                    @if($subscription->expires_on && date('Y-m-d H:i:s',strtotime($subscription->expires_on)) < date('Y-m-d H:i:s') && $subscription->status==\App\Models\Business\Subscription::SUBSCRIPTION_STATUS_DISAPPROVED )
                                                        @can("GATEWAY-addSubscription")
                                                            <button
                                                                    type="button"
                                                                    @if(auth()->user()->cannot('GATEWAY-disapproveSubscription'))
                                                                    disabled
                                                                    @endif
                                                                    data-toggle="modal"
                                                                    data-target="#createNew{{$subscription->id}}"
                                                                    title="Tạo gói mới"
                                                                    class="btn btn-info m-btn m-btn--icon m-btn--icon-only m-btn--pill">
                                                                <i class="la la-plus"></i>
                                                            </button>
                                                            <!--Modal Create New Subcription-->
                                                            <div id="createNew{{$subscription->id}}" class="modal fade"
                                                                 role="dialog">
                                                                <div class="modal-dialog modal-lg">
                                                                    <!-- Modal content-->
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h3>Tạo mới Gói dịch vụ cho
                                                                                "{{$subscription->business_name}}"</h3>
                                                                            <button type="button" class="close"
                                                                                    data-dismiss="modal">&times;
                                                                            </button>
                                                                        </div>
                                                                        <form method="post"
                                                                              action="{{route('Business::subscription@store')}}">
                                                                            {{ csrf_field() }}
                                                                            <div class="modal-body">
                                                                                <div class="form-group m-form__group row">
                                                                                    <label class="col-form-label col-lg-3 col-sm-12">Chọn
                                                                                        doanh nghiệp *</label>
                                                                                    <div class="col-lg-6 col-md-9 col-sm-12">
                                                                                        <select class="business_id form-control m-input"
                                                                                                data-col-index="7"
                                                                                                name="business_id"
                                                                                                required>
                                                                                            <option value=""></option>
                                                                                            @foreach($businesses as $business)
                                                                                                <option value="{{$business->id}}"
                                                                                                        @if (isset($subscription) && $subscription->business_id==$business->id)
                                                                                                        selected
                                                                                                        @endif
                                                                                                >
                                                                                                    {{$business->name}}
                                                                                                </option>
                                                                                            @endforeach
                                                                                        </select>
                                                                                    </div>
                                                                                    @if ($errors->has('business_id'))
                                                                                        <div class="help-block">{{ $errors->first('business_id') }}</div>
                                                                                    @endif
                                                                                </div>
                                                                                <!------------------ Plan--------------->
                                                                                <div class="form-group m-form__group row">
                                                                                    <label class="col-form-label col-lg-3 col-sm-12">Chọn
                                                                                        gói dịch vụ *</label>
                                                                                    <div class="col-lg-6 col-md-9 col-sm-12">
                                                                                        <select class="form-control m-input"
                                                                                                data-col-index="7"
                                                                                                name="plan_id"
                                                                                                @if (isset($subscription) && $subscription->status == \App\Models\Business\Subscription::SUBSCRIPTION_STATUS_APPROVED) disabled
                                                                                                @else
                                                                                                required
                                                                                                @endif>
                                                                                            <option value=""></option>
                                                                                            @foreach($plans as $plan)
                                                                                                <option value="{{$plan->id}}"
                                                                                                        @if (isset($subscription) && $subscription->plan_id==$plan->id)
                                                                                                        selected
                                                                                                        @endif
                                                                                                >
                                                                                                    {{$plan->name}}
                                                                                                    ({{$plan->quota}}sản
                                                                                                    phẩm)
                                                                                                </option>
                                                                                            @endforeach
                                                                                        </select>
                                                                                    </div>
                                                                                    @if ($errors->has('plan_id'))
                                                                                        <div class="help-block">{{ $errors->first('plan_id') }}</div>
                                                                                    @endif
                                                                                </div>
                                                                                <!------------------Contract Period--------------->
                                                                                <div class="form-group m-form__group row">
                                                                                    <label class="col-form-label col-lg-3 col-sm-12">Thời
                                                                                        hạn Hợp đồng *</label>
                                                                                    <div class="col-lg-6 col-md-9 col-sm-12">
                                                                                        <select class="form-control m-input"
                                                                                                data-col-index="7"
                                                                                                name="days"
                                                                                                required>
                                                                                            <option value=""></option>
                                                                                            <option value="7">
                                                                                                7 ngày
                                                                                            </option>
                                                                                            {{--<option value="30">--}}
                                                                                            {{--30 ngày--}}
                                                                                            {{--</option>--}}
                                                                                            <option value="365">
                                                                                                1 năm
                                                                                            </option>
                                                                                            <option value="730">
                                                                                                2 năm
                                                                                            </option>
                                                                                            <option value="1095">
                                                                                                3
                                                                                                năm
                                                                                            </option>
                                                                                            <option value="1460">
                                                                                                4
                                                                                                năm
                                                                                            </option>
                                                                                            <option value="1825">
                                                                                                5
                                                                                                năm
                                                                                            </option>
                                                                                        </select>
                                                                                    </div>
                                                                                    @if ($errors->has('days'))
                                                                                        <div class="help-block">{{ $errors->first('days') }}</div>
                                                                                    @endif
                                                                                </div>
                                                                                <!------------------Giá trị Hợp đồng--------------->
                                                                                <div class="form-group m-form__group row">
                                                                                    <label class="col-form-label col-lg-3 col-sm-12">Giá
                                                                                        trị theo Hợp
                                                                                        đồng </label>
                                                                                    <div class="col-lg-6 col-md-9 col-sm-12">
                                                                                        <input type="number"
                                                                                               class="form-control m-input"
                                                                                               name="contract_value"
                                                                                               min="0"
                                                                                               value="{{ old('contract_value')}}"
                                                                                               data-col-index="0">
                                                                                    </div>
                                                                                </div>
                                                                                <!------------------Ngày bắt đầu Hợp đồng--------------->
                                                                                <div class="form-group m-form__group row">
                                                                                    <label class="col-form-label col-lg-3 col-sm-12">Ngày
                                                                                        bắt đầu hợp
                                                                                        đồng </label>
                                                                                    <div class="col-lg-6 col-md-9 col-sm-12">
                                                                                        <input type="text"
                                                                                               class="start_time form-control"
                                                                                               name="start_time"
                                                                                               data-date-format="dd/mm/yyyy"
                                                                                               value="{{ old('start_time')}}"
                                                                                               autocomplete="off"
                                                                                               data-col-index="5"/>

                                                                                    </div>
                                                                                    @if ($errors->has('start_time'))
                                                                                        <div class="help-block">{{ $errors->first('start_time') }}</div>
                                                                                    @endif
                                                                                </div>
                                                                                <!------------------Ngày hết hạn hợp đồng--------------->
                                                                                <div class="form-group m-form__group row">
                                                                                    <label class="col-form-label col-lg-3 col-sm-12">Ngày
                                                                                        hết hạn hợp đồng
                                                                                        *</label>
                                                                                    <div class="col-lg-6 col-md-9 col-sm-12">
                                                                                        <input type="text"
                                                                                               class="contract_expiration form-control"
                                                                                               name="contract_expiration"
                                                                                               data-date-format="dd/mm/yyyy"
                                                                                               value="{{ old('contract_expiration')}}"
                                                                                               autocomplete="off"
                                                                                               required
                                                                                               data-col-index="5"/>
                                                                                    </div>
                                                                                    @if ($errors->has('contract_expiration'))
                                                                                        <div class="help-block">{{ $errors->first('contract_expiration') }}</div>
                                                                                    @endif
                                                                                </div>
                                                                                <!------------------Demo Period--------------->
                                                                                <div class="demo-period form-group m-form__group row">
                                                                                    <label class="col-form-label col-lg-3 col-sm-12">Thời
                                                                                        hạn Demo *</label>
                                                                                    <div class="col-lg-6 col-md-9 col-sm-12">
                                                                                        <select class="form-control m-input"
                                                                                                data-col-index="7"
                                                                                                name="demo_days"
                                                                                                required
                                                                                        >
                                                                                            <option value=""></option>
                                                                                            <option value="7">7 ngày
                                                                                            </option>
                                                                                            <option value="30">30 ngày
                                                                                            </option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <!------------------Ngày hết demo--------------->
                                                                                <div class="demo-expired form-group m-form__group row">
                                                                                    <label class="col-form-label col-lg-3 col-sm-12">Ngày
                                                                                        hết hạn demo *</label>
                                                                                    <div class="col-lg-6 col-md-9 col-sm-12">
                                                                                        <input type="text"
                                                                                               class="trial_expiration form-control"
                                                                                               name="trial_expiration"
                                                                                               data-date-format="dd/mm/yyyy"
                                                                                               value="{{ old('trial_expiration')}}"
                                                                                               autocomplete="off"
                                                                                               required
                                                                                               data-col-index="5"/>
                                                                                    </div>
                                                                                    @if ($errors->has('trial_expiration'))
                                                                                        <div class="help-block">{{ $errors->first('trial_expiration') }}</div>
                                                                                    @endif
                                                                                </div>

                                                                                <!------------------Nhân viên chăm sóc khách hàng--------------->
                                                                                <div class="form-group m-form__group row">
                                                                                    <label class="col-form-label col-lg-3 col-sm-12">Người
                                                                                        phụ trách </label>
                                                                                    <div class="col-lg-6 col-md-9 col-sm-12">
                                                                                        <select class="supporter form-control m-input"
                                                                                                name="assigned_to">
                                                                                            <option>Chưa rõ</option>
                                                                                            @foreach($icheckUsers as $user)
                                                                                                <option value="{{$user->icheck_id}}"
                                                                                                        @if(isset($subscription) && $subscription->assigned_to == $user->icheck_id) selected @endif
                                                                                                >{{$user->name}}
                                                                                                    ({{$user->icheck_id}}
                                                                                                    )
                                                                                                </option>
                                                                                            @endforeach
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="submit"
                                                                                        class="btn btn-success">
                                                                                    Đồng ý
                                                                                </button>
                                                                                <button type="button"
                                                                                        class="btn btn-default"
                                                                                        data-dismiss="modal">Đóng
                                                                                </button>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--End Modal-->
                                                        @endcan
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                        <!--end::Tbody-->
                                    </table>
                                </div>
                                <div class="modal fade" id="enlargeImageModal" tabindex="-1" role="dialog"
                                     aria-labelledby="enlargeImageModal" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">×</span></button>
                                            </div>
                                            <div class="modal-body">
                                                <img src="" class="enlargeImageModalSource" style="width: 100%;">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--end::Table-->
                                <div style="float:right;"><?php echo $subscriptions->links(); ?></div>
                            </div>
                            <!--End::Section-->
                        </div>
                    </div>
                @endcan
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript" src="{{ asset('qr_code/metronic/js/vendors.bundle.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("input[name='contractValue']").hide();
            // Basic
            $(".payment").select2({});
            $(".supporter").select2({});
            $(".expire_type").select2({});
            $(".status").select2({
                placeholder: 'Chọn trạng thái',
                allowClear: true
            });
            $(".days").select2({
                placeholder: 'Thời hạn gói',
                allowClear: true
            });
            $(".business_id").select2({
                placeholder: 'Chọn doanh nghiệp',
                allowClear: true
            });
            $(".plan_id").select2({
                placeholder: 'Chọn gói dịch vụ',
                allowClear: true
            });
            $(".payment_status").select2({
                placeholder: 'Tình trạng thanh toán',
                allowClear: true
            });
            $("select").on("select2:select", function (evt) {
                var element = evt.params.data.element;
                var $element = $(element);
                $element.detach();
                $(this).append($element);
                $(this).trigger("change");
            });

//            $(".start_time").datepicker({
//                dateFormat: 'dd/mm/yy',
//                changeMonth: true,
//                changeYear: true,
//                todayHighlight: !0,
//                orientation: "bottom left",
//                templates: {
//                    leftArrow: '<i class="la la-angle-left"></i>',
//                    rightArrow: '<i class="la la-angle-right"></i>'
//                }
//            });

//            $(".expires_on").datepicker({
//                dateFormat: 'dd/mm/yy',
//                minDate: 1,
//                changeMonth: true,
//                changeYear: true,
//                todayHighlight: !0,
//                orientation: "bottom right",
//                templates: {
//                    leftArrow: '<i class="la la-angle-left"></i>',
//                    rightArrow: '<i class="la la-angle-right"></i>'
//                }
//            });
//
//            $(".expires_demo").datepicker({
//                dateFormat: 'dd/mm/yy',
//                minDate: 1,
//                changeMonth: true,
//                changeYear: true,
//                todayHighlight: !0,
//                orientation: "bottom right",
//                templates: {
//                    leftArrow: '<i class="la la-angle-left"></i>',
//                    rightArrow: '<i class="la la-angle-right"></i>'
//                }
//            });


            $(".modal").on('hidden.bs.modal', function () {
                document.body.style.padding = 0;
            });

            {{--$("select[name='payment_status']").change(function () {--}}
            {{--var payment_status = parseInt(this.value);--}}
            {{--var subscription_id = this.id.split("_").pop();--}}
            {{--var url = '';--}}
            {{--if (payment_status === parseInt("{{\App\Models\Business\Subscription::PAYMENT_STATUS_FALSE}}")) {--}}
            {{--url = "{{ route('Business::subscription@notConfirmPaymentStatus',":id")}}";--}}
            {{--url = url.replace(':id', subscription_id);--}}
            {{--} else if (payment_status === parseInt("{{\App\Models\Business\Subscription::PAYMENT_STATUS_TRUE}}")) {--}}
            {{--url = "{{ route('Business::subscription@confirmPaymentStatus',":id")}}";--}}
            {{--url = url.replace(':id', subscription_id);--}}
            {{--}--}}

            {{--window.location.href = url;--}}

            {{--// $.ajax({--}}
            {{--//   url: url.replace(':id', subscription_id),--}}
            {{--//   method: 'POST',--}}
            {{--//   data: {--}}
            {{--//     payment_status: payment_status,--}}
            {{--//   },--}}
            {{--//   success: function (data) {--}}
            {{--//     location.reload();--}}
            {{--//   },--}}
            {{--//   error: function () {--}}
            {{--//     console.log('fail');--}}
            {{--//   }--}}
            {{--// });--}}
            {{--});--}}
            //Update Start Time

            {{--$("input[name='start_time']").change(function () {--}}
            {{--var $this = $(this);--}}
            {{--var start_time = $(this).val();--}}
            {{--var subscription_id = $this.closest("tr").find(".id").text();--}}
            {{--var url = "{{ route('Business::subscription@updateStartTime')}}";--}}
            {{--$.ajax({--}}
            {{--url: url,--}}
            {{--method: 'POST',--}}
            {{--data: {--}}
            {{--subscription_id: subscription_id,--}}
            {{--start_time: start_time--}}
            {{--},--}}
            {{--success: function (data) {--}}
            {{--console.log(data);--}}
            {{--},--}}
            {{--error: function (error) {--}}
            {{--console.log(error);--}}
            {{--}--}}
            {{--});--}}
            {{--});--}}
            //            $("input[name='contract_expiration']").change(function () {
            //                var q = new Date();
            //                var m = q.getMonth();
            //                var d = q.getDate();
            //                var y = q.getFullYear();
            //                var today = new Date(y, m, d);
            //                var contract_expiration = $(this).val();
            //                var parts = contract_expiration.split("/");
            //                var formatted_expires_on = new Date(parts[2], parts[1] - 1, parts[0]);
            //                if (formatted_expires_on <= today) {
            //                    alert("Không thể áp dụng ngày hết hạn này. Ngày hết hạn phải lớn hơn ngày hiện tại!");
            //                    $('.confirmPaymentStatusAfterExpired').attr("disabled", true);
            //                    return false;
            //                } else {
            //                    $('.confirmPaymentStatusAfterExpired').removeAttr("disabled");
            //                }
            //            });

            //Update Expire on
            {{--$("input[name='expires_on']").change(function () {--}}
            {{--var $this = $(this);--}}
            {{--var expires_on = $(this).val();--}}
            {{--var parts = expires_on.split("/");--}}
            {{--var formatted_expires_on = new Date(parts[2], parts[1] - 1, parts[0]);--}}

            {{--var q = new Date();--}}
            {{--var m = q.getMonth();--}}
            {{--var d = q.getDate();--}}
            {{--var y = q.getFullYear();--}}

            {{--var today = new Date(y, m, d);--}}
            {{--if (expires_on === "") {--}}
            {{--return false;--}}
            {{--}--}}
            {{--// if (formatted_expires_on <= today) {--}}
            {{--//   alert("Không thể áp dụng ngày hết hạn này. Ngày hết hạn phải lớn hơn ngày hiện tại!");--}}
            {{--//   return ;--}}
            {{--// }--}}

            {{--var subscription_id = $this.closest("tr").find(".id").text();--}}
            {{--var url = "{{ route('Business::subscription@updateExpiresOn')}}";--}}
            {{--$.ajax({--}}
            {{--url: url,--}}
            {{--method: 'POST',--}}
            {{--data: {--}}
            {{--subscription_id: subscription_id,--}}
            {{--expires_on: expires_on--}}
            {{--},--}}
            {{--success: function (data) {--}}
            {{--// console.log(data);--}}
            {{--},--}}
            {{--error: function (error) {--}}
            {{--console.log(error);--}}
            {{--}--}}
            {{--});--}}
            {{--});--}}

            //Update Expire Type
            {{--$("select[name='expire_type']").change(function () {--}}
            {{--var $this = $(this);--}}
            {{--var expire_type = $(this).val();--}}
            {{--var subscription_id = $this.closest("tr").find(".id").text();--}}
            {{--var url = "{{ route('Business::subscription@updateExpireType')}}";--}}
            {{--$.ajax({--}}
            {{--url: url,--}}
            {{--method: 'POST',--}}
            {{--data: {--}}
            {{--subscription_id: subscription_id,--}}
            {{--expire_type: expire_type--}}
            {{--},--}}
            {{--success: function (data) {--}}
            {{--console.log(data);--}}
            {{--},--}}
            {{--error: function (error) {--}}
            {{--console.log(error);--}}
            {{--}--}}
            {{--});--}}
            {{--});--}}

        });

        $(".submit_button").click(function () {
            $(this).attr("disabled", "disabled");
            $(this).closest("form").submit();
        });

        //Show images
        $(function () {
            $('img').on('click', function () {
                $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
                $('#enlargeImageModal').modal('show');
            });
        });
        //Fill Data to Modal
        $(".contract_modal").click(function () {
            let id = $(this).attr('id');
            $(`.${id}_input_list`).empty();
            let value = $(this).attr('value');
            let file = JSON.parse(value);
            appendData(id, file);
        });

        //Upload files
        $(".upload").change(function () {
            let id = $(this).attr('id');
            let x = document.getElementById(id);
            let spinner = `<div class="spinner m-loader m-loader--primary" style="width: 30px; display: inline-block;">
                            </div>`;
            $(`.${id}_list`).append(`${spinner}`);
            var files = x.files;
            var formData = new FormData();
            // Loop through each of the selected files.
            for (let i = 0; i < files.length; i++) {
                let file = files[i];
                // Check the file type.
//                if (!file.type.match('image.*')) {
//                    continue;
//                }

                // Add the file to the request.
                formData.append('files[]', file, file.name);

            }

            var url = "{{ route('Business::subscription@upload')}}";
//            var token = $("input[name='_token']").val();
            $.ajax({
                url: url,
                method: 'POST',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    $("div").remove(".spinner");
                    append_id = id.replace("_input", "");
                    $.each(data, function (key, value) {
                        let file = JSON.parse(value);
                        appendData(append_id, file);
                    });
                }
            });
        });
        var removeItem = function (e) {
            e.closest('li').remove();
        };

        function appendData(id, file) {
            for (let i = 0; i < file.length; i++) {
                let file_name = file[i][0].split("/").pop();
                let html = `<li>
                                <div style="text-align: left;display: inline-block;width: 70%">${file_name}</div>
                                <input type="hidden" name="uploaded_files[]" value="${file[i][0]}">
                                <div style="text-align: right;display: inline-block;float: right;">
                                    <a href="${file[i][1]}" target="_blank" data-toggle="tooltip" title="Xem" class="btn btn-outline-accent m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air"> <i class="la la-eye"></i></a>
                                    <a href="javascript:void(0);" onclick="return removeItem($(this));" data-toggle="tooltip" title="Xóa" class="btn btn-outline-accent m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air"> <i class="la la-remove"></i></a>
                                </div>
                           </li>`;
                $(`.${id}_input_list`).append(`${html}`);
            }
        }

    </script>
    <script type="text/javascript">
        function updateNote(value, id) {
            var url = "{{ route('Business::subscription@updateNote')}}";
            var token = $("input[name='_token']").val();
            $.ajax({
                url: url,
                method: 'POST',
                data: {
                    note: value,
                    subscription_id: id,
                    _token: token
                },
                success: function (data) {
                    console.log(data);
                }
            });
        }
    </script>
    <script>
        $("input[name='contractValue']").change(function () {
            var $this = $(this);
            var subscription_id = $this.closest("tr").find(".id").text();
            var contract_value = $this.val();
            var url = "{{ route('Business::subscription@updateContractValue')}}";
            $.ajax({
                url: url,
                method: 'POST',
                data: {
                    subscription_id: subscription_id,
                    contract_value: contract_value
                },
                success: function (value) {
                    let format_value = value.replace(/[\D\s\._\-]+/g, "");
                    format_value = format_value ? parseInt(format_value, 10) : 0;
                    $this.siblings(".formatted_contract_value").html(function () {
                        return (format_value === 0) ? "0" : format_value.toLocaleString("en-US");
                    });

                    $this.hide();
                },
                error: function (error) {
                    console.log(error);
                }
            });
        });

        $(".edit_contract_value").click(function () {
            if ($(this).children(".contract_value").css('display') === 'none') {
                $(this).children(".contract_value").show();
            }
        });
        $(document).click(function (e) {
            if ($(e.target).children(".contract_value").length === 0 && $(e.target).siblings(".contract_value").length === 0 && !$(e.target).hasClass("contract_value")) {
                $(".contract_value").hide();
            }
        });
    </script>
    <script>
        $("select[name='assignee_icheck_id']").change(function () {
            var $this = $(this);
            var subscription_id = $this.closest("tr").find(".id").text();
            var icheck_id = $this.val();

            var url = "{{ route('Business::subscription@updateAssignedTo')}}";
            $.ajax({
                url: url,
                method: 'POST',
                data: {
                    subscription_id: subscription_id,
                    icheck_id: icheck_id
                },
                success: function (value) {
                    console.log(value);
                },
                error: function (error) {
                    console.log(error);
                }
            });
        });

        // Update Times for Creating Subscription
        $("select[name='days']").change(function () {
            $(".demo-period").show();
            $(".demo-expired").show();
            let days = $(this).val();
            if (!days) {
                $(this).closest("tr").find("input[name='start_time']").val("");
                $(this).closest("tr").find("input[name='contract_expiration']").val("");
                return false;
            }
            if (parseInt(days) === 7) {
                $("input[name='trial_expiration']").val("");
                $("select[name='demo_days']").val("");
                $(".demo-period").hide();
                $(".demo-expired").hide();
            }
            let currentDate = formatDate(new Date());
            $(this).closest("tr").find("input[name='start_time']").val(currentDate);
            let today = new Date();
            today.setDate(today.getDate() + parseInt(days));
            let expire_date = formatDate(today);
//            $("input[name='contract_expiration']").val(expire_date);
            $(this).closest("tr").find("input[name='contract_expiration']").val(expire_date);
        });

        $("select[name='demo_days']").change(function () {
            let days = $(this).val();
            if (!days) {
                $(this).closest("tr").find("input[name='trial_expiration']").val("");
                return false;
            }
            let today = new Date();
            today.setDate(today.getDate() + parseInt(days));
            let expire_demoDate = formatDate(today);
            $(this).closest("tr").find("input[name='trial_expiration']").val(expire_demoDate);
        });

        function formatDate(date) {
            let twoDigitMonth = date.getMonth() + 1 + "";
            if (twoDigitMonth.length === 1) {
                twoDigitMonth = "0" + twoDigitMonth;
            }
            return (date.getDate() + "/" + twoDigitMonth + "/" + date.getFullYear());
        }

        $(".start_time").datepicker({
            dateFormat: 'dd/mm/yy',
            changeMonth: true,
            changeYear: true,
            todayHighlight: !0,
            orientation: "bottom left",
            templates: {leftArrow: '<i class="la la-angle-left"></i>', rightArrow: '<i class="la la-angle-right"></i>'}
        });

        $(".contract_expiration").datepicker({
            dateFormat: 'dd/mm/yy',
            changeMonth: true,
            changeYear: true,
            todayHighlight: !0,
            orientation: "bottom left",
            templates: {leftArrow: '<i class="la la-angle-left"></i>', rightArrow: '<i class="la la-angle-right"></i>'}
        });

        $(".trial_expiration").datepicker({
            dateFormat: 'dd/mm/yy',
            changeMonth: true,
            changeYear: true,
            todayHighlight: !0,
            orientation: "bottom left",
            templates: {leftArrow: '<i class="la la-angle-left"></i>', rightArrow: '<i class="la la-angle-right"></i>'}
        });
    </script>

@endpush
