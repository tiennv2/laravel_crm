@extends('qrcode.layouts.app')
@section('body_class', 'm-page--fluid m-page--loading-enabled m-page--loading m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default')

@push('scripts')
  <script type="text/javascript">
    <?php
      $sharedData = [
        'config' => [
          'csrfToken' => csrf_token(),
          'logoutUrl' => route('logout'),
          'isNative' => true,
        ],
        'loggedInAccount' => auth()->check() ? auth()->user() : null,
      ];
      ?>
      window.SHARED_DATA = {!! json_encode($sharedData) !!}
  </script>
  <script src="https://cdn.jsdelivr.net/npm/tinymce@4.7.11/tinymce.min.js" type="text/javascript"></script>
  <script src="{{ mix('js/app.js') }}"></script>
@endpush

@section('content')
  <!-- begin::Page loader -->
  <div class="m-page-loader m-page-loader--base">
    <div class="m-blockui">
      <span>
        Please wait...
      </span>
      <span>
        <div class="m-loader m-loader--brand"></div>
      </span>
    </div>
  </div>
  <!-- end::page Loader -->
  <div id="app" class="m-grid m-grid--hor m-grid--root m-page">
    <app-header></app-header>
    <div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop  m-body m-page__container">
      <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader">
          <div class="d-flex align-items-center">
            <div class="mr-auto">
              <h3 class="m-subheader__title">
                QUẢN LÝ GÓI DỊCH VỤ
              </h3>
            </div>
            @can("GATEWAY-addPlan")
              <div style="float: right"><a href="{{route('Business::plan@add')}}"
                                           class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air"><span>
                                    <i class="la la-plus-circle"></i>
                                    <span>Thêm gói dịch vụ</span></span></a>
              </div>
            @endcan
          </div>
        </div>
        <!-- END: Subheader -->
        @can("GATEWAY-viewPlan")
          <div class="m-content">
            <!--Begin::Section-->
            <div class="m-portlet m-portlet--mobile">
              <div class="m-portlet__body">
                @if (session('success'))
                  <div
                    class="m-alert m-alert--outline m-alert--square m-alert--outline-2x alert alert-success alert-dismissible fade show"
                    role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    </button>
                    {{ session('success') }}
                  </div>
                @endif
                  <div
                    style="width:200px;text-align:center;padding:5px;margin-bottom: 0;margin-top: 50px;margin-left: 20px;font-weight: bold;font-style: italic;border: solid 1px;border-radius: 6px">
                    Đang hiển thị {{$count_record_in_page}}/{{$count_record_total}} bản ghi
                  </div>
                <div class="table-responsive">
                  <table class="table table-striped- table-bordered table-hover table-checkable"
                         style="margin-top: 20px;margin-left:auto;margin-right:auto">
                    <!--begin::Thead-->
                    <thead>
                    <tr>
                      <th style="font-weight: bold">Id</th>
                      <th style="width: 300px;font-weight: bold">Tên</th>
                      <th style="font-weight: bold;width: 300px">Số lượng sản phẩm</th>
                      <th style="font-weight: bold">Giá (VNĐ/năm)</th>
                      {{--<th style="font-weight: bold">Hành động</th>--}}
                    </tr>
                    </thead>
                    <!--end::Thead-->
                    <!--begin::Tbody-->
                    <tbody>
                    @foreach($plans as $plan)
                      <tr>
                        <td>{{$plan->id}}</td>
                        <td>
                          @if($plan->used)
                            {{$plan->name}}
                          @else
                            <a href="{{route('Business::plan@edit',[$plan->id])}}">{{$plan->name}}</a>
                          @endif
                        </td>
                        <td>{{number_format($plan->quota,0,".",",")}}</td>
                        <td>{{number_format($plan->price,0,".",",")}}</td>
                        {{--<td>--}}
                        {{--<button type="button" class="btn m-btn m-btn--gradient-from-danger m-btn--gradient-to-warning"--}}
                        {{--data-toggle="modal" data-target="#delete{{$plan->id}}"--}}
                        {{-->--}}
                        {{--Xóa--}}
                        {{--</button>--}}
                        {{--<!--Modal Comfirm delete-->--}}
                        {{--<div id="delete{{$plan->id}}" class="modal fade"--}}
                        {{--role="dialog">--}}
                        {{--<div class="modal-dialog">--}}
                        {{--<!-- Modal content-->--}}
                        {{--<div class="modal-content">--}}
                        {{--<div class="modal-header">--}}
                        {{--<h3>Bạn chắc--}}
                        {{--chắn muốn xóa bỏ Gói dịch vụ "{{$plan->name}}"?</h3>--}}
                        {{--<button type="button" class="close"--}}
                        {{--data-dismiss="modal">&times;--}}
                        {{--</button>--}}
                        {{--</div>--}}
                        {{--<form method="get"--}}
                        {{--action="{{ route('Business::plan@delete',[$plan->id]) }}">--}}
                        {{--{{ csrf_field() }}--}}
                        {{--<div class="modal-footer">--}}
                        {{--<button type="submit"--}}
                        {{--class="btn btn-success">--}}
                        {{--Đồng ý--}}
                        {{--</button>--}}
                        {{--<button type="button"--}}
                        {{--class="btn btn-default"--}}
                        {{--data-dismiss="modal">Hủy--}}
                        {{--yêu cầu--}}
                        {{--</button>--}}
                        {{--</div>--}}
                        {{--</form>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--<!--End Modal-->--}}
                        {{--</td>--}}
                      </tr>
                    @endforeach
                    </tbody>
                    <!--end::Tbody-->
                  </table>
                </div>
                <!--end::Table-->
                <div style="float:right;"><?php echo $plans->links(); ?></div>
              </div>
              <!--End::Section-->
            </div>
          </div>
        @endcan
      </div>
    </div>
  </div>
@endsection
@push('scripts')
  <script type="text/javascript" src="{{ asset('qr_code/metronic/js/vendors.bundle.js') }}"></script>
  <script type="text/javascript" src="{{ asset('qr_code/metronic/js/datepicker/bootstrap-datepicker.js') }}"></script>
  <script type="text/javascript" src="{{ asset('qr_code/metronic/js/datatables/datatables.bundle.js') }}"></script>
  <script type="text/javascript" src="{{ asset('qr_code/metronic/js/datatables/scrollable.js') }}"></script>
  <script type="text/javascript">

    $(document).ready(function () {
      // Basic
      $("#status").select2({
        placeholder: 'Trạng thái',
        allowClear: true
      });

      $("select").on("select2:select", function (evt) {
        var element = evt.params.data.element;
        var $element = $(element);
        $element.detach();
        $(this).append($element);
        $(this).trigger("change");
      });


    });
  </script>

@endpush
