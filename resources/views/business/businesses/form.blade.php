@extends('qrcode.layouts.app')
@push('styles')
  <style>
    .help-block {
      color: red;
    }
  </style>

@endpush
@section('body_class', 'm-page--fluid m-page--loading-enabled m-page--loading m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default')
@push('scripts')
  <script type="text/javascript">
    <?php
      $sharedData = [
        'config' => [
          'csrfToken' => csrf_token(),
          'logoutUrl' => route('logout'),
          'isNative' => true,
        ],
        'loggedInAccount' => auth()->check() ? auth()->user() : null,
      ];
      ?>
      window.SHARED_DATA = {!! json_encode($sharedData) !!}
  </script>
  <script src="https://cdn.jsdelivr.net/npm/tinymce@4.7.11/tinymce.min.js" type="text/javascript"></script>
  <script src="{{ mix('js/app.js') }}"></script>
@endpush

@section('content')
  <!-- begin::Page loader -->
  <div class="m-page-loader m-page-loader--base">
    <div class="m-blockui">
      <span>
        Please wait...
      </span>
      <span>
        <div class="m-loader m-loader--brand"></div>
      </span>
    </div>
  </div>
  <!-- end::Page Loader -->
  <div id="app" class="m-grid m-grid--hor m-grid--root m-page">
    <app-header></app-header>
    <div
      class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop 	m-container m-container--responsive m-container--xxl m-page__container m-body">
      <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-content">
          <div class="m-portlet">
            <div class="m-portlet__head">
              <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                  <h3 class="m-portlet__head-text">
                    <a href="{{route('Business::businesses@index')}}">
                      <i class="la la-arrow-left"></i>
                    </a>
                    {{ isset($business) ? 'Sửa thông tin doanh nghiệp ' : 'Thêm mới doanh nghiệp' }}
                  </h3>
                </div>
              </div>
            </div>
            <!-- /page header -->
            @if (session('success'))
              <div
                class="m-alert m-alert--outline m-alert--square m-alert--outline-2x alert alert-success alert-dismissible fade show"
                role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                </button>
                {{ session('success') }}
              </div>
            @endif
            <form method="POST" enctype="multipart/form-data" name="form"
                  class="m-form m-form--fit m-form--label-align-right"
                  action="{{ isset($business) ? route('Business::businesses@update', [$business->id] ): route('Business::businesses@store') }}">
              {{ csrf_field() }}
              @if (isset($business))
                <input type="hidden" name="_method" value="PUT">
              @endif
              <div class="m-portlet__body">
                <!------------------ Name--------------->
                <div class="form-group m-form__group row">
                  <label class="col-form-label col-lg-3 col-sm-12">Tên *</label>
                  <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="text" id="name" name="name"
                           class="form-control"
                           value="{{ old('name') ?: @$business->name }}"
                           required/>
                  </div>
                  @if ($errors->has('name'))
                    <div class="help-block">{{ $errors->first('name') }}</div>
                  @endif
                </div>
                <!------------------ Address--------------->
                <div class="form-group m-form__group row">
                  <label class="col-form-label col-lg-3 col-sm-12">Địa
                    chỉ *</label>
                  <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="text" id="address" name="address"
                           class="form-control"
                           value="{{ old('address') ?: @$business->address}}"
                           required/>
                  </div>
                  @if ($errors->has('address'))
                    <div class="help-block">{{ $errors->first('address') }}</div>
                  @endif
                </div>
                <!------------------ Tax_code--------------->
                <div class="form-group m-form__group row">
                  <label class="col-form-label col-lg-3 col-sm-12">Mã số thuế *</label>
                  <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="text" id="tax_code" name="tax_code"
                           class="form-control"
                           value="{{ old('tax_code') ?: @$business->tax_code}}"
                           required/>
                  </div>
                  @if ($errors->has('tax_code'))
                    <div class="help-block">{{ $errors->first('tax_code') }}</div>
                  @endif
                </div>
                <!------------------Business Email--------------->
                <div class="form-group m-form__group row">
                  <label class="col-form-label col-lg-3 col-sm-12">Email doanh nghiệp *</label>
                  <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="email" name="email" class="form-control"
                           value="{{ old('email') ?: @$business->email }}" required/>
                  </div>
                  @if ($errors->has('email'))
                    <div class="help-block">{{ $errors->first('email') }}</div>
                  @endif
                </div>
                <!------------------Business Email login--------------->
                <div class="form-group m-form__group row">
                  <label class="col-form-label col-lg-3 col-sm-12">Email đăng nhập *</label>
                  <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="email" name="login_email" class="form-control"
                           value="{{ old('login_email') ?: @$business->login_email }}" required/>
                  </div>
                  @if ($errors->has('login_email'))
                    <div class="help-block">{{ $errors->first('login_email') }}</div>
                  @endif
                </div>
                <!------------------ Phone_Number--------------->
                <div class="form-group m-form__group row">
                  <label class="col-form-label col-lg-3 col-sm-12">Số điện thoại *</label>
                  <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="text" name="phone_number" class="form-control"
                           @if(isset($business)) @endif
                           value="{{ old('phone_number') ?: @$business->phone_number}}" required/>
                  </div>
                  @if ($errors->has('phone_number'))
                    <div class="help-block">{{ $errors->first('phone_number') }}</div>
                  @endif
                </div>
                <!------------------ Website--------------->
                <div class="form-group m-form__group row">
                  <label class="col-form-label col-lg-3 col-sm-12">Website</label>
                  <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="text" name="website" class="form-control"
                           value="{{ old('website') ?: @$business->website }}"/>
                  </div>
                  @if ($errors->has('website'))
                    <div class="help-block">{{ $errors->first('website') }}</div>
                  @endif
                </div>
                <!------------------ Fax--------------->
                <div class="form-group m-form__group row">
                  <label class="col-form-label col-lg-3 col-sm-12">Fax </label>
                  <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="text" name="fax" class="form-control"
                           value="{{ old('fax') ?: @$business->fax}}"/>
                  </div>
                  @if ($errors->has('fax'))
                    <div class="help-block">{{ $errors->first('fax') }}</div>
                  @endif
                </div>
                <!------------------ Người phụ trách--------------->
                <div class="form-group m-form__group row">
                  <label class="col-form-label col-lg-3 col-sm-12">Người hỗ trợ </label>
                  <div class="col-lg-4 col-md-9 col-sm-12">
                    <select class="form-control" id="assigned_to" name="assigned_to">
                      <option></option>
                      @foreach($CMSusers as $user)
                        <option value="{{$user->id}}"
                          {{(isset($business) && $business->assigned_to==$user->icheck_id)?'selected="selected"' :''}}
                        >{{$user->name}} ({{$user->icheck_id}})
                        </option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <!------------------ Password--------------->
                <div class="form-group m-form__group row">
                  <label class="col-form-label col-lg-3 col-sm-12">Mật
                    khẩu @if(!isset($business)){{"*"}} @endif</label>
                  <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="password" name="password" class="form-control"
                           value="{{ old('password') }}"
                           @if(!isset($business))
                           required
                      @endif
                    />
                    <p
                      style="margin-top: 5px;margin-bottom: 0;font-family: Arial, sans-serif;color: #7e8c9e;font-size: 12px">
                      Mật khẩu bao gồm ít nhất 6 ký tự!
                    </p>
                  </div>
                  @if ($errors->has('password'))
                    <div class="help-block">{{ $errors->first('password') }}</div>
                  @endif
                </div>
              @if(isset($business))
                <!------------------ Created By--------------->
                  <div class="form-group m-form__group row">
                    <label class="col-form-label col-lg-3 col-sm-12">Người tạo</label>
                    <div class="col-lg-4 col-md-9 col-sm-12">
                      <select class="form-control" id="created_by" name="created_by">
                        <option></option>
                        @foreach($CMSusers as $user)
                          <option value="{{$user->icheck_id}}"
                            {{($business->created_by==$user->icheck_id)?'selected="selected"' :''}}
                          >{{$user->name}} @if($user->role_names)
                              ({{$user->role_names}}) @endif</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                @endif
                <div class="m-portlet__foot m-portlet__foot--fit">
                  <div class="m-form__actions m-form__actions">
                    <div class="row">
                      <div class="col-lg-9 ml-lg-auto">
                        <button type="submit" class="btn btn-success">
                          {{ isset($business) ? 'Cập nhật' : 'Thêm mới' }}
                        </button>
                        <button type="reset" class="btn btn-secondary">
                          {{ isset($business) ? 'Hủy' : 'Nhập lại' }}
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('scripts')
  <script type="text/javascript" src="{{ asset('qr_code/metronic/js/vendors.bundle.js') }}"></script>
  <script>
    $(document).ready(function () {
      $("#created_by").select2({
        placeholder: 'Chọn người tạo',
        allowClear: true
      });

      $("#assigned_to").select2({
        placeholder: 'Chọn người hỗ trợ',
        allowClear: true
      });

      $("select").on("select2:select", function (evt) {
        var element = evt.params.data.element;
        var $element = $(element);
        $element.detach();
        $(this).append($element);
        $(this).trigger("change");
      });

    });

  </script>

@endpush
