@extends('qrcode.layouts.app')
@section('body_class', 'm-page--fluid m-page--loading-enabled m-page--loading m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default')
@push('styles')
  <style>
    .select2 {
      width: 100% !important;
    }
  </style>
@endpush
@push('scripts')
  <script type="text/javascript">
    <?php
      $sharedData = [
        'config' => [
          'csrfToken' => csrf_token(),
          'logoutUrl' => route('logout'),
          'isNative' => true,
        ],
        'loggedInAccount' => auth()->check() ? auth()->user() : null,
      ];
      ?>
      window.SHARED_DATA = {!! json_encode($sharedData) !!}
  </script>
  <script src="https://cdn.jsdelivr.net/npm/tinymce@4.7.11/tinymce.min.js" type="text/javascript"></script>
  <script src="{{ mix('js/app.js') }}"></script>
@endpush

@section('content')
  <!-- begin::Page loader -->
  <div class="m-page-loader m-page-loader--base">
    <div class="m-blockui">
      <span>
        Please wait...
      </span>
      <span>
        <div class="m-loader m-loader--brand"></div>
      </span>
    </div>
  </div>
  <!-- end::page Loader -->
  <div id="app" class="m-grid m-grid--hor m-grid--root m-page">
    <app-header></app-header>
    <div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop  m-body m-page__container">
      <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader">
          <div class="d-flex align-items-center">
            <div class="mr-auto">
              <h3 class="m-subheader__title ">
                QUẢN LÝ DOANH NGHIỆP
              </h3>
            </div>
            @can("GATEWAY-addBusiness")
              <div style="float: right"><a href="{{route('Business::businesses@add')}}"
                                           class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air"><span>
                                    <i class="la la-plus-circle"></i>
                                    <span>Thêm doanh nghiệp</span></span></a>
              </div>
            @endcan
          </div>
        </div>
        <!-- END: Subheader -->
        @if(auth()->user()->can('GATEWAY-viewBusiness') || auth()->user()->can('GATEWAY-viewLimitBusiness'))
          <div class="m-content">
            <!--Begin::Section-->
            <div class="m-portlet m-portlet--mobile">
              <div class="m-portlet__body">
                <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                  <form action="{{route('Business::businesses@index')}}" method="get">
                    <div class="form-group m-form__group row align-items-center">
                      <div class="col-md-3">
                        <div class="m-form__group">
                          <div class="m-form__label">
                            <label>
                              Tên doanh nghiệp:
                            </label>
                          </div>
                          <div class="m-form__control">
                            <input type="text" class="form-control m-input" name="name"
                                   value="{{ Request::input('name') }}"
                                   data-col-index="0">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="m-form__group">
                          <div class="m-form__label">
                            <label>
                              Địa chỉ:
                            </label>
                          </div>
                          <div class="m-form__control">
                            <input type="text" class="form-control m-input" name="address"
                                   value="{{ Request::input('address') }}"
                                   data-col-index="0">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="m-form__group">
                          <div class="m-form__label">
                            <label>
                              Mã số thuế:
                            </label>
                          </div>
                          <div class="m-form__control">
                            <input type="text" class="form-control m-input" name="tax_code"
                                   value="{{ Request::input('tax_code') }}">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="m-form__group">
                          <div class="m-form__label">
                            <label>
                              Số điện thoại:
                            </label>
                          </div>
                          <div class="m-form__control">
                            <input type="text" class="form-control m-input" name="phone_number"
                                   value="{{ Request::input('phone_number') }}">
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="form-group m-form__group row align-items-center">
                      <div class="col-md-3">
                        <div class="m-form__group">
                          <div class="m-form__label">
                            <label>
                              Email Doanh nghiệp:
                            </label>
                          </div>
                          <div class="m-form__control">
                            <input type="text" class="form-control m-input" name="email"
                                   value="{{ Request::input('email') }}">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="m-form__group">
                          <div class="m-form__label">
                            <label>
                              Trạng thái:
                            </label>
                          </div>
                          <div class="m-form__control">
                            <select class="form-control m-input" id="status"
                                    name="status">
                              <option value="" selected="selected">--Tất cả--</option>
                              {{--<option value="0" {{Request::input('status')==="0"?'selected="selected"':''}}>--}}
                              {{--Chờ kích hoạt--}}
                              {{--</option>--}}
                              {{--<option value="1" {{Request::input('status')==1?'selected="selected"':''}}>--}}
                              {{--Kích hoạt--}}
                              {{--</option>--}}
                              {{--<option value="2" {{Request::input('status')==2?'selected="selected"':''}}>--}}
                              {{--Hủy kích hoạt--}}
                              {{--</option>--}}
                              <option value="deleted" {{Request::input('status')=='deleted'?'selected="selected"':''}}>
                                Đã bị xóa
                              </option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="m-form__group">
                          <div class="m-form__label">
                            <label>
                              Người tạo:
                            </label>
                          </div>
                          <div class="m-form__control">
                            <select class="form-control m-input" id="created_by"
                                    name="created_by">
                              <option value="" selected="selected"></option>
                              @foreach($CMSusers as $user)
                                <option value="{{$user->icheck_id}}"
                                  {{Request::input('created_by')==$user->icheck_id?'selected="selected"':''}}
                                >{{$user->name}}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="m-form__group">
                          <div class="m-form__label">
                            <label>
                              Người hỗ trợ:
                            </label>
                          </div>
                          <div class="m-form__control">
                            <select class="form-control m-input" id="assigned_to"
                                    name="assigned_to">
                              <option value="" selected="selected"></option>
                              @foreach($CMSusers as $user)
                                <option value="{{$user->icheck_id}}"
                                  {{Request::input('assigned_to')==$user->icheck_id?'selected="selected"':''}}
                                >{{$user->name}}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="form-group m-form__group row align-items-center">
                      <div class="col-md-5">
                        <div class="m-form__group">
                          <div class="m-form__control">
                            <button class="btn btn-brand m-btn m-btn--icon" type="submit">
                            <span>
                            <i class="la la-search"></i>
                            <span>
                            Tìm kiếm
                            </span>
                            </span>
                            </button>
                            &nbsp;&nbsp;
                            <button class="btn btn-secondary m-btn m-btn--icon" type="button"
                                    onclick="window.location='{{route('Business::businesses@index')}}'">
                            <span>
                            <i class="la la-refresh"></i>
                            <span>
                            Làm mới
                            </span>
                            </span>
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
                @if (session('success'))
                  <div
                    class="m-alert m-alert--outline m-alert--square m-alert--outline-2x alert alert-success alert-dismissible fade show"
                    role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    </button>
                    {{ session('success') }}
                  </div>
                @endif
                {{--<div--}}
                {{--style="width:200px;text-align:center;padding:5px;margin-bottom: 0;margin-top: 50px;margin-left: 20px;font-weight: bold;font-style: italic;border: solid 1px;border-radius: 6px">--}}
                {{--Đang hiển thị {{$count_record_in_page}}/{{$count_record_total}} bản ghi--}}
                {{--</div>--}}
                <div style="margin-bottom: 0;margin-top: 50px;margin-left: 50px;">
                  <span
                    style="width:200px;text-align:center;padding:6px;font-weight: bold;font-style: italic;border: solid 1px;border-radius: 6px">
                    Đang hiển thị {{$count_record_in_page}}/{{$count_record_total}} bản ghi
                  </span>
                  <span style="margin-left: 5px">
                    <a href="{{route('Business::businesses@excelExport',$query)}}"
                       class="btn btn-info m-btn btn-sm m-btn m-btn--icon m-btn--pill">
                      <span>
													<i class="la la-cloud-download"></i>
													<span>
														Xuất Excel
													</span>
                      </span>
                    </a>
                  </span>
                </div>
                <div class="table-responsive">
                  <table class="table table-striped- table-bordered table-hover table-checkable"
                         style="margin-top: 60px;margin-left:auto;margin-right:auto;">
                    <!--begin::Thead-->
                    <thead>
                    <tr>
                      <th style="font-weight: bold">Id</th>
                      <th style="min-width: 300px;font-weight: bold">Tên</th>
                      <th style="font-weight: bold;width: 150px">Thông tin hợp tác</th>
                      <th style="font-weight: bold;min-width: 100px">Tổng số sản phẩm đang quản lý
                      </th>
                      <th style="width: 200px;font-weight: bold">Mã số thuế</th>
                      <th style="font-weight: bold;min-width: 300px">Địa chỉ</th>
                      <th style="font-weight: bold;width: 200px">Email Doanh nghiệp</th>
                      <th style="font-weight: bold;width: 200px">Email đăng nhập</th>
                      <th style="width: 150px;font-weight: bold">Số điện thoại</th>
                      <th style="font-weight: bold;width: 100px">Ngày tạo</th>
                      <th style="font-weight: bold;min-width: 150px">Người tạo</th>
                      <th style="font-weight: bold;width: 150px">Nhân viên hỗ trợ</th>
                      {{--<th style="width: 150px;font-weight: bold">Trạng thái</th>--}}
                      <th style="font-weight: bold;min-width: 500px">Hành động</th>
                    </tr>
                    </thead>
                    <!--end::Thead-->
                    <!--begin::Tbody-->
                    <tbody>
                    @foreach($businesses as $business)
                      <tr>
                        <td>{{$business->id}}</td>
                        <td>
                          {{--@if(auth()->user()->can('GATEWAY-updateBusiness'))--}}
                          {{--<a href="{{route('Business::businesses@edit',[$business->id])}}">{{$business->name}}</a>--}}
                          {{--@else--}}
                          {{--{{$business->name}}--}}
                          {{--@endif--}}
                          <div
                            class="m-dropdown m-dropdown--up m-dropdown--inline m-dropdown--small m-dropdown--arrow m-dropdown--align-left"
                            m-dropdown-toggle="hover">
                            <a class="m-dropdown__toggle btn btn-silver dropdown-toggle">
                              {{$business->name}}
                            </a>
                            <div class="m-dropdown__wrapper">
                              <div class="m-dropdown__inner">
                                <div class="m-dropdown__body" style="padding:10px; width: 350px">
                                  <div class="m-dropdown__content">
                                    <ul class="m-nav">
                                      @can("GATEWAY-viewProduct")
                                        <li class="m-nav__item"
                                            style="margin-bottom: 5px">
                                          <a href="{{route('Business::product@index')}}?business_id={{$business->id}}"
                                             target="_blank" class="m-nav__link">
                                            <span class="m-nav__link-text">Xem danh sách sản phẩm quản lý</span>
                                          </a>
                                        </li>
                                      @endcan
                                      @can("GATEWAY-viewProductRequest")
                                        <li class="m-nav__item"
                                            style="margin-bottom: 5px">
                                          <a
                                            href="{{route('Business::productRequest@index')}}?business_id={{$business->id}}"
                                            target="_blank" class="m-nav__link">
                                            <span class="m-nav__link-text">Xem danh sách yêu cầu QL sản phẩm</span>
                                          </a>
                                        </li>
                                      @endcan
                                      @can("GATEWAY-viewProductDistributor")
                                        <li class="m-nav__item"
                                            style="margin-bottom: 5px">
                                          <a
                                            href="{{route('Business::productDistributor@index')}}?business_id={{$business->id}}"
                                            target="_blank" class="m-nav__link">
                                            <span class="m-nav__link-text">Xem danh sách NPP Sản phẩm</span>
                                          </a>
                                        </li>
                                      @endcan
                                      @can("GATEWAY-viewSubscription")
                                        <li class="m-nav__item"
                                            style="margin-bottom: 5px">
                                          <a
                                            href="{{route('Business::subscription@index')}}?business_id={{$business->id}}"
                                            target="_blank" class="m-nav__link">
                                            <span class="m-nav__link-text">Xem danh Gói dịch vụ sử dụng</span>
                                          </a>
                                        </li>
                                      @endcan
                                      @can("GATEWAY-viewGLN")
                                        <li class="m-nav__item"
                                            style="margin-bottom: 5px">
                                          <a href="{{route('Business::gln@index')}}?business_id={{$business->id}}"
                                             target="_blank" class="m-nav__link">
                                            <span class="m-nav__link-text">Xem danh sách mã GLN</span>
                                          </a>
                                        </li>
                                      @endcan
                                    </ul>
                                  </div>
                                </div>
                              </div>
                              <span class="m-dropdown__arrow m-dropdown__arrow--left"></span>
                            </div>
                          </div>
                        </td>
                        <td>
                          <button
                            type="button"
                            data-target="#subscriptionInfo_{{$business->id}}"
                            class="btn m-btn--pill m-btn--air btn-outline-info btn-sm"
                            data-toggle="modal">Chi tiết
                          </button>
                          <!--Modal-->
                          <div id="subscriptionInfo_{{$business->id}}" class="modal fade" tabindex="-1"
                               role="dialog">
                            <div class="modal-dialog modal-lg modal-dialog-centered">
                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h2>CHI TIẾT DOANH NGHIỆP "{{$business->name}}"</h2>
                                  <button type="button" class="close"
                                          data-dismiss="modal">&times;
                                  </button>
                                </div>
                                <div class="modal-body" style="height:600px;overflow-y: scroll;">
                                  <div style="border-bottom: dashed 2px; margin-bottom: 5px">
                                    <h4>I. THÔNG TIN DOANH NGHIỆP</h4>
                                    <p><span class="m--font-boldest"><span
                                          class="m-badge m-badge--primary m-badge--dot"></span> Tên Công ty: </span><span>{{$business->name}}</span>
                                    </p>
                                    <p><span class="m--font-boldest"><span
                                          class="m-badge m-badge--primary m-badge--dot"></span> Địa chỉ: </span><span>{{$business->address}}</span>
                                    </p>
                                    <p><span class="m--font-boldest"><span
                                          class="m-badge m-badge--primary m-badge--dot"></span> Mã số thuế: </span><span>{{$business->tax_code}}</span>
                                    </p>
                                    <p><span class="m--font-boldest"><span
                                          class="m-badge m-badge--primary m-badge--dot"></span> Số điện thoại: </span><span>{{$business->phone_number}}</span>
                                    </p>
                                    <p><span class="m--font-boldest"><span
                                          class="m-badge m-badge--primary m-badge--dot"></span> Email Doanh nghiệp: </span><span>{{$business->email}}</span>
                                    </p>
                                    <p><span class="m--font-boldest"><span
                                          class="m-badge m-badge--primary m-badge--dot"></span> Email đăng nhập: </span><span>{{$business->login_email}}</span>
                                    </p>
                                    <p><span class="m--font-boldest"><span
                                          class="m-badge m-badge--primary m-badge--dot"></span> Ngày tạo: </span><span>{{date("d/m/Y h:i:s",strtotime($business->created_at))}}</span>
                                    </p>
                                    <p><span class="m--font-boldest"><span
                                          class="m-badge m-badge--primary m-badge--dot"></span> Người tạo: </span><span>{{$business->created_by}}</span>
                                    </p>
                                    <p><span class="m--font-boldest"><span
                                          class="m-badge m-badge--primary m-badge--dot"></span> Nhân viên đang hỗ trợ: </span><span>{{$business->assigned_to}}</span>
                                    </p>
                                  </div>
                                  <div style="border-bottom: dashed 2px; margin-bottom: 8px">
                                    <h4>II. THÔNG TIN SỐ LƯỢNG SẢN PHẨM DOANH NGHIỆP QUẢN LÝ</h4>
                                    <p><span class="m--font-boldest"><span
                                          class="m-badge m-badge--primary m-badge--dot"></span> Số lượng Sản phẩm đang quản lý: </span>
                                      @if($business->product_managed_quantity)
                                        <a href="{{route('Business::product@index')}}?business_id={{$business->id}}"
                                           target="_blank">
                                            <span class="m-badge m-badge--success m-badge--wide"
                                                  style="font-size: 15px">
                                              {{$business->product_managed_quantity}}
                                            </span>
                                        </a>
                                        <cite>
                                          (Có thể click vào số Sản phẩm để xem chi tiết!)
                                        </cite>
                                      @else
                                        <span
                                          class="m-badge m-badge--info m-badge--wide"
                                          style="font-size: 15px">0</span>
                                      @endif
                                    </p>
                                    <p><span class="m--font-boldest"><span
                                          class="m-badge m-badge--primary m-badge--dot"></span> Số lượng Sản phẩm tối đa được phép quản lý: </span><span
                                        class="m-badge m-badge--info m-badge--wide"
                                        style="font-size: 15px">{{$business->product_subscription_quantity}}</span>
                                    </p>
                                  </div>
                                  <div>
                                    <h4>III. THÔNG TIN CÁC GÓI DỊCH VỤ THÔNG TIN CỦA DOANH NGHIỆP</h4>
                                    @if($business->subscriptionsInfo->count())
                                      <p style="font-style: italic;font-weight: bold; font-size: 18px">
                                        Doanh nghiệp có <a
                                          href="{{route('Business::subscription@index')}}?business_id={{$business->id}}"
                                          target="_blank"><span
                                            class="m-badge m-badge--info m-badge--wide"
                                            style="font-size: 15px">{{$business->subscriptionsInfo->count()}}</span></a>
                                        gói dịch vụ
                                      </p>
                                      @foreach($business->subscriptionsInfo as $key => $subscription)
                                        <p><span class="m--font-boldest">  {{$key + 1 }}
                                            : </span><span
                                            style="font-style: italic;font-weight: bold">Gói {{$subscription->plan->name}}</span>
                                          @if($subscription->status == \App\Models\Business\Subscription::SUBSCRIPTION_STATUS_PENDING)
                                            <span
                                              class="m-badge m-badge--warning m-badge--wide m-badge--rounded">{{\App\Models\Business\Subscription::$statusTexts[$subscription->status]}}</span>
                                          @elseif($subscription->status == \App\Models\Business\Subscription::SUBSCRIPTION_STATUS_APPROVED)
                                            <span
                                              class="m-badge m-badge--success m-badge--wide m-badge--rounded">{{\App\Models\Business\Subscription::$statusTexts[$subscription->status]}}</span>
                                          @elseif($subscription->status == \App\Models\Business\Subscription::SUBSCRIPTION_STATUS_DISAPPROVED)
                                            <span
                                              class="m-badge m-badge--danger m-badge--wide m-badge--rounded">{{\App\Models\Business\Subscription::$statusTexts[$subscription->status]}}</span>
                                          @endif
                                        </p>
                                        <p>- Số lượng sản phẩm của gói: {{$subscription->plan->quota}}</p>
                                        <p>- Thời hạn của gói: @if($subscription->days >=365)
                                            {{number_format($subscription->days/365,0,".",",")}} năm
                                          @else
                                            {{number_format($subscription->days,0,".",",")}} ngày
                                          @endif</p>
                                        <p>- Thời gian bắt đầu Hợp
                                          đồng: @if($subscription->start_time) {{date("d/m/Y",strtotime($subscription->start_time))}} @else
                                            <span style="font-style: italic"></span> @endif</p>
                                        <p>- Thời gian hết hạn Hợp
                                          đồng: @if($subscription->contract_expiration) {{date("d/m/Y",strtotime($subscription->contract_expiration))}} @else
                                            <span style="font-style: italic"></span> @endif</p>
                                        <p>- Thời gian hết hạn demo: @if($subscription->trial_expiration) {{date("d/m/Y",strtotime($subscription->trial_expiration))}} @else
                                            <span style="font-style: italic"></span> @endif</p>
                                        <p>- Thời gian hết hạn có hiệu lực: @if($subscription->expires_on) {{date("d/m/Y",strtotime($subscription->expires_on))}} @else
                                            <span style="font-style: italic"></span> @endif</p>
                                        <p>- Loại ngày hết
                                          hạn: @if(is_null($subscription->expire_type)){{\App\Models\Business\Subscription::$expireTypeTexts[\App\Models\Business\Subscription::EXPIRE_TYPE_IN_CONTRACT]}} @else {{\App\Models\Business\Subscription::$expireTypeTexts[$subscription->expire_type]}} @endif</p>
                                        <p>- Tình trạng thanh
                                          toán: @if(is_null($subscription->payment_status)){{\App\Models\Business\Subscription::$paymentStatusTexts[\App\Models\Business\Subscription::PAYMENT_STATUS_UNKNOWN]}} @else {{\App\Models\Business\Subscription::$paymentStatusTexts[$subscription->payment_status]}} @endif</p>
                                        <p>- Người phụ trách: {{$business->assigned_to}}</p>
                                      @endforeach
                                    @else
                                      {{"Doanh nghiệp chưa sử dụng Gói dịch vụ thông tin nào của iCheck!"}}
                                    @endif
                                  </div>
                                </div>
                                <div class="modal-footer">
                                  <button type="button"
                                          class="btn btn-secondary"
                                          data-dismiss="modal"> Đóng
                                  </button>
                                </div>
                              </div>
                            </div>
                          </div>
                          <!--End Modal-->
                        </td>
                        <td>
                          {{$business->product_managed_quantity}}
                          /{{$business->product_subscription_quantity}}
                        </td>
                        <td>{{$business->tax_code}}</td>
                        <td>{{$business->address}}</td>
                        <td>{{$business->email}}</td>
                        <td>{{$business->login_email}}</td>
                        <td>{{$business->phone_number}}</td>
                        <td>{{date("d/m/Y h:i:s",strtotime($business->created_at))}}</td>
                        <td>{{$business->created_by}}</td>
                        <td>{{$business->assigned_to}}</td>
                        <td>
                          @if($business->deleted_at)
                            @can("GATEWAY-restoreBusiness")
                              <button type="button"
                                      class="btn m-btn m-btn--gradient-from-success m-btn--gradient-to-info"
                                      onclick="window.location='{{route('Business::businesses@restore',[$business->id])}}'">
                                Khôi phục
                              </button>
                            @endcan
                          @else
                            {{--@can("GATEWAY-openStoreExpoBusiness")--}}
                            {{--@if($business->phone_number)--}}
                            {{--<button type="button"--}}
                            {{--class="btn m-btn m-btn--gradient-from-success m-btn--gradient-to-info"--}}
                            {{--onclick="window.location='{{route('Business::businesses@openExpoStore',[$business->id])}}'">--}}
                            {{--@if($business->expo_account_id)--}}
                            {{--Đồng bộ gian hàng--}}
                            {{--@else--}}
                            {{--Mở gian hàng--}}
                            {{--@endif--}}
                            {{--</button>--}}
                            {{--@endif--}}
                            {{--@if($business->expo_account_id)--}}
                            {{--<a class="btn m-btn m-btn--gradient-from-info m-btn--gradient-to-brand"--}}
                            {{--target="_blank"--}}
                            {{--href="http://icheckexpo.com/gian-hang/gian-hang-{{$business->expo_account_id}}">--}}
                            {{--Xem gian hàng--}}
                            {{--</a>--}}
                            {{--@endif--}}
                            {{--@endcan--}}
                            @can("GATEWAY-syncLegacyData")
                              @if($business->legacy_id)
                                <button type="button"
                                        class="btn btn-danger active"
                                        onclick="window.location='{{route('Business::businesses@syncLegacyData',[$business->id])}}'"
                                >
                                  Đồng bộ dữ liệu
                                </button>
                              @endif
                            @endcan
                            @can("GATEWAY-updateBusiness")
                              <button type="button"
                                      class="btn m-btn m-btn--gradient-from-info m-btn--gradient-to-warning"
                                      onclick="window.location='{{route('Business::businesses@edit',[$business->id])}}'"
                              >
                                Sửa
                              </button>
                            @endcan
                            @can("GATEWAY-delBusiness")
                              <button type="button"
                                      class="btn m-btn m-btn--gradient-from-danger m-btn--gradient-to-warning"
                                      data-toggle="modal"
                                      data-target="#delete{{$business->id}}"
                                      @if(auth()->user()->cannot('GATEWAY-delBusiness'))
                                      disabled
                                @endif>
                                Xóa
                              </button>
                          @endcan
                        @endif
                        <!--Modal Comfirm delete-->
                          <div id="delete{{$business->id}}" class="modal fade"
                               role="dialog">
                            <div class="modal-dialog">
                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h2 class="modal-title">Bạn chắc
                                    chắn muốn xóa bỏ doanh nghiệp
                                    "{{$business->name}}
                                    "?</h2>
                                  <button type="button" class="close"
                                          data-dismiss="modal">&times;
                                  </button>
                                </div>
                                <form method="get"
                                      action="{{ route('Business::businesses@delete',[$business->id]) }}">
                                  {{ csrf_field() }}
                                  <div class="modal-footer">
                                    <button type="submit"
                                            class="btn btn-success">
                                      Đồng ý
                                    </button>
                                    <button type="button"
                                            class="btn btn-default"
                                            data-dismiss="modal">Hủy
                                      yêu cầu
                                    </button>
                                  </div>
                                </form>
                              </div>
                            </div>
                          </div>
                          <!--End Modal-->
                          <!--Modal Comfirm Sync-data-->
                          <div id="syncLegacyData{{$business->id}}" class="modal fade"
                               role="dialog">
                            <div class="modal-dialog">
                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h2 class="modal-title">Bạn chắc
                                    chắn muốn đồng bộ lại dữ liệu từ hệ thống CMS cũ sang hệ thống mới CMS mới cho doanh
                                    nghiệp "{{$business->name}}"?</h2>
                                  <button type="button" class="close"
                                          data-dismiss="modal">&times;
                                  </button>
                                </div>
                                <form method="get"
                                      action="{{route('Business::businesses@syncLegacyData',[$business->id])}}">
                                  {{ csrf_field() }}
                                  <div class="modal-body">
                                    <p style="color:red; font-size: 16px">CHÚ Ý: Những dữ liệu mới và thay đổi dữ liệu
                                      của Doanh nghiệp sau lần đồng bộ gần nhất sẽ bị mất nếu bạn thực hiện thao tác
                                      này!</p>
                                    <p>Bạn vẫn muốn thực hiện đồng bộ ?</p>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="submit"
                                            class="btn btn-success">
                                      Đồng ý
                                    </button>
                                    <button type="button"
                                            class="btn btn-default"
                                            data-dismiss="modal">Hủy
                                      yêu cầu
                                    </button>
                                  </div>
                                </form>
                              </div>
                            </div>
                          </div>
                          <!--End Modal-->
                        </td>
                      </tr>
                    @endforeach
                    </tbody>
                    <!--end::Tbody-->
                  </table>
                </div>
                <!--end::Table-->
                <div style="float:right;"><?php echo $businesses->links(); ?></div>
              </div>
              <!--End::Section-->
            </div>
          </div>
        @endif
      </div>
    </div>
  </div>
@endsection
@push('scripts')
  <script type="text/javascript" src="{{ asset('qr_code/metronic/js/vendors.bundle.js') }}"></script>
  <script type="text/javascript"
          src="{{ asset('qr_code/metronic/js/datepicker/bootstrap-datepicker.js') }}"></script>
  {{--<script type="text/javascript" src="{{ asset('qr_code/metronic/js/datatables/datatables.bundle.js') }}"></script>--}}
  {{--<script type="text/javascript" src="{{ asset('qr_code/metronic/js/datatables/scrollable.js') }}"></script>--}}
  <script type="text/javascript">

    $(document).ready(function () {
      // Basic
      $("#status").select2({
        placeholder: 'Trạng thái',
        allowClear: true
      });
      $("#created_by").select2({
        placeholder: 'Chọn người tạo',
        allowClear: true
      });
      $("#assigned_to").select2({
        placeholder: 'Chọn người hỗ trợ',
        allowClear: true
      });
      $("select").on("select2:select", function (evt) {
        var element = evt.params.data.element;
        var $element = $(element);
        $element.detach();
        $(this).append($element);
        $(this).trigger("change");
      });

      $(".modal").on('hidden.bs.modal', function () {
        document.body.style.padding = 0;
      });
    });
  </script>

@endpush
