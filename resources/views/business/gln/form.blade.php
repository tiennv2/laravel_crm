@extends('qrcode.layouts.app')
@push('styles')
    <style>
        .help-block {
            color: red;
        }

        .heroes {
            margin: 0 0 2em 0;
            padding: 0;
            width: 25.5em;
        }

        .heroes li {
            position: relative;
            left: 0;
            color: #00c5dc;
            margin: .5em;
            padding: .3em .3em;
            height: auto;
            min-height: 3em;
            border-radius: 4px;
            border-color: #00c5dc;
            border-width: 2px;
            border-style: solid;
        }

        .disabled {
            pointer-events: none;
        }
    </style>

@endpush
@section('body_class', 'm-page--fluid m-page--loading-enabled m-page--loading m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default')
@push('scripts')
    <script type="text/javascript">
        <?php
            $sharedData = [
                'config' => [
                    'csrfToken' => csrf_token(),
                    'logoutUrl' => route('logout'),
                    'isNative' => true,
                ],
                'loggedInAccount' => auth()->check() ? auth()->user() : null,
            ];
            ?>
            window.SHARED_DATA = {!! json_encode($sharedData) !!}
    </script>
    <script src="https://cdn.jsdelivr.net/npm/tinymce@4.7.11/tinymce.min.js" type="text/javascript"></script>
    <script src="{{ mix('js/app.js') }}"></script>
@endpush

@section('content')
    <!-- begin::Page loader -->
    <div class="m-page-loader m-page-loader--base">
        <div class="m-blockui">
      <span>
        Please wait...
      </span>
            <span>
        <div class="m-loader m-loader--brand"></div>
      </span>
        </div>
    </div>
    <!-- end::Page Loader -->
    <div id="app" class="m-grid m-grid--hor m-grid--root m-page">
        <app-header></app-header>
        <div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop 	m-container m-container--responsive m-container--xxl m-page__container m-body">
            <div class="m-grid__item m-grid__item--fluid m-wrapper">
                <div class="m-content">
                    <div class="m-portlet">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text">
                                        <a href="{{route('Business::gln@index')}}">
                                            <i class="la la-arrow-left"></i>
                                        </a>
                                        {{ isset($gln) ? 'Sửa thông tin GLN' : 'Thêm mới GLN' }}
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <!-- /page header -->
                        @if (session('success'))
                            <div class="m-alert m-alert--outline m-alert--square m-alert--outline-2x alert alert-success alert-dismissible fade show"
                                 role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                </button>
                                {{ session('success') }}
                            </div>
                        @endif
                        @if (session('error'))
                            <div class="m-alert m-alert--outline m-alert--square m-alert--outline-2x alert alert-danger alert-dismissible fade show"
                                 role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                </button>
                                {{ session('error') }}
                            </div>
                        @endif
                        <form method="POST" enctype="multipart/form-data" name="form" onsubmit="return validateForm()"
                              class="m-form m-form--fit m-form--label-align-right"
                              action="{{ isset($gln) ? route('Business::gln@update', [$gln->id] ): route('Business::gln@store') }}">
                            {{ csrf_field() }}
                            @if (isset($gln))
                                <input type="hidden" name="_method" value="PUT">
                            @endif
                            <div class="m-portlet__body">
                                <!------------------ Business--------------->
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">Chọn doanh nghiệp *</label>
                                    <div class="col-lg-4 col-md-9 col-sm-12">
                                        <select class="form-control m-input" data-col-index="7" id="business_id"
                                                name="business_id"
                                                @if(isset($gln))
                                                disabled
                                                @endif
                                                required>
                                            <option value=""></option>
                                            @foreach($businesses as $business)
                                                <option value="{{$business->id}}"
                                                        @if ((isset($gln) && $gln->business_id==$business->id) || old('business_id')==$business->id)
                                                        selected
                                                        @endif
                                                >
                                                    {{$business->name}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @if ($errors->has('business_id'))
                                        <div class="help-block">{{ $errors->first('business_id') }}</div>
                                    @endif
                                </div>
                                <!------------------ Mã GLN --------------->
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">Mã GLN *</label>
                                    <div class="col-lg-4 col-md-9 col-sm-12">
                                        <input type="text" id="gln" name="gln"
                                               class="form-control"
                                               value="{{ old('gln') ?: @$gln->gln }}"
                                               @if(isset($gln))
                                               disabled
                                               @endif
                                               onchange="checkGLN(this.value)"
                                               required/>
                                    </div>
                                    <div class="gln_input help-block">
                                        @if ($errors->has('gln'))
                                            {{ $errors->first('gln') }}
                                        @endif
                                    </div>
                                </div>
                                <!------------------ Business code--------------->
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">Mã doanh nghiệp *</label>
                                    <div class="col-lg-4 col-md-9 col-sm-12">
                                        <input type="text" id="business_code" name="business_code"
                                               class="form-control"
                                               value="{{ old('business_code') ?: @$gln->business_code }}"
                                               @if(isset($gln))
                                               disabled
                                               @endif
                                               onchange="checkBusinessCode(this.value)"
                                               required/>
                                    </div>
                                    <div class="businessCode_input help-block">
                                        @if ($errors->has('business_code'))
                                            {{ $errors->first('business_code') }}
                                        @endif
                                    </div>
                                </div>
                                <!------------------ Country--------------->
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">Chọn quốc gia *</label>
                                    <div class="col-lg-4 col-md-9 col-sm-12">
                                        <select class="form-control m-input" data-col-index="7" id="country"
                                                name="country"
                                                required>
                                            <option value=""></option>
                                            @foreach($countries as $country)
                                                <option value="{{$country->alpha_2}}"
                                                        @if ((isset($gln) && $gln->country == $country->alpha_2) || old('country')==$country->alpha_2)selected="selected" @endif>
                                                    {{$country->name}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @if ($errors->has('country_id'))
                                        <div class="help-block">{{ $errors->first('country_id') }}</div>
                                    @endif
                                </div>
                                <!------------------ Name--------------->
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">Tên GLN *</label>
                                    <div class="col-lg-4 col-md-9 col-sm-12">
                                        <input type="text" id="name" name="name"
                                               class="form-control"
                                               value="{{ old('name') ?: @$gln->name }}"
                                               required/>
                                    </div>
                                    @if ($errors->has('name'))
                                        <div class="help-block">{{ $errors->first('name') }}</div>
                                    @endif
                                </div>
                                <!------------------ Address--------------->
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">Địa
                                        chỉ *</label>
                                    <div class="col-lg-4 col-md-9 col-sm-12">
                                        <input type="text" id="address" name="address"
                                               class="form-control"
                                               value="{{ old('address') ?: @$gln->address}}"
                                               required/>
                                    </div>
                                    @if ($errors->has('address'))
                                        <div class="help-block">{{ $errors->first('address') }}</div>
                                    @endif
                                </div>
                                <!------------------ Email--------------->
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">Email </label>
                                    <div class="col-lg-4 col-md-9 col-sm-12">
                                        <input type="email" name="email" class="form-control"
                                               value="{{ old('email') ?: @$gln->email }}"/>
                                    </div>
                                    @if ($errors->has('email'))
                                        <div class="help-block">{{ $errors->first('email') }}</div>
                                    @endif
                                </div>
                                <!------------------ Phone_Number--------------->
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">Số điện thoại *</label>
                                    <div class="col-lg-4 col-md-9 col-sm-12">
                                        <input type="text" name="phone_number" class="form-control"
                                               value="{{ old('phone_number') ?: @$gln->phone_number}}"/>
                                    </div>
                                    @if ($errors->has('phone_number'))
                                        <div class="help-block">{{ $errors->first('phone_number') }}</div>
                                    @endif
                                </div>
                                <!------------------ Website--------------->
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">Website</label>
                                    <div class="col-lg-4 col-md-9 col-sm-12">
                                        <input type="text" name="website" class="form-control"
                                               value="{{ old('website') ?: @$gln->website }}"/>
                                    </div>
                                    @if ($errors->has('website'))
                                        <div class="help-block">{{ $errors->first('website') }}</div>
                                    @endif
                                </div>
                                <!------------------ Fax--------------->
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">Fax </label>
                                    <div class="col-lg-4 col-md-9 col-sm-12">
                                        <input type="text" name="fax" class="form-control"
                                               value="{{ old('fax') ?: @$gln->fax}}"/>
                                    </div>
                                    @if ($errors->has('fax'))
                                        <div class="help-block">{{ $errors->first('fax') }}</div>
                                    @endif
                                </div>
                                <!------------------ Certificate--------------->
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-3 col-sm-12">Giấy tờ </label>
                                    <div class="col-lg-4 col-md-9 col-sm-12">
                                        <select class="form-control m-input" data-col-index="7" id="certificate_id"
                                                name="certificate_id">
                                            <option value=""></option>
                                            @if (isset($certificates))
                                                @foreach($certificates as $certificate)
                                                    <option value="{{$certificate->id}}"
                                                            @if ((isset($gln) && $gln->certificate_id==$certificate->id) || old('certificate_id')==$certificate->id)
                                                            selected
                                                            @endif
                                                    >
                                                        {{$certificate->name}}
                                                    </option>
                                                @endforeach
                                            @endif
                                        </select>
                                        <div>
                                            <ol class="heroes files_list">
                                                @if(isset($gln) && $gln->certificate_files)
                                                    @php $files = $gln->certificate_files  @endphp
                                                    @for($i=0;$i<count($files);$i++)
                                                        <li>
                                                            <div style="text-align: left;display: inline-block;width: 70%">{{$files[$i][0]}}</div>
                                                            <div style="text-align: right;display: inline-block;float: right;">
                                                                <a href="{{$files[$i][2]}}" target="_blank"
                                                                   data-toggle="tooltip" title="Xem"
                                                                   class="btn btn-outline-accent m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air">
                                                                    <i class="la la-eye"></i></a>
                                                            </div>
                                                        </li>
                                                    @endfor
                                                @endif
                                            </ol>
                                        </div>
                                    </div>
                                    {{--<div class="col-lg-4 col-md-9 col-sm-12">--}}
                                    {{--<button type="button" title="Upload files mới"--}}
                                    {{--data-toggle="modal" data-target="#upload"--}}
                                    {{--class="btn btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--outline-2x">--}}
                                    {{--<i class="la la-cloud-upload"></i>--}}
                                    {{--</button>--}}
                                    {{--<!--Modal Upload files-->--}}
                                    {{--<div id="upload" class="modal fade"--}}
                                    {{--role="dialog">--}}
                                    {{--<div class="modal-dialog modal-lg">--}}
                                    {{--<!-- Modal content-->--}}
                                    {{--<div class="modal-content">--}}
                                    {{--<div class="modal-header">--}}
                                    {{--<h2>UPLOAD giấy tờ</h2>--}}
                                    {{--<button type="button" class="close"--}}
                                    {{--data-dismiss="modal">&times;--}}
                                    {{--</button>--}}
                                    {{--</div>--}}
                                    {{--<form method="POST" enctype="multipart/form-data" id="modal_form"--}}
                                    {{--class="m-form m-form--fit m-form--label-align-right"--}}
                                    {{--action="{{route('Business::certificate@store') }}">--}}
                                    {{--{{ csrf_field() }}--}}
                                    {{--<div class="modal-body">--}}
                                    {{--<!------------------ Business--------------->--}}
                                    {{--<div class="form-group m-form__group row">--}}
                                    {{--<label class="col-form-label col-lg-3 col-sm-12">Chọn--}}
                                    {{--doanh nghiệp *</label>--}}
                                    {{--<div class="col-lg-9 col-md-9 col-sm-12">--}}
                                    {{--<select class="form-control m-input"--}}
                                    {{--data-col-index="12" id="modal_business_id"--}}
                                    {{--name="business_id" style="width:380px"--}}
                                    {{--required>--}}
                                    {{--<option value=""></option>--}}
                                    {{--@foreach($businesses as $business)--}}
                                    {{--<option value="{{$business->id}}"--}}
                                    {{-->{{$business->name}}</option>--}}
                                    {{--@endforeach--}}
                                    {{--</select>--}}
                                    {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<!------------------ Name--------------->--}}
                                    {{--<div class="form-group m-form__group row">--}}
                                    {{--<label class="col-form-label col-lg-3 col-sm-12">Tên--}}
                                    {{--giấy tờ *</label>--}}
                                    {{--<div class="col-lg-7 col-md-9 col-sm-12">--}}
                                    {{--<input type="text" id="certificate_name"--}}
                                    {{--name="certificate_name"--}}
                                    {{--class="form-control"--}}
                                    {{--required/>--}}
                                    {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<!------------------Description--------------->--}}
                                    {{--<div class="form-group m-form__group row">--}}
                                    {{--<label class="col-form-label col-lg-3 col-sm-12">Mô--}}
                                    {{--tả</label>--}}
                                    {{--<div class="col-lg-7 col-md-9 col-sm-12">--}}
                                    {{--<textarea class="form-control" rows="3" placeholder="Mô tả "--}}
                                    {{--name="description"></textarea>--}}
                                    {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<!------------------Upload files--------------->--}}
                                    {{--<div class="form-group m-form__group row">--}}
                                    {{--<label class="col-form-label col-lg-3 col-sm-12">Upload--}}
                                    {{--files</label>--}}
                                    {{--<div class="col-lg-6 col-md-9 col-sm-12">--}}
                                    {{--<label for="file"--}}
                                    {{--style="cursor: pointer;padding:8px;background-color: #0aa7ef;color: white;border-radius: 20px"><i--}}
                                    {{--class="la la-cloud-upload"></i> Chọn--}}
                                    {{--tập tin</label>--}}
                                    {{--</div>--}}
                                    {{--<input type="file" name="files[]"--}}
                                    {{--id="file"--}}
                                    {{--class="upload"--}}
                                    {{--style="display: none!important;"--}}
                                    {{--accept=".png,.jpg,.jpeg,.gif,.pdf,.doc,.docx,.xls, .xlsx"--}}
                                    {{--multiple>--}}
                                    {{--</div>--}}
                                    {{--<div class="form-group m-form__group row">--}}
                                    {{--<label style="color: white"--}}
                                    {{--class="col-form-label col-lg-3 col-sm-12">Upload--}}
                                    {{--files</label>--}}
                                    {{--<div class="col-lg-6 col-md-9 col-sm-12">--}}
                                    {{--<ol class="uploadFiles_list heroes">--}}
                                    {{--</ol>--}}
                                    {{--</div>--}}
                                    {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="modal-footer">--}}
                                    {{--<button type="button" id="modal_submit_button"--}}
                                    {{--class="btn btn-success">--}}
                                    {{--Đồng ý--}}
                                    {{--</button>--}}
                                    {{--<button type="button"--}}
                                    {{--class="btn btn-default"--}}
                                    {{--data-dismiss="modal">Đóng--}}
                                    {{--</button>--}}
                                    {{--</div>--}}
                                    {{--</form>--}}
                                    {{--</div>--}}
                                    {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<!--End Modal-->--}}

                                    {{--</div>--}}
                                </div>
                                <div class="m-portlet__foot m-portlet__foot--fit">
                                    <div class="m-form__actions m-form__actions">
                                        <div class="row">
                                            <div class="col-lg-9 ml-lg-auto">
                                                <button type="submit" class="submit_button btn btn-success">
                                                    {{ isset($gln) ? 'Cập nhật' : 'Thêm mới' }}
                                                </button>
                                                <button type="reset" class="btn btn-secondary">
                                                    {{ isset($gln) ? 'Hủy' : 'Nhập lại' }}
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript" src="{{ asset('qr_code/metronic/js/vendors.bundle.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
// Basic
            $("#business_id").select2({
                placeholder: 'Chọn doanh nghiệp',
                allowClear: true,
            });

            $("#modal_business_id").select2({
                placeholder: 'Chọn doanh nghiệp',
                allowClear: true
            });

            $("#certificate_id").select2({
                placeholder: 'Chọn giấy tờ',
                allowClear: true
            });

            $("#country").select2({
                placeholder: 'Chọn quốc gia',
                allowClear: true
            });

            $("select").on("select2:select", function (evt) {
                var element = evt.params.data.element;
                var $element = $(element);
                $element.detach();
                $(this).append($element);
                $(this).trigger("change");
            });
        });
    </script>
    <script type="text/javascript">
        var url = "{{ route('Business::certificate@certificatesByBusinessId')}}";
        $("select[name='business_id']").change(function () {
            var business_id = $(this).val();
            var token = $("input[name='_token']").val();
            $.ajax({
                url: url,
                method: 'POST',
                data: {
                    business_id: business_id,
                    _token: token
                },
                success: function (data) {
                    $("select[name='certificate_id']").html('');
                    $("select[name='certificate_id']").append(
                        "<option value=''" + ">" + '--Chọn giấy tờ--' + "</option>"
                    );
                    $.each(data, function (key, value) {
                        $("select[name='certificate_id']").append(
                            "<option value=" + value.id + ">" + value.name + "</option>"
                        );
                    });
                }
            });
        });

        $("select[name='certificate_id']").change(function () {
            let spinner = `<div class="spinner m-loader m-loader--primary" style="width: 30px; display: inline-block;">
                            </div>`;
            $(`.files_list`).empty();
            $(`.files_list`).append(`${spinner}`);
            let certificate_id = $(this).val();
            appendCurrentFiles(certificate_id);
        });

        function appendData(file) {
            for (let i = 0; i < file.length; i++) {
                let html = `<li>
                                <div style="text-align: left;display: inline-block;width: 70%">${file[i][0]}</div>
                                <div style="text-align: right;display: inline-block;float: right;">
                                    <a href="${file[i][2]}" target="_blank" data-toggle="tooltip" title="Xem" class="btn btn-outline-accent m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air"> <i class="la la-eye"></i></a>
                                </div>
                           </li>`;
                $(`.files_list`).append(`${html}`);
            }
        }

        function appendCurrentFiles(certificate_id) {
            let url = "{{ route('Business::gln@getFilesById')}}";
            let token = $("input[name='_token']").val();
            $.ajax({
                url: url,
                method: 'POST',
                data: {
                    certificate_id: certificate_id,
                    _token: token
                },
                success: function (data) {
                    if (data) {
                        let file = JSON.parse(data);
                        $(`.files_list`).html('');
                        appendData(file);
                    }
                }
            });
        }
    </script>
    <script type="text/javascript">
        var gln_status = 1;
        var businessCode_status = 1;

        function validateForm() {
            if (gln_status !== 1 || businessCode_status !== 1) {
                alert('Thông tin chưa hợp lệ!');
                return false;
            }
            return true;
        }

        function checkGLN(value) {
            var url = "{{ route('Business::gln@checkGLN')}}";
//            console.log(value);
            var formatError_message = `<div class="help-block">Mã GLN không hợp lệ *</div>`;
            var uniqueError_message = `<div class="help-block">Mã GLN đã tồn tại *</div>`;
            var token = $("input[name='_token']").val();
            $.ajax({
                url: url,
                method: 'POST',
                data: {
                    gln: value,
                    _token: token
                },
                success: function (data) {
                    let result = parseInt(data);
                    if (result === -1) {
                        $(`.gln_input`).html("");
                        $(`.gln_input`).append(`${formatError_message}`);
                        gln_status = 2;
                        // console.log(validate_status);
                    } else if (result === -2) {
                        $(`.gln_input`).html("");
                        $(`.gln_input`).append(`${uniqueError_message}`);
                        gln_status = 2;
                    }
                    else {
                        $(`.gln_input`).html("");
                        gln_status = 1;
                    }
                }
            });
        }

        function checkBusinessCode(value) {
            var url = "{{ route('Business::gln@checkBusinessCode')}}";
            var uniqueError_message = `<div class="help-block">Mã doanh nghiệp đã tồn tại *</div>`;
            var token = $("input[name='_token']").val();
            $.ajax({
                url: url,
                method: 'POST',
                data: {
                    business_code: value,
                    _token: token
                },
                success: function (data) {
                    let result = parseInt(data);
                    if (result === -1) {
                        $(`.businessCode_input`).html("");
                        $(`.businessCode_input`).append(`${uniqueError_message}`);
                        businessCode_status = 2;
                    }
                    else {
                        $(`.businessCode_input`).html("");
                        businessCode_status = 1;
                    }
                }
            });
        }

        //Search GLN existed in database
        $("input[name='gln']").change(function(){
          var gln = $(this).val();
          var url = "{{ route('Business::gln@getGLNByGLNCode')}}";
          var token = $("input[name='_token']").val();
          $.ajax({
            url: url,
            method: 'POST',
            data: {
              gln: gln,
              _token: token
            },
            success: function (data) {
              if(data) {
                if(data.address) {
                  $("input[name='address']").val(data.address);
                }
                if(data.business_code) {
                  $("input[name='business_code']").val(data.business_code);
                }
                if(data.name) {
                  $("input[name='name']").val(data.name);
                }
                if(data.phone_number) {
                  $("input[name='phone_number']").val(data.phone_number);
                }
                if(data.email) {
                  $("input[name='email']").val(data.email);
                }
                if(data.fax) {
                  $("input[name='fax']").val(data.fax);
                }
                if(data.website) {
                  $("input[name='website']").val(data.website);
                }
                if(data.country) {
                  $("select[name='country']").val(data.country).change();
                }
              }
            }
          });
        });






        {{--//Upload files--}}
        {{--$(".upload").change(function () {--}}
            {{--let spinner = `<div class="spinner m-loader m-loader--primary" style="width: 30px; display: inline-block;">--}}
                            {{--</div>`;--}}
            {{--let x = document.getElementById("file");--}}
            {{--$(".uploadFiles_list").append(`${spinner}`);--}}
            {{--var files = x.files;--}}
            {{--var formData = new FormData();--}}
            {{--// Loop through each of the selected files.--}}
            {{--for (let i = 0; i < files.length; i++) {--}}
                {{--let file = files[i];--}}
                {{--// Check the file type.--}}
{{--//                if (!file.type.match('image.*')) {--}}
{{--//                    continue;--}}
{{--//                }--}}

                {{--// Add the file to the request.--}}
                {{--formData.append('files[]', file, file.name);--}}

            {{--}--}}

            {{--var url = "{{ route('Business::upload')}}";--}}
{{--//            var token = $("input[name='_token']").val();--}}
            {{--$.ajax({--}}
                {{--url: url,--}}
                {{--method: 'POST',--}}
                {{--data: formData,--}}
                {{--cache: false,--}}
                {{--contentType: false,--}}
                {{--processData: false,--}}
                {{--success: function (data) {--}}
                    {{--$("div").remove(".spinner");--}}
                    {{--$.each(data, function (key, value) {--}}
                        {{--let file = JSON.parse(value);--}}
                        {{--appendData(file);--}}
                    {{--});--}}
                {{--}--}}
            {{--});--}}
        {{--});--}}
        {{--var removeItem = function (e) {--}}
            {{--e.closest('li').remove();--}}
        {{--};--}}

        {{--function appendData(file) {--}}
            {{--for (let i = 0; i < file.length; i++) {--}}
                {{--let file_name = file[i][0].split("_").pop();--}}
                {{--let html = `<li>--}}
                                {{--<div style="text-align: left;display: inline-block;width:70%">${file_name}</div>--}}
                                {{--<input type="hidden" name="uploaded_files[]" value="${file[i][0]}">--}}
                                {{--<div style="text-align: right;display: inline-block;float: right;">--}}
                                    {{--<a href="${file[i][1]}" target="_blank" data-toggle="tooltip" title="Xem" class="btn btn-outline-accent m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air"> <i class="la la-eye"></i></a>--}}
                                    {{--<a href="javascript:void(0);" onclick="return removeItem($(this));" data-toggle="tooltip" title="Xóa" class="btn btn-outline-accent m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air"> <i class="la la-remove"></i></a>--}}
                                {{--</div>--}}
                           {{--</li>`;--}}
                {{--$(".uploadFiles_list").append(`${html}`);--}}
            {{--}--}}
        {{--}--}}

        //        $("#modal_submit_button").click(function() {
        //            $("#modal_form").submit(function (event) {
        //                alert("Handler for .submit() called.");
        //                event.preventDefault();
        //            });
        //        });
    </script>

@endpush
