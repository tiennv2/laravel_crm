@extends('qrcode.layouts.app')
@push('styles')
  <style>
    .heroes {
      margin: 0 0 2em 0;
      padding: 0;
      width: 30.5em;
    }

    .heroes li {
      position: relative;
      left: 0;
      color: #00c5dc;
      margin: .5em;
      padding: .3em .3em;
      height: auto;
      min-height: 3em;
      border-radius: 4px;
      border-color: #00c5dc;
      border-width: 2px;
      border-style: solid;
    }
    .select2 {
      width: 100% !important;
    }
  </style>
@endpush
@section('body_class', 'm-page--fluid m-page--loading-enabled m-page--loading m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default')

@push('scripts')
  <script type="text/javascript">
    <?php
      $sharedData = [
        'config' => [
          'csrfToken' => csrf_token(),
          'logoutUrl' => route('logout'),
          'isNative' => true,
        ],
        'loggedInAccount' => auth()->check() ? auth()->user() : null,
      ];
      ?>
      window.SHARED_DATA = {!! json_encode($sharedData) !!}
  </script>
  <script src="https://cdn.jsdelivr.net/npm/tinymce@4.7.11/tinymce.min.js" type="text/javascript"></script>
  <script src="{{ mix('js/app.js') }}"></script>
@endpush

@section('content')
  <!-- begin::Page loader -->
  <div class="m-page-loader m-page-loader--base">
    <div class="m-blockui">
      <span>
        Please wait...
      </span>
      <span>
        <div class="m-loader m-loader--brand"></div>
      </span>
    </div>
  </div>
  <!-- end::page Loader -->
  <div id="app" class="m-grid m-grid--hor m-grid--root m-page">
    <app-header></app-header>
    <div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop  m-body m-page__container">
      <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader">
          <div class="d-flex align-items-center">
            <div class="mr-auto">
              <h3 class="m-subheader__title">
                QUẢN LÝ DANH SÁCH MÃ GLN
              </h3>
            </div>
            @can("GATEWAY-addGLN")
              <div style="float: right"><a href="{{route('Business::gln@add')}}"
                                           class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air"><span>
                                    <i class="la la-plus-circle"></i>
                                    <span>Thêm mới GLN</span></span></a>
              </div>
            @endcan
          </div>
        </div>
        <!-- END: Subheader -->
        @can("GATEWAY-viewGLN")
          <div class="m-content">
            <!--Begin::Section-->
            <div class="m-portlet m-portlet--mobile">
              <div class="m-portlet__body">
                <form class="m-form m-form--fit m--margin-bottom-20"
                      action="{{route('Business::gln@index')}}" method="get">
                  <div class="row m--margin-bottom-20">
                    <div class="col-lg-4 m--margin-bottom-10-tablet-and-mobile">
                      <label>
                        Tìm kiếm theo doanh nghiệp:
                      </label>
                      <select class="form-control m-input" data-col-index="7" id="business_id"
                              name="business_id">
                        <option value="" selected="selected"></option>
                        @foreach($businesses as $business)
                          <option value="{{$business->id}}"
                            {{Request::input('business_id')==$business->id?'selected="selected"':''}}
                          >{{$business->name}}</option>
                        @endforeach
                      </select>
                    </div>
                    <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile">
                      <label>
                        Tên gln:
                      </label>
                      <input type="text" class="form-control m-input" name="name"
                             value="{{ Request::input('name') }}"
                             data-col-index="0">
                    </div>
                    <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile">
                      <label>
                        Mã doanh nghiệp:
                      </label>
                      <input type="text" class="form-control m-input" name="business_code"
                             value="{{ Request::input('business_code') }}"
                             data-col-index="4">
                    </div>
                    <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile">
                      <label>
                        Mã gln:
                      </label>
                      <input type="text" class="form-control m-input" name="gln_code"
                             value="{{ Request::input('gln_code') }}"
                             data-col-index="4">
                    </div>
                    <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile">
                      <label>
                        Chọn trạng thái:
                      </label>
                      <select class="form-control m-input" data-col-index="7" id="gln_status"
                              name="gln_status">
                        <option value="" selected="selected">--Tất cả--</option>
                        <option value="{{\App\Models\Business\Gln::STATUS_PENDING}}" {{Request::input('gln_status')==="0"?'selected="selected"':''}}>
                          Chờ duyệt
                        </option>
                        <option value="{{\App\Models\Business\Gln::STATUS_APPROVE}}" {{Request::input('gln_status')==1?'selected="selected"':''}}>
                          Đã duyệt
                        </option>
                        <option value="{{\App\Models\Business\Gln::STATUS_DISAPPROVE}}" {{Request::input('gln_status')==2?'selected="selected"':''}}>
                          Đã hủy duyệt
                        </option>
                      </select>
                    </div>
                  </div>
                  <div class="row m--margin-bottom-20">
                    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                      <label>
                        Tình trạng giấy tờ:
                      </label>
                      <select class="form-control m-input" data-col-index="7"
                              id="certificate_status"
                              name="certificate_status">
                        <option value="" selected="selected">--Tất cả--</option>
                        <option value="1" {{Request::input('certificate_status')==1?'selected="selected"':''}}>
                          Chưa có giấy tờ
                        </option>
                        <option value="2" {{Request::input('certificate_status')==2?'selected="selected"':''}}>
                          Có giấy tờ
                        </option>
                      </select>
                    </div>
                    <div class="col-lg-7 m--margin-bottom-10-tablet-and-mobile">
                      <label style="color: white">Hành động </label>
                      <div>
                        <button class="btn btn-brand m-btn m-btn--icon" type="submit">
												<span>
													<i class="la la-search"></i>
													<span>
														Tìm kiếm
													</span>
												</span>
                        </button>
                        &nbsp;&nbsp;
                        <button class="btn btn-secondary m-btn m-btn--icon" type="button"
                                onclick="window.location='{{route('Business::gln@index')}}'">
												<span>
													<i class="la la-refresh"></i>
													<span>
														Làm mới
													</span>
												</span>
                        </button>
                      </div>
                    </div>
                    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile" style="padding-top: 45px">
                      @if($approve_waiting_number)
                        <a href="{{route('Business::gln@index')}}?gln_status=0"><h5
                            style="color: blue;border: solid;text-align: center">Có {{$approve_waiting_number}} mã GLN
                            đang chờ duyệt</h5>
                        </a>
                      @endif
                    </div>
                  </div>
                </form>
                @if (session('success'))
                  <div
                    class="m-alert m-alert--outline m-alert--square m-alert--outline-2x alert alert-success alert-dismissible fade show"
                    role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    </button>
                    {{ session('success') }}
                  </div>
                @endif
                <div
                  style="width:200px;text-align:center;padding:5px;margin-bottom: 0;margin-top: 50px;margin-left: 50px;font-weight: bold;font-style: italic;border: solid 1px;border-radius: 6px">
                  Đang hiển thị {{$count_record_in_page}}/{{$count_record_total}} bản ghi
                </div>
                <div class="table-responsive">
                  <table class="table table-striped- table-bordered table-hover table-checkable"
                         style="margin-top: 20px;margin-left:auto;margin-right:auto;width: 1600px">
                    <!--begin::Thead-->
                    <thead>
                    <tr>
                      <th style="font-weight: bold">Id</th>
                      <th style="width: 300px;font-weight: bold">Tên mã gln</th>
                      <th style="width: 300px;font-weight: bold">Tên doanh nghiệp</th>
                      <th style="font-weight: bold;width: 200px">Mã doanh nghiệp</th>
                      <th style="font-weight: bold;width: 200px">Mã GLN</th>
                      <th style="font-weight: bold">Quốc gia</th>
                      <th style="font-weight: bold">Giấy tờ</th>
                      <th style="font-weight: bold;width: 150px">Trạng thái</th>
                      <th style="font-weight: bold">Ngày tạo</th>
                      <th style="font-weight: bold">Hành động</th>
                    </tr>
                    </thead>
                    <!--end::Thead-->
                    <!--begin::Tbody-->
                    <tbody>
                    @foreach($glns as $gln)
                      <tr>
                        <td>{{$gln->id}}</td>
                        <td>
                          <div
                            class="m-dropdown m-dropdown--up m-dropdown--inline m-dropdown--small m-dropdown--arrow m-dropdown--align-left"
                            m-dropdown-toggle="hover">
                            <a class="m-dropdown__toggle btn btn-silver dropdown-toggle">
                              {{$gln->name}}
                            </a>
                            <div class="m-dropdown__wrapper">
                              <div class="m-dropdown__inner">
                                <div class="m-dropdown__body" style="padding:10px; width: 350px">
                                  <div class="m-dropdown__content">
                                    <ul class="m-nav">
                                      @can("GATEWAY-viewProduct")
                                        <li class="m-nav__item"
                                            style="margin-bottom: 5px">
                                          <a href="{{route('Business::product@index')}}?gln_id={{$gln->id}}"
                                             target="_blank" class="m-nav__link">
                                            <span class="m-nav__link-text">Xem danh sách sản phẩm thuộc GLN</span>
                                          </a>
                                        </li>
                                      @endcan
                                      @can("GATEWAY-updateGLN")
                                        <li class="m-nav__item"
                                            style="margin-bottom: 5px">
                                          <a
                                            href="{{route('Business::gln@edit',[$gln->id])}}"
                                            target="_blank" class="m-nav__link">
                                            <span class="m-nav__link-text">Sửa thông tin GLN</span>
                                          </a>
                                        </li>
                                      @endcan
                                    </ul>
                                  </div>
                                </div>
                              </div>
                              <span class="m-dropdown__arrow m-dropdown__arrow--left"></span>
                            </div>
                          </div>
                        </td>
                        <td>
                          <a href="{{route('Business::businesses@index')}}?business_id={{$gln->business_id}}"
                             target="_blank">
                            {{$gln->business_name}}
                          </a>
                        </td>
                        <td>{{$gln->business_code}}</td>
                        <td>{{$gln->gln}}</td>
                        <td>{{$gln->country_name}}</td>
                        <td>
                          <button
                            type="button"
                            value="{{$gln->certificate_id}}"
                            data-target="#file{{$gln->id}}"
                            class="files_modal btn m-btn--pill m-btn--air btn-outline-info btn-sm"
                            id="upload_files_{{$gln->id}}"
                            data-toggle="modal">Chi tiết
                          </button>
                          <input type="hidden" id="{{$gln->id}}" value="{{$gln->business_id}}">
                          <!--Modal-->
                          <div id="file{{$gln->id}}" class="modal"
                               role="dialog">
                            <div class="modal-dialog">
                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h1 class="modal-title">Files giấy tờ của gln
                                    "{{$gln->gln}}"</h1>
                                  <button type="button" class="close"
                                          data-dismiss="modal">&times;
                                  </button>
                                </div>
                                <form method="POST"
                                      action="{{ route('Business::gln@updateFile')}}">
                                  <div class="modal-body">
                                    <input type="hidden" name="gln_id"
                                           value="{{$gln->id}}">
                                    <div class="upload_files_{{$gln->id}}_select">
                                    </div>
                                    <div>
                                      <ol class="heroes upload_files_{{$gln->id}}_input_list">

                                      </ol>
                                    </div>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="submit"
                                            class="btn btn-success">
                                      Cập nhật
                                    </button>
                                    <button type="button"
                                            class="btn btn-default"
                                            data-dismiss="modal">Hủy bỏ
                                    </button>
                                  </div>
                                </form>
                              </div>
                            </div>
                          </div>
                          <!--End Modal-->
                        </td>
                        <td>
                          @if($gln->status == \App\Models\Business\Gln::STATUS_PENDING)
                            <span class="m-badge m-badge--warning m-badge--wide">Chờ duyệt </span>
                          @elseif($gln->status== \App\Models\Business\Gln::STATUS_APPROVE)
                            <span class="m-badge m-badge--success m-badge--wide">Đã duyệt </span>
                          @else
                            <span class="m-badge m-badge--danger m-badge--wide"><a
                                data-toggle="modal"
                                data-target="#note{{$gln->id}}">Đã hủy duyệt </a></span>
                            <!--Modal update Note-->
                            <div id="note{{$gln->id}}" class="modal fade"
                                 role="dialog">
                              <div class="modal-dialog">
                                <!-- Modal content-->
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h3>Nguyên nhân hủy kích hoạt</h3>
                                    <button type="button" class="close"
                                            data-dismiss="modal">&times;
                                    </button>
                                  </div>
                                  <div class="modal-body">
                                    <div class="form-group m-form__group row">
                                                                        <textarea class="form-control" rows="5"
                                                                                  onchange="updateNote(this.value,{{$gln->id}})"
                                                                                  placeholder="Nhập nguyên nhân hủy kích hoạt GLN"
                                                                                  name="note">{{$gln->note}}</textarea>
                                    </div>
                                  </div>

                                  <div class="modal-footer">
                                    <button type="button"
                                            class="btn btn-default"
                                            data-dismiss="modal">Đóng
                                    </button>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <!--End Modal-->
                          @endif
                        </td>
                        <td>
                          {{date("d/m/Y",strtotime($gln->created_at))}}
                        </td>
                        <td>
                          @if($gln->status==0)
                            @can("GATEWAY-approveGLN")
                              <button
                                type="button"
                                onclick="window.location='{{route('Business::gln@approve',[$gln->id])}}'"
                                title="Duyệt"
                                @if(auth()->user()->cannot('GATEWAY-approveGLN'))
                                disabled
                                @endif
                                class="btn btn-success m-btn m-btn--icon m-btn--icon-only m-btn--pill">
                                <i class="la la-check"></i>
                              </button>
                            @endcan
                            {{--<a href="{{route('Business::gln@disapprove',[$gln->id])}}"--}}
                            {{--title="Từ chối"--}}
                            {{--class="btn btn-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill">--}}
                            {{--<i class="la la-close"></i>--}}
                            {{--</a>--}}
                            @can("GATEWAY-disapproveGLN")
                              <button
                                type="button"
                                @if(auth()->user()->cannot('GATEWAY-disapproveGLN'))
                                disabled
                                @endif
                                data-toggle="modal"
                                data-target="#disapprove{{$gln->id}}"
                                title="Hủy kích hoạt"
                                class="btn btn-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill">
                                <i class="la la-remove"></i>
                              </button>
                            @endcan
                          <!--Modal Comfirm disapprove-->
                            <div id="disapprove{{$gln->id}}" class="modal fade"
                                 role="dialog">
                              <div class="modal-dialog">
                                <!-- Modal content-->
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h3>Bạn chắc
                                      chắn muốn hủy kích hoạt GLN
                                      "{{$gln->name}}"?</h3>
                                    <button type="button" class="close"
                                            data-dismiss="modal">&times;
                                    </button>
                                  </div>
                                  <form method="post"
                                        action="{{route('Business::gln@disapprove',[$gln->id])}}">
                                    {{ csrf_field() }}
                                    <div class="modal-body">
                                      <div class="form-group m-form__group row">
                                                                                <textarea class="form-control" rows="5"
                                                                                          placeholder="Nhập nguyên nhân hủy kích hoạt GLN"
                                                                                          name="note"
                                                                                          required></textarea>
                                      </div>
                                    </div>
                                    <div class="modal-footer">
                                      <button type="submit"
                                              class="btn btn-success">
                                        Đồng ý
                                      </button>
                                      <button type="button"
                                              class="btn btn-default"
                                              data-dismiss="modal">Đóng
                                      </button>
                                    </div>
                                  </form>
                                </div>
                              </div>
                            </div>
                            <!--End Modal-->
                          @endif
                        </td>
                      </tr>
                    @endforeach
                    </tbody>
                    <!--end::Tbody-->
                  </table>
                </div>
                <!--end::Table-->
                <div style="float:right;"><?php echo $glns->links(); ?></div>
              </div>
              <!--End::Section-->
            </div>
          </div>
        @endcan
      </div>
    </div>
  </div>
@endsection
@push('scripts')
  <script type="text/javascript" src="{{ asset('qr_code/metronic/js/vendors.bundle.js') }}"></script>
  <script type="text/javascript">
    $(document).ready(function () {
      // Basic
      $("#gln_status").select2({
        placeholder: 'Chọn trạng thái',
        allowClear: true
      });
      $("#business_id").select2({
        placeholder: 'Chọn doanh nghiệp',
        allowClear: true
      });
      $("#certificate_status").select2({
        placeholder: 'Chọn trạng thái',
        allowClear: true
      });
      $("select").on("select2:select", function (evt) {
        var element = evt.params.data.element;
        var $element = $(element);
        $element.detach();
        $(this).append($element);
        $(this).trigger("change");
      });
      $(".modal").on('hidden.bs.modal', function () {
        document.body.style.padding = 0;
      });
    });
    //Fill Data to Modal
    $(".files_modal").click(function () {
      let id = $(this).attr('id');
      let gln_id = id.split("_").pop();
      let business_id = $(`#${gln_id}`).val();
      let spinner = `<div class="spinner m-loader m-loader--primary" style="width: 30px; display: inline-block;">
                            </div>`;
      $(`.${id}_input_list`).empty();
      $(`.${id}_input_list`).append(`${spinner}`);
      $(`.${id}_select`).empty();
      let value = $(this).attr('value');
      let html = `
                <div class="select_box">
                 <select class="form-control m-input"
                 data-col-index="12"
                  id="business_id_${gln_id}"
                  name="select_id"
                  required>
                  <option value=""></option>
                </select>
                </div>
                `;
      if (value) {
        appendCurrentFiles(id, value);
      } else {
        $(`.${id}_input_list`).append("Chưa có giấy tờ!");
      }
      $(`.${id}_select`).append(`${html}`);
      $(`#business_id_${gln_id}`).select2({
        placeholder: 'Thay đổi files giấy tờ bằng click chọn một trong các giấy tờ sau',
        allowClear: true,
        width: '30.5em'
      });
      appendSelectBox(business_id);
      $("div.select_box select").change(function () {
        $(`.${id}_input_list`).empty();
        let file = JSON.parse($(this).val());
        appendData(id, file);
        let certificate_id = $("div.select_box select option:selected").attr('data-key');
        $(`.${id}_input_list`).append(`<input type="hidden" name="certificate_id" value="${certificate_id}">`);
      });
    });

    function appendData(id, file) {
      for (let i = 0; i < file.length; i++) {
        let html = `<li>
                                <div style="text-align: left;display: inline-block;width: 70%">${file[i][0]}</div>
                                <div style="text-align: right;display: inline-block;float: right;">
                                    <a href="${file[i][2]}" target="_blank" data-toggle="tooltip" title="Xem" class="btn btn-outline-accent m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air"> <i class="la la-eye"></i></a>
                                </div>
                           </li>`;
        $(`.${id}_input_list`).append(`${html}`);
      }
    }

    function appendSelectBox(business_id) {
      let url = "{{ route('Business::certificate@certificatesByBusinessId')}}";
      let token = $("input[name='_token']").val();
      $.ajax({
        url: url,
        method: 'POST',
        data: {
          business_id: business_id,
          _token: token
        },
        success: function (data) {
          $.each(data, function (key, value) {
            $("select[name='select_id']").append(
              `<option value='${value.files}' data-key='${value.id}'>${value.name}</option>`
            );
          });
          $("div").remove(".spinner");
        }
      });
    }

    function appendCurrentFiles(id, certificate_id) {
      let url = "{{ route('Business::gln@getFilesById')}}";
      let token = $("input[name='_token']").val();
      $.ajax({
        url: url,
        method: 'POST',
        data: {
          certificate_id: certificate_id,
          _token: token
        },
        success: function (data) {
          if (data) {
            let file = JSON.parse(data);
            appendData(id, file);
          }
        }
      });
    }
  </script>
  <script type="text/javascript">
    function updateNote(value, id) {
      var url = "{{ route('Business::gln@updateNote')}}";
      var token = $("input[name='_token']").val();
      $.ajax({
        url: url,
        method: 'POST',
        data: {
          note: value,
          gln_id: id,
          _token: token
        },
        success: function (data) {
          console.log(data);
        }
      });
    }
  </script>
@endpush
