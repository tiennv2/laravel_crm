<table>
  <thead>
  <tr>
    <th><h5>ID</h5></th>
    <th>Doanh nghiệp</th>
    <th>Tên gói đăng ký</th>
    <th>Số lượng sản phẩm</th>
    <th>Đơn giá gói dịch vụ (VNĐ/năm)</th>
    <th>Giá trị theo Hợp đồng (VNĐ)</th>
    <th>Thời hạn gói</th>
    <th>Ngày bắt đầu Hợp đồng</th>
    <th>Ngày hết hạn</th>
    <th>Trạng thái</th>
    <th>Người phụ trách</th>
    <th>Tình trạng thanh toán</th>
    <th>Ảnh</th>
  </tr>
  </thead>
  <tbody>
  @foreach($subscriptions as $subscription)
    <tr>
      <td>{{ $subscription['ID'] }}</td>
      <td>{{ $subscription['Tên Doanh nghiệp'] }}</td>
      <td>{{ $subscription['Tên gói đăng ký'] }}</td>
      <td>{{ $subscription['Số lượng sản phẩm'] }}</td>
      <td>{{ $subscription['Đơn giá gói dịch vụ (VNĐ/năm)'] }}</td>
      <td>{{ $subscription['Giá trị theo Hợp đồng (VNĐ)'] }}</td>
      <td>{{ $subscription['Thời hạn gói'] }}</td>
      {{--<td>@if($subscription->days >=365)--}}
          {{--{{number_format($subscription->days/365,0,".",",")}} năm--}}
        {{--@else--}}
          {{--{{number_format($subscription->days,0,".",",")}} ngày--}}
        {{--@endif</td>--}}
      {{--<td>{{ date("d/m/Y",strtotime($subscription->start_time)) }}</td>--}}
      <td>{{ $subscription['Ngày bắt đầu Hợp đồng'] }}</td>
      <td>{{ $subscription['Ngày hết hạn'] }}</td>
      {{--<td>{{ date("d/m/Y",strtotime($subscription->expires_on)) }}</td>--}}
      {{--<td>{{ $subscription->expire_type }}</td>--}}
      <td>{{ $subscription['Trạng thái'] }}</td>
      <td>{{ $subscription['Người phụ trách'] }}</td>
      <td>{{ $subscription['Tình trạng thanh toán'] }}</td>
      {{--<td><img src="https://storage.googleapis.com/test.business-content.icheck.vn/businesses/9820cbac72/product-images/8999999057442/1546569184_wfOf7TXQq0_XWs0Ylo2VosIpS5xJO7c9oTVpMTU8zHCbYAeXUoy.jpeg" width="100px"></td>--}}
      <td>{{'https://storage.googleapis.com/test.business-content.icheck.vn/businesses/9820cbac72/product-images/8999999057442/1546569184_wfOf7TXQq0_XWs0Ylo2VosIpS5xJO7c9oTVpMTU8zHCbYAeXUoy.jpeg'}}</td>
    </tr>
  @endforeach
  </tbody>
</table>
