@extends('qrcode.layouts.app')
@push('styles')
  <style>
    .heroes {
      margin: 0 0 2em 0;
      padding: 0;
      width: 30em;
    }

    .heroes li {
      position: relative;
      left: 0;
      color: #00c5dc;
      margin: .5em;
      padding: .3em .3em;
      height: auto;
      min-height: 3em;
      border-radius: 4px;
      border-color: #00c5dc;
      border-width: 2px;
      border-style: solid;
    }
  </style>
@endpush
@section('body_class', 'm-page--fluid m-page--loading-enabled m-page--loading m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default')
@push('scripts')
  <script type="text/javascript">
    <?php
      $sharedData = [
        'config' => [
          'csrfToken' => csrf_token(),
          'logoutUrl' => route('logout'),
          'isNative' => true,
        ],
        'loggedInAccount' => auth()->check() ? auth()->user() : null,
      ];
      ?>
      window.SHARED_DATA = {!! json_encode($sharedData) !!}
  </script>
  <script src="https://cdn.jsdelivr.net/npm/tinymce@4.7.11/tinymce.min.js" type="text/javascript"></script>
  <script src="{{ mix('js/app.js') }}"></script>
@endpush

@section('content')
  <!-- begin::Page loader -->
  <div class="m-page-loader m-page-loader--base">
    <div class="m-blockui">
      <span>
        Please wait...
      </span>
      <span>
        <div class="m-loader m-loader--brand"></div>
      </span>
    </div>
  </div>
  <!-- end::Page Loader -->
  <div id="app" class="m-grid m-grid--hor m-grid--root m-page">
    <app-header></app-header>
    <div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop  m-body m-page__container">
      <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-content">
          <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__body">
              <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                  {{--<div class="m-portlet__head-title">--}}
                  {{--<h3 class="m-portlet__head-text">--}}
                  {{--<a href="{{route('Business::productDistributor@index')}}">--}}
                  {{--<i class="la la-arrow-left"></i>--}}
                  {{--</a>--}}
                  {{--</h3>--}}
                  {{--</div>--}}
                </div>
              </div>
              <!-- /page header -->
              @if (session('success'))
                <div
                  class="m-alert m-alert--outline m-alert--square m-alert--outline-2x alert alert-success alert-dismissible fade show"
                  role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  </button>
                  {{ session('success') }}
                </div>
              @endif
              <form method="POST" enctype="multipart/form-data" name="form"
                    class="m-form m-form--fit m-form--label-align-right"
                    action="{{route('Business::productDistributor@store') }}"
              >
                {{ csrf_field() }}
                {{--<input type="hidden" name="business_id" value="{{$business_id}}">--}}
                <div class="table-responsive">
                  <table class="table table-striped- table-bordered table-hover table-checkable"
                         style="margin-top: 20px;margin-left:auto;margin-right:auto;width: 1600px">
                    <!--begin::Thead-->
                    <thead>
                    <tr>
                      <th style="width: 300px;font-weight: bold">Sản phẩm</th>
                      <th style="font-weight: bold;width: 300px">Nhà phân phối</th>
                      <th style="font-weight: bold;width: 50px">Files</th>
                      <th style="font-weight: bold;width: 150px">Tiêu đề</th>
                      <th style="font-weight: bold;width: 100px">Hành động</th>
                    </tr>
                    </thead>
                    <!--end::Thead-->
                    <!--begin::Tbody-->
                    <tbody>
                    @foreach($productDistributors as $row)
                      <tr>
                        <td>
                          {{$row['product_name']}} ({{$row['product_barcode']}})
                          <input type="hidden" name="product_id[]"
                                 value="{{$row['product_id']}}">
                        </td>
                        <td>
                          {{$row['distributor_name']}}
                          <input type="hidden" name="distributor_id[]"
                                 value="{{$row['distributor_id']}}">
                        </td>
                        <td>
                          <button
                            type="button"
                            {{--value="{{$certificate_files}}"--}}
                            data-target="#file_{{$row['product_id']}}{{$row['distributor_id']}}"
                            class="certificate_modal btn m-btn--pill m-btn--air btn-outline-info btn-sm"
                            id="upload_files_{{$row['product_id']}}_{{$row['distributor_id']}}"
                            data-toggle="modal">Chi tiết
                          </button>
                          <!--Modal-->
                          <div id="file_{{$row['product_id']}}{{$row['distributor_id']}}"
                               class="modal fade"
                               role="dialog">
                            <div class="modal-dialog">
                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h1 class="modal-title">Files giấy tờ</h1>
                                  <button type="button" class="close"
                                          data-dismiss="modal">&times;
                                  </button>
                                </div>
                                <div class="modal-body">
                                  <div>
                                    <ol
                                      class="heroes upload_files_{{$row['product_id']}}_{{$row['distributor_id']}}_input_list">
                                    </ol>
                                  </div>
                                  <div style="text-align: center">
                                    <label for="upload_files_{{$row['product_id']}}_{{$row['distributor_id']}}_input"
                                           style="cursor: pointer;padding:8px;background-color: #0aa7ef;color: white;border-radius: 20px"><i
                                        class="la la-cloud-upload"></i>
                                      Chọn
                                      tập tin</label>
                                    <input type="file" name="files[]"
                                           id="upload_files_{{$row['product_id']}}_{{$row['distributor_id']}}_input"
                                           class="upload"
                                           style="display: none!important;"
                                           accept=".png,.jpg,.jpeg,.gif,.pdf,.doc,.docx"
                                           multiple>
                                  </div>
                                </div>
                                <div class="modal-footer">
                                  <button type="button"
                                          class="btn btn-default"
                                          data-dismiss="modal"> Đóng
                                  </button>
                                </div>
                              </div>
                            </div>
                          </div>
                          <!--End Modal-->
                        </td>
                        <td>
                          <select class="form-control" id="title_id"
                                  name="title_id[]"
                                  required>
                            <option value="">-Chọn tiêu đề-</option>
                            @foreach($distributor_titles as $distributor_title)
                              <option value="{{$distributor_title->id}}"
                                      @if( $row['default_title_id'] == $distributor_title->id) selected="selected" @endif
                              >{{$distributor_title->title}}</option>
                            @endforeach
                          </select>
                        </td>
                        <td>
                          <button
                            title="Xóa bỏ"
                            type="button"
                            class="delete btn btn-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill">
                            <i class="fa fa-trash-o"></i>
                          </button>
                        </td>
                      </tr>
                    @endforeach
                    </tbody>
                    <!--end::Tbody-->
                  </table>
                </div>
                @if(count($productDistributors) != 0)
                  <div class="m-portlet__foot m-portlet__foot--fit">
                    <div class="m-form__actions m-form__actions">
                      <div class="row">
                        <div class="col-lg-9 ml-lg-auto">
                          <button type="submit" class="btn btn-success">
                            Thêm mới
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                  @else
                  <div class="m-portlet__foot m-portlet__foot--fit">
                    <div class="m-form__actions m-form__actions">
                      <div class="row">
                        <div class="col-lg-9 ml-lg-auto">
                          <span style="color: red">Không tìm thấy Sản phẩm nào trên hệ thống hoặc Sản phẩm không thuộc Doanh nghiệp được chọn.</span>
                          <p><a href="{{route('Business::productDistributor@index')}}">
                      Quay về trang Danh sách Nhà phân phối Sản phẩm</a></p>
                        </div>
                      </div>
                    </div>
                  </div>
                @endif
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
@push('scripts')
  <script type="text/javascript" src="{{ asset('qr_code/metronic/js/vendors.bundle.js') }}"></script>
  <script type="text/javascript">
    $(document).ready(function () {
      // Basic
      $(".distributor_title").select2({
        placeholder: 'Chọn tiêu đề',
        allowClear: true
      });
      $("select").on("select2:select", function (evt) {
        var element = evt.params.data.element;
        var $element = $(element);
        $element.detach();
        $(this).append($element);
        $(this).trigger("change");
      });
    });

    //Fill Data to Modal
    // $(".certificate_modal").click(function () {
    //   let id = $(this).attr('id');
    //   $(`.${id}_input_list`).empty();
    //   let value = $(this).attr('value');
    //   let file = JSON.parse(value);
    //
    //   appendData(id, file, id);
    // });

    //Delete product-distributor
    $(".delete").click(function () {
      var delete_button = $(this);    // Find the row
      delete_button.closest("tr").remove();

    });
    //Upload files
    $(".upload").change(function () {
      let id = $(this).attr('id');
      var upload_index = id.replace("_input", "");
      let x = document.getElementById(id);
      let spinner = `<div class="spinner m-loader m-loader--primary" style="width: 30px; display: inline-block;">
                            </div>`;
      $(`.${id}_list`).append(`${spinner}`);
      var files = x.files;
      var formData = new FormData();
      // Loop through each of the selected files.
      for (let i = 0; i < files.length; i++) {
        let file = files[i];
        // Check the file type.
//                if (!file.type.match('image.*')) {
//                    continue;
//                }

        // Add the file to the request.
        formData.append('files[]', file, file.name);

      }

      var url = "{{ route('Business::upload')}}";
//            var token = $("input[name='_token']").val();
      $.ajax({
        url: url,
        method: 'POST',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {
          $("div").remove(".spinner");
          append_id = id.replace("_input", "");
          $.each(data, function (key, value) {
            let file = JSON.parse(value);
            console.log(file);
            appendData(append_id, file, upload_index);
          });
        }
      });
    });
    var removeItem = function (e) {
      e.closest('li').remove();
    };

    function appendData(id, file, upload_index) {
      console.log(id);
      for (let i = 0; i < file.length; i++) {
        let file_name = file[i][0].split("_").pop();
        let html = `<li>
                                <div style="text-align: left;display: inline-block;width: 70%">${file_name}</div>
                                <input type="hidden" name="${upload_index}[]" value="${file[i][0]}">
                                <div style="text-align: right;display: inline-block;float: right;">
                                    <a href="${file[i][1]}" target="_blank" data-toggle="tooltip" title="Xem" class="btn btn-outline-accent m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air"> <i class="la la-eye"></i></a>
                                    <a href="javascript:void(0);" onclick="return removeItem($(this));" data-toggle="tooltip" title="Xóa" class="btn btn-outline-accent m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air"> <i class="la la-remove"></i></a>
                                </div>
                           </li>`;
         $(`.${id}_input_list`).append(`${html}`);
      }
    }
  </script>
@endpush
