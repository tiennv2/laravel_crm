@extends('qrcode.layouts.app')
@section('body_class', 'm-page--fluid m-page--loading-enabled m-page--loading m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default')
@push('styles')
  <style>
    .heroes {
      margin: 0 0 2em 0;
      padding: 0;
      width: 30.5em;
    }

    .heroes li {
      position: relative;
      left: 0;
      color: #00c5dc;
      margin: .5em;
      padding: .3em .3em;
      height: auto;
      min-height: 3em;
      border-radius: 4px;
      border-color: #00c5dc;
      border-width: 2px;
      border-style: solid;
    }
    .select2 {
      width: 100% !important;
    }
  </style>
@endpush
@push('scripts')
  <script type="text/javascript">
    <?php
      $sharedData = [
        'config' => [
          'csrfToken' => csrf_token(),
          'logoutUrl' => route('logout'),
          'isNative' => true,
        ],
        'loggedInAccount' => auth()->check() ? auth()->user() : null,
      ];
      ?>
      window.SHARED_DATA = {!! json_encode($sharedData) !!}
  </script>
  <script src="https://cdn.jsdelivr.net/npm/tinymce@4.7.11/tinymce.min.js" type="text/javascript"></script>
  <script src="{{ mix('js/app.js') }}"></script>
@endpush

@section('content')
  <!-- begin::Page loader -->
  <div class="m-page-loader m-page-loader--base">
    <div class="m-blockui">
      <span>
        Please wait...
      </span>
      <span>
        <div class="m-loader m-loader--brand"></div>
      </span>
    </div>
  </div>
  <!-- end::page Loader -->
  <div id="app" class="m-grid m-grid--hor m-grid--root m-page">
    <app-header></app-header>
    <div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop  m-body m-page__container">
      <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader">
          <div class="d-flex align-items-center">
            <div class="mr-auto">
              <h3 class="m-subheader__title ">
                QUẢN LÝ NHÀ PHÂN PHỐI CỦA SẢN PHẨM
              </h3>
              @can("GATEWAY-viewDistributor")
                <a href="{{route('Business::distributor@index')}}" target="_blank"
                   class="btn btn-outline-brand m-btn btn-sm m-btn--icon m-btn--pill"><span>
                                    <span style="font-size: 14px">Xem danh sách Nhà phân phối</span></span></a>
              @endcan
            </div>
            @if($waiting_productDistributor_count > 0)
              <div style="border: solid #bf800c;padding: 3px; margin-right: 15px"><a
                  href="{{route('Business::productDistributor@index')}}?status=0"
                  style="color: #bf800c">Có <span id="waiting_count">{{$waiting_productDistributor_count}}</span> yêu
                  cầu duyệt Nhà phân phối cho sản phẩm</a></div>
            @endif
            @can("GATEWAY-addProductDistributor")
              <div style="float: right"><a href="{{route('Business::productDistributor@add')}}"
                                           class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air"><span>
                        <i class="la la-plus-circle"></i>
                        <span>Thêm Nhà phân phối</span></span></a>
              </div>
            @endcan
          </div>
        </div>
        <!-- END: Subheader -->
        @can("GATEWAY-viewProductDistributor")
          <div class="m-content">
            <!--Begin::Section-->
            <div class="m-portlet m-portlet--mobile">
              <div class="m-portlet__body">
                <form class="m-form m-form--fit m--margin-bottom-20"
                      action="{{route('Business::productDistributor@index')}}" method="get">
                  <div class="row m--margin-bottom-20">
                    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                      <label>
                        Tìm kiếm theo doanh nghiệp:
                      </label>
                      <select class="form-control m-input" data-col-index="7" id="business_id"
                              name="business_id">
                        <option value="" selected="selected"></option>
                        @foreach($businesses as $business)
                          <option value="{{$business->id}}"
                            {{Request::input('business_id')==$business->id?'selected="selected"':''}}
                          >{{$business->name}}</option>
                        @endforeach
                      </select>
                    </div>
                    <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile">
                      <label>
                        Tên sản phẩm:
                      </label>
                      <input type="text" class="form-control m-input" name="product_name"
                             value="{{ Request::input('product_name') }}"
                             data-col-index="0">
                    </div>
                    <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile">
                      <label>
                        Barcode sản phẩm:
                      </label>
                      <input type="text" class="form-control m-input" name="product_barcode"
                             value="{{ Request::input('product_barcode') }}"
                             data-col-index="0">
                    </div>
                    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                      <label>
                        Tên Nhà phân phối:
                      </label>
                      <input type="text" class="form-control m-input" name="distributor_name"
                             value="{{ Request::input('distributor_name') }}"
                             data-col-index="4">
                    </div>
                    <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile">
                      <label>
                        Chọn trạng thái:
                      </label>
                      <select class="form-control m-input" data-col-index="7" id="status"
                              name="status">
                        <option value="" selected="selected"></option>
                        <option value="0" {{Request::input('status')=="0"?'selected="selected"':''}}>
                          Chờ duyệt
                        </option>
                        <option value="1" {{Request::input('status')==1?'selected="selected"':''}}>
                          Đã duyệt
                        </option>
                        <option value="2" {{Request::input('status')==2?'selected="selected"':''}}>
                          Đã hủy duyệt
                        </option>
                      </select>
                    </div>
                  </div>
                  <div class="row m--margin-bottom-20">
                    <div class="col-lg-4 m--margin-bottom-10-tablet-and-mobile">
                      <label>
                        Ngày tạo:
                      </label>
                      <div class="input-daterange input-group">
                        <input type="text" class="form-control" name="from" id="m_datepicker_1"
                               value="{{ Request::input('from') }}" autocomplete="off"
                               placeholder="Từ"
                               data-col-index="5"/>
                        <div class="input-group-append">
													<span class="input-group-text">
														<i class="la la-ellipsis-h"></i>
													</span>
                        </div>
                        <input type="text" class="form-control" name="to" id="m_datepicker_1"
                               value="{{ Request::input('to') }}" autocomplete="off"
                               placeholder="Đến"
                               data-col-index="5"/>
                      </div>
                    </div>
                    <div class="col-lg-4 m--margin-bottom-10-tablet-and-mobile">
                      <label style="color: white">
                        Hành động
                      </label>
                      <div>
                        <button class="btn btn-brand m-btn m-btn--icon" type="submit">
												<span>
													<i class="la la-search"></i>
													<span>
														Tìm kiếm
													</span>
												</span>
                        </button>
                        &nbsp;&nbsp;
                        <button class="btn btn-secondary m-btn m-btn--icon" type="button"
                                onclick="window.location='{{route('Business::productDistributor@index')}}'">
												<span>
													<i class="la la-refresh"></i>
													<span>
														Làm mới
													</span>
												</span>
                        </button>
                      </div>
                    </div>
                  </div>
                </form>

                @if (session('success'))
                  <div
                    class="m-alert m-alert--outline m-alert--square m-alert--outline-2x alert alert-success alert-dismissible fade show"
                    role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    </button>
                    {{ session('success') }}
                  </div>
                @endif
                <div
                  style="width:200px;text-align:center;padding:5px;margin-bottom: 0;margin-top: 50px;margin-left: 50px;font-weight: bold;font-style: italic;border: solid 1px;border-radius: 6px">
                  Đang hiển thị {{$count_record_in_page}}/{{$count_record_total}} bản ghi
                </div>
                <div class="table-responsive">
                  <table class="table table-striped- table-bordered table-hover table-checkable"
                         style="margin-top: 20px;margin-left:auto;margin-right:auto;width: 1800px">
                    <!--begin::Thead-->
                    <thead>
                    <tr>
                      <th style="width: 50px;font-weight: bold">Id</th>
                      <th style="width: 300px;font-weight: bold">Tên Doanh nghiệp</th>
                      <th style="font-weight: bold;width: 300px">Tên Sản phẩm</th>
                      <th style="font-weight: bold;width: 100px">Mã Barcode</th>
                      <th style="width: 300px;font-weight: bold">Tên Nhà phân phối</th>
                      <th style="font-weight: bold;width: 300px">Tiêu đề</th>
                      <th style="font-weight: bold">Files</th>
                      <th style="width: 150px;font-weight: bold">Trạng thái</th>
                      <th style="font-weight: bold;width: 100px">Ngày tạo</th>
                      <th style="font-weight: bold;width: 180px">Hành động</th>
                    </tr>
                    </thead>
                    <!--end::Thead-->
                    <!--begin::Tbody-->
                    <tbody>
                    @foreach($productDistributors as $productDistributor)
                      <tr>
                        <td class="id">{{$productDistributor->id}}</td>
                        <td>
                          <a
                            href="{{route('Business::businesses@index')}}?business_id={{$productDistributor->business_id}}"
                            target="_blank">
                            {{$productDistributor->business_name}}
                          </a>
                        </td>
                        <td>{{$productDistributor->product_name}}</td>
                        <td>{{$productDistributor->product_barcode}}</td>
                        <td>{{$productDistributor->distributor_name}}</td>
                        <td>{{$productDistributor->title_name}}</td>
                        <td>
                          <button
                            type="button"
                            @cannot('GATEWAY-viewProductDistributorFiles')
                            disabled
                            @endcannot
                            value="{{$productDistributor->files}}"
                            data-target="#file{{$productDistributor->id}}"
                            class="file_modal btn m-btn--pill m-btn--air btn-outline-info btn-sm"
                            id="upload_files{{$productDistributor->id}}"
                            data-toggle="modal">Chi tiết
                          </button>
                          <!--Modal-->
                          <div id="file{{$productDistributor->id}}" class="modal fade"
                               role="dialog">
                            <div class="modal-dialog">
                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h1 class="modal-title">Files hồ sơ</h1>
                                  <button type="button" class="close"
                                          data-dismiss="modal">&times;
                                  </button>
                                </div>
                                <form method="POST"
                                      action="{{ route('Business::productDistributor@updateFiles')}}">
                                  <div class="modal-body">
                                    <input type="hidden"
                                           name="productDistributor_id"
                                           value="{{$productDistributor->id}}">
                                    <div>
                                      <ol class="heroes upload_files{{$productDistributor->id}}_input_list">

                                      </ol>
                                    </div>
                                    @can('GATEWAY-updateProductDistributorFiles')
                                      <div style="text-align: center">
                                        <label for="upload_files{{$productDistributor->id}}_input"
                                               style="cursor: pointer;padding:8px;background-color: #0aa7ef;color: white;border-radius: 20px"><i
                                            class="la la-cloud-upload"></i>
                                          Chọn
                                          tập tin</label>
                                        <input type="file" name="files[]"
                                               id="upload_files{{$productDistributor->id}}_input"
                                               class="upload"
                                               style="display: none!important;"
                                               accept=".png,.jpg,.jpeg,.gif,.pdf,.doc,.docx"
                                               multiple>
                                      </div>
                                    @endcan
                                  </div>
                                  <div class="modal-footer">
                                    @can('GATEWAY-updateProductDistributorFiles')
                                      <button type="submit"
                                              class="btn btn-success">
                                        Cập nhật
                                      </button>
                                    @endcan
                                    <button type="button"
                                            class="btn btn-default"
                                            data-dismiss="modal"> Đóng
                                    </button>
                                  </div>
                                </form>
                              </div>
                            </div>
                          </div>
                          <!--End Modal-->
                        </td>
                        <td>
                          @if($productDistributor->status==2)
                            <span class="status m-badge m-badge--danger m-badge--wide"><a
                                data-toggle="modal"
                                data-target="#note{{$productDistributor->id}}">Đã hủy duyệt </a></span>
                            <!--Modal update Note-->
                            <div id="note{{$productDistributor->id}}" class="modal fade"
                                 role="dialog">
                              <div class="modal-dialog">
                                <!-- Modal content-->
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h3>Nguyên nhân hủy duyệt Nhà phân
                                      phối {{$productDistributor->distributor_name}}</h3>
                                    <button type="button" class="close"
                                            data-dismiss="modal">&times;
                                    </button>
                                  </div>
                                  <div class="modal-body">
                                    <div class="form-group m-form__group row">
                                                                        <textarea class="form-control" rows="5" readonly
                                                                                  name="note">{{$productDistributor->note}}</textarea>
                                    </div>
                                  </div>

                                  <div class="modal-footer">
                                    <button type="button"
                                            class="btn btn-default"
                                            data-dismiss="modal">Đóng
                                    </button>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <!--End Modal-->
                          @elseif($productDistributor->status==1)
                            <span class="status m-badge m-badge--success m-badge--wide">Đã duyệt </span>
                          @else
                            <span class="status m-badge m-badge--warning m-badge--wide">Chờ duyệt </span>
                          @endif

                        </td>
                        <td>{{date("d/m/Y",strtotime($productDistributor->created_at))}}</td>
                        <td>
                          @can("GATEWAY-approveProductDistributor")
                            <button
                              {{--@if($productDistributor->status==1)--}}
                              {{--style="display: none;"--}}
                              {{--@endif--}}
                              title="Duyệt"
                              type="button"
                              class="approve btn btn-success m-btn m-btn--icon m-btn--icon-only m-btn--pill">
                              <i class="la la-check"></i>
                            </button>
                          @endcan
                          @can("GATEWAY-disapproveProductDistributor")
                            <button
                              @if($productDistributor->status==2)
                              style="display: none;"
                              @endif
                              type="button"
                              data-toggle="modal"
                              data-target="#disapprove{{$productDistributor->id}}"
                              title="Bỏ duyệt"
                              class="disapprove btn btn-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill">
                              <i class="la la-remove"></i>
                            </button>
                          @endcan
                          @can("GATEWAY-delProductDistributor")
                            <button
                              title="Xóa bỏ"
                              type="button"
                              class="delete btn btn-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill">
                              <i class="fa fa-trash-o"></i>
                            </button>
                        @endcan
                        <!--Modal Comfirm disapprove-->
                          <div id="disapprove{{$productDistributor->id}}" class="modal fade"
                               role="dialog">
                            <div class="modal-dialog">
                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h3>Bạn chắc
                                    chắn muốn hủy duyệt Nhà phân phối
                                    "{{$productDistributor->distributor_name}}
                                    "?</h3>
                                  <button type="button" class="close"
                                          data-dismiss="modal">&times;
                                  </button>
                                </div>
                                <form method="post"
                                      action="{{route('Business::productDistributor@disapprove',[$productDistributor->id])}}">
                                  {{ csrf_field() }}
                                  <div class="modal-body">
                                    <div class="form-group m-form__group row">
                                      <textarea class="form-control" rows="5"
                                                placeholder="Nhập nguyên nhân hủy duyệt Nhà phân phối"
                                                name="note"
                                                required></textarea>
                                    </div>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="submit"
                                            class="btn btn-success">
                                      Đồng ý
                                    </button>
                                    <button type="button"
                                            class="btn btn-default"
                                            data-dismiss="modal">Đóng
                                    </button>
                                  </div>
                                </form>
                              </div>
                            </div>
                          </div>
                        </td>
                      </tr>
                    @endforeach
                    </tbody>
                    <!--end::Tbody-->
                  </table>
                </div>
                <!--end::Table-->
                <div style="float:right;"><?php echo $productDistributors->links(); ?></div>
              </div>
              <!--End::Section-->
            </div>
          </div>
        @endcan
      </div>
    </div>
  </div>
@endsection
@push('scripts')
  <script type="text/javascript" src="{{ asset('qr_code/metronic/js/vendors.bundle.js') }}"></script>
  <script type="text/javascript" src="{{ asset('qr_code/metronic/js/datepicker/bootstrap-datepicker.js') }}"></script>
  <script type="text/javascript" src="{{ asset('qr_code/metronic/js/datatables/datatables.bundle.js') }}"></script>
  <script type="text/javascript" src="{{ asset('qr_code/metronic/js/datatables/scrollable.js') }}"></script>
  <script type="text/javascript">
    $(document).ready(function () {
      // Basic
      $("#status").select2({
        placeholder: 'Trạng thái',
        allowClear: true
      });
      $("#business_id").select2({
        placeholder: 'Chọn Doanh nghiệp',
        allowClear: true
      });

      $("select").on("select2:select", function (evt) {
        var element = evt.params.data.element;
        var $element = $(element);
        $element.detach();
        $(this).append($element);
        $(this).trigger("change");
      });
      $(".modal").on('hidden.bs.modal', function () {
        document.body.style.padding = 0;
      });
    });

    function delItem() {
      let conf = confirm("Bạn chắc chắn muốn xoá Nhà phân phối của Sản phẩm này không?");
      return conf;
    }
  </script>
  <script>
    function checkSync(id) {
      var check_url = "{{ route('Business::productDistributor@checkSync',':id')}}";
      var checkResult = null;
      check_url = check_url.replace(':id', id);
      $.ajax({
        url: check_url,
        method: 'GET',
        async: false,
        success: function (data) {
          checkResult = parseInt(data);
        }
      });
      return checkResult;
    }

    //Approve product-distributor
    $(".approve").click(function () {
      var approve_button = $(this);    // Find the row
      var status = approve_button.closest("tr").find(".status"); // Find the text
      status.html('Đang duyệt');
      status.addClass("m-loader m-loader--primary m-loader--left");
      status.css('padding-left', '32px');
      var id = approve_button.closest("tr").find(".id").text();
      var approve_url = "{{ route('Business::productDistributor@approve',':id')}}";
      approve_url = approve_url.replace(':id', id);
      $.ajax({
        url: approve_url,
        method: 'GET',
        async: false,
      });

      var interval_obj = setInterval(function () {
        let checkResult = checkSync(id);
        if (checkResult === 1) {
          clearInterval(interval_obj);
          status.removeClass();
          status.addClass("status m-badge m-badge--success m-badge--wide");
          status.css('padding-left', '10px');
          status.html('Đã duyệt');
          approve_button.css('display', 'none');
          $current_count = parseInt($("#waiting_count").text());
          $("#waiting_count").html($current_count - 1);
          let disapprove_button = approve_button.closest("td").find(".disapprove");
          disapprove_button.css('display', 'inline-block');
        }
      }, 2000);

    });

    //Delete product-distributor
    $(".delete").click(function () {
      let conf = delItem();
      if (conf !== true) {
        return false;
      }
      var delete_button = $(this);    // Find the row
      var id = delete_button.closest("tr").find(".id").text();
      var delete_url = "{{ route('Business::productDistributor@delete',':id')}}";
      delete_url = delete_url.replace(':id', id);
      $.ajax({
        url: delete_url,
        method: 'GET',
        async: false,
      });
      var interval_obj = setInterval(function () {
        let checkResult = checkSync(id);
        if (checkResult === -1) {
          delete_button.closest("tr").remove();
          clearInterval(interval_obj);
        }
      }, 1000);
    });
    //Fill Data to Modal
    $(".file_modal").click(function () {
      let id = $(this).attr('id');
      $(`.${id}_input_list`).empty();
      let value = $(this).attr('value');
      let file = JSON.parse(value);
      appendData(id, file);
    });
    //Upload files
    $(".upload").change(function () {
      let id = $(this).attr('id');
      let x = document.getElementById(id);
      let spinner = `<div class="spinner m-loader m-loader--primary" style="width: 30px; display: inline-block;">
                            </div>`;
      $(`.${id}_list`).append(`${spinner}`);
      var files = x.files;
      var formData = new FormData();
      // Loop through each of the selected files.
      for (let i = 0; i < files.length; i++) {
        let file = files[i];
        // Check the file type.

        // Add the file to the request.
        formData.append('files[]', file, file.name);

      }

      var url = "{{ route('Business::upload')}}";
      $.ajax({
        url: url,
        method: 'POST',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {
          $("div").remove(".spinner");
          append_id = id.replace("_input", "");
          $.each(data, function (key, value) {
            let file = JSON.parse(value);
            appendNewData(append_id, file);
          });
        }
      });
    });
    var removeItem = function (e) {
      e.closest('li').remove();
    };

    function appendData(id, file) {
      for (let i = 0; i < file.length; i++) {
        let file_name = file[i][0].split("/").pop();
        let html = `<li>
                                <div style="text-align: left;display: inline-block;width: 70%">${file_name}</div>
                                <input type="hidden" name="uploaded_files[]" value="${file[i][1]}">
                                <div style="text-align: right;display: inline-block;float: right;">
                                    <a href="${file[i][2]}" target="_blank" data-toggle="tooltip" title="Xem" class="btn btn-outline-accent m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air"> <i class="la la-eye"></i></a>
                                    <a href="javascript:void(0);" onclick="return removeItem($(this));" data-toggle="tooltip" title="Xóa" class="btn btn-outline-accent m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air"> <i class="la la-remove"></i></a>
                                </div>
                           </li>`;
        $(`.${id}_input_list`).append(`${html}`);
      }
    }

    function appendNewData(id, file) {
      for (let i = 0; i < file.length; i++) {
        let file_name = file[i][0].split("/").pop().split("_").pop();
        let html = `<li>
                                <div style="text-align: left;display: inline-block;width: 70%">${file_name}</div>
                                <input type="hidden" name="uploaded_files[]" value="${file[i][0]}">
                                <div style="text-align: right;display: inline-block;float: right;">
                                    <a href="${file[i][1]}" target="_blank" data-toggle="tooltip" title="Xem" class="btn btn-outline-accent m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air"> <i class="la la-eye"></i></a>
                                    <a href="javascript:void(0);" onclick="return removeItem($(this));" data-toggle="tooltip" title="Xóa" class="btn btn-outline-accent m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air"> <i class="la la-remove"></i></a>
                                </div>
                           </li>`;
        $(`.${id}_input_list`).append(`${html}`);
      }
    }
  </script>
@endpush
