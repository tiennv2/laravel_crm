@extends('qrcode.layouts.app')
@push('styles')
  <style>
    .help-block {
      color: red;
    }

    .heroes {
      margin: 0 0 2em 0;
      padding: 0;
      width: 30em;
    }

    .heroes li {
      position: relative;
      left: 0;
      color: #00c5dc;
      margin: .5em;
      padding: .3em .3em;
      height: auto;
      min-height: 3em;
      border-radius: 4px;
      border-color: #00c5dc;
      border-width: 2px;
      border-style: solid;
    }
  </style>
@endpush
@section('body_class', 'm-page--fluid m-page--loading-enabled m-page--loading m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default')
@push('scripts')
  <script type="text/javascript">
    <?php
      $sharedData = [
        'config' => [
          'csrfToken' => csrf_token(),
          'logoutUrl' => route('logout'),
          'isNative' => true,
        ],
        'loggedInAccount' => auth()->check() ? auth()->user() : null,
      ];
      ?>
      window.SHARED_DATA = {!! json_encode($sharedData) !!}
  </script>
  <script src="https://cdn.jsdelivr.net/npm/tinymce@4.7.11/tinymce.min.js" type="text/javascript"></script>
  <script src="{{ mix('js/app.js') }}"></script>
@endpush

@section('content')
  <!-- begin::Page loader -->
  <div class="m-page-loader m-page-loader--base">
    <div class="m-blockui">
      <span>
        Please wait...
      </span>
      <span>
        <div class="m-loader m-loader--brand"></div>
      </span>
    </div>
  </div>
  <!-- end::Page Loader -->
  <div id="app" class="m-grid m-grid--hor m-grid--root m-page">
    <app-header></app-header>
    <div
      class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop 	m-container m-container--responsive m-container--xxl m-page__container m-body">
      <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-content">
          <div class="m-portlet">
            <div class="m-portlet__head">
              <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                  <h3 class="m-portlet__head-text">
                    <a href="{{route('Business::productDistributor@index')}}">
                      <i class="la la-arrow-left"></i>
                    </a>
                  </h3>
                </div>
              </div>
            </div>
            <!-- /page header -->
            @if (session('success'))
              <div
                class="m-alert m-alert--outline m-alert--square m-alert--outline-2x alert alert-success alert-dismissible fade show"
                role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                </button>
                {{ session('success') }}
              </div>
            @endif
            <form method="POST" enctype="multipart/form-data" name="form" target="_blank"
                  class="m-form m-form--fit m-form--label-align-right"
                  action="{{route('Business::productDistributor@form_step1') }}"
            >
              {{ csrf_field() }}

              <div class="m-portlet__body">
                <!------------------ Business--------------->
                <div class="form-group m-form__group row">
                  <label class="col-form-label col-lg-2 col-sm-12">Chọn doanh nghiệp *</label>
                  <div class="col-lg-6 col-md-9 col-sm-12">
                    <select class="form-control m-select2" id="selected_business_id"
                            name="selected_business_id" required>
                      <option value=""></option>
                      @foreach($businesses as $business)
                        <option value="{{$business->id}}"
                                @if( old('selected_business_id') == $business->id) selected="selected" @endif
                        >{{$business->name}}</option>
                      @endforeach
                    </select>
                  </div>
                  @if ($errors->has('business_id'))
                    <div class="help-block">{{ $errors->first('business_id') }}</div>
                  @endif
                </div>
                <!------------------ Product--------------->
                <div class="form-group m-form__group row">
                  <label class="col-form-label col-lg-2 col-sm-12">Chọn sản phẩm *</label>
                  <div class="col-lg-6 col-md-9 col-sm-12">
                        <textarea name="barcode" rows="10" cols="74" placeholder="8935212500124
8923512011414" required>
                    </textarea>
                        <div style="color: silver">Mỗi barcode nằm trên 1 dòng</div>
                      @if ($errors->has('barcode'))
                        <div class="help-block">{{ $errors->first('barcode') }}</div>
                      @endif
                  </div>
                </div>
                <!------------------ Distributor--------------->
                <div class="form-group m-form__group row">
                  <label class="col-form-label col-lg-2 col-sm-12">Chọn Nhà phân phối *</label>
                  <div class="col-lg-6 col-md-9 col-sm-12">
                    <select class="form-control m-input" data-col-index="7" id="distributor_id"
                            name="distributor_id[]" multiple
                            required>
                    </select>
                  </div>
                  <div class="col-lg-3 col-md-9 col-sm-12">
                    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#create_distributor">
                      Tạo mới Nhà phân phối
                    </button>
                  </div>
                  @if ($errors->has('distributor_id'))
                    <div class="help-block">{{ $errors->first('distributor_id') }}</div>
                  @endif
                </div>
                <!------------------ Titles--------------->
                <div class="form-group m-form__group row">
                  <label class="col-form-label col-lg-2 col-sm-12">Chọn tiêu đề mặc định *</label>
                  <div class="col-lg-6 col-md-9 col-sm-12">
                    <select class="form-control m-select2" id="title_id"
                            name="title_id">
                      <option value=""></option>
                      @foreach($distributor_titles as $distributor_title)
                        <option value="{{$distributor_title->id}}"
                                @if( old('title_id') == $distributor_title->id) selected="selected" @endif
                        >{{$distributor_title->title}}</option>
                      @endforeach
                    </select>
                  </div>
                  @if ($errors->has('title_id'))
                    <div class="help-block">{{ $errors->first('title_id') }}</div>
                  @endif
                </div>
                {{--<!------------------ Upload hồ sơ cho Nhà phân phối Sản phẩm--------------->--}}
                {{--<div class="form-group m-form__group row">--}}
                  {{--<label class="col-form-label col-lg-2 col-sm-12">Upload files hồ sơ</label>--}}
                  {{--<div class="col-lg-6 col-md-9 col-sm-12">--}}
                    {{--<label for="file"--}}
                           {{--style="cursor: pointer;padding:8px;background-color: #0aa7ef;color: white;border-radius: 20px"><i--}}
                        {{--class="la la-cloud-upload"></i> Chọn--}}
                      {{--tập tin</label>--}}
                  {{--</div>--}}
                  {{--<input type="file" name="files[]"--}}
                         {{--id="file"--}}
                         {{--class="upload"--}}
                         {{--style="display: none!important;"--}}
                         {{--accept=".png,.jpg,.jpeg,.gif,.pdf,.doc,.docx,.xls, .xlsx"--}}
                         {{--multiple>--}}
                {{--</div>--}}
                {{--<div class="form-group m-form__group row">--}}
                  {{--<label style="color: white" class="col-form-label col-lg-3 col-sm-12">Upload--}}
                    {{--files</label>--}}
                  {{--<div class="col-lg-6 col-md-9 col-sm-12">--}}
                    {{--<ol class="uploadFiles_list heroes">--}}
                    {{--</ol>--}}
                  {{--</div>--}}
                {{--</div>--}}
                <div class="m-portlet__foot m-portlet__foot--fit">
                  <div class="m-form__actions m-form__actions">
                    <div class="row">
                      <div class="col-lg-9 ml-lg-auto">
                        <button type="submit" class="btn btn-secondary">
                          Tiếp tục =>
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </form>
            <!--begin::Modal-->
            <div class="modal fade" id="create_distributor" tabindex="-1" role="dialog"
                 aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h3>
                      TẠO MỚI NHÀ PHÂN PHỐI
                    </h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">
												&times;
											</span>
                    </button>
                  </div>
                  <form method="POST" enctype="multipart/form-data" id="create_distributor_form"
                        action="{{route('Business::distributor@store') }}">
                    <div class="modal-body">
                      <div class="form-group">
                        <label class="form-control-label">
                          Chọn Doanh nghiệp:
                        </label>
                      </div>
                      <div class="form-group">
                        <select class="form-control" id="business_id" style="width: 100%"
                                name="business_id">
                          <option value=""></option>
                          @foreach($businesses as $business)
                            <option value="{{$business->id}}"
                                    @if( old('business_id') == $business->id) selected="selected" @endif>{{$business->name}}</option>
                          @endforeach
                        </select>
                      </div>

                      <div class="form-group">
                        <label class="form-control-label">
                          Tên *:
                        </label>
                        <input type="text" class="form-control" name="name" required>
                      </div>
                      <div class="form-group">
                        <label class="form-control-label">
                          Địa chỉ *:
                        </label>
                        <input type="text" class="form-control" name="address" required>
                      </div>
                      <div class="form-group">
                        <label class="form-control-label">
                          Email:
                        </label>
                        <input type="text" class="form-control" name="email">
                      </div>
                      <div class="form-group">
                        <label class="form-control-label">
                          Số điện thoại:
                        </label>
                        <input type="text" class="form-control" name="phone">
                      </div>
                      <div class="form-group">
                        <label class="form-control-label">
                          Website:
                        </label>
                        <input type="text" class="form-control" name="website">
                      </div>
                    </div>

                    <div class="modal-footer">
                      <button type="button" id="close_modal" class="btn btn-secondary" data-dismiss="modal">
                        Đóng
                      </button>
                      <button type="submit" id="create_distributor_submit" class="btn btn-primary">
                        Thêm mới
                      </button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
            <!--end::Modal-->
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('scripts')
  <script type="text/javascript" src="{{ asset('qr_code/metronic/js/vendors.bundle.js') }}"></script>
  {{--<script type="text/javascript" src="{{ asset('qr_code/metronic/js/scripts.bundle.js') }}"></script>--}}
  <script type="text/javascript">
    $(document).ready(function () {
      // Basic
      $("#business_id").select2({
        placeholder: 'Chọn doanh nghiệp',
        allowClear: true
      });

      $("#selected_business_id").select2({
        placeholder: 'Chọn doanh nghiệp quản lý Sản phẩm',
        allowClear: true
      });

      $("#product_id").select2({
        placeholder: 'Chọn sản phẩm',
        allowClear: true,
        minimumInputLength: 3,
        ajax: {
          url: "{{ route('Business::product@ajaxSearch')}}",
          dataType: 'json',
          data: function (params) {
            return {
              search: params.term,
            };
            // Query parameters will be ?search=[term]
          },
          processResults: function (data) {
            return {
              results: data.results,
            }
          }
        }
      });
      $("#title_id").select2({
        placeholder: 'Chọn tiêu đề',
        allowClear: true
      });
      $("#distributor_id").select2({
        placeholder: 'Chọn Nhà phân phối',
        allowClear: true,
        minimumInputLength: 3,
        ajax: {
          url: "{{ route('Business::distributor@ajaxSearch')}}",
          dataType: 'json',
          data: function (params) {
            return {
              search: params.term,
            };
            // Query parameters will be ?search=[term]
          },
          processResults: function (data) {
            return {
              results: data.results,
            }
          }
        }
      });
      $("select").on("select2:select", function (evt) {
        var element = evt.params.data.element;
        var $element = $(element);
        $element.detach();
        $(this).append($element);
        $(this).trigger("change");
      });


      $('#create_distributor').on('hidden.bs.modal', function () {
        document.getElementById('create_distributor_form').reset();
      });
    });


    $("#create_distributor_form").on("submit", function (event) {
      event.preventDefault();
      console.log($(this).serialize());
      $.ajax({
        type: "POST",
        url: "{{route('Business::distributor@store') }}",
        data: $(this).serialize(),
        success: function (data) {
          console.log(data);
          $('#create_distributor').modal('hide');
          $('.modal-backdrop').hide();
        }
      });
    });

  </script>
  <script>
    //Upload files
    $(".upload").change(function () {
      let spinner = `<div class="spinner m-loader m-loader--primary" style="width: 30px; display: inline-block;">
                            </div>`;
      let x = document.getElementById("file");
      $(".uploadFiles_list").append(`${spinner}`);
      var files = x.files;
      var formData = new FormData();
      // Loop through each of the selected files.
      for (let i = 0; i < files.length; i++) {
        let file = files[i];

        // Add the file to the request.
        formData.append('files[]', file, file.name);

      }

      var url = "{{ route('Business::upload')}}";
//            var token = $("input[name='_token']").val();
      $.ajax({
        url: url,
        method: 'POST',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {
          $("div").remove(".spinner");
          $.each(data, function (key, value) {
            let file = JSON.parse(value);
            appendData(file);
          });
        }
      });
    });
    var removeItem = function (e) {
      e.closest('li').remove();
    };

    function appendData(file) {
      for (let i = 0; i < file.length; i++) {
        let file_name = file[i][0].split("_").pop();
        let html = `<li>
                                <div style="text-align: left;display: inline-block;width:70%">${file_name}</div>
                                <input type="hidden" name="uploaded_files[]" value="${file[i][0]}">
                                <div style="text-align: right;display: inline-block;float: right;">
                                    <a href="${file[i][1]}" target="_blank" data-toggle="tooltip" title="Xem" class="btn btn-outline-accent m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air"> <i class="la la-eye"></i></a>
                                    <a href="javascript:void(0);" onclick="return removeItem($(this));" data-toggle="tooltip" title="Xóa" class="btn btn-outline-accent m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air"> <i class="la la-remove"></i></a>
                                </div>
                           </li>`;
        $(".uploadFiles_list").append(`${html}`);
      }
    }
  </script>
  {{--<script type="text/javascript">--}}
  {{--var url = "{{ route('Business::productDistributor@showProductAndDistributorByBusiness')}}";--}}
  {{--$("select[name='business_id']").change(function () {--}}
  {{--var business_id = $(this).val();--}}
  {{--var token = $("input[name='_token']").val();--}}
  {{--$.ajax({--}}
  {{--url: url,--}}
  {{--method: 'POST',--}}
  {{--data: {--}}
  {{--business_id: business_id,--}}
  {{--_token: token--}}
  {{--},--}}
  {{--success: function (data) {--}}
  {{--$("select[name='product_id[]']").html('');--}}
  {{--$("select[name='distributor_id[]']").html('');--}}
  {{--$("select[name='product_id[]']").append(--}}
  {{--"<option value=''" + ">" + '' + "</option>"--}}
  {{--);--}}
  {{--$("select[name='distributor_id[]']").append(--}}
  {{--"<option value=''" + ">" + '' + "</option>"--}}
  {{--);--}}
  {{--console.log(data.products);--}}
  {{--$.each(data.products, function (key, value) {--}}
  {{--$("select[name='product_id[]']").append(--}}
  {{--"<option value=" + value.id + ">" + value.name + " (" + value.barcode + ")" + "</option>"--}}
  {{--);--}}
  {{--});--}}
  {{--$.each(data.distributors, function (key, value) {--}}
  {{--$("select[name='distributor_id[]']").append(--}}
  {{--"<option value=" + value.id + ">" + value.name + "</option>"--}}
  {{--);--}}
  {{--});--}}
  {{--}--}}
  {{--});--}}
  {{--});--}}
  {{--</script>--}}

@endpush
