@extends('qrcode.layouts.app')
@push('styles')
  <style>
    .heroes {
      margin: 0 0 2em 0;
      padding: 0;
      width: 30.5em;
    }

    .heroes li {
      position: relative;
      left: 0;
      color: #00c5dc;
      margin: .5em;
      padding: .3em .3em;
      height: auto;
      min-height: 3em;
      border-radius: 4px;
      border-color: #00c5dc;
      border-width: 2px;
      border-style: solid;
    }

    .attribute_title {
      font-weight: bold;
      text-decoration: underline;
      font-size: 16px;
    }

    img {
      cursor: zoom-in;
    }

    .select2 {
      width: 100% !important;
    }
  </style>
@endpush
@section('body_class', 'm-page--fluid m-page--loading-enabled m-page--loading m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default')

@push('scripts')
  <script type="text/javascript">
    <?php
      $sharedData = [
        'config' => [
          'csrfToken' => csrf_token(),
          'logoutUrl' => route('logout'),
          'isNative' => true,
        ],
        'loggedInAccount' => auth()->check() ? auth()->user() : null,
      ];
      ?>
      window.SHARED_DATA = {!! json_encode($sharedData) !!}
  </script>
  <script src="https://cdn.jsdelivr.net/npm/tinymce@4.7.11/tinymce.min.js" type="text/javascript"></script>
  <script src="{{ mix('js/app.js') }}"></script>
@endpush

@section('content')
  <!-- begin::Page loader -->
  <div class="m-page-loader m-page-loader--base">
    <div class="m-blockui">
      <span>
        Please wait...
      </span>
      <span>
        <div class="m-loader m-loader--brand"></div>
      </span>
    </div>
  </div>
  <!-- end::page Loader -->
  <div id="app" class="m-grid m-grid--hor m-grid--root m-page">
    <div class="m-header__bottom">
      <div class="m-container m-container--responsive m-container--xxl m-container--full-height m-page__container">
        <div class="m-stack m-stack--ver m-stack--desktop">
          <div class="m-stack__item m-stack__item--middle m-stack__item--fluid">
            <app-header></app-header>
          </div>
        </div>
      </div>
    </div>
    <div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop  m-body m-page__container">
      <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader">
          <div class="d-flex align-items-center">
            <div class="mr-auto">
              <h3 class="m-subheader__title">
                DANH SÁCH SẢN PHẨM DOANH NGHIỆP QUẢN LÝ
              </h3>
            </div>
            @if($waiting_product_count > 0)
              <div style="border: solid #bf800c;padding: 3px;margin-right: 5px"><a
                  href="{{route('Business::product@index')}}?status=1" style="color: #bf800c">Có
                  <span id="waiting_count">{{$waiting_product_count}}</span> sản phẩm đang chờ
                  duyệt thông tin</a></div>
            @endif
            @if($needRecheck_product_count > 0)
              <div style="border: solid red;padding: 3px"><a
                  href="{{route('Business::product@index')}}?need_recheck=1" style="color: red">Có
                  <span>{{$needRecheck_product_count}}</span> sản phẩm cần kiểm tra lại thông tin đã duyệt</a></div>
            @endif
          </div>
          <div>
            @can("GATEWAY-viewRequestProduct")
              <a href="{{route('Business::productRequest@index')}}" target="_blank"
                 class="btn btn-outline-brand m-btn btn-sm m-btn--icon m-btn--pill"><span>
                                    <span style="font-size: 14px">Xem danh sách Yêu cầu Quản lý sản phẩm của Doanh nghiệp</span></span></a>
            @endcan
            @can("GATEWAY-viewHoliday")
              <a href="{{route('Business::holiday@index')}}" target="_blank"
                 class="btn btn-outline-brand m-btn btn-sm m-btn--icon m-btn--pill"><span>
                                    <span style="font-size: 14px">Xem thời gian tự động duyệt thông tin Sản phẩm</span></span></a>
            @endcan
          </div>
        </div>
        <!-- END: Subheader -->
        <div class="m-content">
          <!--Begin::Section-->
          <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__body">
              <form class="m-form m-form--fit m--margin-bottom-20"
                    action="{{route('Business::product@index')}}" method="get">
                <div class="row m--margin-bottom-20">
                  <div class="col-lg-4 m--margin-bottom-10-tablet-and-mobile">
                    <label>
                      Tìm kiếm theo doanh nghiệp:
                    </label>
                    <select class="form-control m-input" data-col-index="7" id="business_id"
                            name="business_id">
                      <option value="" selected="selected"></option>
                      @foreach($businesses as $business)
                        <option value="{{$business->id}}"
                          {{Request::input('business_id')==$business->id?'selected="selected"':''}}
                        >{{$business->name}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile">
                    <label>
                      Tên sản phẩm:
                    </label>
                    <input type="text" class="form-control m-input" name="name"
                           value="{{ Request::input('name') }}"
                           data-col-index="4">
                  </div>
                  <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile">
                    <label>
                      Mã barcode:
                    </label>
                    <input type="text" class="form-control m-input" name="barcode"
                           value="{{ Request::input('barcode') }}"
                           data-col-index="4">
                  </div>
                  <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile">
                    <label>
                      Chọn trạng thái:
                    </label>
                    <select class="form-control m-input" data-col-index="7" id="status"
                            name="status">
                      <option value="" selected="selected">--Tất cả--</option>
                      <option
                        value="{{\App\Models\Business\Product::STATUS_PENDING}}" {{Request::input('status')==\App\Models\Business\Product::STATUS_PENDING?'selected="selected"':''}}>
                        Chờ duyệt
                      </option>
                      <option
                        value="{{\App\Models\Business\Product::STATUS_APPROVED}}" {{Request::input('status')==\App\Models\Business\Product::STATUS_APPROVED?'selected="selected"':''}}>
                        Đã duyệt
                      </option>
                      <option
                        value="{{\App\Models\Business\Product::STATUS_EXPIRED}}" {{Request::input('status')==\App\Models\Business\Product::STATUS_EXPIRED?'selected="selected"':''}}>
                        Đã hết hạn
                      </option>
                    </select>
                  </div>
                  <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile">
                    <label>
                      Tình trạng giấy tờ:
                    </label>
                    <select class="form-control m-input" data-col-index="7" id="certificate_status"
                            name="certificate_status">
                      <option value="" selected="selected">--Tất cả--</option>
                      <option value="1" {{Request::input('certificate_status')==1?'selected="selected"':''}}>
                        Chưa có giấy tờ
                      </option>
                      <option value="2" {{Request::input('certificate_status')==2?'selected="selected"':''}}>
                        Có giấy tờ
                      </option>
                    </select>
                  </div>
                </div>
                <div class="row m--margin-bottom-20">
                  <div class="col-lg-4 m--margin-bottom-10-tablet-and-mobile">
                    <label>
                      Tình trạng Chứng chỉ Sản phẩm:
                    </label>
                    <select class="form-control m-input" data-col-index="7"
                            id="certificateImages_status"
                            name="has_certificate_images">
                      <option value="" selected="selected">--Tất cả--</option>
                      <option value="0" {{Request::input('has_certificate_images')=="0"?'selected="selected"':''}}>
                        Chưa có
                      </option>
                      <option value="1" {{Request::input('has_certificate_images')==1?'selected="selected"':''}}>
                        Có
                      </option>
                    </select>
                  </div>
                  <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                    <label>
                      Sản phẩm cần kiểm tra lại thông tin:
                    </label>
                    <select class="form-control m-input" data-col-index="7"
                            id="need_recheck"
                            name="need_recheck">
                      <option value="" selected="selected">--Tất cả--</option>
                      <option value="0" {{Request::input('need_recheck')=="0"?'selected="selected"':''}}>
                        Không
                      </option>
                      <option value="1" {{Request::input('need_recheck')==1?'selected="selected"':''}}>
                        Có
                      </option>
                    </select>
                  </div>
                  <div class="col-lg-4 m--margin-bottom-10-tablet-and-mobile">
                    <label style="color: white">Hành động </label>
                    <div>
                      <button class="btn btn-brand m-btn m-btn--icon" type="submit">
												<span>
													<i class="la la-search"></i>
													<span>
														Tìm kiếm
													</span>
												</span>
                      </button>
                      &nbsp;&nbsp;
                      <button class="btn btn-secondary m-btn m-btn--icon" type="button"
                              onclick="window.location='{{route('Business::product@index')}}'">
												<span>
													<i class="la la-refresh"></i>
													<span>
														Làm mới
													</span>
												</span>
                      </button>
                    </div>
                  </div>
                </div>
              </form>
              @if (session('success'))
                <div
                  class="m-alert m-alert--outline m-alert--square m-alert--outline-2x alert alert-success alert-dismissible fade show"
                  role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  </button>
                  {{ session('success') }}
                </div>
              @endif
              @can("GATEWAY-disapproveProductManageRole")
                <div style="text-align: right">
                  <button type="button" class="btn btn-success btn-approveMultiProductsInfo">
                    Duyệt thông tin nhiều Sản phẩm
                  </button>
                  <button type="button" class="btn btn-danger btn-disapproveMultiProductsInfo" data-toggle="modal"
                          data-target="#disapproveMultiProductsInfoModal">
                    Hủy duyệt thông tin nhiều Sản phẩm
                  </button>
                  <button type="button" class="btn btn-danger" onclick="check()">
                    Bỏ quyền quản lý nhiều Sản phẩm
                  </button>
                  <button type="button" class="btn btn-danger" data-toggle="modal"
                          data-target="#disapproveAllProducts">
                    Bỏ quyền quản lý tất cả Sản phẩm của Doanh nghiệp
                  </button>
                </div>
              @endcan
              <div style="margin-bottom: 0;margin-top: 50px;margin-left: 50px;">
                  <span
                    style="width:200px;text-align:center;padding:6px;font-weight: bold;font-style: italic;border: solid 1px;border-radius: 6px">
                    Đang hiển thị {{$count_record_in_page}}/{{$count_record_total}} bản ghi
                  </span>
                <span style="margin-left: 5px">
                    <a href="{{route('Business::product@excelExport',$query)}}"
                       class="btn btn-info m-btn btn-sm 	m-btn m-btn--icon m-btn--pill">
                      <span>
													<i class="la la-cloud-download"></i>
													<span>
														Xuất Excel
													</span>
                      </span>
                    </a>
                  </span>
              </div>
              <div class="table-responsive">
                <form method="POST" enctype="multipart/form-data" name="multiDisapprove_form"
                      onsubmit="return validate('multi_note')"
                      action="{{ route('Business::product@disapproveMultiProducts') }}">
                  {{ csrf_field() }}
                  <table class="table table-striped- table-bordered table-hover table-checkable"
                         style="margin-top: 20px;margin-left:auto;margin-right:auto;width: 2000px">
                    <!--begin::Thead-->
                    <thead>
                    <tr>
                      <th><label class="m-checkbox">
                          <input type="checkbox" name="check_all">
                          <span></span>
                        </label></th>
                      <th style="width: 70px;font-weight: bold">Id</th>
                      <th style="min-width: 300px;font-weight: bold">Tên sản phẩm</th>
                      <th style="width: 100px;font-weight: bold">Mã barcode</th>
                      <th style="font-weight: bold;min-width: 300px">Doanh nghiệp quản lý</th>
                      <th style="font-weight: bold;min-width: 300px">Doanh nghiệp sở hữu</th>
                      <th style="font-weight: bold;width: 100px">Tình trạng giấy tờ</th>
                      <th style="font-weight: bold;width: 100px">Chứng chỉ Sản phẩm</th>
                      <th style="font-weight: bold;width: 100px">Thông tin SP</th>
                      <th style="font-weight: bold;min-width: 140px">Trạng thái</th>
                      <th style="font-weight: bold;width: 100px">Ngày tạo</th>
                      <th style="font-weight: bold;width: 100px">Ngày cập nhật gần nhất</th>
                      <th style="font-weight: bold;min-width: 120px">Duyệt/bỏ duyệt thông tin sản phẩm</th>
                      <th style="font-weight: bold;width: 120px">Đồng bộ lại NPP</th>
                      <th style="font-weight: bold;width: 120px">Hủy bỏ quyền quản lý sản phẩm
                      </th>
                    </tr>
                    </thead>
                    <!--end::Thead-->
                    <!--begin::Tbody-->
                    <tbody>
                    @foreach($products as $product)
                      <tr>
                        <td>
                          @if($product->status!=\App\Models\Business\Product::STATUS_EXPIRED)
                            <label class="m-checkbox">
                              <input type="checkbox" name="product_ids[]"
                                     value="{{$product->id}}">
                              <span></span>
                            </label>
                          @endif
                        </td>
                        <td class="id">{{$product->id}}</td>
                        <td>
                          <a href="{{route('Business::product@getUploadCertificateImages',[$product->id])}}"
                             target="_blank">{{$product->name}}</a>
                        </td>
                        <td><span @if($product->validBarcode == 0) title="Mã sai chuẩn định dạng"
                                  style="color: red" @endif>{{$product->barcode}}</span></td>
                        <td>
                          <a href="{{route('Business::businesses@index')}}?business_id={{$product->business_id}}"
                             target="_blank">
                            {{$product->business_name}}
                          </a>
                        </td>
                        <td>{{$product->vendor_name}}</td>
                        <td>
                          @if($product->certificate_id)
                            {{'Có'}}
                          @else
                            {{'Chưa có'}}
                          @endif
                        </td>
                        <td>
                          @if($product->has_certificate_images)
                            {{'Có'}}
                          @else
                            {{'Chưa có'}}
                          @endif
                        </td>
                        <td>
                          @can("GATEWAY-viewDetailProduct")
                            <button
                              type="button"
                              @if(auth()->user()->cannot('GATEWAY-viewDetailProduct'))
                              disabled
                              @endif
                              value="{{$product->certificate_id}}"
                              data-target="#file{{$product->id}}"
                              class="files_modal btn m-btn--pill m-btn--air btn-outline-info btn-sm"
                              id="upload_files_{{$product->id}}"
                              data-toggle="modal">Chi tiết
                            </button>
                          @endcan
                          <input type="hidden" id="{{$product->id}}"
                                 value="{{$product->business_id}}">
                          <!--Modal-->
                          <div id="file{{$product->id}}" class="modal fade"
                               role="dialog">
                            <div class="modal-dialog modal-lg" role="document">
                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h1 class="modal-title">Thông tin của sản phẩm
                                    "{{$product->name}}"</h1>
                                  <button type="button" class="close"
                                          data-dismiss="modal">&times;
                                  </button>
                                </div>
                                <form method="POST"
                                      action="{{ route('Business::product@updateFile')}}">
                                  <div class="modal-body"
                                       style="height:400px;overflow-y: scroll;">
                                    <input type="hidden" name="product_id"
                                           value="{{$product->id}}">
                                    <div class="upload_files_{{$product->id}}_select">
                                    </div>
                                    <div style="border-bottom: solid forestgreen 2px">
                                      <ol class="heroes upload_files_{{$product->id}}_input_list">

                                      </ol>
                                    </div>
                                    <div style="padding-top: 5px">
                                      <p>
                                        <span class="attribute_title">TÊN SẢN PHẨM:</span> {{$product->name}}
                                      </p>
                                      @if($product->original_data && array_key_exists("name", $product->original_data))
                                        <p style="color: red"><span
                                            class="attribute_title">TÊN SẢN PHẨM (cũ):</span>{{$product->original_data['name']}}
                                        </p>
                                      @endif
                                      <p>
                                        <span class="attribute_title">MÃ BARCODE:</span> {{$product->barcode}}
                                      </p>
                                      <p>
                                      @if($product->images)
                                        <div class="attribute_title">ẢNH SẢN
                                          PHẨM:
                                        </div>
                                        <p>
                                          @foreach($product->images as $image)
                                            <span style="margin: 5px;padding:5px"><img
                                                src="{{$image}}"
                                                width="200px"></span>
                                          @endforeach
                                        </p>
                                      @endif
                                      @if($product->original_data && array_key_exists("images", $product->original_data))
                                        <div class="attribute_title" style="color: red">ẢNH SẢN
                                          PHẨM (cũ):
                                        </div>
                                        <p>
                                          @foreach($product->original_data['images'] as $image)
                                            <span style="margin: 5px;padding:5px"><img
                                                src="{{$image}}"
                                                width="200px"></span>
                                          @endforeach
                                        </p>
                                      @endif
                                      @if($product->certificate_images)
                                        <div class="attribute_title">ẢNH CHỨNG
                                          CHỈ
                                          SẢN PHẨM:
                                        </div>
                                        <p>
                                          @foreach($product->certificate_images as $image)
                                            <span style="margin: 5px;padding:5px"><img
                                                src="{{$image}}"
                                                width="200px"></span>
                                          @endforeach
                                        </p>
                                      @endif
                                      @if($product->original_data && array_key_exists("certificate_images", $product->original_data))
                                        <div class="attribute_title" style="color:red;">ẢNH CHỨNG
                                          CHỈ
                                          SẢN PHẨM (cũ):
                                        </div>
                                        <p>
                                          @foreach($product->original_data['certificate_images'] as $image)
                                            <span style="margin: 5px;padding:5px"><img
                                                src="{{$image}}"
                                                width="200px"></span>
                                          @endforeach
                                        </p>
                                      @endif
                                      <p>
                                      <span
                                        class="attribute_title">GIÁ SẢN PHẨM:</span> {{number_format($product->price,0,".",",")}}
                                      </p>
                                      @if($product->original_data && array_key_exists("price", $product->original_data))
                                        <p style="color: red"><span
                                            class="attribute_title">GIÁ SẢN PHẨM (cũ):</span>{{number_format($product->original_data['price'],0,".",",")}}
                                        </p>
                                      @endif
                                      @if($product->attributes)
                                        @foreach($product->attributes as $key => $value)
                                          @if($value != '')
                                            <p><span class="attribute_title">{{$key}}
                                                :</span> @php echo preg_replace('/(<br\s*\/?>\s*)+/', '<br>', nl2br($value)) @endphp
                                            </p>
                                          @endif
                                        @endforeach
                                      @endif
                                      @if($product->original_data && array_key_exists("attributes", $product->original_data))
                                        @if($product->original_data['attributes'])
                                          @foreach($product->original_data['attributes'] as $key => $value)
                                            @if($value != '')
                                              <p><span class="attribute_title" style="color: red">{{$key}} (cũ)
                                                  :</span> @php echo preg_replace('/(<br\s*\/?>\s*)+/', '<br>', nl2br($value)) @endphp
                                              </p>
                                            @endif
                                          @endforeach
                                        @endif
                                      @endif
                                    </div>
                                  </div>
                                  <div class="modal-footer">
                                    @can("GATEWAY-updateProductCertificate")
                                      <button type="submit"
                                              class="btn btn-success">
                                        Cập nhật giấy tờ
                                      </button>
                                    @endcan
                                    @can("GATEWAY-approveProductInfo")
                                      @if($product->status!=\App\Models\Business\Product::STATUS_EXPIRED)
                                        <button type="button" data-dismiss="modal"
                                                class="approveProductInfo_modalButton btn btn-info">
                                          Duyệt thông tin Sản phẩm
                                        </button>
                                        @if($product->need_recheck)
                                          <button
                                            title="Bỏ duyệt thông tin"
                                            type="button"
                                            data-toggle="modal"
                                            data-target="#infoDisapprove{{$product->id}}"
                                            class="btn btn-danger">
                                            Bỏ duyệt thông tin Sản phẩm
                                          </button>
                                        @endif
                                      @endif
                                    @endcan
                                    <button type="button"
                                            class="close-modal btn btn-default"
                                            data-dismiss="modal">Hủy bỏ
                                    </button>
                                  </div>
                                </form>
                              </div>
                            </div>
                          </div>
                          <!--End Modal-->
                        </td>
                        <td>
                          @if($product->status==0)
                            <span class="status m-badge m-badge--metal m-badge--wide">Chưa xác minh </span>
                          @elseif($product->status==\App\Models\Business\Product::STATUS_PENDING)
                            <span class="status m-badge m-badge--warning m-badge--wide">Chờ duyệt </span>
                          @elseif($product->status==\App\Models\Business\Product::STATUS_APPROVED)
                            <span class="status m-badge m-badge--success m-badge--wide">Đã duyệt </span>
                            @if($product->need_recheck)
                              <p style="color:red;">{{"Cần kiểm tra lại thông tin"}}</p>
                            @endif
                          @elseif($product->status==\App\Models\Business\Product::STATUS_EXPIRED)
                            <span class="status m-badge m-badge--danger m-badge--wide">Hết hạn </span>
                          @elseif($product->status==\App\Models\Business\Product::STATUS_DISAPPROVED)
                            <span class="status m-badge m-badge--danger m-badge--wide">Hủy duyệt thông tin </span>
                          @endif
                        </td>
                        <td>
                          {{date("d/m/Y H:i:s",strtotime($product->created_at))}}
                        </td>
                        <td>
                          {{date("d/m/Y H:i:s",strtotime($product->updated_at))}}
                        </td>
                        <td>
                          {{--@if($product->status!=2)--}}
                          {{--<a href="{{route('Business::product@approve',[$product->id])}}"--}}
                          {{--title="Duyệt"--}}
                          {{--class="btn btn-success m-btn m-btn--icon m-btn--icon-only m-btn--pill">--}}
                          {{--<i class="la la-check"></i>--}}
                          {{--</a>--}}
                          @if($product->status!=\App\Models\Business\Product::STATUS_EXPIRED)
                            @can("GATEWAY-approveProductInfo")
                              <button
                                title="Duyệt"
                                type="button"
                                @if(auth()->user()->cannot('GATEWAY-approveProductInfo'))
                                disabled
                                @endif
                                class="approve btn btn-success m-btn m-btn--icon m-btn--icon-only m-btn--pill">
                                <i class="la la-check"></i>
                              </button>
                              @if($product->need_recheck)
                                <button
                                  title="Bỏ duyệt thông tin"
                                  type="button"
                                  @if(auth()->user()->cannot('GATEWAY-approveProductInfo'))
                                  disabled
                                  @endif
                                  data-toggle="modal"
                                  data-target="#infoDisapprove{{$product->id}}"
                                  class="btn btn-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill">
                                  <i class="la la-remove"></i>
                                </button>
                              @endif
                            <!--Modal Comfirm disapprove-->
                              <div id="infoDisapprove{{$product->id}}" class="modal fade"
                                   role="dialog">
                                <div class="modal-dialog">
                                  <!-- Modal content-->
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h2>Bạn chắc
                                        chắn muốn bỏ duyệt thông tin sản phẩm
                                        "{{$product->name}}" của doanh nghiệp
                                        "{{$product->business_name}}"?</h2>
                                      <button type="button" class="close"
                                              data-dismiss="modal">&times;
                                      </button>
                                    </div>
                                    <form method="POST"
                                          action="{{route('Business::product@disapproveMultiProductsInfo')}}">
                                      {{ csrf_field() }}
                                      <div class="modal-body">
                                        <div class="form-group m-form__group row">
                                      <textarea class="form-control" rows="5"
                                                placeholder="Nhập lý do hủy bỏ thông tin sản phẩm. Hệ thống sẽ tự động gửi email thông tin cho doanh nghiệp về lý do được đưa ra!"
                                                name="reason"
                                                required></textarea>
                                        </div>
                                        <input type="hidden" name="product_ids" value="{{json_encode([$product->id])}}">
                                      </div>
                                      <div class="modal-footer">
                                        <button type="submit"
                                                class="btn btn-success">
                                          Đồng ý
                                        </button>
                                        <button type="button"
                                                class="btn btn-default"
                                                data-dismiss="modal">Đóng
                                        </button>
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>
                              <!--End Modal-->
                            @endcan
                          @endif
                        </td>
                        <td>
                          @can("GATEWAY-approveProductInfo")
                            @if($product->status!=\App\Models\Business\Product::STATUS_EXPIRED)
                              <button
                                title="Đồng bộ lại NPP"
                                type="button"
                                @if(auth()->user()->cannot('GATEWAY-approveProductInfo'))
                                disabled
                                @endif
                                onclick="window.location='{{route('Business::product@syncDistributors',[$product->id])}}'"
                                class="btn btn-info m-btn m-btn--icon m-btn--icon-only m-btn--pill">
                                <i class="la la-refresh"></i>
                              </button>
                            @endif
                          @endcan
                        </td>
                        <td>
                          @can("GATEWAY-disapproveProductManageRole")
                            @if($product->status!=\App\Models\Business\Product::STATUS_EXPIRED)
                              <button type="button"
                                      title="Từ chối quyền quản lý"
                                      @if(auth()->user()->cannot('GATEWAY-disapproveProductManageRole'))
                                      disabled
                                      @endif
                                      data-toggle="modal"
                                      data-target="#delete{{$product->id}}"
                                      class="btn btn-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill">
                                <i class="la la-close"></i>
                              </button>
                          @endif
                        @endcan
                        <!--Modal Comfirm disapprove-->
                          <div id="delete{{$product->id}}" class="modal fade"
                               role="dialog">
                            <div class="modal-dialog">
                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h2>Bạn chắc
                                    chắn muốn hủy bỏ quyền quản lý sản phẩm
                                    "{{$product->name}}" đối với doanh nghiệp
                                    "{{$product->business_name}}"?</h2>
                                  <button type="button" class="close"
                                          data-dismiss="modal">&times;
                                  </button>
                                </div>
                                <form method="get" onsubmit="return validate('note-{{$product->id}}')"
                                      action="{{route('Business::product@disapprove',[$product->id])}}">
                                  {{ csrf_field() }}
                                  <div class="modal-body">
                                    <div class="form-group m-form__group row">
                                        <textarea class="form-control" rows="5"
                                                  placeholder="Nhập lý do hủy bỏ quyền quản lý sản phẩm. Hệ thống sẽ tự động gửi email thông tin cho doanh nghiệp về lý do được đưa ra!"
                                                  name="note" id="note-{{$product->id}}"
                                                  required></textarea>
                                    </div>
                                    <h3>Chú ý:</h3> Nếu hủy bỏ quyền quản lý Sản
                                    phẩm
                                    của Doanh nghiệp thì Doanh nghiệp sẽ không
                                    thể
                                    thao
                                    tác hay
                                    cập nhật bất kỳ thông tin gì liên quan đến
                                    Sản
                                    phẩm
                                    nữa. Trường hợp Doanh nghiệp muốn quản lý
                                    sản
                                    phẩm,
                                    phải gửi lại yêu cầu quản lý Sản phẩm.
                                  </div>

                                  <div class="modal-footer">
                                    <button type="submit"
                                            class="btn btn-success">
                                      Đồng ý
                                    </button>
                                    <button type="button"
                                            class="btn btn-default"
                                            data-dismiss="modal">Đóng
                                    </button>
                                  </div>
                                </form>
                              </div>
                            </div>
                          </div>
                          <!--End Modal-->
                        </td>
                      </tr>
                    @endforeach
                    </tbody>
                    <!--end::Tbody-->
                  </table>
                  <!-- Modal disapproveAllProducts-->
                  <div id="disapproveAllProducts" class="modal fade"
                       role="dialog">
                    <div class="modal-dialog">
                      <!-- Modal content-->
                      <div class="modal-content">
                        <div class="modal-header">
                          <h2>Bạn chắc
                            chắn muốn hủy bỏ quyền quản lý tất cả các sản phẩm của Doanh nghiệp?</h2>
                          <button type="button" class="close"
                                  data-dismiss="modal">&times;
                          </button>
                        </div>
                        <form method="POST" enctype="multipart/form-data" onsubmit="return validate('all_note')"
                              action="{{ route('Business::product@disapproveAllProducts') }}">
                          {{ csrf_field() }}
                          <div class="modal-body">
                            <div class="form-group m-form__group row">
                              <select class="form-control" data-col-index="7" id="disapprove_business_id"
                                      style="width: 477px"
                                      name="disapprove_business_id" required>
                                <option value="" selected="selected"></option>
                                @foreach($businesses as $business)
                                  <option value="{{$business->id}}">{{$business->name}}</option>
                                @endforeach
                              </select>
                            </div>
                            <div class="form-group m-form__group row">
                            <textarea class="form-control" rows="5"
                                      placeholder="Nhập lý do hủy bỏ quyền quản lý sản phẩm. Hệ thống sẽ tự động gửi email thông tin cho doanh nghiệp về lý do được đưa ra!"
                                      name="all_note" id="all_note"
                                      required></textarea>
                            </div>
                            <h3>Chú ý:</h3> Nếu hủy bỏ quyền quản lý Sản
                            phẩm
                            của Doanh nghiệp thì Doanh nghiệp sẽ không
                            thể
                            thao
                            tác hay
                            cập nhật bất kỳ thông tin gì liên quan đến
                            Sản
                            phẩm
                            nữa. Trường hợp Doanh nghiệp muốn quản lý
                            sản
                            phẩm,
                            phải gửi lại yêu cầu quản lý Sản phẩm.
                          </div>

                          <div class="modal-footer">
                            <button type="submit"
                                    class="btn btn-success">
                              Đồng ý
                            </button>
                            <button type="button"
                                    class="btn btn-default"
                                    data-dismiss="modal">Hủy bỏ
                            </button>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                  <!-- Modal multiDisapproveProducts-->
                  <div id="multiDisapproveProducts" class="modal fade"
                       role="dialog">
                    <div class="modal-dialog">
                      <!-- Modal content-->
                      <div class="modal-content">
                        <div class="modal-header">
                          <h2>Bạn chắc
                            chắn muốn hủy bỏ quyền quản lý các sản phẩm này?</h2>
                          <button type="button" class="close"
                                  data-dismiss="modal">&times;
                          </button>
                        </div>
                        <div class="modal-body">
                          <div class="form-group m-form__group row">
                            <textarea class="form-control" rows="5"
                                      placeholder="Nhập lý do hủy bỏ quyền quản lý sản phẩm. Hệ thống sẽ tự động gửi email thông tin cho doanh nghiệp về lý do được đưa ra!"
                                      name="multi_note" id="multi_note"
                                      required></textarea>
                          </div>
                          <h3>Chú ý:</h3> Nếu hủy bỏ quyền quản lý Sản
                          phẩm
                          của Doanh nghiệp thì Doanh nghiệp sẽ không
                          thể
                          thao
                          tác hay
                          cập nhật bất kỳ thông tin gì liên quan đến
                          Sản
                          phẩm
                          nữa. Trường hợp Doanh nghiệp muốn quản lý
                          sản
                          phẩm,
                          phải gửi lại yêu cầu quản lý Sản phẩm.
                        </div>
                        <div class="modal-footer">
                          <label for="multiDisapprove_form"
                                 class="btn btn-success">
                            Đồng ý
                          </label>
                          <button type="button"
                                  class="btn btn-default"
                                  data-dismiss="modal">Hủy bỏ
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                  <input type="submit" id="multiDisapprove_form" style="display: none;"/>
                </form>
              </div>
              <!--Modal for showing image-->
              <div class="modal fade" id="enlargeImageModal" tabindex="-1" role="dialog"
                   aria-labelledby="enlargeImageModal" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                      <img src="" class="enlargeImageModalSource" style="width: 100%;">
                    </div>
                  </div>
                </div>
              </div>
              <!-- Modal disapproveMultiProductsInfo-->
              <div id="disapproveMultiProductsInfoModal" class="modal fade"
                   role="dialog">
                <div class="modal-dialog">
                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <h2>Bạn chắc
                        chắn muốn hủy bỏ thông tin hiện tại của các sản phẩm này ?</h2>
                      <button type="button" class="close"
                              data-dismiss="modal">&times;
                      </button>
                    </div>
                    <form method="POST" enctype="multipart/form-data"
                          action="{{ route('Business::product@disapproveMultiProductsInfo') }}">
                      <div class="modal-body">
                        <div class="form-group m-form__group row" id="disapproveMultiProductsInfoForm">
                            <textarea class="form-control" rows="5"
                                      placeholder="Nhập lý do hủy thông tin sản phẩm. Hệ thống sẽ tự động gửi email thông tin cho doanh nghiệp về lý do được đưa ra!"
                                      name="reason" id="reason"></textarea>
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button type="submit" class="btn btn-success">
                          Đồng ý
                        </button>
                        <button type="button"
                                class="btn btn-default"
                                data-dismiss="modal">Hủy bỏ
                        </button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
              <!--end::Table-->
              <div style="float:right;"><?php echo $products->links(); ?></div>
            </div>
            <!--End::Section-->
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
@push('scripts')
  <script type="text/javascript" src="{{ asset('qr_code/metronic/js/vendors.bundle.js') }}"></script>
  <script type="text/javascript">
    $(document).ready(function () {
      $('input[name="check_all"]').click(function () {
        let status = $(this).is(':checked');
        $('input[type="checkbox"]').prop('checked', status);
      });
      //Select2
      $("#status").select2({
        placeholder: 'Chọn trạng thái',
        allowClear: true
      });
      $("#business_id").select2({
        placeholder: 'Chọn doanh nghiệp',
        allowClear: true
      });
      $("#disapprove_business_id").select2({
        placeholder: 'Chọn doanh nghiệp',
        allowClear: true
      });
      $("#certificate_status").select2({
        placeholder: 'Chọn trạng thái',
        allowClear: true
      });
      $("#need_recheck").select2({
        placeholder: 'Chọn',
        allowClear: true
      });
      $("#certificateImages_status").select2({
        placeholder: 'Ảnh chứng chỉ Sản phẩm',
        allowClear: true
      });
      $("select").on("select2:select", function (evt) {
        var element = evt.params.data.element;
        var $element = $(element);
        $element.detach();
        $(this).append($element);
        $(this).trigger("change");
      });
      $(".modal").on('hidden.bs.modal', function () {
        document.body.style.padding = 0;
      });
    });

    //View image
    $(function () {
      $('img').on('click', function () {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
      });
    });

    //
    function check() {
      var checkboxes = document.querySelectorAll('input[type=checkbox]:checked');
      if (parseInt(checkboxes.length) === 0) {
        alert("Bạn chưa chọn Sản phẩm nào!");
        return false;
      }
      $('#multiDisapproveProducts').modal('show');
    }

    // Validate form
    function validate(note) {
      if (document.getElementById(note).value.trim() === '') {
        alert('Ban chưa nhập lý do!');
        return false;
      }
      return true;
    }

    //Fill Data to Modal
    $(".files_modal").click(function () {
      let id = $(this).attr('id');
      let product_id = id.split("_").pop();
      let business_id = $(`#${product_id}`).val();
      let spinner = `<div class="spinner m-loader m-loader--primary" style="width: 30px; display: inline-block;">
                            </div>`;
      $(`.${id}_input_list`).empty();
      $(`.${id}_input_list`).append(`${spinner}`);
      $(`.${id}_select`).empty();
      let value = $(this).attr('value');
      let html = `
                <div class="select_box">
                 <select class="form-control m-input"
                 data-col-index="12"
                  id="business_id_${product_id}"
                  name="select_id"
                  required>
                  <option value=""></option>
                </select>
                </div>
                `;
      if (value) {
        appendCurrentFiles(id, value);
      } else {
        $(`.${id}_input_list`).append("Chưa có giấy tờ!");
      }
      $(`.${id}_select`).append(`${html}`);
      $(`#business_id_${product_id}`).select2({
        placeholder: 'Thay đổi files giấy tờ bằng click chọn một trong các giấy tờ sau',
        allowClear: true,
        width: '30.5em'
      });
      appendSelectBox(business_id);
      $("div.select_box select").change(function () {
        $(`.${id}_input_list`).empty();
        let file = JSON.parse($(this).val());
        appendData(id, file);
        let certificate_id = $("div.select_box select option:selected").attr('data-key');
        $(`.${id}_input_list`).append(`<input type="hidden" name="certificate_id" value="${certificate_id}">`);
      });
    });

    function appendData(id, file) {
      for (let i = 0; i < file.length; i++) {
        let html = `<li>
                                <div style="text-align: left;display: inline-block;width: 70%">${file[i][0]}</div>
                                <div style="text-align: right;display: inline-block;float: right;">
                                    <a href="${file[i][2]}" target="_blank" data-toggle="tooltip" title="Xem" class="btn btn-outline-accent m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air"> <i class="la la-eye"></i></a>
                                </div>
                           </li>`;
        $(`.${id}_input_list`).append(`${html}`);
      }
    }

    function appendSelectBox(business_id) {
      let url = "{{ route('Business::certificate@certificatesByBusinessId')}}";
      let token = $("input[name='_token']").val();
      $.ajax({
        url: url,
        method: 'POST',
        data: {
          business_id: business_id,
          _token: token
        },
        success: function (data) {
          $.each(data, function (key, value) {
            $("select[name='select_id']").append(
              `<option value='${value.files}' data-key='${value.id}'>${value.name}</option>`
            );
          });
          $("div").remove(".spinner");
        }
      });
    }

    function appendCurrentFiles(id, certificate_id) {
      let url = "{{ route('Business::gln@getFilesById')}}";
      let token = $("input[name='_token']").val();
      $.ajax({
        url: url,
        method: 'POST',
        data: {
          certificate_id: certificate_id,
          _token: token
        },
        success: function (data) {
          if (data) {
            let file = JSON.parse(data);
            appendData(id, file);
          }
        }
      });
    }

    //Approve product
    $(".approve").click(function () {
      var approve_button = $(this);    // Find the row
      var status = approve_button.closest("tr").find(".status"); // Find the text
      status.html('Đang duyệt');
      status.addClass("m-loader m-loader--primary m-loader--left");
      status.css('padding-left', '32px');
      var id = approve_button.closest("tr").find(".id").text();
      var approve_url = "{{ route('Business::product@approve',':id')}}";
      approve_url = approve_url.replace(':id', id);
      $.ajax({
        url: approve_url,
        method: 'GET',
      });

      function checkStatus(id) {
        var check_url = "{{ route('Business::product@checkStatus',':id')}}";
        check_url = check_url.replace(':id', id);
        var checkResult = null;
        $.ajax({
          url: check_url,
          method: 'GET',
          async: false,
          success: function (data) {
            checkResult = parseInt(data);
          }
        });
        if (checkResult === 2) {
          return true;
        } else {
          return false;
        }
      }

      var interval_obj = setInterval(function () {
        let checkResult = checkStatus(id);
        if (checkResult) {
          clearInterval(interval_obj);
          status.removeClass();
          status.addClass("status m-badge m-badge--success m-badge--wide");
          status.css('padding-left', '10px');
          status.html('Đã duyệt');
          approve_button.hide();
          $current_count = parseInt($("#waiting_count").text());
          $("#waiting_count").html($current_count - 1);
        }
      }, 1000);
    });

    $(".approveProductInfo_modalButton").click(function () {
      var approveButton = $(this).closest("tr").find(".approve");
      approveButton.click();
    });

    $(".btn-approveMultiProductsInfo").click(function () {
      var checkboxes = document.querySelectorAll('input[type=checkbox]:checked');
      if (parseInt(checkboxes.length) === 0 || parseInt(checkboxes.length) === 1 && $('input[name="check_all"]').is(':checked')) {
        alert("Bạn chưa chọn Sản phẩm nào!");
        return false;
      }
      for (i = 0; i < checkboxes.length; i++) {
        let id = parseInt(checkboxes[i].value);
        if (isNaN(id) === false) {
          $(`:checkbox[value='${id}']`).closest("tr").find(".approve").click();
        }
      }

    });


    $("#disapproveMultiProductsInfoModal").on('shown.bs.modal', function () {
      var checkboxes = document.querySelectorAll('input[type=checkbox]:checked');
      if (parseInt(checkboxes.length) === 0 || parseInt(checkboxes.length) === 1 && $('input[name="check_all"]').is(':checked')) {
        alert("Bạn chưa chọn Sản phẩm nào!");
        return false;
      }
      var data = [];
      for (i = 0; i < checkboxes.length; i++) {
        let id = parseInt(checkboxes[i].value);
        if (isNaN(id) === false) {
          data.push(id);
        }
      }
      var JSONdata = JSON.stringify(data);
      console.log(JSONdata);
      $("#disapproveMultiProductsInfoForm").append(`<input type="hidden" name="product_ids" value="${JSONdata}">`);
    });
  </script>
@endpush
