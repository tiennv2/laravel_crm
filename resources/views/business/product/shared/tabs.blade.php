<ul class="nav nav-tabs m-tabs-line m-tabs-line--success" role="tablist">
    {{--<li class="nav-item m-tabs__item">--}}
        {{--<a class="nav-link m-tabs__link @if(isset($productTab) && $productTab == 'base_info') active @endif" href="{{route('Business::product@showBaseInfo',[$product->id])}}" >--}}
            {{--Thông tin Sản phẩm--}}
        {{--</a>--}}
    {{--</li>--}}
    <li class="nav-item m-tabs__item">
        <a class="nav-link m-tabs__link @if(isset($productTab) && $productTab == 'certificate_images') active @endif" href="{{ route('Business::product@getUploadCertificateImages', $product->id) }}">
            Ảnh chứng chỉ Sản phẩm
        </a>
    </li>
    <li class="nav-item m-tabs__item">
        <a class="nav-link m-tabs__link @if(isset($productTab) && $productTab == 'distributors') active @endif" href="{{route('Business::product@showDistributors',[$product->id])}}">
            Nhà phân phối
        </a>
    </li>
    {{--<li class="nav-item m-tabs__item">--}}
        {{--<a class="nav-link m-tabs__link @if(isset($productTab) && $productTab == 'sale_points') active @endif" href="{{ action('Admin\ProductController@salePoints', $product->id) }}" >--}}
            {{--Điểm Bán--}}
        {{--</a>--}}
    {{--</li>--}}
    {{--<li class="nav-item m-tabs__item">--}}
        {{--<a class="nav-link m-tabs__link @if(isset($productTab) && $productTab == 'product_relate') active @endif" href="{{ action('Admin\ProductController@relateProducts', $product->id) }}" >--}}
            {{--Sản Phẩm Liên Quan--}}
        {{--</a>--}}
    {{--</li>--}}
</ul>
