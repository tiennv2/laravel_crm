@extends('qrcode.layouts.app')
@section('body_class', 'm-page--fluid m-page--loading-enabled m-page--loading m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default')
@push('scripts')
    <script type="text/javascript">
        <?php
            $sharedData = [
                'config' => [
                    'csrfToken' => csrf_token(),
                    'logoutUrl' => route('logout'),
                    'isNative' => true,
                ],
                'loggedInAccount' => auth()->check() ? auth()->user() : null,
            ];
            ?>
            window.SHARED_DATA = {!! json_encode($sharedData) !!}
    </script>
    <script src="https://cdn.jsdelivr.net/npm/tinymce@4.7.11/tinymce.min.js" type="text/javascript"></script>
    <script src="{{ mix('js/app.js') }}"></script>
@endpush

@section('content')
    <!-- begin::Page loader -->
    <div class="m-page-loader m-page-loader--base">
        <div class="m-blockui">
      <span>
        Please wait...
      </span>
            <span>
        <div class="m-loader m-loader--brand"></div>
      </span>
        </div>
    </div>
    <!-- end::Page Loader -->
    <div id="app" class="m-grid m-grid--hor m-grid--root m-page">
        <app-header></app-header>
        <div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop  m-body m-page__container">
            <div class="m-grid__item m-grid__item--fluid m-wrapper">
                <div class="m-content">
                    <div class="m-portlet">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text">
                                        <a href="{{route('Business::product@index')}}">
                                            <i class="la la-arrow-left"></i>
                                        </a>
                                        Chi tiết Nhà phân phối sản phẩm "{{$product->name}}"
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__body">
                            @include('business.product.shared.tabs', ['productTab' => 'distributors'])
                            <div class="tab-content">
                                <div class="m-portlet__body">
                                    @if (session('success'))
                                        <div class="m-alert m-alert--outline m-alert--square m-alert--outline-2x alert alert-success alert-dismissible fade show"
                                             role="alert">
                                            <button type="button" class="close" data-dismiss="alert"
                                                    aria-label="Close">
                                            </button>
                                            {{ session('success') }}
                                        </div>
                                    @endif
                                    <div class="table-responsive">
                                        <table class="table table-striped- table-bordered table-hover table-checkable"
                                               style="margin-top: 20px;margin-left:auto;margin-right:auto;width: 1600px">
                                            <!--begin::Thead-->
                                            <thead>
                                            <tr>
                                                <th style="display: none"></th>
                                                <th style="width: 300px;font-weight: bold">Tên</th>
                                                <th style="font-weight: bold;width: 300px">Địa chỉ</th>
                                                <th style="font-weight: bold;width: 150px">Email</th>
                                                <th style="width: 150px;font-weight: bold">Số điện thoại</th>
                                                <th style="font-weight: bold;width: 150px">Website</th>
                                                <th style="font-weight: bold;width: 150px">Tiêu đề</th>
                                                <th style="font-weight: bold;width: 150px">Trạng thái</th>
                                                <th style="font-weight: bold;width: 150px">Hành động</th>
                                            </tr>
                                            </thead>
                                            <!--end::Thead-->
                                            <!--begin::Tbody-->
                                            <tbody>
                                            @foreach($product_distributors as $row)
                                                <tr>
                                                    <td class="id" style="display: none">{{$row->id}}</td>
                                                    <td>{{$row->distributor_name}}</td>
                                                    <td>{{$row->distributor_address}}</td>
                                                    <td>{{$row->distributor_email}}</td>
                                                    <td>{{$row->distributor_phone}}</td>
                                                    <td>{{$row->distributor_website}}</td>
                                                    <td>{{$row->title_name}}</td>
                                                    <td>
                                                        @if($row->status==0)
                                                            <span class="status m-badge m-badge--warning m-badge--wide">Chờ duyệt</span>
                                                        @elseif($row->status==1)
                                                            <span class="status m-badge m-badge--success m-badge--wide">Đã duyệt </span>
                                                        @else
                                                            <span class="status m-badge m-badge--danger m-badge--wide"><a
                                                                        data-toggle="modal"
                                                                        data-target="#note{{$row->id}}">Đã hủy duyệt </a></span>
                                                            <!--Modal update Note-->
                                                            <div id="note{{$row->id}}" class="modal fade"
                                                                 role="dialog">
                                                                <div class="modal-dialog">
                                                                    <!-- Modal content-->
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h3>Nguyên nhân hủy duyệt Nhà phân
                                                                                phối {{$row->distributor_name}}</h3>
                                                                            <button type="button" class="close"
                                                                                    data-dismiss="modal">&times;
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <div class="form-group m-form__group row">
                                                                        <textarea class="form-control" rows="5" readonly
                                                                                  name="note">{{$row->note}}</textarea>
                                                                            </div>
                                                                        </div>

                                                                        <div class="modal-footer">
                                                                            <button type="button"
                                                                                    class="btn btn-default"
                                                                                    data-dismiss="modal">Đóng
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--End Modal-->
                                                        @endif
                                                    </td>
                                                    <td>
                                                        {{--@if($row->synced==0)--}}
                                                        {{--<a href="{{route('Business::productDistributor@approve',[$row->id])}}"--}}
                                                        {{--title="Duyệt"--}}
                                                        {{--class="btn btn-success m-btn m-btn--icon m-btn--icon-only m-btn--pill">--}}
                                                        {{--<i class="la la-check"></i>--}}
                                                        {{--</a>--}}
                                                        {{--@endif--}}
                                                        @can("GATEWAY-approveDistributor")
                                                            <button
                                                                    @if($row->status==1)
                                                                    style="display: none;"
                                                                    @endif
                                                                    title="Duyệt"
                                                                    type="button"
                                                                    @if(auth()->user()->cannot('GATEWAY-approveDistributor'))
                                                                    disabled
                                                                    @endif
                                                                    class="approve btn btn-success m-btn m-btn--icon m-btn--icon-only m-btn--pill">
                                                                <i class="la la-check"></i>
                                                            </button>
                                                        @endcan
                                                        @can("GATEWAY-disapproveDistributor")
                                                            <button
                                                                    @if($row->status==2)
                                                                    style="display: none;"
                                                                    @endif
                                                                    type="button"
                                                                    @if(auth()->user()->cannot('GATEWAY-disapproveDistributor'))
                                                                    disabled
                                                                    @endif
                                                                    data-toggle="modal"
                                                                    data-target="#disapprove{{$row->id}}"
                                                                    title="Bỏ duyệt"
                                                                    class="disapprove btn btn-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill">
                                                                <i class="la la-remove"></i>
                                                            </button>
                                                        @endcan
                                                    <!--Modal Comfirm disapprove-->
                                                        <div id="disapprove{{$row->id}}" class="modal fade"
                                                             role="dialog">
                                                            <div class="modal-dialog">
                                                                <!-- Modal content-->
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h3>Bạn chắc
                                                                            chắn muốn hủy duyệt Nhà phân phối
                                                                            "{{$row->distributor_name}}"?</h3>
                                                                        <button type="button" class="close"
                                                                                data-dismiss="modal">&times;
                                                                        </button>
                                                                    </div>
                                                                    <form method="post"
                                                                          action="{{route('Business::productDistributor@disapprove',[$row->id])}}">
                                                                        {{ csrf_field() }}
                                                                        <div class="modal-body">
                                                                            <div class="form-group m-form__group row">
                                                                                <textarea class="form-control" rows="5"
                                                                                          placeholder="Nhập nguyên nhân hủy duyệt Nhà phân phối"
                                                                                          name="note"
                                                                                          required></textarea>
                                                                            </div>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="submit"
                                                                                    class="btn btn-success">
                                                                                Đồng ý
                                                                            </button>
                                                                            <button type="button"
                                                                                    class="btn btn-default"
                                                                                    data-dismiss="modal">Đóng
                                                                            </button>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--End Modal-->
                                                        {{--<button--}}
                                                        {{--title="Xóa bỏ"--}}
                                                        {{--type="button"--}}
                                                        {{--class="delete btn btn-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill">--}}
                                                        {{--<i class="fa fa-trash-o"></i>--}}
                                                        {{--</button>--}}
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                            <!--end::Tbody-->
                                        </table>
                                    </div>
                                    <div style="float:right;"><?php echo $product_distributors->links(); ?></div>
                                    <!--end::Table-->
                                </div>
                                <!--End::Section-->

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript" src="{{ asset('qr_code/metronic/js/vendors.bundle.js') }}"></script>
    <script>
        function checkSync(id) {
            var check_url = "{{ route('Business::productDistributor@checkSync',':id')}}";
            var checkResult = null;
            check_url = check_url.replace(':id', id);
            $.ajax({
                url: check_url,
                method: 'GET',
                async: false,
                success: function (data) {
                    checkResult = parseInt(data);
                }
            });
            return checkResult;
        }

        //Approve product-distributor
        $(".approve").click(function () {
            var approve_button = $(this);    // Find the row
            var status = approve_button.closest("tr").find(".status"); // Find the text
            status.html('Đang duyệt');
            status.addClass("m-loader m-loader--primary m-loader--left");
            status.css('padding-left', '32px');
            var id = approve_button.closest("tr").find(".id").text();
            var approve_url = "{{ route('Business::productDistributor@approve',':id')}}";
            approve_url = approve_url.replace(':id', id);
            $.ajax({
                url: approve_url,
                method: 'GET',
                async: false,
            });

            var interval_obj = setInterval(function () {
                let checkResult = checkSync(id);
                if (checkResult === 1) {
                    clearInterval(interval_obj);
                    status.removeClass();
                    status.addClass("status m-badge m-badge--success m-badge--wide");
                    status.css('padding-left', '10px');
                    status.html('Đã duyệt');
                    approve_button.css('display', 'none');
                    let disapprove_button = approve_button.closest("td").find(".disapprove");
                    disapprove_button.css('display', 'inline-block');
                }
            }, 2000);

        });
        //Disapprove product-distributor
        {{--$(".disapprove").click(function () {--}}
        {{--var disapprove_button = $(this);    // Find the row--}}
        {{--var status = disapprove_button.closest("tr").find(".status"); // Find the text--}}
        {{--status.html('Đang hủy duyệt');--}}
        {{--status.addClass("m-loader m-loader--primary m-loader--left");--}}
        {{--status.css('padding-left', '32px');--}}
        {{--var id = disapprove_button.closest("tr").find(".id").text();--}}
        {{--var disapprove_url = "{{ route('Business::productDistributor@disapprove',':id')}}";--}}
        {{--disapprove_url = disapprove_url.replace(':id', id);--}}
        {{--$.ajax({--}}
        {{--url: disapprove_url,--}}
        {{--method: 'GET',--}}
        {{--async: false,--}}
        {{--});--}}
        {{--var interval_obj = setInterval(function () {--}}
        {{--let checkResult = checkSync(id);--}}
        {{--if (checkResult === 0) {--}}
        {{--clearInterval(interval_obj);--}}
        {{--status.removeClass("m-badge m-badge--warning m-badge--wide m-loader m-loader--primary m-loader--left");--}}
        {{--status.addClass("status m-badge m-badge--warning m-badge--wide");--}}
        {{--status.css('padding-left', '10px');--}}
        {{--status.html('Chờ duyệt');--}}
        {{--disapprove_button.css('display', 'none');--}}
        {{--let approve_button = disapprove_button.closest("td").find(".approve");--}}
        {{--approve_button.css('display', 'inline-block');--}}
        {{--}--}}
        {{--}, 2000);--}}

        {{--});--}}

        //Delete product-distributor
        $(".delete").click(function () {
            var delete_button = $(this);    // Find the row
            var id = delete_button.closest("tr").find(".id").text();
            var delete_url = "{{ route('Business::productDistributor@delete',':id')}}";
            delete_url = delete_url.replace(':id', id);
            $.ajax({
                url: delete_url,
                method: 'GET',
                async: false,
            });
            var interval_obj = setInterval(function () {
                let checkResult = checkSync(id);
                if (checkResult === -1) {
                    delete_button.closest("tr").remove();
                    clearInterval(interval_obj);
                }
            }, 1000);

        });

    </script>
@endpush
