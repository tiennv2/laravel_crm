@extends('qrcode.layouts.app')
@push('styles')
  <style>
    .heroes {
      margin: 0 0 2em 0;
      padding: 0;
      width: 30.5em;
    }

    .heroes li {
      position: relative;
      left: 0;
      color: #00c5dc;
      margin: .5em;
      padding: .3em .3em;
      height: auto;
      min-height: 3em;
      border-radius: 4px;
      border-color: #00c5dc;
      border-width: 2px;
      border-style: solid;
    }

    .attribute_title {
      font-weight: bold;
      text-decoration: underline;
      font-size: 16px;
    }

    .select2 {
      width: 100% !important;
    }
  </style>
@endpush
@section('body_class', 'm-page--fluid m-page--loading-enabled m-page--loading m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default')

@push('scripts')
  <script type="text/javascript">
    <?php
      $sharedData = [
        'config' => [
          'csrfToken' => csrf_token(),
          'logoutUrl' => route('logout'),
          'isNative' => true,
        ],
        'loggedInAccount' => auth()->check() ? auth()->user() : null,
      ];
      ?>
      window.SHARED_DATA = {!! json_encode($sharedData) !!}
  </script>
  <script src="https://cdn.jsdelivr.net/npm/tinymce@4.7.11/tinymce.min.js" type="text/javascript"></script>
  <script src="{{ mix('js/app.js') }}"></script>
@endpush

@section('content')
  <!-- begin::Page loader -->
  <div class="m-page-loader m-page-loader--base">
    <div class="m-blockui">
      <span>
        Please wait...
      </span>
      <span>
        <div class="m-loader m-loader--brand"></div>
      </span>
    </div>
  </div>
  <!-- end::page Loader -->
  <div id="app" class="m-grid m-grid--hor m-grid--root m-page">
    <app-header></app-header>
    <div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop  m-body m-page__container">
      <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader">
          <div class="d-flex align-items-center">
            <div class="mr-auto">
              <h3 class="m-subheader__title">
                YÊU CẦU QUẢN LÝ SẢN PHẨM
              </h3>
              @can("GATEWAY-viewProduct")
                <a href="{{route('Business::product@index')}}" target="_blank"
                   class="btn btn-outline-brand m-btn btn-sm m-btn--icon m-btn--pill"><span>
                                    <span
                                      style="font-size: 14px">Xem danh sách Sản phẩm đã được Doanh nghiệp quản lý</span></span></a>
              @endcan
            </div>
            @if($waiting_productRequest_count > 0)
              <div style="border: solid #bf800c;padding: 3px"><a
                  href="{{route('Business::productRequest@index')}}?status=waiting"
                  style="color: #bf800c">Có
                  <span>{{$waiting_productRequest_count}}</span> yêu cầu quản lý sản phẩm đang chờ
                  duyệt</a></div>
            @endif
          </div>
        </div>
        <!-- END: Subheader -->
        <div class="m-content">
          <!--Begin::Section-->
          <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__body">
              <form class="m-form m-form--fit m--margin-bottom-20"
                    action="{{route('Business::productRequest@index')}}" method="get">
                <div class="row m--margin-bottom-20">
                  <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                    <label>
                      Tìm kiếm theo doanh nghiệp:
                    </label>
                    <select class="form-control m-input" data-col-index="7" id="business_id"
                            name="business_id">
                      <option value="" selected="selected"></option>
                      @foreach($businesses as $business)
                        <option value="{{$business->id}}"
                          {{Request::input('business_id')==$business->id?'selected="selected"':''}}
                        >{{$business->name}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                    <label>
                      Mã barcode:
                    </label>
                    <input type="text" class="form-control m-input" name="barcode"
                           value="{{ Request::input('barcode') }}"
                           data-col-index="4">
                  </div>
                  <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                    <label>
                      Tên Sản phẩm:
                    </label>
                    <input type="text" class="form-control m-input" name="name"
                           value="{{ Request::input('name') }}"
                           data-col-index="4">
                  </div>
                  <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                    <label>
                      Chọn trạng thái:
                    </label>
                    <select class="form-control m-input" data-col-index="7" id="status"
                            name="status">
                      <option value="" selected="selected">--Tất cả--</option>
                      <option value="0" {{Request::input('status')==="0"?'selected="selected"':''}}>
                        Chờ duyệt
                      </option>
                      <option value="1" {{Request::input('status')==="1"?'selected="selected"':''}}>
                        Đã duyệt
                      </option>
                      <option value="2" {{Request::input('status')==2?'selected="selected"':''}}>
                        Cần cung cấp giấy tờ chứng minh
                      </option>
                      <option value="3" {{Request::input('status')==3?'selected="selected"':''}}>
                        Đã hủy duyệt
                      </option>
                    </select>
                  </div>
                </div>
                <div class="row m--margin-bottom-20">
                  <div class="col-lg-4 m--margin-bottom-10-tablet-and-mobile">
                    <label>
                      Ngày tạo:
                    </label>
                    <div class="input-daterange input-group">
                      <input type="text" class="form-control" name="from" id="m_datepicker_1"
                             value="{{ Request::input('from') }}" autocomplete="off"
                             placeholder="Từ"
                             data-col-index="5"/>
                      <div class="input-group-append">
													<span class="input-group-text">
														<i class="la la-ellipsis-h"></i>
													</span>
                      </div>
                      <input type="text" class="form-control" name="to" id="m_datepicker_1"
                             value="{{ Request::input('to') }}" autocomplete="off"
                             placeholder="Đến"
                             data-col-index="5"/>
                    </div>
                  </div>
                  <div class="col-lg-4 m--margin-bottom-10-tablet-and-mobile">
                    <label style="color: white">Hành động </label>
                    <div>
                      <button class="btn btn-brand m-btn m-btn--icon" type="submit">
												<span>
													<i class="la la-search"></i>
													<span>
														Tìm kiếm
													</span>
												</span>
                      </button>
                      &nbsp;&nbsp;
                      <button class="btn btn-secondary m-btn m-btn--icon" type="button"
                              onclick="window.location='{{route('Business::productRequest@index')}}'">
												<span>
													<i class="la la-refresh"></i>
													<span>
														Làm mới
													</span>
												</span>
                      </button>
                    </div>
                  </div>
                </div>
              </form>
              @if (session('success'))
                <div
                  class="m-alert m-alert--outline m-alert--square m-alert--outline-2x alert alert-success alert-dismissible fade show"
                  role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  </button>
                  {{ session('success') }}
                </div>
              @endif
              @if (session('message'))
                <div
                  class="m-alert m-alert--outline m-alert--square m-alert--outline-2x alert alert-danger alert-dismissible fade show"
                  role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  </button>
                  {{ session('message') }}
                </div>
              @endif
              <div
                style="width:200px;text-align:center;padding:5px;margin-bottom: 0;margin-top: 50px;margin-left: 50px;font-weight: bold;font-style: italic;border: solid 1px;border-radius: 6px">
                Đang hiển thị {{$count_record_in_page}}/{{$count_record_total}} bản ghi
              </div>
              <div class="table-responsive">
                <table class="table table-striped- table-bordered table-hover table-checkable"
                       style="margin-top: 20px;margin-left:auto;margin-right:auto;">
                  <!--begin::Thead-->
                  <thead>
                  <tr>
                    <th style="font-weight: bold">Id</th>
                    <th style="width: 100px;font-weight: bold">Mã barcode</th>
                    <th style="width: 350px;font-weight: bold">Tên sản phẩm</th>
                    <th style="font-weight: bold;width: 400px">Doanh nghiệp</th>
                    <th style="font-weight: bold;width: 100px">Giấy tờ</th>
                    <th style="font-weight: bold;width: 150px">Trạng thái</th>
                    <th style="font-weight: bold">Ngày tạo</th>
                    <th style="font-weight: bold;min-width: 250px">Hành động</th>
                  </tr>
                  </thead>
                  <!--end::Thead-->
                  <!--begin::Tbody-->
                  <tbody>
                  @foreach($productRequests as $productRequest)
                    @php $data = $productRequest->data @endphp
                    <tr>
                      <td class="id">{{$productRequest->id}}</td>
                      <td>
                                                <span
                                                  @if($productRequest->validBarcode == 0) title="Mã sai chuẩn định dạng"
                                                  style="color: red" @endif>{{$productRequest->barcode}}</span></td>
                      <td>
                        {{$data['name']}}
                      </td>
                      <td>
                        <a href="{{route('Business::businesses@index')}}?business_id={{$productRequest->business_id}}"
                           target="_blank">
                          {{$productRequest->business_name}}
                        </a>
                      </td>
                      <td>
                        @can("GATEWAY-viewRequestProduct")
                          <button
                            type="button"
                            value="{{$data['certificate_id']}}"
                            data-target="#file{{$productRequest->id}}"
                            class="files_modal btn m-btn--pill m-btn--air btn-outline-info btn-sm"
                            id="upload_files_{{$productRequest->id}}"
                            data-toggle="modal">Chi tiết
                          </button>
                        @endcan
                        <input type="hidden" id="{{$productRequest->id}}"
                               value="{{$productRequest->business_id}}">
                        <!--Modal-->
                        <div id="file{{$productRequest->id}}" class="modal fade" tabindex="-1"
                             role="dialog">
                          <div class="modal-dialog modal-lg" role="document">
                            <!-- Modal content-->
                            <div class="modal-content">
                              <div class="modal-header">
                                <h1 class="modal-title">Thông tin sản phẩm có barcode
                                  "{{$productRequest->barcode}}"</h1>
                                <button type="button" class="close"
                                        data-dismiss="modal">&times;
                                </button>
                              </div>
                              <form method="POST"
                                    action="{{ route('Business::productRequest@updateFile')}}">
                                <div class="modal-body"
                                     style="height:400px;overflow-y: scroll;">
                                  <input type="hidden" name="productRequest_id"
                                         value="{{$productRequest->id}}">
                                  <div class="upload_files_{{$productRequest->id}}_select">
                                  </div>
                                  <div style="border-bottom: solid forestgreen 2px">
                                    <ol class="heroes upload_files_{{$productRequest->id}}_input_list">

                                    </ol>
                                  </div>
                                  <div style="padding-top: 5px">
                                    <p>
                                      <span class="attribute_title">MÃ BARCODE:</span> {{$productRequest->barcode}}
                                    </p>
                                    @if($data)
                                      <p>
                                        <span class="attribute_title">TÊN SẢN PHẨM:</span> {{$data['name']}}
                                      </p>
                                      @if($data['images'])
                                        <div class="attribute_title">ẢNH SẢN
                                          PHẨM:
                                        </div>
                                        <p>
                                          @foreach($data['images'] as $image)
                                            <span style="margin: 5px;padding:5px"><img
                                                src="{{$image}}"
                                                width="200px"></span>
                                          @endforeach
                                        </p>
                                      @endif
                                      @if($data['certificate_images'])
                                        <div class="attribute_title">ẢNH CHỨNG
                                          CHỈ
                                          SẢN PHẨM:
                                        </div>
                                        <p>
                                          @foreach($data['certificate_images'] as $certificate_image)
                                            <span style="margin: 5px;padding:5px"><img
                                                src="{{$certificate_image}}"
                                                width="200px"></span>
                                          @endforeach
                                        </p>
                                      @endif
                                      <p>
                                        <span
                                          class="attribute_title">GIÁ SẢN PHẨM:</span> {{number_format($data['price'],0,".",",")}}
                                      </p>
                                      @if($data['attributes'])
                                        @foreach($data['attributes'] as $key => $value)
                                          <p><span class="attribute_title">{{$key}}
                                              :</span> @php echo preg_replace('/(<br\s*\/?>\s*)+/', '<br>', nl2br($value)) @endphp
                                          </p>
                                        @endforeach
                                      @endif
                                    @endif
                                  </div>
                                </div>
                                <div class="modal-footer">
                                  @can("GATEWAY-approveProductRequest")
                                    @if($productRequest->managed != 1 && $productRequest->subscriptionStatus == 1)
                                      <button type="button" data-dismiss="modal"
                                              class="approveProductRequest_modalButton btn m-btn--square btn-primary btn-sm">
                                        Duyệt
                                      </button>
                                      <button type="button" data-dismiss="modal"
                                              class="approveAndVerifyProductRequest_modalButton btn m-btn--square btn-success btn-sm">
                                        Duyệt + Xác thực
                                      </button>
                                    @endif
                                  @endcan
                                  @if($productRequest->status != 3 && $productRequest->managed != 1)
                                    @can("GATEWAY-updateRequestProductCertificate")
                                      <button type="submit"
                                              class="btn m-btn--square  m-btn m-btn--gradient-from-info m-btn--gradient-to-accent btn-sm">
                                        Cập nhật giấy tờ
                                      </button>
                                    @endcan
                                  @endif
                                  <button type="button"
                                          class="btn btn-default btn-sm"
                                          data-dismiss="modal">Đóng
                                  </button>
                                </div>
                              </form>
                            </div>
                          </div>
                        </div>
                        <!--End Modal-->
                      </td>
                      <td>
                        @if($productRequest->status==0)
                          <span class="status m-badge m-badge--warning m-badge--wide">Chờ duyệt </span>
                        @elseif($productRequest->status==1)
                          <span class="status m-badge m-badge--success m-badge--wide">Đã duyệt </span>
                        @elseif($productRequest->status==2)
                          <span
                            class="status m-badge m-badge--metal m-badge--wide">Cần cung cấp giấy tờ chứng minh</span>
                        @else
                          <span class="status m-badge m-badge--danger m-badge--wide">
                                                        <a data-toggle="modal"
                                                           data-target="#note{{$productRequest->id}}">
                                                            Đã hủy duyệt</a></span>
                          <!--Modal Note-->
                          <div id="note{{$productRequest->id}}" class="modal fade"
                               role="dialog">
                            <div class="modal-dialog">
                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h3>Nguyên nhân hủy duyệt yêu cầu quyền quản lý Sản
                                    phẩm</h3>
                                  <button type="button" class="close"
                                          data-dismiss="modal">&times;
                                  </button>
                                </div>
                                <div class="modal-body">
                                  <div class="form-group m-form__group row">
                                                                        <textarea class="form-control" rows="5" readonly
                                                                                  name="note">{{$productRequest->note}}</textarea>
                                  </div>
                                </div>
                                <div class="modal-footer">
                                  <button type="button"
                                          class="btn btn-default"
                                          data-dismiss="modal">Đóng
                                  </button>
                                </div>
                              </div>
                            </div>
                          </div>
                          <!--End Modal-->
                        @endif
                      </td>
                      <td>
                        {{date("d/m/Y",strtotime($productRequest->created_at))}}
                      </td>
                      <td>
                        @if($productRequest->managed != 1 && $productRequest->subscriptionStatus == 1)
                          {{--@can("GATEWAY-approveProductRequest")--}}
                          {{--<a href="{{route('Business::productRequest@approve',[$productRequest->id])}}"--}}
                          {{--title="Duyệt"--}}
                          {{--class="btn btn-success m-btn m-btn--icon m-btn--icon-only m-btn--pill">--}}
                          {{--<i class="la la-check"></i>--}}
                          {{--</a>--}}
                          {{--@endcan--}}
                          @can("GATEWAY-approveProductRequest")
                            <button
                              title="Duyệt,chưa xác thực"
                              type="button"
                              class="approve btn m-btn--square  btn-primary btn-sm">
                              Duyệt
                            </button>
                            <button
                              title="Duyệt+Xác thực"
                              type="button"
                              class="approveAndVerify btn m-btn--square  btn-success btn-sm">
                              Duyệt + Xác thực
                            </button>
                          @endcan
                        @elseif($productRequest->status !=1 && $productRequest->subscriptionStatus == 0)
                          {{"Gói dịch vụ đã hết hạn hoặc đã được sử dụng hết!"}}
                          <br>
                          <a
                            href="{{route('Business::subscription@index')}}?business_id={{$productRequest->business_id}}"
                            target="_blank" class="m-nav__link">
                            <span class="m-nav__link-text">Xem danh Gói dịch vụ sử dụng</span>
                          </a>
                        @endif
                        @if($productRequest->status !=1 && $productRequest->status !=3 && $productRequest->subscriptionStatus == 1)
                          @can("GATEWAY-disapproveProductRequest")
                            <button type="button"
                                    title="Từ chối"
                                    data-toggle="modal"
                                    data-target="#delete{{$productRequest->id}}"
                                    class="disapprove btn m-btn--square btn-danger btn-sm">
                              Từ chối
                            </button>
                          @endcan
                        <!--Modal Comfirm delete-->
                          <div id="delete{{$productRequest->id}}" class="modal fade"
                               role="dialog">
                            <div class="modal-dialog">
                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h2>Bạn chắc
                                    chắn muốn hủy bỏ yêu cầu quản lý sản phẩm của
                                    doanh nghiệp "{{$productRequest->business_name}}
                                    "?</h2>
                                  <button type="button" class="close"
                                          data-dismiss="modal">&times;
                                  </button>
                                </div>
                                <form method="get"
                                      action="{{route('Business::productRequest@disapprove',[$productRequest->id])}}">
                                  {{ csrf_field() }}
                                  <div class="modal-body">
                                    <div class="form-group m-form__group row">
                                                                                <textarea class="form-control" rows="5"
                                                                                          placeholder="Nhập lý do hủy bỏ yêu cầu quyền quản lý sản phẩm. Hệ thống sẽ tự động gửi email thông tin cho doanh nghiệp về lý do được đưa ra!"
                                                                                          name="note"
                                                                                          required></textarea>
                                    </div>
                                    <h3>Chú ý:</h3> Nếu hủy bỏ yêu cầu quản lý Sản
                                    phẩm
                                    của Doanh nghiệp thì Doanh nghiệp sẽ không thể
                                    thao
                                    tác hay
                                    cập nhật bất kỳ thông tin gì liên quan đến Sản
                                    phẩm
                                    nữa. Trường hợp Doanh nghiệp muốn quản lý sản
                                    phẩm,
                                    phải gửi lại yêu cầu quản lý Sản phẩm.
                                  </div>
                                  <div class="modal-footer">
                                    <button type="submit"
                                            class="submit_button btn btn-success">
                                      Đồng ý
                                    </button>
                                    <button type="button"
                                            class="btn btn-default"
                                            data-dismiss="modal">Đóng
                                    </button>
                                  </div>
                                </form>
                              </div>
                            </div>
                          </div>
                          <!--End Modal-->
                        @endif
                      </td>
                    </tr>
                  @endforeach
                  </tbody>
                  <!--end::Tbody-->
                </table>
              </div>
              <!--end::Table-->
              <div style="float:right;"><?php echo $productRequests->links(); ?></div>
            </div>
            <!--End::Section-->
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
@push('scripts')
  <script type="text/javascript" src="{{ asset('qr_code/metronic/js/vendors.bundle.js') }}"></script>
  <script type="text/javascript" src="{{ asset('qr_code/metronic/js/datepicker/bootstrap-datepicker.js') }}"></script>
  <script type="text/javascript">
    $(document).ready(function () {
      // Basic
      $("#status").select2({
        placeholder: 'Chọn trạng thái',
        allowClear: true
      });
      $("#business_id").select2({
        placeholder: 'Chọn doanh nghiệp',
        allowClear: true
      });
      $("select").on("select2:select", function (evt) {
        var element = evt.params.data.element;
        var $element = $(element);
        $element.detach();
        $(this).append($element);
        $(this).trigger("change");
      });
      $(".modal").on('hidden.bs.modal', function () {
        document.body.style.padding = 0;
      });
    });

    $(".submit_button").click(function () {
      $(this).attr("disabled", "disabled");
      $(this).closest("form").submit();
    });

    //Fill Data to Modal
    $(".files_modal").click(function () {
      let id = $(this).attr('id');
      let product_id = id.split("_").pop();
      let business_id = $(`#${product_id}`).val();
      let spinner = `<div class="spinner m-loader m-loader--primary" style="width: 30px; display: inline-block;">
                            </div>`;
      $(`.${id}_input_list`).empty();
      $(`.${id}_input_list`).append(`${spinner}`);
      $(`.${id}_select`).empty();
      let value = $(this).attr('value');
      let html = `
                <div class='select_box'>
                 <select class="form-control m-input"
                 data-col-index="12"
                  id="business_id_${product_id}"
                  name="select_id"
                  required>
                  <option value=""></option>
                </select>
                </div>
                `;
      if (value) {
        appendCurrentFiles(id, value);
      } else {
        $(`.${id}_input_list`).append("Chưa có giấy tờ!");
      }
      $(`.${id}_select`).append(`${html}`);
      $(`#business_id_${product_id}`).select2({
        placeholder: 'Thay đổi files giấy tờ bằng click chọn một trong các giấy tờ sau',
        allowClear: true,
        width: '30.5em'
      });
      appendSelectBox(business_id);
      $("div.select_box select").change(function () {
        $(`.${id}_input_list`).empty();
        let file = JSON.parse($(this).val());
        appendData(id, file);
        let certificate_id = $("div.select_box select option:selected").attr('data-key');
        $(`.${id}_input_list`).append(`<input type="hidden" name="certificate_id" value="${certificate_id}">`);
      });
    });

    function appendData(id, file) {
      for (let i = 0; i < file.length; i++) {
        let html = `<li>
                                <div style="text-align: left;display: inline-block;width: 70%">${file[i][0]}</div>
                                <div style="text-align: right;display: inline-block;float: right;">
                                    <a href="${file[i][2]}" target="_blank" data-toggle="tooltip" title="Xem" class="btn btn-outline-accent m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air"> <i class="la la-eye"></i></a>
                                </div>
                           </li>`;
        $(`.${id}_input_list`).append(`${html}`);
      }
    }

    function appendSelectBox(business_id) {
      let url = "{{ route('Business::certificate@certificatesByBusinessId')}}";
      let token = $("input[name='_token']").val();
      $.ajax({
        url: url,
        method: 'POST',
        data: {
          business_id: business_id,
          _token: token
        },
        success: function (data) {
          $.each(data, function (key, value) {
            $("select[name='select_id']").append(
              `<option value='${value.files}' data-key='${value.id}'>${value.name}</option>`
            );
          });
          $("div").remove(".spinner");
        }
      });
    }

    function appendCurrentFiles(id, certificate_id) {
      console.log(certificate_id);
      let url = "{{ route('Business::gln@getFilesById')}}";
      let token = $("input[name='_token']").val();
      $.ajax({
        url: url,
        method: 'POST',
        data: {
          certificate_id: certificate_id,
          _token: token
        },
        success: function (data) {
          if (data) {
            let file = JSON.parse(data);
            appendData(id, file);
          }
        }
      });
    }

    //Approve product
    $(".approve").click(function () {
      var row = $(this).closest("tr");    // Find the row
      var status = row.find(".status"); // Find the text
      $(this).prop('disabled', true);
      $(this).siblings(".disapprove").prop('disabled', true);
      $(this).siblings(".approveAndVerify").prop('disabled', true);
      status.html('Đang duyệt');
      status.addClass("m-loader m-loader--primary m-loader--left");
      status.css('padding-left', '32px');
      var id = row.find(".id").text();
      var approve_url = "{{ route('Business::productRequest@approve')}}";
      // approve_url = approve_url.replace(':id', id);
      $.ajax({
        url: approve_url,
        method: 'POST',
        data: {
          id: id,
        },
        success: function (data) {
          if (data['messages'] !== 'Request sent!') {
            alert(data['messages']);
            status.removeClass();
            status.addClass("status m-badge m-badge--danger m-badge--wide");
            status.css('padding-left', '10px');
            status.html('Đã hủy duyệt');
          }
        }
      });

      function checkStatus(id) {
        var check_url = "{{ route('Business::productRequest@checkStatus',':id')}}";
        var status = null;
        check_url = check_url.replace(':id', id);
        $.ajax({
          url: check_url,
          method: 'GET',
          async: false,
          success: function (data) {
            status = parseInt(data);
          }
        });
        if (status === 1) {
          return true;
        } else {
          return false;
        }
      }

      var interval_obj = setInterval(function () {
        let check = checkStatus(id);
        if (check) {
          clearInterval(interval_obj);
          status.removeClass();
          status.addClass("status m-badge m-badge--success m-badge--wide");
          status.css('padding-left', '10px');
          status.html('Đã duyệt');
          row.find(".approve").hide();
          row.find(".disapprove").hide();
        }
      }, 1000);
    });

    //ApproveAndVerify product
    $(".approveAndVerify").click(function () {
      var row = $(this).closest("tr");    // Find the row
      var status = row.find(".status"); // Find the text
      $(this).prop('disabled', true);
      $(this).siblings(".disapprove").prop('disabled', true);
      $(this).siblings(".approve").prop('disabled', true);
      status.html('Đang duyệt');
      status.addClass("m-loader m-loader--primary m-loader--left");
      status.css('padding-left', '32px');
      var id = row.find(".id").text();
      var approve_url = "{{ route('Business::productRequest@approve')}}";
      $.ajax({
        url: approve_url,
        method: 'POST',
        data: {
          id: id,
          verify: 1
        },
      });

      function checkStatus(id) {
        var check_url = "{{ route('Business::productRequest@checkStatus',':id')}}";
        var status = null;
        check_url = check_url.replace(':id', id);
        $.ajax({
          url: check_url,
          method: 'GET',
          async: false,
          success: function (data) {
            status = parseInt(data);
          }
        });
        if (status === 1) {
          return true;
        } else {
          return false;
        }
      }

      var interval_obj = setInterval(function () {
        let check = checkStatus(id);
        if (check) {
          clearInterval(interval_obj);
          status.removeClass();
          status.addClass("status m-badge m-badge--success m-badge--wide");
          status.css('padding-left', '10px');
          status.html('Đã duyệt');
          row.find(".approve").hide();
          row.find(".disapprove").hide();
        }
      }, 1000);
    });

    $(".approveProductRequest_modalButton").click(function () {
      var approveButton = $(this).closest("tr").find(".approve");
      approveButton.click();
    });

    $(".approveAndVerifyProductRequest_modalButton").click(function () {
      var approveButton = $(this).closest("tr").find(".approveAndVerify");
      approveButton.click();
    });

  </script>
@endpush
