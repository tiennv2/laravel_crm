<table>
  <thead>
  <tr>
    <th style="font-weight: bold">Thuộc tính</th>
    <th style="width: 50%;font-weight: bold; color: #0a8cf0">Mới</th>
    <th style="width: 50%; font-weight: bold">Cũ</th>
  </tr>
  </thead>
  <tbody>
  <tr>
  <tr>
    <td style="font-weight: bold;font-style: italic">Mã barcode</td>
    <td style="color: #0a8cf0">{{$product->barcode}}</td>
    <td>{{$stdProduct->barcode}}</td>
  </tr>
  <tr>
    <td style="font-weight: bold;font-style: italic">Tên sản phẩm</td>
    <td style="color: #0a8cf0">
      @if($product->product_info && array_key_exists('name', $product->product_info))
        {{$product->product_info['name']}}
      @endif
      </td>
    <td>{{$stdProduct->name}}</td>
  </tr>
  <tr>
    <td style="font-weight: bold;font-style: italic">Giá sản phẩm</td>
    <td style="color: #0a8cf0">
      @if($product->product_info && array_key_exists('price', $product->product_info) && $product->product_info['price'])
        {{number_format($product->product_info['price'],0,".",",")}}
      @endif
    </td>
    <td>{{number_format($stdProduct->price,0,".",",")}}</td>
  </tr>
  <tr>
    <td style="font-weight: bold;font-style: italic">Ảnh sản phẩm
    </td>
    <td>
      @if($product->product_info && array_key_exists('images', $product->product_info) && $product->product_info['images'])
        <p>
          @foreach($product->product_info['images'] as $image)
            <span style="margin: 5px;padding:5px"><img
                src="{{$image}}"
                width="50px"></span>
          @endforeach
        </p>
      @endif
    </td>
    <td>
      @if($stdProduct->images)
        <p>
          @foreach($stdProduct->images as $image)
            <span style="margin: 5px;padding:5px"><img
                src="{{$image}}"
                width="50px"></span>
          @endforeach
        </p>
      @endif
    </td>
  </tr>
  <tr>
    <td style="font-weight: bold;font-style: italic">Ảnh chứng chỉ sản phẩm</td>
    <td>
      @if($product->product_info && array_key_exists('certificate_images', $product->product_info) && $product->product_info['certificate_images'])
        <p>
          @foreach($product->product_info['certificate_images'] as $image)
            <span style="margin: 5px;padding:5px"><img
                src="{{$image}}"
                width="50px"></span>
          @endforeach
        </p>
      @endif
    </td>
    <td>
      @if($stdProduct->certificate_images)
        <p>
          @foreach($stdProduct->certificate_images as $image)
            <span style="margin: 5px;padding:5px"><img
                src="{{$image}}"
                width="50px"></span>
          @endforeach
        </p>
      @endif
    </td>
  </tr>
  @foreach($icheck_attributes as $attribute)
    <tr>
      <td style="font-weight: bold;font-style: italic">{{$attribute['title']}}</td>
      <td style="color: #0a8cf0">
        @if($product->product_info && array_key_exists('attributes', $product->product_info) && $product->product_info['attributes'])
          @foreach($product->product_info['attributes'] as $key => $value)
            @if($key == $attribute['id'] && $value != '')
              <p> @php echo preg_replace('/(<br\s*\/?>\s*)+/', '<br>', nl2br($value)) @endphp</p>
            @endif
          @endforeach
        @endif
      </td>
      <td>
        @if($stdProduct->attributes)
          @foreach($stdProduct->attributes as $key => $value)
            @if($key == $attribute['id'] && $value != '')
              <p> @php echo preg_replace('/(<br\s*\/?>\s*)+/', '<br>', nl2br($value)) @endphp</p>
            @endif
          @endforeach
        @endif
      </td>
    </tr>
  @endforeach
  </tbody>
</table>
