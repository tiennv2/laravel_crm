@extends('qrcode.layouts.app')
@push('styles')
  <style>
    .heroes {
      margin: 0 0 2em 0;
      padding: 0;
      width: 40.5em;
    }

    .heroes li {
      position: relative;
      left: 0;
      color: #00c5dc;
      margin: .5em;
      padding: .3em .3em;
      height: auto;
      min-height: 3em;
      border-radius: 4px;
      border-color: #00c5dc;
      border-width: 2px;
      border-style: solid;
    }

    .attribute_title {
      font-weight: bold;
      font-size: 16px;
    }

    img {
      cursor: zoom-in;
    }

    .select2 {
      width: 100% !important;
    }
  </style>
@endpush
@section('body_class', 'm-page--fluid m-page--loading-enabled m-page--loading m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default')

@push('scripts')
  <script type="text/javascript">
    <?php
      $sharedData = [
        'config' => [
          'csrfToken' => csrf_token(),
          'logoutUrl' => route('logout'),
          'isNative' => true,
        ],
        'loggedInAccount' => auth()->check() ? auth()->user() : null,
      ];
      ?>
      window.SHARED_DATA = {!! json_encode($sharedData) !!}
  </script>
  <script src="https://cdn.jsdelivr.net/npm/tinymce@4.7.11/tinymce.min.js" type="text/javascript"></script>
  <script src="{{ mix('js/app.js') }}"></script>
@endpush

@section('content')
  <!-- begin::Page loader -->
  <div class="m-page-loader m-page-loader--base">
    <div class="m-blockui">
      <span>
        Please wait...
      </span>
      <span>
        <div class="m-loader m-loader--brand"></div>
      </span>
    </div>
  </div>
  <!-- end::page Loader -->
  <div id="app" class="m-grid m-grid--hor m-grid--root m-page">
    <div class="m-header__bottom">
      <div class="m-container m-container--responsive m-container--xxl m-container--full-height m-page__container">
        <div class="m-stack m-stack--ver m-stack--desktop">
          <div class="m-stack__item m-stack__item--middle m-stack__item--fluid">
            <app-header></app-header>
          </div>
        </div>
      </div>
    </div>
    <div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop  m-body m-page__container">
      <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader">
          <div class="d-flex align-items-center">
            <div class="mr-auto">
              <h3 class="m-subheader__title">
                DANH SÁCH SẢN PHẨM DOANH NGHIỆP QUẢN LÝ
              </h3>
            </div>
            <div style="border: solid #bf800c;padding: 3px;margin-right: 5px">
              @if($waitingRoleApproveProduct_count > 0)
                <p style="margin-bottom:0">
                  <a
                    href="{{route('Business::business_product@index')}}?management_status={{\App\Models\Business\Business_Product::MANAGEMENT_STATUS_PENDING}}"
                    style="color: #bf800c">Có
                    {{$waitingRoleApproveProduct_count}} sản phẩm đang chờ
                    duyệt quyền quản lý</a>
                </p>
              @endif
              @if($waitingInfoApproveProduct_count > 0)
                <p style="margin-bottom:0">
                  <a
                    href="{{route('Business::business_product@index')}}?info_status={{\App\Models\Business\Business_Product::INFO_STATUS_PENDING}}&management_type={{\App\Models\Business\Business_Product::MANAGEMENT_TYPE_FULL_CONTROL}}&management_status={{\App\Models\Business\Business_Product::MANAGEMENT_STATUS_APPROVED}}"
                    style="color: #bf800c">Có
                    {{$waitingInfoApproveProduct_count}} sản phẩm đang chờ
                    duyệt thông tin</a>
                </p>
              @endif
              @if($needRecheck_product_count > 0)
                <p style="margin-bottom:0">
                  <a
                    href="{{route('Business::business_product@index')}}?need_recheck=1"
                    style="color: red">Có {{$needRecheck_product_count}} sản phẩm cần kiểm tra lại thông tin đã
                    duyệt</a>
                </p>
              @endif
            </div>
          </div>
          <div>
            @can("GATEWAY-viewHoliday")
              <a href="{{route('Business::holiday@index')}}" target="_blank"
                 class="btn btn-outline-brand m-btn btn-sm m-btn--icon m-btn--pill"><span>
                                    <span style="font-size: 14px">Xem thời gian tự động duyệt thông tin Sản phẩm</span></span></a>
            @endcan
          </div>
        </div>
        <!-- END: Subheader -->
        <div class="m-content">
          <!--Begin::Section-->
          <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__body">
              <form class="m-form m-form--fit m--margin-bottom-20"
                    action="{{route('Business::business_product@index')}}" method="get">
                <div class="row m--margin-bottom-20">
                  <div class="col-lg-4 m--margin-bottom-10-tablet-and-mobile">
                    <label>
                      Tìm kiếm theo doanh nghiệp:
                    </label>
                    <select class="form-control m-input" data-col-index="7" id="business_id"
                            name="business_id">
                      <option value="" selected="selected"></option>
                      @foreach($businesses as $business)
                        <option value="{{$business->id}}"
                          {{Request::input('business_id')==$business->id?'selected="selected"':''}}
                        >{{$business->name}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile">
                    <label>
                      Tên sản phẩm:
                    </label>
                    <input type="text" class="form-control m-input" name="name"
                           value="{{ Request::input('name') }}"
                           data-col-index="4">
                  </div>
                  <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile">
                    <label>
                      Mã barcode:
                    </label>
                    <input type="text" class="form-control m-input" name="barcode"
                           value="{{ Request::input('barcode') }}"
                           data-col-index="4">
                  </div>
                  <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile">
                    <label>
                      Trạng thái quản lý:
                    </label>
                    <select class="form-control m-input" data-col-index="7" id="management_status"
                            name="management_status">
                      <option value="" selected="selected">--Tất cả--</option>
                      <option
                        value="{{\App\Models\Business\Business_Product::MANAGEMENT_STATUS_PENDING}}" {{(!is_null(Request::input('management_status')) && Request::input('management_status')==\App\Models\Business\Business_Product::MANAGEMENT_STATUS_PENDING)?'selected="selected"':''}}>
                        Chờ duyệt
                      </option>
                      <option
                        value="{{\App\Models\Business\Business_Product::MANAGEMENT_STATUS_APPROVED}}" {{Request::input('management_status')==\App\Models\Business\Business_Product::MANAGEMENT_STATUS_APPROVED?'selected="selected"':''}}>
                        Đã duyệt
                      </option>
                      <option
                        value="{{\App\Models\Business\Business_Product::MANAGEMENT_STATUS_DISAPPROVED}}" {{Request::input('management_status')==\App\Models\Business\Business_Product::MANAGEMENT_STATUS_DISAPPROVED?'selected="selected"':''}}>
                        Hủy duyệt
                      </option>
                    </select>
                  </div>
                  <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile">
                    <label>
                      Trạng thái thông tin:
                    </label>
                    <select class="form-control m-input" data-col-index="7" id="info_status"
                            name="info_status">
                      <option value="" selected="selected">--Tất cả--</option>
                      <option
                        value="{{\App\Models\Business\Business_Product::INFO_STATUS_PENDING}}" {{!is_null(Request::input('info_status')) && Request::input('info_status')==\App\Models\Business\Business_Product::INFO_STATUS_PENDING?'selected="selected"':''}}>
                        Chờ duyệt
                      </option>
                      <option
                        value="{{\App\Models\Business\Business_Product::INFO_STATUS_APPROVED}}" {{Request::input('info_status')==\App\Models\Business\Business_Product::INFO_STATUS_APPROVED?'selected="selected"':''}}>
                        Đã duyệt
                      </option>
                      <option
                        value="{{\App\Models\Business\Business_Product::INFO_STATUS_DISAPPROVED}}" {{Request::input('info_status')==\App\Models\Business\Business_Product::INFO_STATUS_DISAPPROVED?'selected="selected"':''}}>
                        Hủy duyệt
                      </option>
                    </select>
                  </div>
                </div>
                <div class="row m--margin-bottom-20">
                  <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile">
                    <label>
                      Vai trò quản lý:
                    </label>
                    <select class="form-control m-input" data-col-index="7"
                            id="management_type"
                            name="management_type">
                      <option value="" selected="selected">--Tất cả--</option>
                      <option
                        value="{{\App\Models\Business\Business_Product::MANAGEMENT_TYPE_FULL_CONTROL}}" {{!is_null(Request::input('management_type')) && Request::input('management_type')==\App\Models\Business\Business_Product::MANAGEMENT_TYPE_FULL_CONTROL?'selected="selected"':''}}>
                        Toàn quyền
                      </option>
                      <option
                        value="{{\App\Models\Business\Business_Product::MANAGEMENT_TYPE_DISTRIBUTOR}}" {{Request::input('management_type')==\App\Models\Business\Business_Product::MANAGEMENT_TYPE_DISTRIBUTOR?'selected="selected"':''}}>
                        Nhà phân phối
                      </option>
                    </select>
                  </div>
                  <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile">
                    <label>
                      Tình trạng giấy tờ:
                    </label>
                    <select class="form-control m-input" data-col-index="7" id="certificate_status"
                            name="certificate_status">
                      <option value="" selected="selected">--Tất cả--</option>
                      <option value="1" {{Request::input('certificate_status')==1?'selected="selected"':''}}>
                        Có giấy tờ
                      </option>
                      <option value="2" {{Request::input('certificate_status')==2?'selected="selected"':''}}>
                        Chưa có giấy tờ
                      </option>
                    </select>
                  </div>
                  {{--<div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">--}}
                  {{--<label>--}}
                  {{--Tình trạng Chứng chỉ Sản phẩm:--}}
                  {{--</label>--}}
                  {{--<select class="form-control m-input" data-col-index="7"--}}
                  {{--id="certificateImages_status"--}}
                  {{--name="has_certificate_images">--}}
                  {{--<option value="" selected="selected">--Tất cả--</option>--}}
                  {{--<option value="0" {{Request::input('has_certificate_images')=="0"?'selected="selected"':''}}>--}}
                  {{--Chưa có--}}
                  {{--</option>--}}
                  {{--<option value="1" {{Request::input('has_certificate_images')==1?'selected="selected"':''}}>--}}
                  {{--Có--}}
                  {{--</option>--}}
                  {{--</select>--}}
                  {{--</div>--}}
                  <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                    <label>
                      Sản phẩm cần kiểm tra lại thông tin:
                    </label>
                    <select class="form-control m-input" data-col-index="7"
                            id="need_recheck"
                            name="need_recheck">
                      <option value="" selected="selected">--Tất cả--</option>
                      <option value="0" {{Request::input('need_recheck')=="0"?'selected="selected"':''}}>
                        Không
                      </option>
                      <option value="1" {{Request::input('need_recheck')==1?'selected="selected"':''}}>
                        Có
                      </option>
                    </select>
                  </div>
                  <div class="col-lg-4 m--margin-bottom-10-tablet-and-mobile">
                    <label style="color: white">Hành động </label>
                    <div>
                      <button class="btn btn-brand m-btn m-btn--icon" type="submit">
												<span>
													<i class="la la-search"></i>
													<span>
														Tìm kiếm
													</span>
												</span>
                      </button>
                      &nbsp;&nbsp;
                      <button class="btn btn-secondary m-btn m-btn--icon" type="button"
                              onclick="window.location='{{route('Business::business_product@index')}}'">
												<span>
													<i class="la la-refresh"></i>
													<span>
														Làm mới
													</span>
												</span>
                      </button>
                    </div>
                  </div>
                </div>
              </form>
              @if (session('success'))
                <div
                  class="m-alert m-alert--outline m-alert--square m-alert--outline-2x alert alert-success alert-dismissible fade show"
                  role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  </button>
                  {{ session('success') }}
                </div>
                {{--@php--}}
                {{--session()->forget('success');--}}
                {{--@endphp--}}
              @endif
              @if (session('AjaxRequestSuccess'))
                <div
                  class="m-alert m-alert--outline m-alert--square m-alert--outline-2x alert alert-success alert-dismissible fade show"
                  role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  </button>
                  {{ session('AjaxRequestSuccess') }}
                </div>
                @php
                  session()->forget('AjaxRequestSuccess');
                @endphp
              @endif
              @if (session('warning'))
                <div
                  class="m-alert m-alert--outline m-alert--square m-alert--outline-2x alert alert-warning alert-dismissible fade show"
                  role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  </button>
                  {{ session('warning') }}
                </div>
              @endif
              @can("GATEWAY-disapproveProductManageRole")
                <div style="text-align: right">
                  <button type="button" class="btn btn-info btn-approveMultiProductsInfo">
                    Duyệt thông tin
                  </button>
                  <button type="button" class="btn btn-success btn-approveMultiProductsManagementRole">
                    Duyệt quyền quản lý
                  </button>
                  <button type="button" class="btn btn-danger" onclick="showModal('#disapproveProductsInfoModal')">
                    Hủy duyệt thông tin
                  </button>
                  <button type="button" class="btn btn-danger"
                          onclick="showModal('#disapproveManagementRoleOfProductsModal')">
                    Bỏ quyền quản lý
                  </button>
                  <button type="button" class="btn btn-danger" data-toggle="modal"
                          data-target="#disapproveAllProducts" style="margin: 2px">
                    Bỏ quyền quản lý theo Doanh nghiệp
                  </button>
                </div>
              @endcan
              <div style="margin-bottom: 0;margin-top: 50px;margin-left: 50px;">
                  <span
                    style="width:200px;text-align:center;padding:6px;font-weight: bold;font-style: italic;border: solid 1px;border-radius: 6px">
                    Đang hiển thị {{$count_record_in_page}}/{{$count_record_total}} bản ghi
                  </span>
                <span style="margin-left: 5px">
                    <a href="{{route('Business::business_product@excelExport',$query)}}"
                       class="btn btn-info m-btn btn-sm 	m-btn m-btn--icon m-btn--pill">
                      <span>
													<i class="la la-cloud-download"></i>
													<span>
														Xuất Excel
													</span>
                      </span>
                    </a>
                  </span>
              </div>
              <div class="table-responsive">
                <table class="table table-striped- table-bordered table-hover table-checkable"
                       style="margin-top: 20px;margin-left:auto;margin-right:auto;width: 1800px">
                  <!--begin::Thead-->
                  <thead>
                  <tr>
                    <th><label class="m-checkbox">
                        <input type="checkbox" name="check_all">
                        <span></span>
                      </label></th>
                    <th style="width: 70px;font-weight: bold">Id</th>
                    <th style="min-width: 300px;font-weight: bold">Tên sản phẩm</th>
                    <th style="width: 100px;font-weight: bold">Mã barcode</th>
                    <th style="font-weight: bold;min-width: 300px">Doanh nghiệp quản lý</th>
                    <th style="font-weight: bold;min-width: 300px">Doanh nghiệp sở hữu</th>
                    {{--<th style="font-weight: bold;width: 100px">Chứng chỉ Sản phẩm</th>--}}
                    <th style="font-weight: bold;min-width: 100px">Thông tin sản phẩm</th>
                    <th style="font-weight: bold;min-width: 100px">Trạng thái quản lý</th>
                    <th style="font-weight: bold;min-width: 100px">Duyệt/bỏ duyệt quyền quản lý sản phẩm
                    </th>
                    <th style="font-weight: bold;min-width: 140px">Quyền hạn quản lý</th>
                    {{--<th style="font-weight: bold;min-width: 140px">Thay đổi quyền quản lý</th>--}}
                    <th style="font-weight: bold;min-width: 140px">Trạng thái thông tin</th>
                    <th style="font-weight: bold;min-width: 100px">Duyệt/bỏ duyệt thông tin sản phẩm</th>
                    <th style="font-weight: bold;width: 100px">Ngày tạo</th>
                    <th style="font-weight: bold;width: 100px">Ngày cập nhật gần nhất</th>
                  </tr>
                  </thead>
                  <!--end::Thead-->
                  <!--begin::Tbody-->
                  <tbody>
                  @foreach($products as $product)
                    <tr>
                      <td>
                        @if($product->is_expired!= \App\Models\Business\Business_Product::EXPIRE_STATUS_TRUE)
                          <label class="m-checkbox">
                            <input type="checkbox" name="product_ids[]"
                                   value="{{$product->id}}">
                            <span></span>
                          </label>
                        @endif
                      </td>
                      <td class="id">{{$product->id}}</td>
                      <td>
                        @if($product->product_info && array_key_exists('name', $product->product_info))
                          {{$product->product_info['name']}}
                        @endif
                      </td>
                      <td><span @if($product->validBarcode == 0) title="Mã sai chuẩn định dạng"
                                style="color: red" @endif>{{$product->barcode}}</span></td>
                      <td>
                        <a href="{{route('Business::businesses@index')}}?business_id={{$product->business_id}}"
                           target="_blank">
                          {{$product->business_name}}
                        </a>
                      </td>
                      <td>{{$product->vendor_name}}</td>
                      <td>
                        @can("GATEWAY-viewDetailProduct")
                          <button
                            type="button"
                            @if(auth()->user()->cannot('GATEWAY-viewDetailProduct'))
                            disabled
                            @endif
                            value="{{$product->certificates}}"
                            data-target="#file{{$product->id}}"
                            class="files_modal btn m-btn--pill m-btn--air btn-outline-info btn-sm"
                            id="upload_files{{$product->id}}"
                            data-toggle="modal">Chi tiết
                          </button>
                      @endcan
                      <!--Modal-->
                        <div id="file{{$product->id}}" class="modal fade"
                             role="dialog">
                          <div class="modal-dialog modal-lg" role="document">
                            <!-- Modal content-->
                            <div class="modal-content">
                              <div class="modal-header">
                                <h1 class="modal-title">
                                  Thông tin của sản phẩm
                                  @if($product->product_info && array_key_exists('name', $product->product_info))
                                    "{{$product->product_info['name']}}"
                                  @endif
                                </h1>
                                <button type="button" class="close"
                                        data-dismiss="modal">&times;
                                </button>
                              </div>
                              <form method="POST"
                                    action="{{ route('Business::business_product@updateCertificates')}}">
                                <div class="modal-body"
                                     style="height:400px;overflow-y: scroll;">
                                  <input type="hidden" name="product_id"
                                         value="{{$product->id}}">
                                  @if(!$product->certificates)
                                    {{"Sản phẩm chưa được cung cấp giấy tờ!"}}
                                  @endif
                                  <div>
                                    <ol class="heroes upload_files{{$product->id}}_input_list">
                                    </ol>
                                  </div>
                                  @if(auth()->user()->can('GATEWAY-updateSubscriptionContract'))
                                    <div style="text-align: center">
                                      <label for="upload_files{{$product->id}}_input"
                                             style="cursor: pointer;padding:8px;background-color: #0aa7ef;color: white;border-radius: 20px"><i
                                          class="la la-cloud-upload"></i>
                                        Chọn
                                        tập tin</label>
                                      <input type="file" name="files[]"
                                             id="upload_files{{$product->id}}_input"
                                             class="upload"
                                             style="display: none!important;"
                                             accept=".png,.jpg,.jpeg,.gif,.pdf,.doc,.docx"
                                             multiple>
                                    </div>
                                  @endif
                                  @if($product->management_status == \App\Models\Business\Business_Product::MANAGEMENT_STATUS_APPROVED && $product->stdProduct && ($product->info_status == \App\Models\Business\Business_Product::INFO_STATUS_PENDING || $product->need_recheck == 1))
                                    <div style="border-top: dashed 2px; margin-top: 8px;padding-top: 5px">
                                      @component('business.business_products.components.product_info_comparison',  ['product' => $product, 'icheck_attributes' => $icheck_attributes, 'stdProduct' => $product->stdProduct])
                                      @endcomponent
                                    </div>
                                  @else
                                    <div style="border-top: dashed 2px; margin-top: 8px;padding-top: 5px">
                                      <p>
                                        <span
                                          class="attribute_title">TÊN SẢN PHẨM:</span>
                                        @if($product->product_info && array_key_exists('name', $product->product_info) && $product->product_info['name'])
                                          {{$product->product_info['name']}}
                                        @endif
                                      </p>
                                      <p>
                                        <span class="attribute_title">MÃ BARCODE:</span> {{$product->barcode}}
                                      </p>
                                      <p>
                                      @if($product->product_info && array_key_exists('images', $product->product_info) && $product->product_info['images'])
                                        <div class="attribute_title">ẢNH SẢN
                                          PHẨM:
                                        </div>
                                        <p>
                                          @foreach($product->product_info['images'] as $image)
                                            <span style="margin: 5px;padding:5px"><img
                                                src="{{$image}}"
                                                width="200px"></span>
                                          @endforeach
                                        </p>
                                      @endif
                                      @if($product->product_info && array_key_exists('certificate_images', $product->product_info) && $product->product_info['certificate_images'])
                                        <div class="attribute_title">ẢNH CHỨNG
                                          CHỈ
                                          SẢN PHẨM:
                                        </div>
                                        <p>
                                          @foreach($product->product_info['certificate_images'] as $image)
                                            <span style="margin: 5px;padding:5px"><img
                                                src="{{$image}}"
                                                width="200px"></span>
                                          @endforeach
                                        </p>
                                      @endif
                                      <p>
                                      <span
                                        class="attribute_title">GIÁ SẢN PHẨM:</span>
                                        @if($product->product_info && array_key_exists('price', $product->product_info) && $product->product_info['price'])
                                          {{number_format($product->product_info['price'],0,".",",")}} đ
                                        @endif
                                      </p>
                                      @if($product->product_info && array_key_exists('attributes', $product->product_info) && $product->product_info['attributes'])
                                        @foreach($product->product_info['attributes'] as $key => $value)
                                          @if($value != '')
                                            <p><span class="attribute_title">{{$key}}
                                                : </span> @php echo preg_replace('/(<br\s*\/?>\s*)+/', '<br>', nl2br($value)) @endphp
                                            </p>
                                          @endif
                                        @endforeach
                                      @endif
                                    </div>
                                  @endif
                                </div>
                                <div class="modal-footer">
                                  @can("GATEWAY-updateProductCertificate")
                                    <button type="submit"
                                            class="btn btn-success">
                                      Cập nhật giấy tờ
                                    </button>
                                  @endcan
                                  @can("GATEWAY-approveProductInfo")
                                    @if($product->management_type == \App\Models\Business\Business_Product::MANAGEMENT_TYPE_FULL_CONTROL && $product->management_status == \App\Models\Business\Business_Product::MANAGEMENT_STATUS_APPROVED && $product->is_expired!=\App\Models\Business\Business_Product::EXPIRE_STATUS_TRUE)
                                      <button type="button" title="Duyệt thông tin sản phẩm"
                                              class="approveInfo btn btn-info">
                                        Duyệt thông tin
                                      </button>
                                    @endif
                                    @if($product->management_status != \App\Models\Business\Business_Product::MANAGEMENT_STATUS_APPROVED && $product->is_expired!=\App\Models\Business\Business_Product::EXPIRE_STATUS_TRUE)
                                      <button type="button" title="Duyệt quyền quản lý sản phẩm"
                                              class="approveManagementRole btn btn-success">
                                        Duyệt quản lý
                                      </button>
                                    @endif
                                    @if($product->management_status == \App\Models\Business\Business_Product::MANAGEMENT_STATUS_APPROVED && ($product->management_type == \App\Models\Business\Business_Product::MANAGEMENT_TYPE_FULL_CONTROL && $product->need_recheck || $product->info_status == \App\Models\Business\Business_Product::INFO_STATUS_PENDING))
                                      <button
                                        title="Bỏ duyệt thông tin"
                                        type="button"
                                        data-toggle="modal"
                                        data-target="#infoDisapprove{{$product->id}}"
                                        class="btn btn-danger">
                                        Bỏ duyệt thông tin
                                      </button>
                                    @endif
                                    @if($product->management_status != \App\Models\Business\Business_Product::MANAGEMENT_STATUS_DISAPPROVED && $product->is_expired!=\App\Models\Business\Business_Product::EXPIRE_STATUS_TRUE)
                                      <button
                                        title="Bỏ duyệt quyền quản lý sản phẩm"
                                        type="button"
                                        data-toggle="modal"
                                        data-target="#managementDisapprove{{$product->id}}"
                                        class="btn btn-danger">
                                        Bỏ duyệt quản lý
                                      </button>
                                    @endif
                                  @endcan
                                  <button type="button"
                                          class="close-modal btn btn-default"
                                          data-dismiss="modal">Hủy bỏ
                                  </button>
                                </div>
                              </form>
                            </div>
                          </div>
                        </div>
                        <!--End Modal-->
                      </td>
                      <td>
                        @if($product->management_status==\App\Models\Business\Business_Product::MANAGEMENT_STATUS_PENDING)
                          <span
                            class="status m-badge m-badge--warning m-badge--wide">{{\App\Models\Business\Business_Product::$managementStatusTexts[\App\Models\Business\Business_Product::MANAGEMENT_STATUS_PENDING]}} </span>
                        @elseif($product->management_status==\App\Models\Business\Business_Product::MANAGEMENT_STATUS_APPROVED)
                          <span
                            class="status m-badge m-badge--success m-badge--wide">{{\App\Models\Business\Business_Product::$managementStatusTexts[\App\Models\Business\Business_Product::MANAGEMENT_STATUS_APPROVED]}} </span>
                        @elseif($product->management_status==\App\Models\Business\Business_Product::MANAGEMENT_STATUS_DISAPPROVED)
                          <span
                            class="status m-badge m-badge--danger m-badge--wide">{{\App\Models\Business\Business_Product::$managementStatusTexts[\App\Models\Business\Business_Product::MANAGEMENT_STATUS_DISAPPROVED]}}</span>
                        @elseif($product->management_status==\App\Models\Business\Business_Product::MANAGEMENT_STATUS_NEED_UPDATE_PROOF)
                          <span
                            class="status m-badge m-badge--metal m-badge--wide">{{\App\Models\Business\Business_Product::$managementStatusTexts[\App\Models\Business\Business_Product::MANAGEMENT_STATUS_NEED_UPDATE_PROOF]}}</span>
                        @endif
                      </td>
                      <td>
                        @can("GATEWAY-approveProductRequest")
                          @if($product->is_expired!=\App\Models\Business\Business_Product::EXPIRE_STATUS_TRUE && $product->management_status != \App\Models\Business\Business_Product::MANAGEMENT_STATUS_APPROVED)
                            <button
                              title="Duyệt quyền quản lý"
                              type="button"
                              @if(auth()->user()->cannot('GATEWAY-approveProductRequest'))
                              disabled
                              @endif
                              class="approveManagementRole btn btn-success m-btn m-btn--icon m-btn--icon-only m-btn--pill">
                              <i class="la la-check"></i>
                            </button>
                          @endif
                        @endcan
                        @can("GATEWAY-disapproveProductManageRole")
                          @if($product->is_expired!=\App\Models\Business\Business_Product::EXPIRE_STATUS_TRUE && $product->management_status != \App\Models\Business\Business_Product::MANAGEMENT_STATUS_DISAPPROVED)
                            <button type="button"
                                    title="Từ chối quyền quản lý"
                                    @if(auth()->user()->cannot('GATEWAY-disapproveProductManageRole'))
                                    disabled
                                    @endif
                                    data-toggle="modal"
                                    data-target="#managementDisapprove{{$product->id}}"
                                    class="btn btn-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill">
                              <i class="la la-close"></i>
                            </button>
                        @endif
                      @endcan
                      <!--Modal Comfirm disapprove-->
                        <div id="managementDisapprove{{$product->id}}" class="modal fade"
                             role="dialog">
                          <div class="modal-dialog">
                            <!-- Modal content-->
                            <div class="modal-content">
                              <div class="modal-header">
                                <h2>Bạn chắc
                                  chắn muốn hủy bỏ quyền quản lý sản phẩm
                                  @if($product->product_info && array_key_exists('name', $product->product_info))
                                    "{{$product->product_info['name']}}"
                                  @endif
                                  đối với doanh nghiệp
                                  "{{$product->business_name}}"?</h2>
                                <button type="button" class="close"
                                        data-dismiss="modal">&times;
                                </button>
                              </div>
                              <form method="post" onsubmit="return validate('note-{{$product->id}}')"
                                    action="{{route('Business::business_product@disapproveManagementRoleOfProducts')}}">
                                {{ csrf_field() }}
                                <div class="modal-body">
                                  <div class="form-group m-form__group row">
                <textarea class="form-control" rows="5"
                          placeholder="Nhập lý do hủy bỏ quyền quản lý sản phẩm. Hệ thống sẽ tự động gửi email thông tin cho doanh nghiệp về lý do được đưa ra!"
                          name="reason" id="note-{{$product->id}}"
                          required></textarea>
                                    <input type="hidden" name="product_ids[]"
                                           value="{{$product->id}}">
                                  </div>
                                  <h3>Chú ý:</h3> Nếu hủy bỏ quyền quản lý Sản
                                  phẩm
                                  của Doanh nghiệp thì Doanh nghiệp sẽ không
                                  thể
                                  thao
                                  tác hay
                                  cập nhật bất kỳ thông tin gì liên quan đến
                                  Sản
                                  phẩm
                                  nữa. Trường hợp Doanh nghiệp muốn quản lý
                                  sản
                                  phẩm,
                                  phải gửi lại yêu cầu quản lý Sản phẩm.
                                </div>

                                <div class="modal-footer">
                                  <button type="submit"
                                          class="btn btn-success">
                                    Đồng ý
                                  </button>
                                  <button type="button"
                                          class="btn btn-default"
                                          data-dismiss="modal">Đóng
                                  </button>
                                </div>
                              </form>
                            </div>
                          </div>
                        </div>
                        <!--End Modal-->
                      </td>
                      <td>
                        @if($product->management_status==\App\Models\Business\Business_Product::MANAGEMENT_STATUS_APPROVED)
                          @if($product->management_type == \App\Models\Business\Business_Product::MANAGEMENT_TYPE_FULL_CONTROL)
                            {{\App\Models\Business\Business_Product::$managementTypeTexts[$product->management_type]}}
                            @else
                            Phân phối
                          @endif
                        @endif
                      </td>
                      {{--<td>--}}
                      {{--@if($product->management_status==\App\Models\Business\Business_Product::MANAGEMENT_STATUS_APPROVED)--}}
                      {{--@if($product->management_type == \App\Models\Business\Business_Product::MANAGEMENT_TYPE_DISTRIBUTOR)--}}
                      {{--<button type="button" title="Click để chuyển sang vai trò toàn quyền"--}}
                      {{--onclick="window.location='{{ route('Business::business_product@setFullControlRole',$product->id)}}'"--}}
                      {{--class="btn m-btn--pill m-btn--air btn-secondary btn-sm">Toàn quyền--}}
                      {{--</button>--}}
                      {{--@endif--}}
                      {{--@if($product->management_type == \App\Models\Business\Business_Product::MANAGEMENT_TYPE_FULL_CONTROL)--}}
                      {{--<button type="button" title="Click để chuyển sang vai trò Nhà phân phối"--}}
                      {{--onclick="window.location='{{ route('Business::business_product@setDistributorRole',$product->id)}}'"--}}
                      {{--class="btn m-btn--pill m-btn--air btn-secondary btn-sm">Nhà phân phối--}}
                      {{--</button>--}}
                      {{--@endif--}}
                      {{--@endif--}}
                      {{--</td>--}}
                      <td>
                        @if($product->management_type == \App\Models\Business\Business_Product::MANAGEMENT_TYPE_FULL_CONTROL && $product->management_status == \App\Models\Business\Business_Product::MANAGEMENT_STATUS_APPROVED)
                          @if($product->info_status==\App\Models\Business\Business_Product::INFO_STATUS_PENDING)
                            <span
                              class="status m-badge m-badge--warning m-badge--wide">{{\App\Models\Business\Business_Product::$infoStatusTexts[\App\Models\Business\Business_Product::INFO_STATUS_PENDING]}} </span>
                          @elseif($product->info_status==\App\Models\Business\Business_Product::INFO_STATUS_APPROVED)
                            <span
                              class="status m-badge m-badge--success m-badge--wide">{{\App\Models\Business\Business_Product::$infoStatusTexts[\App\Models\Business\Business_Product::INFO_STATUS_APPROVED]}} </span>
                          @elseif($product->info_status==\App\Models\Business\Business_Product::INFO_STATUS_DISAPPROVED)
                            <span
                              class="status m-badge m-badge--danger m-badge--wide">{{\App\Models\Business\Business_Product::$infoStatusTexts[\App\Models\Business\Business_Product::INFO_STATUS_DISAPPROVED]}}</span>
                          @endif
                        @endif
                      </td>
                      <td>
                        @if($product->management_type == \App\Models\Business\Business_Product::MANAGEMENT_TYPE_FULL_CONTROL && $product->management_status == \App\Models\Business\Business_Product::MANAGEMENT_STATUS_APPROVED)
                          @if($product->is_expired!=\App\Models\Business\Business_Product::EXPIRE_STATUS_TRUE)
                            @can("GATEWAY-approveProductInfo")
                              <button
                                title="Duyệt thông tin sản phẩm"
                                type="button"
                                @if(auth()->user()->cannot('GATEWAY-approveProductInfo'))
                                disabled
                                @endif
                                class="approveInfo btn btn-info m-btn m-btn--icon m-btn--icon-only m-btn--pill">
                                <i class="la la-check"></i>
                              </button>
                              @if($product->need_recheck || $product->info_status == \App\Models\Business\Business_Product::INFO_STATUS_PENDING)
                                <button
                                  title="Bỏ duyệt thông tin sản phẩm"
                                  type="button"
                                  @if(auth()->user()->cannot('GATEWAY-approveProductInfo'))
                                  disabled
                                  @endif
                                  data-toggle="modal"
                                  data-target="#infoDisapprove{{$product->id}}"
                                  class="btn btn-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill">
                                  <i class="la la-remove"></i>
                                </button>
                              @endif
                            <!--Modal Comfirm disapprove-->
                              <div id="infoDisapprove{{$product->id}}" class="modal fade"
                                   role="dialog">
                                <div class="modal-dialog">
                                  <!-- Modal content-->
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h2>Bạn chắc
                                        chắn muốn bỏ duyệt thông tin sản phẩm
                                        "{{$product->product_info['name']}}" của doanh nghiệp
                                        "{{$product->business_name}}"?</h2>
                                      <button type="button" class="close"
                                              data-dismiss="modal">&times;
                                      </button>
                                    </div>
                                    <form method="POST"
                                          action="{{route('Business::business_product@disapproveProductInfo')}}">
                                      {{ csrf_field() }}
                                      <div class="modal-body">
                                        <div class="form-group m-form__group row">
              <textarea class="form-control" rows="5"
                        placeholder="Nhập lý do hủy bỏ thông tin sản phẩm. Hệ thống sẽ tự động gửi email thông tin cho doanh nghiệp về lý do được đưa ra!"
                        name="reason"
                        required></textarea>
                                        </div>
                                        <input type="hidden" name="product_ids[]"
                                               value="{{$product->id}}">
                                      </div>
                                      <div class="modal-footer">
                                        <button type="submit"
                                                class="btn btn-success">
                                          Đồng ý
                                        </button>
                                        <button type="button"
                                                class="btn btn-default"
                                                data-dismiss="modal">Đóng
                                        </button>
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>
                              <!--End Modal-->
                            @endcan
                          @endif
                        @endif
                      </td>
                      <td>
                        {{date("d/m/Y H:i:s",strtotime($product->created_at))}}
                      </td>
                      <td>
                        {{date("d/m/Y H:i:s",strtotime($product->updated_at))}}
                      </td>
                    </tr>
                  @endforeach
                  </tbody>
                  <!--end::Tbody-->
                </table>
                <!-- Modal disapproveAllProducts-->
                <div id="disapproveAllProducts" class="modal fade"
                     role="dialog">
                  <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                      <div class="modal-header">
                        <h2>Bạn chắc
                          chắn muốn hủy bỏ quyền quản lý tất cả các sản phẩm của Doanh nghiệp?</h2>
                        <button type="button" class="close"
                                data-dismiss="modal">&times;
                        </button>
                      </div>
                      <form method="POST" enctype="multipart/form-data" onsubmit="return validate('all_note')"
                            action="{{ route('Business::business_product@disapproveManagementRoleOfAllProductsOfBusiness') }}">
                        {{ csrf_field() }}
                        <div class="modal-body">
                          <div class="form-group m-form__group row">
                            <select class="form-control" data-col-index="7" id="disapprove_business_id"
                                    style="width: 477px"
                                    name="disapprove_business_id" required>
                              <option value="" selected="selected"></option>
                              @foreach($businesses as $business)
                                <option value="{{$business->id}}">{{$business->name}}</option>
                              @endforeach
                            </select>
                          </div>
                          <div class="form-group m-form__group row">
    <textarea class="form-control" rows="5"
              placeholder="Nhập lý do hủy bỏ quyền quản lý sản phẩm. Hệ thống sẽ tự động gửi email thông tin cho doanh nghiệp về lý do được đưa ra!"
              name="reason"
              required></textarea>
                          </div>
                          <h3>Chú ý:</h3> Nếu hủy bỏ quyền quản lý Sản
                          phẩm
                          của Doanh nghiệp thì Doanh nghiệp sẽ không
                          thể
                          thao
                          tác hay
                          cập nhật bất kỳ thông tin gì liên quan đếns
                          Sản
                          phẩm
                          nữa. Trường hợp Doanh nghiệp muốn quản lý
                          sản
                          phẩm,
                          phải gửi lại yêu cầu quản lý Sản phẩm.
                        </div>

                        <div class="modal-footer">
                          <button type="submit"
                                  class="btn btn-success">
                            Đồng ý
                          </button>
                          <button type="button"
                                  class="btn btn-default"
                                  data-dismiss="modal">Hủy bỏ
                          </button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
                <!-- Modal multiDisapproveProducts-->
                <div id="disapproveManagementRoleOfProductsModal" class="modal fade"
                     role="dialog">
                  <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                      <div class="modal-header">
                        <h2>Bạn chắc
                          chắn muốn hủy bỏ quyền quản lý các sản phẩm này?</h2>
                        <button type="button" class="close"
                                data-dismiss="modal">&times;
                        </button>
                      </div>
                      <form method="POST" enctype="multipart/form-data"
                            action="{{ route('Business::business_product@disapproveManagementRoleOfProducts') }}">
                        <div class="modal-body">
                          <div class="form-group m-form__group row" id="disapproveManagementRoleOfProductsForm">
    <textarea class="form-control" rows="5"
              placeholder="Nhập lý do hủy bỏ quyền quản lý sản phẩm. Hệ thống sẽ tự động gửi email thông tin cho doanh nghiệp về lý do được đưa ra!"
              name="reason"
              required></textarea>
                          </div>
                          <h3>Chú ý:</h3> Nếu hủy bỏ quyền quản lý Sản
                          phẩm
                          của Doanh nghiệp thì Doanh nghiệp sẽ không
                          thể
                          thao
                          tác hay
                          cập nhật bất kỳ thông tin gì liên quan đến
                          Sản
                          phẩm
                          nữa. Trường hợp Doanh nghiệp muốn quản lý
                          sản
                          phẩm,
                          phải gửi lại yêu cầu quản lý Sản phẩm.
                        </div>
                        <div class="modal-footer">
                          <button type="submit"
                                  class="btn btn-success">
                            Đồng ý
                          </button>
                          <button type="button"
                                  class="btn btn-default"
                                  data-dismiss="modal">Hủy bỏ
                          </button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
              <!--Modal for showing image-->
              <div class="modal fade" id="enlargeImageModal" tabindex="-1" role="dialog"
                   aria-labelledby="enlargeImageModal" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                      <img src="" class="enlargeImageModalSource" style="width: 100%;">
                    </div>
                  </div>
                </div>
              </div>
              <!-- Modal disapproveMultiProductsInfo-->
              <div id="disapproveProductsInfoModal" class="modal fade"
                   role="dialog">
                <div class="modal-dialog">
                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <h2>Bạn chắc
                        chắn muốn hủy bỏ thông tin hiện tại của các sản phẩm này ?</h2>
                      <button type="button" class="close"
                              data-dismiss="modal">&times;
                      </button>
                    </div>
                    <form method="POST" enctype="multipart/form-data"
                          action="{{ route('Business::business_product@disapproveProductInfo') }}">
                      <div class="modal-body">
                        <div class="form-group m-form__group row" id="disapproveProductsInfoForm">
    <textarea class="form-control" rows="5"
              placeholder="Nhập lý do hủy thông tin sản phẩm. Hệ thống sẽ tự động gửi email thông tin cho doanh nghiệp về lý do được đưa ra!"
              name="reason" id="reason"></textarea>
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button type="submit" class="btn btn-success">
                          Đồng ý
                        </button>
                        <button type="button"
                                class="btn btn-default"
                                data-dismiss="modal">Hủy bỏ
                        </button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
              <!--end::Table-->
              <div style="float:right;"><?php echo $products->links(); ?></div>
            </div>
            <!--End::Section-->
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
@push('scripts')
  <script type="text/javascript" src="{{ asset('qr_code/metronic/js/vendors.bundle.js') }}"></script>
  <script type="text/javascript">
    $(document).ready(function () {
      $('input[name="check_all"]').click(function () {
        let status = $(this).is(':checked');
        $('input[type="checkbox"]').prop('checked', status);
      });
//Select2
      $("#management_status").select2({
        placeholder: 'Trạng thái quản lý',
        allowClear: true
      });
      $("#management_type").select2({
        placeholder: 'Vai trò quản lý',
        allowClear: true
      });

      $("#info_status").select2({
        placeholder: 'Trạng thái thông tin',
        allowClear: true
      });
      $("#business_id").select2({
        placeholder: 'Chọn doanh nghiệp',
        allowClear: true
      });
      $("#disapprove_business_id").select2({
        placeholder: 'Chọn doanh nghiệp',
        allowClear: true
      });
      $("#certificate_status").select2({
        placeholder: 'Chọn trạng thái',
        allowClear: true
      });
      $("#need_recheck").select2({
        placeholder: 'Chọn',
        allowClear: true
      });
      $("#certificateImages_status").select2({
        placeholder: 'Ảnh chứng chỉ Sản phẩm',
        allowClear: true
      });
      $("select").on("select2:select", function (evt) {
        var element = evt.params.data.element;
        var $element = $(element);
        $element.detach();
        $(this).append($element);
        $(this).trigger("change");
      });
      $(".modal").on('hidden.bs.modal', function () {
        document.body.style.padding = 0;
      });
    });

    //View image
    $(function () {
      $('img').on('click', function () {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
      });
    });

    //
    function showModal(modal) {
      var checkboxes = document.querySelectorAll('input[type=checkbox]:checked');
      if (parseInt(checkboxes.length) === 0) {
        alert("Bạn chưa chọn Sản phẩm nào!");
        return false;
      }
      // $('#multiDisapproveProducts').modal('show');
      $(modal).modal('show');
    }

    // Validate form
    function validate(note) {
      if (document.getElementById(note).value.trim() === '') {
        alert('Ban chưa nhập lý do!');
        return false;
      }
      return true;
    }

    $("#disapproveMultiProductsInfoModal").on('shown.bs.modal', function () {
      var checkboxes = document.querySelectorAll('input[type=checkbox]:checked');
      if (parseInt(checkboxes.length) === 0 || parseInt(checkboxes.length) === 1 && $('input[name="check_all"]').is(':checked')) {
        alert("Bạn chưa chọn Sản phẩm nào!");
        return false;
      }
      var data = [];
      for (i = 0; i < checkboxes.length; i++) {
        let id = parseInt(checkboxes[i].value);
        if (isNaN(id) === false) {
          data.push(id);
        }
      }
      var JSONdata = JSON.stringify(data);
      console.log(JSONdata);
      $("#disapproveProductsInfoForm").append(`<input type="hidden" name="product_ids" value="${JSONdata}">`);
    });
    $("#disapproveManagementRoleOfProductsModal").on('shown.bs.modal', function () {
      var checkboxes = document.querySelectorAll('input[type=checkbox]:checked');
      if (parseInt(checkboxes.length) === 0 || parseInt(checkboxes.length) === 1 && $('input[name="check_all"]').is(':checked')) {
        alert("Bạn chưa chọn Sản phẩm nào!");
        return false;
      }
      var data = [];
      for (i = 0; i < checkboxes.length; i++) {
        let id = parseInt(checkboxes[i].value);
        if (isNaN(id) === false) {
          data.push(id);
        }
      }
      var JSONdata = JSON.stringify(data);
      console.log(JSONdata);
      $("#disapproveManagementRoleOfProductsForm").append(`<input type="hidden" name="product_ids" value="${JSONdata}">`);
    });

    $(".approveInfo").click(function () {
      var approve_button = $(this);    // Find the row
      let id = parseInt(approve_button.closest("tr").find(".id").text());
      var product_ids = [id];
      console.log(product_ids);
      postApproveProductInfo(product_ids);
    });
    $(".approveManagementRole").click(function () {
      var approve_button = $(this);    // Find the row
      let id = parseInt(approve_button.closest("tr").find(".id").text());
      var product_ids = [id];
      postApproveManagementRoleOfProduct(product_ids);
    });
    $(".btn-approveMultiProductsInfo").click(function () {
      var checkboxes = document.querySelectorAll('input[type=checkbox]:checked');
      if (parseInt(checkboxes.length) === 0 || parseInt(checkboxes.length) === 1 && $('input[name="check_all"]').is(':checked')) {
        alert("Bạn chưa chọn Sản phẩm nào!");
        return false;
      }
      var product_ids = [];
      for (i = 0; i < checkboxes.length; i++) {
        let id = parseInt(checkboxes[i].value);
        product_ids.push(id);
      }
      postApproveProductInfo(product_ids);
    });
    $(".btn-approveMultiProductsManagementRole").click(function () {
      var checkboxes = document.querySelectorAll('input[type=checkbox]:checked');
      if (parseInt(checkboxes.length) === 0 || parseInt(checkboxes.length) === 1 && $('input[name="check_all"]').is(':checked')) {
        alert("Bạn chưa chọn Sản phẩm nào!");
        return false;
      }
      var product_ids = [];
      for (i = 0; i < checkboxes.length; i++) {
        let id = parseInt(checkboxes[i].value);
        product_ids.push(id);
      }
      postApproveManagementRoleOfProduct(product_ids);
    });

    function postApproveProductInfo(product_ids) {
      var approveInfo_url = "{{ route('Business::business_product@approveProductInfo')}}";
      let token = $("input[name='_token']").val();
      $.ajax({
        url: approveInfo_url,
        method: 'POST',
        data: {
          product_ids: product_ids,
          _token: token
        },
        success: function (data) {
          location.reload();
        }
      })
    }

    function postApproveManagementRoleOfProduct(product_ids) {
      var url = "{{ route('Business::business_product@approveManagementRoleOfProduct')}}";
      let token = $("input[name='_token']").val();
      $.ajax({
        url: url,
        method: 'POST',
        data: {
          product_ids: product_ids,
          _token: token
        },
        success: function (data) {
          location.reload();
        }
      })
    }

    //Upload certificates
    //Fill Data to Modal
    $(".files_modal").click(function () {
      let id = $(this).attr('id');
      $(`.${id}_input_list`).empty();
      let value = $(this).attr('value');
      let file = JSON.parse(value);
      appendData(id, file);
    });

    //Upload files
    $(".upload").change(function () {
      let id = $(this).attr('id');
      let x = document.getElementById(id);
      let spinner = `<div class="spinner m-loader m-loader--primary" style="width: 30px; display: inline-block;">
                            </div>`;
      $(`.${id}_list`).append(`${spinner}`);
      var files = x.files;
      var formData = new FormData();
      // Loop through each of the selected files.
      for (let i = 0; i < files.length; i++) {
        let file = files[i];
        // Check the file type.
//                if (!file.type.match('image.*')) {
//                    continue;
//                }

        // Add the file to the request.
        formData.append('files[]', file, file.name);

      }

      var url = "{{ route('Business::business_product@uploadFiles')}}";
//            var token = $("input[name='_token']").val();
      $.ajax({
        url: url,
        method: 'POST',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {
          $("div").remove(".spinner");
          append_id = id.replace("_input", "");
          $.each(data, function (key, value) {
            let file = JSON.parse(value);
            appendData(append_id, file);
          });
        }
      });
    });
    var removeItem = function (e) {
      e.closest('li').remove();
    };

    function appendData(id, file) {
      for (let i = 0; i < file.length; i++) {
        let file_name = file[i][0].split("/").pop();
        let html = `<li>
                                <div style="text-align: left;display: inline-block;width: 70%;overflow-x: hidden">${file_name}</div>
                                <input type="hidden" name="uploaded_files[]" value="${file[i][0]}">
                                <div style="text-align: right;display: inline-block;float: right;">
                                    <a href="${file[i][1]}" target="_blank" data-toggle="tooltip" title="Xem" class="btn btn-outline-accent m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air"> <i class="la la-eye"></i></a>
                                    <a href="javascript:void(0);" onclick="return removeItem($(this));" data-toggle="tooltip" title="Xóa" class="btn btn-outline-accent m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air"> <i class="la la-remove"></i></a>
                                </div>
                           </li>`;
        $(`.${id}_input_list`).append(`${html}`);
      }
    }
  </script>
@endpush
