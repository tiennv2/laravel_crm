@extends('qrcode.layouts.app')
@section('body_class', 'm-page--fluid m-page--loading-enabled m-page--loading m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default')

@push('scripts')
  <script type="text/javascript">
    <?php
      $sharedData = [
        'config' => [
          'csrfToken' => csrf_token(),
          'logoutUrl' => route('logout'),
          'isNative' => true,
        ],
        'loggedInAccount' => auth()->check() ? auth()->user() : null,
      ];
      ?>
      window.SHARED_DATA = {!! json_encode($sharedData) !!}
  </script>
  <script src="https://cdn.jsdelivr.net/npm/tinymce@4.7.11/tinymce.min.js" type="text/javascript"></script>
  <script src="{{ mix('js/app.js') }}"></script>
@endpush

@section('content')
  <!-- begin::Page loader -->
  <div class="m-page-loader m-page-loader--base">
    <div class="m-blockui">
      <span>
        Please wait...
      </span>
      <span>
        <div class="m-loader m-loader--brand"></div>
      </span>
    </div>
  </div>
  <!-- end::page Loader -->
  <div id="app" class="m-grid m-grid--hor m-grid--root m-page">
    <app-header></app-header>
    <div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop  m-body m-page__container">
      <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader">
          <div class="d-flex align-items-center">
            <div class="mr-auto">
              <h3 class="m-subheader__title">
                CÀI ĐẶT THỜI GIAN HỆ THỐNG TỰ ĐỘNG DUYỆT THÔNG TIN SẢN PHẨM
              </h3>
            </div>
            @can("GATEWAY-addHoliday")
              <div style="float: right"><a data-toggle="modal" data-target="#createNew" style="color: white"
                                           class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air"><span>
                                    <i class="la la-plus-circle"></i>
                                    <span>Thêm mới</span></span></a>
              </div>
              <!--Modal createNew-->
              <div id="createNew" class="modal fade"
                   role="dialog">
                <div class="modal-dialog">
                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <h3>Tạo mới khoảng thời gian</h3>
                      <button type="button" class="close"
                              data-dismiss="modal">&times;
                      </button>
                    </div>
                    <form name="createForm" method="post" onsubmit="return validateCreateForm()"
                          action="{{ route('Business::holiday@store')}}">
                      {{ csrf_field() }}
                      <div class="modal-body">
                        <div class="form-group m-form__group row">
                          <label class="col-form-label col-lg-3 col-sm-12">Ngày
                            bắt đầu *</label>
                          <div class="col-lg-6 col-md-9 col-sm-12">
                            <input type="text"
                                   class="start_time form-control"
                                   name="start_time"
                                   data-date-format="dd/mm/yyyy hh:ii"
                                   autocomplete="off"
                                   data-col-index="5" required/>

                          </div>
                        </div>
                        <!------------------Ngày hết hạn hợp đồng--------------->
                        <div class="form-group m-form__group row">
                          <label class="col-form-label col-lg-3 col-sm-12">Ngày
                            kết thúc *</label>
                          <div class="col-lg-6 col-md-9 col-sm-12">
                            <input type="text"
                                   class="end_time form-control"
                                   name="end_time"
                                   data-date-format="dd/mm/yyyy hh:ii"
                                   autocomplete="off"
                                   required
                                   data-col-index="5"/>
                          </div>
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button type="submit"
                                class="btn btn-success">
                          Tạo mới
                        </button>
                        <button type="button"
                                class="btn btn-default"
                                data-dismiss="modal">Hủy
                          yêu cầu
                        </button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
              <!--End Modal-->
            @endcan
          </div>
        </div>
        <!-- END: Subheader -->
        @can("GATEWAY-viewHoliday")
          <div class="m-content">
            <!--Begin::Section-->
            <div class="m-portlet m-portlet--mobile">
              <div class="m-portlet__body">
                @if (session('success'))
                  <div
                    class="m-alert m-alert--outline m-alert--square m-alert--outline-2x alert alert-success alert-dismissible fade show"
                    role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    </button>
                    {{ session('success') }}
                  </div>
                @endif
                <div
                  style="width:200px;text-align:center;padding:5px;margin-bottom: 0;margin-top: 50px;margin-left: 20px;font-weight: bold;font-style: italic;border: solid 1px;border-radius: 6px">
                  Đang hiển thị {{$count_record_in_page}}/{{$count_record_total}} bản ghi
                </div>
                <div class="table-responsive">
                  <table class="table table-striped- table-bordered table-hover table-checkable"
                         style="margin-top: 20px;margin-left:auto;margin-right:auto">
                    <!--begin::Thead-->
                    <thead>
                    <tr>
                      <th style="font-weight: bold">Id</th>
                      <th style="width: 300px;font-weight: bold">Thời gian bắt đầu</th>
                      <th style="font-weight: bold;width: 300px">Thời gian kết thúc</th>
                      <th style="font-weight: bold">Ngày tạo</th>
                      <th style="font-weight: bold">Ngày cập nhật gần nhất</th>
                      <th style="font-weight: bold">Hành động</th>
                    </tr>
                    </thead>
                    <!--end::Thead-->
                    <!--begin::Tbody-->
                    <tbody>
                    @foreach($holidays as $holiday)
                      <tr>
                        <td>{{$holiday->id}}</td>
                        <td style="font-weight: bold">{{date("d/m/Y H:i:s",strtotime($holiday->start_time))}}</td>
                        <td style="font-weight: bold">{{date("d/m/Y H:i:s",strtotime($holiday->end_time))}}</td>
                        <td>{{date("d/m/Y H:i:s",strtotime($holiday->created_at))}}</td>
                        <td>{{date("d/m/Y H:i:s",strtotime($holiday->updated_at))}}</td>
                        <td>
                          @can("GATEWAY-updateHoliday")
                            <a data-toggle="modal" data-target="#update_{{$holiday->id}}" title="Sửa"
                               style="color:white;"
                               class="btn btn-info m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                              <i class="la la-edit"></i>
                            </a>
                          @endcan
                          @can("GATEWAY-delHoliday")
                            <a href="{{route("Business::holiday@delete",[$holiday->id])}}"
                               onclick="return delItem()" title="Xóa"
                               class="btn btn-danger m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                              <i class="la la-trash"></i>
                            </a>
                        @endcan
                        <!--Modal update-->
                          <div id="update_{{$holiday->id}}" class="modal fade"
                               role="dialog">
                            <div class="modal-dialog">
                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h3>Cập nhật</h3>
                                  <button type="button" class="close"
                                          data-dismiss="modal">&times;
                                  </button>
                                </div>
                                <form name="updateForm" method="post" onsubmit="return validateUpdateForm()"
                                      action="{{ route("Business::holiday@update",[$holiday->id])}}">
                                  {{ csrf_field() }}
                                  <input type="hidden" name="_method" value="PUT">
                                  <div class="modal-body">
                                    <div class="form-group m-form__group row">
                                      <label class="col-form-label col-lg-3 col-sm-12">Ngày
                                        bắt đầu *</label>
                                      <div class="col-lg-6 col-md-9 col-sm-12">
                                        <input type="text"
                                               class="start_time form-control"
                                               name="start_time"
                                               value="{{date("d/m/Y H:i", strtotime($holiday->start_time))}}"
                                               data-date-format="dd/mm/yyyy hh:ii"
                                               autocomplete="off"
                                               data-col-index="5" required/>

                                      </div>
                                    </div>
                                    <!------------------Ngày kết thúc--------------->
                                    <div class="form-group m-form__group row">
                                      <label class="col-form-label col-lg-3 col-sm-12">Ngày
                                        kết thúc *</label>
                                      <div class="col-lg-6 col-md-9 col-sm-12">
                                        <input type="text"
                                               class="end_time form-control"
                                               name="end_time"
                                               value="{{date("d/m/Y H:i", strtotime($holiday->end_time))}}"
                                               data-date-format="dd/mm/yyyy hh:ii"
                                               autocomplete="off"
                                               required
                                               data-col-index="5"/>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="submit"
                                            class="btn btn-success">
                                      Cập nhật
                                    </button>
                                    <button type="button"
                                            class="btn btn-default"
                                            data-dismiss="modal">Hủy
                                      yêu cầu
                                    </button>
                                  </div>
                                </form>
                              </div>
                            </div>
                          </div>
                          <!--End Modal-->
                        </td>
                      </tr>
                    @endforeach
                    </tbody>
                    <!--end::Tbody-->
                  </table>
                </div>
                <!--end::Table-->
                <div style="float:right;"><?php echo $holidays->links(); ?></div>
              </div>
              <!--End::Section-->
            </div>
          </div>
        @endcan
      </div>
    </div>
  </div>
@endsection
@push('scripts')
  <script type="text/javascript" src="{{ asset('qr_code/metronic/js/vendors.bundle.js') }}"></script>
  {{--<script type="text/javascript" src="{{ asset('qr_code/metronic/js/datepicker/bootstrap-datetimepicker.js') }}"></script>--}}
  <script type="text/javascript">

    $(document).ready(function () {
      // $("#status").select2({
      //   placeholder: 'Trạng thái',
      //   allowClear: true
      // });
      //
      // $("select").on("select2:select", function (evt) {
      //   var element = evt.params.data.element;
      //   var $element = $(element);
      //   $element.detach();
      //   $(this).append($element);
      //   $(this).trigger("change");
      // });
      $(".start_time").datetimepicker({
        dateFormat: 'dd/mm/yy hh:ii',
        changeMonth: true,
        changeYear: true,
        todayHighlight: !0,
        autoclose:!0,
        orientation: "bottom left",
        templates: {leftArrow: '<i class="la la-angle-left"></i>', rightArrow: '<i class="la la-angle-right"></i>'}
      });

      $(".end_time").datetimepicker({
        dateFormat: 'dd/mm/yy hh:ii',
        changeMonth: true,
        changeYear: true,
        todayHighlight: !0,
        orientation: "bottom left",
        templates: {leftArrow: '<i class="la la-angle-left"></i>', rightArrow: '<i class="la la-angle-right"></i>'}
      });

    });

    function validateCreateForm() {

      var start_time = document.forms["createForm"]["start_time"].value;
      var end_time = document.forms["createForm"]["end_time"].value;
      var moment_start = moment(start_time,"DD/MM/YYYY HH:II:SS");
      var moment_end = moment(end_time,"DD/MM/YYYY HH:II:SS");
      if (moment_start >= moment_end) {
        alert("Thời gian kết thúc phải lớn hơn thời gian bắt đầu! Hãy chọn lại thời gian!");
        return false;
      }
      // var start_time_parts = start_time.split("/");
      // var formatted_start_time = new Date(start_time_parts[2], start_time_parts[1] - 1, start_time_parts[0]);
      // var end_time_parts = end_time.split("/");
      // var formatted_end_time = new Date(end_time_parts[2], end_time_parts[1] - 1, end_time_parts[0]);
      // if (formatted_start_time > formatted_end_time) {
      //   alert("Thời gian bắt đầu không được lớn hơn thời gian kết thúc! Hãy chọn lại thời gian!");
      //   return false;
      // }
    }

    function validateUpdateForm() {
      var start_time = document.forms["updateForm"]["start_time"].value;
      var end_time = document.forms["updateForm"]["end_time"].value;
      var moment_start = moment(start_time,"DD/MM/YYYY HH:II:SS");
      var moment_end = moment(end_time,"DD/MM/YYYY HH:II:SS");
      if (moment_start >= moment_end) {
        alert("Thời gian kết thúc phải lớn hơn thời gian bắt đầu! Hãy chọn lại thời gian!");
        return false;
      }
    }

    function delItem() {
      let conf = confirm("Bạn chắc chắn muốn xoá không ?");
      return conf;
    }
  </script>

@endpush
