<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title>iCheck - Hệ thống quản lý sản phẩm</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
</head>
<body>
<p style="font-weight: bold"><span style="font-style: italic">Kính gửi:</span> {{$business->name}}</p>
<p>Quý khách không được duyệt quyền quản lý Sản phẩm <span style="color: blue;font-style: italic;font-weight: bold">"{{$product['name']}}"</span> có mã số <span style="color: blue;font-style: italic;font-weight: bold">"{{$product['barcode']}}"</span> vì lý do:
<p>{{$note}}</p>
<p style="font-style: italic">Cảm ơn Quý khách đã sử dụng dịch vụ của iCheck!</p>
</body>
<!-- END BODY -->
</html>
