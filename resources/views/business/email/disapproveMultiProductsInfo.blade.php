<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
  <meta charset="utf-8"/>
  <title>iCheck - Hệ thống quản lý sản phẩm</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
  <meta http-equiv="Content-type" content="text/html; charset=utf-8">
  <meta content="" name="description"/>
  <meta content="" name="author"/>
  <!-- BEGIN GLOBAL MANDATORY STYLES -->
</head>
<body>
<p style="font-weight: bold"><span style="font-style: italic">Kính gửi:</span> {{$business_name}}</p>
<p>Quý khách không được duyệt thông tin những Sản phẩm sau vì lý do:
<p>"{{$note}}"</p>
<div>
  <p style="font-weight: bold;font-style: italic">Danh sách sản phẩm</p>
  <table style="border: 1px solid black;border-collapse: collapse;">
    <thead>
    <tr>
      <th style="width: 400px;font-weight: bold;text-align: left;border: 1px solid black;border-collapse: collapse;">Tên</th>
      <th style="font-weight: bold;width: 300px;text-align: center;border: 1px solid black;border-collapse: collapse;">Mã Barcode</th>
    </tr>
    </thead>
    <tbody>
    @foreach($product_data as $key=>$value)
      <tr>
        <td style="text-align: left;border: 1px solid black;border-collapse: collapse;">{{$value}}</td>
        <td style="text-align: center;border: 1px solid black;border-collapse: collapse;">{{$key}}</td>
      </tr>
    @endforeach
    </tbody>
  </table>
</div>
<p style="font-style: italic">Cảm ơn Quý khách đã sử dụng dịch vụ của iCheck!</p>
</body>
<!-- END BODY -->
</html>
