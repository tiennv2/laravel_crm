@extends('layouts.app')

@section('body_class', 'm-page--fluid m-page--loading-enabled m-page--loading m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default')

@push('scripts')
  <script type="text/javascript">
    <?php
    $sharedData = [
      'config' => [
        'csrfToken' => csrf_token(),
        'logoutUrl' => route('logout'),
        'isNative' => false,
        'coreAPIUrl' => env('CORE_API_URL', 'https://core.icheck.com.vn'),
        'gatewayAPIUrl' => env('GATEWAY_API_URL', 'https://core.icheck.com.vn'),
        'chatAPIUrl' => env('CHAT_API_URL', 'https://chat.icheck.com.vn'),
      ],
      'loggedInAccount' => auth()->check() ? auth()->user() : null,
    ];
    ?>
    window.SHARED_DATA = {!! json_encode($sharedData) !!}
  </script>
  <script src="https://cdn.jsdelivr.net/npm/tinymce@4.7.11/tinymce.min.js" type="text/javascript"></script>
  <script src="{{ mix('js/app.js') }}" async defer></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCe546KUQ0N7-XgmfwwWDMIKXcwvEWBq_o&libraries=places" async defer></script>
@endpush

@section('content')
  <!-- begin::Page loader -->
  <div class="m-page-loader m-page-loader--base">
    <div class="m-blockui">
      <span>
        Please wait...
      </span>
      <span>
        <div class="m-loader m-loader--brand"></div>
      </span>
    </div>
  </div>
  <!-- end::Page Loader -->
  <div id="app" class="m-grid m-grid--hor m-grid--root m-page">
    <app-header></app-header>
    <router-view></router-view>
  </div>
@endsection
