@extends('qrcode.layouts.app')
@push('styles')
  <style>
    .help-block {
      color: red;
    }

    .category_select {
      margin-top: 5px;
    }
  </style>

@endpush
@section('body_class', 'm-page--fluid m-page--loading-enabled m-page--loading m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default')
@push('scripts')
  <script type="text/javascript">
    <?php
      $sharedData = [
        'config' => [
          'csrfToken' => csrf_token(),
          'logoutUrl' => route('logout'),
          'isNative' => true,
        ],
        'loggedInAccount' => auth()->check() ? auth()->user() : null,
      ];
      ?>
      window.SHARED_DATA = {!! json_encode($sharedData) !!}
  </script>
  <script src="https://cdn.jsdelivr.net/npm/tinymce@4.7.11/tinymce.min.js" type="text/javascript"></script>
  <script src="{{ mix('js/app.js') }}"></script>
@endpush

@section('content')
  <!-- begin::Page loader -->
  <div class="m-page-loader m-page-loader--base">
    <div class="m-blockui">
      <span>
        Please wait...
      </span>
      <span>
        <div class="m-loader m-loader--brand"></div>
      </span>
    </div>
  </div>
  <!-- end::Page Loader -->
  <div id="app" class="m-grid m-grid--hor m-grid--root m-page">
    <app-header></app-header>
    <div
      class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop 	m-container m-container--responsive m-container--xxl m-page__container m-body">
      <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-content">
          <div class="m-portlet">
            <div class="m-portlet__head">
              <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                  <h3 class="m-portlet__head-text">
                    <a href="{{route('Collaborator::job@index')}}">
                      <i class="la la-arrow-left"></i> Quay lại
                    </a>
                  </h3>
                </div>
              </div>
            </div>
            <!-- /page header -->
            @if (session('success'))
              <div
                class="m-alert m-alert--outline m-alert--square m-alert--outline-2x alert alert-success alert-dismissible fade show"
                role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                </button>
                {{ session('success') }}
              </div>
            @endif
            <form method="POST" enctype="multipart/form-data" name="form" onsubmit=" return check()"
                  class="m-form m-form--fit m-form--label-align-right"
                  action="{{ route('Collaborator::job@store') }}">
              {{ csrf_field() }}
              <div class="m-portlet__body">
                <!------------------ Not in System--------------->
              {{--<div class="form-group m-form__group row">--}}
              {{--<label class="col-form-label col-lg-2 col-sm-12">Chưa có trên hệ thống </label>--}}
              {{--<div class="col-lg-6 col-md-9 col-sm-12">--}}
              {{--<div class="m-checkbox-inline">--}}
              {{--<label class="m-checkbox" style="font-weight: bold; color: red">--}}
              {{--Sản phẩm chưa có trên hệ thống--}}
              {{--<input type="checkbox" name="non_exist" value="1">--}}
              {{--<span></span>--}}
              {{--</label>--}}
              {{--</div>--}}
              {{--</div>--}}
              {{--</div>--}}
              <!------------------ On System--------------->
                <div class="form-group m-form__group row">
                  <div class="col-lg-6 col-md-9 col-sm-12">
                    <label style="font-weight: bold">
                      Sản phẩm đã có trên hệ thống
                    </label>
                  </div>
                </div>
                <div class="form-group m-form__group row" style="padding-top: 0">
                  <label class="col-form-label col-lg-2 col-sm-12" style="font-style: italic;font-weight: bold">Tình
                    trạng </label>
                  <div class="col-lg-9 col-md-9 col-sm-12">
                    <div class="m-checkbox-inline">
                      <label class="m-checkbox">
                        <input type="checkbox" name="non_name" value="1">
                        Chưa có tên
                        <span></span>
                      </label>
                      <label class="m-checkbox">
                        <input type="checkbox" name="non_images" value="1">
                        Chưa có hình ảnh
                        <span></span>
                      </label>
                      <label class="m-checkbox">
                        <input type="checkbox" name="non_descriptions" value="1">
                        Chưa có mô tả
                        <span></span>
                      </label>
                      <label class="m-checkbox">
                        <input type="checkbox" name="non_categories" value="1" checked="checked"
                               onclick="return false;">
                        Chưa có danh mục
                        <span></span>
                      </label>
                      <label class="m-checkbox">
                        <input type="checkbox" name="non_gln" value="1">
                        Chưa có thông tin Nhà sản xuất
                        <span></span>
                      </label>
                    </div>
                  </div>
                </div>
                <!--------------------------------->
              {{--<div class="form-group m-form__group row" style="margin-bottom: 0;padding-bottom: 0">--}}
              {{--<div class="col-lg-6 col-md-9 col-sm-12">--}}
              {{--<div class="m-checkbox-inline">--}}
              {{--<label class="m-checkbox">--}}
              {{--Lọc sản phẩm theo danh mục--}}
              {{--<input type="checkbox" name="category_filter">--}}
              {{--<span></span>--}}
              {{--</label>--}}
              {{--</div>--}}
              {{--</div>--}}
              {{--</div>--}}
              <!--------------------------------->
                <div class="categories form-group m-form__group row"
                     style="margin-left: 200px;margin-top: 0;margin-bottom: 0">

                </div>
                <!--------------------------------->
                <div class="form-group m-form__group row" style="margin-top: 0;padding-top: 0">
                  <div class="col-lg-6 col-md-9 col-sm-12">
                    <div class="m-checkbox-inline">
                      <label class="m-checkbox">
                        Lọc sản phẩm theo Nhà sản xuất
                        <input type="checkbox" name="gln_filter">
                        <span></span>
                      </label>
                    </div>
                  </div>
                </div>
                <!--------------------------------->
                <div class="gln form-group m-form__group row" style="margin-left: 200px;margin-top: 0;">

                </div>
                <!------------------ Group--------------->
                <div class="form-group m-form__group row" style="padding-top: 0">
                  <label class="col-form-label col-lg-3 col-sm-12" style="text-align:left;font-weight: bold">Chọn nhóm
                    cộng tác viên *</label>
                  <div class="col-lg-6 col-md-9 col-sm-12">
                    <select class="form-control m-input" data-col-index="7" id="group_id"
                            name="group_ids[]" required multiple>
                      <option></option>
                      @foreach($groups as $group)
                        <option value="{{$group->id}}"
                                @if( old('group_id') == $group->id) selected="selected" @endif
                          {{(isset($product) and ($product->group_id == $group->id))?'selected="selected"' :''}}
                        >{{$group->name}}</option>
                      @endforeach>
                    </select>
                  </div>
                  @if ($errors->has('group_id'))
                    <div class="help-block">{{ $errors->first('group_id') }}</div>
                  @endif
                </div>
                <!------------------ Contribution Info--------------->
                <div class="form-group m-form__group row">
                  <label class="col-form-label col-lg-3 col-sm-12" style="text-align:left;font-weight: bold">Chọn thông
                    tin cần đóng góp
                    *</label>
                  <div class="col-lg-9 col-md-9 col-sm-12">
                    <div class="m-checkbox-inline">
                      <label class="m-checkbox">
                        <input type="checkbox" name="need_update[]" id="name_info"
                               value="{{\App\Models\Collaborator\Need_Update_Product::NAME}}"

                        >
                        Tên sản phẩm
                        <span></span>
                      </label>
                      <label class="m-checkbox">
                        <input type="checkbox" name="need_update[]" id="images_info"
                               value="{{\App\Models\Collaborator\Need_Update_Product::IMAGES}}"

                        >
                        Hình ảnh sản phẩm
                        <span></span>
                      </label>
                      <label class="m-checkbox">
                        <input type="checkbox" name="need_update[]" id="descriptions_info"
                               value="{{\App\Models\Collaborator\Need_Update_Product::DESCRIPTIONS}}"

                        >
                        Mô tả sản phẩm
                        <span></span>
                      </label>
                      <label class="m-checkbox">
                        <input type="checkbox" name="need_update[]" id="category_info"
                               value="{{\App\Models\Collaborator\Need_Update_Product::CATEGORIES_INFO}}"
                               checked="checked" onclick="return false;">
                        Danh mục
                        <span></span>
                      </label>
                      <label class="m-checkbox">
                        <input type="checkbox" name="need_update[]" id="gln_info"
                               value="{{\App\Models\Collaborator\Need_Update_Product::GLN_INFO}}"
                        >
                        Nhà sản xuất
                        <span></span>
                      </label>
                    </div>
                  </div>
                </div>

                <div class="form-group m-form__group row">
                  <label class="col-form-label col-lg-3 col-sm-12" style="text-align:left;font-weight: bold">Cần tìm
                    kiếm ?</label>
                  <div class="col-lg-2 col-md-9 col-sm-12">
                    <label class="m-checkbox">
                      <input type="checkbox" name="need_search" id="need_search"
                             value="1"
                      >
                      Có
                      <span></span>
                    </label>
                  </div>
                </div>

                <div class="form-group m-form__group row">
                  <label class="col-form-label col-lg-3 col-sm-12" style="text-align:left;font-weight: bold">Thời gian
                    bắt đầu phân việc *</label>
                  <div class="col-lg-2 col-md-9 col-sm-12">
                    <label class="m-checkbox">
                      <input type="checkbox" name="immediately_start"
                      >
                      Tiến hành ngay
                      <span></span>
                    </label>
                  </div>
                  <div class="col-lg-3 col-md-9 col-sm-12">
                    <input type="text" class="form-control" name="scheduled_at" id="m_datepicker_1" autocomplete="off"
                           placeholder="Ngày bắt đầu" required
                           data-col-index="5"/>
                  </div>

                </div>
                <div class="m-portlet__foot m-portlet__foot--fit">
                  <div class="m-form__actions m-form__actions">
                    <div class="row">
                      <div class="col-lg-9 ml-lg-auto">
                        <button type="submit" class="btn btn-success">
                          Thêm mới
                        </button>
                        <button type="reset" class="btn btn-secondary">
                          Nhập lại
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('scripts')
  <script type="text/javascript" src="{{ asset('qr_code/metronic/js/vendors.bundle.js') }}"></script>
  <script type="text/javascript" src="{{ asset('qr_code/metronic/js/datepicker/bootstrap-datepicker.js') }}"></script>
  <script type="text/javascript">
    var url = "{{ route('Collaborator::product_category@root_cat')}}";
    $(document).ready(function () {

      // Basic
      $("#group_id").select2({
        placeholder: 'Chọn nhóm tác viên',
        allowClear: true
      });
      $("select").on("select2:select", function (evt) {
        var element = evt.params.data.element;
        var $element = $(element);
        $element.detach();
        $(this).append($element);
        $(this).trigger("change");
      });

      $("#name_info").prop('disabled', true);
      $("#images_info").prop('disabled', true);
      $("#descriptions_info").prop('disabled', true);
      $('#gln_info').prop('disabled', true);


      $('input[name="non_exist"]').click(function () {
        let status = $(this).is(':checked');
        if (status) {
          // if ($('input[name="category_filter"]').is(':checked')) {
          //   $('input[name="category_filter"]').click();
          // }
          if ($('input[name="gln_filter"]').is(':checked')) {
            $('input[name="gln_filter"]').click();
          }
        }
        $('input[name="non_name"]').prop('disabled', status);
        $('input[name="non_images"]').prop('disabled', status);
        $('input[name="non_descriptions"]').prop('disabled', status);
        $('input[name="non_categories"]').prop('disabled', status);
        $('input[name="non_gln"]').prop('disabled', status);
        // $("select[name='categories[]']").prop('disabled', status);
        $("textarea[name='glns']").prop('disabled', status);
        // $('input[name="category_filter"]').prop('disabled', status);
        $('input[name="gln_filter"]').prop('disabled', status);

      });

      $('input[name="non_name"]').click(function () {
        let status = $(this).is(':checked');
        $("#name_info").prop({'checked': status, 'disabled': !status});
      });

      $('input[name="non_images"]').click(function () {
        let status = $(this).is(':checked');
        $("#images_info").prop({'checked': status, 'disabled': !status});
      });

      $('input[name="non_descriptions"]').click(function () {
        let status = $(this).is(':checked');
        $("#descriptions_info").prop({'checked': status, 'disabled': !status});
      });

      // $('input[name="non_categories"]').click(function () {
      //   let status = $(this).is(':checked');
      //   $("#category_info").prop({'checked': status, 'disabled': !status});
      // });

      // $('input[name="non_gln"]').click(function () {
      //   let status = $(this).is(':checked');
      //   $("#category_info").prop({'checked': status, 'disabled': !status});
      // });

      $('input[name="immediately_start"]').click(function () {
        let status = $(this).is(':checked');
        $('input[name="scheduled_at"]').prop('disabled', status);
      });

      $("input[name='scheduled_at']").change(function () {
        var scheduled_at = this.value;
        var q = new Date();
        var m = q.getMonth();
        var d = q.getDate();
        var y = q.getFullYear();

        var today = new Date(y, m, d);
        if (scheduled_at === "") {
          return false;
        }
        if (Date.parse(scheduled_at.concat(" 00:00:00")) <= today) {
          alert("Không thể áp dụng ngày hết hạn này. Ngày hết hạn phải lớn hơn ngày hiện tại!");
          $("input[name='scheduled_at']").val('');
          return false;
        }
      });

      // $('input[name="non_categories"]').click(function () {
      //   let status = $(this).is(':checked');
      //   if (status) {
      //     $('input[name="category_filter"]').prop({'checked': !status, 'disabled': status});
      //     $(".categories").children().remove();
      //     $('#category_info').prop({'disabled': !status});
      //   } else {
      //     $('input[name="category_filter"]').prop({'disabled': status});
      //   }
      // });

      $('input[name="non_gln"]').click(function () {
        let status = $(this).is(':checked');
        if (status) {
          $('input[name="gln_filter"]').prop({'checked': !status, 'disabled': status});
          $(".gln").children().remove();
          $('#gln_info').prop({'disabled': !status, 'checked': status});
        } else {
          $('input[name="gln_filter"]').prop({'disabled': status});
          $('#gln_info').prop({'disabled': !status, 'checked': status});
        }
      });
      // $('input[name="category_filter"]').click(function () {
      //   let status = $(this).is(':checked');
      //   if (status) {
      //     $(".categories").append(
      //       `<div class="col-lg-4 col-md-9 col-sm-12">
      //                       <select class="category_select form-control m-input"
      //                   name="categories[]" required>
      //                       <option value="">Chọn danh mục</option>
      //                   </select>
      //                   </div>`
      //     );
      //     getData();
      //     $('#category_info').prop({'checked': !status, 'disabled': status});
      //   } else {
      //     $(".categories").children().remove();
      //     $('#category_info').prop({'disabled': status});
      //   }
      // });

      $('input[name="gln_filter"]').click(function () {
        let status = $(this).is(':checked');
        if (status) {
          $(".gln").append(
            `<div class="col-lg-5 col-md-9 col-sm-12">
                    <textarea name="glns" rows="4" cols="74" placeholder="8935212500124
8923512011414" required></textarea>
                    <div style="color: silver">Nhập mã GLN của Nhà sản xuất, mỗi GLN nằm trên 1 dòng</div>
                  </div>`
          );
          $('#gln_info').prop({'checked': !status, 'disabled': status});
        } else {
          $(".gln").children().remove();
          $('#gln_info').prop({'disabled': status});
        }
      });

    });

    {{--$("div.categories").delegate(".category_select", "change", function () {--}}
    {{--var category_id = $(this).val();--}}
    {{--var current_select = $(this);--}}
    {{--var parent = current_select.parent();--}}
    {{--if (!category_id) {--}}
    {{--parent.nextAll().remove();--}}
    {{--return false;--}}
    {{--}--}}
    {{--let url = "{{ route('Collaborator::product_category@sub_cat',":id")}}";--}}
    {{--url = url.replace(':id', category_id);--}}
    {{--$.ajax({--}}
    {{--url: url,--}}
    {{--method: 'GET',--}}
    {{--success: function (data) {--}}
    {{--let obj = JSON.parse(data);--}}
    {{--console.log(obj);--}}
    {{--if (obj.length > 0) {--}}
    {{--parent.nextAll().remove();--}}
    {{--$(".categories").append(--}}
    {{--`<div class="col-lg-4 col-md-9 col-sm-12">--}}
    {{--<select class="category_select form-control m-input"--}}
    {{--name="categories[]">--}}
    {{--<option value="">Chọn danh mục</option>--}}
    {{--</select>--}}
    {{--</div>`--}}
    {{--);--}}
    {{--let next = parent.next();--}}
    {{--for (i = 0; i < obj.length; i++) {--}}
    {{--next.children().append(--}}
    {{--`<option value="${obj[i].id}">${obj[i].name}</option>`--}}
    {{--);--}}
    {{--}--}}
    {{--}--}}
    {{--}--}}
    {{--});--}}
    {{--});--}}

    {{--function getData() {--}}
    {{--$.ajax({--}}
    {{--url: url,--}}
    {{--method: 'GET',--}}
    {{--success: function (data) {--}}
    {{--let obj = JSON.parse(data);--}}
    {{--console.log(obj);--}}
    {{--for (i = 0; i < obj.length; i++) {--}}
    {{--$("select[name='categories[]']").append(--}}
    {{--`<option value="${obj[i].id}">${obj[i].name}</option>`--}}
    {{--);--}}
    {{--}--}}
    {{--}--}}
    {{--});--}}
    {{--}--}}


    function check() {
      var productFilterCheckboxes = document.querySelectorAll('input[name="non_exist"]:checked,input[name="non_name"]:checked,input[name="non_images"]:checked,input[name="non_descriptions"]:checked,input[name="non_categories"]:checked,input[name="non_gln"]:checked');
      if (parseInt(productFilterCheckboxes.length) === 0) {
        alert("Bạn chưa chọn loại Sản phẩm cần đóng góp thông tin!");
        return false;
      }
      var needUpdateCheckboxes = document.querySelectorAll('input[name="need_update[]"]:checked');
      if (parseInt(needUpdateCheckboxes.length) === 0) {
        alert("Bạn cần chọn một loại thông tin cần đóng góp!");
        return false;
      }
      return true;
    }


  </script>

@endpush
