@extends('qrcode.layouts.app')

@section('body_class', 'm-page--fluid m-page--loading-enabled m-page--loading m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default')

@push('scripts')
  <script type="text/javascript">
    <?php
      $sharedData = [
        'config' => [
          'csrfToken' => csrf_token(),
          'logoutUrl' => route('logout'),
          'isNative' => true,
        ],
        'loggedInAccount' => auth()->check() ? auth()->user() : null,
      ];
      ?>
      window.SHARED_DATA = {!! json_encode($sharedData) !!}
  </script>
  <script src="https://cdn.jsdelivr.net/npm/tinymce@4.7.11/tinymce.min.js" type="text/javascript"></script>
  <script src="{{ mix('js/app.js') }}"></script>
@endpush

@section('content')
  <!-- begin::Page loader -->
  <div class="m-page-loader m-page-loader--base">
    <div class="m-blockui">
      <span>
        Please wait...
      </span>
      <span>
        <div class="m-loader m-loader--brand"></div>
      </span>
    </div>
  </div>
  <!-- end::Page Loader -->
  <div id="app" class="m-grid m-grid--hor m-grid--root m-page">
    <div class="m-header__bottom">
      <div class="m-container m-container--responsive m-container--xxl m-container--full-height m-page__container">
        <div class="m-stack m-stack--ver m-stack--desktop">
          <div class="m-stack__item m-stack__item--middle m-stack__item--fluid">
            <app-header></app-header>
          </div>
        </div>
      </div>
    </div>

    <div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop  m-body m-page__container">
      <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader ">
          <div class="d-flex align-items-center">
            <div class="mr-auto">
              <h3 class="m-subheader__title ">
                QUẢN LÝ CÔNG VIỆC
              </h3>
            </div>
            @can("COLLABORATOR-addJob")
              <div style="float: right"><a href="{{route('Collaborator::job@add')}}"
                                           class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air"><span>
                                    <i class="la la-plus-circle"></i>
                                    <span>Đặt lịch phân công công việc</span></span></a>
              </div>
            @endcan
          </div>
        </div>
        <!-- END: Subheader -->
        @can("COLLABORATOR-viewJobs")
          <div class="m-content">
            <!--Begin::Section-->
            <div class="m-portlet m-portlet--mobile">
              <div class="m-portlet__body">
                @if (session('success'))
                  <div
                    class="m-alert m-alert--outline m-alert--square m-alert--outline-2x alert alert-success alert-dismissible fade show"
                    role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    </button>
                    {{ session('success') }}
                  </div>
              @endif
              <!--begin::Table-->
                <div class="table-responsive">
                  <table class="table table-striped- table-bordered table-hover table-checkable"
                         style="width: 1650px">
                    <thead>
                    <tr>
                      <th style="font-weight: bold;width:50px;">Id</th>
                      <th style="font-weight: bold;width:300px;">Nhóm thông tin cần đóng góp</th>
                      <th style="font-weight: bold;width:400px;">Điều kiện lọc</th>
                      <th style="font-weight: bold;width:400px;">Nhóm cộng tác viên</th>
                      <th style="font-weight: bold;width:200px;">Ngày tạo</th>
                      <th style="font-weight: bold;width:150px;">Chi phí tìm kiếm thông tin (USD)</th>
                      <th style="font-weight: bold;width:200px;">Ngày bắt đầu công việc</th>
                      <th style="font-weight: bold;width:200px;">Trạng thái</th>
                      {{--<th style="font-weight: bold;width:200px;text-align: center">Hành động</th>--}}
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($jobs as $job)
                      <tr role="row">
                        <td class="id">{{$job->id}}</td>
                        <td>{{$job->need_update_name}}</td>
                        <td>{{$job->filters_content}}</td>
                        <td>{{$job->group_names}}</td>
                        <td>{{$job->created_at}}</td>
                        <td>{{$job->search_cost/1000}}</td>
                        <td>{{$job->scheduled_at}}</td>
                        <td>
                          @if($job->status==\App\Models\Collaborator\Job::PENDING_STATUS || ($job->status==\App\Models\Collaborator\Job::POURING_GTINS_STATUS))
                            <span class="status m-badge m-badge--warning m-badge--wide">{{$job->status_name}}</span>
                          @elseif($job->status==\App\Models\Collaborator\Job::POURED_GTINS_STATUS)
                            <span class="status m-badge m-badge--success m-badge--wide">{{$job->status_name}}</span>
                          @elseif($job->status==\App\Models\Collaborator\Job::GOT_GTINS_STATUS)
                            <span class="status m-badge m-badge--info m-badge--wide">{{$job->status_name}}</span>
                          @else
                            <span class="status m-badge m-badge--danger m-badge--wide">{{$job->status_name}}</span>
                          @endif
                        </td>
                      {{--<td style="text-align: center">--}}
                      {{--</td>--}}
                    @endforeach
                    </tbody>
                  </table>
                </div>
                <!--end::Table-->
                <div style="float:right;"><?php echo $jobs->links(); ?></div>
              </div>
            </div>
          </div>
        @endcan
      </div>
    </div>
  </div>
  <router-view></router-view>
@endsection
@push('scripts')
  <script type="text/javascript" src="{{ asset('qr_code/metronic/js/vendors.bundle.js') }}"></script>
  <script>

    function delItem() {
      let conf = confirm("Bạn chắc chắn muốn xoá không?");
      return conf;
    }
  </script>
@endpush
