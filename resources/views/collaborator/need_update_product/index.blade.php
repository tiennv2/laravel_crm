@extends('qrcode.layouts.app')
@push('styles')
  <style>
    .select2 {
      width: 100% !important;
    }
  </style>
@endpush
@section('body_class', 'm-page--fluid m-page--loading-enabled m-page--loading m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default')

@push('scripts')
  <script type="text/javascript">
    <?php
      $sharedData = [
        'config' => [
          'csrfToken' => csrf_token(),
          'logoutUrl' => route('logout'),
          'isNative' => true,
        ],
        'loggedInAccount' => auth()->check() ? auth()->user() : null,
      ];
      ?>
      window.SHARED_DATA = {!! json_encode($sharedData) !!}
  </script>
  <script src="https://cdn.jsdelivr.net/npm/tinymce@4.7.11/tinymce.min.js" type="text/javascript"></script>
  <script src="{{ mix('js/app.js') }}"></script>
@endpush

@section('content')
  <!-- begin::Page loader -->
  <div class="m-page-loader m-page-loader--base">
    <div class="m-blockui">
      <span>
        Please wait...
      </span>
      <span>
        <div class="m-loader m-loader--brand"></div>
      </span>
    </div>
  </div>
  <!-- end::Page Loader -->
  <div id="app" class="m-grid m-grid--hor m-grid--root m-page">
    <div class="m-header__bottom">
      <div class="m-container m-container--responsive m-container--xxl m-container--full-height m-page__container">
        <div class="m-stack m-stack--ver m-stack--desktop">
          <div class="m-stack__item m-stack__item--middle m-stack__item--fluid">
            <app-header></app-header>
          </div>
        </div>
      </div>
    </div>

    <div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop  m-body m-page__container">
      <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader ">
          <div class="d-flex align-items-center">
            <div class="mr-auto">
              <h3 class="m-subheader__title ">
                QUẢN LÝ BARCODE SẢN PHẨM CẦN ĐÓNG GÓP
              </h3>
            </div>
            @can("COLLABORATOR-addNeedUpdateProducts")
              <div style="float: right"><a href="{{route('Collaborator::need_update_product@add')}}"
                                           class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air"><span>
                                    <i class="la la-plus-circle"></i>
                                    <span>Thêm mới BARCODE</span></span></a>
              </div>
            @endcan
          </div>
        </div>
        <!-- END: Subheader -->
        @can("COLLABORATOR-viewNeedUpdateProducts")
          <div class="m-content">
            <!--Begin::Section-->
            <div class="m-portlet m-portlet--mobile">
              <div class="m-portlet__body">
                <form class="m-form m-form--fit m--margin-bottom-20"
                      action="{{route('Collaborator::need_update_product@index')}}" method="get">
                  <div class="row m--margin-bottom-20">
                    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                      <label>
                        Mã GTIN:
                      </label>
                      <input type="text" class="form-control m-input" name="gtin"
                             value="{{ Request::input('gtin') }}"
                             data-col-index="4">
                    </div>
                    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                      <label>
                        Nhóm CTV:
                      </label>
                      <select class="form-control m-input" data-col-index="7" id="group_id"
                              name="group_id">
                        <option value="" selected="selected"></option>
                        @foreach($groups as $group)
                          <option value="{{$group->id}}"
                            {{Request::input('group_id')==$group->id?'selected="selected"':''}}
                          >{{$group->name}}</option>
                        @endforeach
                      </select>
                    </div>
                    <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile">
                      <label>
                        Tên CTV:
                      </label>
                      <select class="form-control m-input" data-col-index="7" id="collaborator_id"
                              name="collaborator_id">
                        <option value="" selected="selected"></option>
                        @foreach($collaborators as $collaborator)
                          <option value="{{$collaborator->id}}"
                            {{Request::input('collaborator_id')==$collaborator->id?'selected="selected"':''}}
                          >{{$collaborator->name}}</option>
                        @endforeach
                      </select>
                    </div>
                    <div class="col-lg-4 m--margin-bottom-10-tablet-and-mobile">
                      <label>
                        Ngày tạo:
                      </label>
                      <div class="input-daterange input-group">
                        <input type="text" class="form-control" name="from" id="m_datepicker_1"
                               value="{{ Request::input('from') }}" autocomplete="off"
                               placeholder="Từ"
                               data-col-index="5"/>
                        <div class="input-group-append">
													<span class="input-group-text">
														<i class="la la-ellipsis-h"></i>
													</span>
                        </div>
                        <input type="text" class="form-control" name="to" id="m_datepicker_1"
                               value="{{ Request::input('to') }}" autocomplete="off"
                               placeholder="Đến"
                               data-col-index="5"/>
                      </div>
                    </div>

                  </div>
                  <div class="row m--margin-bottom-20">
                    <div class="col-lg-4 m--margin-bottom-10-tablet-and-mobile">
                      <label>
                        Thông tin cần đóng góp:
                      </label>
                      <select class="form-control m-input" data-col-index="7" id="need_update"
                              name="need_update[]" multiple="multiple">
                        <option></option>
                        <option
                          value="{{\App\Models\Collaborator\Need_Update_Product::GLN_INFO}}" {{(Request::input('need_update') && in_array(\App\Models\Collaborator\Need_Update_Product::GLN_INFO,Request::input('need_update')))?'selected="selected"':''}}>
                          Nhà sản xuất
                        </option>
                        <option
                          value="{{\App\Models\Collaborator\Need_Update_Product::CATEGORIES_INFO}}" {{(Request::input('need_update') && in_array(\App\Models\Collaborator\Need_Update_Product::CATEGORIES_INFO,Request::input('need_update')))?'selected="selected"':''}}>
                          Danh mục
                        </option>
                        <option
                          value="{{\App\Models\Collaborator\Need_Update_Product::NAME}}" {{(Request::input('need_update') && in_array(\App\Models\Collaborator\Need_Update_Product::NAME,Request::input('need_update')))?'selected="selected"':''}}>
                          Tên sản phẩm
                        </option>
                        <option
                          value="{{\App\Models\Collaborator\Need_Update_Product::IMAGES}}" {{(Request::input('need_update') && in_array(\App\Models\Collaborator\Need_Update_Product::IMAGES,Request::input('need_update')))?'selected="selected"':''}}>
                          Ảnh sản phẩm
                        </option>
                        <option
                          value="{{\App\Models\Collaborator\Need_Update_Product::DESCRIPTIONS}}" {{(Request::input('need_update') && in_array(\App\Models\Collaborator\Need_Update_Product::DESCRIPTIONS,Request::input('need_update')))?'selected="selected"':''}}>
                          Mô tả sản phẩm
                        </option>
                      </select>
                    </div>
                    <div class="col-lg-4 m--margin-bottom-10-tablet-and-mobile">
                      <label style="color: white">Hành động </label>
                      <div>
                        <button class="btn btn-brand m-btn m-btn--icon" type="submit">
												<span>
													<i class="la la-search"></i>
													<span>
														Tìm kiếm
													</span>
												</span>
                        </button>
                        &nbsp;&nbsp;
                        <button class="btn btn-secondary m-btn m-btn--icon" type="button"
                                onclick="window.location='{{route('Collaborator::need_update_product@index')}}'">
												<span>
													<i class="la la-refresh"></i>
													<span>
														Làm mới
													</span>
												</span>
                        </button>
                      </div>
                    </div>
                  </div>
                </form>
                @if (session('success'))
                  <div
                    class="m-alert m-alert--outline m-alert--square m-alert--outline-2x alert alert-success alert-dismissible fade show"
                    role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    </button>
                    {{ session('success') }}
                  </div>
                @endif
                <div
                  style="width:300px;text-align:center;margin-bottom: 20px;margin-top: 50px;padding:10px;font-weight: bold;font-style: italic;border: solid 1px;border-radius: 6px">
                  Đang hiển thị {{$count_record_in_page}}/{{$count_record_total}} bản ghi
                </div>
                <!--begin::Table-->
                <div class="table-responsive">
                  <table class="table table-striped- table-bordered table-hover table-checkable"
                         style="width: 1600px">
                    <thead>
                    <tr>
                      <th style="font-weight: bold;width:50px;">Id</th>
                      <th style="font-weight: bold;width:100px;">Mã GTIN</th>
                      <th style="font-weight: bold;width:200px;">Nhóm cộng tác viên tham gia đóng góp thông tin</th>
                      <th style="font-weight: bold;width:400px;text-align: center">Thông tin cần đóng góp</th>
                      <th style="font-weight: bold;width:300px;text-align: center">Cộng tác viên đang thực hiện</th>
                      <th style="font-weight: bold;width:200px;text-align: center">Ngày tạo</th>
                      <th style="font-weight: bold;width:100px;text-align: center">Ẩn/Hiện mã Sản phẩm cần đóng góp</th>
                      <th style="font-weight: bold;width:200px;text-align: center">Hành động</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($products as $product)
                      <tr role="row">
                        <td class="id">{{$product->id}}</td>
                        <td>{{$product->gtin}}</td>
                        <td>{{$product->group_name}}</td>
                        <td>{{$product->need_update_text}}</td>
                        <td>{{$product->assigned_name}}</td>
                        <td style="text-align: center">{{$product->created_at}}</td>
                        <td style="text-align: center">
                          @can("COLLABORATOR-hideNeedUpdateProducts")
                            @if(!$product->assigned_to)
                              <span class="m-switch m-switch--icon">
                            <label>
                              <input type="checkbox"
                                     @if($product->status == 1)
                                     checked="checked"
                                     @endif
                                     name="show_gtin"
                                     value="{{$product->status}}">
                              <span></span>
                            </label>
                          </span>
                            @endif
                          @endcannot
                        </td>
                        <td style="text-align: center">
                          @can("COLLABORATOR-updateNeedUpdateProducts")
                            <a href="{{route("Collaborator::need_update_product@edit",[$product->id])}}" title="Sửa"
                               class="btn btn-info m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                              <i class="la la-edit"></i>
                            </a>
                          @endcan
                          @can("COLLABORATOR-delNeedUpdateProducts")
                            <a href="{{route("Collaborator::need_update_product@delete",[$product->id])}}"
                               onclick="return delItem()" title="Xóa"
                               class="btn btn-danger m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                              <i class="la la-trash"></i>
                            </a>
                          @endcan
                        </td>
                    @endforeach
                    </tbody>
                  </table>
                </div>
                <!--end::Table-->
                <div style="float:right;"><?php echo $products->links(); ?></div>
              </div>
            </div>
          </div>
        @endcan
      </div>
    </div>
  </div>
  <router-view></router-view>
@endsection
@push('scripts')
  <script type="text/javascript" src="{{ asset('qr_code/metronic/js/vendors.bundle.js') }}"></script>
  <script type="text/javascript" src="{{ asset('qr_code/metronic/js/datepicker/bootstrap-datepicker.js') }}"></script>
  <script>
    $(document).ready(function () {
      // Basic
      $("#need_update").select2({
        placeholder: 'Chọn loại thông tin',
        allowClear: true
      });
      $("#group_id").select2({
        placeholder: 'Chọn nhóm CTV',
        allowClear: true
      });
      $("#collaborator_id").select2({
        placeholder: 'Chọn CTV',
        allowClear: true
      });

      $("select").on("select2:select", function (evt) {
        var element = evt.params.data.element;
        var $element = $(element);
        $element.detach();
        $(this).append($element);
        $(this).trigger("change");
      });
    });

    $('input[name="show_gtin"]').click(function () {
      let status = $(this).is(':checked');
      let row = $(this).closest("tr");    // Find the row
      let id = row.find(".id").text(); // Find the text
      var url = "";
      if (status) {
        url = "{{ route('Collaborator::need_update_product@showGtin',':id')}}";
      } else {
        url = "{{ route('Collaborator::need_update_product@hideGtin',':id')}}";
      }
      url = url.replace(':id', id);
      $.ajax({
        url: url,
        method: 'GET',
      });
    });

    function delItem() {
      let conf = confirm("Bạn chắc chắn muốn xoá không ?");
      return conf;
    }
  </script>
@endpush
