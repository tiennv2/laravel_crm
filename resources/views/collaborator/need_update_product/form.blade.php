@extends('qrcode.layouts.app')
@push('styles')
<style>
  .help-block {
    color: red;
  }
</style>

@endpush
@section('body_class', 'm-page--fluid m-page--loading-enabled m-page--loading m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default')
@push('scripts')
  <script type="text/javascript">
    <?php
      $sharedData = [
        'config' => [
          'csrfToken' => csrf_token(),
          'logoutUrl' => route('logout'),
          'isNative' => true,
        ],
        'loggedInAccount' => auth()->check() ? auth()->user() : null,
      ];
      ?>
      window.SHARED_DATA = {!! json_encode($sharedData) !!}
  </script>
  <script src="https://cdn.jsdelivr.net/npm/tinymce@4.7.11/tinymce.min.js" type="text/javascript"></script>
  <script src="{{ mix('js/app.js') }}"></script>
@endpush

@section('content')
  <!-- begin::Page loader -->
  <div class="m-page-loader m-page-loader--base">
    <div class="m-blockui">
      <span>
        Please wait...
      </span>
      <span>
        <div class="m-loader m-loader--brand"></div>
      </span>
    </div>
  </div>
  <!-- end::Page Loader -->
  <div id="app" class="m-grid m-grid--hor m-grid--root m-page">
    <app-header></app-header>
    <div
      class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop 	m-container m-container--responsive m-container--xxl m-page__container m-body">
      <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-content">
          <div class="m-portlet">
            <div class="m-portlet__head">
              <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                  <h3 class="m-portlet__head-text">
                    <a href="{{route('Collaborator::need_update_product@index')}}">
                      <i class="la la-arrow-left"></i> Quay lại
                    </a>
                  </h3>
                </div>
              </div>
            </div>
            <!-- /page header -->
            @if (session('success'))
              <div
                class="m-alert m-alert--outline m-alert--square m-alert--outline-2x alert alert-success alert-dismissible fade show"
                role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                </button>
                {{ session('success') }}
              </div>
            @endif
            <form method="POST" enctype="multipart/form-data" name="form" onsubmit=" return check()"
                  class="m-form m-form--fit m-form--label-align-right"
                  action="{{ isset($product) ? route('Collaborator::need_update_product@update', [$product->id] ): route('Collaborator::need_update_product@store') }}">
              {{ csrf_field() }}
              @if (isset($product))
                <input type="hidden" name="_method" value="PUT">
              @endif
              <div class="m-portlet__body">
                <!------------------ Group Name--------------->
                <div class="form-group m-form__group row">
                  <label class="col-form-label col-lg-3 col-sm-12">Thêm barcode sản phẩm *</label>
                  <div class="col-lg-6 col-md-9 col-sm-12">
                    @if (isset($product))
                      <input type="text" name="gtin"
                             class="form-control"
                             value="{{ old('gtin') ?: @$product->gtin }}"
                             required/>
                    @else
                      <textarea name="gtin" rows="10" cols="74" placeholder="8935212500124
8923512011414" required>
                    </textarea>
                      <div style="color: silver">Mỗi barcode nằm trên 1 dòng</div>
                    @endif
                    @if ($errors->has('gtin'))
                      <div class="help-block">{{ $errors->first('gtin') }}</div>
                    @endif
                  </div>
                </div>
              <!------------------ Group--------------->
              <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">Chọn nhóm cộng tác viên</label>
                <div class="col-lg-6 col-md-9 col-sm-12">
                  <select class="form-control m-input" data-col-index="7" id="group_id"
                          name="group_ids[]" multiple required>
                    <option></option>
                    @foreach($groups as $group)
                      <option value="{{$group->id}}"
                              @if( old('group_id') == $group->id) selected="selected" @endif
                        {{(isset($product) and (in_array($group->id,$product->group_ids)))?'selected="selected"' :''}}
                      >{{$group->name}}</option>
                    @endforeach>
                  </select>
                </div>
                @if ($errors->has('group_id'))
                  <div class="help-block">{{ $errors->first('group_id') }}</div>
                @endif
              </div>
              <!------------------ Contribution Info--------------->
              <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-3 col-sm-12">Chọn thông tin cần đóng góp *</label>
                <div class="col-lg-6 col-md-9 col-sm-12">
                  <div class="m-checkbox-inline">
                    <label class="m-checkbox">
                      <input type="checkbox" name="need_update[]"
                             value="{{\App\Models\Collaborator\Need_Update_Product::GLN_INFO}}"
                        {{ (isset($product) && $product->need_update & \App\Models\Collaborator\Need_Update_Product::GLN_INFO) ? 'checked' : '' }}
                      >
                      Nhà sản xuất
                      <span></span>
                    </label>
                    <label class="m-checkbox">
                      <input type="checkbox" name="need_update[]"
                             value="{{\App\Models\Collaborator\Need_Update_Product::CATEGORIES_INFO}}"
                        {{ (isset($product) && $product->need_update & \App\Models\Collaborator\Need_Update_Product::CATEGORIES_INFO) ? 'checked' : '' }}
                      >
                      Danh mục
                      <span></span>
                    </label>
                    <label class="m-checkbox">
                      <input type="checkbox" name="need_update[]"
                             value="{{\App\Models\Collaborator\Need_Update_Product::NAME}}"
                        {{ (isset($product) && $product->need_update & \App\Models\Collaborator\Need_Update_Product::NAME) ? 'checked' : '' }}
                      >
                      Tên sản phẩm
                      <span></span>
                    </label>
                    <label class="m-checkbox">
                      <input type="checkbox" name="need_update[]"
                             value="{{\App\Models\Collaborator\Need_Update_Product::IMAGES}}"
                        {{ (isset($product) && $product->need_update & \App\Models\Collaborator\Need_Update_Product::IMAGES) ? 'checked' : '' }}
                      >
                      Ảnh sản phẩm
                      <span></span>
                    </label>
                    <label class="m-checkbox">
                      <input type="checkbox" name="need_update[]"
                             value="{{\App\Models\Collaborator\Need_Update_Product::DESCRIPTIONS}}"
                        {{ (isset($product) && $product->need_update & \App\Models\Collaborator\Need_Update_Product::DESCRIPTIONS) ? 'checked' : '' }}
                      >
                     Mô tả sản phẩm
                      <span></span>
                    </label>
                  </div>
                </div>
              </div>
                <div class="form-group m-form__group row">
                  <label class="col-form-label col-lg-3 col-sm-12" style="text-align:left;font-weight: bold">Cần tìm
                    kiếm ?</label>
                  <div class="col-lg-2 col-md-9 col-sm-12">
                    <label class="m-checkbox">
                      <input type="checkbox" name="need_search" id="need_search"
                             value="1"
                      >
                      Có
                      <span></span>
                    </label>
                  </div>
                </div>
              <div class="m-portlet__foot m-portlet__foot--fit">
                <div class="m-form__actions m-form__actions">
                  <div class="row">
                    <div class="col-lg-9 ml-lg-auto">
                      <button type="submit" class="btn btn-success">
                        {{ isset($product) ? 'Cập nhật' : 'Thêm mới' }}
                      </button>
                      <button type="reset" class="btn btn-secondary">
                        {{ isset($product) ? 'Hủy bỏ' : 'Nhập lại' }}
                      </button>
                    </div>
                  </div>
                </div>
              </div>
          </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  </div>
@endsection

@push('scripts')
  <script type="text/javascript" src="{{ asset('qr_code/metronic/js/vendors.bundle.js') }}"></script>
  <script type="text/javascript">
    $(document).ready(function () {
      // Basic
      $("#group_id").select2({
        placeholder: 'Chọn nhóm tác viên',
        allowClear: true
      });
      $("select").on("select2:select", function (evt) {
        var element = evt.params.data.element;
        var $element = $(element);
        $element.detach();
        $(this).append($element);
        $(this).trigger("change");
      });
    });

    function check() {
      var checkboxes = document.querySelectorAll('input[type=checkbox]:checked');
      if (parseInt(checkboxes.length) === 0) {
        alert("Bạn cần chọn một loại thông tin cần đóng góp!");
        return false;
      }
      return true;
    }
  </script>

@endpush
