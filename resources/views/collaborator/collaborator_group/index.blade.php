@extends('qrcode.layouts.app')

@section('body_class', 'm-page--fluid m-page--loading-enabled m-page--loading m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default')

@push('scripts')
  <script type="text/javascript">
    <?php
      $sharedData = [
        'config' => [
          'csrfToken' => csrf_token(),
          'logoutUrl' => route('logout'),
          'isNative' => true,
        ],
        'loggedInAccount' => auth()->check() ? auth()->user() : null,
      ];
      ?>
      window.SHARED_DATA = {!! json_encode($sharedData) !!}
  </script>
  <script src="https://cdn.jsdelivr.net/npm/tinymce@4.7.11/tinymce.min.js" type="text/javascript"></script>
  <script src="{{ mix('js/app.js') }}"></script>
@endpush

@section('content')
  <!-- begin::Page loader -->
  <div class="m-page-loader m-page-loader--base">
    <div class="m-blockui">
      <span>
        Please wait...
      </span>
      <span>
        <div class="m-loader m-loader--brand"></div>
      </span>
    </div>
  </div>
  <!-- end::Page Loader -->
  <div id="app" class="m-grid m-grid--hor m-grid--root m-page">
    <div class="m-header__bottom">
      <div class="m-container m-container--responsive m-container--xxl m-container--full-height m-page__container">
        <div class="m-stack m-stack--ver m-stack--desktop">
          <div class="m-stack__item m-stack__item--middle m-stack__item--fluid">
            <app-header></app-header>
          </div>
        </div>
      </div>
    </div>

    <div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop  m-body m-page__container">
      <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader ">
          <div class="d-flex align-items-center">
            <div class="mr-auto">
              <h3 class="m-subheader__title ">
                QUẢN LÝ NHÓM CỘNG TÁC VIÊN
              </h3>
            </div>
            @can("COLLABORATOR-addNewCollaboratorGroup")
              <div style="float: right"><a href="{{route('Collaborator::collaborator_group@add')}}"
                                           class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air"><span>
                                    <i class="la la-plus-circle"></i>
                                    <span>Thêm mới nhóm CTV</span></span></a>
              </div>
            @endcan
          </div>
        </div>
        <!-- END: Subheader -->
        @can("COLLABORATOR-viewCollaboratorGroups")
          <div class="m-content">
            <!--Begin::Section-->
            <div class="m-portlet m-portlet--mobile">
              <div class="m-portlet__body">
                @if (session('success'))
                  <div
                    class="m-alert m-alert--outline m-alert--square m-alert--outline-2x alert alert-success alert-dismissible fade show"
                    role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    </button>
                    {{ session('success') }}
                  </div>
              @endif
              <!--begin::Table-->
                <div class="table-responsive">
                  <table class="table table-striped- table-bordered table-hover table-checkable"
                         style="width: 1650px">
                    <thead>
                    <tr>
                      <th style="font-weight: bold;width:50px;">Id</th>
                      <th style="font-weight: bold;width:300px;">Tên nhóm</th>
                      <th style="font-weight: bold;width:300px;">Các thành viên</th>
                      <th style="font-weight: bold;width:400px;">Mô tả</th>
                      <th style="font-weight: bold;width:200px;text-align: center">Hành động</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($groups as $group)
                      <tr role="row">
                        <td class="id">{{$group->id}}</td>
                        <td>{{$group->name}}</td>
                        <td>{{$group->group_members}}</td>
                        <td>{{$group->description}}</td>
                        <td style="text-align: center">
                          @can("COLLABORATOR-updateCollaboratorGroup")
                            <a href="{{route("Collaborator::collaborator_group@edit",[$group->id])}}" title="Sửa"
                               class="btn btn-info m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                              <i class="la la-edit"></i>
                            </a>
                          @endcan
                        </td>
                    @endforeach
                    </tbody>
                  </table>
                </div>
                <!--end::Table-->
                <div style="float:right;"><?php echo $groups->links(); ?></div>
              </div>
            </div>
          </div>
        @endcan
      </div>
    </div>
  </div>
  <router-view></router-view>
@endsection
@push('scripts')
  <script type="text/javascript" src="{{ asset('qr_code/metronic/js/vendors.bundle.js') }}"></script>
  <script>

    function delItem() {
      let conf = confirm("Bạn chắc chắn muốn xoá không?");
      return conf;
    }
  </script>
@endpush
