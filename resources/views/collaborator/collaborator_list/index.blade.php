@extends('qrcode.layouts.app')

@section('body_class', 'm-page--fluid m-page--loading-enabled m-page--loading m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default')

@push('scripts')
  <script type="text/javascript">
    <?php
      $sharedData = [
        'config' => [
          'csrfToken' => csrf_token(),
          'logoutUrl' => route('logout'),
          'isNative' => true,
        ],
        'loggedInAccount' => auth()->check() ? auth()->user() : null,
      ];
      ?>
      window.SHARED_DATA = {!! json_encode($sharedData) !!}
  </script>
  <script src="https://cdn.jsdelivr.net/npm/tinymce@4.7.11/tinymce.min.js" type="text/javascript"></script>
  <script src="{{ mix('js/app.js') }}"></script>
@endpush

@section('content')
  <!-- begin::Page loader -->
  <div class="m-page-loader m-page-loader--base">
    <div class="m-blockui">
      <span>
        Please wait...
      </span>
      <span>
        <div class="m-loader m-loader--brand"></div>
      </span>
    </div>
  </div>
  <!-- end::Page Loader -->
  <div id="app" class="m-grid m-grid--hor m-grid--root m-page">
    <div class="m-header__bottom">
      <div class="m-container m-container--responsive m-container--xxl m-container--full-height m-page__container">
        <div class="m-stack m-stack--ver m-stack--desktop">
          <div class="m-stack__item m-stack__item--middle m-stack__item--fluid">
            <app-header></app-header>
          </div>
        </div>
      </div>
    </div>

    <div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop  m-body m-page__container">
      <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader ">
          <div class="d-flex align-items-center">
            <div class="mr-auto">
              <h3 class="m-subheader__title ">
                QUẢN LÝ CỘNG TÁC VIÊN
              </h3>
            </div>
            @can("COLLABORATOR-addCollaborator")
            <div style="float: right"><a href="{{route('Collaborator::collaborator_list@add')}}"
                                         class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air"><span>
                                    <i class="la la-plus-circle"></i>
                                    <span>Thêm mới CTV</span></span></a>
            </div>
              @endcan
          </div>
        </div>
        <!-- END: Subheader -->
        @can("COLLABORATOR-viewCollaboratorList")
        <div class="m-content">
          <!--Begin::Section-->
          <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__body">
              <form class="m-form m-form--fit m--margin-bottom-20"
                    action="{{route('Collaborator::collaborator_list@index')}}" method="get">
                <div class="row m--margin-bottom-20">
                  <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                    <label>
                      Tìm kiếm CTV:
                    </label>
                    <select class="form-control m-input" data-col-index="7" id="collaborator_id"
                            name="collaborator_id">
                      <option value="" selected="selected"></option>
                      @foreach($collaborator_total as $collaborator)
                        <option value="{{$collaborator->id}}"
                          {{Request::input('collaborator_id')==$collaborator->id?'selected="selected"':''}}
                        >{{$collaborator->name}} ({{$collaborator->icheck_id}})
                        </option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="row m--margin-bottom-20">
                  <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                    <label style="color: white">Hành động </label>
                    <div>
                      <button class="btn btn-brand m-btn m-btn--icon" type="submit">
												<span>
													<i class="la la-search"></i>
													<span>
														Tìm kiếm
													</span>
												</span>
                      </button>
                      &nbsp;&nbsp;
                      <button class="btn btn-secondary m-btn m-btn--icon" type="button"
                              onclick="window.location='{{route('Collaborator::collaborator_list@index')}}'">
												<span>
													<i class="la la-refresh"></i>
													<span>
														Làm mới
													</span>
												</span>
                      </button>
                    </div>
                  </div>
                </div>
              </form>
              @if (session('success'))
                <div
                  class="m-alert m-alert--outline m-alert--square m-alert--outline-2x alert alert-success alert-dismissible fade show"
                  role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  </button>
                  {{ session('success') }}
                </div>
            @endif
            <!--begin::Table-->
              <div class="table-responsive">
                <table class="table table-striped- table-bordered table-hover table-checkable"
                       style="width: 1600px">
                  <thead>
                  <tr>
                    <th style="font-weight: bold;width:50px;">Id</th>
                    <th style="font-weight: bold;width:300px;">Icheck_ID</th>
                    <th style="font-weight: bold;width:400px;">Tên</th>
                    <th style="font-weight: bold;width:400px;">Nhóm đang tham gia</th>
                    <th style="font-weight: bold;width:400px;">Số dư</th>
                    <th style="font-weight: bold;width:400px;">Ngày tạo</th>
                    <th style="font-weight: bold;width:200px;text-align: center">Hành động</th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($collaborators as $collaborator)
                    <tr role="row">
                      <td>{{$collaborator->id}}</td>
                      <td>{{$collaborator->icheck_id}}</td>
                      <td>{{$collaborator->name}}</td>
                      <td>{{$collaborator->group_name}}</td>
                      <td>
                        @if($collaborator->balance)
                          {{$collaborator->balance}} đ
                        @else
                          0 đ
                        @endif
                      </td>
                      <td>{{$collaborator->created_at}}</td>
                      <td style="text-align: center;">
                        @can("COLLABORATOR-updateCollaborator")
                        <a href="{{route("Collaborator::collaborator_list@edit",[$collaborator->id])}}" title="Sửa"
                           class="btn btn-info m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                          <i class="la la-edit"></i>
                        </a>
                        @endcan
                        {{--<a href="{{route("Collaborator::collaborator_group@delete",[$group->id])}}"--}}
                        {{--onclick="return delItem()" title="Xóa"--}}
                        {{--class="btn btn-danger m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">--}}
                        {{--<i class="la la-trash"></i>--}}
                        {{--</a>--}}
                      </td>
                  @endforeach
                  </tbody>
                </table>
              </div>
              <!--end::Table-->
              <div style="float:right;"><?php echo $collaborators->links(); ?></div>
            </div>
          </div>
        </div>
          @endcan
      </div>
    </div>
  </div>
  <router-view></router-view>
@endsection
@push('scripts')
  <script type="text/javascript" src="{{ asset('qr_code/metronic/js/vendors.bundle.js') }}"></script>
  <script>
    $(document).ready(function () {
      // Basic
      $("#collaborator_id").select2({
        placeholder: 'Chọn CTV',
        allowClear: true
      });

      $("select").on("select2:select", function (evt) {
        var element = evt.params.data.element;
        var $element = $(element);
        $element.detach();
        $(this).append($element);
        $(this).trigger("change");
      });
    });

    function delItem() {
      let conf = confirm("Bạn chắc chắn muốn xoá không?");
      return conf;
    }
  </script>
@endpush
