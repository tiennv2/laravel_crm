@extends('qrcode.layouts.app')
@push('styles')
  <style>
    .help-block {
      color: red;
    }
  </style>

@endpush
@section('body_class', 'm-page--fluid m-page--loading-enabled m-page--loading m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default')
@push('scripts')
  <script type="text/javascript">
    <?php
      $sharedData = [
        'config' => [
          'csrfToken' => csrf_token(),
          'logoutUrl' => route('logout'),
          'isNative' => true,
        ],
        'loggedInAccount' => auth()->check() ? auth()->user() : null,
      ];
      ?>
      window.SHARED_DATA = {!! json_encode($sharedData) !!}
  </script>
  <script src="https://cdn.jsdelivr.net/npm/tinymce@4.7.11/tinymce.min.js" type="text/javascript"></script>
  <script src="{{ mix('js/app.js') }}"></script>
@endpush

@section('content')
  <!-- begin::Page loader -->
  <div class="m-page-loader m-page-loader--base">
    <div class="m-blockui">
      <span>
        Please wait...
      </span>
      <span>
        <div class="m-loader m-loader--brand"></div>
      </span>
    </div>
  </div>
  <!-- end::Page Loader -->
  <div id="app" class="m-grid m-grid--hor m-grid--root m-page">
    <app-header></app-header>
    <div
      class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop 	m-container m-container--responsive m-container--xxl m-page__container m-body">
      <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-content">
          <div class="m-portlet">
            <div class="m-portlet__head">
              <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                  <h3 class="m-portlet__head-text">
                    <a href="{{route('Collaborator::collaborator_list@index')}}">
                      <i class="la la-arrow-left"></i> Quay lại
                    </a>
                  </h3>
                </div>
              </div>
            </div>
            <!-- /page header -->
            @if (session('success'))
              <div
                class="m-alert m-alert--outline m-alert--square m-alert--outline-2x alert alert-success alert-dismissible fade show"
                role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                </button>
                {{ session('success') }}
              </div>
            @endif
            <form method="POST" enctype="multipart/form-data" name="form"
                  class="m-form m-form--fit m-form--label-align-right"
                  action="{{ isset($collaborator) ? route('Collaborator::collaborator_list@update', [$collaborator->id] ): route('Collaborator::collaborator_list@store') }}">
              {{ csrf_field() }}
              @if (isset($collaborator))
                <input type="hidden" name="_method" value="PUT">
              @endif
              <div class="m-portlet__body">
                <!------------------ Collaborator--------------->
                <div class="form-group m-form__group row">
                  <label class="col-form-label col-lg-3 col-sm-12">Chọn Cộng tác viên</label>
                  <div class="col-lg-6 col-md-9 col-sm-12">
                    <select class="form-control m-input" data-col-index="7" id="icheck_id"
                            name="icheck_id" required @if (isset($collaborator)) disabled @endif>
                      <option></option>
                      @foreach($icheckUsers as $user)
                        <option value="{{$user->icheck_id}}"
                                @if( old('icheck_id') == $user->icheck_id) selected="selected" @endif
                          {{(isset($collaborator) and $user->icheck_id == $collaborator->icheck_id )?'selected="selected"' :''}}
                        >{{$user->name}} ({{$user->icheck_id}})
                        </option>
                      @endforeach
                      required>
                    </select>
                  </div>
                  @if ($errors->has('icheck_id'))
                    <div class="help-block">{{ $errors->first('icheck_id') }}</div>
                  @endif
                </div>
                <!------------------ Group--------------->
                <div class="form-group m-form__group row">
                  <label class="col-form-label col-lg-3 col-sm-12">Chọn nhóm cộng tác viên</label>
                  <div class="col-lg-6 col-md-9 col-sm-12">
                    <select class="form-control m-input" data-col-index="7" id="group_ids"
                            name="group_ids[]" multiple>
                      <option></option>
                      @foreach($groups as $group)
                        <option value="{{$group->id}}"
                                @if( old('group_id') == $group->id) selected="selected" @endif
                          {{(isset($collaborator) and in_array($group->id, $collaborator->group_ids))?'selected="selected"' :''}}
                        >{{$group->name}}</option>
                      @endforeach>
                    </select>
                  </div>
                  @if ($errors->has('group_id'))
                    <div class="help-block">{{ $errors->first('group_id') }}</div>
                  @endif
                </div>
                <!------------------ Email--------------->
                <div class="form-group m-form__group row">
                  <label class="col-form-label col-lg-3 col-sm-12">Email *</label>
                  <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="email" name="email" class="form-control"
                           value="{{ old('email') ?: @$collaborator->email }}" required/>
                  </div>
                  @if ($errors->has('email'))
                    <div class="help-block">{{ $errors->first('email') }}</div>
                  @endif
                </div>
                <div class="m-portlet__foot m-portlet__foot--fit">
                  <div class="m-form__actions m-form__actions">
                    <div class="row">
                      <div class="col-lg-9 ml-lg-auto">
                        <button type="submit" class="btn btn-success">
                          {{ isset($collaborator) ? 'Cập nhật' : 'Thêm mới' }}
                        </button>
                        <button type="reset" class="btn btn-secondary">
                          {{ isset($collaborator) ? 'Hủy bỏ' : 'Nhập lại' }}
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('scripts')
  <script type="text/javascript" src="{{ asset('qr_code/metronic/js/vendors.bundle.js') }}"></script>
  <script type="text/javascript">
    $(document).ready(function () {
      // Basic
      $("#group_ids").select2({
        placeholder: 'Chọn nhóm tác viên',
        allowClear: true
      });

      $("#icheck_id").select2({
        placeholder: 'Chọn cộng tác viên',
        allowClear: true
      });
      $("select").on("select2:select", function (evt) {
        var element = evt.params.data.element;
        var $element = $(element);
        $element.detach();
        $(this).append($element);
        $(this).trigger("change");
      });
    });
  </script>

@endpush
