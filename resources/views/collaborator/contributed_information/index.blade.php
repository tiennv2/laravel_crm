@extends('qrcode.layouts.app')

@section('body_class', 'm-page--fluid m-page--loading-enabled m-page--loading m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default')
@push('styles')
  <style>
    td, th {
      text-align: center;
    }

    p {
      font-size: 12px;
      text-align: left;
    }

    img {
      cursor: zoom-in;
      width: 200px;
      max-width: 100%;
    }

    /*.selectBox {*/
    /*position: relative;*/
    /*}*/

    .selectBox select {
      width: 100%;
      font-weight: bold;
    }

    .overSelect {
      position: absolute;
      left: 0;
      right: 0;
      top: 0;
      bottom: 0;
    }

    #checkboxes {
      display: none;
      width: 200px;
      /*border: 1px #dadada solid;*/
      position: absolute;
      z-index: 10;
      background-color: whitesmoke;
    }

    #checkboxes label {
      display: block;
    }

    .select2 {
      width: 100% !important;
    }
  </style>
@endpush
@push('scripts')
  <script type="text/javascript">
    <?php
      $sharedData = [
        'config' => [
          'csrfToken' => csrf_token(),
          'logoutUrl' => route('logout'),
          'isNative' => true,
        ],
        'loggedInAccount' => auth()->check() ? auth()->user() : null,
      ];
      ?>
      window.SHARED_DATA = {!! json_encode($sharedData) !!}
  </script>
  <script src="https://cdn.jsdelivr.net/npm/tinymce@4.7.11/tinymce.min.js" type="text/javascript"></script>
  <script src="{{ mix('js/app.js') }}"></script>
@endpush

@section('content')
  <!-- begin::Page loader -->
  <div class="m-page-loader m-page-loader--base">
    <div class="m-blockui">
      <span>
        Please wait...
      </span>
      <span>
        <div class="m-loader m-loader--brand"></div>
      </span>
    </div>
  </div>
  <!-- end::Page Loader -->
  <div id="app" class="m-grid m-grid--hor m-grid--root m-page">
    <app-header></app-header>
    <div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop  m-body m-page__container">
      <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader ">
          <div class="d-flex align-items-center">
            <div class="mr-auto">
              <h3 class="m-subheader__title ">
                QUẢN LÝ THÔNG TIN SẢN PHẨM ĐÓNG GÓP
              </h3>
            </div>
            @can("COLLABORATOR-viewContributedInfo")
              @if($waiting_product_count > 0)
                <div style="border: solid #bf800c;padding: 3px"><a
                    href="{{route('Collaborator::contributed_information@index')}}?status=0" style="color: #bf800c">Có
                    <span id="waiting_count">{{$waiting_product_count}}</span> sản phẩm đang chờ
                    duyệt</a></div>
              @endif
            @endcan
          </div>
        </div>
        <!-- END: Subheader -->
        @can("COLLABORATOR-viewContributedInfo")
          <div class="m-content">
            <!--Begin::Section-->
            <div class="m-portlet m-portlet--mobile">
              <div class="m-portlet__body">
                <form class="m-form m--margin-bottom-20"
                      action="{{route('Collaborator::contributed_information@index')}}" method="get">
                  <div class="row m--margin-bottom-20">
                    <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile">
                      <label>
                        Mã GTIN:
                      </label>
                      <input type="text" class="form-control m-input" name="gtin"
                             value="{{ Request::input('gtin') }}"
                             data-col-index="4">
                    </div>
                    <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile">
                      <label>
                        CTV đóng góp:
                      </label>
                      <select class="form-control m-input" data-col-index="7" id="collaborator_id"
                              name="collaborator_id">
                        <option value="" selected="selected"></option>
                        @foreach($collaborators as $collaborator)
                          <option value="{{$collaborator->id}}"
                            {{Request::input('collaborator_id')==$collaborator->id?'selected="selected"':''}}
                          >{{$collaborator->name}}</option>
                        @endforeach
                      </select>
                    </div>
                    <div class="col-lg-4 m--margin-bottom-10-tablet-and-mobile">
                      <label>
                        Ngày đóng góp:
                      </label>
                      <div class="input-daterange input-group">
                        <input type="text" class="form-control" name="contributed_from"
                               id="m_datepicker_1"
                               value="{{ Request::input('contributed_from') }}" autocomplete="off"
                               placeholder="Từ"
                               data-col-index="5"/>
                        <div class="input-group-append">
              <span class="input-group-text">
              <i class="la la-ellipsis-h"></i>
              </span>
                        </div>
                        <input type="text" class="form-control" name="contributed_to"
                               id="m_datepicker_1"
                               value="{{ Request::input('contributed_to') }}" autocomplete="off"
                               placeholder="Đến"
                               data-col-index="5"/>
                      </div>
                    </div>
                    <div class="col-lg-4 m--margin-bottom-10-tablet-and-mobile">
                      <label>
                        Ngày duyệt:
                      </label>
                      <div class="input-daterange input-group">
                        <input type="text" class="form-control" name="approved_from"
                               id="m_datepicker_1"
                               value="{{ Request::input('approved_from') }}" autocomplete="off"
                               placeholder="Từ"
                               data-col-index="5"/>
                        <div class="input-group-append">
              <span class="input-group-text">
              <i class="la la-ellipsis-h"></i>
              </span>
                        </div>
                        <input type="text" class="form-control" name="approved_to"
                               id="m_datepicker_1"
                               value="{{ Request::input('approved_to') }}" autocomplete="off"
                               placeholder="Đến"
                               data-col-index="5"/>
                      </div>
                    </div>

                  </div>

                  <div class="row m--margin-bottom-20">
                    <div class="col-lg-4 m--margin-bottom-10-tablet-and-mobile">
                      <label>
                        Thông tin đóng góp:
                      </label>
                      <select class="form-control m-input" data-col-index="7"
                              id="contributed_informations"
                              name="contributed_informations[]" multiple>
                        <option></option>
                        <option
                          value="{{\App\Models\Collaborator\Need_Update_Product::GLN_INFO}}" {{(Request::input('contributed_informations') && in_array(\App\Models\Collaborator\Need_Update_Product::GLN_INFO,Request::input('contributed_informations')))?'selected="selected"':''}}>
                          Nhà sản xuất
                        </option>
                        <option
                          value="{{\App\Models\Collaborator\Need_Update_Product::NAME}}" {{(Request::input('contributed_informations') && in_array(\App\Models\Collaborator\Need_Update_Product::NAME,Request::input('contributed_informations')))?'selected="selected"':''}}>
                          Tên sản phẩm
                        </option>
                        <option
                          value="{{\App\Models\Collaborator\Need_Update_Product::IMAGES}}" {{(Request::input('contributed_informations') && in_array(\App\Models\Collaborator\Need_Update_Product::IMAGES,Request::input('contributed_informations')))?'selected="selected"':''}}>
                          Ảnh sản phẩm
                        </option>
                        <option
                          value="{{\App\Models\Collaborator\Need_Update_Product::DESCRIPTIONS}}" {{(Request::input('contributed_informations') && in_array(\App\Models\Collaborator\Need_Update_Product::DESCRIPTIONS,Request::input('contributed_informations')))?'selected="selected"':''}}>
                          Mô tả sản phẩm
                        </option>
                        <option
                          value="{{\App\Models\Collaborator\Need_Update_Product::CATEGORIES_INFO}}" {{(Request::input('contributed_informations') && in_array(\App\Models\Collaborator\Need_Update_Product::CATEGORIES_INFO,Request::input('contributed_informations')))?'selected="selected"':''}}>
                          Danh mục sản phẩm
                        </option>
                      </select>
                    </div>
                    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                      <label>
                        Trạng thái:
                      </label>
                      <select class="form-control m-input" data-col-index="7" id="status"
                              name="status">
                        <option></option>
                        <option
                          value="{{\App\Models\Collaborator\Contributed_Information::STATUS_PENDING}}" {{ Request::input('status') === "0"?'selected="selected"':''}}>
                          Chờ duyệt
                        </option>
                        <option
                          value="{{\App\Models\Collaborator\Contributed_Information::STATUS_APPROVED}}" {{ Request::input('status') == \App\Models\Collaborator\Contributed_Information::STATUS_APPROVED?'selected="selected"':''}}>
                          Đã duyệt
                        </option>
                        <option
                          value="{{\App\Models\Collaborator\Contributed_Information::STATUS_DISAPPROVED}}" {{ Request::input('status') == \App\Models\Collaborator\Contributed_Information::STATUS_DISAPPROVED?'selected="selected"':''}}>
                          Hủy duyệt
                        </option>
                      </select>
                    </div>
                    <div class="col-lg-4 m--margin-bottom-10-tablet-and-mobile">
                      <label style="color: white;">
                        Trạng thái:
                      </label>
                      <div>
                        <button class="btn btn-brand m-btn m-btn--icon" type="submit"><span><i
                              class="la la-search"></i><span>Tìm kiếm</span></span>
                        </button>
                        &nbsp;&nbsp;
                        <button class="btn btn-secondary m-btn m-btn--icon" type="button"
                                onclick="window.location='{{route('Collaborator::contributed_information@index')}}'">
                          <span><i class="la la-refresh"></i><span>Làm mới</span></span></button>
                      </div>
                    </div>
                  </div>
                </form>
                @if (session('success'))
                  <div
                    class="m-alert m-alert--outline m-alert--square m-alert--outline-2x alert alert-success alert-dismissible fade show"
                    role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    </button>
                    {{ session('success') }}
                  </div>
                @endif
                <div class="row m--margin-bottom-20">
                  <div class="col-lg-1 m--margin-bottom-10-tablet-and-mobile"
                       style="margin-bottom: 10px;margin-right: 20px;margin-top: 50px;">
                    <div class="selectBox" onclick="showCheckboxes()">
                      <select class="form-control m-input">
                        <option>Ẩn cột</option>
                      </select>
                      <div class="overSelect" id="overSelect"></div>
                    </div>
                    <div id="checkboxes">
                      <label class="m-checkbox" for="name">
                        <input class="col-check" type="checkbox" id="name"/>Tên sản phẩm<span></span></label>
                      <label class="m-checkbox" for="category">
                        <input class="col-check" type="checkbox" id="category"/>Danh mục sản phẩm<span></span></label>
                      <label class="m-checkbox" for="price">
                        <input class="col-check" type="checkbox" id="price"/>Giá sản phẩm<span></span></label>
                      <label class="m-checkbox" for="image">
                        <input class="col-check" type="checkbox" id="image"/>Ảnh sản phẩm<span></span></label>
                      <label class="m-checkbox" for="description">
                        <input class="col-check" type="checkbox" id="description"/>Mô tả sản phẩm<span></span></label>
                      <label class="m-checkbox" for="gln_info">
                        <input class="col-check" type="checkbox" id="gln_info"/>Thông tin NSX<span></span></label>
                      <label class="m-checkbox" for="info_type">
                        <input class="col-check" type="checkbox" id="info_type"/>Loại thông tin đóng
                        góp<span></span></label>
                      <label class="m-checkbox" for="contributor_name">
                        <input class="col-check" type="checkbox" id="contributor_name"/>Cộng tác viên đóng
                        góp<span></span></label>
                      <label class="m-checkbox" for="contribution_day">
                        <input class="col-check" type="checkbox" id="contribution_day"/>Ngày đóng
                        góp<span></span></label>
                      <label class="m-checkbox" for="approvement_day">
                        <input class="col-check" type="checkbox" id="approvement_day"/>Ngày duyệt<span></span></label>
                      <label class="m-checkbox" for="profit">
                        <input class="col-check" type="checkbox" id="profit"/>Lợi nhuận<span></span></label>
                      <label class="m-checkbox" for="condition">
                        <input class="col-check" type="checkbox" id="condition"/>Trạng thái<span></span></label>
                      <label class="m-checkbox" for="action">
                        <input class="col-check" type="checkbox" id="action"/>Hành động<span></span></label>
                    </div>
                  </div>
                  <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile"
                       style="width:200px;text-align:center;padding:5px;margin-bottom: 10px;margin-right: 20px;margin-top: 50px;font-weight: bold;font-style: italic;border: solid 1px;border-radius: 6px">
                    Đang hiển thị {{$count_record_in_page}}/{{$count_record_total}} bản ghi
                  </div>
                  <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile"
                       style="width:200px;text-align:center;padding:5px;margin-bottom: 10px;margin-top: 50px;font-weight: bold;font-style: italic;border: solid 1px;border-radius: 6px">
                    Số tiền đóng góp đã duyệt: <span
                      id="approved_amount_total">{{number_format($approved_amount_total)}}</span><span> đ</span>
                  </div>

                </div>
                <!--begin::Table-->
                <div class="table-responsive">
                  <table class="table table-striped- table-bordered table-hover table-checkable" id="myTable">
                    <thead>
                    <tr>
                      <th style="font-weight: bold;min-width:50px;">Id</th>
                      <th style="font-weight: bold;min-width:150px;">Mã GTIN</th>
                      <th style="font-weight: bold;min-width:220px;" class="name">Tên sản phẩm</th>
                      <th style="font-weight: bold;min-width:220px;" class="category">Danh mục sản phẩm</th>
                      <th style="font-weight: bold;min-width:220px;" class="price">Giá sản phẩm</th>
                      <th style="font-weight: bold;min-width:220px;" class="image">Ảnh sản phẩm</th>
                      <th style="font-weight: bold;min-width:220px;" class="description">Mô tả sản phẩm</th>
                      <th style="font-weight: bold;min-width:220px;" class="gln_info">Thông tin Nhà Sản xuất</th>
                      <th style="font-weight: bold;min-width:300px;" class="info_type">Loại thông tin đóng góp</th>
                      <th style="font-weight: bold;min-width:200px;" class="contributor_name">Cộng tác viên đóng góp
                      </th>
                      <th style="font-weight: bold;min-width:150px;" class="contribution_day">Ngày đóng góp</th>
                      <th style="font-weight: bold;min-width:150px;" class="approvement_day">Ngày duyệt</th>
                      <th style="font-weight: bold;min-width:100px;" class="profit">Lợi nhuận</th>
                      <th style="font-weight: bold;min-width:150px;" class="condition">Trạng thái</th>
                      <th style="font-weight: bold;min-width:150px;" class="action">Hành động</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($contributed_informations as $contributed_information)
                      <tr role="row">
                        <td class="id" data-toggle="collapse" style="cursor: pointer"
                            href="#{{$contributed_information->id}}">{{$contributed_information->id}}</td>
                        <td>{{$contributed_information->gtin}}</td>
                        <td class="name">
                          @if(array_key_exists("name",$contributed_information->contributed_data))
                            {{$contributed_information->contributed_data['name']}}
                          @endif
                        </td>
                        <td class="category">
                          @if(array_key_exists("categories",$contributed_information->contributed_data))
                            {{$contributed_information->contributed_data['categories']}}
                          @endif
                        </td>
                        <td class="price">
                          @if(array_key_exists("price",$contributed_information->contributed_data))
                            {{number_format(intval($contributed_information->contributed_data['price']))}} đ
                          @endif
                        </td>
                        <td class="image">
                          @if(array_key_exists("images",$contributed_information->contributed_data))
                            @foreach($contributed_information->contributed_data['images'] as $value)
                              <img src="{{$value}}" width="150px"
                                   style="border: 1px solid;margin: 2px"><br/>
                            @endforeach
                          @endif
                        </td>
                        <td class="description">
                          @if(array_key_exists("description",$contributed_information->contributed_data))
                            {!! $contributed_information->contributed_data['description'] !!}
                          @endif
                        </td>
                        <td class="gln_info">
                          @if(array_key_exists("glnInfo",$contributed_information->contributed_data))
                            @foreach($contributed_information->contributed_data['glnInfo'] as $key_2 => $value_2)
                              @if($key_2 == "gln")
                                <p>
                                  <span style="font-weight: bold">* Mã GLN:</span> {{$value_2}}
                                </p>
                              @endif
                              @if($key_2 == "businessPrefix")
                                <p>
                                  <span style="font-weight: bold">* Mã Doanh nghiệp:</span> {{$value_2}}
                                </p>
                              @endif
                              @if($key_2 == "name")
                                <p>
                                  <span style="font-weight: bold">* Tên Nhà sản xuất:</span> {{$value_2}}
                                </p>
                              @endif
                              @if($key_2 == "email")
                                <p>
                                  <span style="font-weight: bold">* Email:</span> {{$value_2}}
                                </p>
                              @endif
                              @if($key_2 == "website")
                                <p>
                                  <span style="font-weight: bold">* Website:</span> {{$value_2}}
                                </p>
                              @endif
                              @if($key_2 == "phone")
                                <p>
                                  <span style="font-weight: bold">* Số điện thoại:</span> {{$value_2}}
                                </p>
                              @endif
                              @if($key_2 == "address")
                                <p>
                                  <span style="font-weight: bold">* Địa chỉ:</span> {{$value_2}}
                                </p>
                              @endif
                              @if($key_2 == "country")
                                <p>
                                  <span style="font-weight: bold">* Quốc gia:</span> {{$value_2}}
                                </p>
                              @endif
                            @endforeach
                          @endif
                        </td>
                        <td class="info_type">{{$contributed_information->contributed_informations_name}}</td>
                        <td class="contributor_name">{{$contributed_information->contributor_name}}</td>
                        <td class="contribution_day">{{$contributed_information->contributed_at}}</td>
                        <td class="approvement_day">{{$contributed_information->approved_at}}</td>
                        <td class="profit"><span class="profits">{{$contributed_information->profits}}</span> đ</td>
                        <td class="condition">
                          @if($contributed_information->status==\App\Models\Collaborator\Contributed_Information::STATUS_APPROVED)
                            <span class="m-badge m-badge--success m-badge--wide">
									Đã duyệt </span>
                          @elseif($contributed_information->status==\App\Models\Collaborator\Contributed_Information::STATUS_DISAPPROVED)
                            <span class="m-badge m-badge--danger m-badge--wide">
									Hủy duyệt </span>
                          @elseif($contributed_information->status==\App\Models\Collaborator\Contributed_Information::STATUS_PENDING)
                            <span class="status m-badge m-badge--warning m-badge--wide">
									Chờ duyệt </span>
                          @endif
                        </td>
                        <td style="text-align: center" class="action">
                          {{--@if($contributed_information->status == \App\Models\Collaborator\Contributed_Information::STATUS_PENDING)--}}
                          {{--<a--}}
                          {{--href="{{route("Collaborator::contributed_information@approve",[$contributed_information->id])}}"--}}
                          {{--title="Duyệt"--}}
                          {{--class="btn btn-success m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">--}}
                          {{--<i class="la la-check"></i>--}}
                          {{--</a>--}}
                          {{--@endif--}}
                          {{--@if($contributed_information->status == \App\Models\Collaborator\Contributed_Information::STATUS_PENDING)--}}
                          {{--<a--}}
                          {{--href="{{route("Collaborator::contributed_information@disapprove",[$contributed_information->id])}}"--}}
                          {{--title="Bỏ duyệt"--}}
                          {{--class="btn btn-danger m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">--}}
                          {{--<i class="la la-close"></i>--}}
                          {{--</a>--}}
                          {{--@endif--}}
                          @can("COLLABORATOR-approveContributedInfo")
                            @if($contributed_information->status == \App\Models\Collaborator\Contributed_Information::STATUS_PENDING)
                              <button
                                type="button"
                                title="Duyệt"
                                class="approve btn btn-success m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                <i class="la la-check"></i>
                              </button>
                            @endif
                          @endcan
                          @can("COLLABORATOR-disapproveContributedInfo")
                            @if($contributed_information->status == \App\Models\Collaborator\Contributed_Information::STATUS_PENDING)
                              <button
                                type="button"
                                title="Bỏ duyệt"
                                class="disapprove btn btn-danger m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                <i class="la la-close"></i>
                              </button>
                            @endif
                          @endcan
                        </td>
                      </tr>
                      <tr class="collapse" id="{{$contributed_information->id}}">
                        <td style="font-weight: bold;font-style: italic">Dữ liệu iCheck</td>
                        <td></td>
                        <td class="name">{{$contributed_information->icheck_info['product_name']}}</td>
                        <td class="category"></td>
                        <td class="price">{{number_format($contributed_information->icheck_info['price'])}} đ</td>
                        <td class="image">
                          @if($contributed_information->icheck_info['image'])
                            <img
                              src="https://static.icheck.com.vn/{{$contributed_information->icheck_info['image']}}_medium.jpg"
                              width="150px" style="border: 1px solid;margin: 2px"><br/>
                          @endif
                        </td>
                        <td class="description">{!! $contributed_information->icheck_info['descriptions'] !!}</td>
                        <td class="gln_info">
                          @if($contributed_information->icheck_vendor)
                            @foreach($contributed_information->icheck_vendor as $key_3 => $value_3)
                              @if($key_3 == "gln")
                                <p>
                                  <span style="font-weight: bold">* Mã GLN:</span> {{$value_3}}
                                </p>
                              @endif
                              @if($key_3 == "businessPrefix")
                                <p>
                                  <span style="font-weight: bold">* Mã Doanh nghiệp:</span> {{$value_3}}
                                </p>
                              @endif
                              @if($key_3 == "name")
                                <p>
                                  <span style="font-weight: bold">* Tên Nhà sản xuất:</span> {{$value_3}}
                                </p>
                              @endif
                              @if($key_3 == "email")
                                <p>
                                  <span style="font-weight: bold">* Email:</span> {{$value_3}}
                                </p>
                              @endif
                              @if($key_3 == "website")
                                <p>
                                  <span style="font-weight: bold">* Website:</span> {{$value_3}}
                                </p>
                              @endif
                              @if($key_3 == "phone")
                                <p>
                                  <span style="font-weight: bold">* Số điện thoại:</span> {{$value_3}}
                                </p>
                              @endif
                              @if($key_3 == "address")
                                <p>
                                  <span style="font-weight: bold">* Địa chỉ:</span> {{$value_3}}
                                </p>
                              @endif
                              @if($key_3 == "country")
                                <p>
                                  <span style="font-weight: bold">* Quốc gia:</span> {{$value_3}}
                                </p>
                              @endif
                            @endforeach
                          @endif
                        </td>
                        <td class="info_type"></td>
                        <td class="contributor_name"></td>
                        <td class="contribution_day"></td>
                        <td class="approvement_day"></td>
                        <td class="profit"></td>
                        <td class="condition"></td>
                        <td class="action"></td>
                      </tr>
                    @endforeach
                    </tbody>
                  </table>
                </div>
                <div class="modal fade" id="enlargeImageModal" tabindex="-1" role="dialog"
                     aria-labelledby="enlargeImageModal" aria-hidden="true">
                  <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">×</span></button>
                      </div>
                      <div class="modal-body">
                        <img src="" class="enlargeImageModalSource" style="width: 100%;">
                      </div>
                    </div>
                  </div>
                </div>
                <!--end::Table-->
                <div style="float:right;"><?php echo $contributed_informations->links(); ?></div>
              </div>
            </div>
          </div>
        @endcan
      </div>
    </div>
  </div>
  <router-view></router-view>
@endsection
@push('scripts')
  <script type="text/javascript" src="{{ asset('qr_code/metronic/js/vendors.bundle.js') }}"></script>
  <script type="text/javascript" src="{{ asset('qr_code/metronic/js/datepicker/bootstrap-datepicker.js') }}"></script>
  <script>
    $(document).ready(function () {
      // Basic
      $("#contributed_informations").select2({
        placeholder: 'Chọn loại thông tin',
        allowClear: true
      });
      $("#status").select2({
        placeholder: 'Trạng thái',
        allowClear: true
      });
      $("#collaborator_id").select2({
        placeholder: 'Chọn CTV',
        allowClear: true
      });

      $("select").on("select2:select", function (evt) {
        var element = evt.params.data.element;
        var $element = $(element);
        $element.detach();
        $(this).append($element);
        $(this).trigger("change");
      });

      $(".modal").on('hidden.bs.modal', function () {
        document.body.style.padding = 0;
      });
    });
    $(function () {
      $('img').on('click', function () {
        $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
        $('#enlargeImageModal').modal('show');
      });
    });

    //Approve product
    $(".approve").click(function () {
      var approve_button = $(this);
      var disapprove_button = $(this).siblings(".disapprove");
      approve_button.prop('disabled', true);
      disapprove_button.prop('disabled', true);
      var status = approve_button.closest("tr").find(".status"); // Find the text
      status.html('Đang xử lý');
      status.addClass("m-loader m-loader--primary m-loader--left");
      status.css('padding-left', '32px');
      var id = approve_button.closest("tr").find(".id").text();
      var approve_url = "{{ route('Collaborator::contributed_information@approve',':id')}}";
      approve_url = approve_url.replace(':id', id);
      $.ajax({
        url: approve_url,
        method: 'GET',

      });

      function checkStatus(id) {
        var check_url = "{{ route('Collaborator::contributed_information@checkStatus',':id')}}";
        check_url = check_url.replace(':id', id);
        var checkResult = null;
        $.ajax({
          url: check_url,
          method: 'GET',
          async: false,
          success: function (data) {
            checkResult = parseInt(data);
          }
        });
        return checkResult;
      }

      var interval_obj = setInterval(function () {
        let checkResult = checkStatus(id);
        if (checkResult === 1) {
          clearInterval(interval_obj);
          status.removeClass("m-badge m-badge--warning m-badge--wide m-loader m-loader--primary m-loader--left");
          status.addClass("status m-badge m-badge--success m-badge--wide");
          status.css('padding-left', '10px');
          status.html('Đã duyệt');
          approve_button.hide();
          disapprove_button.hide();
          let current_count = parseInt($("#waiting_count").text());
          $("#waiting_count").html(current_count - 1);

          let profit = parseInt(approve_button.closest("tr").find(".profits").text()); // Find the profit
          $current_approved_amount_total = parseInt($("#approved_amount_total").text());
          $("#approved_amount_total").html($current_approved_amount_total + profit);
        } else if (checkResult === 2) {
          clearInterval(interval_obj);
          status.removeClass("m-badge m-badge--warning m-badge--wide m-loader m-loader--primary m-loader--left");
          status.addClass("status m-badge m-badge--danger m-badge--wide");
          status.css('padding-left', '10px');
          status.html('Hủy duyệt');
          approve_button.hide();
          disapprove_button.hide();
        }
      }, 2000);
    });
    //Disapprove product
    $(".disapprove").click(function () {
      var disapprove_button = $(this);
      var approve_button = $(this).siblings(".approve");
      approve_button.prop('disabled', true);
      disapprove_button.prop('disabled', true);
      var status = disapprove_button.closest("tr").find(".status"); // Find the text
      status.html('Đang xử lý');
      status.addClass("m-loader m-loader--primary m-loader--left");
      status.css('padding-left', '32px');
      var id = approve_button.closest("tr").find(".id").text();
      var approve_url = "{{ route('Collaborator::contributed_information@disapprove',':id')}}";
      approve_url = approve_url.replace(':id', id);
      $.ajax({
        url: approve_url,
        method: 'GET',
      });

      function checkStatus(id) {
        var check_url = "{{ route('Collaborator::contributed_information@checkStatus',':id')}}";
        check_url = check_url.replace(':id', id);
        var checkResult = null;
        $.ajax({
          url: check_url,
          method: 'GET',
          async: false,
          success: function (data) {
            checkResult = parseInt(data);
          }
        });
        return checkResult;
      }

      var interval_obj = setInterval(function () {
        let checkResult = checkStatus(id);
        if (checkResult === 2) {
          clearInterval(interval_obj);
          status.removeClass("m-badge m-badge--warning m-badge--wide m-loader m-loader--primary m-loader--left");
          status.addClass("status m-badge m-badge--danger m-badge--wide");
          status.css('padding-left', '10px');
          status.html('Hủy duyệt');
          disapprove_button.hide();
          approve_button.hide();
          let current_count = parseInt($("#waiting_count").text());
          $("#waiting_count").html(current_count - 1);
        }
      }, 1000);
    });

    //
    $(".id").click(function () {
      var row = $(this).closest("tr");
      var id = $(this).text();
      row.siblings("tr").css("background-color", "");
      $(".collapse").css("background-color", "");
      row.css("background-color", "#D8D8D8");
      $(`#${id}`).css("background-color", "#D8D8D8");
    });


    var expanded = false;

    function showCheckboxes() {
      if (!expanded) {
        $("#checkboxes").css("display", "block");
        expanded = true;
      } else {
        $("#checkboxes").css("display", "none");
        expanded = false;
      }
    }

    $(document).click(function (e) {
      if ($(e.target).parents("#checkboxes").length === 0 && e.target.id !== 'overSelect') {
        $("#checkboxes").css("display", "none");
        expanded = false;
      }
    });
  </script>

  <script>
    //Hide column Options
    $(function () {
      var hiddenCols = JSON.parse(localStorage.getItem('hidden-cols') || '[]');
      var checkBoxes = $('.col-check');
      $rows = $('#myTable tr');

      $.each(hiddenCols, function (i, col) {
        checkBoxes.filter('[id=' + col + ']').prop('checked', true);
        $rows.find('.' + col).hide();
      });


      checkBoxes.change(function () {
        let name = $(this).attr('id');
        $(`table .${name}`).toggle(!this.checked);
        hiddenCols = checkBoxes.filter(':checked').map(function () {
          return $(this).attr('id');
        }).get();
        logHiddenCols();
        localStorage.setItem('hidden-cols', JSON.stringify(hiddenCols))
      });

      logHiddenCols();

      // demo helper function
      function logHiddenCols() {
        console.log(hiddenCols)
      }
    });
  </script>
@endpush
