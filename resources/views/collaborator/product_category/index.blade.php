@extends('qrcode.layouts.app')
@section('body_class', 'm-page--fluid m-page--loading-enabled m-page--loading m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default')

@push('scripts')
  <script type="text/javascript">
    <?php
      $sharedData = [
        'config' => [
          'csrfToken' => csrf_token(),
          'logoutUrl' => route('logout'),
          'isNative' => true,
        ],
        'loggedInAccount' => auth()->check() ? auth()->user() : null,
      ];
      ?>
      window.SHARED_DATA = {!! json_encode($sharedData) !!}
  </script>
  <script src="https://cdn.jsdelivr.net/npm/tinymce@4.7.11/tinymce.min.js" type="text/javascript"></script>
  <script src="{{ mix('js/app.js') }}"></script>
@endpush

@section('content')
  <!-- begin::Page loader -->
  <div class="m-page-loader m-page-loader--base">
    <div class="m-blockui">
      <span>
        Please wait...
      </span>
      <span>
        <div class="m-loader m-loader--brand"></div>
      </span>
    </div>
  </div>
  <!-- end::page Loader -->
  <div id="app" class="m-grid m-grid--hor m-grid--root m-page">
    <app-header></app-header>
    <div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop  m-body m-page__container">
      <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader">
          <div class="d-flex align-items-center">
            <div class="mr-auto">
              <h3 class="m-subheader__title">
                QUẢN LÝ DANH MỤC SẢN PHẨM
              </h3>
            </div>
            @can("COLLABORATOR-addProductCategory")
              <div style="float: right"><a href="{{route('Collaborator::product_category@add')}}"
                                           class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air"><span>
                                    <i class="la la-plus-circle"></i>
                                    <span>Thêm danh mục</span></span></a>
              </div>
            @endcan
          </div>
        </div>
        <!-- END: Subheader -->
        @can("COLLABORATOR-viewProductCategory")
          <div class="m-content">
            <!--Begin::Section-->
            <div class="m-portlet m-portlet--mobile">
              <div class="m-portlet__body">
                @if (session('success'))
                  <div
                    class="m-alert m-alert--outline m-alert--square m-alert--outline-2x alert alert-success alert-dismissible fade show"
                    role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    </button>
                    {{ session('success') }}
                  </div>
                @endif
                <div class="table-responsive">
                  <table class="table table-striped- table-bordered table-hover table-checkable"
                         style="">
                    <!--begin::Thead-->
                    <thead>
                    <tr>
                      <th style="font-weight: bold">Id</th>
                      <th style="font-weight: bold">Tên</th>
                      <th style="width: 100px;font-weight: bold">Ảnh đại diện</th>
                      <th style="font-weight: bold">Ngày tạo</th>
                      <th style="width: 150px;font-weight: bold">Hành động</th>
                    </tr>
                    </thead>
                    <!--end::Thead-->
                    <!--begin::Tbody-->
                    <tbody id="content">
                    </tbody>
                    <!--end::Tbody-->
                  </table>
                </div>
                <!--end::Table-->
              </div>
              <!--End::Section-->
            </div>
          </div>
        @endcan
      </div>
    </div>
  </div>
@endsection
@push('scripts')
  <script>
    function delCat() {
      var conf = confirm("Bạn chắc chắn muốn xoá danh mục này?");
      return conf;
    }
  </script>
  <script>
    var url = "{{ route('Collaborator::product_category@root_cat')}}";
    var edit_url = "{{ route('Collaborator::product_category@edit',":id") }}";
    var del_url = "{{ route('Collaborator::product_category@delete',":id") }}";
    $(document).ready(function () {
      getData();
    });

    function getSubCat(id, num = 1) {
      var sub_url = "{{ route('Collaborator::product_category@sub_cat',":id")}}";
      sub_url = sub_url.replace(':id', id);
      $.ajax({
        url: sub_url,
        method: 'GET',
        data: {},
        success: function (data) {
          var obj = JSON.parse(data);
          if (obj.length > 0) {
            for (i = 0; i < obj.length; i++) {
              edit_url = edit_url.replace(':id', obj[i].id);
              del_url = del_url.replace(':id', obj[i].id);
              var date = new Date(obj[i].created_at);
              var day = date.getDate();
              var month = date.getMonth() + 1;
              var year = date.getFullYear();
              date = day + "/" + month + "/" + year;
              $("#" + id).after(
                "<tr id='" + obj[i].id + "'>" +
                "<td>" + obj[i].id + "</td>" +
                "<td>" +
                "<a onclick='getSubCat(" + obj[i].id + "," + (num + 1) + "); this.onclick=null;' href= 'javascript:void(0);'>" + '- '.repeat(num) + obj[i].name + "</a>" +
                "</td>" +
                "<td>" + "<img width='100px' src='" + obj[i].image + "'/>" + "</td>" +
                "<td>" + date + "</td>" +
                "<td>" + "<a href='" + edit_url + "' class=\"btn m-btn m-btn--gradient-from-primary m-btn--gradient-to-primary\">" + 'Sửa' + "</a>" +
                "<a onclick='return delCat();' href='" + del_url + "' class=\"btn m-btn m-btn--gradient-from-danger m-btn--gradient-to-warning\">" + 'Xóa' + "</a>" +
                "</td>" +
                "</tr>"
              );
              edit_url = edit_url.replace(obj[i].id, ':id');
              del_url = del_url.replace(obj[i].id, ':id');
            }
          } else {
            alert("Không có danh mục con của danh mục này!");
          }
        }
      });

    }

    function getData() {
      $.ajax({
        url: url,
        method: 'GET',
        data: {},
        success: function (data) {
          $("#content").html('');
          var obj = JSON.parse(data);
          for (i = 0; i < obj.length; i++) {
            edit_url = edit_url.replace(':id', obj[i].id);
            del_url = del_url.replace(':id', obj[i].id);
            var date = new Date(obj[i].created_at);
            var day = date.getDate();
            var month = date.getMonth() + 1;
            var year = date.getFullYear();
            date = day + "/" + month + "/" + year;
            $("#content").append(
              "<tr id='" + obj[i].id + "'>" +
              "<td>" + obj[i].id + "</td>" +
              "<td style='font-weight:bold;font-style: italic'>" +
              "<a onclick='getSubCat(" + obj[i].id + "); this.onclick=null;' href= 'javascript:void(0);'>" + obj[i].name + "</a>" +
              "</td>" +
              "<td>" + "<img width='100px' src='" + obj[i].image + "'/>" + "</td>" +
              "<td>" + date + "</td>" +
              "<td>" + "<a href='" + edit_url + "' class=\"btn m-btn m-btn--gradient-from-primary m-btn--gradient-to-primary\">" + 'Sửa' + "</a>" +
              "<a onclick='return delCat();' href='" + del_url + "' class=\"btn m-btn m-btn--gradient-from-danger m-btn--gradient-to-warning\">" + 'Xóa' + "</a>" +
              "</td>" +
              "</tr>"
            );
            edit_url = edit_url.replace(obj[i].id, ':id');
            del_url = del_url.replace(obj[i].id, ':id');
          }
        }
      });
    }
  </script>
@endpush
