@extends('qrcode.layouts.app')
@section('body_class', 'm-page--fluid m-page--loading-enabled m-page--loading m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default')
@push('scripts')
    <script type="text/javascript">
        <?php
            $sharedData = [
                'config' => [
                    'csrfToken' => csrf_token(),
                    'logoutUrl' => route('logout'),
                    'isNative' => true,
                ],
                'loggedInAccount' => auth()->check() ? auth()->user() : null,
            ];
            ?>
            window.SHARED_DATA = {!! json_encode($sharedData) !!}
    </script>
    <script src="https://cdn.jsdelivr.net/npm/tinymce@4.7.11/tinymce.min.js" type="text/javascript"></script>
    <script src="{{ mix('js/app.js') }}"></script>
@endpush

@section('content')
    <!-- begin::Page loader -->
    <div class="m-page-loader m-page-loader--base">
        <div class="m-blockui">
      <span>
        Please wait...
      </span>
            <span>
        <div class="m-loader m-loader--brand"></div>
      </span>
        </div>
    </div>
    <!-- end::Page Loader -->
    <div id="app" class="m-grid m-grid--hor m-grid--root m-page">
        <app-header></app-header>
        <div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop 	m-container m-container--responsive m-container--xxl m-page__container m-body">
            <div class="m-grid__item m-grid__item--fluid m-wrapper">
                <div class="m-content">
                    <div class="m-portlet">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text">
                                        <a href="{{route('Collaborator::product_category@index')}}">
                                            <i class="la la-arrow-left"></i>
                                        </a>
                                        {{ isset($cat) ? 'Sửa thông tin danh mục' : 'Thêm mới danh mục' }}
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <!-- /page header -->

                        <form method="POST" enctype="multipart/form-data" name="form"
                              class="m-form m-form--fit m-form--label-align-right"
                              action="{{ isset($cat) ? route('Collaborator::product_category@update', [$cat->id] ): route('Collaborator::product_category@store') }}">
                            {{ csrf_field() }}
                            @if (isset($cat))
                                <input type="hidden" name="_method" value="PUT">
                            @endif
                            <div class="m-portlet__body">
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-3 col-sm-12"><strong>Tên danh mục *</strong></label>
                                    <div class="col-lg-4 col-md-9 col-sm-12">
                                        <input type="text" id="name" name="name"
                                               class="form-control"
                                               value="{{ old('username') ?: @$cat->name }}"
                                               required/>
                                    </div>
                                    @if ($errors->has('name'))
                                        <div class="form-control-feedback">
                                            <i class="icon-notification2"></i>
                                        </div>
                                        <div class="help-block" style="color: red">{{ $errors->first('name') }}</div>
                                    @endif
                                </div>

                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-3 col-sm-12"><strong>Chọn danh mục cha *</strong></label>
                                    <div class="col-lg-4 col-md-9 col-sm-12">
                                        <select name="parent_id" class="form-control" id="parent_id"
                                                data-placeholder="Chọn danh mục">
                                            <option value=""></option>
                                            @foreach($category_list as $row)
                                                <option value="{{$row->id}}" {{ ((old('parent_id') and old('parent_id') == $row->id) or (isset($cat) and $cat->parent_id == $row->id)) ? ' selected="selected"' : '' }}>
                                                    {{ str_repeat(' - ',$row->level)}}
                                                        {{$row->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @if ($errors->has('parent_id'))
                                        <div class="form-control-feedback">
                                            <i class="icon-notification2"></i>
                                        </div>
                                        <div class="help-block"
                                             style="color: red">{{ $errors->first('parent_id') }}</div>
                                    @endif
                                </div>

                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-3 col-sm-12" for="inputFile"><strong>Ảnh đại diện</strong></label>
                                    <div class="col-lg-5 col-md-9 col-sm-12">
                                        <input type="file" name="image" id="inputFile" value="{{ old('image') ?: "" }}">
                                        @if(isset($cat))
                                            <div>Ảnh hiện tại</div>
                                            <img width="100px"
                                                 src="{{$cat->image}}"/>
                                        @endif
                                    </div>
                                    @if ($errors->has('image'))
                                        <div class="form-control-feedback">
                                            <i class="icon-notification2"></i>
                                        </div>
                                        <div class="help-block" style="color: red">{{ $errors->first('image') }}</div>
                                    @endif
                                </div>


                                <div class="m-portlet__foot m-portlet__foot--fit">
                                    <div class="m-form__actions m-form__actions">
                                        <div class="row">
                                            <div class="col-lg-9 ml-lg-auto">
                                                <button type="submit" class="btn btn-success">
                                                    {{ isset($cat) ? 'Cập nhật' : 'Thêm mới' }}
                                                </button>
                                                <button type="reset" class="btn btn-secondary">
                                                    Nhập lại
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <router-view></router-view>
@endsection

@push('scripts')
    <script type="text/javascript" src="{{ asset('qr_code/metronic/js/vendors.bundle.js') }}"></script>
    <script type="text/javascript" src="{{ asset('qr_code/metronic/js/scripts.bundle.js') }}"></script>
    <script>
        $(document).ready(function () {
            // Basic
            $('#parent_id').select2(
                {
                    placeholder: 'Chọn danh mục',
                    allowClear: true
                }
            );

        });

    </script>

@endpush
