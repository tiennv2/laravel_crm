<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title>Thông tin đối soát cộng tác viên iCheck </title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
</head>
<body>
<p style="font-weight: bold">Dear {{$collaborator_name}}</p>
<p>iCheck gửi bạn thông tin đối soát tháng {{$reviewMonth}} năm {{$reviewYear}}:</p>
<p>Tổng sản phẩm đóng góp thông tin được duyệt: <span style="font-weight: bold">{{$contributed_total}}</span> - Tổng số tiền được hưởng: <span style="font-weight: bold">{{$profits_total}} vnđ</span></p>
<p>Cảm ơn bạn đã đóng góp thông tin cho iCheck.</p>
<p style="font-weight: bold">Cheers!</p>
</body>
<!-- END BODY -->
</html>
