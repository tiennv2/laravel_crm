<table>
  <thead>
  <tr>
    <th><h5>Mã GTIN</h5></th>
    <th>Lợi nhuận</th>
  </tr>
  </thead>
  <tbody>
  @foreach($contributed_informations as $contributed_information)
    <tr>
      <td>{{ $contributed_information->gtin }}</td>
      <td>{{ $contributed_information->profits }}</td>
    </tr>
  @endforeach
  <tr>
    <td>Tổng cộng</td>
    <td>
      {{$profits_total}}
    </td>
  </tr>
  </tbody>
</table>
