@extends('layouts.app')

@section('body_class', 'm--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default')

@section('content')
  <!-- begin:: Page -->
  <div class="m-grid m-grid--hor m-grid--root m-page">
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--signin m-login--2 m-login-2--skin-2" id="m_login" style="background-image: url({{ asset('img/login-bg.jpg') }});">
      <div class="m-grid__item m-grid__item--fluid  m-login__wrapper">
        <div class="m-login__container">
          <div class="m-login__logo">
            <a href="#">
              <img src="{{ asset('img/logo.png') }}" width="200" />
            </a>
          </div>
          <div class="m-login__signin">
            <div class="m-login__head">
              <h3 class="m-login__title">Đăng nhập vào CMS</h3>
            </div>
            <form class="m-login__form m-form" method="POST" action="{{ route('login') }}">
              @csrf
              <div class="form-group m-form__group{{ $errors->has('phone') ? ' has-danger' : '' }}">
                <input class="form-control{{ $errors->has('phone') ? ' form-control-danger' : '' }} m-input" type="text" placeholder="Số điện thoại" name="phone" autocomplete="off" value="{{ old('phone') }}" required autofocus>
                @if ($errors->has('phone'))
                  <div class="form-control-feedback">{{ $errors->first('phone') }}</div>
                @endif
              </div>
              <div class="form-group m-form__group{{ $errors->has('password') ? ' has-danger' : '' }}">
                <input class="form-control{{ $errors->has('password') ? ' form-control-danger' : '' }} m-input m-login__form-input--last" type="password" placeholder="Mật khẩu" name="password" required>
                @if ($errors->has('password'))
                  <div class="form-control-feedback">{{ $errors->first('password') }}</div>
                @endif
              </div>
              <div class="row m-login__form-sub">
                <!-- <div class="col m--align-left m-login__form-left">
                  <label class="m-checkbox  m-checkbox--focus">
                  <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}
                  <span></span>
                  </label>
                </div> -->
                <div class="col m--align-right m-login__form-right">
                  <a href="//icheck.vn/forgot-password" target="_blank" id="m_login_forget_password" class="m-link">Quên mật khẩu?</a>
                </div>
              </div>
              <div class="m-login__form-action">
                <button type="submit" id="m_login_signin_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn m-login__btn--accent">Đăng nhập</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- end:: Page -->
@endsection
