<?php
$current_page = isset($_GET['page']) ? $_GET['page'] : 1;
$limit = 10;
$total_page = ceil($total_record / $limit);
if ($current_page > $total_page) {
    $current_page = $total_page;
} elseif ($current_page < 1) {
    $current_page = 1;
}

$start = ($current_page - 1) * $limit + 1;
if ($total_record < $current_page * $limit) {
    $end = $total_record;
} else {
    $end = $current_page * $limit;
}
echo '<span style="text-align:left;font-weight:bold;color:blue;font-size:15px">';
if ($total_page == 0) {
    echo "Không tồn tại bản ghi nào";
} else {
    echo "Đang hiển thị $start - $end trong tổng số $total_record bản ghi";
}
echo '</span>';
echo "<ul class='pagination pagination-sm' style='float:right;'>";
if (strpos(url()->current(), 'filter') == false) {
    if ($current_page > 1 && $total_page > 1) {
        echo '<li><a href="' . url()->current() . '?page=' . ($current_page - 1) . '" style="background-color:#0c199c ;color:white">Trang trước</a></li>';
    }

    if ($current_page < $total_page && $total_page > 1) {
        echo '<li><a href="' . url()->current() . '?page=' . ($current_page + 1) . '"style="background-color: #f59e00;color:white">Trang sau</a></li>';
    }
}
else
{
    $prev=$current_page-1;
    $next=$current_page+1;
    $prev=str_replace("page=$current_page","page=$prev",url()->full());
    $next=str_replace("page=$current_page","page=$next",url()->full());
    if ($current_page > 1 && $total_page > 1) {
        echo '<li><a href="'.$prev.'" style="background-color: #9c7c32;color:white">Trang trước</a></li>';
    }
    if($current_page==1){

        echo '<li><a href="' . url()->full() . '&page=' . ($current_page + 1) . '" style="background-color: #00aabd;color:white">Trang sau</a></li>';
    }

    if ($current_page > 1 && $current_page < $total_page && $total_page > 1) {
        echo '<li><a href="'.$next.'" style="background-color: #00aabd;color:white">Trang sau</a></li>';
    }
}
echo "</ul>";
?>

<!--
url()->full() lấy ra url hiện tại trên trình duyệt
https://laravel.com/docs/5.6/urls
-->