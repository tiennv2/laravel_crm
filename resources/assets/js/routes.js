import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

import DashboardPage from './pages/DashboardPage';
import ContributedProductsPage from './pages/contributed-product';
import ContributedProductsPendingReviewPage from './pages/contributed-product/PendingReviewPage';
import ContributedProductsReviewHistoricalPage from './pages/contributed-product/ReviewHistoricalPage';
import iamAdminRoutes from './components/iam-admin/routes'
import productRoutes from './components/product/routes'
import businessRoutes from './components/business/routes'
import qrCodeRoutes from './components/qr-code/routes'

const routes = [
  { path: '/', name: 'dashboard', component: DashboardPage },
  {
    path: '/contributedProducts',
    name: 'contributedProduct',
    component: ContributedProductsPage,
    children: [
      {
        path: 'pendingReview',
        name: 'contributedProduct.pendingReview',
        component: ContributedProductsPendingReviewPage
      },
      {
        path: 'reviewHistorical',
        name: 'contributedProduct.reviewHistorical',
        component: ContributedProductsReviewHistoricalPage
      }
    ]
  },
  iamAdminRoutes,
  productRoutes,
  businessRoutes,
  qrCodeRoutes
]

export default new VueRouter({
  routes
})
