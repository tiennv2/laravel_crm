const Helpers = {
  install(Vue, options) {
    const helpers = {
      apiUrl: url => url,
      apiProxyUrl: url => `/apiProxy?url=${encodeURIComponent(url)}`
    }

    Vue.helpers = helpers
    Vue.prototype.$helpers = helpers
  }
}

export default Helpers
