
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import Vue from 'vue'
import store from './store'
import router from './routes'
import Helpers from './helpers'
import VeeValidate from 'vee-validate'
import moment from 'moment'

moment.locale('vi')

Vue.use(VeeValidate)
Vue.use(Helpers)

router.beforeEach((to, from, next) => {
  if (!window.SHARED_DATA.config.isNative) {
    next()
  } else if (to.path !== '/') {
    window.location = '/#' + to.fullPath
  }
})

import 'ag-grid-enterprise/main'
import { LicenseManager } from 'ag-grid-enterprise/main'
LicenseManager.setLicenseKey('huytq_OTk5OTk5OTk5OTk5OQ==e7159f05db6aebc51dfdc18ec1f02b3f')
import "ag-grid/dist/styles/ag-grid.css";
import "ag-grid/dist/styles/ag-theme-material.css";
import "ag-grid/dist/styles/ag-theme-bootstrap.css";

// import 'js-cookie'
// import 'jquery-smooth-scroll'
// import 'moment'
// import 'wnumb'
import 'malihu-custom-scrollbar-plugin'
import 'block-ui'
import 'jstree/dist/themes/default/style.css'
import 'jstree'

// window.swal = require('sweetalert2')

// import '../metronic/js/framework/components/plugins/base/sweetalert2.init.js'

// Metronic
window.mUtil        = require('exports-loader?mUtil!../metronic/js/framework/base/util.js')
window.mApp         = require('exports-loader?mApp!../metronic/js/framework/base/app.js')
window.mDropdown    = require('exports-loader?mDropdown!../metronic/js/framework/components/general/dropdown.js')
window.mHeader      = require('exports-loader?mHeader!../metronic/js/framework/components/general/header.js')
window.mMenu        = require('exports-loader?mMenu!../metronic/js/framework/components/general/menu.js')
window.mOffcanvas   = require('exports-loader?mOffcanvas!../metronic/js/framework/components/general/offcanvas.js')
window.mPortlet     = require('exports-loader?mPortlet!../metronic/js/framework/components/general/portlet.js')
window.mQuicksearch = require('exports-loader?mQuicksearch!../metronic/js/framework/components/general/quicksearch.js')
window.mScrollTop   = require('exports-loader?mScrollTop!../metronic/js/framework/components/general/scroll-top.js')
window.mToggle      = require('exports-loader?mToggle!../metronic/js/framework/components/general/toggle.js')
window.mWizard      = require('exports-loader?mWizard!../metronic/js/framework/components/general/wizard.js')
window.mLayout      = require('exports-loader?mLayout!../metronic/js/demo/demo5/base/layout.js')

window.mApp.initComponents()

import HeaderComponent from './components/HeaderComponent'

Vue.component('app-header', HeaderComponent)

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    store,
    router,
    data() {
      const sharedData = window.SHARED_DATA || {}

      return sharedData
    },
    mounted() {
      let body = document.body
      body.classList.remove('m-page--loading')
    }
})
