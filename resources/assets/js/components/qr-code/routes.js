import Index from './'

export default {
  path: '/qrCode',
  name: 'qrCode',
  component: Index,
  children: [
    {
      path: 'agencies',
      name: 'qrCode.agency.index',
      component: () => import('./ListAgencyComponent')
    },
    {
      path: 'agencies/create',
      name: 'qrCode.agency.create',
      component: () => import('./CreateAgencyComponent')
    },
    {
      path: 'agencies/:id',
      name: 'qrCode.agency.edit',
      component: () => import('./EditAgencyComponent')
    },
    {
      path: 'companies',
      name: 'qrCode.company.index',
      component: () => import('./ListCompanyComponent')
    }
  ]
}
