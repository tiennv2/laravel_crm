import Vue from 'vue'
import Index from './'

export default {
  path: '/product',
  name: 'product',
  component: Index,
  children: [
    {
      path: 'categories',
      name: 'product.category.index',
      component: () => import('./ListCategoryComponent')
    },
    {
      path: 'products',
      name: 'product.product.index',
      component: () => import('./ListProductComponent')
    },
    {
      path: 'products/:gtin',
      name: 'product.product.edit',
      component: () => import('./EditProductComponent')
    },
    {
      path: 'batchUpdateProductStatus',
      name: 'product.product.batchUpdateProductStatus',
      component: () => import('./BatchUpdateProductStatusComponent')
    },
    {
      path: 'batchShowHideProductStore',
      name: 'product.product.batchShowHideProductStore',
      component: () => import('./BatchShowHideProductStoreComponent')
    }
  ]
}
