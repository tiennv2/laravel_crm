import Index from './'
import IAMComponent from './IAMComponent'
import RoleComponent from './RoleComponent'

export default {
  path: '/iamAdmin',
  name: 'iamAdmin',
  component: Index,
  children: [
    {
      path: 'iam',
      name: 'iamAdmin.member.index',
      component: IAMComponent,
      meta: {
        requiresPermissions: [
          'iamAdmin.iam.read'
        ]
      }
    },
    {
      path: 'iam/add',
      name: 'iamAdmin.member.add',
      component: () => import('./AddMemberComponent')
    },
    {
      path: 'roles',
      name: 'iamAdmin.role.index',
      component: RoleComponent
    }
  ]
}
