import Vue from 'vue'

export default {
  path: '/business',
  name: 'business',
  component: {
    template: '<router-view></router-view>'
  },
  children: [
    {
      path: 'businesses',
      name: 'business.business.index',
      component: () => import('./ListBusinessComponent')
    },
    {
      path: 'businesses/create',
      name: 'business.business.create',
      component: () => import('./CreateBusiness')
    },
    {
      path: 'businesses/:id',
      name: 'business.business.edit',
      component: () => import('./EditBusiness')
    },
    {
      path: 'plans',
      name: 'business.plan.index',
      component: () => import('./ListPlanComponent')
    },
    {
      path: 'subscriptions',
      name: 'business.subscription.index',
      component: () => import('./ListSubscriptionComponent')
    },
    {
      path: 'manageProductRequests',
      name: 'business.manageProductRequest.index',
      component: () => import('./ListManageProductRequestComponent')
    },
    {
      path: 'batchAssignProduct',
      name: 'business.batchAssignProduct',
      component: () => import('./BatchAssignProductComponent')
    },
    {
      path: 'sendBusinessMessageToAllUsers',
      name: 'business.sendBusinessMessageToAllUsers',
      component: () => import('./SendBusinessMessageToAllUsersComponent')
    }
  ]
}
