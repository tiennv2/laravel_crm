import Vue from 'vue';
import Vuex from 'vuex';

import contributedProduct from './modules/contributed-product';
import accountModule from './modules/account';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    contributedProduct,
    account: accountModule
  }
})
