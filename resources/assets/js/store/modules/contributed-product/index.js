import Vue from 'vue'
import axios from 'axios'
import { COUNT, PRODUCTS, PENDING_REVIEW_PRODUCTS } from './getter-types'
import { SYNC_COUNT, LOAD_PRODUCTS, UPDATE_PRODUCT, LOAD_PENDING_REVIEW_PRODUCTS } from './action-types'
import { SET_COUNT, SET_PRODUCTS, SET_PRODUCT, SET_PENDING_REVIEW_PRODUCTS } from './mutation-types'

// initial state
const state = {
  count: {
    total: 0,
    pendingReview: 0,
    approved: 0,
    rejected: 0,
  },
  products: [],
  pendingReviewProducts: []
}

// getters
const getters = {
  [COUNT] : state => state.count,

  [PRODUCTS]: state => state.products,

  [PENDING_REVIEW_PRODUCTS]: state => state.pendingReviewProducts
}

// actions
const actions = {
  [SYNC_COUNT] ({ commit }) {
    axios.get('contributed-products/count', { responseType: 'json' }).then(res => {
      let count = res.data
      count.total = count.pendingReview + count.approved + count.rejected

      commit(SET_COUNT, count)
    })
  },

  [LOAD_PENDING_REVIEW_PRODUCTS] ({ commit }) {
    axios.get('contributed-products?type=pending-review', { responseType: 'json' }).then(res => {
      commit(SET_PENDING_REVIEW_PRODUCTS, res.data.data)
    })
  },

  [LOAD_PRODUCTS] ({ commit }) {
    axios.post('contributed-products', {}, { responseType: 'json' }).then(data => {
      commit(SET_PRODUCTS, data.data.data)
    })
  },

  [UPDATE_PRODUCT] ({ state, commit }, product) {
    const index = state.products.findIndex(item => item.id === product.id)

    if (index >= 0) {
      commit(SET_PRODUCT, { index, product })
    }
  }
}

// mutations
const mutations = {
  [SET_COUNT] (state, count) {
    state.count = count
  },

  [SET_PENDING_REVIEW_PRODUCTS] (state, products) {
    state.pendingReviewProducts = products
  },

  [SET_PRODUCTS] (state, products) {
    state.products = products
  },

  [SET_PRODUCT] (state, { index, product }) {
    Vue.set(state.products, index, product)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
