export const SET_COUNT    = 'setCount'
export const SET_PENDING_REVIEW_PRODUCTS  = 'setPendingReviewProducts'
export const SET_PRODUCTS = 'setProducts'
export const SET_PRODUCT  = 'setProduct'
