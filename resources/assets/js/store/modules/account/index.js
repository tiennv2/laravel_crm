import Vue from 'vue'
import axios from 'axios'
import { COUNT, PRODUCTS, PENDING_REVIEW_PRODUCTS } from './getter-types'
import { SYNC_COUNT, LOAD_PRODUCTS, UPDATE_PRODUCT, LOAD_PENDING_REVIEW_PRODUCTS } from './action-types'
import { SET_COUNT, SET_PRODUCTS, SET_PRODUCT, SET_PENDING_REVIEW_PRODUCTS } from './mutation-types'

// initial state
const state = {
  accounts: []
}

// getters
const getters = {
  account: state => (id) => state.accounts.find(account => account.icheckId == id),
  accountByInternalId: state => (id) => state.accounts.find(account => account.id == id)
}

// actions
const actions = {
  async fetchAccounts({ commit }, params) {
    const res = await axios.get('accounts', { params, responseType: 'json' })

    res.data.data.forEach(account => {
      const a = state.accounts.find(item => item.id === account.id)

      if (!a) {
        commit('pushAccount', account)
      } else {
        commit('updateAccount', account)
      }
    })
  }
}

// mutations
const mutations = {
  pushAccount(state, account) {
    state.accounts.push(account)
  },

  updateAccount(state, account) {
    const index = state.accounts.findIndex(item => item.id === account.id)

    if (index > -1) {
      Vue.set(state.accounts, index, account)
    }
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
