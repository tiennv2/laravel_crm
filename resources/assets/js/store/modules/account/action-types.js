export const SYNC_COUNT     = 'syncCount'
export const LOAD_PRODUCTS  = 'loadProducts'
export const LOAD_PENDING_REVIEW_PRODUCTS  = 'loadPendingReviewProducts'
export const UPDATE_PRODUCT = 'updateProduct'
