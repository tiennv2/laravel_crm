webpackJsonp([25],{

/***/ 507:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(12)
/* script */
var __vue_script__ = __webpack_require__(687)
/* template */
var __vue_template__ = __webpack_require__(688)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/components/business/BatchAssignProductComponent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-e81c6342", Component.options)
  } else {
    hotAPI.reload("data-v-e81c6342", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 687:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_lodash__ = __webpack_require__(125);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_axios__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_axios___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_axios__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      businessId: null,
      business: null,
      productIdsString: null,
      productIds: []
    };
  },

  watch: {
    businessId: function businessId(val) {
      this.fetchBusinessInfo();
    },
    productIdsString: function productIdsString(val) {
      this.productIds = this.productIdsString ? this.productIdsString.split(/\r?\n/).filter(function (item) {
        return item.trim() !== '';
      }) : [];
    }
  },
  methods: {
    fetchBusinessInfo: __WEBPACK_IMPORTED_MODULE_0_lodash___default.a.debounce(function () {
      var _this = this;

      if (this.businessId) {
        __WEBPACK_IMPORTED_MODULE_1_axios___default.a.get('/business/businesses/' + this.businessId).then(function (_ref) {
          var data = _ref.data;

          _this.business = data.data;
        }, function () {
          _this.business = null;
        });
      }
    }, 500),
    submit: function submit() {
      var _this2 = this;

      __WEBPACK_IMPORTED_MODULE_1_axios___default.a.post('/business/businesses/' + this.businessId + '/assignProduct', { ids: this.productIds }).then(function (_ref2) {
        var data = _ref2.data;

        alert('done');
        _this2.businessId = null;
        _this2.productIdsString = null;
      }, function () {
        alert('error');
      });
    }
  }
});

/***/ }),

/***/ 688:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    {
      staticClass:
        "m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop  m-body m-page__container"
    },
    [
      _c("div", { staticClass: "m-grid__item m-grid__item--fluid m-wrapper" }, [
        _vm._m(0),
        _vm._v(" "),
        _c("div", { staticClass: "m-content" }, [
          _c("div", { staticClass: "m-form" }, [
            _c("div", { staticClass: "form-group m-form__group" }, [
              _c("label", { attrs: { for: "business-id" } }, [
                _vm._v("ID doanh nghiệp")
              ]),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.businessId,
                    expression: "businessId"
                  }
                ],
                staticClass: "form-control m-input",
                attrs: {
                  type: "text",
                  id: "business-id",
                  placeholder: "ID của doanh nghiệp cần gán sản phẩm"
                },
                domProps: { value: _vm.businessId },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.businessId = $event.target.value
                  }
                }
              })
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "form-group m-form__group" }, [
              _c("label", { attrs: { for: "product-ids" } }, [
                _vm._v("ID của những sản phẩm muốn gán")
              ]),
              _vm._v(" "),
              _c("textarea", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.productIdsString,
                    expression: "productIdsString"
                  }
                ],
                staticClass: "form-control m-input",
                attrs: {
                  id: "product-ids",
                  placeholder: "0000080108238\n0000080108239",
                  rows: "6"
                },
                domProps: { value: _vm.productIdsString },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.productIdsString = $event.target.value
                  }
                }
              }),
              _vm._v(" "),
              _c("span", { staticClass: "m-form__help" }, [
                _vm._v("Mỗi ID nằm trên 1 dòng.")
              ])
            ]),
            _vm._v(" "),
            _vm.business && _vm.productIds.length > 0
              ? _c("p", [
                  _vm._v("Sẽ gán "),
                  _c("strong", [_vm._v(_vm._s(_vm.productIds.length))]),
                  _vm._v(" sản phẩm cho "),
                  _c("strong", [_vm._v(_vm._s(_vm.business.name))])
                ])
              : _vm._e(),
            _vm._v(" "),
            _c("div", { staticClass: "m-form__actions" }, [
              _c(
                "button",
                {
                  staticClass: "btn btn-primary",
                  attrs: {
                    type: "button",
                    disabled: !(_vm.business && _vm.productIds.length > 0)
                  },
                  on: { click: _vm.submit }
                },
                [_vm._v("Thực hiện")]
              )
            ])
          ])
        ])
      ])
    ]
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "m-subheader " }, [
      _c("div", { staticClass: "d-flex align-items-center" }, [
        _c("div", { staticClass: "mr-auto" }, [
          _c("h3", { staticClass: "m-subheader__title" }, [
            _vm._v("Gán nhiều sản phẩm cho doanh nghiệp")
          ])
        ])
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-e81c6342", module.exports)
  }
}

/***/ })

});